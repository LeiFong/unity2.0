﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSet : MonoBehaviour {

	public string weaponName;
    public WeaponItem weaponItem; //scriptableObject da creare e trascinare nell'editor
    public WeaponWrapper weaponWrapper;
    public int defaultGhostHitboxFramesOffset = 10;
    public float lowStanceDamageMultiplier = 0.5f; //se sei in stance bassa, fai questo ammontare meno danno
    public GameObject weaponSprite; //da trascinare nell'editor dallo scheletro, indica quale sprite deve essere attivo
    public string weaponAnimatorParameter; //indica all'animator quale parametro deve essere attivo mentre si indossa quest'arma; il parametro serve a riprodurre 
    //animazioni diverse a seconda dell'arma, per esempio la camminata 
    //public string getParriedAnimationParameter; //ogni arma ha un'animazione diversa del getparried
    public string blockstunAnimationParameter;
    public string parrySuccessAnimationParameter;
    public string unsheathAnimatorParameter;
    public string sheathAnimatorParameter;
    public MoveScript customDash;
    public MoveScript customBackDash; //da settare nell'editor; se non nulli, il dash standard viene sostituito col customDash
    public WallHitSpark wallHitSpark;
    public Transform hitsparkOrigin; //posizione dell'origine dell'hitspark; sarà un oggetto vuoto figlio del bone dell'arma, da trascinare nell'editor dallo scheletro
    public HitsparkGenerator myHitsparkGenerator; //da trascinare nell'editor; ogni arma ha un suo hitsparkgenerator, anche se la maggior parte userà quello di default
    [HideInInspector]
    public Controller2D controller;

	public MoveScript [] moves;
	public ActivationConditions conditions;
    public WeaponGlowAnimationSetter[] glowAnimationSetters;

    [System.Serializable]
    public struct WeaponScaling
    {
        public int baseWeaponDamage;
        public float vitality, stamina, strength, dexterity, synthesis;
    }

    public WeaponScaling [] weaponScaling = new WeaponScaling [10];
    public int currentWeaponUpgrade = 0;

}
