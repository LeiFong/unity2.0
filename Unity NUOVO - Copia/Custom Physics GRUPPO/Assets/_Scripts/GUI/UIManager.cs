﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

//[ExecuteInEditMode]
public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    private PlayersManager playersManager;
    private DialogueManager dialogueManager;
    private PromptManager promptManager;
    public Canvas canvas;    
    public MenuManager baseMenuManager;
    //private MenuManager currentMenuManager;
    [Space(10)]
    public float UIsize = 1f;
    private Vector2 uiSizeVector = new Vector2(1f, 1f);
    public Font font;
    public enum Language { english, italian };
    public Language language = Language.english;
        
    public enum ElementType { button, panel, subPanel, scrollbar }    
    public Color buttonColor, panelColor, subPanelColor, scrollbarColor;

    public enum ButtonName { square, circle, cross, triangle, dirUp, dirLeft, dirDown, dirRight, rightWing, rightTrigger,
        leftWing, leftTrigger, start, leftAnalog, rightAnalog}
    
    [Header("Button Icons")]
    public Sprite sprSquare;
    public Sprite sprCircle;
    public Sprite sprCross;
    public Sprite sprTriangle;
    public Sprite sprDirUp;
    public Sprite sprDirLeft;
    public Sprite sprDirDown;
    public Sprite sprDirRight;
    public Sprite sprRightWing;
    public Sprite sprRightTrigger;
    public Sprite sprLeftWing;
    public Sprite sprLeftTrigger;
    public Sprite sprStart;
    public Sprite sprLeftAnalog;
    public Sprite sprRightAnalog;
    
    public Dictionary<ButtonName, Sprite> spriteDictionary;

    public enum Gender { male, female }
    public Gender playerGender = Gender.male;

    public List<MenuManager> openMenus;    
    private List<WindowManager> windowManagers;
    

    private IEnumerator inputDelayCoroutine;

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
            Instance = this;

        spriteDictionary = new Dictionary<ButtonName, Sprite>();
        spriteDictionary.Add(ButtonName.square, sprSquare);
        spriteDictionary.Add(ButtonName.circle, sprCircle);
        spriteDictionary.Add(ButtonName.cross, sprCross);
        spriteDictionary.Add(ButtonName.triangle, sprTriangle);
        spriteDictionary.Add(ButtonName.dirUp, sprDirUp);
        spriteDictionary.Add(ButtonName.dirLeft, sprDirLeft);
        spriteDictionary.Add(ButtonName.dirDown, sprDirDown);
        spriteDictionary.Add(ButtonName.dirRight, sprDirRight);
        spriteDictionary.Add(ButtonName.rightWing, sprRightWing);
        spriteDictionary.Add(ButtonName.rightTrigger, sprRightTrigger);
        spriteDictionary.Add(ButtonName.leftWing, sprLeftWing);
        spriteDictionary.Add(ButtonName.leftTrigger, sprLeftTrigger);
        spriteDictionary.Add(ButtonName.start, sprStart);
        spriteDictionary.Add(ButtonName.leftAnalog, sprLeftAnalog);
        spriteDictionary.Add(ButtonName.rightAnalog, sprRightAnalog);

        if (canvas == null)
        {
            print("UIManager: Canvas not set from editor!");
            canvas = FindObjectOfType<Canvas>();
        }

        if (canvas == null)
            return;
        

        
        openMenus = new List<MenuManager>();
        windowManagers = new List<WindowManager>();
        playersManager = PlayersManager.Instance ?? FindObjectOfType<PlayersManager>();
        dialogueManager = DialogueManager.Instance ?? FindObjectOfType<DialogueManager>();
        promptManager = PromptManager.Instance ?? FindObjectOfType<PromptManager>();

        SetUISize(UIsize);
        SetFont(font);
        ChangeLanguage(language);
    }


    /*
    public void ChangeLanguage(Language l) {
        language = l;
        List<Text> texts = new List<Text>(canvas.GetComponentsInChildren<Text>());
        for (int i = 0; i < texts.Count; i++)
        {
            
            MultiLanguageText languageText = texts[i].gameObject.GetComponent<MultiLanguageText>();
            if (languageText == null)
                Debug.LogWarning("Detected Text with no language option attached. GameObject: " + texts[i].gameObject.name);
            else
            {
                languageText.SetUIManager(this);
                languageText.UpdateText();
            }
        }
    }
    */
    public void ChangeLanguage(Language l)
    {
        language = l;
    }

    public void ChangeLanguage(int l) {
        switch (l) {
            case 0: ChangeLanguage(Language.english);
                break;
            case 1: ChangeLanguage(Language.italian);
                break;
        }
    }
    
    public void SetUISize(float s) {
        UIsize = s;
        uiSizeVector.x = s;
        uiSizeVector.y = s;

        for (int i = 0; i < windowManagers.Count; i++)
        {
            if (windowManagers[i] == null)
            {
                windowManagers.RemoveAt(i);
                i--;
                continue;
            }

            windowManagers[i].gameObject.transform.localScale = uiSizeVector;
        }
    }

    public void SetFont(Font f)
    {
        font = f;
        List<Text> texts = new List<Text>(FindObjectsOfType<Text>());
        for (int i = 0; i < texts.Count; i++)
        {
            texts[i].font = font;
        }
    }

    public void DelayInput(float time)
    {
        if (inputDelayCoroutine != null)
            StopCoroutine(inputDelayCoroutine);

        inputDelayCoroutine = DelayInteractability(time);
        StartCoroutine(inputDelayCoroutine);
        
    }

    private IEnumerator DelayInteractability(float time)
    {
        EventSystem.current.sendNavigationEvents = false;       
        yield return new WaitForSeconds(time);
        EventSystem.current.sendNavigationEvents = true;
        yield break;
    }

    public void SetButtonIconImage(ButtonName buttonName, Image image) {
        if (image == null)
            return;
        image.sprite = spriteDictionary[buttonName];
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //se non c'è niente di aperto
            if (openMenus.Count == 0)
            {
                baseMenuManager.OpenMenu();
            }
            else
            {
                CloseEverything();

            }

        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            if (openMenus.Count > 0)
            {
                openMenus[openMenus.Count - 1].onCloseRequest.Invoke(new BaseEventData(EventSystem.current));

            }
        }
    }

    /*
    public void SetCurrentMenuManager(MenuManager mm)
    {
        currentMenuManager = mm;

    }
    */
    public void AddMenuManagerToList(MenuManager mm)
    {
        mm.hyerarchyId = openMenus.Count;
        openMenus.Add(mm);
        playersManager.inputsEnabled = false;
        promptManager.promptsEnabled = false;

    }

    public void RemoveMenuManagerFromList(MenuManager mm)
    {
        //scala di 1 gli id di tutti i menu che gli stavano sopra
        for (int i = openMenus.IndexOf(mm) + 1; i < openMenus.Count; i++)
            openMenus[i].hyerarchyId--;
        
        openMenus.Remove(mm);
        if (openMenus.Count == 0) {
            playersManager.inputsEnabled = true;
            promptManager.promptsEnabled = true;
        }
    }
    
    

    private void CloseEverything()
    {
        //rimuovi i null
        for (int i = 0; i < openMenus.Count; i++) {
            if (openMenus[i] == null) {
                openMenus.RemoveAt(i);
                i--;
                continue;
            }
        }

        //se c'è un dialogo aperto, non chiudere nulla
        if (dialogueManager.currentNpc != null)
            return;

        //chiudi i menu
        if (openMenus.Count >= 0)
            openMenus[0].CloseMenu();
        
        
    }


    public void AddWindowManagerToList(WindowManager wm) {
        windowManagers.Add(wm);
        
    }


    public string ChangeStringBasedOnGender(string s)
    {
        
        bool found = false;
        bool skip = false;
        string ret = "";

        for (int i = 0; i < s.Length; i++)
        {
            if (!found && !skip)
            {
                if (s[i] == '@' && i < s.Length - 1)
                {
                    if (s[i + 1] == 'f' && playerGender == UIManager.Gender.female || s[i + 1] == 'm' && playerGender == UIManager.Gender.male)
                    {
                        found = true;
                        i++;
                    }
                    else if (s[i + 1] == 'f' && playerGender == UIManager.Gender.male || s[i + 1] == 'm' && playerGender == UIManager.Gender.female)
                    {
                        skip = true;
                        i++;
                    }

                }
                if (!found && !skip)
                    ret += s[i];
            }
            else
            {
                if (s[i] == '@')
                {
                    found = false;
                    skip = false;
                }
                else if (found)
                    ret += s[i];
            }
        }
        return ret;
    }
}
