﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuffData
{
    public string controllerID;
    //public BuffOwner [] buffOwners;

    [System.Serializable]
    public struct BuffProperties
    {
        public int buffID;
        public int durationLeft;
        public int currentStacks;
    }

    public BuffProperties[] buffs;

    public BuffData (Controller2D controller)
    {
        /*
        controllerID = controller.myID;
        buffOwners = new BuffOwner[controller.buffManager.buffOwners.Count];

        for (int i = 0; i < buffOwners.Length; i++)
        {
            buffOwners[i].buffOwnerID = controller.buffManager.buffOwners[i].ownerID;
            buffOwners[i].buffs = new BuffProperties[controller.buffManager.buffOwners[i].buffs.Count];

            for (int k = 0; k < controller.buffManager.buffOwners[i].buffs.Count; k++)
            {
                buffOwners[i].buffs[k].buffID =
                    controller.
                    buffManager.
                    buffOwners[i].buffs[k].
                    buffScriptableObject.index;

                Debug.Log("savebuffs?");

                buffOwners[i].buffs[k].durationLeft =
                    controller.
                    buffManager.
                    buffOwners[i].buffs[k].durationTimer;

            }
        }

        */

        controllerID = controller.myID;
        buffs = new BuffProperties[controller.buffManager.buffs.Count];

            for (int k = 0; k < controller.buffManager.buffs.Count; k++)
            {
                buffs[k].buffID =
                    controller.
                    buffManager.
                    buffs[k].buff.
                    buffScriptableObject.index;

                Debug.Log("savebuffs?");

                buffs[k].durationLeft =
                    controller.
                    buffManager.
                    buffs[k].durationTimer;

            buffs[k].currentStacks =
                    controller.
                    buffManager.
                    buffs[k].currentStacks;

        }
    }

        
}
