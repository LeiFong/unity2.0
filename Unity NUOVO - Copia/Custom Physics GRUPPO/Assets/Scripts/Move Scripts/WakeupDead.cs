﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WakeupDead : MoveScript
{
    public override void MyStartSequence()
    {
        status.dead = true;
        status.mystatus = Status.status.dead;

        if (!controller.animator.GetBool(controller.dedIndex))
            controller.animator.SetBool(controller.dieingIndex, true);
        else
        {

            controller.animator.SetBool(controller.dieingIndex, false);
        }
        

    }

    public override void MyExecuteSequence()
    {
        status.dead = true;
        status.mystatus = Status.status.dead;

        

        if (controller.seq < 500)
        {
            controller.seq++;
        }
    }

}
