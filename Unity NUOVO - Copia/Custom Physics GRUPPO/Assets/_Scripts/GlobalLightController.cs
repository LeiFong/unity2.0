﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class GlobalLightController : MonoBehaviour
{
    public Light2D globalLight;
    public float defaultIntensity;
    private float targetIntensity;
    private Coroutine currentCoroutine;

    private float startValue, delta;
    private WaitForFixedUpdate waitTime;

    public static GlobalLightController Instance;

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
            Instance = this;

        globalLight.intensity = defaultIntensity;
    }

    public void UpdateIntensity(float value) {
        UpdateIntensity(value, 60);
    }

    public void UpdateIntensity(float value, int time)
    {
        if (currentCoroutine != null)
            StopCoroutine(currentCoroutine);
        currentCoroutine = StartCoroutine(GraduallyChangeIntensity(value, time));
    }

    public void ResetIntensityToDefaultValue(int time) {
        UpdateIntensity(defaultIntensity, time);
    }

    public void ResetIntensityToDefaultValue() {
        ResetIntensityToDefaultValue(60);
    }

    IEnumerator GraduallyChangeIntensity(float value, int time) {
        startValue = globalLight.intensity;
        delta = (value - startValue) / (float)time;

        while (time > 0) {
            time--;
            globalLight.intensity += delta;
            yield return waitTime;
        }
    }
}
