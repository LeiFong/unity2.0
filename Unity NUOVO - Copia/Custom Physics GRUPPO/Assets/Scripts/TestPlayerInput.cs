﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (TestPlayer))]
public class TestPlayerInput : MonoBehaviour {

	TestPlayer player;

	void Start () {
		player = GetComponent<TestPlayer> ();
	}

	void Update () {
		Vector2 directionalInput = new Vector2 (Input.GetAxisRaw ("P1_horizontal"), Input.GetAxisRaw ("P1_vertical"));
		player.SetDirectionalInput (directionalInput);

		if (Input.GetKeyDown (KeyCode.Space)) {
			player.OnJumpInputDown ();
		}
		if (Input.GetKeyUp (KeyCode.Space)) {
			player.OnJumpInputUp ();
		}
	}
}
