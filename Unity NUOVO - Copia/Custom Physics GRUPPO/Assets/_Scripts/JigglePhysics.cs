﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JigglePhysics : MonoBehaviour, IPreAwake
{
    [Tooltip("To set it up: have an animator component on the same GameObject as this; the base layer will contain the animation with the neutral state " +
        "(default animation when no forces are applied). Then add 4 more layers: " +
        "call them \"Up\", \"Down\", \"Left\" and \"Right\"; set them to Additive; in each one, place the additive animation it " +
        "needs to play when pushed in that direction (so if the object moves to the left, the animation on \"Right\" will play); an animation is \"additive\" " +
        "when it contains 2 keyframes: the first one is the neutral state, and the second one is the movement that will be added to the neutral state." +
        " Then set them not to loop, so they'll always stay on the second keyframe.")]
    public bool MouseOverForTutorial;
    public enum AnimatorUpdateMode { fixedUpdate, update }
    public AnimatorUpdateMode animatorUpdateMode = AnimatorUpdateMode.fixedUpdate;
    public Animator animator;
    [Tooltip("The transform that will determine the direction (not the movement itself!); needed for the transform.localScale.x direction." +
        " If null, it will use this GameObject's Transform as reference.")]
    public Transform directionReference;
    private Vector2 prevPosition, prevSpeed, direction;
    private int animUp, animDown, animLeft, animRight;
    private float upW, downW, leftW, rightW;

    [Space(10)]
    [Tooltip("The speed that is considerable as high speed. It refers to the distance made in one frame, so keep it low (default = 0.12). " +
        "The lower, the faster the object starts jiggling; the higher, the more firm it is when it moves.")]
    public Vector2 highSpeedThreshold;
    [Tooltip("The rotation at which you animated the object. Any subsequent rotation will be relative to this. E.g. if the default rotation is" +
        " 0°, and the object gets rotated 90° anti-clockwise, if the object moves to the right, the Up animation will play instead of Left.")]
    [Range(-360, 360)]
    public float defaultRotation = 0f;

    [Space(10)]
    [Range(0f, 360f)]
    [Tooltip("The direction the object will go to by default. 0 = right; 90 = up; 180 = left; 270 = down.")]
    public float gravityDirection = 270f;
    [Range(0f, 1f)]
    [Tooltip("How strong is the gravity; 0 = no gravity.")]
    public float gravityStrength = 0.3f;

    [Space(10)]
    [Range(0f, 20)]
    [Tooltip("How sensitive it is to applied forces. The lower, the more firm it is; the higher, the more it jiggles.")]
    public float sensitivity = 5f;
    private float animatorSpeed;
    [Range(0.1f, 2f)]
    [Tooltip("The base speed of the animations. It will accelerate when the object gains speed.")]
    public float defaultAnimatorSpeed = 1f;
    [Range(0f, 10f)]
    [Tooltip("How much the animations accelerate when the object gains speed.")]
    public float animatorAccelerationRate = 5f;
    
    private Vector2 virtualPosition;

    [Space(10)]
    [Range(1f, 100f)]
    [Tooltip("How bouncy it gets when forces are applied. Higher means a stronger force will pull the jiggling object towards its center.")]
    public float elasticity = 50f;
    private Vector2 speed;
    [Tooltip("How much resistance to movement is applied.")]
    [Range(0, 100f)]
    public float friction = 50f;
    private Transform tr;

    private bool init = false;

    //anti-garbo vars
    private Vector2 maxDist, dist, deltaPos, relativePos, tempRotation;
    private float attrito = 0f, myspeed = 0f, facing = 0f, currentRotation, rotation;
    

    private void Start()
    {
        if (!init)
            PreAwake(0, null);
    }
    public bool PreAwake(int a, BackgroundAwaker bga)
    {
        tr = transform;

        if (animator == null)
            animator = GetComponent<Animator>();

        animUp = animator.GetLayerIndex("Up");
        animDown = animator.GetLayerIndex("Down");
        animLeft = animator.GetLayerIndex("Left");
        animRight = animator.GetLayerIndex("Right");

        if (directionReference == null)
            directionReference = tr;

        animatorSpeed = defaultAnimatorSpeed;

        speed = new Vector2(0, 0);
        
        virtualPosition = new Vector2(tr.position.x, tr.position.y);

        maxDist = new Vector2();
        dist = new Vector2();
        deltaPos = new Vector2();
        relativePos = new Vector2();
        tempRotation = new Vector2();

        init = true;

        return true;
    }
    
    void FixedUpdate()
    {
        if (animatorUpdateMode == AnimatorUpdateMode.fixedUpdate)
            UpdateWeights();
    }

    void Update()
    {
        if (animatorUpdateMode == AnimatorUpdateMode.update)
            UpdateWeights();
    }


    private void UpdateWeights()
    {

        maxDist.x = highSpeedThreshold.x * (21f - sensitivity);
        maxDist.y = highSpeedThreshold.y * (21f - sensitivity);
        dist.x = (virtualPosition.x - tr.position.x);
        dist.y = (virtualPosition.y - tr.position.y);

        //se raggiunge un massimo
        if (Mathf.Abs(dist.x) > maxDist.x)       
            virtualPosition.x -= dist.x - maxDist.x * Mathf.Sign(dist.x);
        if (Mathf.Abs(dist.y) > maxDist.y)        
            virtualPosition.y -= dist.y - maxDist.y * Mathf.Sign(dist.y);

        //applica gravità
        speed.x += 0.1f * gravityStrength * highSpeedThreshold.x * Mathf.Cos(gravityDirection * Mathf.Deg2Rad);
        speed.y += 0.1f * gravityStrength * highSpeedThreshold.y * Mathf.Sin(gravityDirection * Mathf.Deg2Rad);

        //vai verso il centro
        speed.x -= dist.x / ((101f - elasticity) * TimeVariables.currentTimeScale);
        speed.y -= dist.y / ((101f - elasticity) * TimeVariables.currentTimeScale);


        //applica attrito
        attrito = speed.x * friction / (100f * TimeVariables.currentTimeScale);
        if (Mathf.Abs(attrito) > Mathf.Abs(speed.x))
            attrito = speed.x;
        speed.x -= attrito;
        attrito = speed.y * friction / (100f * TimeVariables.currentTimeScale);
        if (Mathf.Abs(attrito) > Mathf.Abs(speed.y))
            attrito = speed.y;
        speed.y -= attrito;

        
        virtualPosition.x += speed.x / TimeVariables.currentTimeScale;
        virtualPosition.y += speed.y / TimeVariables.currentTimeScale;

        //setta la velocità dell'animator in base alla velocità a cui stai andando
        deltaPos.x = tr.position.x - prevPosition.x;
        deltaPos.y = tr.position.y - prevPosition.y;
        myspeed = deltaPos.magnitude;
        animatorSpeed = (myspeed / highSpeedThreshold.magnitude * animatorAccelerationRate / (5f)) + defaultAnimatorSpeed;
        animator.speed = animatorSpeed / TimeVariables.currentTimeScale;

        //crea posizione relativa per capire i pesi dei layer
        facing = Mathf.Sign(directionReference.localScale.x);
        relativePos.x = (virtualPosition.x - tr.position.x) / maxDist.x * facing;
        relativePos.y = (virtualPosition.y - tr.position.y) / maxDist.y;        
        
        //applica eventuali rotazioni alla posizione relativa
        currentRotation = tr.eulerAngles.z * Mathf.Deg2Rad * facing;
        rotation = defaultRotation * Mathf.Deg2Rad - currentRotation;
        tempRotation.x = relativePos.x;
        tempRotation.y = relativePos.y;
        relativePos.x = tempRotation.x * Mathf.Cos(rotation) - tempRotation.y * Mathf.Sin(rotation);
        relativePos.y = tempRotation.x * Mathf.Sin(rotation) + tempRotation.y * Mathf.Cos(rotation);

        //setta i pesi
        animator.SetLayerWeight(animUp, Mathf.Clamp(relativePos.y, 0, 1));
        animator.SetLayerWeight(animDown, -Mathf.Clamp(relativePos.y, relativePos.y, 0));
        animator.SetLayerWeight(animRight, Mathf.Clamp(relativePos.x, 0, 1));
        animator.SetLayerWeight(animLeft, -Mathf.Clamp(relativePos.x, relativePos.x, 0));

        
        prevPosition.x = tr.position.x;
        prevPosition.y = tr.position.y;
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(virtualPosition, new Vector3(virtualPosition.x, virtualPosition.y + 0.2f));
    }

    
}

