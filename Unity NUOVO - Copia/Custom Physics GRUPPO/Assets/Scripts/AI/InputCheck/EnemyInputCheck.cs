﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInputCheck : InputCheck {

	public Vector2 myPosition, targetPosition;
	public float currentDistance, targetDistance;

	public override void SetBuffers ()
	{
		myPosition = gameObject.transform.position;

		player.targetpressed = true;

		if (player.targetList.myTarget != null) {

			targetPosition = player.targetList.myTarget.transform.position;
			currentDistance = Mathf.Abs (myPosition.x - targetPosition.x);
		}

		/*
				if (currentDistance > targetDistance)
					player.horizontal = player.facingRight ? 1 : -1;
				else
					player.horizontal = 0;
		*/
			
	}
}
