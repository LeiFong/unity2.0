﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueOptionChooserCreator : MonoBehaviour
{
    public MultiLanguageText questionText;    
    public MultiLanguageText button1Text;
    public CustomFunction button1Function;
    public MultiLanguageText button2Text;
    public CustomFunction button2Function;
    public MultiLanguageText button3Text;
    public CustomFunction button3Function;
    public MultiLanguageText button4Text;
    public CustomFunction button4Function;
    public MultiLanguageText button5Text;
    public CustomFunction button5Function;

    public void SetUpDialogueOptionChooser(DialogueOptionChooser optionChooser) {

        List<CustomFunction> functions = new List<CustomFunction>();
        List<MultiLanguageText> texts = new List<MultiLanguageText>();

        if (button1Function != null) {
            functions.Add(button1Function);
        }
        if (button1Text != null)
        {
            texts.Add(button1Text);
        }
        if (button2Function != null)
        {
            functions.Add(button2Function);
        }
        if (button2Text != null)
        {
            texts.Add(button2Text);
        }
        if (button3Function != null)
        {
            functions.Add(button3Function);
        }
        if (button3Text != null)
        {
            texts.Add(button3Text);
        }
        if (button4Function != null)
        {
            functions.Add(button4Function);
        }
        if (button4Text != null)
        {
            texts.Add(button4Text);
        }
        if (button5Function != null)
        {
            functions.Add(button5Function);
        }
        if (button5Text != null)
        {
            texts.Add(button5Text);
        }

        optionChooser.DisplayOptions(questionText, texts, functions);
    }
}
