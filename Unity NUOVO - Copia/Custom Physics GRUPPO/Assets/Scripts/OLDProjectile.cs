﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OLDProjectile : MonoBehaviour {

	public FrameData move;
	public bool destroyOnCollision;
	protected bool hasCollided;

	public Vector3 velocity;

	public Vector2 targetVelocity;
	public Rigidbody2D rb;
	protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
	protected Collider2D[] collisionBuffer = new Collider2D[16];


	public BoxCollider2D myCollider;
	public float skinWidth = .015f;
	public LayerMask collisionMask;


	protected string myLayer;
	protected ContactFilter2D contactFilter;

	void OnEnable () {
		rb = GetComponent<Rigidbody2D> ();
		myCollider = GetComponent<BoxCollider2D> ();
		myLayer = LayerMask.LayerToName (gameObject.layer);
		contactFilter.useTriggers = false;
		//contactFilter.SetLayerMask (Physics2D.GetLayerCollisionMask (gameObject.layer));
		contactFilter.SetLayerMask (collisionMask);
		contactFilter.useLayerMask = true;

		move = GetComponent<FrameData> ();
		MyOnEnable ();
	}




	protected virtual void MyOnEnable() {

	}

	protected virtual void ComputeVelocity () {

	}


	void Update () {
		
		ComputeVelocity ();

		velocity.y = targetVelocity.y;
		velocity.x = targetVelocity.x;
		Move (velocity * Time.deltaTime);
	}

	public void Move (Vector3 velocity) {
		CollisionCheck ();

		if (hasCollided) {
			OnCollisionBehaviour ();
			hasCollided = false;
		}

		HorizontalCollisions (ref velocity);
		VerticalCollisions (ref velocity);

		transform.Translate (velocity);		



	}

	public virtual void OnCollisionBehaviour() {

	}

	void CollisionCheck() {
		int count = rb.OverlapCollider(contactFilter, collisionBuffer);
		if (count > 0) {
			for (int i = 0; i < count; i++) {
				string otherLayer = LayerMask.LayerToName (collisionBuffer [i].gameObject.layer);
				//Debug.Log (otherLayer);
				if ((otherLayer == "HurtBox" && collisionBuffer[i].gameObject.GetComponent<HurtboxHit>().status.faction != move.faction)
					|| otherLayer == "Default")
					//if (otherLayer == "HurtBox")
				{
					hasCollided = true;
				}
			}
		}
	}

	void HorizontalCollisions (ref Vector3 velocity) {
		float distance = Mathf.Abs(velocity.x) + skinWidth;
		float directionX = Mathf.Sign (velocity.x);
		int count = rb.Cast (directionX * Vector2.right, contactFilter, hitBuffer, distance);

		if (count > 0) {
			for (int i = 0; i < count; i++) {
				string otherLayer = LayerMask.LayerToName (hitBuffer [i].collider.gameObject.layer);
				Debug.Log (otherLayer);
				if ((otherLayer == "HurtBox" && hitBuffer[i].collider.gameObject.GetComponent<HurtboxHit>().status.faction != move.faction)
					|| otherLayer == "Default")
				//if (otherLayer == "HurtBox")
				{
					//Debug.Log (hitBuffer[i].collider.gameObject.GetComponent<HurtboxHit>().status.faction + " " + move.faction + " " + otherLayer);
					//hasCollided = true;
					float newDistance = hitBuffer [i].distance - skinWidth;
					//Debug.Log (newDistance + " " + distance);
					//Debug.Log (newDistance + " " + hitBuffer[i].distance);
					distance = newDistance < distance ? newDistance : distance;
					velocity.x = distance * directionX;
				}


			}
		}

		//Debug.Log ("prima: " + velocity.x);

		//Debug.Log (" dopo: " + velocity.x);
	}

	void VerticalCollisions (ref Vector3 velocity) {
		float distance = velocity.magnitude;
		float directionY = Mathf.Sign (velocity.y);
		int count = rb.Cast (directionY * Vector2.up, contactFilter, hitBuffer, distance);

		if (count > 0) {
			for (int i = 0; i < count; i++) {

				string otherLayer = LayerMask.LayerToName (hitBuffer [i].collider.gameObject.layer);

				if ((otherLayer == "HurtBox" && hitBuffer [i].collider.gameObject.GetComponent<HurtboxHit> ().status.faction != move.faction)
				    || otherLayer == "Default") {
					//hasCollided = true;
					float newDistance = hitBuffer [i].distance - skinWidth;
					//Debug.Log (newDistance + " " + distance);
					//Debug.Log (newDistance + " " + hitBuffer[i].distance);
					distance = newDistance < distance ? newDistance : distance;
					velocity.y = distance * directionY;
				}
			}

		}

		//Debug.Log ("prima: " + velocity.x);

		//Debug.Log (" dopo: " + velocity.x);
	}
}
