﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParryActivator : MoveScript {

	public int parryDuration = 20;
	public int parryWindowStart = 3;
	public int parryWindowEnd = 10;
	public MoveScript parryMove;
	public int randomParryFailChance = 0;
	public bool blocksOutsideParryWindow = true;

	public override void MyExecuteSequence ()
	{
		if (controller.seq < parryWindowEnd && controller.seq >= parryWindowStart)
		{
			

			if (randomParryFailChance != 0)
			{
				if (Random.Range(0, 100) >= randomParryFailChance)
				{
					controller.isParrying = true;
				}
				else controller.isParrying = false;
			}
			else
				controller.isParrying = true;
			//controller.isParrying = true;

		}
		else
			controller.isParrying = false;

		if (blocksOutsideParryWindow)
        {
			status.mystatus = Status.status.blocking;
        }
		else
        {
			if (controller.isParrying)
				status.mystatus = Status.status.blocking;
			else
				status.mystatus = Status.status.attacking;
		}
			/*
			if (controller.seq >= parryWindowStart - 1)
				controller.status.mystatus = Status.status.blocking;
			else
				controller.status.mystatus = Status.status.attacking;
			*/



			if (controller.seq >= parryDuration)
		{

			itsTimeToStopTheSequence = true;
			/*
			if (relatedMoves[0] != null && isCharging)
			{
				
				controller.StartNewMove(relatedMoves[1]);
			}
			*/
		}
		else
		{
			//if (controller.seq < parryDuration -1 || !controller.holdingDash)
			{
				controller.seq++;
			}

		}

	}


	public override void SetStatus()
	{
		if (blocksOutsideParryWindow)
		{
			status.mystatus = Status.status.blocking;
		}
		else
		{
			if (controller.isParrying)
				status.mystatus = Status.status.blocking;
			else
				status.mystatus = Status.status.attacking;
		}
	}

	public override void MyStopSequence ()
	{
		controller.isParrying = false;
	}
}
