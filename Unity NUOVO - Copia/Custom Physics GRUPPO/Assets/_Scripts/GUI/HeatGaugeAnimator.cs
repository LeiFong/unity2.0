﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatGaugeAnimator : MonoBehaviour
{
    private bool initialized = false;
    public float lowSpeed = 0.4f, mediumSpeed = 1.5f, highSpeed = 3f;
    private Animator animator;
    public enum Heat { lowHeat, mediumHeat, highHeat }    
    private int redParam, blueParam, heatParam, heatIncreaseParam, glowParam, glow2Param, speedParam;
    private float defaultSpeed = 0.8f;
    public enum BackgroundColor { red, blue }
    private Heat currentHeat;
    private BackgroundColor currentColor;

    private void Awake()
    {
        if (!initialized)
            Init();
    }

    public void Init()
    {
        initialized = true;
        animator = GetComponent<Animator>();
        redParam = Animator.StringToHash("Red");
        blueParam = Animator.StringToHash("Blue");
        heatParam = Animator.StringToHash("Heat");
        heatIncreaseParam = Animator.StringToHash("Heat increase");
        glowParam = Animator.StringToHash("Glow");
        glow2Param = Animator.StringToHash("Glow2");
        speedParam = Animator.StringToHash("Loop speed");
        SetHeat(Heat.lowHeat);
        SetColor(BackgroundColor.blue);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
            StartHeatIncreaseAnimation();
        else if (Input.GetKeyDown(KeyCode.O))
            if (currentHeat == Heat.lowHeat)
                SetHeat(Heat.mediumHeat);
            else if (currentHeat == Heat.mediumHeat)
                SetHeat(Heat.highHeat);
            else
                SetHeat(Heat.lowHeat);
        else if (Input.GetKeyDown(KeyCode.I))
            if (currentColor == BackgroundColor.red)
                SetColor(BackgroundColor.blue);
            else
                SetColor(BackgroundColor.red);
    }

    public void SetHeat(Heat heat) {
        switch (heat) {
            case Heat.lowHeat:
                animator.SetFloat(speedParam, lowSpeed);
                animator.SetInteger(heatParam, 0);
                defaultSpeed = lowSpeed;
                currentHeat = Heat.lowHeat;
                break;
            case Heat.mediumHeat:
                animator.SetFloat(speedParam, mediumSpeed);
                animator.SetInteger(heatParam, 1);
                defaultSpeed = mediumSpeed;
                currentHeat = Heat.mediumHeat;
                break;
            case Heat.highHeat:
                animator.SetFloat(speedParam, highSpeed);
                animator.SetInteger(heatParam, 2);
                defaultSpeed = highSpeed;
                currentHeat = Heat.highHeat;
                break;
        }
    }

    public void SetColor(BackgroundColor color) {
        switch (color) {
            case BackgroundColor.red:
                animator.SetTrigger(redParam);
                currentColor = BackgroundColor.red;
                break;
            case BackgroundColor.blue:
                animator.SetTrigger(blueParam);
                currentColor = BackgroundColor.blue;
                break;
        }
        Glow2();
    }

    public void StartHeatIncreaseAnimation() {
        //animator.SetLayerWeight(3, 1);
        //animator.SetTrigger(heatIncreaseParam);
        Glow();
        //animator.SetFloat(speedParam, 5f);
    }

    public void StopHeatIncreaseAnimation() {
        //animator.SetFloat(speedParam, defaultSpeed);
        //animator.SetLayerWeight(3, 0);
    }

    public void SetHeatIncreaseLayerWeight(float value) {
        animator.SetLayerWeight(3, value);
    }

    public void Glow() {
        animator.SetTrigger(glowParam);
    }
    public void Glow2()
    {
        animator.SetTrigger(glow2Param);
    }

}
