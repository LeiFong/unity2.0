﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;

public class DialogueOptionChooser : MonoBehaviour
{
       
    public int maximumNumberOfOptions = 5;
    public Text questionText;
    
    public List<Button> buttons;
    private MenuManager myMenuManager;
    private DialogueManager dialogueManager;
    public RectTransform buttonContainerRT;
    private float defaultH, defaultPosY;


    // Start is called before the first frame update
    void Start()
    {
        myMenuManager = GetComponent<MenuManager>();
        dialogueManager = FindObjectOfType<DialogueManager>();
        RectTransform t = (RectTransform)myMenuManager.transform;
        defaultH = t.rect.height;
        defaultPosY = t.anchoredPosition.y;
        for (int i = 0; i < maximumNumberOfOptions; i++) {
            buttons[i].animator.keepAnimatorControllerStateOnDisable = true;
        }
    }


    public void DisplayOptions(MultiLanguageText question, List<MultiLanguageText> texts, List<CustomFunction> functions) {
        int n = functions.Count;
        if (n > maximumNumberOfOptions)
            n = maximumNumberOfOptions;

        RectTransform t = (RectTransform)myMenuManager.transform;
        float optionsPanelH = buttonContainerRT.rect.height;
        float buttonSize = optionsPanelH / 2;
        t.sizeDelta = new Vector2(t.sizeDelta.x, defaultH - optionsPanelH + n * buttonSize);
        t.anchoredPosition = new Vector2(t.anchoredPosition.x, (t.rect.height - defaultH) / 2 + defaultPosY);
        

        for (int i = 0; i < n; i++) {
            if (texts[i] != null && functions[i] != null) {
                SetCustomFunction(functions[i], i);
                SetButtonText(texts[i], i);                
            }
        }

        List<RectTransform> rects = new List<RectTransform>();
        for (int i = 0; i < maximumNumberOfOptions; i++) {
            rects.Add((RectTransform)buttons[i].transform);
            rects[i].anchorMin = new Vector2(0, (float)(1 - (1 / (float)n) * ((float)i + 1)));
            rects[i].anchorMax = new Vector2(1, rects[i].anchorMin.y + (float)(1 / (float)n));
            rects[i].anchoredPosition = new Vector2(0, 0);
            buttons[i].interactable = true;
            buttons[i].gameObject.SetActive(true);
            if (i >= n) {
                buttons[i].interactable = false;
                buttons[i].gameObject.SetActive(false);
            }
        }
        
        if (question != null)
            SetQuestionText(question);

    }

    public void SetCustomFunction(CustomFunction cf, int num) {
        buttons[num].onClick.RemoveAllListeners();
        buttons[num].onClick.AddListener(myMenuManager.CloseMenu);
        buttons[num].onClick.AddListener(cf.InvokeCustomFunction);
    }

    public void SetButtonText(MultiLanguageText mltext, int num) {
        Text t = buttons[num].gameObject.GetComponentInChildren<Text>();
        mltext.UpdateText(t);
    }

    public void SetQuestionText(MultiLanguageText mltext)
    {        
        mltext.UpdateText(questionText);
    }
}
