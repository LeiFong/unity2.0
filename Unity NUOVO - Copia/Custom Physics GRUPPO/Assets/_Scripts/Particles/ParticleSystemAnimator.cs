﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemAnimator : MonoBehaviour
{
    [Tooltip("Metti questo componente nello stesso GO di un animator. In questo modo puoi usare gli animation event. " +
        "Imposta le categorie di particle system tipo 'Ex Cannon', e poi inseriscici tutti i particle system tipici dell'ex cannon. " +
        "Poi, nell'animation event, chiama il metodo SetActiveCategory(\"Ex Cannon\") per istruirmi di usare la categoria Ex Cannon. " +
        "Poi, chiama il metodo SetActiveIndex(0) per iniziare a usare, ad esempio, il primo particle system della categoria Ex Cannon. " +
        "Infine, chiama i metodi ParticleStart(), ParticleStop(), ParticleBurst() per usare il particle system selezionato.")]
    public bool tutorial;

    [System.Serializable]
    public struct ParticleSystemCategory {
        public string categoryName;
        public List<ParticleSystem> particleSystems;
    }

    public List<ParticleSystemCategory> categories;

    private string activeCategory;
    private int activeIndex = 0;

    private Dictionary<string, List<ParticleSystem>> categoryDictionary;

    private void Awake()
    {
        categoryDictionary = new Dictionary<string, List<ParticleSystem>>();
        for (int i = 0; i < categories.Count; i++) {
            categoryDictionary.Add(categories[i].categoryName, categories[i].particleSystems);
        }
    }
    

    public void SetActiveIndex(int i) {
        activeIndex = i;
    }
    public void SetActiveCategory(string c)
    {
        activeCategory = c;
    }


    public void ParticleStart()
    {
        if (categoryDictionary[activeCategory][activeIndex] == null || activeIndex >= categoryDictionary[activeCategory].Count)
            return;
        categoryDictionary[activeCategory][activeIndex].Stop();
        categoryDictionary[activeCategory][activeIndex].Play(false);
    }

    public void ParticleStartWithChildren() {
        if (categoryDictionary[activeCategory][activeIndex] == null || activeIndex >= categoryDictionary[activeCategory].Count)
            return;
        categoryDictionary[activeCategory][activeIndex].Stop();
        categoryDictionary[activeCategory][activeIndex].Play(true);
    }

    public void ParticleStop()
    {
        if (categoryDictionary[activeCategory][activeIndex] == null)
            return;
        categoryDictionary[activeCategory][activeIndex].Stop();
    }

    public void ParticleStopAllCurrentCategory() {
        for (int i = 0; i < categoryDictionary[activeCategory].Count; i++) {
            categoryDictionary[activeCategory][i].Stop();
        }
    }

    public void ParticleBurst(int num) {
        if (categoryDictionary[activeCategory][activeIndex] == null)
            return;
        categoryDictionary[activeCategory][activeIndex].Emit(num);
    }
    
}
