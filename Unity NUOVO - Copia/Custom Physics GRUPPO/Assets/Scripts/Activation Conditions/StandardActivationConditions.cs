﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardActivationConditions : ActivationConditions {

	public bool zulul = false;

	public override void CheckConditions () {

        if (cantChangeStamina > 0)
            cantChangeStamina--;

        if (controller.enemySpecializer != null && (controller.enemySpecializer.isAI || controller.enemySpecializer.isDummy))
        {
            for (int i = 0; i < controller.enemyBuffers.Count; i++)
            {
                if (controller.enemyBuffers[i] > 0)
                {
                    controller.enemyBuffers[i]--;

                        if ((controller.hitstop <= 0) && CheckUsability(moves[i]))
                        {
                            controller.StartNewMove(moves[i].move);
                            controller.enemyBuffers[i] = 0;
                        }
                }
            }

            for (int i = 0; i < controller.enemyAnimationsBuffers.Count; i++)
            {
                if (controller.enemyAnimationsBuffers[i] > 0)
                {
                    controller.enemyAnimationsBuffers[i]--;

                    if ((controller.hitstop <= 0) && CheckUsability(animations[i]))
                    {
                        controller.StartNewMove(animations[i].move);

                        controller.enemyAnimationsBuffers[i] = 0;
                    }
                }
            }
        }
        else
        {
            if (controller.hitstop <= 0 && controller.special1Buffer > 0)
            {
                /*
                if (CheckUsability(animations[23]))
                {

                    controller.StartNewMove(animations[23].move);
                    controller.special1Buffer = 0;
                }
                */

            }




            if (controller.hitstop <= 0 && controller.special2Buffer > 0)
            {
                //print("special maybe?");
                /*
                if (specialMoveUsable <= 0 && CheckUsability(moves[11]))
                {
                    controller.StartNewMove(moves[11].move);
                    controller.special2Buffer = 0;
                }
                

                if (CheckUsability(moves[14]))
                {
                    //print("sspecialll");
                    controller.StartNewMove(moves[14].move);
                    
                    controller.special2Buffer = 0;
                }
                */

                if (CheckUsability(animations[24]) || CheckUsability(animations[2]))
                {
                    controller.StartNewMove(animations[24].move);
                    controller.special2Buffer = 0;
                }

            }


            /*
		if (zulul && Random.value < 0.005) {
			if (CheckUsability(animations[2])) {
				controller.StartNewMove (animations[2].move);

			}
		}
		if (CheckUsability(moves[2])) {
			controller.StartNewMove (moves [2].move);
			controller.atk1Buffer = 0;
		}

*/



            /*
            if (controller.parryBuffer > 0) // || (controller.holdingDash && controller.horizontal == 0))
            {

                //if (CheckUsability(moves[9]) && !controller.inHitstop)
                if (CheckUsability(animations[2]) && !controller.inHitstop)

                {

                    //print("block");
                    controller.StartNewMove(moves[9].move);
                    
                    
                    controller.parryBuffer = 0;
                }

            }
            
            else if (controller.blockBuffer > 0)
            {
                if (CheckUsability(moves[20]) && !controller.inHitstop)
                {

                    //print("block");
                    controller.StartNewMove(moves[20].move);


                    controller.blockBuffer = 0;
                }
            }
            */









            
            /*

                if (status.currentStance == Status.Stances.slow)
                {
                    if (CheckUsability(moves[5]))
                    {
                        //print ("CIAO");
                        controller.StartNewMove(moves[5].move);
                        controller.atk2Buffer = 0;
                    }
                }
                else
                {
                    if (CheckUsability(moves[12]))
                    {
                        //print ("CIAO");
                        controller.StartNewMove(moves[12].move);
                        controller.atk2Buffer = 0;
                    }

                }

                if (CheckUsability(moves[3]))
                {
                    //print ("CIAO");
                    controller.StartNewMove(moves[3].move);
                    controller.atk2Buffer = 0;
                }

            
            }
            */
            /*
                        else if (controller.currentMove == moves[17].move)
                        {
                            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
                            {
                                //print("chaaarge");
                                moves[17].move.isCharging = true;
                            }
                            else
                                moves[17].move.isCharging = false;
                        }
                        */
        }
        
        CustomCheckConditions();

    }


	


	
}
