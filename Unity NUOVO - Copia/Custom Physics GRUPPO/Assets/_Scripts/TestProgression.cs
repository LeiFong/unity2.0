﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TestProgression : MonoBehaviour
{
    
    [System.Serializable]
    public struct Section {
        public float difficulty;
        public int expReward;
        public float gearReward;
    }

    [System.Serializable]
    public struct TestingOrder
    {        
        public OrderType ordertype;
        public List<int> order;
        public int n;
    }

    public enum CharacterLevelProgression { custom, dsLike, linear, exponential, constant }
    public CharacterLevelProgression characterLevelProgression = CharacterLevelProgression.custom;
    public enum GearCharacterLevelProgression { overwrite, cumulative }
    public GearCharacterLevelProgression gearCharacterLevelProgression = GearCharacterLevelProgression.overwrite;
    public enum GrowthType { linear, incremental }
    public enum OrderType { manual, fromLowToHigh, nReverse, fromHighToLow }


    public bool printAllSections = true;
    public bool noMistakes = false;
    [Space(10)]
    public float playerSkill = 10f;
    [Space(10)]
    public float startingGear = 1f;

    [Space(20)]
    public List<TestingOrder> testingOrders;
    [Space(20)]
    public float difficultyWeight = 1f;    
    public float playerSkillWeight = 1f, gearLevelWeight = 1f, charLevelWeight = 1f;
    
    [Space(20)]
    public GrowthType powerGainOnLevelUp = GrowthType.linear;
    public int basePowerGainOnLevelUp = 1;
    public float powerGainOnLevelUpMultiplier = 0.3f;
    [Space(20)]
    public bool automaticallySetDifficulty = false;
    public GrowthType difficultyGrowth = GrowthType.linear;
    public float difficultyIncrease = 0.2f;
    [Space(20)]
    public bool automaticallySetGearRewards = false;
    public GrowthType gearGrowth = GrowthType.linear;
    public float gearRewardIncrease = 0.2f;

    [SerializeField]
    public List<Section> allSections;

    public List<int> levelUpReqs;
    

    [Space(10)]
    public int charLevel;
    public int expPoints;
    [Space(10)]
    public float gearLevel;

    private List<float> comparison;
    


    // Start is called before the first frame update
    void OnEnable()
    {
        if (automaticallySetDifficulty) {
            automaticallySetDifficulty = false;
            SetDifficultyToType(difficultyGrowth, difficultyIncrease);

        }
        if (automaticallySetGearRewards)
        {
            automaticallySetGearRewards = false;
            SetGearRewardsToType(gearGrowth, gearRewardIncrease);
        }

        comparison = new List<float>();
        for (int i = 0; i < allSections.Count; i++)
            comparison.Add(0);

        for (int i = 0; i < testingOrders.Count; i++) {
            TestingOrder to = testingOrders[i];
            
            List<int> l = new List<int>(allSections.Count);
            for (int j = 0; j < allSections.Count; j++)
                l.Add(0);
            switch (to.ordertype) {
                case OrderType.nReverse:
                    int n = to.n - 1;
                    int k = 0;
                    for (int j = 0; j < allSections.Count; j++) {
                        while (n + k >= allSections.Count)
                            k--;
                        l[j] = (n + k);
                        n--;
                        if (n < 0)
                        {
                            n = to.n - 1;
                            k = j+1;
                        }
                    }
                    to.order = l;
                    break;

                case OrderType.fromHighToLow:
                    for (int j = 0; j < allSections.Count; j++)
                        l[j] = (allSections.Count - j - 1);
                    to.order = l;
                    break;

                case OrderType.fromLowToHigh:
                    for (int j = 0; j < allSections.Count; j++)
                        l[j] = j;                  
                    to.order = l;
                    break;
            }
            testingOrders[i] = to;
        }


        if (testingOrders.Count > 0)
            print("TOTAL TIME TO COMPLETE THE GAME: " + Compute(testingOrders[0].order, true));
        for (int i = 1; i < testingOrders.Count; i++) {
            print("TOTAL TIME TO COMPLETE THE GAME: " + Compute(testingOrders[i].order, false));
        }

    }


    public float Compute(List<int> order, bool toBeCompared) {
        charLevel = 1;
        expPoints = 0;
        gearLevel = startingGear;
        float tot = 0, averageDeathCount = 0, sectionTime = 0;
        for (int i = 0; i < order.Count; i++) {
            if (characterLevelProgression == CharacterLevelProgression.custom && charLevel >= levelUpReqs.Count)
            {
                print("Reached level cap");
                break;
            }
            averageDeathCount = difficultyWeight * (allSections[order[i]].difficulty);
            averageDeathCount /= (float)charLevel * charLevelWeight + playerSkill * playerSkillWeight + gearLevel * gearLevelWeight;
            averageDeathCount = Mathf.Pow(averageDeathCount, 2);
            if (noMistakes)
                averageDeathCount = 0;

            float dps = 0;
            if (powerGainOnLevelUp == GrowthType.linear)
                dps = (gearLevel + ((float)charLevel) * basePowerGainOnLevelUp);
            else if (powerGainOnLevelUp == GrowthType.incremental)
            {
                float charPower = basePowerGainOnLevelUp;
                for (int j = 1; j < charLevel; j++)
                    charPower += ComputeIncremental(basePowerGainOnLevelUp, powerGainOnLevelUpMultiplier, j);

                dps = gearLevel + charPower;
            }

            sectionTime = allSections[order[i]].difficulty / dps;
            sectionTime += sectionTime * averageDeathCount * 0.6f;
            if (printAllSections)
            {
                if (averageDeathCount != 0)
                    print("Level " + charLevel + "; gear level " + gearLevel + "; section " + order[i] + " - average death count: " + averageDeathCount + "; time: " + sectionTime +
                        " (" + (Mathf.Sign(sectionTime - comparison[i]) > 0 ? "+" : "") + (sectionTime - comparison[i]) + ")");
                else
                    print("dps: " + dps + "; level " + charLevel + "; gear level " + gearLevel + "; section difficulty " + allSections[order[i]].difficulty + "; time: " + sectionTime +
                        " (" + (Mathf.Sign(sectionTime - comparison[i]) > 0 ? "+" : "") + (sectionTime - comparison[i]) + ")");
            }
            tot += sectionTime;

            if (toBeCompared)
                comparison[i] = sectionTime;

            expPoints += allSections[order[i]].expReward;
            LevelUp();
            if (gearCharacterLevelProgression == GearCharacterLevelProgression.overwrite)
                gearLevel = Mathf.Max(gearLevel, allSections[order[i]].gearReward);
            else if (gearCharacterLevelProgression == GearCharacterLevelProgression.cumulative)
                gearLevel += allSections[order[i]].gearReward;
        }
        return tot;
    }

    private void LevelUp() {

        switch (characterLevelProgression)
        {
            case CharacterLevelProgression.custom:
                while (charLevel < levelUpReqs.Count && expPoints >= levelUpReqs[charLevel - 1])
                {
                    expPoints -= levelUpReqs[charLevel - 1];
                    charLevel++;
                }
                break;
            case CharacterLevelProgression.linear:

                while (expPoints >= charLevel * 10)
                {
                    expPoints -= charLevel * 10;
                    charLevel++;
                }
                break;
            case CharacterLevelProgression.constant:

                while (expPoints >= 12)
                {
                    expPoints -= 12;
                    charLevel++;
                }
                break;
            case CharacterLevelProgression.dsLike:
                int e = 10;
                for (int i = 0; i < charLevel; i++) {
                    e += e / 10;
                }

                while (expPoints >= e)
                {
                    expPoints -= e;
                    charLevel++;
                    e += e / 10;
                }
                break;
            case CharacterLevelProgression.exponential:

                while (expPoints >= Mathf.Pow(2, charLevel))
                {
                    expPoints -= (int)Mathf.Pow(2, charLevel);
                    charLevel++;
                }
                break;
        }
    }

    private float ComputeIncremental(float start, float multiplier, int times) {
        while (times > 0) {
            start += start * multiplier;
            times--;
        }
        return start;
    }

    private void SetDifficultyToType(GrowthType type, float increment)
    {
        switch (type)
        {
            case GrowthType.incremental:
                for (int i = 0; i < allSections.Count; i++)
                {
                    Section s = allSections[i];
                    s.difficulty = i == 0 ? allSections[0].difficulty : allSections[i - 1].difficulty + allSections[i - 1].difficulty * increment;
                    allSections[i] = s;
                }
                break;
            case GrowthType.linear:
                for (int i = 0; i < allSections.Count; i++)
                {
                    Section s = allSections[i];
                    s.difficulty = i == 0 ? allSections[0].difficulty : allSections[i - 1].difficulty + increment;
                    allSections[i] = s;
                }
                break;
        }
    }
    private void SetGearRewardsToType(GrowthType type, float increment)
    {
        switch (type)
        {
            case GrowthType.incremental:
                for (int i = 0; i < allSections.Count; i++)
                {
                    Section s = allSections[i];
                    s.gearReward = i == 0 ? allSections[0].gearReward : allSections[i - 1].gearReward + allSections[i - 1].gearReward * increment;
                    allSections[i] = s;
                }
                break;
            case GrowthType.linear:
                for (int i = 0; i < allSections.Count; i++)
                {
                    Section s = allSections[i];
                    s.gearReward = i == 0 ? allSections[0].gearReward : allSections[i - 1].gearReward + increment;
                    allSections[i] = s;
                }
                break;
        }
    }

}
