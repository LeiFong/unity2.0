﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpecializer : MonoBehaviour {
    //classe da inserire nell'editor per rendere il Player un'intelligenza artificiale. Contiene le inizializzazioni dei behavior, gli inputCheck e altre cose

    public bool isAI = false;
	public bool isDummy = false;
	public Transform healthBarPosition;
	public BehaviourSelector bSelector;
    //public EnemyDetector enemyDetector;
	public int lootDropFrame;
	public bool hasDroppedLoot;
	public bool onlyDropsOnce;
	public bool onlyDiesOnce;
	public GameObject lootContainer;
	//public BehaviorOption currentBehaviour;
	public Player controller;
	public InputRecorder inputRecorder;
	[SerializeField]
	CharacterContainer characterContainer;
	public int dannoSwing, dannoParry, vitaMia, vitaNemico;
	

	[System.Serializable]
	public struct LootDropProbability
	{
		public Loot loot;
		[Range(0, 100)]
		public float dropProbability;
	}




	public LootDropProbability[] lootDropProbabilities;

    private void Start()
    {
		//if (controller.mystatus.faction == 0)
		if (!isAI)
		TESTparryValues();
    }
    void TESTparryValues()
    {
		int tot = 0;
		while (vitaMia > dannoParry)
        {
			int heybubu = 2 * dannoSwing + 4 * dannoSwing;
			tot += heybubu;
			vitaMia -= dannoParry;
			
        }
		//print("tot: " + tot);
    }

	public void LoadInputs ()
	{
		//print("enemyyyy " + gameObject);

		//characterContainer = GetComponentInParent<CharacterContainer>();

		//enemyDetector = characterContainer.GetComponentInChildren<EnemyDetector>();
		//bSelector = characterContainer.GetComponentInChildren<BehaviourSelector> ();
		//targetingTransform = controller.targetList.transform;
		bSelector.controller = characterContainer.controller;
        //bSelector.enemyDetector = enemyDetector;
		bSelector.InitializeBehaviours ();

		//inputRecorder = GetComponent<InputRecorder>();

		if (controller.enemyBuffers.Count < controller.moves.Length)
		for (int i = 0; i < controller.moves.Length; i++) {
			controller.enemyBuffers.Add (0);
		}
		if (controller.enemyAnimationsBuffers.Count < controller.animations.Length)
			for (int i = 0; i < controller.animations.Length; i++)
        {
            controller.enemyAnimationsBuffers.Add(0);
        }

		ResetLoots();
	}

	public void ResetLoots ()
	{
		//print("resetloots");
		for (int i = 0; i < lootDropProbabilities.Length; i++)
		{
			if (lootDropProbabilities[i].loot != null)
			{
                //lootDropProbabilities[i].loot.gameObject.transform.position = transform.position;


                if (onlyDropsOnce)
				{
					lootDropProbabilities[i].loot.serializableEvent = true;

                    //lootDropProbabilities[i].loot.dropsFromEnemy = false;

                }
                else
				{
					lootDropProbabilities[i].loot.serializableEvent = false;
					//lootDropProbabilities[i].loot.dropsFromEnemy = true;

				}
                lootDropProbabilities[i].loot.DeactivateEvent();

                lootDropProbabilities[i].loot.dropsFromEnemy = true;

                //lootDropProbabilities[i].loot.gameObject.SetActive(false);
				lootDropProbabilities[i].loot.owner = lootContainer;
				lootDropProbabilities[i].loot.transform.parent = lootContainer.transform;


				//lootDropProbabilities[i].loot.resetsOnLoad = true;
			}
		}
	}

	public void InputCheck ()
	{
		/*
		if (inputRecorder != null)
		{
			inputRecorder.SetBuffers();
			return;
		}
		*/
		controller.targetList.EnemyDetect();
		bSelector.BehaviourSelect ();
		//if (Time.frameCount > 200)
		//controller.horizontal = 1;
		//if (Time.frameCount == 130)
			//controller.enemyAnimationsBuffers[4] = 20;
		//controller.targetVelocity.x = 2;



		if (controller.targetList.myTarget != null && bSelector.constantBehaviour != null) {
			bSelector.constantBehaviour.ExecuteBehaviour();
		}
		if (bSelector.currentBehaviour != null) {
				bSelector.currentBehaviour.ExecuteBehaviour ();
            
			//else
			{
				bSelector.skipBehaviourExecution = false;
			}
		}
	}

	
}
