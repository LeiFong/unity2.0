﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIElementColor : MonoBehaviour
{
    public UIManager.ElementType elementType = UIManager.ElementType.button;
    private UIManager manager;
    private Image image;

    // Start is called before the first frame update
    void Start()
    {
        manager = UIManager.Instance ?? FindObjectOfType<UIManager>();
        image = GetComponent<Image>();
        switch (elementType) {
            case UIManager.ElementType.button:
                image.color = manager.buttonColor;
                break;
            case UIManager.ElementType.panel: image.color = manager.panelColor;                
                break;
            case UIManager.ElementType.scrollbar: image.color = manager.scrollbarColor;
                break;
            case UIManager.ElementType.subPanel: image.color = manager.subPanelColor;
                break;
        }
    }

}
