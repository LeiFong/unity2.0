﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopstateSetter : MonoBehaviour
{
    Animator animator; 
    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    // Start is called before the first frame update
    public void SetLoopState()
    {
        animator.SetBool("loopstate", true);
    }
}
