﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordActivationConditions : StandardActivationConditions
{
    protected override void CustomCheckConditions()
    {
        if (controller.enemySpecializer.isAI)
            return;
        //ATTACCHI CARICATI
        
        if (controller.currentMove == moves[5].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                //print("chaaarge");
                moves[5].move.isCharging = true;
            }
            else
                moves[5].move.isCharging = false;


        }
        /*
        else if (controller.currentMove == moves[2].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                //print("chaaarge");
                moves[2].move.isCharging = true;
            }
            else
                moves[2].move.isCharging = false;
        }
        else if (controller.currentMove == moves[11].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                //print("chaaarge");
                moves[11].move.isCharging = true;
            }
            else
                moves[11].move.isCharging = false;
        }
        else if (controller.currentMove == moves[4].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                //print("chaaarge");
                moves[4].move.isCharging = true;
            }
            else
                moves[4].move.isCharging = false;
        }
        else if (controller.currentMove == moves[7].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                //print("chaaarge");
                moves[7].move.isCharging = true;
            }
            else
                moves[7].move.isCharging = false;
        }
        else if (controller.currentMove == moves[10].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                //print("chaaarge");
                moves[10].move.isCharging = true;
            }
            else
                moves[10].move.isCharging = false;
        }
        else if (controller.currentMove == moves[8].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                //print("chaaarge");
                moves[8].move.isCharging = true;
            }
            else
                moves[8].move.isCharging = false;
        }
        else if (controller.currentMove == moves[13].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                //print("chaaarge");
                moves[13].move.isCharging = true;
            }
            else
                moves[13].move.isCharging = false;
        }
        */


        if (controller.hitstop <= 0)
        {
            if (zulul)
                if (CheckUsability(animations[2]))
                {
                    controller.StartNewMove(animations[2].move);
                }

            if (parry)
            {
                parry = false;
                //print ("skidikippappa");
                controller.StartNewMove(moves[7].move);
            }

            if (controller.hitstop <= 0 && controller.special1Buffer > 0)
            {
                
                if (CheckUsability(moves[12]))
                {
                    if (status.currentHealthPercentage > 50)
                        controller.StartNewMove(moves[12].move);
                    else
                        controller.StartNewMove(moves[13].move);

                    controller.special1Buffer = 0;
                }
                

                if (CheckUsability(moves[13]))
                {

                    controller.StartNewMove(moves[13].move);
                    controller.special1Buffer = 0;
                }


            }

            if (controller.parryBuffer > 0 && (CheckUsability(moves[15]) || CheckUsability(animations[2])))
            {
                controller.StartNewMove(moves[15].move);
                controller.parryBuffer = 0;
            }

            if (controller.atk1Buffer > 0)
            {
                /*
                if (((controller.horizontal > 0 && controller.facingRight) || (controller.horizontal < 0 && !controller.facingRight)) &&
                CheckUsability(moves[16]))
                {
                    controller.StartNewMove(moves[16].move);
                    controller.atk1Buffer = 0;

                }
                */

                /*
			if (Input.GetAxisRaw (controller.verticalmovementCMD) < 0) {
				if (CheckUsability(moves[3])) {
					controller.StartNewMove (moves [3].move);
					controller.atk1Buffer = 0;
				}
			}

			if (Input.GetAxisRaw (controller.verticalmovementCMD) > 0) {
				if (CheckUsability(moves[10])) {
					controller.StartNewMove (moves [10].move);
					controller.atk1Buffer = 0;
				}
			}
				
			*/
                if (CheckUsability(moves[3]))
                {
                    controller.StartNewMove(moves[3].move);
                    controller.atk2Buffer = 0;
                }

                //if (status.currentStance == Status.Stances.slow)
                {
                    //if (status.currentHeatAmount < status.heatGaugeThreshold)
                    {
                        if (CheckUsability(moves[4]))
                        {
                            controller.StartNewMove(moves[4].move);
                            //controller.jumpstartup++;
                            controller.atk1Buffer = 0;
                        }
                    }
                    /*
                    else
                    {
                        if (CheckUsability(moves[4]))
                        {
                            controller.StartNewMove(moves[5].move);
                            //controller.jumpstartup++;
                            controller.atk1Buffer = 0;
                        }
                    }
                    */

                    if (CheckUsability(moves[5]))
                    {
                        controller.StartNewMove(moves[5].move);
                        controller.atk1Buffer = 0;
                    }

                    if (CheckUsability(moves[6]))
                    {
                        controller.StartNewMove(moves[6].move);
                        controller.atk1Buffer = 0;
                    }

                    if (CheckUsability(moves[11]))
                    {
                        controller.StartNewMove(moves[11].move);
                        controller.atk1Buffer = 0;
                    }
                }
                /*
                else
                {
                    if (status.currentHeatAmount < status.heatGaugeThreshold)
                    {
                        if (CheckUsability(moves[10]))
                        {
                            controller.StartNewMove(moves[10].move);
                            //controller.jumpstartup++;
                            controller.atk1Buffer = 0;
                        }
                    }
                    else
                    {
                        if (CheckUsability(moves[10]))
                        {
                            controller.StartNewMove(moves[8].move);
                            //controller.jumpstartup++;
                            controller.atk1Buffer = 0;
                        }
                    }
                    if (CheckUsability(moves[13]))
                    {
                        controller.StartNewMove(moves[13].move);
                        controller.atk1Buffer = 0;
                    }

                }
                */
                
                /*
                if (CheckUsability(moves[17]))
                {
                    controller.StartNewMove(moves[17].move);
                    controller.atk1Buffer = 0;
                }
                
                if (CheckUsability(moves[18]))
                {
                    controller.StartNewMove(moves[18].move);
                    controller.atk1Buffer = 0;
                }
                */


            }



            

            if (controller.atk2Buffer > 0)
            {

                if (CheckUsability(moves[11]))
                {
                    controller.StartNewMove(moves[11].move);
                    controller.atk2Buffer = 0;
                }
                //print ("premuto atk2");

                if //(controller.jumpDuration > controller.minimumJumpDurationBeforeAirdash && controller.minimumGroundDistanceBeforeAirAction < controller.currentGroundDistance &&
                   (CheckUsability(moves[0]))
                {
                    controller.StartNewMove(moves[0].move);
                    controller.atk2Buffer = 0;
                }
                if (CheckUsability(moves[2]))
                {
                    controller.StartNewMove(moves[2].move);
                    controller.atk2Buffer = 0;
                }
                

                if (CheckUsability(moves[7]))
                {
                    controller.StartNewMove(moves[7].move);
                    controller.atk2Buffer = 0;
                    
                }

               
                if (CheckUsability(moves[14]))
                {
                    controller.StartNewMove(moves[14].move);
                    controller.atk2Buffer = 0;

                }
                /*
                if (status.currentStance == Status.Stances.fast)

                {
                    if (CheckUsability(animations[2]))
                    {
                        //print("block");
                        controller.StartNewMove(moves[9].move);


                        controller.atk2Buffer = 0;
                    }
                }
                else
                {
                    if (CheckUsability(moves[12]))
                    {
                        //print("block");
                        controller.StartNewMove(moves[12].move);


                        controller.atk2Buffer = 0;
                    }
                }
                */
            }
            
            
        }
    }

}
