﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingAnimationChanger : MonoBehaviour
{
    Controller2D controller;
    public string backTrigger, forwardTrigger, neutralTrigger;
    int backTriggerIndex, forwardTriggerIndex, neutralTriggerIndex;
    float previousHorizontal;
    bool prevFacingRight;

    public void Initialize(Controller2D cont)
    {
        controller = cont;
        backTriggerIndex = Animator.StringToHash(backTrigger);
        forwardTriggerIndex = Animator.StringToHash(forwardTrigger);
        neutralTriggerIndex = Animator.StringToHash(neutralTrigger);

    }

    public void Stop()
    {
        controller.animator.ResetTrigger(backTriggerIndex);
        controller.animator.ResetTrigger(forwardTriggerIndex);
        controller.animator.ResetTrigger(neutralTriggerIndex);
    }

    public void SetWalkingAnimation()
    {
        if (controller == null)
        {
            print("NO COOC");
            return;
        }

        if (controller.horizontal == 0 && (previousHorizontal != 0 || controller.seq == 0 || prevFacingRight != controller.facingRight))
        {
            controller.animator.SetTrigger(neutralTriggerIndex);
        }
        else if (controller.horizontal > 0 && (previousHorizontal <= 0 || controller.seq == 0 || prevFacingRight != controller.facingRight))
        {
            if (prevFacingRight == controller.facingRight)
                controller.animator.SetTrigger(controller.facingRight ? forwardTriggerIndex : backTriggerIndex);
            else
            {
                controller.animator.ResetTrigger(backTriggerIndex);
                controller.animator.ResetTrigger(forwardTriggerIndex);
                controller.animator.ResetTrigger(neutralTriggerIndex);

                controller.animator.Play(controller.facingRight ? forwardTriggerIndex : backTriggerIndex);
            }

        }
        else if (controller.horizontal < 0 && (previousHorizontal >= 0 || controller.seq == 0 || prevFacingRight != controller.facingRight))
        {
            if (prevFacingRight == controller.facingRight)
                controller.animator.SetTrigger(controller.facingRight ? backTriggerIndex : forwardTriggerIndex);
            else
            {
                controller.animator.ResetTrigger(backTriggerIndex);
                controller.animator.ResetTrigger(forwardTriggerIndex);
                controller.animator.ResetTrigger(neutralTriggerIndex);


                controller.animator.Play(controller.facingRight ? backTriggerIndex : forwardTriggerIndex);
            }


        }

        previousHorizontal = controller.horizontal;
        prevFacingRight = controller.facingRight;
    }
}
