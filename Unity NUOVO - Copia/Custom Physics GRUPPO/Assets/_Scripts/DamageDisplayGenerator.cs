﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDisplayGenerator : GeneratorFromPool
{
    

    public void Display(string damage) {
        //print("beh ma alora " + components);
        DamageDisplayBehavior display = (DamageDisplayBehavior) Generate();
        display.Display(damage);
    }
}
