﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Juggle : MoveScript {

	public HurtboxHit hbh;
    public float savedHorizontalVelocity;
    //public FrameData enemyMove;
    private float juggleHorizontalVelocity, juggleVerticalVelocity;
    public int wakeupType = 0;
    protected float positionDifference;
    protected Vector2 enemyPosition;


    //public Wakeup [] standardWakeup;

    public override void MyStopSequence ()
	{
        //controller.status.myPhysicsStatus = Status.physicsStatus.airborne;
        //controller.animator.SetLayerWeight(controller.juggleLayerIndex, 0);
        controller.ResetProjectileOffenders ();
		hbh = null;
        controller.shaking = false;
        wakeupType = 0;
        savedHorizontalVelocity = 0;

		//enemyMove = null;
	}

	public override void SetStatus ()
	{
        
        //controller.animator.SetLayerWeight(controller.juggleLayerIndex, 1);
        controller.status.myPhysicsStatus = Status.physicsStatus.airborne;
        /*
        if (status.currentHealth > 0)
        {
            //print("non muori????" + status.mystatus);
            status.mystatus = Status.status.juggle;
            
        }
        else
        {
            //print("non muori?");
            status.mystatus = Status.status.dead;
        }
        */
        status.mystatus = Status.status.juggle;

    }

    public override void MyStartSequence()
    {
        //setto la gravità normale invece che quella del salto
        //controller.jumpingState = false;
        //print("start juggle: " + controller.targetVelocity.x);
        if (hbh == null)
        {
            hbh = controller.lastCollisionHurtbox.hurtbox;
            FrameData tempFD = controller.lastCollisionHurtbox.move.frameData;

            if (hbh == null || tempFD == null)
            {
                SetDefaultJuggleValues();
                //StopSequence();
                return;
            }
            //enemyMove = controller.lastCollisionHurtbox.move;
            controller.lastCollisionHurtbox.hurtbox = null;
            controller.lastCollisionHurtbox.move.frameData = null;

            if (tempFD.currentHitProperties.hitstop > 7)
                controller.shaking = true;

            wakeupType = tempFD.currentHitProperties.wakeUpType;

            

            juggleVerticalVelocity = hbh.verticalJugglevelocity;
            controller.animator.SetFloat(controller.vspeedIndex, juggleVerticalVelocity);
            float direction;
            enemyPosition = tempFD.isProjectile ? tempFD.projectileScript.transform.position : tempFD.owner.tr.position;

            positionDifference = Mathf.Sign(controller.tr.position.x - enemyPosition.x);

            if (tempFD.isProjectile)
            {
                
                if (tempFD.projectileWithDirection)
                {
                    juggleHorizontalVelocity = tempFD.facingRight ? hbh.horizontalJuggleVelocity : -hbh.horizontalJuggleVelocity;
                }
                else
                {
                    
                    direction = Mathf.Sign(hbh.hurtbox.bounds.center.x - enemyPosition.x);

                    if (direction > 0)
                        juggleHorizontalVelocity = hbh.horizontalJuggleVelocity;
                    else
                        juggleHorizontalVelocity = -hbh.horizontalJuggleVelocity;
                }

                if ((controller.facingRight && juggleHorizontalVelocity > 0) || (!controller.facingRight && juggleHorizontalVelocity < 0))
                {
                    //print("da dietro");
                    controller.Flip();
                    //controller.animator.Play(fromBehindAnimatorParameter, 0, 0);
                }
            }
            else
            {
                direction = tempFD.owner.facingRight ? 1 : -1;
                if (!tempFD.currentHitProperties.positionDependantPushback)
                {
                    if (direction > 0)
                        juggleHorizontalVelocity = hbh.horizontalJuggleVelocity;
                    else
                        juggleHorizontalVelocity = -hbh.horizontalJuggleVelocity;
                }
                else
                {

                    juggleHorizontalVelocity = positionDifference > 0 ? hbh.horizontalJuggleVelocity : -hbh.horizontalJuggleVelocity;

                }



                if ((controller.facingRight && juggleHorizontalVelocity > 0) || (!controller.facingRight && juggleHorizontalVelocity < 0))
                {
                    //print("da dietro");
                    controller.Flip();
                    //controller.animator.Play(fromBehindAnimatorParameter, 0, 0);
                }
            }

            for (int i = 0; i < relatedMoves.Length; i++)
            {

                    if (relatedMoves[i].horizontalMovements.Length > 0)
                    {
                        relatedMoves[i].horizontalMovements[0].velocity = controller.facingRight ? juggleHorizontalVelocity * 0.5f : -juggleHorizontalVelocity * 0.5f; ;
                    }

            }

            controller.velocity.y = juggleVerticalVelocity;

            if (controller.hitsparkRetriever != null)
            {
                direction = controller.facingRight ? -1 : 1;
                if (status.myPreviousStatus == Status.status.blocking || status.previousIsBlocking || status.myPreviousStatus == Status.status.blockstun1 ||
                    status.myPreviousStatus == Status.status.guardBreak)
                    controller.hitsparkRetriever.GenerateSparksOnBlock(3, hbh.transform, 1.4f *  direction, 0.5f);
                else
                    controller.hitsparkRetriever.GenerateSparksOnHit(3, hbh.transform, 1.4f * direction, 0.5f);
            }
            else
            {
                print("no hitspark retriever");
            }
        }
    }

    private void SetDefaultJuggleValues ()
    {
        //print("default juggle: " + savedHorizontalVelocity);
        wakeupType = 0;
        juggleHorizontalVelocity = savedHorizontalVelocity;
        juggleVerticalVelocity = 0;

        for (int i = 0; i < relatedMoves.Length; i++)
        {

            if (relatedMoves[i].horizontalMovements.Length > 0)
            {
                relatedMoves[i].horizontalMovements[0].velocity = controller.facingRight ? savedHorizontalVelocity * 0.5f : -savedHorizontalVelocity * 0.5f;
            }

        }

    }

    public override void MyExecuteSequence ()
	{
		if (controller.seq == 0) {
            //hbh = controller.hurtboxesHitList [0];

            //print ("jugglescript");

            /*
			if (!hitstopAlreadyHappened)
				controller.hitstop = hbh.offenderMoveSet.hitstop;
			hitstopAlreadyHappened = true;
			*/

            //controller.velocity.y = juggleVerticalVelocity;
            controller.status.myPhysicsStatus = Status.physicsStatus.airborne;
            //print ("velocità verticale: " + controller.velocity.y);
            //print ("primo frame di juggle");
            //print ("JUMP");
        }

        if (!controller.enemySpecializer.isAI && controller.status.currentHealth <= 0 && controller.seq <= 6)
        {
            controller.timeManager.AlterTimeRequest(8);
            controller.currentTimeScale = 8;
        }
        //if (controller.seq == 1)
        //controller.velocity.y = hbh.verticalJugglevelocity;

        //controller.targetVelocity.x = hbh.direction * hbh.horizontalJuggleVelocity;
        controller.targetVelocity.x = juggleHorizontalVelocity;


        /*
        if (status.currentHealth > 0)
        {
            //print("percheè non muori " + status.mystatus);
            status.mystatus = Status.status.juggle;
        }
        else
        {
            //print("perchè non muori " + status.mystatus);
            status.mystatus = Status.status.dead;

        }
        */
        status.mystatus = Status.status.juggle;

        //if (controller.hitstop == 0 && !resumingFromHitstop)
        controller.seq++;
	} 
}
//ciò ho un problema
//se farei che il blockstun ha come followup una mossa dell'arma (il counter), cosa succedesse se cambierei arma????
//io nel blockstun introdusco l'INDICE del followup all'interno del vettore delle mosse dell'arma, ma se unaltra arma avrebbe il counter in unaltra posizzione?
//o che metto che il counter è sempre nella stessa posizione in tutte le armi
//oppure metto una variabile nella classe dell'arma che contiene l'indice del counter, e lo vado a cambiare manualmente in tutte le armi... 
//oppure???
//in ogni caso mi devo ricordare di fare delle cose ogni volta che faccio una nuova arma
//oppure potrei fare che ogni arma ha sempre le stesse mosse, al più cambia lo script, ma ognuna ha un attacco1, un attacco2, counter1, counter2...