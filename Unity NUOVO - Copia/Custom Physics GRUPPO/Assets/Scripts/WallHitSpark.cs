﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallHitSpark : MonoBehaviour
{
    // Start is called before the first frame update
    public MoveSet moveset;
    public bool attackIsActive;
    public LayerMask layerMask;
    public bool hasEmitted;
    GlobalHitSparkGenerator hitSparkGenerator;
    public Collider2D col;

    private void Start()
    {
        layerMask &= 0;
        layerMask |= 1 << LayerMask.NameToLayer("Default");
        layerMask |= 1 << LayerMask.NameToLayer("Platform");
        hitSparkGenerator = GlobalHitSparkGenerator.Instance == null ? FindObjectOfType<GlobalHitSparkGenerator>() : GlobalHitSparkGenerator.Instance;


    }

    void FixedUpdate()
    {
        //Debug.DrawRay(moveset.hitsparkOrigin.position, Vector2.up * 5, Color.green);
        if (moveset.hitsparkOrigin == null)
            return;
        if (attackIsActive && !hasEmitted)
        //if (attackIsActive)
        {
            col = Physics2D.OverlapCircle(moveset.hitsparkOrigin.position, 0.1f, layerMask);
            if (col != null)
            {                    
                hitSparkGenerator.GenerateSparks(GlobalHitSparkGenerator.SparkType.sparks, 3, moveset.hitsparkOrigin, 1.4f * (moveset.controller.facingRight?1:-1), 0.5f);
                hasEmitted = true;
                
                //attackIsActive = false;
            }
        }
    }

    public void WallHitSparkStartChecking() {
        attackIsActive = true;
        hasEmitted = false;
    }
    public void WallHitSparkStopChecking() {
        attackIsActive = false;
        hasEmitted = false;
    }
}
