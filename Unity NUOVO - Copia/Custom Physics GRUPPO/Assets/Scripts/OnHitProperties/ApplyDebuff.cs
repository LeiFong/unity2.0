﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyDebuff : OnHitProperties {

	public BuffScriptableObject debuff;
    public bool onSelf;
    public enum BuffConditions {onHit, onActivation};
    public BuffConditions buffConditions;

    public int activationFrame = 0; //se il buff è aggiunto su attivazione (e non onHit), specifica il frame dell'attivazione

    public void OnActivationApply ()
    {
        //Buff newBuff = move.controller.pm.buffDatabase.buffs[debuff.index];
        //newBuff.owner = move.controller;

        //if (onSelf)
        {
            //print("aggiunto buff 1");
            move.controller.buffManager.AddBuff(move.controller.pm.buffDatabase.buffs[debuff.index],
                move.controller.pm.buffDatabase.buffs[debuff.index].buffScriptableObject.buffTotalDuration, 1);
        }
    }

	public override void OnHitApply ()
	{


        //debuff.GetComponent<Buff>().owner = move.controller;

        Buff newBuff = move.controller.pm.buffDatabase.buffs[debuff.index];
        //newBuff.owner = move.controller;

        if (onSelf)
        {
            //print("aggiunto buff 2    " + debuff.buffTotalDuration);

            move.controller.buffManager.AddBuff(newBuff, newBuff.buffScriptableObject.buffTotalDuration, 1);

        }
        else
        {
            if (move.move != null && move.move.objectsHit.Count > 0)
            {
                for (int i = 0; i < move.move.objectsHit.Count; i++)
                {
                    if (move.move.objectsHit[i] != null)
                    {
                        //print("aggiunto buff 3");

                        move.move.objectsHit[i].GetComponent<Controller2D>().buffManager.AddBuff(newBuff, newBuff.buffScriptableObject.buffTotalDuration, 1);
                    }
                }
            }
        }

	}
}
