﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Landing : MoveScript {

	public int landLagFrames = 60;



	public override void SetStatus()
	{
		status.mystatus = Status.status.landing;
	}
	public override void MyExecuteSequence ()
	{
		//if (controller.seq < landLagFrames -1) {
			if (controller.seq == 0) {
				//controller.animator.SetBool ("neutral", false);
				//controller.ChangeParameter (animatorParameterName, true);
				//controller.ChangeParameter (animatorParameterName);


				//controller.animator.Play(animatorParameterName, -1, 0f);
				//print ("primo frame di landing");

			}
			status.mystatus = Status.status.landing;
			//status.mystatus = Status.status.stuckinanimation;
			controller.seq++;

		//} else
			//StopSequence ();
			//moveIsFinished = true;
            if (controller.seq >= landLagFrames)
			    itsTimeToStopTheSequence = true;
		
	}

	public override void MyStopSequence () {
		controller.seq = 0;
		//Debug.Log ("STOP!");
		status.mystatus = Status.status.neutral;
		//controller.animator.SetBool (animatorParameterName, false);
		//controller.animator.SetBool ("neutral", true);
		//controller.animator.speed = 1;
		//controller.currentMove = null;
	}

}
