﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBox : MonoBehaviour {

	protected Camera cam;
	protected BoxCollider2D col;
	protected float lowerBound, upperBound;

	// Use this for initialization
	void OnEnable () {
		cam = GetComponent<Camera> ();
		col = GetComponent<BoxCollider2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		lowerBound = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0)).x - Camera.main.ViewportToWorldPoint(new Vector3(0,0,0)).x;

		upperBound = Camera.main.ViewportToWorldPoint(new Vector3(0,1,0)).y - Camera.main.ViewportToWorldPoint(new Vector3(0,0,0)).y;

		col.size = new Vector2 (lowerBound, upperBound);


	}
}
