﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NPCData
{
    public string myID;
    public int currentHealth;
    public float[] position;
    public bool facingRight;
    public bool hasDroppedLoot;
    public bool isPlayer;

    [System.Serializable]
    public struct BuffProperties
    {
        public int buffID;
        public int durationLeft;
        public int currentStacks;
    }

    public BuffProperties[] buffs;


    public NPCData (string ID, Status status)
    {
        currentHealth = status.currentHealth;
        myID = ID;
        if (status.controller.enemySpecializer != null)
        {
            hasDroppedLoot = status.controller.enemySpecializer.hasDroppedLoot;

        }
        else
            hasDroppedLoot = true;

        position = new float[3];
        position[0] = status.controller.lastSerializablePosition.x;
        position[1] = status.controller.lastSerializablePosition.y;
        position[2] = status.controller.lastSerializablePosition.z;
        //Debug.Log("savedNPC position: " + position[0] + " " + position[1] + " " + position[2]);
        facingRight = status.controller.lastSerializableFacing;

        buffs = new BuffProperties[status.controller.buffManager.buffs.Count];

        for (int k = 0; k < status.controller.buffManager.buffs.Count; k++)
        {
            buffs[k].buffID =
                status.controller.
                buffManager.
                buffs[k].buff.
                buffScriptableObject.index;

            //Debug.Log("savebuffs?");

            buffs[k].durationLeft =
                status.controller.
                buffManager.
                buffs[k].durationTimer;

            buffs[k].currentStacks =
                    status.controller.
                    buffManager.
                    buffs[k].currentStacks;

        }
    }
   
   
}
