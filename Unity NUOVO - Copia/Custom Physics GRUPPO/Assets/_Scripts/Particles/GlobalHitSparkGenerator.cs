﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalHitSparkGenerator : MonoBehaviour
{
    public enum SparkType { redBlood, sparks, shatteringPieces }

    [System.Serializable]
    public struct HitSparkType {
        public SparkType type;
        public ParticleSystem particleSystem;
    }

    public struct HitSparkData {
        public ParticleSystem particleSystem;
        public List<ParticleSystem> subEmitters;
        public Transform psTransform;
        public SparkType type;
        public ParticleSystem.MinMaxCurve defaultSpeedCurveX, defaultSpeedCurveY;
        public List<ParticleSystem.MinMaxCurve> subEmittersDefaultSpeedCurvesX, subEmittersDefaultSpeedCurvesY;
    }

    public List<HitSparkType> hitsparks;
    private List<HitSparkData> hsData;
    
    private HitSparkData currentHitSparkData;
    
    public static GlobalHitSparkGenerator Instance { get; private set; }
    
    
    //anti-garbage vars
    private ParticleSystem.MinMaxCurve curve;
    private bool found;
    private ParticleSystem.VelocityOverLifetimeModule velocity, subV;

    private void Awake()
    {
        Instance = this;

        hsData = new List<HitSparkData>();
        foreach (HitSparkType hs in hitsparks) {

            HitSparkData data = new HitSparkData();
            data.particleSystem = hs.particleSystem;
            data.psTransform = hs.particleSystem.transform;
            data.type = hs.type;
            var velocity = data.particleSystem.velocityOverLifetime;
            data.defaultSpeedCurveX = velocity.x;
            data.defaultSpeedCurveY = velocity.y;
            data.subEmitters = new List<ParticleSystem>(hs.particleSystem.GetComponentsInChildren<ParticleSystem>());
            data.subEmittersDefaultSpeedCurvesX = new List<ParticleSystem.MinMaxCurve>();
            data.subEmittersDefaultSpeedCurvesY = new List<ParticleSystem.MinMaxCurve>();
            for (int i = 0; i < data.subEmitters.Count; i++)
            {
                var subV = data.subEmitters[i].velocityOverLifetime;
                data.subEmittersDefaultSpeedCurvesX.Add(subV.x);
                data.subEmittersDefaultSpeedCurvesY.Add(subV.y);
            }
            hsData.Add(data);
        }

        curve = new ParticleSystem.MinMaxCurve(1, 1);

    }
    
    public void GenerateSparks(SparkType type, int quantity, Transform position, float xSpeedMultiplier, float ySpeedMultiplier)
    {

        //codice orribile ma usare un dizionario con un enum come chiave crea garbage
        found = false;
        foreach (HitSparkData data in hsData)
        {
            if (data.type == type) {
                currentHitSparkData = data;
                found = true;
                break;
            }
        }
        if (!found)
        {
            Debug.LogError("Il particleSystem di tipo " + type.ToString() + " non è stato assegnato!");
            return;
        }

        //dove emetto
        currentHitSparkData.psTransform.position = position.position;

        //speed multiplier
        velocity = currentHitSparkData.particleSystem.velocityOverLifetime;
        curve.constantMin = currentHitSparkData.defaultSpeedCurveX.constantMin * xSpeedMultiplier;
        curve.constantMax = currentHitSparkData.defaultSpeedCurveX.constantMax * xSpeedMultiplier;
        velocity.x = curve;
        
        curve.constantMin = currentHitSparkData.defaultSpeedCurveY.constantMin * ySpeedMultiplier;
        curve.constantMax = currentHitSparkData.defaultSpeedCurveY.constantMax * ySpeedMultiplier;
        velocity.y = curve;

        //setto la velocità anche ai subemitters
        for (int i = 0; i < currentHitSparkData.subEmitters.Count; i++)
        {
            subV = currentHitSparkData.subEmitters[i].velocityOverLifetime;
            curve.constantMin = currentHitSparkData.subEmittersDefaultSpeedCurvesX[i].constantMin * xSpeedMultiplier;
            curve.constantMax = currentHitSparkData.subEmittersDefaultSpeedCurvesX[i].constantMax * xSpeedMultiplier;
            subV.x = curve;
            curve.constantMin = currentHitSparkData.subEmittersDefaultSpeedCurvesY[i].constantMin * ySpeedMultiplier;
            curve.constantMax = currentHitSparkData.subEmittersDefaultSpeedCurvesY[i].constantMax * ySpeedMultiplier;
            subV.y = curve;
        }

        currentHitSparkData.particleSystem.Emit(quantity);
        
    }
}
