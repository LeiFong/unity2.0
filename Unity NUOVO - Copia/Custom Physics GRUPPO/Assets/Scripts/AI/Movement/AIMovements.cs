﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.Mathematics;
using UnityEngine;

public class AIMovements : MonoBehaviour
{
    [SerializeField]
    Controller2D controller;
    [HideInInspector]
    public float distanceFromLeftEdge, distanceFromRightEdge;
    public BoxCollider2D mySpawnPosition, myCompetenceZone, currentCompetenceZone;
    //public bool hasCompetenceZone;
    Vector2 myPosition;
    public bool isInsideCompetenceZone, isInsideSpawnZone, returningToSpawnZone;
    private int competenceZoneLsayerIndex;
    //public int initialFacing = 1;

    public void CalculatePositionRelativeToCompetenceZone()
    {
        if (currentCompetenceZone != null)
        {
            float angle = (currentCompetenceZone.transform.rotation.eulerAngles.z) * Mathf.Deg2Rad;
            //print("angle: " + Mathf.Sin (12.86f));

            Vector2 leftEdge, rightEdge, lowerCenter;

            lowerCenter.x = currentCompetenceZone.bounds.center.x + currentCompetenceZone.size.y/2 * Mathf.Sin(angle);
            lowerCenter.y = currentCompetenceZone.bounds.center.y - currentCompetenceZone.size.y/2 * Mathf.Cos(angle);


            leftEdge.x = lowerCenter.x - currentCompetenceZone.size.x/2 * Mathf.Cos(angle);
            leftEdge.y = lowerCenter.y - currentCompetenceZone.size.x / 2 * Mathf.Sin(angle);

            /*
            Debug.DrawRay(leftEdge, 100000*Vector2.down, Color.red);
            Debug.DrawRay(leftEdge, 100000 * Vector2.up, Color.red);
            Debug.DrawRay(leftEdge, 100000 * Vector2.right, Color.red);
            Debug.DrawRay(leftEdge, 100000 * Vector2.left, Color.red);

            print("leftedge: " + leftEdge);
            */
            rightEdge.x = lowerCenter.x + currentCompetenceZone.size.x / 2 * Mathf.Cos(angle);
            rightEdge.y = lowerCenter.y + currentCompetenceZone.size.x / 2 * Mathf.Sin(angle);

            Debug.DrawRay(rightEdge, 1000*Vector2.down, Color.red);

            

            distanceFromLeftEdge = Mathf.Abs(controller.tr.position.x - leftEdge.x);
            distanceFromRightEdge = Mathf.Abs(controller.tr.position.x - rightEdge.x);
            
            /*
            if (controller.horizontal < 0 && distanceFromLeftEdge < 1)
                controller.horizontal = 0;
            else if (controller.horizontal > 0 && distanceFromRightEdge < 1)
                controller.horizontal = 0;
            */
        }
        else
        {
            distanceFromLeftEdge = 500;
            distanceFromRightEdge = 500;
        }
    }
    
    private void Start()
    {
        /*
        if (mySpawnPosition == null || myCompetenceZone == null)
        {
            hasCompetenceZone = false;
        }
        else
            hasCompetenceZone = true;
            */

        competenceZoneLsayerIndex = LayerMask.NameToLayer("CompetenceZone");

        if (mySpawnPosition == null && myCompetenceZone != null)
        {
            mySpawnPosition = myCompetenceZone;
        }
        //controller = GetComponentInParent<Controller2D>();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //print("fuoriiii");
        //if (currentCompetenceZone == collision)
        if (collision.gameObject.layer != competenceZoneLsayerIndex)
            return;
    

        if (collision == myCompetenceZone)
        {
            isInsideCompetenceZone = false;
            currentCompetenceZone = null;
            //returningToSpawnZone = true;
        }
        
        if (collision == mySpawnPosition)
            isInsideSpawnZone = false;
        else
            currentCompetenceZone = null;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer != competenceZoneLsayerIndex)
            return;
        if (currentCompetenceZone == null)// && collision != mySpawnPosition)
            currentCompetenceZone = (BoxCollider2D) collision;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //print("dentroooo");
        if (collision.gameObject.layer != competenceZoneLsayerIndex)
            return;

        if (collision == myCompetenceZone)
        {
            isInsideCompetenceZone = true;

        }

        

        //if (collision == mySpawnPosition)
        {
            //isInsideSpawnZone = true;
            //returningToSpawnZone = false;
        }
        //else
            currentCompetenceZone = (BoxCollider2D) collision;
    }

    public void MoveToReachSpawnZone (Vector2 destination, float walkSpeed)
    {
        myPosition = controller.gameObject.transform.position;
        //print("movetoreachspawnxone");
        //se è sullo stesso nodo?
        if (destination.x > myPosition.x)
        {
            controller.horizontal = walkSpeed;

        }
        else
        {
            controller.horizontal = -walkSpeed;
        }
        if (Vector2.Distance(myPosition, destination) < 1)
        {
            returningToSpawnZone = false;
            controller.horizontal = 0;
            if (controller.status.mystatus == Status.status.neutral && controller.currentMove == null &&
                controller.facingRight != (controller.initialFacingRight))
            {
                controller.Flip();
            }
            isInsideSpawnZone = true;
            //print("arivato");
        }
        else
        {
            returningToSpawnZone = true;
            //isInsideSpawnZone = false;
        }
    }

    
}
