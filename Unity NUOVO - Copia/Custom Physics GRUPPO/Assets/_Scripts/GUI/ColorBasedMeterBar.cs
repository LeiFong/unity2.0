﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorBasedMeterBar : MonoBehaviour
{
    public Image image;
    [Range(0f, 1f)]
    public float value;
    public Color minColor, maxColor;
    private Color c;
    public float timer;
    private float direction = 1;
    public float glowSpeed = 10f;
    public Animator animator;
    private int glowAnimationInt, glowBigAnimationInt;

    private void Awake()
    {
        if (animator == null)
            animator = GetComponent<Animator>();
        glowAnimationInt = Animator.StringToHash("Glow");
        glowBigAnimationInt = Animator.StringToHash("Glow big");

    }

    public void UpdateColor() {
        c = Color.Lerp(minColor, maxColor, value);
        image.color = c;
    }

    public void Glow() {
        if (value < 0.5f)
        {
            timer += glowSpeed / 50f;
            if (timer > glowSpeed)
                timer = glowSpeed;
        }
        else
        {
            timer += value * direction;
            if (timer > glowSpeed)
            {
                timer = glowSpeed;
                direction = -1;
                
            }
            else if (timer < 0)
            {
                timer = 0;
                direction = 1;

                if (value == 1)
                    animator.Play(glowBigAnimationInt);
            }
        }
        c = image.color;
        if (value == 1)
            c.a = 0.3f + 0.7f * timer / glowSpeed;
        else
            c.a = 0.5f + 0.5f * timer / glowSpeed;

        image.color = c;
    }

    private void FixedUpdate()
    {
        UpdateColor();

        Glow();
    }

}
