﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPhysics : MonoBehaviour {

	public int gravityModifier = 1;
	public float shellRadius = 0.01f;
	public float minGroundNormalY = 0.3f;
	public float minGroundNormalSlipperyY = 0.65f;
	public Vector2 slipVelocity = new Vector2 (5, 0);
	public Status status;
	public Collider2D mycollider;

	protected Rigidbody2D RB;
	public Vector2 targetVelocity;
	protected ContactFilter2D contactFilter;
	public Vector2 velocity;
	protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
	protected List<RaycastHit2D> hitbufferList = new List<RaycastHit2D> (16);

	protected RaycastHit2D[] groundHitBuffer = new RaycastHit2D[16];
	protected List<RaycastHit2D> groundHitbufferList = new List<RaycastHit2D> (16);
	public LayerMask whatIsGround;
	protected ContactFilter2D groundContactFilter;
	public float groundRadius = 1;

	protected Vector2 groundNormal;
	protected float minMoveDistance = 0.001f;
	protected Vector2 moveAlongGround;
	public Vector2 environmentVelocity;
	protected bool grounded;
	protected string myLayer;



	public AvoidCharacters avoid;

	public bool avoidsCharacters = false;



	//public Collider2D characterAvoider;
	//public bool attack;



	void OnEnable () {
		RB = GetComponent<Rigidbody2D>();
		//attack = false;
		//Application.targetFrameRate = 10;

		mycollider = GetComponent<Collider2D> ();
		status = GetComponent<Status>();
		contactFilter.useTriggers = false;
		contactFilter.SetLayerMask (Physics2D.GetLayerCollisionMask (gameObject.layer));
		contactFilter.useLayerMask = true;
		myLayer = LayerMask.LayerToName (gameObject.layer);
		groundContactFilter.SetLayerMask (whatIsGround);
		MyOnEnable ();

	}


	// Update is called once per frame
	public void Update () {
		//Time.timeScale = 0.3f;
		targetVelocity.x = 0;
		//StatusUpdate ();
		ComputeVelocity ();
		//Attack ();
		//Debug.Log(Time.frameCount);


	}



	protected virtual void ComputeVelocity()
	{
		
	}

	void FixedUpdate () {


		//Attack ();
		//Time.timeScale = 0.3f;

		velocity += gravityModifier * Physics2D.gravity * Time.deltaTime;
		velocity.x = targetVelocity.x + environmentVelocity.x;

		Vector2 deltaPosition = velocity * Time.deltaTime;


		moveAlongGround = new Vector2 (groundNormal.y, -groundNormal.x);

		Vector2 move = moveAlongGround * deltaPosition.x;

		move = CheckCollision (move, false);

		int count = RB.Cast (Vector2.down, groundContactFilter, groundHitBuffer, groundRadius);
		/*se ymove
				cast in giu di una perdonanza
				se trova qualcosa
					provo a muovermi in giu di quella

				*/
		if (count != 0) {
			float newdistance = groundRadius;
			grounded = true;
			for (int i = 0; i < count; i++) {
				newdistance = groundHitBuffer [i].distance < newdistance ? groundHitBuffer [i].distance : newdistance;


				//RB.position += new Vector2 (0, groundHitBuffer [i].distance);
			}

			move = new Vector2 (0, -newdistance);
			move = CheckCollision (move, true);
		} else
			grounded = false;
		
			


		move = new Vector2 (0, deltaPosition.y);

		move = CheckCollision (move, true);





	}

	//funzione per evitare che un personaggio salti su un altro personaggio


		//Debug.Log (move);
	Vector2 CheckCollision (Vector2 newmove, bool ymove) {

		//Debug.Log(Time.frameCount);
		//Debug.Log (ymove);

		environmentVelocity = Vector2.zero;




		float distance = newmove.magnitude;

		if (!ymove || (ymove && distance > minMoveDistance)) 
		{

			int count = RB.Cast (newmove, contactFilter, hitBuffer, distance + shellRadius);
			hitbufferList.Clear ();



			for (int i = 0; i < count; i++) {

				bool otherAvoidsCharacters;
				MyPhysics otherPhysics = hitBuffer [i].collider.gameObject.GetComponent<MyPhysics> ();

				if (otherPhysics != null)
					otherAvoidsCharacters = otherPhysics.avoidsCharacters;
				else
					otherAvoidsCharacters = false;
				string otherLayer = LayerMask.LayerToName(hitBuffer [i].collider.gameObject.layer);

				{
					if (ymove) {




							
						if (myLayer != "Character" && otherLayer != "Character")
							hitbufferList.Add (hitBuffer [i]);
						else if (!(myLayer == "Character" && otherLayer == "Character" ) ) {
							if (!avoidsCharacters && !otherAvoidsCharacters)
								hitbufferList.Add (hitBuffer [i]);
							else if (myLayer == "Character" && !otherAvoidsCharacters)
								hitbufferList.Add (hitBuffer [i]);
							
						}

					}

					else
					{
						if (myLayer == "Character") {
							if (otherLayer == "Character") {
								if (!avoidsCharacters && !otherAvoidsCharacters)
									hitbufferList.Add (hitBuffer [i]);
							} else {
								if (!otherAvoidsCharacters)
									hitbufferList.Add (hitBuffer [i]);
							}
						} else {
							if (otherLayer != "Character")
								hitbufferList.Add (hitBuffer [i]);
							else {
								if (!avoidsCharacters)
									hitbufferList.Add (hitBuffer [i]);
							}
						}
							
						
							
					}
				}


			}

			for (int i = 0; i < hitbufferList.Count; i++) {
				
					
				Vector2 currentNormal = hitbufferList[i].normal;


				//se cade su un oggetto con layer "Character", allora si sposta al di fuori di esso
				/*
				if (ymove && hitbufferList [i].collider.gameObject.layer == LayerMask.NameToLayer("Character"))
					environmentVelocity.x = Mathf.Sign((mycollider.bounds.center.x - hitBuffer[i].collider.bounds.center.x)) * 4;
					//RB.position += new Vector2(Mathf.Sign((mycollider.bounds.center.x - hitBuffer[i].collider.bounds.center.x)) * 0.1f, 0);
				*/


				if (ymove) {

				}

				if (currentNormal.y > minGroundNormalY) {
										
					if (ymove) {


						groundNormal = currentNormal;
						currentNormal.x = 0;
						if(myLayer=="Character")
							Debug.Log ("ZULUL");	
					}

				} 
					
					
				if (currentNormal.y > minGroundNormalY && currentNormal.y < minGroundNormalSlipperyY && ymove) {
					environmentVelocity = groundNormal.x > 0 ? slipVelocity : -slipVelocity;
				}

				//if (currentNormal.y > minGroundNormalSlipperyY && ymove)

				
				float projection = Vector2.Dot (velocity, currentNormal);

				if (projection < 0) {
					velocity = velocity - projection * currentNormal ;
				}


				float modifiedDistance = hitbufferList [i].distance - shellRadius;

				if (ymove && hitbufferList [i].distance <= 0) {
					
					modifiedDistance = 0;
					//serve per evitare che il personaggio salga in alto quando e' incastrato

				}

				if (myLayer == "Character" && !ymove && hitbufferList [i].distance <= 0) {


					RB.position += new Vector2(Mathf.Sign((mycollider.bounds.center.x - hitbufferList[i].collider.bounds.center.x)) * 0.05f, 0);
					//serve a disincastrare il personaggio nel caso in cui rimanga in mezzo a un collider
				}
			
				distance = modifiedDistance < distance ? modifiedDistance : distance;

			}
		
		}

		RB.position += newmove.normalized * distance;

		return newmove;

		}



	protected virtual void MyOnEnable() {

	}

	}

