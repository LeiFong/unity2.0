﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemWrapper : MonoBehaviour
{
    //classe che contiene vari dati essenziali che identificano un item

    public Item item;

    [HideInInspector]
    public Inventory inventory;
    [HideInInspector]
    public Controller2D controller;

    public MultiLanguageText itemName, description, flavorText;



    public void InitializeItem(Inventory inventory)
    {
        this.inventory = inventory;
        controller = inventory.controller;
    }

    protected virtual void MyInitializeItem()
    {

    }
}
