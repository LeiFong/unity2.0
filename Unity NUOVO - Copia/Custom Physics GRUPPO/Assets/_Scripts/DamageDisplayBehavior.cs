﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DamageDisplayBehavior : PoolObject
{
    public float originOffset;    
    private RectTransform myRect;
    private Vector2 startPosition;
    private float targetY;
    private bool isDisplaying = false;
    [Range(0f, 10f)]
    public float initialSpeed = 5f;
    [Range(0f, 10f)]
    public float finalSpeed = 2f;
    private float currentSpeed = 0;
    public int displayTime = 60;
    [Range(0.01f, 1f)]
    public float stopPortion = 0.5f;
    private TextMeshProUGUI text;
    private int timer = 0;
    private int stopTime;
    private Vector2 newPos;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
        text.text = "";
        myRect = GetComponent<RectTransform>();
        startPosition = new Vector2(myRect.anchoredPosition.x, myRect.anchoredPosition.y + originOffset);
        targetY = startPosition.y;

        newPos = new Vector2();

    }

    public void Display(string damage) {
        
        text.text = damage; 
        timer = displayTime;
        currentSpeed = initialSpeed;
        targetY = startPosition.y;
        myRect.anchoredPosition = startPosition;
    }

    /*
    public void GoUp(int iteration, float distance) {
        if (iteration == 0)
            return;
        targetY = targetY + distance;
        previousDisplay.GoUp(iteration - 1, floorHeight);

    }
    

    public void GoDown(int iteration, float distance)
    {
        if (iteration == 0)
            return;
        targetY = targetY - distance;
        //myRect.anchoredPosition = new Vector2(myRect.anchoredPosition.x, targetY);
        previousDisplay.GoDown(iteration - 1, floorHeight);

    }

    */
    private void FixedUpdate()
    {
        if (timer > 0) {
            timer--;
            stopTime = (int)((float)displayTime * (1 - stopPortion));
            if (timer > (stopTime))
            {
                newPos.x = myRect.anchoredPosition.x;
                newPos.y = myRect.anchoredPosition.y + currentSpeed / 100;

                myRect.anchoredPosition = newPos;
                currentSpeed -= (float)((initialSpeed - finalSpeed) / (float)(displayTime - stopTime));
            }
            
            /*
             * 
             * 0    50
             * 100  25
             * 
             * cs-=(speed-finalspeed)/dtime
             * 1 60
             * 0
             * 0.6 100
             * 100>40 sì, 40>0 no
             * 
             * 
             * */
            if (timer == 0) {
                text.text = "";
                newPos.x = myRect.anchoredPosition.x;
                newPos.y = startPosition.y;
                myRect.anchoredPosition = newPos;
            }
        }
        
    }

    public bool IsDisplaying() {
        return isDisplaying;
    }
    public float GetTargetY() {
        return targetY;
    }
}
