﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleObjectMover : MonoBehaviour
{
    public Vector2 velocity;

    private void Start()
    {
        velocity *= 0.016666f;
    }
    private void FixedUpdate()
    {
        transform.Translate(velocity);
    }
}
