﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostHitboxDetector : MonoBehaviour
{
    public BoxCollider2D ghostHurtbox;
    public BehaviourSelector bSelector;
    //public bool insideEnemyGhostHitbox = false;

    public bool CheckGhostHitbox (Collider2D testedCollider)
    {
       
        if (ghostHurtbox != null && bSelector != null)
        {
            
            //Physics2D.OverlapCollider()
            if (testedCollider.GetComponentInParent<MoveScript>().controller.status.faction != bSelector.controller.status.faction)
            {
                
                return true;
            }
        }
        return false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //print("sono dentro");
        //insideEnemyGhostHitbox = CheckGhostHitbox(collision);
        

    }
    private void OnTriggerStay2D(Collider2D collision)
    {

        //insideEnemyGhostHitbox = CheckGhostHitbox(collision);
    }
}
