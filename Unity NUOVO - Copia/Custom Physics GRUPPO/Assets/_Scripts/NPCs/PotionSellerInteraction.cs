﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionSellerInteraction : NpcInteraction
{
    
    [Space(20)]
    public bool neverMet = true;

    public Dialogue firstMeeting, defaultDialogue, defaultChatDialogue;
    

    protected override void Start()
    {
        base.Start();
        dialogueManager = DialogueManager.Instance ?? FindObjectOfType<DialogueManager>();
    }

    public override void StartInteraction()
    {
        base.StartInteraction();
        animator.Play("enemy first idle break");
        if (neverMet)
        {
            
            dialogueManager.dialoguePanelMenuManager.OpenMenu();
            firstMeeting.StartDialogue();
        }
        else {
            dialogueManager.dialoguePanelMenuManager.OpenMenu();
            defaultDialogue.StartDialogue();
        }

    }
    public override void EndInteraction()
    {
        base.EndInteraction();
        animator.Play("enemy first idle");
    }
    public void Chat() {
        //do some tests and logics
        
        defaultChatDialogue.StartDialogue();
    }
    

    public void SetNeverMet(bool hasNeverMet) {
        neverMet = hasNeverMet;
    }
}
