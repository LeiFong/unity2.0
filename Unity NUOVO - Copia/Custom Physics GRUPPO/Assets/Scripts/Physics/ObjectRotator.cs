﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotator : MonoBehaviour
{
    public float rotationVelocity;
   

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate (new Vector3 (0, 0, rotationVelocity * Time.fixedDeltaTime), Space.Self);
    }
}
