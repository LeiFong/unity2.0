﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunBehaviour : BehaviorOption
{

    public override void MyExecuteBehaviour()
    {
        controller.horizontal = controller.facingRight ? 1 : -1;
        if ((bSelector.targetPosition.x - controller.transform.position.x > 0 && !controller.facingRight) ||
            ((bSelector.targetPosition.x - controller.transform.position.x <= 0 && controller.facingRight)))
        {
            //print("scambiato, stop behaviour");
            StopBehaviour();
            return;
        }
        controller.holdingDash = true;
        controller.runToggle = false;
        
        if (moves.Length == 0 || executedMovesIndex < 0)
            controller.runToggle = true;
        
        //print("run: " + bSelector.bseq);

    }

    public override void MyStopBehaviour()
    {
        //print("runstop");
        controller.runToggle = false;
        controller.holdingDash = false;
    }
}
