﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode] 
public class GeneratorFromPool : MonoBehaviour
{
    
    protected int activeIndex;
    [SerializeField]
    protected List<PoolObject> components;
    [SerializeField]
    private int dim;
    
    
    void OnEnable()
    {
        if (Application.isPlaying)
            return;

        components = new List<PoolObject>(GetComponentsInChildren<PoolObject>(true));

        dim = components.Count;
        activeIndex = 0;

        PrefabInstanceSaver.MarkDirtyObject(this.gameObject);
        

    }

    public PoolObject Generate() {
        if (components.Count == 0)
            return null;
        PoolObject generated = components[activeIndex];
        activeIndex = (activeIndex + 1) % dim;
        return generated;
    }
}
