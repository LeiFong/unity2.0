﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Holder : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col) {

		col.transform.parent = transform;

	}

	void OnTriggerExit2D(Collider2D col) {
		col.transform.parent = null;
	}


}
