﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtboxHit : MoveScript {

	//public List<GameObject> offendersIDs;


	int hitboxLayer;

	[System.Serializable]
	public struct HitInfo
	{
		public bool gotHit;
		public HitType hitType;
		public FrameData frameData;
		public MoveScript moveScript;
		public int staminaDamage;
		public int healthDamage;
		public int staggerDamage;
		public bool deathBlow;
		public bool counterhit;
	}
	public enum HitType
	{
		hit, block, guardBreak, juggle, parry, armor, grab, dash, onlyDamage
		//public bool gotHit;
	};


	public float direction;
	public float hurtboxDamageMultiplier = 1;

	public BoxCollider2D hurtbox;
	public int hurtboxPriority = 0;
	protected Collider2D enemyHitbox;
	public LayerMask collisionMask0, collisionMask1, collisionMask2;
	//protected int seq = 0;
	//public Controller controller;
	//public Status status;

	//public Collider2D hurtbox;
	public Hitstun hitstunScript;
	/*

	protected int hitstun, hitstunScript.blockstun;
	protected float pushbackHit, hitstunScript.verticalJugglevelocity;

	protected int damageOnHit;
	protected int hitstunScript.damageOnBlock;
	protected int staminaCost;
	protected int staminaDamageOnHit;
	protected int staminaDamageOnBlock;
	protected int hitstunType;
	protected int blockstunType;

*/
	FrameData ownerFrameData;
	public int hitstun, blockstun;
	public float pushbackHit, pushbackBlock, horizontalJuggleVelocity, verticalJugglevelocity;

	//public float whiteHealthPercentage = 75; //percentuale del danno trasferito alla vita bianca
	public int damageOnHit;
	public int damageOnBlock;
	//public int staminaCost;
	public int staminaDamageOnHit;
	public int staminaDamageOnBlock;
	public Status.status hitstunType;
	//public int blockstunType;

	public bool hit = false;
	public bool block = false;
	public bool juggle = false;
	public bool parry = false;


	public GameObject offender;
	public Controller2D offenderController;
	public FrameData offenderMoveSet;
	public MoveScript offenderMove;
	public Projectile offenderProjectile;
	[HideInInspector]
	public int hb0Index, hb1Index, hb2Index;
	int friendlyLayer;


	protected int enemyFaction = 0;
	public Collider2D enemyPosition;

	public bool gotHit = false;



	public void Initialize() {

		//hurtbox = GetComponent<Collider2D> ();
		//hurtboxHit = hurtbox.GetComponent<HurtboxHit> ();
		//controller = GetComponentInParent<Player> ();
		status = controller.status;
		conditions = controller.conditions;
		collisionMask = controller.groundMask;
		//controller.seq = 0;
		collisionMask0 = controller.groundMask;
		collisionMask1 = collisionMask0;
		collisionMask2 = collisionMask0;

		collisionMask0 |= 1 << controller.character0LayerIndex;

		collisionMask1 |= 1 << controller.character1LayerIndex;

		collisionMask2 |= 1 << controller.character2LayerIndex;


		if (status.faction == 0)
		{
			friendlyLayer = hb0Index;
		}
		else if (status.faction == 1)
		{
			friendlyLayer = hb1Index;
		}
		else
		{
			friendlyLayer = hb2Index;
		}
	}


	public override void MyStopSequence() {

		controller.seq = 0;
		status.mystatus = Status.status.neutral;
		//controller.animator.SetBool ("hitstun", false);
		//controller.animator.SetBool ("blockstun", false);

		//controller.animator.SetBool ("neutral", true);

		hit = false;
		block = false;
		juggle = false;
		//StartCoroutine (status.WaitBeforeRecharge ());
		controller.currentMove = null;
		//controller.animator.speed = 1;
	}


	public HitInfo CompileHitInfo()
	{
		HitInfo hitInfo = new HitInfo();
		hitInfo.frameData = offenderMoveSet;
		hitInfo.moveScript = offenderMove;

		float positionDifference;

		if (offenderMoveSet.currentHitProperties.hitstun <= 0)
		{

			status.hyperArmor -= offenderMoveSet.currentHitProperties.hyperArmorDamage;
			status.hyperArmorRechargeCounter = 0;
			hitInfo.gotHit = true;
			hitInfo.hitType = HitType.onlyDamage;
			hitInfo.staminaDamage = 0;// Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.parryStaminaDamagePercentage);
			hitInfo.staggerDamage = 0;
			hitInfo.healthDamage = WeaponDamageCalculator.CalculateWeaponDamage(offenderMoveSet.currentHitProperties.damageOnHit * hitInfo.frameData.damageMultiplier * hurtboxDamageMultiplier, 
				controller, hitInfo.frameData.owner);
					
			//return hitInfo;
		}
		else
		{
			if (status.hyperArmor <= 0 || offenderMoveSet.currentHitProperties.breaksHyperArmor || status.hyperArmor <= offenderMoveSet.currentHitProperties.hyperArmorDamage)
			{
				if (!hitInfo.frameData.isProjectile)
				{
					positionDifference = Mathf.Sign(hurtbox.bounds.center.x - enemyPosition.bounds.center.x);
					direction = offenderMoveSet.facingRight ? 1 : -1;
				}
				else
				{
					Vector2 enemyPosition = offenderMoveSet.projectileScript.transform.position;
					positionDifference = Mathf.Sign(hurtbox.bounds.center.x - enemyPosition.x);
					direction = controller.facingRight ? -1 : 1;
				}

				if (status.myPhysicsStatus == Status.physicsStatus.grounded)
				{

					if (controller.isParrying && ((positionDifference < 0 && controller.facingRight) ||
						(positionDifference > 0 && !controller.facingRight)) && !offenderMoveSet.currentHitProperties.unblockable)
					{

						hitInfo.gotHit = true;
						hitInfo.staminaDamage = Mathf.RoundToInt(offenderMoveSet.currentHitProperties.staminaDamageOnHit * status.parryStaminaDamagePercentage);
						hitInfo.staggerDamage = Mathf.RoundToInt(offenderMoveSet.currentHitProperties.staminaDamageOnHit * status.blockStaggerMultiplier);

						//hitInfo.healthDamage = Mathf.RoundToInt(offenderMoveSet.damageOnHit * status.blockDamagePercentage);



						if (hitInfo.staminaDamage <= status.currentStamina)// && hitInfo.staggerDamage <= status.maxStagger - status.currentStagger)
						{
							//print("parryyyyy");
							hitInfo.hitType = HitType.parry;
							hitInfo.healthDamage = WeaponDamageCalculator.CalculateWeaponDamage(offenderMoveSet.currentHitProperties.damageOnHit * status.blockDamagePercentage
														* hitInfo.frameData.damageMultiplier * hurtboxDamageMultiplier,
													controller, hitInfo.frameData.owner);
						}
						else
						{
							//hitInfo.staminaDamage = 0;
							hitInfo.healthDamage = WeaponDamageCalculator.CalculateWeaponDamage(offenderMoveSet.currentHitProperties.damageOnHit
														* hitInfo.frameData.damageMultiplier * hurtboxDamageMultiplier,
													controller, hitInfo.frameData.owner);
							hitInfo.hitType = HitType.guardBreak;
							//print("guardbreak PARRY");
						}



							//print("compile: parry");
							//return hitInfo;
						


					}

					else
					if ((!status.isBlocking && status.mystatus != Status.status.blocking && status.mystatus != Status.status.blockstun1) ||
							   (((positionDifference > 0 && controller.facingRight) || (positionDifference < 0 && !controller.facingRight)))
							   || offenderMoveSet.currentHitProperties.unblockable)
					{

						if (offenderMoveSet.currentHitProperties.launcher == false)
						{

							hitInfo.gotHit = true;
							hitInfo.hitType = HitType.hit;



						}
						else
						{
							hitInfo.gotHit = true;
							hitInfo.hitType = HitType.juggle;
						}

						hitInfo.staminaDamage = 0;// Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.hitStaminaDamagePercentage);
						hitInfo.staggerDamage = Mathf.RoundToInt(offenderMoveSet.currentHitProperties.staminaDamageOnHit * status.hitStaggerMultiplier);

						hitInfo.healthDamage = WeaponDamageCalculator.CalculateWeaponDamage(offenderMoveSet.currentHitProperties.damageOnHit
							* hitInfo.frameData.damageMultiplier * hurtboxDamageMultiplier,
						controller, hitInfo.frameData.owner);

						if (controller.counterhitState)
						{
							hitInfo.counterhit = true;
							hitInfo.healthDamage += Mathf.RoundToInt((float)hitInfo.healthDamage * status.counterhitBonusDamage);
						}
					}
					else
					if ((status.isBlocking || status.mystatus == Status.status.blocking || status.mystatus == Status.status.blockstun1) &&
							((positionDifference < 0 && controller.facingRight) || (positionDifference > 0 && !controller.facingRight))
							&& !offenderMoveSet.currentHitProperties.unblockable)
					{
						hitInfo.gotHit = true;
						hitInfo.staminaDamage = Mathf.RoundToInt(offenderMoveSet.currentHitProperties.staminaDamageOnHit * status.blockStaminaDamagePercentage);
						hitInfo.staggerDamage = Mathf.RoundToInt(offenderMoveSet.currentHitProperties.staminaDamageOnHit * status.blockStaggerMultiplier);

						//print("BLOCK: " + hitInfo.staminaDamage);

						if (hitInfo.staminaDamage <= status.currentStamina)// && hitInfo.staggerDamage <= status.maxStagger - status.currentStagger)
						{
							hitInfo.hitType = HitType.block;
							hitInfo.healthDamage = WeaponDamageCalculator.CalculateWeaponDamage(offenderMoveSet.currentHitProperties.damageOnHit * status.blockDamagePercentage
														* hitInfo.frameData.damageMultiplier * hurtboxDamageMultiplier,
													controller, hitInfo.frameData.owner);

							//print("no guardbreak block");
						}
						else
                        {
							//hitInfo.staminaDamage = 0;
							hitInfo.healthDamage = WeaponDamageCalculator.CalculateWeaponDamage(offenderMoveSet.currentHitProperties.damageOnHit
														* hitInfo.frameData.damageMultiplier * hurtboxDamageMultiplier,
													controller, hitInfo.frameData.owner);
							hitInfo.hitType = HitType.guardBreak;
							//print("guardbreak BLOCK");
						}
					}
				}
				else
				{
					hitInfo.gotHit = true;
					hitInfo.hitType = HitType.juggle;
					hitInfo.staminaDamage = 0;// Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.hitStaminaDamagePercentage);
					hitInfo.staggerDamage = Mathf.RoundToInt(offenderMoveSet.currentHitProperties.staminaDamageOnHit * status.hitStaggerMultiplier);

					hitInfo.healthDamage = WeaponDamageCalculator.CalculateWeaponDamage(offenderMoveSet.currentHitProperties.damageOnHit
												* hitInfo.frameData.damageMultiplier * hurtboxDamageMultiplier,
											controller, hitInfo.frameData.owner);

					if (controller.counterhitState)
					{
						hitInfo.counterhit = true;
						hitInfo.healthDamage += Mathf.RoundToInt((float)hitInfo.healthDamage * status.counterhitBonusDamage);
					}
				}
			}
			else if ((status.hyperArmor > 0 && status.hyperArmor > offenderMoveSet.currentHitProperties.hyperArmorDamage) || !offenderMoveSet.currentHitProperties.breaksHyperArmor)
			{
				hitInfo.gotHit = false;
				hitInfo.hitType = HitType.armor;

				hitInfo.healthDamage = WeaponDamageCalculator.CalculateWeaponDamage(offenderMoveSet.currentHitProperties.damageOnHit
							* hitInfo.frameData.damageMultiplier * hurtboxDamageMultiplier,
						controller, hitInfo.frameData.owner);
				hitInfo.staggerDamage = Mathf.RoundToInt(offenderMoveSet.currentHitProperties.staminaDamageOnHit * status.hitStaggerMultiplier);

			}
		}
		
		if (hitInfo.healthDamage >= status.currentHealth)
			hitInfo.deathBlow = true;
		else
			hitInfo.deathBlow = false;

		//print("Hurt: " + hitInfo.hitType);
		return hitInfo;

	}
	/*
	Controller:
	dammi le hitbox
	per ogni hitbox:
		chiamo hurtboxhit
			test status, fazione, etc
			se tutto ok,
				setta flag dentro hurtboxhit
	controllo i flag da hurtboxhit
	hitstun

	Controller:
	dammi le hitbox
	per ogni hitbox:
		creo hurtboxhit
			test status, fazione, etc
			se tutto ok, aggiungo alla lista
				setta flag dentro hurtboxhit
	prelevo i flag da hurtboxhit
	hitstun
*/
	public void CheckHitboxCollisions(Collider2D hitbox, bool onTriggerEnter) {

		hitboxLayer = hitbox.gameObject.layer;
		if (hitboxLayer == friendlyLayer)
		{
			//if (controller.status.faction == 1)
			//print("That's a friendly " + friendlyLayer);
			return;
		}

		if (hitbox.transform.parent == null)
		{

			if (onTriggerEnter)
			{
				//print("ghost");

				controller.insideEnemyGhostHitbox = true;
				
			}
			return;
		}

		//if (!status.invincible)
		{

			ownerFrameData = hitbox.transform.parent.GetComponent<FrameData>();
			MoveScript ownerMove = ownerFrameData.moveScript;
			offenderController = ownerFrameData.owner;

			bool isInLineOfSight;
			Vector3 rayOrigin = controller.lineOfSightTarget;
			Vector3 heading = (offenderController == null ? ownerFrameData.transform.position : (Vector3)offenderController.lineOfSightOrigin) - rayOrigin;
			//Vector2 heading = (hitbox.bounds.center) - rayOrigin;
			//Vector2 heading = (offenderController.lineOfSightOrigin) - rayOrigin;

			Debug.DrawRay(rayOrigin, heading, Color.green, 1);

			if (hitboxLayer == hb0Index)
			{
				collisionMask = collisionMask0;
			}
			else if (hitboxLayer == hb1Index)
			{
				collisionMask = collisionMask1;
			}
			else
			{
				collisionMask = collisionMask2;
			}



			//RaycastHit2D [] hit = Physics2D.RaycastAll(rayOrigin, heading, heading.magnitude, collisionMask);
			RaycastHit2D[] hit = Physics2D.RaycastAll(rayOrigin, heading, heading.magnitude, controller.groundCF.layerMask);

			isInLineOfSight = true;
			for (int i = 0; i < hit.Length; i++)
			{
				if (hit[i].collider.gameObject.layer == controller.character0LayerIndex || hit[i].collider.gameObject.layer == controller.character1LayerIndex
					|| hit[i].collider.gameObject.layer == controller.character2LayerIndex)
				{
					if (hit[i].collider == offenderController.verticalCollider)
					{
						//print("non los");
						isInLineOfSight = false;
						break;
					}

				}
				else
				{
					//print("non lossss");
					isInLineOfSight = false;
					break;
				}
			}
			/*
			if (!hit)
			{
				isInLineOfSight = true;
				//print("is los");
			}
			else
			{
				isInLineOfSight = false;
				//print("is not los ");

			}
			*/
			//Debug.DrawRay(controller.tr.position, heading, Color.green, 20);

			if (isInLineOfSight)
			{
				/*
				if (hitbox == ownerFrameData.ghostHitbox)
				{

					if (onTriggerEnter)
					{
						controller.insideEnemyGhostHitbox = true;
						print("ghost");
					}
					return;
				}
				*/

				//print("is los");
				if (!status.invincible)
				{

					/*
					if (ownerMoveSet.grab != null)
					{
						print("owner grab!");
						controller.status.invincible = true;

						//ownerMoveSet.owner.status.invincible = true;
						ownerMoveSet.owner.timedFrameDatas.Clear();
						//ownerMoveSet.owner.StartNewMove(ownerMoveSet.grab);
						ownerMoveSet.owner.status.invincible = true;
						ownerMoveSet.owner.lastCollisionHurtbox.move = null;
						ownerMoveSet.owner.lastCollisionHurtbox.hurtbox = null;
						ownerMoveSet.owner.collisionHurtbox.Clear();
						controller.hitDelay = 0;
						ownerMoveSet.owner.hitDelay = 0;
					}
					*/
					//print("move: " + ownerMoveSet);
					if (!ownerFrameData.isProjectile)
					{
						offender = hitbox.gameObject;

						//offenderMove = offender.GetComponent<MoveScript> ();
						offenderMove = ownerMove;
						/*
						if (offenderMove == null) {
							offenderMove = offender.GetComponentInParent<MoveScript> ();
							//print ("nomove");
						}
						*/
						offenderController = offenderMove.controller;
						//offenderMoveSet = offenderMove.move;
						offenderMoveSet = ownerFrameData;
						enemyHitbox = hitbox;

						//enemyFaction = offender.transform.parent.transform.parent.GetComponent<Status> ().faction;
						enemyFaction = offenderMoveSet.faction;
						//enemyPosition = offender.transform.parent.transform.parent.GetComponent<Collider2D> ();
						enemyPosition = offenderMoveSet.owner.verticalCollider;

						//if (status.faction != enemyFaction) {
						/*
					print ("gameobject: " + gameObject);

					print ("offender: " + offender);
	*/






						//VECCHIO
						/*
						for (int i = 1; i < controller.horizontalRayCount - 1; i++) {
							Vector2 rayOrigin = (heading.x <= 0) ? controller.rayCastOrigins.bottomLeft : controller.rayCastOrigins.bottomRight;
							rayOrigin += Vector2.up * (controller.horizontalRaySpacing * i);

							RaycastHit2D hit = Physics2D.Raycast (rayOrigin, heading, heading.magnitude, collisionMask);

							Debug.DrawRay (rayOrigin, heading.normalized * heading.magnitude, Color.red);

							if (!hit) {
								isInLineOfSight = true;
								break;
							} else
								isInLineOfSight = false;



						}
						*/






						//if (isInLineOfSight)
						{

							//controller.ManageCollisionHurtboxes (ownerMoveSet, this);
							HitInfo newHitInfo = CompileHitInfo();

							/*
							if (ownerMoveSet.grab != null && newHitInfo.hitType == HitType.hit)
							{
								print("owner grab!");
								//controller.status.invincible = true;

								//ownerMoveSet.owner.status.invincible = true;
								ownerMoveSet.owner.timedFrameDatas.Clear();
								//ownerMoveSet.owner.StartNewMove(ownerMoveSet.grab);
								ownerMoveSet.owner.status.invincible = true;
								ownerMoveSet.owner.lastCollisionHurtbox.move.frameData = null;
								ownerMoveSet.owner.lastCollisionHurtbox.hurtbox = null;
								ownerMoveSet.owner.collisionHurtbox.Clear();
								controller.hitDelay = 0;
								ownerMoveSet.owner.hitDelay = 0;
							}
							*/
							controller.ManageCollisionHurtboxes(ref newHitInfo, this);
						}
					}
					else
					{
						offender = hitbox.gameObject;
						offenderMoveSet = ownerFrameData;
						/*
						 * timemanager: 1
						 * fixedupdatestart: 1
						 * 
						 * fixedupdateend: 0.3
						 * 
						 * 
						 * */



						//offenderProjectile = offender.GetComponentInParent<Projectile> ();
						offenderProjectile = offenderMoveSet.projectileScript;
						//print ("nomove");
						offenderMoveSet = offenderProjectile.move;
						offenderController = offenderMoveSet.owner;

						enemyHitbox = hitbox;

						//enemyFaction = offender.transform.parent.transform.parent.GetComponent<Status> ().faction;
						enemyFaction = offenderMoveSet.faction;
						//enemyPosition = offender.transform.parent.transform.parent.GetComponent<Collider2D> ();
						//enemyPosition = offenderMoveSet.owner.myCollider;

						HitInfo newHitInfo = CompileHitInfo();
						controller.ManageCollisionHurtboxes(ref newHitInfo, this);


						//controller.ManageCollisionHurtboxes (ownerMoveSet, this);

					}

				}
				else //se sono invincibile, TEST
				{
					//if (controller.status.mystatus == Status.status.dashing)
					{
						if (onTriggerEnter || !ownerFrameData.objectsHit.Contains(controller.gameObject))
						{
							controller.hitWhileInvincible = true;
						}
					}
				}
			}
		}

	}
	void OnTriggerEnter2D(Collider2D hitbox) {

		CheckHitboxCollisions(hitbox, true);
		return;
	}

	void OnTriggerStay2D(Collider2D hitbox) {
		CheckHitboxCollisions(hitbox, false);
		return;
		//print ("ontriggerenter: " + Time.fixedTime + " " + controller.gameObject);
		//print (status.invincible);
		if (!status.invincible) {

			GameObject HBowner = hitbox.gameObject;
			FrameData ownerMoveSet;
			MoveScript ownerMove = HBowner.GetComponent<MoveScript>();
			if (ownerMove == null)
				ownerMove = HBowner.GetComponentInParent<MoveScript>();
			if (ownerMove == null)
				ownerMoveSet = HBowner.GetComponentInParent<FrameData>();
			else
				ownerMoveSet = ownerMove.move;

			print("owner: " + ownerMoveSet.gameObject);
			int ownerFaction = ownerMoveSet.faction;





			if (status.faction != ownerFaction) {


				if (!ownerMoveSet.isProjectile) {
					offender = hitbox.gameObject;

					offenderMove = offender.GetComponent<MoveScript>();
					if (offenderMove == null) {
						offenderMove = offender.GetComponentInParent<MoveScript>();
						//print ("nomove");
					}
					offenderController = offenderMove.controller;
					offenderMoveSet = offenderMove.move;
					enemyHitbox = hitbox;

					//enemyFaction = offender.transform.parent.transform.parent.GetComponent<Status> ().faction;
					enemyFaction = offenderMoveSet.faction;
					//enemyPosition = offender.transform.parent.transform.parent.GetComponent<Collider2D> ();
					//enemyPosition = offenderMoveSet.owner.myCollider;

					//if (status.faction != enemyFaction) {
					/*
				print ("gameobject: " + gameObject);

				print ("offender: " + offender);
*/





					bool isInLineOfSight = false;

					Vector3 heading = offenderController.gameObject.transform.position - gameObject.transform.position;

					for (int i = 1; i < controller.horizontalRayCount - 1; i++) {
						Vector2 rayOrigin = (heading.x <= 0) ? controller.rayCastOrigins.bottomLeft : controller.rayCastOrigins.bottomRight;
						rayOrigin += Vector2.up * (controller.horizontalRaySpacing * i);

						RaycastHit2D hit = Physics2D.Raycast(rayOrigin, heading, heading.magnitude, collisionMask);

						Debug.DrawRay(rayOrigin, heading.normalized * heading.magnitude, Color.red);

						if (!hit) {
							isInLineOfSight = true;
							break;
						} else
							isInLineOfSight = false;



					}







					if (isInLineOfSight) {
						//controller.ManageCollisionHurtboxes (ownerMoveSet, this);
					}
				} else {
					offender = hitbox.gameObject;


					offenderProjectile = offender.GetComponentInParent<Projectile>();
					//print ("nomove");
					offenderMoveSet = offenderProjectile.move;
					offenderController = offenderMoveSet.owner;

					enemyHitbox = hitbox;

					//enemyFaction = offender.transform.parent.transform.parent.GetComponent<Status> ().faction;
					enemyFaction = offenderMoveSet.faction;
					enemyPosition = hitbox;
					//enemyPosition = offender.transform.parent.transform.parent.GetComponent<Collider2D> ();
					//enemyPosition = offenderMoveSet.owner.myCollider;
				}

			}
		}
	}

	public void GetHitByProjectile(FrameData offenderMoveSet) {
		//if (status.faction != enemyFaction) {
		//print ("OUCH");
		//int whiteHealthDamage = Mathf.RoundToInt(offenderMoveSet.damageOnHit * status.currentRedHealthPercentage / 100);
		if (offenderMoveSet == null)
			return;

		/*
		if (offenderMoveSet.hitstun <= 0)
		{
			damageOnHit = offenderMoveSet.damageOnHit;
			staminaDamageOnHit = Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.hitStaminaDamagePercentage);

			status.TakeDamage(Mathf.RoundToInt
							(WeaponDamageCalculator.CalculateWeaponDamage(damageOnHit * offenderMoveSet.damageMultiplier, controller, offenderMoveSet.owner)
							 * hurtboxDamageMultiplier), staminaDamageOnHit);
			//status.hyperArmor -= offenderMoveSet.hyperArmorDamage;
			//status.hyperArmorRechargeCounter = 0;
			return;
		}
		*/
		Vector2 enemyPosition = offenderMoveSet.projectileScript.transform.position;

		//offenderMoveSet.projectileScript.OnCollisionBehaviour();

		if (status.hyperArmor <= 0 || offenderMoveSet.currentHitProperties.breaksHyperArmor || offenderMoveSet.currentHitProperties.hyperArmorDamage >= status.hyperArmor) {


			offenderMoveSet.gameObject.GetComponent<Projectile>().OnCollisionBehaviour();

			if (status.maxHyperArmor > 0)
				status.hyperArmor = status.maxHyperArmor - (offenderMoveSet.currentHitProperties.hyperArmorDamage - status.hyperArmor);
			if (status.hyperArmor < 0)
				status.hyperArmor = status.maxHyperArmor;

			status.hyperArmorRechargeCounter = 0;

			float positionDifference = Mathf.Sign(hurtbox.bounds.center.x - enemyPosition.x);

			direction = controller.facingRight ? -1 : 1;


			if (status.myPhysicsStatus == Status.physicsStatus.grounded) {

				if (controller.isParrying && ((positionDifference < 0 && controller.facingRight) || (positionDifference > 0 && !controller.facingRight))
					&& !offenderMoveSet.currentHitProperties.unblockable) {

					if (offenderMoveSet.currentHitProperties.hitstun > 0)
					{
						parry = true;
						gotHit = true;
					}

					hitstun = offenderMoveSet.currentHitProperties.minimumUntechableHitstunWindow;
					pushbackHit = offenderMoveSet.currentHitProperties.pushbackHit;
					damageOnHit = Mathf.RoundToInt(offenderMoveSet.currentHitProperties.damageOnHit * status.blockDamagePercentage);
					staminaDamageOnHit = Mathf.RoundToInt(offenderMoveSet.currentHitProperties.staminaDamageOnHit * status.parryStaminaDamagePercentage);

					//staminaDamageOnHit = 0;
					//hitstunType = offenderMoveSet.blockstunType;

					//status.currentStamina -= staminaDamageOnBlock;
					//status.currentHealth -= damageOnBlock;

					//status.TakeDamage (whiteHealthDamage, staminaDamageOnBlock, whiteHealthDamage, offenderMoveSet.guardDamage);
					

					//status.TakeDamage (- status.currentRedHealth, 0, 0, offenderMoveSet.guardDamage);
					//status.currentRedHealth = 0;




					return;

				}

				/*
				if ((offenderMoveSet.kick == false && status.mystatus != Status.status.blocking && status.mystatus != Status.status.blockstun1 &&
					status.mystatus != Status.status.blockstun2) ||
					(offenderMoveSet.kick == false && (status.mystatus == Status.status.blocking || status.mystatus == Status.status.blockstun1) &&
						((positionDifference > 0 && controller.facingRight) || (positionDifference < 0 && !controller.facingRight))) ||
					(offenderMoveSet.kick))
					*/
				if (offenderMoveSet.currentHitProperties.unblockable ||
				(!status.isBlocking && status.mystatus != Status.status.blocking && status.mystatus != Status.status.blockstun1 && status.mystatus != Status.status.guardBreak) ||
						   (((positionDifference > 0 && controller.facingRight) || (positionDifference < 0 && !controller.facingRight))))
				{

					//print ("beh ma alora" + status.mystatus);

					//if (controller.currentMove != null)
					//controller.currentMove.itsTimeToStopTheSequence = true;
					//controller.StartNewMove (controller.animations[5]);

					//print ("HIT");




					//Debug.Log (offenderMoveSet.kick + " " + status.mystatus);
					/*
					if (direction > 0 && controller.facingRight)
						controller.Flip ();
					else if (direction < 0 && !controller.facingRight)
						controller.Flip ();
						*/


					/*
					if (controller.facingRight && positionDifference > 0)
						controller.Flip ();
					else if (!controller.facingRight && positionDifference < 0)
						controller.Flip ();
                        */


					//controller.hitstop = offenderMoveSet.hitstop;


					//int whiteHealthDamage = offenderMoveSet.damageOnHit * whiteHealthPercentage / 100;

					//print ("danno: " + damageOnHit);

					//controller.seq = 0;

					if (offenderMoveSet.currentHitProperties.launcher == false) {

						hitstun = offenderMoveSet.currentHitProperties.hitstun;
						pushbackHit = offenderMoveSet.currentHitProperties.pushbackHit;
						damageOnHit = offenderMoveSet.currentHitProperties.damageOnHit;
						staminaDamageOnHit = 0;// Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.hitStaminaDamagePercentage);
						hitstunType = offenderMoveSet.hitstunType;


						//status.currentRedHealth = 0;
						

						if (offenderMoveSet.currentHitProperties.hitstun > 0)
						{
							hit = true;
							gotHit = true;
						}
						//print ("HIT");


					} else {

						//launcher
						Debug.Log("launchhhhh");
						damageOnHit = offenderMoveSet.currentHitProperties.damageOnHit;
						staminaDamageOnHit = 0;// Mathf.RoundToInt(status.hitStaminaDamagePercentage * offenderMoveSet.staminaDamageOnHit);
						hitstunType = offenderMoveSet.hitstunType;
						verticalJugglevelocity = offenderMoveSet.currentHitProperties.launchVertivalVelocity;
						horizontalJuggleVelocity = offenderMoveSet.currentHitProperties.launchHorizontalVelocity;


						//status.currentRedHealth = 0;
					



						//status.currentStamina -= staminaDamageOnHit;
						//status.currentHealth -= damageOnHit;
						//status.TakeDamage(damageOnHit, 0, damageOnHit);
						if (offenderMoveSet.currentHitProperties.hitstun > 0)
						{
							juggle = true;
							//status.mystatus = Status.status.juggle;
							gotHit = true;
						}
						print("JUGGLE");
					}



				}
				else //if ((status.mystatus == Status.status.blocking || status.mystatus == Status.status.blockstun1) &&
					 //((positionDifference < 0 && controller.facingRight) || (positionDifference > 0 && !controller.facingRight)))
				{

					//if (controller.currentMove != null)
					//controller.currentMove.itsTimeToStopTheSequence = true;
					//controller.StartNewMove (controller.animations[5]);

					/*
					 * hurtbox ontriggerenter
					 * fai test fazione, lineofsight, etc
					 * se tutto ok:
					 * 	se controller non ha una collisione con mossa
					 * 	invio a controller numero hurtbox, mossa che l'ha colpito
					 * 	else se l'hurtbox è minore di quello della collisione.hurtbox
					 * 		sostituisco collisione.hurtbox con il nuovo
					 * 	
					 * ripeti per ogni hurtbox
					 * 
					 * controller ha vettore di collisioni
					 * per ogni mossa nemica:
					 * 	guardo l'hurtbox con numero minore
					 * 	gestisco quella collisione
					 * svuoto lista collisioni
					 * 
					 * vettore di struct collisione
					 * 
					 * struct collisione {
					 * mossa
					 * hurtbox
					 * }
					 *
					 * 
					 * 
					 * 
					 * 
					 * 
					 * 
					 * */






					/*
					if (direction > 0 && controller.facingRight)
						controller.Flip ();
					else if (direction < 0 && !controller.facingRight)
						controller.Flip ();
					*/

					//print ("BLOCK");
					//offenderMove.OnBlock();

					hitstun = offenderMoveSet.currentHitProperties.minimumUntechableHitstunWindow;
					pushbackHit = offenderMoveSet.currentHitProperties.pushbackHit;
					damageOnHit = Mathf.RoundToInt(offenderMoveSet.currentHitProperties.damageOnHit * status.blockDamagePercentage);
					staminaDamageOnHit = Mathf.RoundToInt(offenderMoveSet.currentHitProperties.staminaDamageOnHit * status.blockStaminaDamagePercentage);
					//if (offenderMoveSet.kick)
					//staminaDamageOnHit *= 2;
					hitstunType = offenderMoveSet.blockstunType;

					//status.currentStamina -= staminaDamageOnBlock;
					//status.currentHealth -= damageOnBlock;

					//status.TakeDamage (whiteHealthDamage, staminaDamageOnBlock, whiteHealthDamage, offenderMoveSet.guardDamage);
					

					if (offenderMoveSet.currentHitProperties.hitstun > 0)
					{
						block = true;
						//status.mystatus = Status.status.blockstun1;

						gotHit = true;
					}
				}

			} else { //airborne
				/*
				if (direction > 0 && controller.facingRight)
					controller.Flip ();
				else if (direction < 0 && !controller.facingRight)
					controller.Flip ();
				*/
				//controller.hitstop = offenderMoveSet.hitstop;
				/*
				if (controller.facingRight && positionDifference > 0)
					controller.Flip ();
				else if (!controller.facingRight && positionDifference < 0)
					controller.Flip ();
                    */

				//if (controller.currentMove != null)
				//controller.currentMove.itsTimeToStopTheSequence = true;
				//controller.StartNewMove (controller.animations[5]);


				if (status.mystatus == Status.status.juggle) { //JUGGLE
					Debug.Log("juggle");
					damageOnHit = offenderMoveSet.currentHitProperties.damageOnHit;
					staminaDamageOnHit = 0;// Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.hitStaminaDamagePercentage);
					hitstunType = offenderMoveSet.hitstunType;
					verticalJugglevelocity = offenderMoveSet.currentHitProperties.juggleVertivalVelocity;
					horizontalJuggleVelocity = offenderMoveSet.currentHitProperties.juggleHorizontalVelocity;

					//status.currentStamina -= staminaDamageOnHit;
					//status.currentHealth -= damageOnHit;

					
					if (offenderMoveSet.currentHitProperties.hitstun > 0)
					{
						juggle = true;
						//status.mystatus = Status.status.juggle;
						//print("JUGGLE!");
						gotHit = true;
					}
				} else {
					//Debug.Log ("launch");
					damageOnHit = offenderMoveSet.currentHitProperties.damageOnHit;
					staminaDamageOnHit = 0;// Mathf.RoundToInt(status.hitStaminaDamagePercentage * offenderMoveSet.staminaDamageOnHit);
					hitstunType = offenderMoveSet.hitstunType;
					verticalJugglevelocity = offenderMoveSet.currentHitProperties.launchVertivalVelocity;
					horizontalJuggleVelocity = offenderMoveSet.currentHitProperties.launchHorizontalVelocity;

					//status.currentStamina -= staminaDamageOnHit;
					//status.currentHealth -= damageOnHit;

					
					if (offenderMoveSet.currentHitProperties.hitstun > 0)
					{
						juggle = true;
						//status.mystatus = Status.status.juggle;
						gotHit = true;
					}
				}
			}
		} else if ((status.hyperArmor > 0 && status.hyperArmor > offenderMoveSet.currentHitProperties.hyperArmorDamage) && !offenderMoveSet.currentHitProperties.breaksHyperArmor) { //da testare
																																		   //offenderController.hitstop = offenderMoveSet.hitstop;
																																		   //controller.hitstop = offenderMoveSet.hitstop;

			//controller.hitstop = offenderMoveSet.hitstop;
			controller.shaking = true;
			//status.currentHealth -= offenderMoveSet.damageOnHit;
			//print("ARMOR");
			if (offenderMoveSet.currentHitProperties.hitstun > 0)
				controller.hitstop = Mathf.RoundToInt(15f * controller.currentTimeScale);

			damageOnHit = offenderMoveSet.currentHitProperties.damageOnHit;
			staminaDamageOnHit = 0;// Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.hitStaminaDamagePercentage);

			//offenderController.hitstop = (int)(15f * controller.currentTimeScale);
			

			status.hyperArmor -= offenderMoveSet.currentHitProperties.hyperArmorDamage;
			status.hyperArmorRechargeCounter = 0;

		}


	}

	public void GetHit(HitInfo hitInfo)
	{
		status.TakeDamage(hitInfo);
		if (status.hyperArmor <= 0 || hitInfo.frameData.currentHitProperties.breaksHyperArmor || status.hyperArmor <= hitInfo.frameData.currentHitProperties.hyperArmorDamage)
		{

			if (hitInfo.frameData.isProjectile)
			{
				//print ("PUF");
				offenderMoveSet.gameObject.GetComponent<Projectile>().OnCollisionBehaviour();
			}
			// status.armor = status.maxArmor;
			if (status.maxHyperArmor > 0)
				status.hyperArmor = status.maxHyperArmor - (hitInfo.frameData.currentHitProperties.hyperArmorDamage - status.hyperArmor);
			if (status.hyperArmor < 0)
				status.hyperArmor = status.maxHyperArmor;

			status.hyperArmorRechargeCounter = 0;

		}

			if (hitInfo.hitType == HitType.parry)
			{
				
				//staminaDamageOnHit = 0;
				hitInfo.moveScript.OnBlock();
				//status.currentStamina -= staminaDamageOnBlock;
				//status.currentHealth -= damageOnBlock;

				//status.TakeDamage (whiteHealthDamage, staminaDamageOnBlock, whiteHealthDamage, offenderMoveSet.guardDamage);
				

				//status.currentRedHealth = 0;

				return;

			}

			if (hitInfo.hitType == HitType.hit || hitInfo.hitType == HitType.juggle || hitInfo.hitType == HitType.grab)
			{
				hitInfo.moveScript.OnHit();
			}


			if (hitInfo.hitType == HitType.hit)
			{
				//if (hitInfo.frameData.launcher == false)
				{
					//if (offenderMoveSet.hitstun > 0)
					{
						hitstun = hitInfo.frameData.currentHitProperties.hitstun;
						pushbackHit = hitInfo.frameData.currentHitProperties.pushbackHit;
						
						hitstunType = hitInfo.frameData.hitstunType;


						//status.currentRedHealth = 0;
						


						if (hitInfo.frameData.currentHitProperties.hitstun > 0)
						{
							hit = true;
							//status.mystatus = Status.status.hitstun1;
							gotHit = true;
						}
					}

					//print ("HIT");
				}
			}

			if (hitInfo.hitType == HitType.juggle)
			{
				if (status.myPhysicsStatus == Status.physicsStatus.grounded) //da testare
				{
					//launcher
					Debug.Log("launch");
					
					hitstunType = hitInfo.frameData.hitstunType;
					verticalJugglevelocity = hitInfo.frameData.currentHitProperties.launchVertivalVelocity;
					horizontalJuggleVelocity = hitInfo.frameData.currentHitProperties.launchHorizontalVelocity;


					//status.currentRedHealth = 0;
					
				}
				else
				{
					
					hitstunType = hitInfo.frameData.hitstunType;
					verticalJugglevelocity = hitInfo.frameData.currentHitProperties.juggleVertivalVelocity;
					horizontalJuggleVelocity = hitInfo.frameData.currentHitProperties.juggleHorizontalVelocity;

					//status.currentStamina -= staminaDamageOnHit;
					//status.currentHealth -= damageOnHit;

				
				}
			}

		if (hitInfo.hitType == HitType.block)
        {
			hitInfo.moveScript.OnBlock();

			hitstun = hitInfo.frameData.currentHitProperties.minimumUntechableHitstunWindow;
			
			hitstunType = hitInfo.frameData.blockstunType;

			//status.currentStamina -= staminaDamageOnBlock;
			//status.currentHealth -= damageOnBlock;

			//status.TakeDamage (whiteHealthDamage, staminaDamageOnBlock, whiteHealthDamage, offenderMoveSet.guardDamage);
			

		}


	}

		

	

	/* VECCHIO
	public void GetHit (MoveScript offenderMove, FrameData offenderMoveSet) {
		if (status.faction == 1) 
		print ("OUCH " + gameObject);
		//int whiteHealthDamage = Mathf.RoundToInt(offenderMoveSet.damageOnHit * status.currentRedHealthPercentage / 100);
		//int whiteHealthBlockDamage = Mathf.RoundToInt(offenderMoveSet.damageOnHit * status.blockRedHealthPercentage / 100);

		//print ("hitbox id: " + hitbox.gameObject.GetInstanceID());
		//print ("move id: " + offenderMove.GetInstanceID());


		
		if (status.hyperArmor <= 0 || offenderMoveSet.breaksHyperArmor || status.hyperArmor <= offenderMoveSet.hyperArmorDamage) {

						if (offenderMoveSet.isProjectile) {
							//print ("PUF");
							offenderMoveSet.gameObject.GetComponent<Projectile> ().OnCollisionBehaviour ();
						}
            // status.armor = status.maxArmor;
            if (status.maxHyperArmor > 0)
              status.hyperArmor = status.maxHyperArmor - (offenderMoveSet.hyperArmorDamage - status.hyperArmor);
            if (status.hyperArmor < 0)
                status.hyperArmor = status.maxHyperArmor;

            status.hyperArmorRechargeCounter = 0;

            float positionDifference = Mathf.Sign (hurtbox.bounds.center.x - enemyPosition.bounds.center.x);
						direction = offenderMoveSet.facingRight ? 1 : -1;



			if (status.myPhysicsStatus == Status.physicsStatus.grounded) {

					if (controller.isParrying && ((positionDifference < 0 && controller.facingRight) || (positionDifference > 0 && !controller.facingRight)) 
					&& !offenderMoveSet.unblockable)
                    {
					if (offenderMoveSet.hitstun > 0)
					{
						parry = true;
						gotHit = true;
					}

                    
                    damageOnHit = Mathf.RoundToInt(offenderMoveSet.damageOnHit * status.blockDamagePercentage);
					staminaDamageOnHit = offenderMoveSet.staminaDamageOnHit;
					print("stamina parry " + staminaDamageOnHit);
					//staminaDamageOnHit = 0;
					offenderMove.OnBlock();
					//status.currentStamina -= staminaDamageOnBlock;
					//status.currentHealth -= damageOnBlock;

					//status.TakeDamage (whiteHealthDamage, staminaDamageOnBlock, whiteHealthDamage, offenderMoveSet.guardDamage);
					status.TakeDamage(Mathf.RoundToInt
							(WeaponDamageCalculator.CalculateWeaponDamage(damageOnHit * offenderMoveSet.damageMultiplier, controller, offenderMoveSet.owner)
							 * hurtboxDamageMultiplier), staminaDamageOnHit);

					//status.currentRedHealth = 0;

					return;

					} 
			 
					if (offenderMoveSet.unblockable || (!status.isBlocking && status.mystatus != Status.status.blocking && status.mystatus != Status.status.blockstun1 && status.mystatus != Status.status.blockstun2) ||
							   (((positionDifference > 0 && controller.facingRight) || (positionDifference < 0 && !controller.facingRight))))
					{ 

						print (status.mystatus);

						//if (controller.currentMove != null)
						//controller.currentMove.itsTimeToStopTheSequence = true;
						//controller.StartNewMove (controller.animations[5]);

						print ("HIT");






						offenderMove.OnHit ();
						//controller.hitstop = offenderMoveSet.hitstop;


						//int whiteHealthDamage = offenderMoveSet.damageOnHit * whiteHealthPercentage / 100;

						//print ("danno: " + damageOnHit);

						//controller.seq = 0;

					if (offenderMoveSet.launcher == false) {
						//if (offenderMoveSet.hitstun > 0)
						{
							hitstun = offenderMoveSet.hitstun;
							pushbackHit = offenderMoveSet.pushbackHit;
							damageOnHit = offenderMoveSet.damageOnHit;
							staminaDamageOnHit = 0;// Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.hitStaminaDamagePercentage);
							hitstunType = offenderMoveSet.hitstunType;
							print("hit stamina " + staminaDamageOnHit);

							status.currentRedHealth = 0;
							status.TakeDamage(Mathf.RoundToInt
							(WeaponDamageCalculator.CalculateWeaponDamage(damageOnHit * offenderMoveSet.damageMultiplier, controller, offenderMoveSet.owner)
							 * hurtboxDamageMultiplier), staminaDamageOnHit);


							if (offenderMoveSet.hitstun > 0)
							{
								hit = true;
								//status.mystatus = Status.status.hitstun1;
								gotHit = true;
							}
						}
						
									//print ("HIT");
					} else { 
						
									//launcher
									Debug.Log("launch");
									damageOnHit = offenderMoveSet.damageOnHit;
						staminaDamageOnHit = 0;// Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.hitStaminaDamagePercentage);
									hitstunType = offenderMoveSet.hitstunType;
									verticalJugglevelocity = offenderMoveSet.launchVertivalVelocity;
									horizontalJuggleVelocity = offenderMoveSet.launchHorizontalVelocity;


						//status.currentRedHealth = 0;
						status.TakeDamage(Mathf.RoundToInt
				(WeaponDamageCalculator.CalculateWeaponDamage(damageOnHit * offenderMoveSet.damageMultiplier, controller, offenderMoveSet.owner)
				 * hurtboxDamageMultiplier), staminaDamageOnHit);

						//status.currentStamina -= staminaDamageOnHit;
						//status.currentHealth -= damageOnHit;
						//status.TakeDamage(damageOnHit, 0, damageOnHit);
						if (offenderMoveSet.hitstun > 0)
						{
							juggle = true;
							//status.mystatus = Status.status.juggle;
							gotHit = true;
						}
									//print ("JUGGLE");
					}



					} else //if ((status.mystatus == Status.status.blocking || status.mystatus == Status.status.blockstun1) &&
							 //         ((positionDifference < 0 && controller.facingRight) || (positionDifference > 0 && !controller.facingRight)))
					{              
                    
                  
                    print ("BLOCK");
                    offenderMove.OnBlock();

                    hitstun = offenderMoveSet.minimumUntechableHitstunWindow;
                    pushbackHit = offenderMoveSet.pushbackHit;
                    damageOnHit = Mathf.RoundToInt(offenderMoveSet.damageOnHit * status.blockDamagePercentage);
					
					staminaDamageOnHit = Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.blockStaminaDamagePercentage);
					//if (offenderMoveSet.kick)
						//staminaDamageOnHit *= 2;
					print("block stamina " + staminaDamageOnHit);
					hitstunType = offenderMoveSet.blockstunType;

					//status.currentStamina -= staminaDamageOnBlock;
					//status.currentHealth -= damageOnBlock;

					//status.TakeDamage (whiteHealthDamage, staminaDamageOnBlock, whiteHealthDamage, offenderMoveSet.guardDamage);
					status.TakeDamage(Mathf.RoundToInt
							(WeaponDamageCalculator.CalculateWeaponDamage(damageOnHit * offenderMoveSet.damageMultiplier, controller, offenderMoveSet.owner)
							 * hurtboxDamageMultiplier), staminaDamageOnHit);


					//status.currentStamina -= staminaDamageOnBlock;
					//status.currentHealth -= damageOnBlock;

					//status.TakeDamage (whiteHealthDamage, staminaDamageOnBlock, whiteHealthDamage, offenderMoveSet.guardDamage);
					if (offenderMoveSet.hitstun > 0)
					{
						block = true;
						//status.mystatus = Status.status.blockstun1;

						gotHit = true;
					}
								}

			}
            else
            { //airborne
                                 
                offenderMove.OnHit ();
                                 //controller.hitstop = offenderMoveSet.hitstop;

                
                //if (controller.currentMove != null)
                //controller.currentMove.itsTimeToStopTheSequence = true;
                //controller.StartNewMove (controller.animations[5]);


                if (status.mystatus == Status.status.juggle) { //JUGGLE
								//Debug.Log("juggle");
								damageOnHit = offenderMoveSet.damageOnHit;
					staminaDamageOnHit = 0;// Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.hitStaminaDamagePercentage);
								hitstunType = offenderMoveSet.hitstunType;
								verticalJugglevelocity = offenderMoveSet.juggleVertivalVelocity;
								horizontalJuggleVelocity = offenderMoveSet.juggleHorizontalVelocity;

					//status.currentStamina -= staminaDamageOnHit;
					//status.currentHealth -= damageOnHit;

					status.TakeDamage(Mathf.RoundToInt
				(WeaponDamageCalculator.CalculateWeaponDamage(damageOnHit * offenderMoveSet.damageMultiplier, controller, offenderMoveSet.owner)
				 * hurtboxDamageMultiplier), staminaDamageOnHit);

					if (offenderMoveSet.hitstun > 0)
					{
						juggle = true;
						//status.mystatus = Status.status.juggle;
						print("JUGGLE!");
						gotHit = true;
					}
				}
                else
                {
								//Debug.Log ("launch");
					damageOnHit = offenderMoveSet.damageOnHit;
					staminaDamageOnHit = 0;// Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.hitStaminaDamagePercentage);
					hitstunType = offenderMoveSet.hitstunType;
								verticalJugglevelocity = offenderMoveSet.launchVertivalVelocity;
								horizontalJuggleVelocity = offenderMoveSet.launchHorizontalVelocity;

					//status.currentStamina -= staminaDamageOnHit;
					//status.currentHealth -= damageOnHit;

					status.TakeDamage(Mathf.RoundToInt
				(WeaponDamageCalculator.CalculateWeaponDamage(damageOnHit * offenderMoveSet.damageMultiplier, controller, offenderMoveSet.owner)
				 * hurtboxDamageMultiplier), staminaDamageOnHit);

					if (offenderMoveSet.hitstun > 0)
					{
						juggle = true;
						//status.mystatus = Status.status.juggle;
						gotHit = true;
					}
				}
			}
		} 
        else if ((status.hyperArmor > 0 && status.hyperArmor > offenderMoveSet.hyperArmorDamage) && !offenderMoveSet.breaksHyperArmor) //da testare
        {
			damageOnHit = offenderMoveSet.damageOnHit;
			staminaDamageOnHit = 0;// Mathf.RoundToInt(offenderMoveSet.staminaDamageOnHit * status.hitStaminaDamagePercentage);
			controller.shaking = true;

			controller.shakerWeight = 0.7f;
			offenderMove.OnHit ();
			//offenderController.hitstop = offenderMoveSet.hitstop;
			//controller.hitstop = offenderMoveSet.hitstop;

			//controller.hitstop = offenderMoveSet.hitstop;
			//status.currentHealth -= offenderMoveSet.damageOnHit;
			if (offenderMoveSet.hitstun > 0)
			{
				//controller.hitstop = Mathf.RoundToInt(15f * controller.currentTimeScale);
				//offenderMove.controller.hitstop = Mathf.RoundToInt(15f * controller.currentTimeScale);
			}
			status.TakeDamage(Mathf.RoundToInt
							(WeaponDamageCalculator.CalculateWeaponDamage(damageOnHit * offenderMoveSet.damageMultiplier, controller, offenderMoveSet.owner)
							 * hurtboxDamageMultiplier), staminaDamageOnHit);

			status.hyperArmor -= offenderMoveSet.hyperArmorDamage;
            status.hyperArmorRechargeCounter = 0;
            
        }
	}

	*/

	
}
			//Debug.Log ("HIT!");

			//HitBehaviour (hitstun, pushbackHit);
		 


	/*
	public override void MyExecuteSequence () {
		//controller.animator.SetB
		if (hit)
			HitBehaviour (hitstunScript.hitstun, hitstunScript.pushbackHit, hitstunScript.damageOnHit, hitstunScript.staminaDamageOnHit, hitstunScript.hitstunType);
		else if (block)
			BlockBehaviour (hitstunScript.blockstun, hitstunScript.pushbackBlock, hitstunScript.damageOnBlock, hitstunScript.staminaDamageOnBlock, hitstunScript.blockstunType);
		else if (juggle)
			JuggleBehaviour (hitstunScript.horizontalJuggleVelocity, hitstunScript.verticalJugglevelocity);
	}
	*/





