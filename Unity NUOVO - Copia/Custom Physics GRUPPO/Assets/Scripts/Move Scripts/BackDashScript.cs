﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackDashScript : DashScript
{
    public DashScript dash;
    protected override void MyOnEnable()
    {
        base.MyOnEnable();
        if (dash != null)
        {
            totalDashDuration = dash.totalDashDuration;
            horizontalMovements = new movement[dash.horizontalMovements.Length];
            for (int i = 0; i < dash.horizontalMovements.Length; i++)
            {
                horizontalMovements[i] = dash.horizontalMovements[i];
                horizontalMovements[i].velocity = -horizontalMovements[i].velocity;
            }
        }
    }

    public override void MyStartSequence()
    {
        if (controller.moveset != null)
        {
            if (controller.moveset.customBackDash != null && controller.moveset.customBackDash != this)
            {
                controller.StartNewMove(controller.moveset.customBackDash);
            }
        }
    }

    public override void MyExecuteSequence()
    {
        if (controller.hitWhileInvincible && relatedMoves.Length > 0 && controller.seq >= 2)
        {
            //controller.Flip();
            relatedMoves[0].isCharging = true;
        }
        base.MyExecuteSequence();
    }
}
