﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheathUnsheath : MoveScript
{
    public int duration;
    public bool isUnsheath;
    public int unsheathFrame;

    public override void MyStartSequence()
    {
        if (isUnsheath) 
        {
            animatorParameterName = controller.moveset.unsheathAnimatorParameter;
            variantAnimatorParameterName = controller.moveset.unsheathAnimatorParameter;
        }
        else
        {
            animatorParameterName = controller.moveset.sheathAnimatorParameter;
            variantAnimatorParameterName = controller.moveset.sheathAnimatorParameter;
        }
        animatorParameterIndex = Animator.StringToHash(animatorParameterName);
        variantAnimatorParameterIndex = Animator.StringToHash(variantAnimatorParameterName);


    }
    // Start is called before the first frame update
    public override void MyExecuteSequence()
    {
        if (controller.seq < duration)
        {
            controller.seq++;
        }
        else
        {
            itsTimeToStopTheSequence = true;
        }

        if (controller.activeWeaponSprite != null)
        {
            controller.activeWeaponSprite.SetActive(true);
            if (isUnsheath)
            {
                if (controller.seq < unsheathFrame)
                    controller.activeWeaponSprite.SetActive(false);
            }
            else
            {
                if (controller.seq > unsheathFrame)
                {
                    controller.activeWeaponSprite.SetActive(false);
                }
            }
        }
        
    }
}
