﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemEffect : ItemWrapper
{
    
    public void OnUse(Inventory inventory)
    {
        InitializeItem(inventory);
        MyOnUse();
    }

    protected virtual void MyOnUse ()
    {

    }
}
