﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class NewInputCheck : PlayerInputCheck
{
    //Keyboard keyboard;
    public PlayerControls controls;
    public int interactCounter;

    //public PlayerInput controls;
    public float hor;
    //public bool horizontalWasZeroWhenIPressedTheButton;
    
    //public InputAction attack1Action, attack2Action, special1Action, special2Action, special3Action, dashAction, jumpAction;
    
    private void Awake()
    {
        //keyboard = InputSystem.GetDevice<Keyboard>();
        //controls.Player.Attack1.performed += ctx => SetBuffers();
        //controls = new InputMaster();
        //InputSystem.pollingFrequency = 300;
        
    }

    public void InitializeControls()
    {
        controls.Player.Horizontal.performed += ctx => hor = (ctx.ReadValue<float>());

    }

    private void MoveHorizontal(float hor)
    {
        print("muoviti");
        if (hor < 0)
            player.horizontal = -0.5f;
        if (hor < -0.5f)
            player.horizontal = -1f;
        if (hor > 0)
            player.horizontal = 0.5f;
        if (hor > 0.5f)
            player.horizontal = 1f;
        if (hor == 0)
            player.horizontal = 0;
    }


    public override void SetBuffers()
    {
        
        if (player.pm.inputsEnabled == false)
        {
            //print("bufferssss " + player.gameObject);
            player.FlushBuffers();
            return;
        }
        //if (!player.currentlyUsingJoystick)
        {
            //controls.controlSchemes[0].
            player.horizontal = hor;
            //player.vertical = hor;
            //InputSystem.Update();
            InputSystem.pollingFrequency = 1/Time.deltaTime;
            //print("polling: " + InputSystem.pollingFrequency);

            float ver = controls.Player.Vertical.ReadValue<float>();
            if (ver < 0)
                player.vertical = -1;
            if (ver > 0)
                player.vertical = 1;
            if (ver == 0)
                player.vertical = 0;

            //float hor = controls.Player.Horizontal.ReadValue<float>();
            //print("hor: " + hor + " " + player.gameObject);
            /*
            if (hor < 0)
                player.horizontal = -0.5f;
            if (hor < -0.5f)
                player.horizontal = -1f;
            if (hor > 0)
                player.horizontal = 0.5f;
            if (hor > 0.5f)
                player.horizontal = 1f;
            if (hor == 0)
                player.horizontal = 0;
*/
            /*
            if (Input.GetAxisRaw(player.horizontalmovementCMD) < -0.5)
                player.horizontal = -1f;
            if (Input.GetAxisRaw(player.horizontalmovementCMD) > 0)
                player.horizontal = 0.5f;
            if (Input.GetAxisRaw(player.horizontalmovementCMD) > 0.5)
                player.horizontal = 1f;
            if (Input.GetAxisRaw(player.horizontalmovementCMD) == 0)
                player.horizontal = 0;
            */

            //provvisorio, da togliere
            if (player.horizontal != 0 && player.vertical == -1)
                player.horizontal /= 3;



            if (ver > 0 && player.verticalprevious == 0)
            {
                player.upPressed = true;
            }
            else
                player.upPressed = false;

            if (ver < 0 && player.verticalprevious == 0)
            {
                player.downpressed = true;
            }
            else
                player.downpressed = false;

            if (hor > 0 && player.horizontalprevious == 0)
            {
                player.rightpressed = true;
            }
            else
                player.rightpressed = false;

            if (hor < 0 && player.horizontalprevious == 0)
            {
                player.leftpressed = true;
            }
            else
                player.leftpressed = false;

            if (ver == 0 && player.verticalprevious == 1)
            {
                player.upreleased = true;
            }
            else
                player.upreleased = false;

            if (ver == 0 && player.verticalprevious == -1)
            {
                player.downreleased = true;
            }
            else
                player.downreleased = false;

            if (hor == 0 && player.horizontalprevious == 1)
            {
                player.rightreleased = true;
            }
            else
                player.rightreleased = false;

            if (hor == 0 && player.horizontalprevious == -1)
            {
                player.leftreleased = true;
            }
            else
                player.leftreleased = false;

            player.verticalprevious = player.vertical;
            player.horizontalprevious = player.horizontal;

            /*
            if (controls.Player.Dash.triggered)
            {
                //FlushBuffers ();
                player.dashpressed = true;
                //if (player.status.myPhysicsStatus == Status.physicsStatus.airborne || player.currentMove == player.animations[4])
                //player.dashBuffer = player.bufferLength;

                //if (runToggleTimer < timeToRunToggle) 


                //runToggleTimer = 0;
            }
            */
            
            
             player.holdingDash = false;


            if (controls.Player.Run.ReadValue<float>() > 0)
            {
                
                    player.holdingDash = true;
                
            }

            player.holdingBlock = false;
            if (controls.Player.BlockHold.ReadValue<float>() > 0)
            {

                player.holdingBlock = true;

            }



            if (controls.Player.Target.ReadValue<float>() > 0)
            {
                //FlushBuffers ();
                if (!player.targetpressed)
                {
                    if (player.horizontal == 0)
                    {
                        if (player.facingRight)
                            player.rightDirectionLock = true;
                        else
                            player.leftDirectionLock = true;
                    }
                    else
                    {
                        if (player.horizontal > 0)
                        {
                            player.rightDirectionLock = true;
                        }
                        else
                        {
                            player.leftDirectionLock = true;
                        }
                    }
                }
                player.targetpressed = true;
                //player.targetBuffer = player.bufferLength;
            }
            else
            {
                player.targetpressed = false;
            }
            if (controls.Player.Attack1.triggered)
            {
                //print("attaaack");
                //FlushBuffers ();
                player.atk1pressed = true;
                player.atk1Buffer = 1*player.bufferLength;
                atk1miniBuffer = simultaneusPressLeniency;
            }
            if (controls.Player.Special1.triggered)
            {
                //FlushBuffers ();
                if (interactCounter < player.pm.promptManager.interactTimerDuration)
                {
                    //print("special1111");
                    player.special1pressed = true;
                    player.special1Buffer = player.bufferLength;
                }
            }

            if (controls.Player.Interact.ReadValue<float>() > 0)
            {
                if (interactCounter < player.pm.promptManager.interactTimerDuration)
                {
                    interactCounter++;
                }
            }
            else
                interactCounter = 0;

            if (controls.Player.Special2.triggered)
            {
                //FlushBuffers ();
                player.special2pressed = true;
                player.special2Buffer = player.bufferLength;
            }
            
            /*
            if (Input.GetButtonDown(player.special2CMD))
            {
                //FlushBuffers ();
                player.special2pressed = true;
                player.special2Buffer = player.bufferLength;
            }
            */
            if (Input.GetButtonDown(player.special3CMD))
            {
                //FlushBuffers ();
                player.special3pressed = true;
                player.special3Buffer = player.bufferLength;
            }
            if (Input.GetButtonDown(player.special4CMD))
            {
                //FlushBuffers ();
                player.special4pressed = true;
                player.special4Buffer = player.bufferLength;
            }
            if (controls.Player.Attack2.triggered)
            {
                //FlushBuffers ();
                player.atk2pressed = true;
                player.atk2Buffer = player.bufferLength;
                atk2miniBuffer = simultaneusPressLeniency;
            }
            //if (controls.Player.BlockHold.ReadValue<float>() > 0)
            
            {
                //FlushBuffers ();
                //player.blockpressed = true;
                
                //player.blockBuffer = 1;
            }
            if (controls.Player.Jump.triggered)
            {
                //print("jumpppp");
                //FlushBuffers ();
                player.jumppressed = true;
                player.jumpBuffer = player.bufferLength;
            }
            /*
            if (player.horizontal == 0)
            {
                if (controls.Player.Parry.triggered)
                {
                    //print("block trigger");
                    player.parryBuffer = player.bufferLength;
                    horizontalWasZeroWhenIPressedTheButton = true;
                }
                
                else if (player.holdingDash && player.parryBuffer <= 0)
                {
                    
                    player.blockBuffer = 1;
                }
                
            }
            */

            if (controls.Player.Parry.triggered)
            {
                //print("block trigger");
                player.parryBuffer = player.bufferLength;
            }

            if (Input.GetButtonUp(player.atk1CMD))
            {
                player.atk1released = true;
            }


            if (Input.GetButtonUp(player.atk2CMD))
            {
                player.atk2released = true;
            }

            if (Input.GetButtonUp(player.special1CMD))
            {
                player.special1released = true;
            }

            if (Input.GetButtonUp(player.special2CMD))
            {
                player.special2released = true;
            }

            if (Input.GetButtonUp(player.special3CMD))
            {
                player.special3released = true;
            }

            if (Input.GetButtonUp(player.special4CMD))
            {
                player.special4released = true;
            }

            if (Input.GetButtonUp(player.targetCMD))
            {
                player.targetreleased = true;
            }

            if (controls.Player.Dash.triggered)
            {
                //if (player.horizontal != 0 || !horizontalWasZeroWhenIPressedTheButton)
                {
                    player.dashreleased = true;
                    //player.dashBuffer = player.bufferLength;
                    //if (player.dashreleased)
                    {

                        if (player.framesHoldingDash > player.timeToRunToggle)
                        {
                            player.dashBuffer = 0;
                            //print("ZEro");
                        }
                        else //if (!horizontalWasZeroWhenIPressedTheButton)
                            player.dashBuffer = player.bufferLength;

                        player.runToggleTimer = 0;
                    }
                }

                /*
                else
                {
                    if (player.framesHoldingDash < player.timeToRunToggle)
                        player.blockBuffer = player.bufferLength;
                }
                */
                //horizontalWasZeroWhenIPressedTheButton = false;
            }

            if (player.runToggleTimer <= player.timeToRunToggle || player.horizontal == 0)
                player.runToggle = false;

            /*
            if (atk1miniBuffer > 0 && atk2miniBuffer > 0)
            {
                player.special2Buffer = player.bufferLength;
                atk1miniBuffer = 0;
                atk2miniBuffer = 0;
                player.atk1Buffer = 0;
                player.atk2Buffer = 0;
            }

            if (atk2miniBuffer > 0)
                atk2miniBuffer--;
            if (atk1miniBuffer > 0)
                atk1miniBuffer--;
            */

            if (Input.GetButtonUp(player.blockCMD))
            {
                player.blockreleased = true;
            }


            if (Input.GetButtonUp(player.jumpCMD))
            {
                player.jumpreleased = true;
            }

        }
    }
}

