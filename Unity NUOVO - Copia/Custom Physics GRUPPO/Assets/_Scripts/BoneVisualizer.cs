﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoneVisualizer : MonoBehaviour
{
    [Range(0f, 360f)]
    public float angle = 0f;
    [Range(0f, 10f)]
    public float length = 1f;
    
    public Color color = Color.white;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnDrawGizmos() {
        
        DrawBone(false);
    }

    private void OnDrawGizmosSelected()
    {
        DrawBone(true);
    }

    private void DrawBone(bool selected) {
        float totalAngle = angle + transform.eulerAngles.z;

        Vector2 start = new Vector2(transform.position.x, transform.position.y);
        Vector2 finish = new Vector2(start.x + length * Mathf.Cos(totalAngle * Mathf.Deg2Rad), start.y + length * Mathf.Sin(totalAngle * Mathf.Deg2Rad));
        Vector2 middleR = new Vector2(start.x + length / 5 * Mathf.Cos((totalAngle - 30f) * Mathf.Deg2Rad),
            start.y + length / 5 * Mathf.Sin((totalAngle - 30f) * Mathf.Deg2Rad));
        Vector2 middleL = new Vector2(start.x + length / 5 * Mathf.Cos((totalAngle + 30f) * Mathf.Deg2Rad),
            start.y + length / 5 * Mathf.Sin((totalAngle + 30f) * Mathf.Deg2Rad));

        Gizmos.color = selected ? Color.yellow : color;
        Gizmos.DrawLine(start, finish);
        Gizmos.DrawLine(start, middleR);
        Gizmos.DrawLine(start, middleL);
        Gizmos.DrawLine(finish, middleR);
        Gizmos.DrawLine(finish, middleL);
    }
}
