﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourInitializer : MonoBehaviour {

	public BehaviorOption[] behaviours;
	[HideInInspector]
	public Player controller;
	[HideInInspector]
	public BehaviourSelector bselector;

	public void InitializeBehaviours () {
		for (int i = 0; i < behaviours.Length; i++) {
			if (behaviours[i] != null)
			{
				behaviours[i].controller = controller;
				behaviours[i].bSelector = bselector;
				behaviours[i].BehaviourInitialization();
			}
		}
        if (bselector.onEngageBehaviour != null)
        {
            bselector.onEngageBehaviour.controller = controller;
            bselector.onEngageBehaviour.bSelector = bselector;
            bselector.onEngageBehaviour.BehaviourInitialization();
        }
	}
}
