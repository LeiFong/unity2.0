﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunScript : MoveScript {

    //public int staminaCost;
    //public float runVelocity = 5;
	[System.Serializable]
	public struct AccelerationPeriod
	{
		public int start;
		public int accelerationDuration;
		public float minVelocity;
		public float maxVelocity;
	}

	public AccelerationPeriod accelerationPeriod;
    
    /*
void OnEnable () {
    controller = transform.parent.transform.parent.GetComponent<Controller2D> ();
    status = transform.parent.transform.parent.GetComponent<Status> ();
    LoadFrameData ();
    controller.seq = 0;
}
*/


    public override void MyStartSequence()
    {
        if ((controller.facingRight && controller.horizontal < 0) || (!controller.facingRight && controller.horizontal > 0))
        {
            //controller.Flip();

        }
    }

    public override void MyStopSequence() {

        
        controller.seq = 0;
			//Debug.Log ("STOP!");
			status.mystatus = Status.status.neutral;
			controller.avoidingCharacters = false;
			//StopCoroutine ("Attack");
			//StartCoroutine (status.WaitBeforeRecharge());
			//controller.animator.SetBool ("dash", false);
			//controller.animator.SetBool ("neutral", true);
			//controller.animator.speed = 1;
			controller.currentMove = null;

	}

	public override void SetStatus()
	{


		status.mystatus = Status.status.dashing;
		//status.canRecharge = false;




	}

	public override void MyExecuteSequence ()
	{
        
			status.canRecharge = false;
		status.mystatus = Status.status.dashing;

		/*
		if (horizontalMovements.Length == 0)
		{
			float accelerationSegment = (-accelerationPeriod.minVelocity + accelerationPeriod.maxVelocity)
					/ (accelerationPeriod.start + accelerationPeriod.accelerationDuration);
			float vel = accelerationPeriod.minVelocity + Mathf.Max((controller.seq - accelerationPeriod.start) * accelerationSegment, 0);
			controller.targetVelocity.x = Mathf.Sign(controller.gameObject.transform.localScale.x) * Mathf.Min(accelerationPeriod.maxVelocity, vel);
		}
		*/
		
		controller.animator.SetBool(controller.walkForwardIndex, true);

		if (controller.seq == controller.minimumSprintFrames - 10)
			controller.animator.SetTrigger(controller.slowWalkTriggerIndex);
		
			float ws = controller.animator.GetFloat(controller.walkBlendIndex);
			if (ws < 1.3f)
			{
				ws += Mathf.Min(0.01f, 1.3f - ws);
				controller.animator.SetFloat(controller.walkBlendIndex, ws);
				

			}
			controller.animator.SetFloat(controller.walkspeedIndex, ws);
			controller.animator.SetBool(controller.walkingIndex, true);
		




		
		if (controller.seq < 500)
			controller.seq++;


		
		}
	}


