﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitstun : MoveScript {

	/*
	public int hitstun, blockstun;
	public float pushbackHit, pushbackBlock, horizontalJuggleVelocity, verticalJugglevelocity;

	public int damageOnHit;
	public int damageOnBlock;
	public int staminaCost;
	public int staminaDamageOnHit;
	public int staminaDamageOnBlock;
	public int hitstunType;
	public int blockstunType;

	public bool hit = false;
	public bool block = false;
	public bool juggle = false;
	*/

	//public followup [] followupsOnBlock, followupsOnHit;
	protected Vector2 enemyPosition;
	protected float positionDifference;
	protected float direction, pushbackHit;
    public int hitstun, minimumUntechableHitstunWindow;
    protected int pushbackDuration;
	protected int tempFlushFrames;

	bool hasParried = false;

	protected float blockstunPushbackDifference;
	public HurtboxHit hbh;
	//public FrameData enemyMove;
	public int framesToFlushBuffers; //i primi frame di hitstun in cui i buffer vengono svuotati; metti che schivi in ritardo una mossa, il roll non deve avvenire alla fine dell'htstun
    public string fromBehindAnimatorParameter; //il parametro usato dall'animatore in caso sono stato colpito da dietro
	public string fromFrontAnimatorParameter;
	public Status.status stunType; //IMPORTANTE: DA SETTARE NELL'EDITOR, SPECIFICA SE SI TRATTA DI BLOCKSTUN, HITSTUN, JUGGLE, ETC.
    [System.Serializable]
    public struct MoveTypeIndex
    {
        public moveType moveType;
        public int index;
    }

    public MoveTypeIndex[] techableMoves; //le mosse che puoi fare nella finestra di techable hitstun
	protected FrameData tempFD;
	//public int minimumUntechableHitstun;
	//public int techableHitstun;
	protected int counterHitAdditionalHitstun;

	protected override void MyOnEnable ()
	{
		//FD = new FrameData();
		//FD = Instantiate(FD);
		//print ("ciao");
		//hbh = controller.gameObject.GetComponentInChildren<HurtboxHit> ();
		animatorParameterName = fromFrontAnimatorParameter;

		//blockstunPushbackDifference = 12;

		tempFD = null;
	}

    public override void MyStartSequence()
    {
		tempFD = null;
		animatorParameterName = fromFrontAnimatorParameter;
		canBlockInterval.end = 999999;
		counterHitAdditionalHitstun = 0;
		hasParried = false;

		if (hbh == null)
        {
            hbh = controller.lastCollisionHurtbox.hurtbox;
            tempFD = controller.lastCollisionHurtbox.move.frameData;

            if (tempFD == null || hbh == null)
            {
                StopSequence();
                return;
            }
			enemyPosition = tempFD.isProjectile ? tempFD.projectileScript.transform.position : tempFD.owner.tr.position;
			positionDifference = Mathf.Sign(controller.tr.position.x - enemyPosition.x);

			controller.shaking = true;

			//print("hitstuntype: " + stunType);
			if (stunType == Status.status.hitstun1 || stunType == Status.status.hitstun2)
            {
				animatorParameterName = fromFrontAnimatorParameter;

				status.currentMeter += Mathf.RoundToInt(status.meterGainWhenHit * status.currentMeterGainMultiplier);
				if (status.currentMeter > status.maxMeter)
					status.currentMeter = status.maxMeter;

				//print("HIT: " + tempFD.gameObject + " " + tempFD.pushbackHit);
				hitstun = tempFD.currentHitProperties.hitstun;
				if (controller.lastCollisionHurtbox.move.counterhit)
				{
					hitstun += counterHitAdditionalHitstun;
					print("COUNTER");
				}
                if (tempFD.isProjectile)
                {
					if (tempFD.projectileWithDirection)
					{
						pushbackHit = tempFD.facingRight ? tempFD.currentHitProperties.pushbackHit : -tempFD.currentHitProperties.pushbackHit;
					}
					else
					{
						
						
						if (positionDifference > 0)
							pushbackHit = tempFD.currentHitProperties.pushbackHit;
						else
							pushbackHit = -tempFD.currentHitProperties.pushbackHit;
					}
                    /*
                    if ((controller.facingRight && positionDifference > 0) || (!controller.facingRight && positionDifference < 0))
                        pushbackHit = -tempFD.pushbackHit;
                    else
                        pushbackHit = tempFD.pushbackHit;
                        */

                }
                else
                {
                    //print("hitttt");
                    direction = tempFD.owner.facingRight ? 1 : -1;

					if (!tempFD.currentHitProperties.positionDependantPushback)
					{
						if (direction > 0)
							pushbackHit = tempFD.currentHitProperties.pushbackHit;
						else
							pushbackHit = -tempFD.currentHitProperties.pushbackHit;
					}
                    else
                    {
						
						pushbackHit = positionDifference > 0 ? tempFD.currentHitProperties.pushbackHit : -tempFD.currentHitProperties.pushbackHit;
                        
                    }
                    /*
                    if ((direction > 0 && controller.facingRight) || (direction < 0 && !controller.facingRight))
                        pushbackHit = -tempFD.pushbackHit;
                    else
                        pushbackHit = tempFD.pushbackHit;
                    */
                    if (stunType == Status.status.hitstun1)
                    {
                        if ((controller.facingRight && pushbackHit > 0) || (!controller.facingRight && pushbackHit < 0))
                        {
                            //print("da dietro");
                            //controller.animator.Play(fromBehindAnimatorParameter, 0, 0);
                            animatorParameterName = fromBehindAnimatorParameter;
                        }

						{
							//stretcho l'animazione per fittarla con la durata dell'hitstun
							animationSpeedIntervals = new AnimationSpeedInterval[2];
							animationSpeedIntervals[0].windowEnd = 5;
							animationSpeedIntervals[0].animationSpeed = 1;

							animationSpeedIntervals[1].windowEnd = hitstun - 34;
							animationSpeedIntervals[1].animationSpeed = (7f / ((float)Mathf.Max(1f, (hitstun - 39f))));

						}
					}
                    else if (stunType == Status.status.hitstun2)
                    {
						//questo è il getparried, e ogni arma ha un'animazione diversa; me la vado a prendere
						animatorParameterName = "long hitstun";//controller.moveset.getParriedAnimationParameter;
						//print("get parried: " + animatorParameterName);
					}
					/*
                    if (hitstun >= 60)
                    {
                        //animatorParameterName = "long hitstun";
                    }
                    else
					*/
					
                    

                    

                }


            }
            else if (stunType == Status.status.blockstun1 || stunType == Status.status.guardBreak)
            {
				hitstun = tempFD.currentHitProperties.hitstun + tempFD.currentHitProperties.blockstunDifference + 16;
				//blockstunPushbackDifference = tempFD.lastHit ? 100 / tempFD.pushbackDuration : 0;
				blockstunPushbackDifference = 0;

				status.currentMeter += Mathf.RoundToInt(status.meterGainOnParry * status.currentMeterGainMultiplier);
				if (status.currentMeter > status.maxMeter)
					status.currentMeter = status.maxMeter;

				controller.shaking = false;

				//print("block");
				if (tempFD.isProjectile)
				{

					if (tempFD.projectileWithDirection)
					{
						pushbackHit = tempFD.facingRight ? (tempFD.currentHitProperties.pushbackHit + blockstunPushbackDifference) : (-tempFD.currentHitProperties.pushbackHit - blockstunPushbackDifference);
					}
					else
					{


						/*
						if (positionDifference < 0)
							pushbackHit = -tempFD.pushbackHit;
						else
							pushbackHit = tempFD.pushbackHit;
							*/
						if ((direction > 0))// && controller.facingRight) || (direction < 0 && !controller.facingRight))
							pushbackHit = tempFD.currentHitProperties.pushbackHit + blockstunPushbackDifference;
						else
							pushbackHit = -tempFD.currentHitProperties.pushbackHit - blockstunPushbackDifference;
					}
				}
				else
				{
					direction = tempFD.owner.facingRight ? 1 : -1;
					if (!tempFD.currentHitProperties.positionDependantPushback)
					{
						if (direction > 0)
							pushbackHit = tempFD.currentHitProperties.pushbackHit + blockstunPushbackDifference;
						else
							pushbackHit = -tempFD.currentHitProperties.pushbackHit - blockstunPushbackDifference;
					}
					else
					{

						pushbackHit = positionDifference > 0 ? (tempFD.currentHitProperties.pushbackHit + blockstunPushbackDifference) : (-tempFD.currentHitProperties.pushbackHit - blockstunPushbackDifference);

					}
				}

				if (controller.lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.parry)
				{
					hitstun -= 16;
					hasParried = true;
					//if (!tempFD.kick && tempFD.owner.conditions.weaponBounceAnimation != null)
					if (!tempFD.currentHitProperties.kick)
					{
						hitstun = tempFD.owner.conditions.weaponBounceAnimation.duration - 16;
						pushbackHit /= 2.5f;

					}
					else
                    {
						hitstun += 16;
                    }

					//print("blockstun: " + tempFD.moveScript + " " + hitstun);

					/*
					if (tempFD.currentHitProperties.kick)
					{
						//hitstun += 10;
                    }
					else
						pushbackHit /= 3;
					*/


				}
				/*
				else
				{
					if (!tempFD.currentHitProperties.kick && tempFD.owner.conditions.weaponBounceAnimation != null)
					{
						hitstun = tempFD.owner.conditions.weaponBounceAnimation.duration - 16;
						//print("blockstun: " + tempFD.moveScript + " " + hitstun);

					}
					else
						hitstun = tempFD.currentHitProperties.hitstun;
					//print("blockstun BBB: " + hitstun);

				}
				*/


				if (stunType != Status.status.guardBreak)
				{
					if (controller.lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.parry && controller.moveset.parrySuccessAnimationParameter != "")
						animatorParameterName = controller.moveset.parrySuccessAnimationParameter;
					else
						animatorParameterName = controller.moveset.blockstunAnimationParameter;
				}
				else
				{
					animatorParameterName = "long hitstun";
					//hitstun = 100; //test

				}

				//controller.shaking = false;
				{
					//stretcho l'animazione per fittarla con la durata dell'hitstun
					animationSpeedIntervals = new AnimationSpeedInterval[2];
					animationSpeedIntervals[0].windowEnd = 15;
					animationSpeedIntervals[0].animationSpeed = 1;

					animationSpeedIntervals[1].windowEnd = hitstun - 34;
					animationSpeedIntervals[1].animationSpeed = (7f / ((float)Mathf.Max(1f, (hitstun - 49f))));

				}
				
            }

			direction = hbh.direction;
            pushbackDuration = tempFD.currentHitProperties.pushbackDuration;

			if (stunType != Status.status.guardBreak)
				minimumUntechableHitstunWindow = tempFD.currentHitProperties.minimumUntechableHitstunWindow;
			else
				minimumUntechableHitstunWindow = hitstun;


			if (controller.lastCollisionHurtbox.move.counterhit)
				minimumUntechableHitstunWindow += counterHitAdditionalHitstun;


			if (minimumUntechableHitstunWindow > hitstun) //da testare
				minimumUntechableHitstunWindow = hitstun - 1;

			if (stunType != Status.status.guardBreak)
				canBlockInterval.start = minimumUntechableHitstunWindow;
			else
				canBlockInterval.start = canBlockInterval.end;

			//if (controller.lastCollisionHurtbox.move.hitType != HurtboxHit.HitType.parry)
			if (stunType != Status.status.blockstun1)
			{
				tempFlushFrames = framesToFlushBuffers;
				if (minimumUntechableHitstunWindow - tempFlushFrames < 25)
				{
					tempFlushFrames = minimumUntechableHitstunWindow - 25;
				}
			}
			else
            {
				tempFlushFrames = framesToFlushBuffers;

				//tempFlushFrames = 0;
			}
			//hbh = controller.lastCollisionHurtbox.hurtbox;


			//if (tempFD.hitstop > 7)
			//controller.shaking = true;
			if ((stunType == Status.status.hitstun1 || stunType == Status.status.hitstun2))
            {
				//print("BLOOOOOOD " + controller.lastCollisionHurtbox.hurtbox + " " + direction);
				//GlobalHitSparkGenerator.Instance.GenerateBlood(3, controller.lastCollisionHurtbox.hurtbox.transform, 1.4f*direction, 0.5f);
				if (controller.hitsparkRetriever != null)
                {
					controller.hitsparkRetriever.GenerateSparksOnHit(3, controller.lastCollisionHurtbox.hurtbox.transform, direction, 1f);
                }
				else
                {
					print("no hitspark retriever");
                }
            }
			else if (stunType == Status.status.blockstun1 || stunType == Status.status.guardBreak)
            {
				if (controller.hitsparkRetriever != null)
				{
					controller.hitsparkRetriever.GenerateSparksOnBlock(30, controller.lastCollisionHurtbox.hurtbox.transform, direction, 1);
				}
				else
				{
					print("no hitspark retriever");
				}
			}
				//controller.hitstop = tempFD.hitstop;

				controller.lastCollisionHurtbox.hurtbox = null;
            controller.lastCollisionHurtbox.move.frameData = null;
        }

		animatorParameterIndex = Animator.StringToHash(animatorParameterName);

	}

	public override void SetStatus ()
	{
		

		status.mystatus = stunType;
		//status.canRecharge = false;


		

	}

	public override void MyStopSequence() {

		//print ("fine hitstun");
        //print (controller.projectileOffenders.Count);
        /*
		for (int i = 0; i < controller.projectileOffenders.Count; i++) {
			if (controller.projectileOffenders [i].objectsHit.Contains (controller.gameObject)) {
				controller.projectileOffenders [i].objectsHit.Remove (controller.gameObject);
				}
			}
		controller.projectileOffenders.Clear ();
*/
        //controller.ResetProjectileOffenders ();
        controller.shaking = false;
		controller.seq = 0;
		status.mystatus = Status.status.neutral;
		hbh = null;
		controller.hasParried = false;

		//TimeVariables.StopWatch();
		//enemyMove = null;
		//controller.animator.SetBool(animatorParameterName, false);
		//print ("WHAT");
		//controller.lastCollisionHurtbox.move = null;
		//controller.lastCollisionHurtbox.hurtbox = null;
			
		//controller.animator.SetBool ("hitstun", false);
		//controller.animator.SetBool ("blockstun", false);

		//controller.animator.SetBool ("neutral", true);



		//StartCoroutine (status.WaitBeforeRecharge ());
		//controller.currentMove = null;
		//controller.animator.speed = 1;
	}

	public override void MyExecuteSequence()
	{

		if (hasParried)
		{
			controller.hasParried = true;
			//print("hasparrieddddd");
		}
		else
        {
			controller.hasParried = false;
        }

		if (controller.seq == 0)
		{
			//print ("HITSTUN " + Time.frameCount);
			//hbh = controller.hurtboxesHitList [0];
			//hbh = controller.lastCollisionHurtbox.hurtbox;
			//print (hbh);
			//controller.seq = 1; //questo perchè l'hitstun partirà sempre un frame in ritardo rispetto all'attaccante



		}
		if (tempFlushFrames > 0)
		{

			if (controller.seq <= tempFlushFrames)
			{
				//controller.FlushBuffers();
				controller.FlushDefensiveBuffers(); //test

			}
		}


		//if (status.mystatus == Status.status.hitstun1 || status.mystatus == Status.status.hitstun2) {
		//if (controller.seq < hitstun)
		{

			//print ("pushback duration: "+ hbh.offenderMoveSet.pushbackDuration);
			//if (controller.hitstop <= 0)
			{
				if (controller.seq < pushbackDuration)
				{

					controller.targetVelocity.x = pushbackHit;
					//print (controller.targetVelocity.x);
				}
				else
				{
					controller.targetVelocity.x = 0;
					//print ("OOOOO");
				}

			}

			//print (hbh.pushbackHit);
			//print (hbh.direction);
			//print (controller.targetVelocity.x);



			status.mystatus = stunType;
			//status.canRecharge = false;


			//setta i dash e i jump a usabile dopo che la untechableHitstunWindow è finita
			if (controller.seq >= minimumUntechableHitstunWindow - 1)
			{
				
				for (int i = 0; i < techableMoves.Length; i++)
				{
					if (techableMoves[i].moveType == moveType.animation)
					{
						conditions.animations[techableMoves[i].index].usable = true;
					}
					else
					{
						conditions.moves[techableMoves[i].index].usable = true;
					}
				}
			}

			//if (controller.hitstop == 0 && !resumingFromHitstop)
			controller.seq++;


		}

		//else if (status.mystatus == Status.status.blockstun1 || status.mystatus == Status.status.blockstun2) {




		//else {
		if (controller.seq >= hitstun)
		{
			//StopSequence ();
			//moveIsFinished = true;
			//print ("STOP ");
			itsTimeToStopTheSequence = true;
			//hbh.hit = false;
			//hbh.block = false;
			//hbh.juggle = false;

		}
	}

	
}
