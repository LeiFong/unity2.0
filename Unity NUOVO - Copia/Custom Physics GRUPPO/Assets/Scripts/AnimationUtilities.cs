﻿using UnityEngine;
using System.Collections;

public class AnimationUtilities : MonoBehaviour {
	public SpriteRenderer ArmL, ArmR, LowerArmL, LowerArmR, LegL, LegR, LowerLegL, LowerLegR, WeaponR, WeaponL;
	public int ArmLDefaultLayer, LowerArmLDefaultLayer, ArmRDefaultLayer, LowerArmRDefaultLayer, LegLDefaultLayer, 
	LowerLegLDefaultLayer, LegRDefaultLayer, LowerLegRDefaultLayer, WeaponRDefaultLayer, WeaponLDefaultLayer;

	public int desiredFrames = 0;

	public Animator anim;

	void OnEnable () {
		anim = GetComponent<Animator> ();
	}
	// Use this for initialization
	void Start () {
		/*
		ArmLDefaultLayer = 50;
		LowerArmLDefaultLayer = 51;
		ArmRDefaultLayer = 10;
		LowerArmRDefaultLayer = 11;
		LegLDefaultLayer = 40; 
		LowerLegLDefaultLayer = 41;
		LegRDefaultLayer = 20;
		LowerLegRDefaultLayer=21;
		WeaponRDefaultLayer=9;
		WeaponLDefaultLayer=49;
		*/
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetSequenceLength(int actual) {
		/*
		float factual = actual;
		float fdesired = desiredFrames;
		anim.speed = factual / fdesired;
		*/
	}

	public void ResetSpriteLayer(){
		//anim.speed = 1;
		//print ("ciao");
		ArmLChangeLayer (ArmLDefaultLayer);
		LowerArmLChangeLayer (LowerArmLDefaultLayer);
		ArmRChangeLayer (ArmRDefaultLayer);
		LowerArmRChangeLayer (LowerArmRDefaultLayer);
		LegLChangeLayer (LegLDefaultLayer);
		LowerLegLChangeLayer (LowerLegLDefaultLayer);
		LegRChangeLayer (LegRDefaultLayer);
		LowerLegRChangeLayer (LowerLegRDefaultLayer);
		WeaponLChangeLayer (WeaponLDefaultLayer);
		WeaponRChangeLayer (WeaponRDefaultLayer);

	}

	public void ArmLChangeLayer(int v){
		ArmL.sortingOrder = v;
	}
	public void ArmRChangeLayer(int v){
		ArmR.sortingOrder = v;
	}
	public void LowerArmLChangeLayer(int v){
		LowerArmL.sortingOrder = v;
	}
	public void LowerArmRChangeLayer(int v){
		LowerArmR.sortingOrder = v;
	}
	public void LegLChangeLayer(int v){
		LegL.sortingOrder = v;
	}
	public void LegRChangeLayer(int v){
		LegR.sortingOrder = v;
	}
	public void LowerLegLChangeLayer(int v){
		LowerLegL.sortingOrder = v;
	}
	public void LowerLegRChangeLayer(int v){
		LowerLegR.sortingOrder = v;
	}
	public void WeaponLChangeLayer(int v){
		WeaponL.sortingOrder = v;
	}
	public void WeaponRChangeLayer(int v){
		WeaponR.sortingOrder = v;
	}
}
