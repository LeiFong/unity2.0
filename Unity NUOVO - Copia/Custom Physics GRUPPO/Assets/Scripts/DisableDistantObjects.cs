﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableDistantObjects : MonoBehaviour
{
    public Transform[] transforms;
    public float maxDistance;
    public bool checkAllObjects; //se vera, il test della distanza viene fatto su ogni oggetto della lista; altrimenti, la distanza viene controllata solo su questo oggetto

    public Transform cameraPosition;
    public bool turnedOff;
    bool firstFrame = true;

    [System.Serializable]
    public struct EnableElement
    {
        public bool enableCheck;
        public Transform transform;
    }
    public EnableElement [] enableElements;// = new List<EnableElement>();

    private void Start()
    {
        turnedOff = false;
        cameraPosition = FindObjectOfType<CameraMovement>().transform;
        firstFrame = true;
        if (checkAllObjects)
            CreateEnablingList();
    }

    void CreateEnablingList ()
    {
        enableElements = new EnableElement[transforms.Length];
        for (int i = 0; i < enableElements.Length; i++)
        {
            EnableElement newElem = new EnableElement
            {
                transform = transforms[i],
                enableCheck = true
            };
            enableElements[i] = newElem;
        }
    }

    private void Update()
    {
        if (checkAllObjects)
        {
            if (firstFrame)
            {
                for (int i = 0; i < enableElements.Length; i++)
                {

                    enableElements[i].enableCheck = enableElements[i].transform.gameObject.activeInHierarchy;

                }
            }
            firstFrame = false;

            for (int i = 0; i < enableElements.Length; i++)
            {
                if (enableElements[i].transform.gameObject.activeInHierarchy)
                {
                    enableElements[i].enableCheck = true;
                }

                if (enableElements[i].enableCheck)
                {
                    if (Mathf.Abs(cameraPosition.position.x - enableElements[i].transform.position.x) < maxDistance &&
                        Mathf.Abs(cameraPosition.position.y - enableElements[i].transform.position.y) < maxDistance)
                    {
                        print("enabbb");
                        enableElements[i].transform.gameObject.SetActive(true);
                    }
                    else
                    {
                        enableElements[i].transform.gameObject.SetActive(false);
                        print("dsable");
                    }
                }
            }
        }
        else
        {
            if (Mathf.Abs(cameraPosition.position.x - transform.position.x) < maxDistance &&
                        Mathf.Abs(cameraPosition.position.y - transform.position.y) < maxDistance)
            {
                if (turnedOff == true)
                {
                    print("enabbbbble");

                    for (int i = 0; i < transforms.Length; i++)
                    {
                        transforms[i].gameObject.SetActive(true);
                    }
                }
                turnedOff = false;
            }
            else
            {
                if (turnedOff == false)
                {
                    print("disabbbble");
                    for (int i = 0; i < transforms.Length; i++)
                    {
                        transforms[i].gameObject.SetActive(false);
                    }
                }
                turnedOff = true;
            }
        }
    }
}
