﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpearActivationConditions : StandardActivationConditions
{
    public ParticleSystemAnimator particleSystemAnimator;
    public bool sniperGaming;
    Status.Stances previousStance;

    public override void LoadMoveInfo()
    {
        base.LoadMoveInfo();
        sniperGaming = false;

    }
    protected override void CustomCheckConditions()
    {

        if (controller.enemySpecializer.isAI)
            return;

        if (sniperGaming && (previousStance != status.currentStance || (controller.seq == 2 && (status.mystatus == Status.status.hitstun1 || status.mystatus == Status.status.hitstun2 
            || status.mystatus == Status.status.juggle
            || status.mystatus == Status.status.grabbed || status.mystatus == Status.status.dead || 
            controller.currentMove == moves[32].move))))
        {
            sniperGaming = false;
            particleSystemAnimator.SetActiveCategory("Spear");
            particleSystemAnimator.SetActiveIndex(2);
            particleSystemAnimator.ParticleStart();
            particleSystemAnimator.SetActiveIndex(1);
            particleSystemAnimator.ParticleStop();
        }



        //ATTACCHI CARICATI
        if (controller.currentMove == moves[2].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                moves[2].move.isCharging = true;
            }
            else
                moves[2].move.isCharging = false;


        }
        else if (controller.currentMove == moves[4].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                moves[4].move.isCharging = true;
               

            }
            else
            {
                moves[4].move.isCharging = false;
                
            }
        }
        else if (controller.currentMove == moves[24].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                moves[24].move.isCharging = true;
            }
            else
                moves[24].move.isCharging = false;
        }
        else if (controller.currentMove == moves[30].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                moves[30].move.isCharging = true;
            }
            else
                moves[30].move.isCharging = false;
        }
        else if (controller.currentMove == moves[26].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                moves[26].move.isCharging = true;
            }
            else
                moves[26].move.isCharging = false;
        }
        else if (controller.currentMove == moves[27].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                moves[27].move.isCharging = true;
            }
            else
                moves[27].move.isCharging = false;
        }
        else if (controller.currentMove == moves[7].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                moves[7].move.isCharging = true;
            }
            else
                moves[7].move.isCharging = false;
        }
        else if (controller.currentMove == moves[10].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                moves[10].move.isCharging = true;
            }
            else
                moves[10].move.isCharging = false;
        }
        else if (controller.currentMove == moves[32].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                moves[32].move.isCharging = true;
            }
            else
                moves[32].move.isCharging = false;
        }
        else if (controller.currentMove == moves[13].move)
        {
            if (controller.inputCheck.controls.Player.Attack1Charged.ReadValue<float>() > 0)
            {
                moves[13].move.isCharging = true;
            }
            else
                moves[13].move.isCharging = false;
        }

        if (controller.hitstop <= 0)
        {
            if (controller.atk1Buffer > 0)
            {
                if (CheckUsability(moves[2]))
                {
                    controller.StartNewMove(moves[2].move);
                    controller.atk1Buffer = 0;
                }
                if (CheckUsability(moves[6]))
                {
                    controller.StartNewMove(moves[6].move);
                    controller.atk1Buffer = 0;
                }
                if (CheckUsability(moves[26]))
                {
                    controller.StartNewMove(moves[26].move);
                    controller.atk1Buffer = 0;
                }
                if (CheckUsability(moves[30]))
                {
                    controller.StartNewMove(moves[30].move);
                    controller.atk1Buffer = 0;
                }

                if (status.currentStance == Status.Stances.slow)
                {
                    if (status.currentHeatAmount < status.heatGaugeThreshold)
                    {
                        if (CheckUsability(moves[4]))
                        {
                            controller.StartNewMove(moves[4].move);
                            controller.atk1Buffer = 0;
                        }
                    }
                    else
                    {
                        if (CheckUsability(moves[24]))
                        {
                            controller.StartNewMove(moves[24].move);
                            controller.atk1Buffer = 0;
                        }
                    }

                    if (CheckUsability(moves[7]))
                    {
                        controller.StartNewMove(moves[7].move);
                        controller.atk1Buffer = 0;
                    }
                }
                else
                {
                    
                        if (status.currentHeatAmount < status.heatGaugeThreshold)
                        {
                            if (CheckUsability(moves[10]))
                            {
                                controller.StartNewMove(moves[10].move);
                                controller.atk1Buffer = 0;
                            }
                        }
                        else
                        {
                            if (CheckUsability(moves[27]))
                            {
                                controller.StartNewMove(moves[27].move);
                                controller.atk1Buffer = 0;
                            }
                        }
                    
                    if (CheckUsability(moves[13]))
                    {
                        controller.StartNewMove(moves[13].move);
                        controller.atk1Buffer = 0;
                    }
                }
            }
            if (controller.atk2Buffer > 0)
            {
                if (CheckUsability(moves[28]))
                {
                    controller.StartNewMove(moves[28].move);
                    controller.atk2Buffer = 0;
                }
                if (status.currentStance == Status.Stances.slow)
                {
                    if (CheckUsability(moves[5]))
                    {
                        controller.StartNewMove(moves[5].move);
                        controller.atk2Buffer = 0;
                    }

                }
                else
                {
                    if (!sniperGaming)
                    {
                        if (CheckUsability(moves[9]))
                        {
                            controller.StartNewMove(moves[9].move);
                            controller.atk2Buffer = 0;
                        }
                    }
                    else
                    {
                        if (CheckUsability(moves[32]))
                        {
                            controller.StartNewMove(moves[32].move);
                            controller.atk2Buffer = 0;
                        }
                    }
                   
                }
            }
        }

        previousStance = status.currentStance;
    }
}
