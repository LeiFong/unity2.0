﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LedgeGrab : MoveScript {

	public int moveDuration = 40;
	private Collider2D [] hitBuffer = new Collider2D[30];
	public Transform ledgeGrabPosition;
    public Transform lineOfSightOrigin;
	public float ledgeGrabCheckDistance;
	public LayerMask platformLayerMask;
	public bool hasHit = false;
	public string forwardHangingAnimationParameter;
	public string neutralHangingAnimationParameter;
	public int hangingFrames = 30;
	public float horizontalOffset = 0;
	public LedgeClimb ledgeClimb;
	private float savedHorizontalVelocity = 0;
	//private Vector2 lineOfSight;
	Vector2 topRight, topLeft;
	public float ledgeGrabbingFrames = 10;
	private float verticalDistance = 0;
    public Collider2D climbedPlatform;
    GenericWallMover movingPlatform;
    Vector2 currentLedgeGrabPosition;
    private Vector2 hitPosition;
    private string sheathAnimatorParameter, unsheathAnimatorParameter;

    public override void MyStartSequence()
    {
        canWalkWhenAirborne = true;
        controller.movingPlatform = null;
        enableHurtboxChanging = false;
        sheathAnimatorParameter = controller.moveset.sheathAnimatorParameter;
        unsheathAnimatorParameter = controller.moveset.unsheathAnimatorParameter;
        
    }

    protected override void MyOnEnable ()
	{
		/*
		topRight = new Vector2 (controller.myCollider.transform.position.x + controller.myCollider.bounds.max.x, controller.myCollider.transform.position.y +
			controller.myCollider.bounds.max.y);
		topLeft = new Vector2 (controller.myCollider.transform.position.x + controller.myCollider.bounds.min.x, controller.myCollider.transform.position.y +
			controller.myCollider.bounds.max.y);
			*/
		horizontalOffset = Mathf.Abs(controller.verticalCollider.transform.position.x + controller.verticalCollider.bounds.extents.x - ledgeGrabPosition.position.x);
        //if (status.faction == 0)
          //  print("horoffset: " + horizontalOffset);
        //horizontalOffset = Mathf.Abs(controller.transform.position.x - ledgeGrabPosition.position.x);

        platformLayerMask |= 1 << controller.platformLayerIndex;
        platformLayerMask |= 1 << controller.defaultLayerIndex;
        platformLayerMask |= 1 << controller.movingPlatformLayerIndex;


        


        ledgeClimb = relatedMoves [0].GetComponent<LedgeClimb> ();
        ledgeClimb.platformClimbLayerMask |= 1 << controller.platformLayerIndex;
        ledgeClimb.platformClimbLayerMask |= 1 << controller.movingPlatformLayerIndex;
        ledgeClimb.CF.useTriggers = false;
		//ledgeClimb.CF.SetLayerMask(ledgeClimb.ledgeClimbLayerMask);
		ledgeClimb.CF.useLayerMask = true;
		//lineOfSight.x = Mathf.Abs(controller.transform.position.x - ledgeGrabPosition.position.x);;
        //lineOfSight.y =  Mathf.Abs(controller.transform.position.y - ledgeGrabPosition.position.y);
        //lineOfSight.y = 0;
        //lineOfSight.y = Mathf.Abs(controller.transform.position.y - ledgeGrabPosition.position.y);

		//ledgeClimb = GetComponentInChildren<LedgeClimb> ();

	}

	public override void MyExecuteSequence ()
	{
		//Debug.DrawRay (new Vector2(controller.transform.position.x, controller.transform.position.y), lineOfSight, Color.white);
		//print (controller.seq);

        //all'inizio dell'animazione
		//if (!hasHit && controller.seq == 0)
			//Debug.DrawRay (ledgeGrabPosition.position, Vector2.right * 100, Color.blue, 2);
		
			//Debug.DrawRay (new Vector2(controller.transform.position.x, ledgeGrabPosition.position.y), lineOfSight, Color.green);
		status.mystatus = Status.status.stuckinanimation;
        

        //controller.collisions.isIgnoringPlatforms = 100;
        /*
		int count = controller.ledgeGrabber.OverlapCollider (controller.platformCheckerCF, hitBuffer);
		for (int i = 0; i < count; i++) {
			if (hitBuffer [i].gameObject.layer == LayerMask.NameToLayer ("Platform") && hitBuffer[i].tag != "Stairs") {
				controller.StartNewMove(controller.animations[18]);

				break;
			}


		}
		*/




        if (hasHit) { 
			controller.isAffectedByGravity = false;
            canFlip = false;
            canWalkWhenAirborne = false;
            controller.activeWeaponSprite.SetActive(false);
            //print("no more flipping");
            if (controller.seq < ledgeGrabbingFrames)
            {
                //print (controller.seq);
                //print("vertical distance: " + verticalDistance);
                
                //ATTUALE, DA TESTARE
				controller.targetVelocity.x = controller.facingRight ? (horizontalOffset / ledgeGrabbingFrames) / Time.fixedDeltaTime
					: -(horizontalOffset / ledgeGrabbingFrames) / Time.fixedDeltaTime;
				controller.velocity.y = -((verticalDistance / ledgeGrabbingFrames) / Time.fixedDeltaTime);
                controller.avoidingCharacters = true;
                canWalkWhenAirborne = false;

                if (movingPlatform != null)
                {
                    controller.movingPlatform = movingPlatform;

                    //print("mv");
                    //if (!movingPlatform.publicL.Contains(controller.verticalCollider))
                    //  movingPlatform.publicL.Add(controller.verticalCollider);
                }
                //status.myPhysicsStatus = Status.physicsStatus.airborne;
                //print("x: " + controller.targetVelocity + " y: " + controller.velocity.y);
                /*
                float newHorizontalOffset = - controller.verticalCollider.bounds.center.x + hitPosition.x;
                float newVerticalOffset = - (controller.verticalCollider.bounds.center.y - controller.verticalCollider.bounds.extents.y) + hitPosition.y;
                Vector2 normalizedVelocity = new Vector2(newHorizontalOffset, newVerticalOffset).normalized;

                controller.targetVelocity.x = normalizedVelocity.x *15;
                controller.velocity.y = normalizedVelocity.y * 15;
                Debug.DrawRay(controller.verticalCollider.bounds.center, normalizedVelocity);
                */
            }
            else
            {
                
                controller.targetVelocity.x = 0;
				controller.velocity.y = 0;
                controller.movingPlatform = null;
			}


		}



		if (hasHit && controller.seq >= ledgeGrabbingFrames) {
            /*
			int count = ledgeClimb.ledgeGrabber.OverlapCollider (controller.platformCheckerCF, hitBuffer);
			if (count != 0) {
				for (int i = 0; i < count; i++) {
					if (hitBuffer [i].gameObject.layer == LayerMask.NameToLayer ("Platform") && hitBuffer [i].tag != "Stairs") {
					*/
            //if (controller.seq == 1) {

            enableHurtboxChanging = true;

						if (controller.seq == ledgeGrabbingFrames) {
							if ((controller.facingRight && savedHorizontalVelocity > 0) || (!controller.facingRight && savedHorizontalVelocity < 0))
                                 controller.animator.Play (forwardHangingAnimationParameter, -1, 0f);
                                  
                             else
                                    controller.animator.Play (neutralHangingAnimationParameter, -1, 0f);
						}
            controller.climbedPlatform = climbedPlatform; //da testare

            //print ("WTF");
            //controller.velocity.y = 0;
            //break;
            /*			}

                    }
                } else {
                    print ("WTH");
                    //itsTimeToStopTheSequence = true;
                    //return;
                }
    */
            //controller.velocity.y = 0;
            if (controller.seq >= hangingFrames) {
				hasHit = false;
                ledgeClimb.climbedPlatform = climbedPlatform;
                ledgeClimb.hitPosition = hitPosition;
                if (climbedPlatform != null)
                {
                    if (climbedPlatform.gameObject.layer == controller.defaultLayerIndex) {
                        ledgeClimb.CF.SetLayerMask(ledgeClimb.ledgeClimbLayerMask);
                        ledgeClimb.isPlatform = false;
                        
                    }
                    else
                    {
                        ledgeClimb.CF.SetLayerMask(ledgeClimb.platformClimbLayerMask);
                        ledgeClimb.isPlatform = true;
                    }

                }

                controller.StartNewMove (relatedMoves[0]);
				return;

			}
		}
        if (controller.facingRight)
            horizontalOffset = Mathf.Abs(controller.verticalCollider.transform.position.x + controller.verticalCollider.bounds.extents.x - ledgeGrabPosition.position.x);
        else
            horizontalOffset = Mathf.Abs(controller.verticalCollider.transform.position.x - controller.verticalCollider.bounds.extents.x - ledgeGrabPosition.position.x);
        //print("hor: " + horizontalOffset);
        if (!hasHit && CheckLineOfSight()) {
            //print ("!hashit");
            float actualLedgeGrabCheckDistance = Mathf.Max((-controller.velocity.y + controller.skinWidth) * Time.fixedDeltaTime,
                ledgeGrabCheckDistance);
            //print("ledgecheck: " + actualLedgeGrabCheckDistance);
            //RaycastHit2D hit = Physics2D.Raycast (ledgeGrabPosition.position, Vector2.down, ledgeGrabCheckDistance, platformLayerMask);
            RaycastHit2D hit = Physics2D.Raycast (currentLedgeGrabPosition, Vector2.down, actualLedgeGrabCheckDistance, platformLayerMask);

            //if (hit.collider.gameObject.layer != LayerMask.NameToLayer ("Platform") && hit.collider.tag != "Grabbable Wall")
            //	return;

            float slopeAngle = Vector2.Angle (hit.normal, Vector2.up);
            
            if (hit && hit.distance > 0 && slopeAngle <= controller.maxSlopeAngle) { 
                 //&& (controller.currentGroundDistance - controller.skinWidth - hit.distance/ledgeGrabbingFrames) > (hit.distance + controller.skinWidth * (ledgeGrabbingFrames + 1))) {
                if (controller.facingRight)
                    horizontalOffset = Mathf.Abs(controller.verticalCollider.transform.position.x + controller.verticalCollider.bounds.extents.x - currentLedgeGrabPosition.x);
                else
                    horizontalOffset = Mathf.Abs(controller.verticalCollider.transform.position.x - controller.verticalCollider.bounds.extents.x - currentLedgeGrabPosition.x);

                //print("distanceeeeeeeeeeeeeeeeee: " + (hit.distance + controller.skinWidth * (ledgeGrabbingFrames + 1))
                //  + " grounddistance: " + (controller.currentGroundDistance - controller.skinWidth - hit.distance / ledgeGrabbingFrames));
                Debug.DrawRay(currentLedgeGrabPosition, Vector2.right * 100, Color.red, 3);
                Debug.DrawRay(currentLedgeGrabPosition, Vector2.down * hit.distance, Color.white, 3);
                //Debug.DrawRay(controller.verticalCollider.bounds.min, Vector2.down *
                  //  (controller.currentGroundDistance - controller.skinWidth - hit.distance/(ledgeGrabbingFrames - 1)), Color.white, 10);

                //controller.gameObject.transform.Translate (new Vector2 (0, -hit.distance));
                verticalDistance = hit.distance;

                climbedPlatform = hit.collider;
                hitPosition = hit.point;
                //da testare!!
                //controller.climbedPlatform = climbedPlatform;
                if (climbedPlatform.gameObject.layer == controller.movingPlatformLayerIndex)
                {
                    movingPlatform = climbedPlatform.GetComponent<GenericWallMover>();
                    if (movingPlatform != null)
                    {
                        controller.movingPlatform = movingPlatform;

                        //print("mv");
                        
                    }
                }
                //ledgeClimb.climbedPlatform = hit.collider;


                //controller.velocity.y = -((hit.distance / ledgeGrabbingFrames) / Time.fixedDeltaTime);
                Vector2 drawrayPosition = ledgeGrabPosition.position;
                drawrayPosition.y -= hit.distance;
                Debug.DrawRay(drawrayPosition, Vector2.left * 100, Color.green, 2);

                //itsTimeToStopTheSequence = true;
                hasHit = true;
				canWalkWhenAirborne = false;
				savedHorizontalVelocity = controller.targetVelocity.x;
                controller.velocity.y = 0;
                controller.targetVelocity.x = 0;
                //if (hasHit)
                {
                    controller.isAffectedByGravity = false;
                    canFlip = false;
                    canWalkWhenAirborne = false;
                    controller.avoidingCharacters = true;
                }
                //controller.targetVelocity.x = controller.facingRight ? (horizontalOffset / Time.fixedDeltaTime) : -(horizontalOffset / Time.fixedDeltaTime);

                //Mathf.Abs(topRight.x - ledgeGrabPosition.position.x);

                controller.seq = -1;
			}

		}
        /*
		if (hasHit) { 
			if (controller.seq < ledgeGrabbingFrames) {
				print (controller.seq);
				controller.targetVelocity.x = controller.facingRight ? (horizontalOffset / ledgeGrabbingFrames) / Time.fixedDeltaTime
				: -(horizontalOffset / ledgeGrabbingFrames) / Time.fixedDeltaTime;
				controller.velocity.y = -((verticalDistance / ledgeGrabbingFrames) / Time.fixedDeltaTime);
			}
			else {
				controller.targetVelocity.x = 0;
				controller.velocity.y = 0;
			}

 
		}
		*/
        //print (verticalDistance);


        

        if (controller.seq < moveDuration)
        {
            if (hasHit || controller.seq <= 100)
                controller.seq++;
        }
        else
        {
            itsTimeToStopTheSequence = true;
            
        }
	}

	private bool CheckLineOfSight () {
        currentLedgeGrabPosition = ledgeGrabPosition.position;
        //RaycastHit2D hit = Physics2D.Raycast (controller.transform.position, Vector2.up, Mathf.Abs(controller.transform.position.y - ledgeGrabPosition.position.y), controller.upwardCollisionMask);
        //if (hit.collider.tag != "Grabbable Wall")
        //	return false;
        //hit = null;
        Vector2 lineofsight = new Vector2(-(lineOfSightOrigin.position.x - ledgeGrabPosition.position.x), 
            (-lineOfSightOrigin.position.y + ledgeGrabPosition.position.y));
        //Debug.DrawLine(new Vector2(controller.transform.position.x, controller.transform.position.y + 1), new Vector2(ledgeGrabPosition.position.x,
          //  ledgeGrabPosition.position.y), Color.green, 10);
        //lineofsight.x = controller.facingRight ? lineofsight.x : -lineofsight.x;
        //print(lineofsight);
        //lineofsight.x = controller.facingRight ? lineofsight.x : -lineofsight.x;
        //RaycastHit2D hit = Physics2D.Raycast (new Vector2(controller.transform.position.x, ledgeGrabPosition.position.y), lineOfSight, lineOfSight.magnitude, controller.upwardCollisionMask);
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(lineOfSightOrigin.position.x, lineOfSightOrigin.position.y), 
            lineofsight, lineofsight.magnitude, controller.upwardCollisionMask);
        //Debug.DrawRay(lineOfSightOrigin.position, lineofsight, Color.yellow, 0.2f);


        if (hit)
        {
            //print("los");
            currentLedgeGrabPosition.x = hit.point.x + (controller.facingRight ? -0.05f : 0.05f);
            Debug.DrawRay(new Vector2(currentLedgeGrabPosition.x, currentLedgeGrabPosition.y), Vector2.down, Color.yellow, 2f);
            

            return true;

        }
		return true;
	}


    public override void SetStatus()
    {
        status.mystatus = Status.status.stuckinanimation;
    }

    public override void MyStopSequence ()
	{
		controller.collisions.isIgnoringPlatforms = 0;
        /*
        if (movingPlatform != null && movingPlatform.publicL.Contains(controller.verticalCollider))
        {
            movingPlatform.publicL.Remove(controller.verticalCollider);
        }
        */
        controller.activeWeaponSprite.SetActive(true);
        enableHurtboxChanging = false;
        controller.movingPlatform = null;
        movingPlatform = null;
        //print("ledgegrab, " + controller.movingPlatform);
        climbedPlatform = null;
        controller.climbedPlatform = null;
        verticalDistance = 0;
        //print ("reset!");
        currentLedgeGrabPosition = ledgeGrabPosition.position;
		canWalkWhenAirborne = true;
		savedHorizontalVelocity = 0;
        canWalkWhenAirborne = true;
		verticalDistance = 0;
		hasHit = false;
	}
}
