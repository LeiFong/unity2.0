﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EquipmentItem : Item
{
    public enum EquipmentSlot
    {
        chest, hands, legs, head
    }

    public int equipmentIndex;
    

}
