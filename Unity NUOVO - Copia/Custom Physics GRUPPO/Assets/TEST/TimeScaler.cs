﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaler : MonoBehaviour
{
    // Start is called before the first frame update
    public float timeScale = 1;
    void Start()
    {
        Time.timeScale = timeScale;
        Physics2D.autoSimulation = false;
    }

}
