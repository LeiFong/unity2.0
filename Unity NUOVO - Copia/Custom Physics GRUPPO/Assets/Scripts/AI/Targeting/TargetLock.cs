﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetLock : MonoBehaviour {

	public TargetableList list;
	private Vector3 originalScale;
	private SpriteRenderer myRenderer;
	private Vector3 targetPosition;

	// Use this for initialization
	void OnEnable () {
		list = transform.parent.GetComponent<TargetableList> ();
		originalScale = transform.localScale;
		myRenderer = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
        
        if (list.myTarget == null) {
			myRenderer.enabled = false;
			transform.localScale = new Vector3 (0,0,0);
			transform.position = transform.parent.position;
		}
		else {
			myRenderer.enabled = true;

			transform.localScale = originalScale;
			transform.position = list.targetLockPosition.position;
		}
        
    }
}
