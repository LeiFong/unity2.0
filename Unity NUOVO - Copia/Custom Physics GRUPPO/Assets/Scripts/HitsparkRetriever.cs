﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitsparkRetriever : MonoBehaviour
{

    private GlobalHitSparkGenerator generator;
    public GlobalHitSparkGenerator.SparkType hitSpark = GlobalHitSparkGenerator.SparkType.redBlood;
    public GlobalHitSparkGenerator.SparkType blockSpark = GlobalHitSparkGenerator.SparkType.sparks;
    
    

    public void InitializeSparks ()
    {
        generator = GlobalHitSparkGenerator.Instance ?? FindObjectOfType<GlobalHitSparkGenerator>();
        
    }

    public void GenerateSparksOnHit (int quantity, Transform position, float xScale, float yScale)
    {
        //print("hitsparks on hit: " + hitsparkOnHit);
        if (generator != null)
            generator.GenerateSparks(hitSpark, quantity, position, xScale, yScale);
        else
            print("NO HITSPARKSSS");
    }

    public void GenerateSparksOnBlock(int quantity, Transform position, float xScale, float yScale)
    {
        if (generator != null)
            generator.GenerateSparks(blockSpark, quantity, position, xScale, yScale);
        //else
          //  print("NO HITSPARKSSS");
    }
}
