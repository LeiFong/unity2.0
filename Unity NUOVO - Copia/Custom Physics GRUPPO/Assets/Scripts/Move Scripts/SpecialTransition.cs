﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialTransition : MoveScript
{
    public int specialTransitionFrame;

    public override void MyExecuteSequence()
    {
        if (controller.seq >= specialTransitionFrame)
        {
            controller.StartNewMove(conditions.specialMoves[controller.weaponSwitcher.equippedSpecialMove].move);
            return;
        }

        controller.seq++;
    }

}
