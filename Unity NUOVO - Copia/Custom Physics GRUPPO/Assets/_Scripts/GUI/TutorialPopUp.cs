﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPopUp : Event
{
    [Space(20)]
    public Animator animator;
    private int openAnim, closeAnim;

    protected override void InitializeEvent()
    {
        animator.keepAnimatorControllerStateOnDisable = true;
        openAnim = Animator.StringToHash("Open");
        closeAnim = Animator.StringToHash("Close");
    }

    protected override void OnPlayerIsInside()
    {
        animator.SetTrigger(openAnim);
        print("IN");
    }
    protected override void OnPlayerIsOutside()
    {
        animator.SetTrigger(closeAnim);
        print("OUT");
    }


}
