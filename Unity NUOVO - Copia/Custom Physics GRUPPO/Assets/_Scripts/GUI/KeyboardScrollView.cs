﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class KeyboardScrollView : MonoBehaviour
{
    [Range(0f, 0.5f)]
    public float upperOffset = 0.2f;
    [Range(0f, 0.5f)]
    public float lowerOffset = 0.2f;
    [Range(1f, 100f)]
    public float scrollSpeed = 30f;

    private float targetY;
    RectTransform scrollRectTransform;
    RectTransform contentPanel;
    RectTransform selectedRectTransform;
    GameObject lastSelected;

    void Start()
    {
        
        scrollRectTransform = GetComponent<RectTransform>();
        contentPanel = GetComponent<ScrollRect>().content;
        targetY = contentPanel.anchoredPosition.y;
        
        
    }

    void Update()
    {

        TravelToTargetPositionY(targetY);

        // Get the currently selected UI element from the event system.
        GameObject selected = EventSystem.current.currentSelectedGameObject;

        // Return if there are none.
        if (selected == null)
        {
            return;
        }
        // Return if the selected game object is not inside the scroll rect.
        if (selected.transform.parent != contentPanel.transform)
        {
            return;
        }
        // Return if the selected game object is the same as it was last frame,
        // meaning we haven't moved.
        if (selected == lastSelected)
        {
            return;
        }

        // Get the rect tranform for the selected game object.
        selectedRectTransform = selected.GetComponent<RectTransform>();
        // The position of the selected UI element is the absolute anchor position,
        // ie. the local position within the scroll rect + its height if we're
        // scrolling down. If we're scrolling up it's just the absolute anchor position.
        //float selectedPositionY = Mathf.Abs(selectedRectTransform.anchoredPosition.y) + selectedRectTransform.rect.height;
        float selectedPositionY = selectedRectTransform.anchoredPosition.y;


        // The upper bound of the scroll view is the anchor position of the content we're scrolling.
        //float scrollViewMinY = contentPanel.anchoredPosition.y;
        float scrollViewUpperY = scrollRectTransform.anchoredPosition.y;
        // The lower bound is the anchor position + the height of the scroll rect.
        //float scrollViewMaxY = contentPanel.anchoredPosition.y + scrollRectTransform.rect.height;
        float scrollViewLowerY = scrollRectTransform.anchoredPosition.y - scrollRectTransform.rect.height;
        
        // If the selected position is below the current lower bound of the scroll view we scroll down.
        if (selectedPositionY + contentPanel.anchoredPosition.y + contentPanel.rect.height * 0.5f
            > scrollViewUpperY - scrollRectTransform.rect.height * upperOffset)
        {
            targetY = -selectedPositionY - contentPanel.rect.height * 0.5f - scrollRectTransform.rect.height * upperOffset;

            if (targetY + contentPanel.rect.height * 0.5f < scrollViewUpperY)
                targetY = -contentPanel.rect.height * 0.5f + scrollViewUpperY;
        }


        // If the selected position is above the current upper bound of the scroll view we scroll up.
        else if (selectedPositionY + contentPanel.anchoredPosition.y + contentPanel.rect.height * 0.5f
            < scrollViewLowerY + scrollRectTransform.rect.height * lowerOffset)
        {
            
            targetY = -selectedPositionY - scrollRectTransform.rect.height
                - contentPanel.rect.height * 0.5f + scrollRectTransform.rect.height * lowerOffset;
            if (targetY - contentPanel.rect.height * 0.5f > scrollViewLowerY)
                targetY = +contentPanel.rect.height * 0.5f + scrollViewLowerY;
        }

        lastSelected = selected;
    }

    private void TravelToTargetPositionY(float targetY) {
        contentPanel.anchoredPosition = new Vector2(contentPanel.anchoredPosition.x, 
            contentPanel.anchoredPosition.y + (targetY - contentPanel.anchoredPosition.y) * scrollSpeed / 100);

    }
}