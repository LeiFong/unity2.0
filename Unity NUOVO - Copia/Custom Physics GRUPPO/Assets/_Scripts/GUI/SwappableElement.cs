﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwappableElement : MonoBehaviour
{
    public EventTrigger.TriggerEvent OnSwapIn;
    public EventTrigger.TriggerEvent OnSwapOut;
    private CanvasGroup myCanvasGroup;

    public CanvasGroup GetCanvasGroup() {
        return myCanvasGroup;
    }

    public void SetCanvasGroup ()
    {
        myCanvasGroup = GetComponent<CanvasGroup>();
    }
}
