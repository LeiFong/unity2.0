﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageBasedProgressBar : MonoBehaviour
{
    public Image redBar, progressBar;
    public bool isRedBarIndipendent = false;
    [Range(0, 1)]
    [SerializeField]
    private float value = 0, redBarValue = 0;
    private float prevValue = -1;
    [Range(0, 60)]
    [SerializeField]
    private int timeToUpdateIncrement = 10;
    [Range(0, 60)]
    [SerializeField]
    private int timeToUpdateDeplete = 10;
    [Range(0, 100)]
    [SerializeField]
    private float updateSpeedIncrement = 3f;
    [Range(0, 100)]
    [SerializeField]
    private float updateSpeedDeplete = 3f;
    
    private int timer, redTimer;

    private List<Image> allImages;
    private List<Color> originalColors;
    
    // Start is called before the first frame update
    void Start()
    {
        allImages = new List<Image>(GetComponentsInChildren<Image>());
        originalColors = new List<Color>();
        for (int i = 0; i < allImages.Count; i++) {
            originalColors.Add(allImages[i].color);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isRedBarIndipendent)
            redBarValue = value;

        //se sto calando
        if (value < prevValue)
        {
            //setta il timer se non ero già in animazione
            if (!isRedBarIndipendent)
            {
                //if (progressBar.fillAmount == redBar.fillAmount)
                    redTimer = timeToUpdateDeplete;
                redBar.fillAmount = prevValue;
                progressBar.fillAmount = value;
            }
        }

        //se sto aumentando
        if (value > prevValue)
        {
            //setta il timer se non ero già in animazione
            if (!isRedBarIndipendent)
            {
                if (progressBar.fillAmount == redBar.fillAmount)
                    timer = timeToUpdateIncrement;

                if (redTimer == 0)
                {
                    if (redBar.fillAmount < value)
                        redBar.fillAmount = value;
                }
                else
                {
                    timer = 0;
                    redBarValue = value;
                }
            }
            
        }

        //setta animazione della redbar

        //se dovrei calare
        if (!isRedBarIndipendent)
        {
            if (redBar.fillAmount > redBarValue)
            {
                if (redTimer > 0)
                    redTimer--;
                else
                {
                    redBar.fillAmount -= updateSpeedDeplete / 100;
                    if (redBar.fillAmount < redBarValue)
                        redBar.fillAmount = redBarValue;
                }
            }
        }
        else {
            redBar.fillAmount = redBarValue;
        }
        //se dovrei salire
        /*if (redBar.fillAmount < redBarValue)
        {
            if (redTimer > 0)
                redTimer--;
            else
            {
                redBar.fillAmount += updateSpeedDeplete / 100;
                if (redBar.fillAmount > redBarValue)
                    redBar.fillAmount = redBarValue;
            }
        }
        */
        //setta animazione della progress bar

        //se dovrei salire
        if (progressBar.fillAmount < value)
        {
            if (timer > 0)
                timer--;
            else
            {
                progressBar.fillAmount += updateSpeedIncrement / 100;
                if (progressBar.fillAmount > value)
                    progressBar.fillAmount = value;
            }
        }
        //se dovrei calare
        
        if (progressBar.fillAmount > value)
        {
            if (timer > 0)
                timer--;
            else
            {
                progressBar.fillAmount -= updateSpeedIncrement / 100;
                if (progressBar.fillAmount < value)
                    progressBar.fillAmount = value;
            }
        }
        
        prevValue = value;
    }

    public void SetValue(float v) {
        if (v > 1)
            v = 1;
        if (v < 0)
            v = 0;
        value = v;
        //print("setvalue");
    }

    public void SetRedBarValue(float v)
    {
        if (v > 1)
            v = 1;
        if (v < 0)
            v = 0;
        redBarValue = v;
    }

    public void SetRedBarValueRelativeToProgressBar(float v) {
        SetRedBarValue(value + v);
    }

    public void IncreaseValue(float v) {
        SetValue(v + value);
    }
    public void DecreaseValue(float v)
    {
        SetValue(value - v);
    }

    public void SetVisible(bool v) {
        if (v)
        {
            for (int i = 0; i < allImages.Count; i++)
            {
                allImages[i].color = originalColors[i];
            }
        }
        else {
            for (int i = 0; i < allImages.Count; i++)
            {
                Color oc = originalColors[i];
                Color c = new Color(oc.r, oc.g, oc.b, 0);
                allImages[i].color = c;
            }
        }
    }

    public void SetTimeToUpdateIncrement(int t) {
        if (t < 0)
            t = 0;
        if (t > 60)
            t = 60;
        timeToUpdateIncrement = t;
    }
    public void SetTimeToUpdateDeplete(int t)
    {
        if (t < 0)
            t = 0;
        if (t > 60)
            t = 60;
        timeToUpdateDeplete = t;
    }

    public void SetUpdateSpeedIncrement(float speed) {
        if (speed < 0)
            speed = 0;
        if (speed > 100)
            speed = 100;
        updateSpeedIncrement = speed;
    }
    public void SetUpdateSpeedDeplete(float speed)
    {
        if (speed < 0)
            speed = 0;
        if (speed > 100)
            speed = 100;
        updateSpeedDeplete = speed;
    }

    public void SetProgressBarColor(Color c)
    {
        progressBar.color = c;
    }

    public void SetRedBarColor(Color c)
    {
        redBar.color = c;
    }

    public int GetTimeToUpdateDeplete()
    {
        return timeToUpdateDeplete;
    }
    public int GetTimeToUpdateIncrement()
    {
        return timeToUpdateIncrement;
    }
    public float GetUpdateSpeedIncrement()
    {
        return updateSpeedIncrement;
    }
    public float GetUpdateSpeedDeplete()
    {
        return updateSpeedDeplete;
    }
}
