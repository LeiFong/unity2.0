﻿using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void CheckForSceneFolder (int sceneIndex)
    {
        //OLD
        /*
        if (!Directory.Exists(Application.persistentDataPath + "/" + sceneName))
        {
            Debug.Log("cartella non esistente, creata adesso");
            Directory.CreateDirectory(Application.persistentDataPath + "/" + sceneName);
        }
        */

        

        if (!Directory.Exists(Application.persistentDataPath + "/" + sceneIndex.ToString()))
        {
            Debug.Log("cartella non esistente, creata adesso");
            Directory.CreateDirectory(Application.persistentDataPath + "/" + sceneIndex.ToString());
        }
    }

    public static void SaveNPCs(NPCData[] NPCs, int sceneIndex)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/" + sceneIndex.ToString() + "/NPCs.lul";
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, NPCs);
        stream.Close();

    }


    public static NPCData[] LoadNPCs(int sceneIndex)
    {
        string path = Application.persistentDataPath + "/" + sceneIndex.ToString() + "/NPCs.lul";
        BinaryFormatter formatter = new BinaryFormatter();
        

        if (File.Exists(path))
        {
            FileStream stream = new FileStream(path, FileMode.Open);

            NPCData [] data = formatter.Deserialize(stream) as NPCData[];

            stream.Close();

            return data;


        }
        else
        {
            Debug.LogError("file non trovato LUL");
            return null;
        }
       
    }

   public static void SavePlayer (Status status)
   {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player_status.lul";

        
        {
            FileStream stream = new FileStream(path, FileMode.Create);

            PlayerData data = new PlayerData(status);

            formatter.Serialize(stream, data);
            stream.Close();
        }

        
    }

    
    /*
    public static void SaveNPCBuffs (BuffData [] buffDatas)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/" + SceneManager.GetActiveScene().name + "/NPC_buffs.lul";

        
        {
            FileStream stream = new FileStream(path, FileMode.Create);

            formatter.Serialize(stream, buffDatas);
            stream.Close();
        }
        
    }
    
    public static void SavePlayerBuffs (BuffData buffDatas)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player_buffs.lul";


        {
            FileStream stream = new FileStream(path, FileMode.Create);

            formatter.Serialize(stream, buffDatas);
            stream.Close();
        }

    }

    public static BuffData[] LoadNPCBuffs ()
    {
        string path = Application.persistentDataPath + "/" + SceneManager.GetActiveScene().name + "/NPC_buffs.lul";
        BinaryFormatter formatter = new BinaryFormatter();
        

        if (File.Exists(path))
        {
            FileStream stream = new FileStream(path, FileMode.Open);

            BuffData [] data = formatter.Deserialize(stream) as BuffData[];

            stream.Close();

            return data;


        }
        else
        {
            //stream.Close();
            Debug.LogError("file non trovato LUL");
            return null;
        }
    }

    public static BuffData LoadPlayerBuffs()
    {
        string path = Application.persistentDataPath + "/player_buffs.lul";
        BinaryFormatter formatter = new BinaryFormatter();


        if (File.Exists(path))
        {
            FileStream stream = new FileStream(path, FileMode.Open);

            BuffData data = formatter.Deserialize(stream) as BuffData;

            stream.Close();

            return data;


        }
        else
        {
            //stream.Close();
            Debug.LogError("file non trovato LUL");
            return null;
        }
    }
    */
    public static PlayerData LoadPlayer()
    {
        string path = Application.persistentDataPath + "/" + SceneManager.GetActiveScene().name + "/player_status.lul";
        BinaryFormatter formatter = new BinaryFormatter();
        

        if (File.Exists(path))
        {
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;

            stream.Close();

            return data;


        }
        else
        {
            
            Debug.LogError("file non trovato LUL");
            return null;
        }


    }

    public static void SaveInventory(Inventory inventory)
    {
        SaveItems(inventory);
        SaveWeapons(inventory);
    }

    public static void LoadInventory (Inventory inventory)
    {
        inventory.consumables.Clear();
        inventory.weapons.Clear();

        LoadItems(inventory);
        LoadWeapons(inventory);
    }

    public static void SaveItems(Inventory inventory)
    {
        //Debug.Log("inventory: " + inventory);
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player_consumables.lul";
        FileStream stream = new FileStream(path, FileMode.Create);

        ItemData[] itemData = new ItemData[inventory.consumables.Count];
        for (int i = 0; i < inventory.consumables.Count; i++)
        {
            //Debug.Log(inventory.consumables[i].itemEffect.itemCode);
            ItemData newItemData = new ItemData(inventory.consumables[i].item);
            newItemData.quantity = inventory.consumables[i].quantity;
            itemData[i] = newItemData;
        }

        formatter.Serialize(stream, itemData);
        stream.Close();
        SaveMaterials(inventory);
        SaveQuickItemsSelection(inventory);
    }
    
    private static void SaveQuickItemsSelection (Inventory inventory)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player_quick_items_selection.lul";
        FileStream stream = new FileStream(path, FileMode.Create);

        ItemData[] itemData = new ItemData[inventory.quickItemSelection.items.Length];
        for (int i = 0; i < inventory.quickItemSelection.items.Length; i++)
        {
            

            if (inventory.quickItemSelection.items[i].item == null)
            {
                continue;
            }
            //Debug.Log(inventory.consumables[i].itemEffect.itemCode);
            ItemData newItemData = new ItemData(inventory.quickItemSelection.items[i].item);
            //newItemData.quantity = i;
            itemData[i] = newItemData;
            if (i == 0)
            {
                itemData[0].quantity = inventory.quickItemSelection.currentIndex; //salvo l'indice corrente del quickItemSelection nella quantita' del primo elemento 
            }
        }

        formatter.Serialize(stream, itemData);
        stream.Close();
        
    }

    private static void LoadQuickItemsSelection(Inventory inventory)
    {
        string path = Application.persistentDataPath + "/player_quick_items_selection.lul";
        BinaryFormatter formatter = new BinaryFormatter();

        //Debug.Log("cons stream: " + stream.Length);

        if (File.Exists(path))
        {

            FileStream stream = new FileStream(path, FileMode.Open);
            ItemData[] data = formatter.Deserialize(stream) as ItemData[];


            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == null)
                {
                    continue;
                }
                inventory.AddQuickConsumable(inventory.itemDatabase.GetItemFromDatabase(data[i].ID, data[i].itemType).item, i);
                if (i == 0)
                {
                    inventory.quickItemSelection.currentIndex = data[i].quantity; //avevo salvato l'indice del menu nella quantiy del primo elemento
                }
            }

            //return data;
            stream.Close();
            inventory.UpdateQuickConsumables();
        }

    }
    

    public static void SaveMaterials (Inventory inventory) { 

        string path = Application.persistentDataPath + "/player_materials.lul";
        BinaryFormatter formatter = new BinaryFormatter();

        FileStream stream = new FileStream(path, FileMode.Create);
        ItemData[] itemData = new ItemData[inventory.materials.Count];
        //Debug.Log("materials: " + itemData.Length);

        for (int i = 0; i < inventory.materials.Count; i++)
        {
            //Debug.Log(inventory.consumables[i].itemEffect.itemCode);
            ItemData newItemData = new ItemData(inventory.materials[i].item);
            newItemData.quantity = inventory.materials[i].quantity;
            itemData[i] = newItemData;
        }
        formatter.Serialize(stream, itemData);
        stream.Close();
    }



    public static void LoadItems(Inventory inventory)
    {
        string path = Application.persistentDataPath + "/player_consumables.lul";
        BinaryFormatter formatter = new BinaryFormatter();
        
        //Debug.Log("cons stream: " + stream.Length);

        if (File.Exists(path))
        {

            FileStream stream = new FileStream(path, FileMode.Open);
            ItemData[] data = formatter.Deserialize(stream) as ItemData[];


            for (int i = 0; i < data.Length; i++)
            {
                inventory.AddItem(inventory.itemDatabase.consumableItemsDatabase[data[i].ID].item,
                    data[i].quantity);
            }

            //return data;
            stream.Close();


        }
        else
        {
            Debug.LogError("file non trovato LUL");
        }
        
        LoadMaterials(inventory);
        LoadQuickItemsSelection(inventory);
    }

    public static void LoadMaterials (Inventory inventory) { 

        string path = Application.persistentDataPath + "/player_materials.lul";
        BinaryFormatter formatter = new BinaryFormatter();

        
        //Debug.Log("stream: " + File.Exists(path));

        if (File.Exists(path))
        {

            FileStream stream = new FileStream(path, FileMode.Open);
            ItemData[] data = formatter.Deserialize(stream) as ItemData[];

            

            for (int i = 0; i < data.Length; i++)
            {
                inventory.AddItem(inventory.itemDatabase.materialsDatabase[data[i].ID].item,
                    data[i].quantity);
            }

            //return data;
            stream.Close();

        }
        else
        {
            
            Debug.LogError("file non trovato LUL");
        }
        

    }



    public static void SaveEvents(EventData [] events, int sceneIndex)
    {
        //Debug.Log("inventory: " + inventory);
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/" + sceneIndex.ToString() + "/events.lul";
        FileStream stream = new FileStream(path, FileMode.Create);

        

        formatter.Serialize(stream, events);
        stream.Close();
    }


    public static EventData[] LoadEvents(int sceneIndex)
    {
        string path = Application.persistentDataPath + "/" + sceneIndex.ToString() + "/events.lul";
        BinaryFormatter formatter = new BinaryFormatter();
        

        if (File.Exists(path))
        {

            FileStream stream = new FileStream(path, FileMode.Open);
            EventData[] data = formatter.Deserialize(stream) as EventData[];

            stream.Close();
            return data;


        }
        else
        {
            
            Debug.LogError("file non trovato LUL");
            return null;
        }


    }

    public static void SaveWeapons (Inventory inventory)
    {
        
        WeaponData[] weaponDatas = new WeaponData[inventory.weapons.Count];
        for (int i = 0; i < inventory.weapons.Count; i++)
        {
            WeaponData newWeaponData = new WeaponData(inventory.weapons[i].weapon.weaponIndex, inventory.weapons[i].currentUpgrade);
            weaponDatas[i] = newWeaponData;
        }

        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player_weapons.lul";
        FileStream stream = new FileStream(path, FileMode.Create);



        formatter.Serialize(stream, weaponDatas);
        stream.Close();
    }

    public static void LoadWeapons (Inventory inventory)
    {
        string path = Application.persistentDataPath + "/player_weapons.lul";
        BinaryFormatter formatter = new BinaryFormatter();
        //Debug.Log(Application.persistentDataPath);

        if (File.Exists(path))
        {

            FileStream stream = new FileStream(path, FileMode.Open);
            WeaponData[] data = formatter.Deserialize(stream) as WeaponData[];

            //return data;
            for (int i = 0; i < data.Length; i++)
            {
                inventory.AddWeaponItem(inventory.controller.weaponSwitcher.weapons[data[i].weaponIndex].weaponItem, data[i].upgrade);
            }
            stream.Close();


        }
        else
        {
            Debug.LogError("file non trovato LUL");
        }
        
    }

    public static void SaveGlobals(GlobalGameStateData[] globalData)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/globals.lul";
        FileStream stream = new FileStream(path, FileMode.Create);

        Debug.Log("saveglobals: " + globalData.Length);

        formatter.Serialize(stream, globalData);
        stream.Close();
    }

    public static GlobalGameStateData[] LoadGlobals()
    {
        string path = Application.persistentDataPath + "/globals.lul";
        BinaryFormatter formatter = new BinaryFormatter();
        //Debug.Log(Application.persistentDataPath);

        if (File.Exists(path))
        {

            FileStream stream = new FileStream(path, FileMode.Open);
            GlobalGameStateData[] data = formatter.Deserialize(stream) as GlobalGameStateData[];

            //return data;
            //Debug.Log("savesystem.loadglobals: " + data.Length);
            
            stream.Close();
            return data;

        }
        else
        {
            Debug.LogError("file non trovato LUL");
            return null;
        }
    }

}
