﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalStatesContainer : MonoBehaviour
{

    public Dictionary<string, GlobalGameStateData> globals = new Dictionary<string, GlobalGameStateData>();

    public void AddGlobal (GlobalGameStateData newGlobal)
    {
        globals.Add(newGlobal.myName, newGlobal);
    }

    public void CreateTestGlobal()
    {
        TestGlobal testGlobal = new TestGlobal();
        testGlobal.myName = "minghie";
        testGlobal.a = "buzzicone";
        AddGlobal(testGlobal);
    }

    public void PrintA ()
    {
        TestGlobal testGlobal = (TestGlobal) globals["minghie"];
        print("testglobal: " + testGlobal.a);
    }

    private void Start()
    {
        CreateTestGlobal();
        PrintA();
    }

}
