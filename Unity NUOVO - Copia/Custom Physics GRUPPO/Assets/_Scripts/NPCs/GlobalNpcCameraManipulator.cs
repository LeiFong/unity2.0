﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalNpcCameraManipulator : MonoBehaviour
{
    public static GlobalNpcCameraManipulator Instance;
    public CameraManipulator cameraManipulator;

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
            Instance = this;
    }
    
}
