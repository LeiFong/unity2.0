﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SwappableMenu : MonoBehaviour
{
    

    public Direction direction = Direction.horizontal;
    [SerializeField]
    private List<SwappableElement> swappingMenus;
    private List<GameObject> firstSelected, lastSelected;

    public enum Direction { horizontal, vertical }

    private MenuManager myMenuManager;
    
    private int currentMenu = 0;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < swappingMenus.Count; i++) {
            swappingMenus[i].SetCanvasGroup();
            if (i != 0)
            {
                swappingMenus[i].GetCanvasGroup().alpha = 0;
                swappingMenus[i].GetCanvasGroup().interactable = false;
            }
            else {
                swappingMenus[i].GetCanvasGroup().alpha = 1;
                swappingMenus[i].GetCanvasGroup().interactable = true;
            }
        }

        firstSelected = new List<GameObject>();
        lastSelected = new List<GameObject>();
        for (int i = 0; i < swappingMenus.Count; i++) {
            Button b = swappingMenus[i].gameObject.GetComponentInChildren<Button>();
            if (b != null)
            {
                firstSelected.Add(b.gameObject);
            }
            else
            {
                firstSelected.Add(null);

            }
            lastSelected.Add(null);
        }
        
        myMenuManager = GetComponent<MenuManager>();
    }

    private void Update()
    {
        if (myMenuManager.IsMenuCurrentlySelected()) {

            if (Input.GetKeyDown(KeyCode.LeftArrow) && direction == Direction.horizontal ||
                Input.GetKeyDown(KeyCode.UpArrow) && direction == Direction.vertical)
            {
                SwapMenu(-1);
            }
            if (Input.GetKeyDown(KeyCode.RightArrow) && direction == Direction.horizontal ||
                Input.GetKeyDown(KeyCode.DownArrow) && direction == Direction.vertical)
            {
                SwapMenu(1);
            }
        }
    }

    public void SwapMenu(int delta) {

        lastSelected[currentMenu] = EventSystem.current.currentSelectedGameObject;

        swappingMenus[currentMenu].GetCanvasGroup().alpha = 0;
        swappingMenus[currentMenu].GetCanvasGroup().interactable = false;

        swappingMenus[currentMenu].OnSwapOut.Invoke(new BaseEventData(EventSystem.current));
        currentMenu = (currentMenu + delta) % swappingMenus.Count;
        if (currentMenu < 0)
            currentMenu = swappingMenus.Count - 1;
        swappingMenus[currentMenu].OnSwapIn.Invoke(new BaseEventData(EventSystem.current));
                
        swappingMenus[currentMenu].GetCanvasGroup().alpha = 1;
        swappingMenus[currentMenu].GetCanvasGroup().interactable = true;
        
        if (lastSelected[currentMenu] == null)
            EventSystem.current.SetSelectedGameObject(firstSelected[currentMenu]);
        else
            EventSystem.current.SetSelectedGameObject(lastSelected[currentMenu]);

    }


    public void ResetMenu() {
        SwapMenu(-currentMenu);
        for (int i = 0; i < swappingMenus.Count; i++)
        {
            lastSelected[i] = null;
            if (i != 0)
            {
                swappingMenus[i].GetCanvasGroup().alpha = 0;
                swappingMenus[i].GetCanvasGroup().interactable = false;
            }
            else
            {
                swappingMenus[i].GetCanvasGroup().alpha = 1;
                swappingMenus[i].GetCanvasGroup().interactable = true;
            }
        }
        currentMenu = 0;
    }
}
