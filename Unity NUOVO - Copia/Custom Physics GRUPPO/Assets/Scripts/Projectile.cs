﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	//float gravity = -20;
	RaycastHit2D[] results = new RaycastHit2D[50];
	Collider2D nearestColliderHit;
	float minDistance;
	//public enum colliderShape {box, circle};
	//public colliderShape myColliderShape;
	public bool destroyOnCollision;
	public Vector3 velocity;
    public int timeToLive = -1;
    public int timeToLiveCunter;
	public Rigidbody2D rb;
	public FrameData move;
    public int hitFrequency = 1; //ogni quanti frame colpisce se rimane all'interno dell'hurtbox per piu' frame consecutivi

	public Vector2 targetVelocity;
	public bool avoidsCharacters = false;
	protected Collider2D[] hitBuffer = new Collider2D[16];
	protected Collider2D[] collisionBuffer = new Collider2D[16];

	[HideInInspector]
	public Vector2 startingPosition;

	//public BoxCollider2D myBoxCollider;
	public Collider2D myCollider;
	public float skinWidth = .015f;
	//RayCastOrigins rayCastOrigins;
	public bool grounded = false;
	//public LayerMask collidesWith;

	public float maxSlopeAngle = 80;

	public LayerMask collisionMask;

	public int horizontalRayCount = 4;
	public int verticalRayCount = 4;

	public float horizontalRaySpacing;
	public float verticalRaySpacing;

	public CollisionInfo collisions;

	public float groundShell = 0.5f;
	public LayerMask groundMask;
	private bool hasCollided;

	protected string myLayer;
	protected ContactFilter2D contactFilter;
	//public Status mystatus;

	private bool markedForDestruction;
	bool initialized;
	int hurtboxLayerIndex;

    private void Start()
    {
		if (!initialized)
			InitializeProjectile();
    }

    public void InitializeProjectile () {
		//rb = GetComponent<Rigidbody2D> ();
		//myBoxCollider = GetComponentInChildren<BoxCollider2D> ();
		//myCircleCollider = GetComponentInChildren<CircleCollider2D> ();
		//move = GetComponent<FrameData> ();
		move.projectileScript = this;

		
		
		//CalculateRaySpacing ();
		//myLayer = LayerMask.LayerToName (gameObject.layer);
		//if (myLayer == "Character") {
			//mystatus = GetComponent<Status> ();
		//}

		//MyOnEnable ();
		contactFilter.useTriggers = true;
		contactFilter.SetLayerMask (collisionMask);
		contactFilter.useLayerMask = true;

		hurtboxLayerIndex = LayerMask.NameToLayer("Hurtbox");

        if (timeToLive >= 0)
        {
            timeToLiveCunter = timeToLive;

        }

		initialized = true;
	}

    private void OnEnable()
    {
        MyOnEnable();
    }

    protected virtual void MyOnEnable() {

	}

	void FixedUpdate () {
		//Time.timeScale = 0.1f;
		if (!TimeVariables.canExecuteLogic)
			return;

		//grounded = collisions.below;
		if (markedForDestruction)
		{
			//print("destroy: " + Time.frameCount);
			Destroy(gameObject);

		}

        if (timeToLiveCunter > 0)
        {
            timeToLiveCunter--;
        }
        else if (timeToLiveCunter == 0)
        {
            Destroy(gameObject);
        }


        ComputeVelocity ();
		
		if (targetVelocity.x >= 0)
		{
			move.facingRight = true;
		}
		else
		{
			move.facingRight = false;
		}

		grounded = false;



		if (Time.timeScale == 1)
		{
			velocity.y = targetVelocity.y * Time.fixedDeltaTime;
			velocity.x = targetVelocity.x * Time.fixedDeltaTime;
			Move();
		}
        

		//Debug.Log (collisions.slidingDownMaxSlope);
	}
    private void Update()
    {
		if (!TimeVariables.canExecuteLogic)
			return;
		if (Time.timeScale != 1)
		{
			velocity.y = targetVelocity.y * Time.deltaTime;
			velocity.x = targetVelocity.x * Time.deltaTime;
			Move();
		}
    }

    protected virtual void ComputeVelocity () {

	}

	public struct CollisionInfo
	{
		public bool above, below;
		public bool left, right;
		public int faceDir;

		public bool climbingSlope;
		public bool descendingSlope;
		public bool slidingDownMaxSlope;

		public float slopeAngle, slopeAngleOld;
		public Vector3 velocityOld;

		public void Reset () {
			above = below = false;
			right = left = false;
			climbingSlope = false;
			slidingDownMaxSlope = false;
			slopeAngleOld = slopeAngle;
			slopeAngle = 0;
			faceDir = 0;
		}
	}

	public void Move () {
		//UpdateRayCastOrigins ();



		if (!hasCollided)
			Collisions();
		//CollisionCheck ();
		


		if (hasCollided) {
			//hasCollided = false;
			OnCollisionBehaviour ();

			if (!destroyOnCollision)
				hasCollided = false;
		}
		


		//ExitCharacters (ref velocity);




		//Debug.Log (velocity.x);

		//if (velocity.x != 0)
		//	HorizontalCollisions (ref velocity);

		//if (velocity.y != 0)
		//	VerticalCollisions (ref velocity);


		transform.Translate (velocity);		

		//if (!collisions.slidingDownMaxSlope)
		//	GroundCheck (ref velocity);
	}

	void Collisions () {

		
		minDistance = velocity.magnitude;
		nearestColliderHit = null;
		//string nearestLayer = "";
		//if (myColliderShape == colliderShape.circle)
		
			//results = Physics2D.CircleCastAll (transform.position, myCircleCollider.radius, velocity, velocity.magnitude, collisionMask);
		int count = myCollider.Cast(velocity, contactFilter, results, minDistance);
		int otherLayer;

		//if (results.Length > 0)
		{
			for (int i = 0; i < count; i++) {
				otherLayer = (results [i].collider.gameObject.layer);

				if (otherLayer == hurtboxLayerIndex)
				{
					HurtboxHit hurtboxHit = results[i].collider.gameObject.GetComponent<HurtboxHit>();
					if (hurtboxHit.status.faction == move.faction || hurtboxHit.status.invincible)
					{

						continue;

					}
					//print("BOOOOOOOOOM");
					hurtboxHit.CheckHitboxCollisions(myCollider, true);
				}
				
				{

					if (results[i].distance <= minDistance)
					{
						//print("vistoooooooo " + minDistance);
						minDistance = results[i].distance;
						nearestColliderHit = results[i].collider;
						//nearestLayer = otherLayer;
						//nearestColliderHit.GetComponent<HurtboxHit>().CheckHitboxCollisions(myCircleCollider);
						//print("BOOOOOOOOOM: " + nearestColliderHit.gameObject + " " + results.Length + " " + Time.frameCount);
						hasCollided = true;
					}
				}
                
               
			}

			if (destroyOnCollision && nearestColliderHit != null) {
				
				velocity = minDistance * velocity.normalized;

					
			}
		}

	}

	void CollisionCheck() {
		int count = rb.OverlapCollider(contactFilter, collisionBuffer);
		if (count > 0) {
			for (int i = 0; i < count; i++) {
				//string otherLayer = LayerMask.LayerToName (collisionBuffer [i].gameObject.layer);

				LayerMask otherLayer = collisionBuffer[i].gameObject.layer;
				//Debug.Log (LayerMask.LayerToName(otherLayer));
				//Debug.Log (LayerMask.LayerToName(collidesWith));
				//Debug.Log (LayerMask.LayerToName(collisionMask));

				if ((otherLayer == LayerMask.NameToLayer("HurtBox") && collisionBuffer[i].gameObject.GetComponent<HurtboxHit> ().status.faction != move.faction)
					|| otherLayer == 0)
				
					//if (otherLayer == "HurtBox")
				{
					
					//Debug.Log ("COLLIDED " + LayerMask.LayerToName(otherLayer) + move.faction + " " + collisionBuffer[i].gameObject.GetComponent<HurtboxHit> ().status.faction);
					hasCollided = true;
				}
			}
		}
	}

	public virtual void OnCollisionBehaviour() {
        //print("booooom");
        if (destroyOnCollision)
        {
            myCollider.enabled = false;
            //Destroy (gameObject);
            //gameObject.SetActive (false);
            markedForDestruction = true;
        }
		
	}





}
