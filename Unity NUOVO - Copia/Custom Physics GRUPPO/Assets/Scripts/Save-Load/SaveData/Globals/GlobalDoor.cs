﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalDoor : MonoBehaviour
{
    GlobalDoorData doorData;
    public string myName;
    public bool open = false;

    PlayersManager playersManager;

    public Collider2D myCollider;

    void Start()
    {
        playersManager = PlayersManager.Instance ?? FindObjectOfType<PlayersManager>();
        //playersManager.gameManager.LoadGlobals();

        doorData = (GlobalDoorData) playersManager.gameManager.GetGlobal(myName);

        if (doorData != null)
        {
            //print("data c'è");
            open = doorData.open;
        }
        else
        {
            UpdateGlobalDictionary();
        }
    }

    public void UpdateStateFromDictionary()
    {
        doorData = (GlobalDoorData)playersManager.gameManager.GetGlobal(myName);

        if (doorData != null)
        {
            //print("data c'è");
            open = doorData.open;
        }
    }

    public void UpdateGlobalDictionary()
    {
        
        doorData = new GlobalDoorData();
        doorData.myName = myName;
        doorData.open = open;
        playersManager.gameManager.AddGlobal(doorData);
        //playersManager.gameManager.SaveGlobals();
    }

    // Update is called once per frame
    void Update()
    {
        myCollider.enabled = !open;
    }
}
