﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterAttack : MoveScript
{

    public int moveDuration;

    public override void SetStatus()
    {
        status.mystatus = Status.status.attacking;
    }

    public override void MyExecuteSequence()
    {
        status.mystatus = Status.status.attacking;
        if (controller.seq < moveDuration)
            controller.seq++;
        else
        {
            itsTimeToStopTheSequence = true;
        }
        if (controller.hitWhileInvincible)
        {


            
            controller.StartNewMove(relatedMoves[0]);

            return;
        }
    }
}
