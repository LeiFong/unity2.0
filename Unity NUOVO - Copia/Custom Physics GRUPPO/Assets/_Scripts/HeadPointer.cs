﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadPointer : MonoBehaviour
{
    [Tooltip("You need an animator with 3 layers, one with an animation of the head straight (looking right in front of it), " +
        "one with an animation of the head up, and one with an animation of the head down." +
        " The layer with the head straight must be higher in the hyerarchy than the other two.")]
    public bool mouseOverForTutorial;
    [Space(20)]
    public Animator animator;
    [Tooltip("The number of the layer for the head neutral animation. E.g. if it's the second layer in the hyerarchy, write 1; if it's the third, write 2.")]
    public int animatorNeutralLayerIndex;
    [Tooltip("The number of the layer for the head up animation. E.g. if it's the second layer in the hyerarchy, write 1; if it's the third, write 2.")]
    public int animatorUpLayerIndex;
    [Tooltip("The number of the layer for the head down animation. E.g. if it's the third layer in the hyerarchy, write 2; if it's the foirth, write 3.")]
    public int animatorDownLayerIndex;
    [Space(20f)]
    public Transform followedObject;
    public bool isActive = true;
    [Space(20f)]
    [Range(-90, 0)]
    public float minAngle = -90;
    [Range(0, 90)]
    public float maxAngle = 90;
    [Space(20f)]
    [Range(0f, 1f)]
    public float precision = 1;
    [Range(0, 100)]
    public float speed = 50;
    [Space(20f)]
    public int direction = 1;
    private float angle = 0;
    private float neutralW = 0, targetNeutral = 0;
    public bool keepTrackingWhenBehindYou = false;

    private Transform tr;
    private Vector2 delta = new Vector2();

    private void Awake()
    {
        tr = transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (followedObject != null && isActive)
        {

            delta.x = -tr.position.x + followedObject.position.x;
            delta.y = -tr.position.y + followedObject.position.y;
            float targetAngle = Vector2.SignedAngle(new Vector2((float)direction, 0f), delta);

            bool lostTrack = false;

            //se ci è andato dietro
            if ((targetAngle > 90 || targetAngle < -90) && !keepTrackingWhenBehindYou)
            {
                lostTrack = true;
                targetAngle = 0;
                targetNeutral = 0;
                //animator.SetLayerWeight(animatorNeutralLayerIndex, animator.GetLayerWeight(animatorNeutralLayerIndex) * 0.95f);
                //neutralW = animator.GetLayerWeight(animatorNeutralLayerIndex);
            }
            /*else {
                if (neutralW < precision * ( 1f - (Mathf.Abs(angle) / 90f)))
                    neutralW += 0.02f;
                else
                    neutralW = precision;
                animator.SetLayerWeight(animatorNeutralLayerIndex, neutralW);
            }
            */
            if (targetAngle > maxAngle)
                targetAngle = maxAngle;
            else if (targetAngle < minAngle)
                targetAngle = minAngle;

            targetAngle *= precision;

            if (!lostTrack)
                angle = angle + (targetAngle - angle) / (101 - speed);
            else
                angle = angle + (targetAngle - angle) / 60f;

            if (angle >= 0)
            {
                float r = angle / 90f;
                if (!lostTrack)
                    targetNeutral = 1f - r;
                else
                    targetNeutral = 0;
                
                targetNeutral *= precision;
                neutralW += (targetNeutral - neutralW) / (101 - speed);
                animator.SetLayerWeight(animatorUpLayerIndex, r);
                animator.SetLayerWeight(animatorDownLayerIndex, 0f);
                animator.SetLayerWeight(animatorNeutralLayerIndex, neutralW);
                
                

            }
            else
            {
                float r = -angle / 90f;
                if (!lostTrack)
                    targetNeutral = 1f - r;
                else
                    targetNeutral = 0;
                
                targetNeutral *= precision;
                neutralW += (targetNeutral - neutralW) / (101 - speed);
                animator.SetLayerWeight(animatorDownLayerIndex, r);
                animator.SetLayerWeight(animatorUpLayerIndex, 0f);
                animator.SetLayerWeight(animatorNeutralLayerIndex, neutralW);
            }

        }
        else
        {

            if (neutralW > 0.01f)
            {
                animator.SetLayerWeight(animatorNeutralLayerIndex, animator.GetLayerWeight(animatorNeutralLayerIndex) * 0.9f);
                animator.SetLayerWeight(animatorUpLayerIndex, animator.GetLayerWeight(animatorUpLayerIndex) * 0.9f);
                animator.SetLayerWeight(animatorDownLayerIndex, animator.GetLayerWeight(animatorDownLayerIndex) * 0.9f);
                neutralW = animator.GetLayerWeight(animatorNeutralLayerIndex);
            }
            else {
                angle = 0;
                animator.SetLayerWeight(animatorNeutralLayerIndex, 0f);
                animator.SetLayerWeight(animatorUpLayerIndex, 0f);
                animator.SetLayerWeight(animatorDownLayerIndex, 0f);
            }
        }
    }


    public void SetActive(bool active) {
        isActive = active;
    }
}
