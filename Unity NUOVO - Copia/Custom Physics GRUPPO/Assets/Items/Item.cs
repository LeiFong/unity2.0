﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Item : ScriptableObject {

	public string itemName;
	public int itemCode;
    public Inventory.ItemType itemType;

    //public int quantity;
    public int maxQuantity;
    //public bool consumable;

    public string description;
    public Sprite sprite;

}
