﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class IPreAwakeRetriever : MonoBehaviour
{
    public bool disableRetrievedObjects = true;
    [SerializeField]
    public List<Component> preawakes;
    public List<GameObject> objects;
    public List<Object> objectsToManuallyUnload;
    public string myName;

    private BackgroundAwaker awaker;

    private void OnDisable()
    {
        if (Application.isPlaying)
        {
            return;
        }
        else
            print("In editor, retrieving IPreAwake");

        myName = "HAHAHAH";

        //Prendo tutte le interfacce IPreAwake che trovo nei figli
        preawakes = new List<Component>(GetComponentsInChildren(typeof(IPreAwake), true));

        objects = new List<GameObject>();

        foreach (Transform child in transform) {
            objects.Add(child.gameObject);
        }

        List<SubItemActivator> items = new List<SubItemActivator>(GetComponentsInChildren<SubItemActivator>(true));
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].leaf)
                objects.Add(items[i].gameObject);
            if (items[i].node)
                foreach (Transform child in items[i].transform)
                    objects.Add(child.gameObject);
        }

        print("Found " + objects.Count + " root objects");
        
        awaker = GetComponent<BackgroundAwaker>();
        if (awaker == null)
        {
            print("Awaker not found");
            return;
        }
        awaker.preAwakes = preawakes;
        awaker.objects = objects;
        awaker.objectsToManuallyUnload = objectsToManuallyUnload;
        if (disableRetrievedObjects)
            for (int i = 0; i < objects.Count; i++)
            {
                objects[i].SetActive(false);
            }
        print("Found " + preawakes.Count + " preawakes");

    }
    private void OnEnable()
    {
        if (Application.isPlaying || objects == null || !disableRetrievedObjects)
            return;
        for (int i = 0; i < objects.Count; i++)
            objects[i].SetActive(true);
    }

}
