﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomUsabilityConditions : MonoBehaviour
{
    //classe da abbinare a un movescript, serve a customizzare le usability conditions per una singola mossa

    public enum LogicOption
    {
        or, and
    }

    [HideInInspector]
    public MoveScript relativeMove;

    public LogicOption logic;

    private void Start()
    {
        relativeMove = GetComponent<MoveScript>();
    }

    public virtual bool CheckMyUsabilityConditions ()
    {
        return true;
    }
}
