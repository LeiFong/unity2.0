﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour
{
    public EquipmentPiece[] equipmentPieces;

    [System.Serializable]
    public struct BoneName
    {
        public string boneName;
        public SpriteRenderer boneRenderer;
    }

    public BoneName[] boneNames;

    public Dictionary<string, SpriteRenderer> bonesDictionary = new Dictionary<string, SpriteRenderer>();

    void Start()
    {
        

         for (int i = 0; i < boneNames.Length; i++)
        {
            if (boneNames[i].boneRenderer != null)
            {
                boneNames[i].boneName = boneNames[i].boneRenderer.gameObject.name;
                
                bonesDictionary.Add(boneNames[i].boneName, boneNames[i].boneRenderer);


            }
            else
            {
                //print("error: missing bone from equipmanager");
            }
        }
    }

    public void Equip (string boneName, Sprite newSprite)
    {
        bonesDictionary[boneName].sprite = newSprite;
    }






}
