﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockScript : MoveScript {

	//public int secondPartBlockFrame = 30;
	public override void MyStopSequence() {
		controller.seq = 0;
		//Debug.Log ("STOP!");
		//controller.animator.SetBool(animatorParameterName, false);

		status.mystatus = Status.status.neutral;
		controller.avoidingCharacters = false;
		//StopCoroutine ("Attack");
		//StartCoroutine (status.WaitBeforeRecharge ());
		//controller.animator.SetBool ("roll", false);
		//controller.animator.SetBool ("neutral", true);
		//controller.animator.speed = 1;
		controller.currentMove = null;
	}

	public override void SetStatus()
	{
		status.mystatus = Status.status.blocking;
	}

	public override void MyExecuteSequence ()
	{
		status.mystatus = Status.status.blocking;
		if (controller.inputCheck.controls.Player.BlockHold.ReadValue<float>() == 0)
		{
			print("no more blocking");
			itsTimeToStopTheSequence = true;
		}
	}
}
