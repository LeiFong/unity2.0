﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighlightableElement : MonoBehaviour
{
    private Image image;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
    }

    public void Highlight(bool v) {
        if (v)
            image.color = new Color(image.color.r, image.color.g, image.color.b, 1);
        else
            image.color = new Color(image.color.r, image.color.g, image.color.b, 0.5f);
    }
}
