﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomPhysics : MonoBehaviour, IPreAwake {
	//public bool slowTime;
	//public float slowTimeScale = 0.01f;
	public bool logicEnabled = true;
	[HideInInspector]
	public PlayersManager pm;
	[HideInInspector]
	public BackgroundAwaker backgroundAwaker;
	public bool debugMode = false;
	public bool simplePhysics = false;
	private Collider2D genericCollider;
	public int platformLayerIndex;
	public int movingPlatformLayerIndex;
	public int defaultLayerIndex;
    public TimeManager timeManager;
	public bool fallingThroughPlatform;
	//public BoxCollider2D myCollider;
    public bool exitsCharacters = true;
	public bool getsPushedByCharacters = false;
	public bool checksPlatforms = true;
	public bool checksUpperAngle = false;
	//public Collider2D horizontalExitCollider;//, fastHorizontalExitCollider; 
	//public Collider2D platformCollisionChecker;
	public bool changesGravityWhenDescending = false;
	public Transform movingParts; 
	protected int currentCharacterExiterLayerIndex;

    [System.Serializable]
    public struct GravitySections
    {
        public float thresholdVelocity;
        public float customGravity;
    }
	[HideInInspector]
	public int character0LayerIndex, character1LayerIndex, character2LayerIndex;
	//[HideInInspector]
	public GravitySections[] jumpingGravitySections;
	[HideInInspector]
    public GravitySections[] juggleGravitySections;

    
	public float maxDescendingVelocity = -60; //velocità max raggiungibile quando si cade

	public ExitObjects exitObjects;
	public float currentGravity;
	public float jumpingGravity; //gravità applicata solo dopo aver saltato
    public float normalGravity; //gravità applicata normalmente
    public bool jumpingState; //vera solo durante un salto, falsa quando grounded o quando airborne non a seguito di un salto
	public float jumpVelocity;
	public float jumpHeight;
	public float timeToJumpApex;
	public Vector2 velocity;
    public Collider2D objectUnderMe; //ciò su cui sono in piedi
    public Collider2D climbedPlatform; //collider colpito dal LedgeGrab e LedgClimb
    private Vector2 tempVelocity = new Vector2();

	public float horizontalMomentum;

	public bool inHitstop;
	[HideInInspector]
	public LayerMask platformLayerMask;
	public Vector2 targetVelocity;
	public bool avoidingCharacters = false;
	protected bool previousAvoidingCharacters = false;
	protected Collider2D[] hitBuffer = new Collider2D[20];
	//protected Collider2D[] platformsHit = new Collider2D[30];
	public List<Collider2D> platformsHit = new List<Collider2D> ();
	protected List<Collider2D> stairsHit = new List<Collider2D> ();


	//public BoxCollider2D myCollider, horizontalExitCollider;
	public float skinWidth = .015f;
	public float characterAvoidSkinwidth = 0.1f;

	public float shellRadius = .015f;
	public RayCastOrigins rayCastOrigins;
	public bool grounded = false;
	public bool isAffectedByGravity = true;

	public float maxSlopeAngle = 80;
	public float maxUpperSlopeAngle = 55;
    Vector2 movingPlatformVelocity;

	public LayerMask collisionMask;

	public int horizontalRayCount = 4;
	public int verticalRayCount = 4;

	public float horizontalRaySpacing;
	public float verticalRaySpacing;


	public CollisionInfo collisions;

	public float groundShell = 0.5f;
	public float groundDistanceMax = 3;
	public float currentGroundDistance;
	public LayerMask groundMask;
	public LayerMask upwardCollisionMask;
    public LayerMask horizontalCollisionMask;


	public bool isExiting = false;
	protected float exitVelocity = 4;
	protected float fastExitVelocity = 90;

	public int jumpDuration;

	public string myLayer;//, myTag;
	protected ContactFilter2D contactFilter;
	public ContactFilter2D characterAvoiderCF, platformCheckerCF;

    public ContactFilter2D collisionCF, groundCF, upwardCF, horizontalCF, groundCheckCF, noPlatformsGroundCheckCF;
	public Status mystatus;
	//public HurtboxHit hurtboxhit;
	public HurtboxHit[] hurtboxes;
	public float recoil = 0; //viene settato nel caso in cui il nemico viene colpito all'angolo, e quindi il pushback viene rediretto all'attaccante
	public int remainingRecoilFrames = 0; //per quanti frame deve avvenire il recoil
	GameObject [] objectsImExiting;
    protected bool isCharacter;

    protected virtual void PhysicsInitialization()
    {

    }

    private void OnEnable()
    {
        MyOnEnable();
    }

	[System.Serializable]
	public struct JumpParameters
    {
		public float defaultGravity;
		public float jumpVelocity;
		public GravitySections[] gravitySections;
    }

	public JumpParameters defaultJumpParameters= new JumpParameters();
	public GravitySections[] defaultJuggleGravitySections;

	public void RestoreOriginalGravity()
    {
		jumpingGravity = defaultJumpParameters.defaultGravity;
		jumpVelocity = defaultJumpParameters.jumpVelocity;
		jumpingGravitySections = defaultJumpParameters.gravitySections;

		juggleGravitySections = defaultJuggleGravitySections;
    }


	public bool PreAwake (int preAwakeIndex, BackgroundAwaker backgroundAwaker) {

		if (preAwakeIndex == 0)
		{
			movingPlatformVelocity = new Vector2(0, 0);
			
			jumpHeight = 3.2f;
			timeToJumpApex = 0.3f;
			jumpingGravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
			currentGravity = jumpingGravity;
			jumpVelocity = Mathf.Abs(currentGravity) * timeToJumpApex;

			
			print("jumpvelocity: " + jumpVelocity + " currentgravity: " + currentGravity);

			
			jumpingGravity = -71;
			jumpVelocity = 21;

			defaultJumpParameters.defaultGravity = jumpingGravity;
			defaultJumpParameters.jumpVelocity = jumpVelocity;

			defaultJumpParameters.gravitySections = new GravitySections[4];

			defaultJumpParameters.gravitySections[0].thresholdVelocity = 1000;
			defaultJumpParameters.gravitySections[0].customGravity = -50;
			defaultJumpParameters.gravitySections[1].thresholdVelocity = 12;
			defaultJumpParameters.gravitySections[1].customGravity = -120;
			defaultJumpParameters.gravitySections[2].thresholdVelocity = 4;
			defaultJumpParameters.gravitySections[2].customGravity = -20;
			defaultJumpParameters.gravitySections[3].thresholdVelocity = 0;
			defaultJumpParameters.gravitySections[3].customGravity = -120;

			currentGravity = jumpingGravity;

			jumpingGravitySections = new GravitySections[4];
			/*
			jumpingGravitySections[0].thresholdVelocity = 1000;
			jumpingGravitySections[0].customGravity = -50;
			jumpingGravitySections[1].thresholdVelocity = 12;
			jumpingGravitySections[1].customGravity = -120;
			jumpingGravitySections[2].thresholdVelocity = 4;
			jumpingGravitySections[2].customGravity = -20;
			jumpingGravitySections[3].thresholdVelocity = 0;
			jumpingGravitySections[3].customGravity = -120;
			*/


			


			juggleGravitySections = new GravitySections[4];

			juggleGravitySections[0].thresholdVelocity = 100;
			juggleGravitySections[0].customGravity = -50;
			juggleGravitySections[1].thresholdVelocity = 16;
			juggleGravitySections[1].customGravity = -140;
			juggleGravitySections[2].thresholdVelocity = 2;
			juggleGravitySections[2].customGravity = -1.5f;
			juggleGravitySections[3].thresholdVelocity = 1;
			juggleGravitySections[3].customGravity = -70;

			defaultJuggleGravitySections = new GravitySections[juggleGravitySections.Length];
			defaultJuggleGravitySections = juggleGravitySections;



			jumpingState = false;
			exitVelocity = 3;
			fastExitVelocity = 20;
			//timeManager = FindObjectOfType<TimeManager>();

			//CalculateRaySpacing ();
			//myTag = gameObject.tag;
			//if (myTag == "Character") {
			//mystatus = GetComponent<Status> ();
			//hurtboxhit = GetComponentInChildren<HurtboxHit> ();
			//}

			platformLayerIndex = LayerMask.NameToLayer("Platform");
			movingPlatformLayerIndex = LayerMask.NameToLayer("MovingPlatform");
			defaultLayerIndex = LayerMask.NameToLayer("Default");
			character0LayerIndex = LayerMask.NameToLayer("Character0");
			character1LayerIndex = LayerMask.NameToLayer("Character1");
			character2LayerIndex = LayerMask.NameToLayer("Character2");

			//print("1: " + LayerMask.LayerToName(character1LayerIndex));
			contactFilter.useTriggers = true;
			contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
			contactFilter.useLayerMask = true;

			characterAvoiderCF.layerMask &= 0;

			if (mystatus.faction == 0)
			{
				characterAvoiderCF.layerMask |= 1 << character1LayerIndex;
				characterAvoiderCF.layerMask |= 1 << character2LayerIndex;

				//characterAvoiderCF.SetLayerMask(Physics2D.GetLayerCollisionMask(LayerMask.NameToLayer("CharacterExiter0")));
				//currentCharacterExiterLayerIndex = LayerMask.NameToLayer("CharacterExiter0");
			}
			else if (mystatus.faction == 1)
			{
				characterAvoiderCF.layerMask |= 1 << character0LayerIndex;
				characterAvoiderCF.layerMask |= 1 <<  character2LayerIndex;
				//characterAvoiderCF.SetLayerMask(Physics2D.GetLayerCollisionMask(LayerMask.NameToLayer("CharacterExiter1")));
				//currentCharacterExiterLayerIndex = LayerMask.NameToLayer("CharacterExiter1");

			}
			else
			{
				//characterAvoiderCF.SetLayerMask(Physics2D.GetLayerCollisionMask(LayerMask.NameToLayer("CharacterExiter2")));
				//currentCharacterExiterLayerIndex = LayerMask.NameToLayer("CharacterExiter2");
				characterAvoiderCF.layerMask |= 1 << character0LayerIndex;
				characterAvoiderCF.layerMask |= 1 << character1LayerIndex;
			}
			characterAvoiderCF.useTriggers = true;
			characterAvoiderCF.useLayerMask = true;

			
			//jumpVelocity = 10;
			//jumpHeight = 3.2f;
			//timeToJumpApex = 0.3f;


			//platformCheckerCF.SetLayerMask (Physics2D.GetLayerCollisionMask (LayerMask.NameToLayer("PlatformChecker")));






			platformLayerMask |= 1 << platformLayerIndex;
			platformLayerMask |= 1 << movingPlatformLayerIndex;
			platformCheckerCF.SetLayerMask(platformLayerMask);
			platformCheckerCF.useTriggers = true;
			platformCheckerCF.useLayerMask = true;


			collisionCF.useTriggers = true;
			collisionCF.SetLayerMask(collisionMask);
			collisionCF.useLayerMask = true;

			groundMask |= 1 << platformLayerIndex;
			groundMask |= 1 << movingPlatformLayerIndex;
			groundMask |= 1 << defaultLayerIndex;
			groundCF.useTriggers = true;
			groundCF.SetLayerMask(groundMask);
			groundCF.useLayerMask = true;

			upwardCF.useTriggers = true;
			upwardCF.SetLayerMask(upwardCollisionMask);
			upwardCF.useLayerMask = true;

			groundCheckCF.useTriggers = true;
			groundCheckCF.SetLayerMask(groundMask);
			groundCheckCF.useLayerMask = true;
			groundCheckCF.layerMask |= characterAvoiderCF.layerMask;

			noPlatformsGroundCheckCF.useTriggers = true;
			noPlatformsGroundCheckCF.SetLayerMask(upwardCF.layerMask);
			noPlatformsGroundCheckCF.useLayerMask = true;
			noPlatformsGroundCheckCF.layerMask |= characterAvoiderCF.layerMask;

			horizontalCF.useTriggers = true;
			//horizontalCF.SetLayerMask(horizontalCollisionMask);
			horizontalCF.layerMask &= 0;
			horizontalCF.useLayerMask = true;


			//if (gameObject.layer == LayerMask.NameToLayer("Character"))
			//if (myTag == "Character")
			isCharacter = true;
			//else
			//  isCharacter = false;
			return false;
		}

		else if (preAwakeIndex == 1)
		{
			PhysicsInitialization();
			return false;
		}

		else if (preAwakeIndex > 1 && preAwakeIndex <= 8)
		{
			MyStart(preAwakeIndex, backgroundAwaker);
			if (preAwakeIndex == 8)
			{
				//print("finito");
				return true;
			}
			return false;
		}
		//print("FINITO");
		return true;
    }

	public virtual void MyOnEnable() {

	}
	
    protected virtual void MyStart(int preAwakeIndex, BackgroundAwaker backgroundAwaker)
    {

    }

    private void FixedUpdate()
    {
		//float timeStart = Time.realtimeSinceStartup;
		if (logicEnabled && pm.doneLoading && (backgroundAwaker == null || backgroundAwaker.mySceneLoader.activated))
		{
			if (Time.timeScale == 1)
            {
				UpdateAnimatorMode(true);
            }

			if (pm.player1 != this)
				Movement();
			else
			{
				if (debugMode)
					SimpleMovement();
				else
					Movement();
			}
		}
		//velocity.x = 6;
		//tempVelocity.x = 6;
		//Move(tempVelocity * Time.fixedDeltaTime);
        //print("time: " + (Time.realtimeSinceStartup - timeStart));
    }

	protected virtual void SimpleMovement() //funzione di debugging
    {
		
    }

    protected virtual void MyUpdate ()
    {

    }

    

    private void Update()
    {
		if (pm.doneLoading && (backgroundAwaker == null || backgroundAwaker.mySceneLoader.activated))
		{
			MyUpdate();
			
			
			if (Time.timeScale != 1)
			{
				UpdateAnimatorMode(false);
				//print("sono " + gameObject);
				//Move(tempVelocity * Time.deltaTime);
			}
			
		}

    }

	protected virtual void UpdateAnimatorMode (bool onFixedUpdate)
    {

    }



    protected virtual void Movement () {
		//QualitySettings.vSyncCount = 0;

		//if (!TimeVariables.canExecuteLogic)
			//return;

        //UpdateLayer();
        //movingPlatformVelocity = new Vector2(0, 0);

		/*
        if (changesGravityWhenDescending) {
            if (mystatus.myPhysicsStatus == Status.physicsStatus.airborne)
            {
                
                //se sono in aria e sono andato in su, allora jumpingstate è vero per tutta la durata del salto
                if (velocity.y > 0)
                {
                    jumpingState = true;

                    
                }
               
                if (jumpingState)
                {
                    if (mystatus.mystatus != Status.status.juggle)
                    {
                        

                        bool a = false;
                        for (int i = 0; i < jumpingGravitySections.Length; i++)
                        {
                            if (velocity.y < jumpingGravitySections[i].thresholdVelocity)
                            {
                                currentGravity = jumpingGravitySections[i].customGravity;
                                a = true;
								
                            }
                            else if (i == jumpingGravitySections.Length - 1 && a == false)
                            {
                               
                               
                                    currentGravity = jumpingGravity;
                                
                            }
                        }

                        
                        



                    }
                    else
                    {
                        for (int i = 0; i < juggleGravitySections.Length; i++)
                        {
                            if (velocity.y < juggleGravitySections[i].thresholdVelocity)
                                currentGravity = juggleGravitySections[i].customGravity;
                            else if (i == juggleGravitySections.Length)
                                currentGravity = jumpingGravity;
                        }
                    }
                }
                else
                {
                    //print("ma perchè, ma che dici");
                    currentGravity = normalGravity;

                }


            }

            else
            {
                currentGravity = jumpingGravity;
                jumpingState = false;
            }
                
		}
		*/

		platformsHit.Clear ();
		stairsHit.Clear ();
		fallingThroughPlatform = false;

        if (climbedPlatform == null && checksPlatforms)
		    CheckPlatformCollision();
		//fallingThroughPlatform = collisions.fallingThroughPlatform;
		if (platformsHit.Count > 0)
			fallingThroughPlatform = true;
		else
			fallingThroughPlatform = false;

		ManageTimeChanges(true);

		//attualeeeeEEEEEEEEEEEEEEEEeeeeeeeee
		if (TimeVariables.canExecuteLogic)
		{
			//print("big big chungus");
			if (TimeVariables.currentTimeScale != 1)
            {
				//if (mystatus.faction == 0)
				//print("ALORA");
            }
			ComputeVelocity();


			if (changesGravityWhenDescending)
			{
				if (mystatus.myPhysicsStatus == Status.physicsStatus.airborne)
				{

					//se sono in aria e sono andato in su, allora jumpingstate è vero per tutta la durata del salto
					if (velocity.y > 0)
					{
						jumpingState = true;


					}

					if (jumpingState)
					{
						if (mystatus.mystatus != Status.status.juggle)
						{


							bool a = false;
							for (int i = 0; i < jumpingGravitySections.Length; i++)
							{
								if (velocity.y < jumpingGravitySections[i].thresholdVelocity)
								{
									currentGravity = jumpingGravitySections[i].customGravity;
									a = true;

								}
								else if (i == jumpingGravitySections.Length - 1 && a == false)
								{


									currentGravity = jumpingGravity;

								}
							}






						}
						else
						{
							for (int i = 0; i < juggleGravitySections.Length; i++)
							{
								if (velocity.y < juggleGravitySections[i].thresholdVelocity)
									currentGravity = juggleGravitySections[i].customGravity;
								else if (i == juggleGravitySections.Length)
									currentGravity = jumpingGravity;
							}
						}
					}
					else
					{
						//print("ma perchè, ma che dici");
						currentGravity = normalGravity;

					}


				}

				else
				{
					currentGravity = jumpingGravity;
					jumpingState = false;
				}

			}

			if (isAffectedByGravity && !inHitstop)
			{

				//print("prima di grabbity: " + velocity.y);
				velocity.y += currentGravity * Time.fixedDeltaTime;// / TimeVariables.currentTimeScale;
				//print("grav: " + additiveGravity);
				//if (mystatus.faction ==1)
				//print ("grabbity: " + velocity.y);
			}
		}



		ManageTimeChanges(false);

		/*
        if (targetVelocity.x != 0)
            targetVelocity.x += Mathf.Sign(targetVelocity.x) * shellRadius;
        if (velocity.y != 0)
            velocity.y += Mathf.Sign(velocity.y) * shellRadius;
            */


		/*
		if (Input.GetKey (KeyCode.RightArrow))
			targetVelocity.x = 10;
*/










		//print ("prima: " + velocity.y);
		if (remainingRecoilFrames <= 0)
			recoil = 0f;
		//velocity.y += gravity * Time.deltaTime;
		//print("no grav: " + velocity.y);
		/*
		if (isAffectedByGravity && !inHitstop)
		{

			//print("prima di grabbity: " + velocity.y);
			velocity.y += currentGravity * Time.fixedDeltaTime / TimeVariables.currentTimeScale;
			//print("grav: " + additiveGravity);
			//if (mystatus.faction ==1)
			//print ("grabbity: " + velocity.y);
		}
		*/

		//print ("prima: " + velocity);
		//print("con grav: " + velocity.y);

		velocity.x = horizontalMomentum + targetVelocity.x - recoil;
		//print ("prima: " + velocity);

		if (!inHitstop) {
            //THISSSS
			//ExitObjects (ref velocity);
            //grounded = GroundCheck(velocity * Time.fixedDeltaTime);

            //velocity.x += exitObjects.movement.x;

        }

        if (!inHitstop)
        {
            //grounded = GroundCheck(ref velocity);

        }

        if (velocity.y < maxDescendingVelocity)
            velocity.y = maxDescendingVelocity;


		//tempVelocity = new Vector2(velocity.x, velocity.y);
		tempVelocity = velocity;
		/*
        GenericWallMover movingPlatform = null;

        if (objectUnderMe != null && objectUnderMe.tag == "moving platform")
        {
            //print("player: " + tempVelocity);
            movingPlatform = objectUnderMe.GetComponent<GenericWallMover>();
            if (movingPlatform != null)
            {
                tempVelocity.x += movingPlatform.totVelocity.x;
                //tempVelocity.y += movingPlatform.totVelocity.y;

                if (movingPlatform.totVelocity.y > 0)// && tempVelocity.y < movingPlatform.totVelocity.y)
                {
                    //tempVelocity.y += movingPlatform.totVelocity.y;
                    transform.Translate(0, movingPlatform.totVelocity.y * Time.fixedDeltaTime,0);
                    print("c'è qualcuno?");

                }
                else
                {
                    tempVelocity.y += movingPlatform.totVelocity.y;
                }

                

            }
        }
        */








		//print ("con grav + shell: " + velocity.y);

		if (timeManager == null || Time.timeScale == 1)
		{
			//Move(tempVelocity * Time.fixedDeltaTime);
			Move(tempVelocity * Time.fixedDeltaTime / TimeVariables.currentTimeScale);

		}
		//print("collisionsbelow: " + collisions.below);

		/*
        if (collisions.below)// || grounded)
        {

            //print("zero");
            if (!collisions.slidingDownMaxSlope && climbedPlatform == null)
                velocity.y = 0;
        }
		*/



		//if (movingPlatform != null)
		//  velocity.y -= Mathf.Sign(movingPlatform.totVelocity.y) * movingPlatform.totVelocity.y;

		//if (velocity.y != 0)
		//  velocity.y -= Mathf.Sign(velocity.y) * (shellRadius);

		//velocity.y -= (shellRadius * Mathf.Sin(collisions.slopeAngle) * Time.fixedDeltaTime) * Mathf.Sign(velocity.y);


		//Move (velocity * Time.fixedDeltaTime);
		//Debug.Log (collisions.slidingDownMaxSlope);
		if (remainingRecoilFrames > 0)
			remainingRecoilFrames--;

		if (remainingRecoilFrames <= 0)
			remainingRecoilFrames = 0;

		

        //ComputeVelocity();

        //print("sono player: " + Time.frameCount);
        
    }

    protected virtual void ManageTimeChanges(bool startOfFrame)
    {

    }

	protected virtual void ComputeVelocity () {

	}

    [System.Serializable]
	public struct CollisionInfo
	{
		public bool above, below;
		public bool left, right;
		public int faceDir;

		public bool climbingSlope;
		public bool descendingSlope;
		public bool slidingDownMaxSlope;
		public bool slidingUpMaxSlope;
		public bool fallingThroughPlatform;
		public int isIgnoringPlatforms;
		public int isIgnoringStairs;
		public bool standingOnPlatform;
		public bool standingOnStairs;


		public float slopeAngle, slopeAngleOld;
		public Vector2 slopeNormal;
		public Vector3 velocityOld;

		public void Reset () {
			above = below = false;
			right = left = false;
			climbingSlope = false;
			descendingSlope = false;
			slidingDownMaxSlope = false;
			slidingUpMaxSlope = false;
			fallingThroughPlatform = false;
			standingOnPlatform = false;
			standingOnStairs = false;
			slopeAngleOld = slopeAngle;
			slopeAngle = 0;
			slopeNormal = Vector2.zero;
			faceDir = 0;
		}
	}



    void SingleRayVerticalCollisions (ref Vector2 moveAmount)
    {
        RaycastHit2D[] hit;
        float minDistance = Mathf.Abs(moveAmount.y) + skinWidth;

        float minDistanceFound = minDistance;

        float directionY = Mathf.Sign(moveAmount.y);
        float rayLength = Mathf.Abs(moveAmount.y) + skinWidth;
        //string closestObjectLayer = "";

        if (moveAmount.y <= 0)
            hit = Physics2D.RaycastAll(new Vector2(transform.position.x, rayCastOrigins.bottomLeft.y), Vector2.up * directionY, rayLength, groundMask);
        else
            hit = Physics2D.RaycastAll(new Vector2(transform.position.x, rayCastOrigins.topLeft.y), Vector2.up * directionY, rayLength, groundMask);

        if (hit.Length != 0)
        {
            for (int j = 0; j < hit.Length; j++)
            {


                if (platformsHit.Contains(hit[j].collider))
                {

                    //print ("BAU");
                    continue;

                }

                if (stairsHit.Contains(hit[j].collider))
                {
                    continue;
                }

                float slopeAngle = Vector2.Angle(hit[j].normal, Vector2.up);
                string otherLayer = LayerMask.LayerToName(hit[j].collider.gameObject.layer);
                string otherLabel = hit[j].collider.tag;

                if (otherLayer == "Platform")
                {
                    //print ("porattoforumu");
                    if (collisions.isIgnoringPlatforms > 0)
                        continue;
                    if (hit[j].normal.y < 0)
                        continue;
                    //if (slopeAngle > maxSlopeAngle)
                    //continue;
                }

                if (collisions.isIgnoringStairs > 0 && otherLabel == "Stairs")
                {
                    continue;
                }

                



                /*
            if (hit.collider.tag == "Through") {
                if (directionY == 1 || hit.distance == 0) {
                    continue;
                }

                if (collisions.fallingThroughPlatform) {
                    continue;
                }
                if (playerInput.y == -1) {
                    collisions.fallingThroughPlatform = true;
                    Invoke("ResetFallingThroughPlatform",.5f);
                    continue;
                }

            }
*/
                if (hit[j].distance < minDistance)
                {
                    minDistance = hit[j].distance;
                    if (minDistance < minDistanceFound)
                    {
                        minDistanceFound = minDistance;
                        //closestObjectLayer = LayerMask.LayerToName (hit [j].collider.gameObject.layer);
                        collisions.slopeAngle = slopeAngle;
                    }

                }

                moveAmount.y = (minDistance - skinWidth) * directionY;
                rayLength = hit[j].distance;
                //rayLength = minDistance;


                if (collisions.climbingSlope)
                {
                    moveAmount.x = moveAmount.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveAmount.x);
                }

                collisions.below = directionY == -1;
                collisions.above = directionY == 1;
            }
        }

    }




	   protected virtual void Move (Vector2 velocity) {
        //Vector3 newVelocity = velocity;

        UpdateRayCastOrigins();
        collisions.Reset();


        //collisions.velocityOld = velocity;
        if (!inHitstop && exitsCharacters)
        {
            
            ExitCharacters(ref velocity);
            //grounded = GroundCheck(velocity);

            //velocity.x += exitObjects.movement.x;

        }
        
        grounded = GroundCheck(velocity);
        
        
        if (mystatus.mystatus == Status.status.juggle && inHitstop)
                grounded = false;

        //print("velocità prima: " + velocity.y / Time.fixedDeltaTime);

        GenericWallMover movingPlatform = null;

        if ((objectUnderMe != null && objectUnderMe.tag == "moving platform") || (climbedPlatform != null && climbedPlatform.tag == "moving platform"))
        {
            //print("under: " + objectUnderMe);
            if (climbedPlatform == null)
                movingPlatform = objectUnderMe.GetComponent<GenericWallMover>();
            else
                movingPlatform = climbedPlatform.GetComponent<GenericWallMover>();
            if (movingPlatform != null) 
            {
                if (velocity.y <= 0) //se sta saltando, non si deve fare spostare orizzontalmente; DA TESTARE
                    velocity.x += movingPlatform.totVelocity.x * Time.fixedDeltaTime;
                //tempVelocity.y += movingPlatform.totVelocity.y;
               
                if (movingPlatform.totVelocity.y > 0)// && tempVelocity.y < movingPlatform.totVelocity.y)
                {
                    //velocity.y += movingPlatform.totVelocity.y * Time.fixedDeltaTime;
                    transform.Translate(0, movingPlatform.totVelocity.y * Time.fixedDeltaTime, 0);
                    if (velocity.y > 0)
                        velocity.y += movingPlatform.totVelocity.y * Time.fixedDeltaTime; //DA TESTARE
                    

                    UpdateRayCastOrigins();
                    collisions.Reset();
                    //print("c'è qualcuno?");

                }
                else
                {
                    if (velocity.y <= 0)
                        velocity.y += movingPlatform.totVelocity.y * Time.fixedDeltaTime;
                }

                //print("velocità dopo: " + velocity.y / Time.fixedDeltaTime);

            }
        }

        
        collisions.velocityOld = velocity;




        /*
        if (Mathf.Abs(velocity.x) <= shellRadius)
            velocity.x = 0;
        if (Mathf.Abs(velocity.y) <= shellRadius)
            velocity.y = 0;
            */
        /*
		platformsHit.Clear ();
		stairsHit.Clear ();
	
		CheckPlatformCollision();
		fallingThroughPlatform = collisions.fallingThroughPlatform;
*/
        //print ("appena prima il dopoooo " + velocity.y);

        //if (!collisions.slidingDownMaxSlope && !inHitstop)



        /*
        if (!inHitstop) {
			grounded = GroundCheck (ref velocity);
            
		}

        
        GenericWallMover movingPlatform;
        if (objectUnderMe != null && objectUnderMe.tag == "moving platform")
        {
            movingPlatform = objectUnderMe.GetComponent<GenericWallMover>();
            if (movingPlatform != null)
            {
                velocity.x += movingPlatform.deltaMovement.x * Time.fixedDeltaTime;
                velocity.y += movingPlatform.deltaMovement.y * Time.fixedDeltaTime;
            }
        }

		if (mystatus.mystatus == Status.status.juggle && inHitstop)
			grounded = false;
        */

        //print ("appena prima il dopoooo " + velocity.y);

        if (velocity.y <= 0) {
			TESTDescendSlope (ref velocity);
			//print ("dopoooo: " + velocity.y);

			//DescendSlope (ref velocity);
		} else if (velocity.y > 0) {
			
			RaycastHit2D maxSlopeHitLeft = Physics2D.Raycast (rayCastOrigins.topLeft, Vector2.up, Mathf.Abs (velocity.y) + skinWidth, upwardCollisionMask);
			RaycastHit2D maxSlopeHitRight = Physics2D.Raycast (rayCastOrigins.topRight, Vector2.up, Mathf.Abs (velocity.y) + skinWidth, upwardCollisionMask);
			SlideUpMaxSlope (maxSlopeHitLeft, ref velocity);
			SlideUpMaxSlope (maxSlopeHitRight, ref velocity);

		}


		if (velocity.x != 0) {
			collisions.faceDir = (int)Mathf.Sign (velocity.x);
		}
			


		if (velocity.x != 0)
        {

            HorizontalCollisions (ref velocity);
            //CastHorizontalCollisions(ref velocity);

		}


        if (velocity.y != 0)
        {
            NEWTESTVerticalCollisions(ref velocity);
            //SingleRayVerticalCollisions(ref velocity);
            //CastVerticalCollisions(ref velocity);

        }

        //if (Mathf.Abs( velocity.y) <= 0.01f)
        //	velocity.y = 0;

        //if (Mathf.Abs(velocity.x) >= 0.01f)

        /*
        Vector2 tempVel = new Vector2(velocity.x, velocity.y);

        
        if (tempVel.x != 0)
            tempVel.x -= (shellRadius * Time.fixedDeltaTime) * Mathf.Sign(tempVel.x);
        if (tempVel.y != 0)
            tempVel.y -= (shellRadius * Time.fixedDeltaTime) * Mathf.Sign(tempVel.y);
            */

        if (velocity.x != 0)
        {
            //if (collisions.climbingSlope)
            {
                //print ("cos: " + (shellRadius * Mathf.Cos(collisions.slopeAngle)) * Mathf.Sign(velocity.x));
                //velocity.x -= (shellRadius * Mathf.Cos(collisions.slopeAngle) * Time.fixedDeltaTime) * Mathf.Sign(velocity.x);
                //velocity.y -= (shellRadius * Mathf.Sin(collisions.slopeAngle) * Time.fixedDeltaTime) * Mathf.Sign(velocity.y);
                //print("climb");
            }
        }
        //if (velocity.y != 0)
          //  velocity.y -= (shellRadius * Time.fixedDeltaTime) * Mathf.Sign(velocity.y);


        //if (Mathf.Abs(velocity.y) >= 0.01f)
        /*
        if (tempVel.y != 0)
            tempVel.y -= shellRadius * Mathf.Sign(tempVel.y);
            */

        //print("dopo: " + velocity.y / Time.fixedDeltaTime);
        transform.Translate (velocity);	

		//print (jumpDuration);
		isExiting = false;

        if (collisions.below)// || grounded)
        {

            print("zero");
            if (!collisions.slidingDownMaxSlope && climbedPlatform == null)
                velocity.y = 0;
        }



    }



	protected virtual void ExitCharacters (ref Vector2 velocity) {

        

		int count = genericCollider.OverlapCollider (characterAvoiderCF, hitBuffer);
		Vector3 movement;
		//print ("sono " + gameObject + " " + count);
		for (int i=0; i<count; i++) {
			//print ("Bau");

			//string otherLayer = LayerMask.LayerToName (hitBuffer[i].gameObject.layer);


            /*
			if (myLayer == "Character" && otherLayer == "Default") {
				movement = new Vector3(Mathf.Sign (horizontalExitCollider.bounds.center.x - hitBuffer [i].bounds.center.x) * (-transform.localScale.x)  * exitVelocity, 0);
				//print (movement);
				if (Mathf.Abs(movement.x) == 0)
					//movement.x = Mathf.Sign(Random.value - 0.5f) * (-0.08f);
					movement.x = -exitVelocity;
				velocity.x += movement.x;
				isExiting = true;


*/
            //uscita verticale su oggetti Default
            /*
            movement = new Vector3(0, Mathf.Sign (horizontalExitCollider.bounds.center.y - hitBuffer [i].bounds.center.y) * 0.18f);
            print (movement);

            if (Mathf.Abs(movement.x) == 0f)
                movement.y = Mathf.Sign(Random.value - 0.5f) * exitVelocity;
            velocity.y = movement.y;


        }

*/

            //if ((otherLayer == "CharacterAvoider" && !avoidsCharacters)) {
            if ((!avoidingCharacters))
            {
                //la tag attaccata all'object rappresenta la fazione, senza ricorrere a getcomponent
                bool sameFaction = hitBuffer[i].gameObject.CompareTag(genericCollider.tag);

                //Controller2D otherPhysics = hitBuffer[i].gameObject.GetComponentInParent<Controller2D>();
				if (isCharacter) {
					
					//if (!otherPhysics.avoidsCharacters && otherPhysics.mystatus.faction != mystatus.faction) 
                    if (!sameFaction)
                    { 
						//movement = new Vector3(Mathf.Sign (horizontalExitCollider.bounds.center.x - hitBuffer [i].bounds.center.x) * exitVelocity, 0);
						movement = new Vector3(Mathf.Sign (gameObject.transform.position.x - hitBuffer [i].gameObject.transform.position.x) * exitVelocity * Time.fixedDeltaTime, 0);
						//print ("sono " + gameObject + " " + (gameObject.transform.position.x - hitBuffer [i].gameObject.transform.position.x));
						//print (movement);

						if (Mathf.Abs (gameObject.transform.position.x - hitBuffer [i].gameObject.transform.position.x) <= 0.01f) {
							float randomValue = Mathf.Sign (Random.value - 0.5f);
							//print ("sono " + gameObject + " " + randomValue);
							movement.x = randomValue * (exitVelocity * Time.fixedDeltaTime);
							//print ("CIao");
						}
							//movement.x = - 30;
						velocity.x += movement.x;
						isExiting = true;
					}
				}
                else //if (!otherPhysics.avoidsCharacters)
                {
					//transform.Translate (new Vector2 (Mathf.Sign ((myCollider.bounds.center.x - hitBuffer [i].bounds.center.x)) * 0.01f, 0));
					//movement = new Vector3(Mathf.Sign (horizontalExitCollider.bounds.center.x - hitBuffer [i].bounds.center.x) * exitVelocity, 0);
					movement = new Vector3(Mathf.Sign (gameObject.transform.position.x - hitBuffer [i].gameObject.transform.position.x) * exitVelocity * Time.fixedDeltaTime, 0);

					//print (movement);

					if (Mathf.Abs (gameObject.transform.position.x - hitBuffer [i].gameObject.transform.position.x) <= 0.1f) {
						movement.x = Mathf.Sign (Random.value - 0.5f) * (exitVelocity * Time.fixedDeltaTime);
						print ("CIOA");
					}
						//movement.x = - 30;
					velocity.x += movement.x;
					isExiting = true;

				}
			}
		}
        
        
		count = genericCollider.OverlapCollider (characterAvoiderCF, hitBuffer);

		//print ("sono " + gameObject + " " + count);
		for (int i=0; i<count; i++) {
			//print ("Bau");

			//string otherLayer = LayerMask.LayerToName (hitBuffer[i].gameObject.layer);


            //if ((otherLayer == "CharacterAvoider" && !avoidsCharacters)) {
            if ((!avoidingCharacters))
            {

                //Controller2D otherPhysics = hitBuffer[i].gameObject.GetComponentInParent<Controller2D>();
                bool sameFaction = hitBuffer[i].gameObject.CompareTag(genericCollider.tag);
                //print(hitBuffer[i].gameObject);
                if (isCharacter) {

                    //if (!otherPhysics.avoidsCharacters && otherPhysics.mystatus.faction != mystatus.faction)
                    if (!sameFaction)
                    {
						//movement = new Vector3(Mathf.Sign (horizontalExitCollider.bounds.center.x - hitBuffer [i].bounds.center.x) * exitVelocity, 0);
						movement = new Vector3(Mathf.Sign (gameObject.transform.position.x - hitBuffer [i].gameObject.transform.position.x) * fastExitVelocity * Time.fixedDeltaTime, 0);
						//print ("sono " + gameObject + " " + (gameObject.transform.position.x - hitBuffer [i].gameObject.transform.position.x));
						//print (movement);

						if (Mathf.Abs (gameObject.transform.position.x - hitBuffer [i].gameObject.transform.position.x) <= 0.01f) {
							float randomValue = Mathf.Sign (Random.value - 0.5f);
							//print ("sono " + gameObject + " " + randomValue);
							movement.x = randomValue * (fastExitVelocity * Time.fixedDeltaTime);
							//print ("CIao");
						}
						//movement.x = - 30;
						velocity.x += movement.x;
						isExiting = true;
					}
				} else// if (!otherPhysics.avoidsCharacters)
                {
					//transform.Translate (new Vector2 (Mathf.Sign ((myCollider.bounds.center.x - hitBuffer [i].bounds.center.x)) * 0.01f, 0));
					//movement = new Vector3(Mathf.Sign (horizontalExitCollider.bounds.center.x - hitBuffer [i].bounds.center.x) * exitVelocity, 0);
					movement = new Vector3(Mathf.Sign (gameObject.transform.position.x - hitBuffer [i].gameObject.transform.position.x) * fastExitVelocity * Time.fixedDeltaTime, 0);

					//print (movement);

					if (Mathf.Abs (gameObject.transform.position.x - hitBuffer [i].gameObject.transform.position.x) <= 0.1f) {
						movement.x = Mathf.Sign (Random.value - 0.5f) * (fastExitVelocity * Time.fixedDeltaTime);
						//print ("CIOA");
					}
					//movement.x = - 30;
					velocity.x += movement.x;
					isExiting = true;

				}
			}
		}
        
        

	}


    //ATTUALE
	void HorizontalCollisions (ref Vector2 velocity) {

		float directionX = collisions.faceDir;

		//print (directionX);
		float rayLength = Mathf.Abs (velocity.x) + skinWidth;

		for (int i = 0; i < horizontalRayCount; i++) {
			Vector2 rayOrigin = (directionX == -1) ? rayCastOrigins.bottomLeft : rayCastOrigins.bottomRight;
			rayOrigin += Vector2.up * (horizontalRaySpacing * i);


            RaycastHit2D[] hit;
            //= Physics2D.RaycastAll (rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

			if (!collisions.slidingDownMaxSlope)
				hit = Physics2D.RaycastAll (rayOrigin, Vector2.right * directionX, rayLength, collisionMask);
			else 
				hit = Physics2D.RaycastAll (rayOrigin, Vector2.right * directionX, rayLength, groundMask);
			

			//print ("i = " + i + " " + hit.Length);
			Debug.DrawRay (rayOrigin, Vector2.right * directionX * rayLength, Color.red);

			//float minDistance = velocity.x;
			float minDistance = rayLength;
			//print ("length: " + hit.Length);
			if (hit.Length != 0) {
				for (int j = 0; j < hit.Length; j++) {


					bool otherAvoidsCharacters;
					CustomPhysics otherPhysics = hit[j].collider.gameObject.GetComponentInParent<CustomPhysics> ();

					if (otherPhysics != null)
						otherAvoidsCharacters = otherPhysics.avoidingCharacters;
					else
						otherAvoidsCharacters = false;
					string otherLayer = LayerMask.LayerToName (hit[j].collider.gameObject.layer);
					string otherLabel = hit [j].collider.tag;

					//print ("orizzontale, layer: " + otherLayer + ", distanza: " + hit [j].distance);

					/*
					if (i != 0 && otherLayer == "Platform") {
						//print ("BUA");
						continue;
					}
*/
					if (platformsHit.Contains(hit[j].collider)) {
						//print ("BUA");
						continue;
					}

					if (stairsHit.Contains(hit[j].collider)) {
						//print ("BUA");
						continue;
					}

					float slopeAngle = Vector2.Angle (hit[j].normal, Vector2.up);


					if (otherLayer == "Platform") {
						if (collisions.isIgnoringPlatforms > 0)
							continue;
						if (hit [j].normal.y < 0) {
							//print ("A");
							platformsHit.Add(hit[j].collider);
							continue;

						}
						if (slopeAngle > maxSlopeAngle) {
							//print ("B");
							platformsHit.Add(hit[j].collider);
							continue;

						}
					}

					if (collisions.isIgnoringStairs > 0 && otherLabel == "Stairs")
						continue;

					bool zulul = false;
					//print (hit [j].collider.gameObject + " distanza: " + hit [j].distance);

					if (myLayer == "Character") {
						if (otherLayer == "Character") {
							if (!avoidingCharacters && !otherAvoidsCharacters) {
								zulul = true;
								//print ("frame: " + Time.frameCount + " 1");
								if (isExiting && hit[j].distance < 0.01f)
									zulul = false;
								//print ("1");
							}
						} else {
							if (!otherAvoidsCharacters) {
								zulul = true;
								//print ("frame: " + Time.frameCount + " 2");

							}

						}
					} else {
						if (otherLayer != "Character")
							zulul = true;
						else {
							if (!avoidingCharacters)
								zulul = true;
						}
					}

					if (otherLayer == "Character" && otherPhysics.mystatus.faction == mystatus.faction) {
						zulul = false;
						//print ("frame: " + Time.frameCount + " 3");
					}

					//if (otherLayer == "Character")
					//	zulul = false;

					//print (zulul);
					//print ("frame: " + Time.frameCount + " " + zulul);

					//print ("frame: " + Time.frameCount + " rayLength: " + rayLength + " i = " + i + ", j = " + j + " " + hit[j].collider.gameObject);

					if (zulul) {
						//print ("i = " + i + ", j = " + j + " " + hit[j].collider.gameObject);

				
						//print (targetVelocity.x - (hit.distance - skinWidth));


						//float slopeAngle = Vector2.Angle (hit[j].normal, Vector2.up);

						if (i == 0 && slopeAngle <= maxSlopeAngle) {

							//print ("bau");
							if (collisions.descendingSlope) {
								collisions.descendingSlope = false;
								velocity = collisions.velocityOld;
							}
							float distanceToSlopeStart = 0;
							if (slopeAngle != collisions.slopeAngleOld) {
								distanceToSlopeStart = hit[j].distance - skinWidth;
								velocity.x -= distanceToSlopeStart * directionX;
							}
							//print ("bau");
							ClimbSlope (ref velocity, slopeAngle);
							velocity.x += distanceToSlopeStart * directionX;
						}

						if (!collisions.climbingSlope || slopeAngle > maxSlopeAngle) {	

							//if (otherLayer == "Platform") {
							if (platformsHit.Contains(hit[j].collider)) {
								//print ("BUA");
								continue;
							}

							if (stairsHit.Contains(hit[j].collider)) {
								//print ("BUA");
								continue;
							}
							//velocity.x = Mathf.Min(Mathf.Abs(velocity.x), (hit.distance - skinWidth)) * directionX;
							//rayLength = Mathf.Min(Mathf.Abs(velocity.x) + skinWidth, hit.distance);

							/*
							if (myLayer == "Character" && (mystatus.mystatus == Status.status.hitstun1 || mystatus.mystatus == Status.status.blockstun1 ||
							   mystatus.mystatus == Status.status.juggle)) {

								//if (hurtboxhit.offenderController.inHitstop == false) 
								{
									//print (targetVelocity.x);
									//hurtboxhit.offenderController.targetVelocity.x -= hurtboxhit.offenderController.collisions.faceDir * (targetVelocity.x - (hit.distance - skinWidth));
									//hurtboxhit.offenderController.recoil = targetVelocity.x; // - (hit.distance - skinWidth);
									if (mystatus.mystatus == Status.status.hitstun1)
										hurtboxhit.offenderController.recoil = hurtboxhit.offenderMoveSet.pushbackHit;
									else if (mystatus.mystatus == Status.status.blockstun1)
										hurtboxhit.offenderController.recoil = hurtboxhit.offenderMoveSet.pushbackBlock;



									//print ("recoil: " + hurtboxhit.offenderController.recoil);
									//if (hurtboxhit.offenderController.inHitstop == false) {
									if (mystatus.mystatus == Status.status.hitstun1 || mystatus.mystatus == Status.status.blockstun1) {
										//print ("i=" + i + ", remainingrecoil: " + hurtboxhit.offenderController.remainingRecoilFrames);

										if (hurtboxhit.offenderMoveSet.pushbackDuration >= hurtboxhit.controller.seq && hurtboxhit.offenderController.remainingRecoilFrames <= 0) {
											//print ("remainingrecoil: " + hurtboxhit.offenderController.remainingRecoilFrames);
											print ("i=" + i + ", remainingrecoil: " + hurtboxhit.offenderController.remainingRecoilFrames);

											hurtboxhit.offenderController.remainingRecoilFrames = (hurtboxhit.offenderMoveSet.pushbackDuration + 1) - hurtboxhit.controller.seq;

										}
										//print ("seq: " + hurtboxhit.controller.seq);
										//print ("1");
									} else if (mystatus.mystatus == Status.status.juggle) {

										if (hurtboxhit.offenderController.remainingRecoilFrames <= 0 && hurtboxhit.controller.seq < 10) {
											hurtboxhit.offenderController.remainingRecoilFrames = 10;
											print ("remaining recoil frames = " + hurtboxhit.offenderController.remainingRecoilFrames);


										}
										//print ("seq = " + hurtboxhit.controller.seq);
										//if (hurtboxhit.offenderController.remainingRecoilFrames <= 0)
										//	hurtboxhit.offenderController.remainingRecoilFrames = 0;
									}
								
								}
							}

							*/

							if (zulul) 
							{
								
								if (hit [j].distance < minDistance)
									minDistance = hit [j].distance;
							}
				
							velocity.x = (minDistance - skinWidth) * directionX;
							//velocity.x = (minDistance) * directionX;


							rayLength = hit[j].distance;
							//print (hit[j].distance);

							if (collisions.climbingSlope) {
								velocity.y = Mathf.Tan (collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs (velocity.x);
							}

							collisions.left = directionX == -1;
							collisions.right = directionX == 1;
						}
					}
				}
			}

		}
	}

    void CastHorizontalCollisions(ref Vector2 velocity)
    {
		/*
        float directionX = collisions.faceDir;

        //print (directionX);
        float rayLength = Mathf.Abs(velocity.x) + skinWidth;

        //for (int i = 0; i < horizontalRayCount; i++)
        {
            Vector2 rayOrigin = (directionX == -1) ? rayCastOrigins.bottomLeft : rayCastOrigins.bottomRight;
            //rayOrigin += Vector2.up * (horizontalRaySpacing * i);


            RaycastHit2D[] hit = new RaycastHit2D [30];
            //= Physics2D.RaycastAll (rayOrigin, Vector2.right * directionX, rayLength, collisionMask);
            int count = 0;

            if (!collisions.slidingDownMaxSlope)
                count = myCollider.Cast(Vector2.right * directionX, collisionCF, hit, rayLength);
            else
                count = myCollider.Cast(Vector2.right * directionX, groundCF, hit, rayLength);


            //Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);

            //float minDistance = velocity.x;
            float minDistance = rayLength;
            //print ("length: " + hit.Length);
            if (count > 0)
            {
                for (int j = 0; j < count; j++)
                {


                    bool otherAvoidsCharacters;
                    CustomPhysics otherPhysics = hit[j].collider.gameObject.GetComponentInParent<CustomPhysics>();

                    if (otherPhysics != null)
                        otherAvoidsCharacters = otherPhysics.avoidingCharacters;
                    else
                        otherAvoidsCharacters = false;
                    string otherLayer = LayerMask.LayerToName(hit[j].collider.gameObject.layer);
                    string otherLabel = hit[j].collider.tag;

                    //print ("orizzontale, layer: " + otherLayer + ", distanza: " + hit [j].distance);

                    /*
					if (i != 0 && otherLayer == "Platform") {
						//print ("BUA");
						continue;
					}

                    if (platformsHit.Contains(hit[j].collider))
                    {
                        //print ("BUA");
                        continue;
                    }

                    if (stairsHit.Contains(hit[j].collider))
                    {
                        //print ("BUA");
                        continue;
                    }

                    float slopeAngle = Vector2.Angle(hit[j].normal, Vector2.up);


                    if (otherLayer == "Platform")
                    {
                        if (collisions.isIgnoringPlatforms > 0)
                            continue;
                        if (hit[j].normal.y < 0)
                        {
                            //print ("A");
                            platformsHit.Add(hit[j].collider);
                            continue;

                        }
                        if (slopeAngle > maxSlopeAngle)
                        {
                            //print ("B");
                            platformsHit.Add(hit[j].collider);
                            continue;

                        }
                    }

                    if (collisions.isIgnoringStairs > 0 && otherLabel == "Stairs")
                        continue;

                    bool zulul = false;
                    //print (hit [j].collider.gameObject + " distanza: " + hit [j].distance);

                    if (myLayer == "Character")
                    {
                        if (otherLayer == "Character")
                        {
                            if (!avoidingCharacters && !otherAvoidsCharacters)
                            {
                                zulul = true;
                                //print ("frame: " + Time.frameCount + " 1");
                                if (isExiting && hit[j].distance < 0.01f)
                                    zulul = false;
                                //print ("1");
                            }
                        }
                        else
                        {
                            if (!otherAvoidsCharacters)
                            {
                                zulul = true;
                                //print ("frame: " + Time.frameCount + " 2");

                            }

                        }
                    }
                    else
                    {
                        if (otherLayer != "Character")
                            zulul = true;
                        else
                        {
                            if (!avoidingCharacters)
                                zulul = true;
                        }
                    }

                    if (otherLayer == "Character" && otherPhysics.mystatus.faction == mystatus.faction)
                    {
                        zulul = false;
                        //print ("frame: " + Time.frameCount + " 3");
                    }

                    //if (otherLayer == "Character")
                    //	zulul = false;

                    //print (zulul);
                    //print ("frame: " + Time.frameCount + " " + zulul);

                    //print ("frame: " + Time.frameCount + " rayLength: " + rayLength + " i = " + i + ", j = " + j + " " + hit[j].collider.gameObject);

                    if (zulul)
                    {
                        //print ("i = " + i + ", j = " + j + " " + hit[j].collider.gameObject);


                        //print (targetVelocity.x - (hit.distance - skinWidth));


                        //float slopeAngle = Vector2.Angle (hit[j].normal, Vector2.up);



                        //if (i == 0 && slopeAngle <= maxSlopeAngle)
                        if (slopeAngle <= maxSlopeAngle)

                        {

                            //print ("bau");
                            if (collisions.descendingSlope)
                            {
                                collisions.descendingSlope = false;
                                velocity = collisions.velocityOld;
                            }
                            float distanceToSlopeStart = 0;
                            if (slopeAngle != collisions.slopeAngleOld)
                            {
                                distanceToSlopeStart = hit[j].distance - skinWidth;
                                velocity.x -= distanceToSlopeStart * directionX;
                            }
                            //print ("bau");
                            ClimbSlope(ref velocity, slopeAngle);
                            velocity.x += distanceToSlopeStart * directionX;
                        }
                        

                        if (!collisions.climbingSlope || slopeAngle > maxSlopeAngle)
                        {

                            //if (otherLayer == "Platform") {
                            if (platformsHit.Contains(hit[j].collider))
                            {
                                //print ("BUA");
                                continue;
                            }

                            if (stairsHit.Contains(hit[j].collider))
                            {
                                //print ("BUA");
                                continue;
                            }
                            //velocity.x = Mathf.Min(Mathf.Abs(velocity.x), (hit.distance - skinWidth)) * directionX;
                            //rayLength = Mathf.Min(Mathf.Abs(velocity.x) + skinWidth, hit.distance);

                            /*
							if (myLayer == "Character" && (mystatus.mystatus == Status.status.hitstun1 || mystatus.mystatus == Status.status.blockstun1 ||
							   mystatus.mystatus == Status.status.juggle)) {

								//if (hurtboxhit.offenderController.inHitstop == false) 
								{
									//print (targetVelocity.x);
									//hurtboxhit.offenderController.targetVelocity.x -= hurtboxhit.offenderController.collisions.faceDir * (targetVelocity.x - (hit.distance - skinWidth));
									//hurtboxhit.offenderController.recoil = targetVelocity.x; // - (hit.distance - skinWidth);
									if (mystatus.mystatus == Status.status.hitstun1)
										hurtboxhit.offenderController.recoil = hurtboxhit.offenderMoveSet.pushbackHit;
									else if (mystatus.mystatus == Status.status.blockstun1)
										hurtboxhit.offenderController.recoil = hurtboxhit.offenderMoveSet.pushbackBlock;



									//print ("recoil: " + hurtboxhit.offenderController.recoil);
									//if (hurtboxhit.offenderController.inHitstop == false) {
									if (mystatus.mystatus == Status.status.hitstun1 || mystatus.mystatus == Status.status.blockstun1) {
										//print ("i=" + i + ", remainingrecoil: " + hurtboxhit.offenderController.remainingRecoilFrames);

										if (hurtboxhit.offenderMoveSet.pushbackDuration >= hurtboxhit.controller.seq && hurtboxhit.offenderController.remainingRecoilFrames <= 0) {
											//print ("remainingrecoil: " + hurtboxhit.offenderController.remainingRecoilFrames);
											print ("i=" + i + ", remainingrecoil: " + hurtboxhit.offenderController.remainingRecoilFrames);

											hurtboxhit.offenderController.remainingRecoilFrames = (hurtboxhit.offenderMoveSet.pushbackDuration + 1) - hurtboxhit.controller.seq;

										}
										//print ("seq: " + hurtboxhit.controller.seq);
										//print ("1");
									} else if (mystatus.mystatus == Status.status.juggle) {

										if (hurtboxhit.offenderController.remainingRecoilFrames <= 0 && hurtboxhit.controller.seq < 10) {
											hurtboxhit.offenderController.remainingRecoilFrames = 10;
											print ("remaining recoil frames = " + hurtboxhit.offenderController.remainingRecoilFrames);


										}
										//print ("seq = " + hurtboxhit.controller.seq);
										//if (hurtboxhit.offenderController.remainingRecoilFrames <= 0)
										//	hurtboxhit.offenderController.remainingRecoilFrames = 0;
									}
								
								}
							}

							

                            if (zulul)
                            {

                                if (hit[j].distance < minDistance)
                                    minDistance = hit[j].distance;
                            }

                            velocity.x = (minDistance - skinWidth) * directionX;
                            //velocity.x = (minDistance) * directionX;


                            rayLength = hit[j].distance;
                            //print (hit[j].distance);

                            if (collisions.climbingSlope)
                            {
                                velocity.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x);
                            }

                            collisions.left = directionX == -1;
                            collisions.right = directionX == 1;
                        }
                    }
                }
            }

        }*/
    }




    void TESTHorizontalCollisions (ref Vector2 moveAmount) {
		{
			float directionX = collisions.faceDir;
			float rayLength = Mathf.Abs (moveAmount.x) + skinWidth;

			if (Mathf.Abs(moveAmount.x) < skinWidth) {
				rayLength = 2*skinWidth;
			}

			for (int i = 0; i < horizontalRayCount; i ++) {
				Vector2 rayOrigin = (directionX == -1)?rayCastOrigins.bottomLeft:rayCastOrigins.bottomRight;
				rayOrigin += Vector2.up * (horizontalRaySpacing * i);
				RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

				Debug.DrawRay(rayOrigin, Vector2.right * directionX,Color.red);

				if (hit) {

					if (hit.distance == 0) {
						continue;
					}

					float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

					if (i == 0 && slopeAngle <= maxSlopeAngle) {
						if (collisions.descendingSlope) {
							collisions.descendingSlope = false;
							moveAmount = collisions.velocityOld;
						}
						float distanceToSlopeStart = 0;
						if (slopeAngle != collisions.slopeAngleOld) {
							distanceToSlopeStart = hit.distance-skinWidth;
							moveAmount.x -= distanceToSlopeStart * directionX;
						}
						TESTClimbSlope(ref moveAmount, slopeAngle, hit.normal);
						moveAmount.x += distanceToSlopeStart * directionX;
					}

					if (!collisions.climbingSlope || slopeAngle > maxSlopeAngle) {
						moveAmount.x = (hit.distance - skinWidth) * directionX;
						rayLength = hit.distance;

						if (collisions.climbingSlope) {
							moveAmount.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x);
						}

						collisions.left = directionX == -1;
						collisions.right = directionX == 1;
					}
				}
			}
		}
	}

	void NEWTESTHorizontalCollisions (ref Vector2 moveAmount) {
		{
			float directionX = collisions.faceDir;
			float rayLength = Mathf.Abs (moveAmount.x) + skinWidth;

			if (Mathf.Abs(moveAmount.x) < skinWidth) {
				rayLength = 2*skinWidth;
			}

			for (int i = 0; i < horizontalRayCount; i ++) {
				Vector2 rayOrigin = (directionX == -1)?rayCastOrigins.bottomLeft:rayCastOrigins.bottomRight;
				rayOrigin += Vector2.up * (horizontalRaySpacing * i);
				RaycastHit2D [] hit = Physics2D.RaycastAll(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

				Debug.DrawRay(rayOrigin, Vector2.right * directionX,Color.red);

					float minDistance = rayLength;
					//print ("length: " + hit.Length);
					if (hit.Length != 0) {
						for (int j = 0; j < hit.Length; j++) {

						if (hit[j].distance == 0) {
						continue;
						}

						float slopeAngle = Vector2.Angle(hit[j].normal, Vector2.up);

					if (i == 0 && slopeAngle <= maxSlopeAngle) {
						if (collisions.descendingSlope) {
							collisions.descendingSlope = false;
							moveAmount = collisions.velocityOld;
						}
						float distanceToSlopeStart = 0;
						if (slopeAngle != collisions.slopeAngleOld) {
								distanceToSlopeStart = hit[j].distance-skinWidth;
							moveAmount.x -= distanceToSlopeStart * directionX;
						}
							TESTClimbSlope(ref moveAmount, slopeAngle, hit[j].normal);
						moveAmount.x += distanceToSlopeStart * directionX;
					}

					if (!collisions.climbingSlope || slopeAngle > maxSlopeAngle) {
							moveAmount.x = (hit[j].distance - skinWidth) * directionX;
							rayLength = hit[j].distance;

						if (collisions.climbingSlope) {
							moveAmount.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x);
						}

						collisions.left = directionX == -1;
						collisions.right = directionX == 1;
					}
				}
			}
		}
	}
	}

	void VerticalCollisions (ref Vector2 velocity) {
		float directionY = Mathf.Sign (velocity.y);

		/*
		bbbVector2 rayOrigin = (directionY == -1) ? rayCastOrigins.bottomLeft : rayCastOrigins.topLeft;

		RaycastHit2D[] objectsHit = Physics2D.RaycastAll (rayOrigin, Vector2.right, myCollider.size.x, collisionMask);

		if (objectsHit.Length > 0) {
		bbbb
			//velocity.y = 0;
			//print ("zulul");
		}
*/

		float rayLength = Mathf.Abs (velocity.y) + skinWidth;

		for (int i = 0; i < verticalRayCount; i++) {
			Vector2 rayOrigin = (directionY == -1) ? rayCastOrigins.bottomLeft : rayCastOrigins.topLeft;

			rayOrigin += Vector2.right * (verticalRaySpacing * i + velocity.x);

			float minDistance = rayLength;


			//RaycastHit2D hit = Physics2D.Raycast (rayOrigin, Vector2.up * directionY, rayLength, collisionMask);
			RaycastHit2D[] hit = Physics2D.RaycastAll (rayOrigin, Vector2.up * directionY, rayLength, collisionMask);


			Debug.DrawRay (rayOrigin, Vector2.up * directionY * rayLength, Color.red);

			if (hit.Length > 0) {
				for (int j = 0; j < hit.Length; j++) {
				bool otherAvoidsCharacters;
					CustomPhysics otherPhysics = hit[j].collider.gameObject.GetComponent<CustomPhysics> ();

				if (otherPhysics != null) {
					otherAvoidsCharacters = otherPhysics.avoidingCharacters;
				} else {
					otherAvoidsCharacters = false;
				}

					string otherLayer = LayerMask.LayerToName (hit[j].collider.gameObject.layer);

				 
					bool zulul = false;

				if (myLayer != "Character" && otherLayer != "Character") {
					zulul = true;

				}
				else if (!(myLayer == "Character" && otherLayer == "Character")) {
					if (!avoidingCharacters && !otherAvoidsCharacters) {
						zulul = true;

					} else if (myLayer == "Character" && !otherAvoidsCharacters) {
						zulul = true;

					}
				}
					
				//Debug.Log (zulul);

					if (zulul) 
					{

						if (hit [j].distance < minDistance)
							minDistance = hit [j].distance;
						velocity.y = (minDistance - skinWidth) * directionY;

						//velocity.y = (hit[j].distance - skinWidth) * directionY;
						//rayLength = hit[j].distance;

						if (collisions.climbingSlope) {
							velocity.x = velocity.y / Mathf.Tan (collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign (velocity.x);
						}

						collisions.below = directionY == -1;
						collisions.above = directionY == 1;
					}

			}
				
		}


		if (collisions.climbingSlope) {
			float directionX = Mathf.Sign (velocity.x);
			rayLength = Mathf.Abs (velocity.x) + skinWidth;
			rayOrigin = ((directionX == -1) ? rayCastOrigins.bottomLeft:rayCastOrigins.bottomRight) + Vector2.up * velocity.y;
			RaycastHit2D objectsHit = Physics2D.Raycast (rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

				if (objectsHit) {
					float slopeAngle = Vector2.Angle (objectsHit.normal, Vector2.up);
				if (slopeAngle != collisions.slopeAngle) {
						velocity.x = (objectsHit.distance - skinWidth) * directionX;
					collisions.slopeAngle = slopeAngle;
				}
			}
		}
		}
	}

	protected virtual bool GroundCheck (Vector2 velocity) {


        objectUnderMe = null;
		//calcola la distanza dal suolo
		for (int i = 0; i < verticalRayCount; i++) {
			Vector2 rayOrigin = rayCastOrigins.bottomLeft;

			RaycastHit2D hit = Physics2D.Raycast (rayOrigin, -Vector2.up, groundDistanceMax, groundMask);
			currentGroundDistance = groundDistanceMax;

			if (hit) {
				float slopeAngle = Vector2.Angle (hit.normal, Vector2.up);
				//print (velocity.y);
				if (slopeAngle < maxSlopeAngle && hit.distance < currentGroundDistance) {
					currentGroundDistance = hit.distance;

				}

			}
		}

		float minDistanceFound = groundShell;
		string closestObjectLayer = "";
		bool groundCheckResult = false;
		Collider2D closestObject = null;

		for (int i = 0; i < verticalRayCount; i++) {


			Vector2 rayOrigin = rayCastOrigins.bottomLeft;
            float currentGroundShell = velocity.y >= 0 ? groundShell : Mathf.Max(groundShell, Mathf.Abs(velocity.y));

			rayOrigin += Vector2.right * (verticalRaySpacing * i);
			RaycastHit2D [] hit;
			if (collisions.isIgnoringPlatforms <= 0)
				hit = Physics2D.RaycastAll (rayOrigin, -Vector2.up, currentGroundShell, groundMask);
			else
				hit = Physics2D.RaycastAll (rayOrigin, -Vector2.up, currentGroundShell, upwardCollisionMask);
            //print("groundcheck: " + hit.Length + ", shell: " + currentGroundShell);

			//print (collisions.standingOnPlatform);


			float minDistance = currentGroundShell;

			Debug.DrawRay (rayOrigin, -Vector2.up * currentGroundShell, Color.blue);
			if (hit.Length > 0) {
				for (int j = 0; j < hit.Length; j++) {

                    
					if (platformsHit.Contains (hit [j].collider)) {
						//print ("removed!");

						//platformsHit.Remove (hit [j].collider);
						continue;


					}
					if (collisions.isIgnoringStairs > 0 && hit [j].collider.tag == "Stairs")
						continue;
					if (stairsHit.Contains(hit[j].collider)) {
						//print ("BUA");
						continue;
					}


					//if (collisions.isIgnoringStairs > 0 && hit [j].collider.tag == "Stairs")
					//	continue;
					
					float slopeAngle = Vector2.Angle (hit[j].normal, Vector2.up);
					//print (velocity.y);

					if (hit[j].collider.tag != "moving platform" && hit [j].distance < minDistance) {
						minDistance = hit [j].distance;

						if (minDistance < minDistanceFound) {
							minDistanceFound = minDistance;
							closestObject = hit [j].collider;
							closestObjectLayer = LayerMask.LayerToName (hit [j].collider.gameObject.layer);
						}
					}

                    if (hit[j].collider.tag == "moving platform")
                    {
                        //print("movingplatform found");
                        closestObject = hit[j].collider;
                        minDistanceFound = hit[j].distance;
                        closestObjectLayer = LayerMask.LayerToName(hit[j].collider.gameObject.layer);
                    }



                    //print("closest: " + closestObject);

                    if (slopeAngle < maxSlopeAngle && velocity.y <= 0)
                        //&& ((closestObject != null && closestObject.tag == "moving platform") || velocity.y <= 0)) 
                    //if (slopeAngle < maxSlopeAngle && velocity.y <= 0)  
                    {

                        //if (slopeAngle < maxSlopeAngle) {

                        //print("closest: " + closestObject);
                        groundCheckResult = true;
                        //objectUnderMe = closestObject;
                    }
                    else
                    {
                        //print("no, slopeangle: " + slopeAngle + " closest: " + closestObject + " velocity: " + velocity.y);
                        //objectUnderMe = null;
                    }
				}
                
				//return false;
			}
            else
            {
                //print("NO?");
                //objectUnderMe = null;
            }

		}
        objectUnderMe = closestObject;

        if (closestObjectLayer == "Platform") {
			collisions.standingOnPlatform = true;
			if (platformsHit.Contains (closestObject)) {
				platformsHit.Remove (closestObject);
				print ("removed");

			}
		}
		if (closestObject != null && closestObject.tag == "Stairs") {
			collisions.standingOnStairs = true;
			if (stairsHit.Contains (closestObject))
				stairsHit.Remove (closestObject);
		}



		//print (collisions.standingOnPlatform);

		if (collisions.slidingDownMaxSlope)
			return false;

		if (groundCheckResult)
			return true;
		return false;








		//print ("groundDistance = " + currentGroundDistance);
	}

	void CheckCharacterCollisions () {

	}

	void ClimbSlope (ref Vector2 velocity, float angle) {
		//print ("bau");




			float moveDistance = Mathf.Abs (velocity.x);
			float climbVelocityY = Mathf.Sin (angle * Mathf.Deg2Rad) * moveDistance;

			if (velocity.y <= climbVelocityY) {
				velocity.y = climbVelocityY;
				velocity.x = Mathf.Cos (angle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign (velocity.x);
				collisions.below = true;
				collisions.climbingSlope = true;
				collisions.slopeAngle = angle;
			}

	}

	void DescendSlope (ref Vector2 velocity) {

		RaycastHit2D maxSlopeHitLeft = Physics2D.Raycast (rayCastOrigins.bottomLeft, Vector2.down, Mathf.Abs (velocity.y) + skinWidth, groundMask);
		RaycastHit2D maxSlopeHitRight = Physics2D.Raycast (rayCastOrigins.bottomRight, Vector2.down, Mathf.Abs (velocity.y) + skinWidth, groundMask);
		SlideDownMaxSlope (maxSlopeHitLeft, ref velocity);
		SlideDownMaxSlope (maxSlopeHitRight, ref velocity);

		if (!collisions.slidingDownMaxSlope) {
			float directionX = Mathf.Sign (velocity.x);
			Vector2 rayOrigin = (directionX == -1) ? rayCastOrigins.bottomRight : rayCastOrigins.bottomLeft;
			RaycastHit2D hit = Physics2D.Raycast (rayOrigin, -Vector2.up, Mathf.Infinity, groundMask);

			if (hit) {
				float slopeAngle = Vector2.Angle (hit.normal, Vector2.up);
				if (slopeAngle != 0 && slopeAngle <= maxSlopeAngle) {
					if (Mathf.Sign (hit.normal.x) == directionX) {
						if (hit.distance - skinWidth <= Mathf.Tan (slopeAngle * Mathf.Deg2Rad) * Mathf.Abs (velocity.x)) {
							float moveDistance = Mathf.Abs (velocity.x);
							float descendVelocityY = Mathf.Sin (slopeAngle * Mathf.Deg2Rad) * moveDistance;
							velocity.x = Mathf.Cos (slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign (velocity.x);
							velocity.y -= descendVelocityY;

							collisions.slopeAngle = slopeAngle;
							collisions.descendingSlope = true;
							collisions.below = true;


						}
					}
				}
			}
		}
	}

	void SlideDownMaxSlope (RaycastHit2D hit, ref Vector2 velocity) {
		print ("XXX");
		if (hit) {
			float slopeAngle = Vector2.Angle (hit.normal, Vector2.up);
			string otherLayer = LayerMask.LayerToName (hit.collider.gameObject.layer);

			if (slopeAngle > maxSlopeAngle) {
				if (otherLayer == "Platform" || hit.collider.tag == "Stairs") {
					collisions.slidingDownMaxSlope = true;
                    print("slidingggggggg1");

					//return;

				}
				velocity.x = Mathf.Sign(hit.normal.x) * ((Mathf.Abs (velocity.y) - hit.distance) / Mathf.Tan (slopeAngle * Mathf.Deg2Rad));
				collisions.slopeAngle = slopeAngle;
				collisions.slidingDownMaxSlope = true;
                print("slidingggggggg2");
            }
		}
	}

	void TESTSlideDownMaxSlope(RaycastHit2D hit, ref Vector2 moveAmount) {

		if (hit) {
			float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
			if (slopeAngle > maxSlopeAngle) {
				
				moveAmount.x = Mathf.Sign(hit.normal.x) * (Mathf.Abs (moveAmount.y) - hit.distance) / Mathf.Tan (slopeAngle * Mathf.Deg2Rad);

				collisions.slopeAngle = slopeAngle;
				collisions.slidingDownMaxSlope = true;
				collisions.slopeNormal = hit.normal;
			}
		}

	}

    //ATTUALE
	void SlideUpMaxSlope (RaycastHit2D hit, ref Vector2 velocity) {
		if (hit) {
			float slopeAngle = Vector2.Angle (hit.normal, Vector2.down);
			if (slopeAngle > maxSlopeAngle) {
				//print ("bau");
				velocity.x = Mathf.Sign(hit.normal.x) * ((Mathf.Abs (velocity.y) - hit.distance) / Mathf.Tan (slopeAngle * Mathf.Deg2Rad));
				collisions.slopeAngle = slopeAngle;
				collisions.slidingUpMaxSlope = true;
			}
		}
	}

	protected void UpdateRayCastOrigins () {
		/*
		Bounds bounds = myCollider.bounds;
		bounds.Expand (skinWidth * -2);

		rayCastOrigins.bottomLeft = new Vector2 (bounds.min.x, bounds.min.y);
		rayCastOrigins.bottomRight = new Vector2 (bounds.max.x, bounds.min.y);
		rayCastOrigins.topLeft = new Vector2 (bounds.min.x, bounds.max.y);
		rayCastOrigins.topRight = new Vector2 (bounds.max.x, bounds.max.y);
		*/

	}

	void CalculateRaySpacing () { /*
		Bounds bounds = myCollider.bounds;
		bounds.Expand (skinWidth * -2);

		horizontalRayCount = Mathf.Clamp (horizontalRayCount, 2, int.MaxValue);
		verticalRayCount = Mathf.Clamp (verticalRayCount, 2, int.MaxValue);

		horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
		verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
		*/
	}

	public struct RayCastOrigins {
		public Vector2 topLeft, topRight;
		public Vector2 bottomLeft, bottomRight;
	}

	protected virtual void CheckPlatformCollision () {
		/*
        //print (hitBuffer[0]);
        //print("check");
		int counter = platformCollisionChecker.OverlapCollider (platformCheckerCF, hitBuffer);
		for (int i = 0; i < counter; i++) {
			if (hitBuffer [i].gameObject.layer == platformLayerIndex) {
				platformsHit.Add(hitBuffer[i]);
			}
			if (hitBuffer [i].gameObject.tag == "Stairs")
				stairsHit.Add (hitBuffer [i]);
		}
		//return false;
		*/
	}


	//TEST!!!

	void TESTVerticalCollisions(ref Vector2 moveAmount) {
		float directionY = Mathf.Sign (moveAmount.y);
		float rayLength = Mathf.Abs (moveAmount.y) + skinWidth;


		for (int i = 0; i < verticalRayCount; i ++) {

			Vector2 rayOrigin = (directionY == -1)?rayCastOrigins.bottomLeft:rayCastOrigins.topLeft;
			rayOrigin += Vector2.right * (verticalRaySpacing * i + moveAmount.x);
			RaycastHit2D hit;
			if (!collisions.fallingThroughPlatform) {
				if (directionY == 1)
					hit = Physics2D.Raycast (rayOrigin, Vector2.up * directionY, rayLength, upwardCollisionMask);
				else
					hit = Physics2D.Raycast (rayOrigin, Vector2.up * directionY, rayLength, groundMask);
				
			} else {
				
				hit = Physics2D.Raycast (rayOrigin, Vector2.up * directionY, rayLength, upwardCollisionMask);
			}

				/*
			if (directionY == 1)
				hit = Physics2D.Raycast (rayOrigin, Vector2.up * directionY, rayLength, upwardCollisionMask);
			else if (!collisions.fallingThroughPlatform)
				hit = Physics2D.Raycast (rayOrigin, Vector2.up * directionY, rayLength, groundMask);
			*/
				
			Debug.DrawRay(rayOrigin, Vector2.up * directionY,Color.red);

			if (hit) {

				if (platformsHit.Contains (hit.collider)) {

					//print ("BAU");
					continue;

				}

				/*
				if (hit.collider.tag == "Through") {
					if (directionY == 1 || hit.distance == 0) {
						continue;
					}

					if (collisions.fallingThroughPlatform) {
						continue;
					}
					if (playerInput.y == -1) {
						collisions.fallingThroughPlatform = true;
						Invoke("ResetFallingThroughPlatform",.5f);
						continue;
					}

				}
*/

				moveAmount.y = (hit.distance - skinWidth) * directionY;
				rayLength = hit.distance;

				if (collisions.climbingSlope) {
					moveAmount.x = moveAmount.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveAmount.x);
				}

				collisions.below = directionY == -1;
				collisions.above = directionY == 1;
			}
		}

		if (collisions.climbingSlope) {
			float directionX = Mathf.Sign(moveAmount.x);
			rayLength = Mathf.Abs(moveAmount.x) + skinWidth;
			Vector2 rayOrigin = ((directionX == -1)?rayCastOrigins.bottomLeft:rayCastOrigins.bottomRight) + Vector2.up * moveAmount.y;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin,Vector2.right * directionX,rayLength,groundMask);

			if (hit) {
				float slopeAngle = Vector2.Angle(hit.normal,Vector2.up);
				if (slopeAngle != collisions.slopeAngle) {
					moveAmount.x = (hit.distance - skinWidth) * directionX;
					collisions.slopeAngle = slopeAngle;
					collisions.slopeNormal = hit.normal;
				}
			}
		}
	}


    //ATTUALE
	void NEWTESTVerticalCollisions (ref Vector2 moveAmount) {
		


		float directionY = Mathf.Sign (moveAmount.y);
		float rayLength = Mathf.Abs (moveAmount.y) + skinWidth;
		string closestObjectLayer = "";
		//print (collisions.standingOnPlatform);

		float minDistanceFound = rayLength;
		//CheckPlatformCollision ();

		for (int i = 0; i < verticalRayCount; i++) {

			Vector2 rayOrigin = (directionY == -1) ? rayCastOrigins.bottomLeft : rayCastOrigins.topLeft;
			rayOrigin += Vector2.right * (verticalRaySpacing * i + moveAmount.x);
			RaycastHit2D[] hit;

			if (directionY == 1)
				hit = Physics2D.RaycastAll (rayOrigin, Vector2.up * directionY, rayLength, upwardCollisionMask);
			else
				hit = Physics2D.RaycastAll (rayOrigin, Vector2.up * directionY, rayLength, groundMask);

			/*
			if (directionY == 1)
				hit = Physics2D.Raycast (rayOrigin, Vector2.up * directionY, rayLength, upwardCollisionMask);
			else if (!collisions.fallingThroughPlatform)
				hit = Physics2D.Raycast (rayOrigin, Vector2.up * directionY, rayLength, groundMask);
			*/

			Debug.DrawRay (rayOrigin, Vector2.up * directionY * rayLength, Color.red);
			float minDistance = rayLength;
			//print ("length: " + hit.Length);
			if (hit.Length != 0) {
				for (int j = 0; j < hit.Length; j++) {
			

					if (platformsHit.Contains (hit [j].collider)) {

						//print ("BAU");
						continue;

					}

					if (stairsHit.Contains (hit [j].collider)) {
						continue;
					}

					//float slopeAngle = Vector2.Angle (hit[j].normal, Vector2.up);
					string otherLayer = LayerMask.LayerToName (hit[j].collider.gameObject.layer);
					string otherLabel = hit [j].collider.tag;

					if (otherLayer == "Platform") {
						//print ("porattoforumu");
						if (collisions.isIgnoringPlatforms > 0)
							continue;
						if (hit [j].normal.y < 0)
							continue;
						//if (slopeAngle > maxSlopeAngle)
							//continue;
					}

					if (collisions.isIgnoringStairs > 0 && otherLabel == "Stairs") {
						continue;
					}



					/*
				if (hit.collider.tag == "Through") {
					if (directionY == 1 || hit.distance == 0) {
						continue;
					}

					if (collisions.fallingThroughPlatform) {
						continue;
					}
					if (playerInput.y == -1) {
						collisions.fallingThroughPlatform = true;
						Invoke("ResetFallingThroughPlatform",.5f);
						continue;
					}

				}
*/
					if (hit [j].distance < minDistance) {
						minDistance = hit [j].distance;
						if (minDistance < minDistanceFound) {
							minDistanceFound = minDistance;
							//closestObjectLayer = LayerMask.LayerToName (hit [j].collider.gameObject.layer);
						}

					}
					
					moveAmount.y = (minDistance - skinWidth) * directionY;
					rayLength = hit [j].distance;
					//rayLength = minDistance;

                    //IMPORTANTE: SE SONO IN SALITA E TOCCO QUALCOSA SOPRA, ALLORA DEVO RIDURRE ANCHE LA VELOCITA ORIZZONTALE
					if (collisions.climbingSlope) {
						moveAmount.x = moveAmount.y / Mathf.Tan (collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign (moveAmount.x);
					}

					collisions.below = directionY == -1;
					collisions.above = directionY == 1;
				}
			}
		}

		if (closestObjectLayer == "Platform") {
			//collisions.standingOnPlatform = true;
		}
		//print (collisions.standingOnPlatform);


		if (collisions.climbingSlope) {
			float directionX = Mathf.Sign(moveAmount.x);
			rayLength = Mathf.Abs(moveAmount.x) + skinWidth;
			Vector2 rayOrigin = ((directionX == -1)?rayCastOrigins.bottomLeft:rayCastOrigins.bottomRight) + Vector2.up * moveAmount.y;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin,Vector2.right * directionX,rayLength,groundMask);

			if (hit) {
				float slopeAngle = Vector2.Angle(hit.normal,Vector2.up);
				if (slopeAngle != collisions.slopeAngle) {
					moveAmount.x = (hit.distance - skinWidth) * directionX;
					collisions.slopeAngle = slopeAngle;
					collisions.slopeNormal = hit.normal;
				}
			}
		
		}
	}

    void CastVerticalCollisions (ref Vector2 moveAmount)
    {




        float directionY = Mathf.Sign(moveAmount.y);
        float rayLength = Mathf.Abs(moveAmount.y) + skinWidth;
        string closestObjectLayer = "";
        //print (collisions.standingOnPlatform);

        float minDistanceFound = rayLength;
        //CheckPlatformCollision ();

        //for (int i = 0; i < verticalRayCount; i++)
        {

            Vector2 rayOrigin = (directionY == -1) ? rayCastOrigins.bottomLeft : rayCastOrigins.topLeft;
            //rayOrigin += Vector2.right * (verticalRaySpacing * i + moveAmount.x);
            RaycastHit2D[] hit = new RaycastHit2D[30];
            int count = 0;
			/*
            if (directionY == 1)
                count = myCollider.Cast(Vector2.up * directionY, upwardCF, hit, rayLength);
            else
                count = myCollider.Cast(Vector2.up * directionY, groundCF, hit, rayLength);
			*/

            //Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red);
            float minDistance = rayLength;
            //print ("length: " + hit.Length);
            if (count > 0)
            {
                for (int j = 0; j < count; j++)
                {


                    if (platformsHit.Contains(hit[j].collider))
                    {

                        //print ("BAU");
                        continue;

                    }

                    if (stairsHit.Contains(hit[j].collider))
                    {
                        continue;
                    }

                    float slopeAngle = Vector2.Angle(hit[j].normal, Vector2.up);
                    string otherLayer = LayerMask.LayerToName(hit[j].collider.gameObject.layer);
                    string otherLabel = hit[j].collider.tag;

                    if (otherLayer == "Platform")
                    {
                        //print ("porattoforumu");
                        if (collisions.isIgnoringPlatforms > 0)
                            continue;
                        if (hit[j].normal.y < 0)
                            continue;
                        //if (slopeAngle > maxSlopeAngle)
                        //continue;
                    }

                    if (collisions.isIgnoringStairs > 0 && otherLabel == "Stairs")
                    {
                        continue;
                    }



                    /*
				if (hit.collider.tag == "Through") {
					if (directionY == 1 || hit.distance == 0) {
						continue;
					}

					if (collisions.fallingThroughPlatform) {
						continue;
					}
					if (playerInput.y == -1) {
						collisions.fallingThroughPlatform = true;
						Invoke("ResetFallingThroughPlatform",.5f);
						continue;
					}

				}
*/
                    if (hit[j].distance < minDistance)
                    {
                        minDistance = hit[j].distance;
                        if (minDistance < minDistanceFound)
                        {
                            minDistanceFound = minDistance;
                            //closestObjectLayer = LayerMask.LayerToName (hit [j].collider.gameObject.layer);
                        }

                    }

                    moveAmount.y = (minDistance - skinWidth) * directionY;
                    rayLength = hit[j].distance;
                    //rayLength = minDistance;


                    if (collisions.climbingSlope)
                    {
                        moveAmount.x = moveAmount.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveAmount.x);
                    }

                    collisions.below = directionY == -1;
                    collisions.above = directionY == 1;
                }
            }
        }

        if (closestObjectLayer == "Platform")
        {
            //collisions.standingOnPlatform = true;
        }
        //print (collisions.standingOnPlatform);


        if (collisions.climbingSlope)
        {
            float directionX = Mathf.Sign(moveAmount.x);
            rayLength = Mathf.Abs(moveAmount.x) + skinWidth;
            Vector2 rayOrigin = ((directionX == -1) ? rayCastOrigins.bottomLeft : rayCastOrigins.bottomRight) + Vector2.up * moveAmount.y;
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, groundMask);

            if (hit)
            {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (slopeAngle != collisions.slopeAngle)
                {
                    moveAmount.x = (hit.distance - skinWidth) * directionX;
                    collisions.slopeAngle = slopeAngle;
                    collisions.slopeNormal = hit.normal;
                }
            }

        }
    }


	void TESTClimbSlope(ref Vector2 moveAmount, float slopeAngle, Vector2 slopeNormal) {
		float moveDistance = Mathf.Abs (moveAmount.x);
		float climbmoveAmountY = Mathf.Sin (slopeAngle * Mathf.Deg2Rad) * moveDistance;

		if (moveAmount.y <= climbmoveAmountY) {
			moveAmount.y = climbmoveAmountY;
			moveAmount.x = Mathf.Cos (slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign (moveAmount.x);
			collisions.below = true;
			collisions.climbingSlope = true;
			collisions.slopeAngle = slopeAngle;
			collisions.slopeNormal = slopeNormal;
		}
	}

    //ATTUALE
	void TESTDescendSlope(ref Vector2 moveAmount) {

        

		RaycastHit2D maxSlopeHitLeft;
		RaycastHit2D maxSlopeHitRight;


		if (collisions.isIgnoringPlatforms <= 0) {
			maxSlopeHitLeft = Physics2D.Raycast (rayCastOrigins.bottomLeft, Vector2.down, Mathf.Abs (moveAmount.y) + skinWidth, groundMask);
			maxSlopeHitRight = Physics2D.Raycast (rayCastOrigins.bottomRight, Vector2.down, Mathf.Abs (moveAmount.y) + skinWidth, groundMask);
		} else 

		{
			//print ("YYY");
			maxSlopeHitLeft = Physics2D.Raycast (rayCastOrigins.bottomLeft, Vector2.down, Mathf.Abs (moveAmount.y) + skinWidth, upwardCollisionMask);
			maxSlopeHitRight = Physics2D.Raycast (rayCastOrigins.bottomRight, Vector2.down, Mathf.Abs (moveAmount.y) + skinWidth, upwardCollisionMask);
		}
		if (maxSlopeHitLeft ^ maxSlopeHitRight) {
			SlideDownMaxSlope (maxSlopeHitLeft, ref moveAmount);
			SlideDownMaxSlope (maxSlopeHitRight, ref moveAmount);
		}
        

		if (!collisions.slidingDownMaxSlope) {
			float directionX = Mathf.Sign (moveAmount.x);
			Vector2 rayOrigin = (directionX == -1) ? rayCastOrigins.bottomRight : rayCastOrigins.bottomLeft;
            RaycastHit2D hit = Physics2D.Raycast (rayOrigin, -Vector2.up, Mathf.Infinity, groundMask);


            if (hit) {
                

                string otherLayer = LayerMask.LayerToName (hit.collider.gameObject.layer);
				if (collisions.isIgnoringStairs > 0 && hit.collider.tag == "Stairs")
					return;
				float slopeAngle = Vector2.Angle (hit.normal, Vector2.up);
				if (slopeAngle != 0 && slopeAngle <= maxSlopeAngle) {
					if (Mathf.Sign (hit.normal.x) == directionX) {
                        //print("descend");
                        if (hit.distance - skinWidth <= Mathf.Tan (slopeAngle * Mathf.Deg2Rad) * Mathf.Abs (moveAmount.x))

                        {
                            print("descend");
                            float moveDistance = Mathf.Abs (moveAmount.x);
							float descendmoveAmountY = Mathf.Sin (slopeAngle * Mathf.Deg2Rad) * moveDistance;
							moveAmount.x = Mathf.Cos (slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign (moveAmount.x);
							moveAmount.y -= descendmoveAmountY;

							collisions.slopeAngle = slopeAngle;
							collisions.descendingSlope = true;
							collisions.below = true;
							collisions.slopeNormal = hit.normal;
						}
					}
				}
			}
		}
			
	}



}
