﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.InputSystem;

//[RequireComponent(typeof(PlayerInput))]
public class PlayersManager : MonoBehaviour {

    public static PlayersManager Instance {get; private set;}
    public bool debug;
    //public List<Player> players = new List<Player>();
    //public List<Player> players;
    public Player player1, player2;
	public PlayerHealthbars playerHBs;
    public Inventory inventoryP1, inventoryP2;
    public BuffDatabase buffDatabase;
    public CustomGameManager gameManager;
    public TimeManager timeManager;
    public PromptManager promptManager;
    public bool inputsEnabled;
    public CameraMovement cameraMovement;
    //public PlayerInput playerInput;
    SceneLoader sceneLoader;

    //[HideInInspector]
    public bool doneLoading = false;
    //[HideInInspector]
    public int scenesCurrentlyLoading = 0, scenesCurrentlyActivating = 0;


    private void Awake()
    {
        Instance = this;
        sceneLoader = FindObjectOfType<SceneLoader>();
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 1000;
        gameManager.LoadGlobals();
        if (player1 != null)
        {
            int index = 0;
            while (!player1.PreAwake(index, null))
            {
                index++;
            }
        }
        if (player2 != null)
        {
            int index = 0;
            while (!player2.PreAwake(index, null))
            {
                index++;
            }
        }
        
    }

    

    public void InitializePlayersManager ()
    {
        //gameManager = FindObjectOfType<CustomGameManager>();
        //playerHBs = FindObjectOfType<PlayerHealthbars>();
        //promptManager = FindObjectOfType<PromptManager>();
        //cameraMovement = FindObjectOfType<CameraMovement>();
        promptManager.playersManager = this;
        gameManager.playersManager = this;

       
        //print("devices: " + playerInput.devices);
        //playerInput.enabled = true;
        //playerInput.actions = controls.asset;
        inputsEnabled = true;
    }

    public void InitializePlayer(Player player)
    {
        playerHBs.InitializeBars(this);
        //players[0].inputCheck.controls = controls;
        //print("controls: " + players[0].inputCheck.controls);
        //players[0].inputCheck.InitializeControls();

        cameraMovement.FollowedObject = player1.transform;

        cameraMovement.InitializeCameraMovement();
        //print("currentscheme: " + playerInput.currentControlScheme);
        inventoryP1.controller = player1;
        //print("player: " + players[0]);
        inventoryP1.itemDatabase.weaponsDatabase = player1.weaponSwitcher.weapons;

        inventoryP1.lootNotificationGenerator = FindObjectOfType<LootNotificationGenerator>();



        //if (sceneLoader.saveOnSceneChange)
        //    gameManager.LoadAll();
    }

    

    

}
