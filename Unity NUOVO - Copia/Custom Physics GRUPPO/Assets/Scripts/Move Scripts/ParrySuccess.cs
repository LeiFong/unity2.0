﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParrySuccess : Hitstun
{
	public int defaultFollowupFrame = 28;
	public int kickFollowupFrame = 48;
	public int defaultDuration = 40;
	public int kickDuration = 60;
	public int counterAttackWindowDuration = 40;

	protected override void MyOnEnable()
	{
		//FD = new FrameData();
		//FD = Instantiate(FD);
		//print ("ciao");
		//hbh = controller.gameObject.GetComponentInChildren<HurtboxHit> ();
		//animatorParameterName = fromFrontAnimatorParameter;
		tempFD = null;
	}

	public override void MyStartSequence()
	{
		tempFD = null;
		if (hbh == null)
		{
			hbh = controller.lastCollisionHurtbox.hurtbox;
			tempFD = controller.lastCollisionHurtbox.move.frameData;

			if (tempFD == null || hbh == null)
			{
				StopSequence();
				return;
			}

			direction = hbh.direction;

			minimumUntechableHitstunWindow = tempFD.currentHitProperties.kick ? kickFollowupFrame : defaultFollowupFrame;
			hitstun = tempFD.currentHitProperties.kick ? kickDuration : defaultDuration;

			{
				//stretcho l'animazione per fittarla con la durata dell'hitstun
				animationSpeedIntervals = new AnimationSpeedInterval[2];
				animationSpeedIntervals[0].windowEnd = 10;
				animationSpeedIntervals[0].animationSpeed = 1;

				animationSpeedIntervals[1].windowEnd = hitstun - 44;
				animationSpeedIntervals[1].animationSpeed = (7f / ((float)Mathf.Max(1f, (hitstun - 54f))));

			}

			

			//hitstun = defaultDuration;
			//minimumUntechableHitstunWindow = defaultFollowupFrame;


			controller.lastCollisionHurtbox.hurtbox = null;
			controller.lastCollisionHurtbox.move.frameData = null;
		}

	}

	public override void MyExecuteSequence()
	{
		if (controller.seq == 0)
		{

			controller.seq = 1; //questo perchè l'hitstun partirà sempre un frame in ritardo rispetto all'attaccante
								

		}

		if (controller.seq >= minimumUntechableHitstunWindow - 1 && controller.seq <= minimumUntechableHitstunWindow - 1 + counterAttackWindowDuration)
		{
			for (int i = 0; i < techableMoves.Length; i++)
			{
				if (techableMoves[i].moveType == moveType.animation)
				{
					conditions.animations[techableMoves[i].index].usable = true;
				}
				else
				{
					conditions.moves[techableMoves[i].index].usable = true;
				}
			}
		}
		else
        {
			for (int i = 0; i < techableMoves.Length; i++)
			{
				if (techableMoves[i].moveType == moveType.animation)
				{
					conditions.animations[techableMoves[i].index].usable = false;
				}
				else
				{
					conditions.moves[techableMoves[i].index].usable = false;
				}
			}
		}
		status.mystatus = Status.status.blocking;
		//if (controller.hitstop == 0 && !resumingFromHitstop)

		if (controller.seq < 500)
		{
			controller.seq++;
		}

		if (controller.seq >= hitstun)
		{
			//StopSequence ();
			//moveIsFinished = true;
			//print ("STOP");
			//if (!controller.holdingDash)
				itsTimeToStopTheSequence = true;
			//hbh.hit = false;
			//hbh.block = false;
			//hbh.juggle = false;

		}
	}

	public override void SetStatus()
	{


		status.mystatus = Status.status.blocking;
		//status.canRecharge = false;




	}
}
