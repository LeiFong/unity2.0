﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolBehaviour : BehaviorOption
{
    public bool onlyOnSpawnZone;

    [System.Serializable]
    public struct MoveInSequence
    {
        public int index;
        public moveType type;
        public int customBuffer;
    }

    public enum StepType
    {
        movement, move
    }

    [System.Serializable]
    public struct PatrolStep
    {
        public StepType stepType;
        public int waitTime;
        public MoveInSequence move;
        [HideInInspector]
        public bool moveInitiated;
        public Transform destination;
    }

    public PatrolStep[] patrolSteps;
    public float targetDestinationDistance = 2;
    public bool randomizeStepOrder;
    
    public int currentPatrolStep;
    private bool stepDone;

    public override void ExecuteBehaviour()
    {
        if (patrolSteps.Length == 0)
        {
            //print("non ci sono patrol steps!");
            return;
        }
        if (bSelector.aiMovements != null && bSelector.aiMovements.myCompetenceZone != null && bSelector.aiMovements.currentCompetenceZone != bSelector.aiMovements.myCompetenceZone)
        {
            return;
        }
        if (stepDone)
        {
            bSelector.bseq = 0;
            SelectNextStep();
        }
        if (currentPatrolStep >= 0 && currentPatrolStep < patrolSteps.Length)
        {
            ExecuteCurrentPatrolStep();
        }
    }

    private void ExecuteCurrentPatrolStep ()
    {
        if (patrolSteps[currentPatrolStep].stepType == StepType.movement)
        {
            if (patrolSteps[currentPatrolStep].destination == null)
                return;
            stepDone = false;
            if (MoveToReachDestination(patrolSteps[currentPatrolStep].destination.position, chaseSpeed))
            {
                bSelector.bseq++;
                if (bSelector.bseq >= patrolSteps[currentPatrolStep].waitTime)
                {
                    stepDone = true;
                }
            }
            else
            {
                stepDone = false;
                bSelector.bseq = 0;
            }

        }
        else if (patrolSteps[currentPatrolStep].stepType == StepType.move)
        {
            
            if ((patrolSteps[currentPatrolStep].move.type == moveType.move && controller.currentMove == controller.moves[patrolSteps[currentPatrolStep].move.index]) ||
                    (patrolSteps[currentPatrolStep].move.type == moveType.animation &&
                    controller.currentMove == controller.animations[patrolSteps[currentPatrolStep].move.index]))
            {
                patrolSteps[currentPatrolStep].moveInitiated = true;
                stepDone = false;
            }
            else
            {
                if (patrolSteps[currentPatrolStep].moveInitiated)
                {
                    //print("bseq++ " + controller.currentMove.gameObject);
                    bSelector.bseq++;
                    if (bSelector.bseq >= patrolSteps[currentPatrolStep].waitTime)
                    {
                        stepDone = true;
                        patrolSteps[currentPatrolStep].moveInitiated = false;

                    }
                }
                else
                {
                    //riempi buffer
                    SetBuffers();
                    stepDone = false;
                }
            }
        }
    }

    private void SetBuffers ()
    {
        if (patrolSteps[currentPatrolStep].move.type == moveType.move)
        {
            if (controller.enemyBuffers[patrolSteps[currentPatrolStep].move.index] == 0)
                controller.enemyBuffers[patrolSteps[currentPatrolStep].move.index] = controller.bufferLength;

        }
        else if (patrolSteps[currentPatrolStep].move.type == moveType.animation)
        {
            if (controller.enemyAnimationsBuffers[patrolSteps[currentPatrolStep].move.index] == 0)
                controller.enemyAnimationsBuffers[patrolSteps[currentPatrolStep].move.index] = controller.bufferLength;
        }
    }

    private bool MoveToReachDestination (Vector2 destination, float walkSpeed)
    {
        Vector2 myPosition = controller.gameObject.transform.position;

        if (Vector2.Distance(myPosition, destination) < targetDestinationDistance)
        {
            controller.horizontal = 0;
            return true;
        }
        

        //se è sullo stesso nodo?
        if (destination.x > myPosition.x)
        {
            controller.horizontal = walkSpeed;

        }
        else
        {
            controller.horizontal = -walkSpeed;
        }

        return false;
        
    }

    void SelectNextStep()
    {
        if (patrolSteps.Length == 1)
        {
            currentPatrolStep = 0;
            return;
        }

        if (!randomizeStepOrder)
        {
            currentPatrolStep++;
            currentPatrolStep %= patrolSteps.Length;
            return;
        }
        else
        {
            currentPatrolStep = Random.Range(0, patrolSteps.Length);
            return;
        }

        //currentPatrolStep = -1;
    }

    public override void MyStopBehaviour()
    {
        stepDone = true;
        currentPatrolStep = -1;
        for (int i = 0; i < patrolSteps.Length; i++)
        {
            patrolSteps[i].moveInitiated = false;

        }
    }

    public override void MyLoadInformations()
    {
        stepDone = true;
        bSelector.bseq = 0;
        currentPatrolStep = -1;
    }
    /*
     * if (stepdone)
     *      selectnextstep
     * executepatrolstep;
     * 
     * 
     * 
     * 
     * */
}
