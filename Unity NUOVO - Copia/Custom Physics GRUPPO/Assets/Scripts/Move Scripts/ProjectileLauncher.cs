﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileLauncher : MoveScript {



	public override void MyStopSequence() {
		controller.seq = 0;
		//Debug.Log ("STOP!");
		status.mystatus = Status.status.neutral;
		//controller.animator.SetBool ("neutral", true);
		status.hyperArmor = 0;

		//StopCoroutine ("Attack");
		//StartCoroutine (status.WaitBeforeRecharge());
		controller.currentMove = null;
		//controller.ChangeParameter (controller.animParameter, false);
		//controller.animator.speed = 1;
		//print ("stop");

	}
	public override void SetStatus()
	{


		status.mystatus = Status.status.attacking;
		//status.canRecharge = false;




	}
	public override void MyExecuteSequence ()
	{
		//move = LoadFrameData (controller.moveIndex);
		status.canRecharge = false;
		if (controller.seq == 0) {

			//status.currentStamina -= move.staminaCost;
			//SetAnimationSpeed (move.actualAnimationFrames [0], move.startup);
		}

		/*
		if (move.projectiles[0].startup <= move.midFrames) {
			move.midFrames = 0;
		}
*/

		//if (controller.attack)
		{

			status.mystatus = Status.status.attacking;

			/*
			if (controller.seq < move.projectiles[0].startup - move.midFrames) {
				startupPhase = true;
				activePhase = false;
				recoveryPhase = false;
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * move.startupVelocity;
				//Debug.Log ("start");
			} else if (move.projectiles[0].startup - move.midFrames <= controller.seq && controller.seq < move.projectiles[0].startup) {
				startupPhase = true;
				recoveryPhase = false;
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * move.activeVelocity;

				//Debug.Log ("active");
			} else if (controller.seq >= move.projectiles[0].startup) {
				startupPhase = false;
				recoveryPhase = true;
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * move.recoveryVelocity;
*/

				for (int i = 0; i < move.projectiles.Length; i++) {
					if (controller.seq == move.projectiles [i].startup -1) {
						Shoot (move.projectiles[i].proj, move.projectiles[i].projectileOrigin.position);
					}
				}

				//Debug.Log("recovery");
			}

			if (move.hyperArmorPoints > 0) {
				if (controller.seq >= move.hyperArmorWindowStart && controller.seq <= move.hyperArmorWindowEnd)
					status.hyperArmor = move.hyperArmorPoints;
			}

			//Debug.Log (controller.seq);
			//Debug.Log (attack);

			if (controller.seq == 0) {
				//controller.animUtils.desiredFrames = move.startup;
				//controller.animator.SetBool ("neutral", false);
				//controller.ChangeParameter (animatorParameterName, true);
				//controller.ChangeParameter (animatorParameterName);
				//controller.animator.Play(animatorParameterName, -1, 0f);

			} else if (controller.seq > 0) {
				//controller.animUtils.desiredFrames = move.recovery;
			}

			//Debug.Log (controller.targetVelocity);
			controller.seq++;
			/*
			if (move.followups.Length != 0) {

				int a = move.cancelWindowStart + move.cancelWindowEnd;
				//Debug.Log (a);
				Debug.Log (controller.seq);
				if (controller.seq >= move.cancelWindowStart && controller.seq <= a)
				{
					for (int i = 0; i < move.followups.Length; i++) {
						move.followups [i].GetComponent<FrameData> ().actionUsable = true;
						Debug.Log (move.followups [i].GetComponent<FrameData> ().actionUsable);
						//controller.StartNewMove (move.followupAttack);
					}
				}


				else 
				{
					for (int i = 0; i < move.followups.Length; i++) {
						move.followups [i].GetComponent<FrameData> ().actionUsable = false;
						Debug.Log (move.followups [i].GetComponent<FrameData> ().actionUsable);
						//controller.StartNewMove (move.followupAttack);
					}
				}


			}
			*/
			if (controller.seq >= totalFrames) {
				//print ("stop");
				//targetVelocity.x = 0;
				//status.mystatus = Status.status.standing;
				//Debug.Log("FINITO!");
				//StopSequence ();
			//moveIsFinished = true;
				itsTimeToStopTheSequence = true;


			}




	}

	public void Shoot (Projectile projectile, Vector2 position) {
		//print ("pewpew");
		/*
		GameObject newProj = Instantiate (projectile, position, Quaternion.identity);
        newProj.SetActive(true);
		Projectile projScript = newProj.GetComponent<Projectile> ();
		projScript.move.faction = move.faction;
		projScript.move.owner = move.owner;
		projScript.move.facingRight = controller.facingRight;
		*/
	}

}
