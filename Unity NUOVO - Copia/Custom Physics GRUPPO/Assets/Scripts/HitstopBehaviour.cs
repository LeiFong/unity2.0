﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitstopBehaviour : MonoBehaviour
{
    public int myHitstop;
    public int theirHitstop;

    public MoveScript moveScript;

    public void SetHitstop(Controller2D myController, Controller2D theirController)
    {
        //print("hitstopppppp");
        myController.hitstop = myHitstop;
        if (theirController != null)
            theirController.hitstop = theirHitstop;
    }
}
