﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public Status status;

    [System.Serializable]
    public struct PrimaryStats
    {
        public int vitality, stamina, strength, dexterity, synthesis;
    }
    public PrimaryStats startingStats;
    public PrimaryStats currentStats;
    public PrimaryStats statsMultipliers;
    public PrimaryStats statsAdditives;

    
    public void CalculateCurrentStats()
    {
        
        CalculateMaxHealth();
    }

    public void CalculateMaxHealth()
    {
        status.maxHealth = (currentStats.vitality * statsMultipliers.vitality + statsAdditives.vitality) * 10;
        
    }
}
