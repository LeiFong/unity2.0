﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class MenuManager : MonoBehaviour {

    
    private EventSystem eventSystem;
    [Tooltip("[not mandatory] The first button that gets selected when gaining control. " +
        "If nothing is selected, it will try to grab the first one from the hierarchy.")]
    public GameObject firstSelected;
    public bool rememberSelectionUntilClosed = true;

    public EventTrigger.TriggerEvent onOpen;//, onSoftClose, onHardClose;
    public EventTrigger.TriggerEvent onCloseRequest, onClose;
    public GameObject lastSelected;
    private Animator panelAnimator;
    private CanvasGroup cg;
    //private MenuManager parentMenuManager = null;
    //private MenuManager childMenuManager = null;
    //private bool closed = true;
    //private bool currentlySelected = false;
    private UIManager UImanager;
    private DialogueManager dialogueManager;
    public int hyerarchyId = -1; //viene settato dallo UIManager quando viene chiamato AddMenuManagerToList

    [Tooltip("The time it delays input grabbing when the panel gains focus.")]
    public float interactabilityDelay = 0.3f;



    private void Start() {
        if (firstSelected == null) {
            Button butt = GetComponentInChildren<Button>();
            firstSelected = butt.gameObject;
            
        }

        /*if (firstSelected != null)
        {
            Button b = firstSelected.GetComponent<Button>();
            if (b != null)
                b.Select();
        }
        */
        cg = GetComponent<CanvasGroup>();
        panelAnimator = GetComponent<Animator>();
        UImanager = UIManager.Instance ?? FindObjectOfType<UIManager>();
        dialogueManager = DialogueManager.Instance ?? FindObjectOfType<DialogueManager>();
        eventSystem = EventSystem.current;
    }




    public void SetLastSelected(GameObject obj) {
        lastSelected = obj;
    }

    public void SetCurrentSelectionAsLastSelected() {
        if (eventSystem.currentSelectedGameObject.transform.IsChildOf(gameObject.transform))
            SetLastSelected(eventSystem.currentSelectedGameObject);
        else {
            SetFirstSelectedAsLastSelected();
        }
    }
    public void SetFirstSelectedAsLastSelected() {
        SetLastSelected(firstSelected);
    }


    public void OpenMenu(bool leaveParentVisible = false)
    {
        if (panelAnimator != null)
            panelAnimator.SetTrigger("Open");
        
        DelayInput(interactabilityDelay);

        UImanager.AddMenuManagerToList(this);

        //cosa deve fare con l'eventuale menu parent
        if (hyerarchyId > 0) {
            if (leaveParentVisible)
                UImanager.openMenus[hyerarchyId - 1].FadeMenu();
            else
                UImanager.openMenus[hyerarchyId - 1].BecomeInvisible();
        }

        BaseEventData b = new BaseEventData(EventSystem.current);
        onOpen.Invoke(b);

        cg.interactable = true;

        if (lastSelected != null)
            eventSystem.SetSelectedGameObject(lastSelected);
        else if (firstSelected != null)
            eventSystem.SetSelectedGameObject(firstSelected);
    }

    public void ResumeMenu() {
        if (panelAnimator != null)
            panelAnimator.SetTrigger("Resume");
        
        cg.interactable = true;
        
        DelayInput(interactabilityDelay);

        if (lastSelected != null)
            eventSystem.SetSelectedGameObject(lastSelected);
        else if (firstSelected != null)
            eventSystem.SetSelectedGameObject(firstSelected);
    }

    public void CloseMenu()
    {
        //animazione
        if (panelAnimator != null)
        {
            panelAnimator.SetTrigger("Close");
            
        }

        cg.interactable = false;
        SetFirstSelectedAsLastSelected();

        BaseEventData b = new BaseEventData(EventSystem.current);
        onClose.Invoke(b);

        //rimuovimi dalla lista dei menu aperti del UIManager
        UImanager.RemoveMenuManagerFromList(this);
        //chiudi a cascata eventuali menu figli
        if (hyerarchyId < UImanager.openMenus.Count)
            UImanager.openMenus[hyerarchyId].CloseMenu();
        else {
            //resuma eventuale menu parent
            if (UImanager.openMenus.Count > 0)
                UImanager.openMenus[UImanager.openMenus.Count - 1].ResumeMenu();
        }

        hyerarchyId = -1;
        
    }

    public void FadeMenu()
    {
        if (panelAnimator != null)
            panelAnimator.SetTrigger("Fade");

        SetCurrentSelectionAsLastSelected();

        cg.interactable = false;
    }

    public void BecomeInvisible()
    {
        if (panelAnimator != null)
            panelAnimator.SetTrigger("Invisible");

        if (rememberSelectionUntilClosed)
            SetCurrentSelectionAsLastSelected();
        else
            SetFirstSelectedAsLastSelected();

        cg.interactable = false;

    }
    /*
    public void ResumeMenu()
    {
        if (panelAnimator != null)
        {
            panelAnimator.SetTrigger("Resume");
        }
        cg.interactable = true;
        currentlySelected = true;
        DelayInput(interactabilityDelay);

        if (lastSelected != null)
            eventSystem.SetSelectedGameObject(lastSelected);
        else if (firstSelected != null)
            eventSystem.SetSelectedGameObject(firstSelected);

        UImanager.SetCurrentMenuManager(this);
    }

    
    
    public void CloseMenu() {
        //animazione
        if (panelAnimator != null) {
            panelAnimator.SetTrigger("Close");
            panelAnimator.ResetTrigger("Resume");
            panelAnimator.ResetTrigger("Open");
        }
        
        cg.interactable = false;
        closed = true;
        currentlySelected = false;
        SetFirstSelectedAsLastSelected();

        //rimuovimi dalla lista dei menu aperti del UIManager
        UImanager.RemoveMenuManagerFromList(this);

        //resuma eventuale menu parent, e setta il suo child a null
        if (parentMenuManager != null)
        {
            if (!parentMenuManager.closed)
                parentMenuManager.ResumeMenu();
            parentMenuManager.SetChildMenuManager(null);
        }
        SetParentMenuManager(null);

        //chiudi a cascata eventuale menu figlio
        if (childMenuManager != null)        
            childMenuManager.onHardClose.Invoke(new BaseEventData(EventSystem.current));           
        
        SetChildMenuManager(null);


    }

    
    public void CloseMenuFast() {
        //animazione
        if (panelAnimator != null)
        {
            panelAnimator.SetTrigger("Close Quick");
            panelAnimator.ResetTrigger("Resume");
            panelAnimator.ResetTrigger("Open");
        }

        cg.interactable = false;
        closed = true;
        currentlySelected = false;
        SetFirstSelectedAsLastSelected();

        //rimuovimi dalla lista dei menu aperti del UIManager
        UImanager.RemoveMenuManagerFromList(this);

        //resuma l'eventuale menu parent
        if (parentMenuManager != null)
        {
            if (!parentMenuManager.closed)
                parentMenuManager.ResumeMenu();
            parentMenuManager.SetChildMenuManager(null);
        }
        SetParentMenuManager(null);

        //chiudi a cascata eventuale menu figlio aperto
        if (childMenuManager != null)
            childMenuManager.onHardClose.Invoke(new BaseEventData(EventSystem.current));

        SetChildMenuManager(null);


    }
    
    
    public void OpenMenu() {
        if (panelAnimator != null)
        {
            panelAnimator.SetTrigger("Open");
            panelAnimator.ResetTrigger("Close");
            panelAnimator.ResetTrigger("Close Quick");
        }
        cg.interactable = true;
        closed = false;
        currentlySelected = true;

        DelayInput(interactabilityDelay);

        UImanager.AddMenuManagerToList(this);

        BaseEventData b = new BaseEventData(EventSystem.current);
        onOpen.Invoke(b);

        if (lastSelected != null)
            eventSystem.SetSelectedGameObject(lastSelected);
        else if (firstSelected != null)
            eventSystem.SetSelectedGameObject(firstSelected);

        UImanager.SetCurrentMenuManager(this);
    }

    public void OpenChildMenu(MenuManager mm) {
        SetCurrentSelectionAsLastSelected();
        FadeMenu();
        currentlySelected = false;
        mm.OpenMenu();
        mm.parentMenuManager = this;
        childMenuManager = mm;



    }

    public void OpenChildMenuAndBecomeInvisible(MenuManager mm) {
        SetCurrentSelectionAsLastSelected();
        BecomeInvisible();
        currentlySelected = false;
        mm.OpenMenu();
        mm.parentMenuManager = this;
        childMenuManager = mm;

    }
    */

    public void OpenDialoguePanel() {
        dialogueManager.dialoguePanelMenuManager.OpenMenu();
    }

    public void DelayInput(float time) {
        if (UImanager != null) {
            UImanager.DelayInput(time);
        }
        
    }

    public bool IsMenuCurrentlySelected()
    {
        if (cg.interactable && UImanager.openMenus.Count - 1 == hyerarchyId)
            return true;
        return false;
    }

    /*

    public void SetParentMenuManager(MenuManager mm) {
        parentMenuManager = mm;
    }

    public void SetChildMenuManager(MenuManager mm)
    {
        childMenuManager = mm;
    }


    public bool IsMenuOpen() {
        return !closed;
    }

    public bool IsMenuCurrentlySelected() {
        return currentlySelected;
    }

    */

    public Animator GetAnimator() {
        return panelAnimator;
    }

    public CanvasGroup GetCanvasGroup() {
        return cg;
    }

    
    
}
