﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DustCloudController : MonoBehaviour
{
    public DustCloudMaker cloudMaker;
    public bool executeDuringAnimationTransitions = true;

    public void CreateDustBackwards()
    {
        cloudMaker.CreateDustBackwards();
    }

    public void CreateDustBackwardsBig()
    {
        cloudMaker.CreateDustBackwardsBig();
    }

    public void CreateDustForSteps()
    {
        cloudMaker.CreateDustForSteps();
    }

    public void CreateDustUpwards()
    {
        cloudMaker.CreateDustUpwards();
    }

    public void CreateDustForward()
    {
        cloudMaker.CreateDustForward();
    }

    public void CreateExplosionBig()
    {
        cloudMaker.CreateExplosionBig();
    }

    public void CreateExplosionSmall()
    {
        cloudMaker.CreateExplosionSmall();
    }
}
