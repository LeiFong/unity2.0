﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandingAttackFirstPart : MoveScript {

	//public string landingAnimationParameterName;
	public int minimumActivationFrames = 0; //numero minimo di frame dall'attivazione dopo cui può avere luogo il LandingAttack
    
    [System.Serializable]
    public struct Intervals
    {
        public int frame;
        public int damage;
        
    }

    [System.Serializable]
    public struct DamageIntervals
    {
        //public int defaultDamage;
        public int whichRelatedMove;
        //public Intervals[] Intervals;
    }

    public DamageIntervals [] damageIntervals;
    //public int noGravityFrames = 0;

    public override void SetStatus()
    {
        status.mystatus = Status.status.attacking;
    }
    public override void MyExecuteSequence () {

		status.mystatus = Status.status.attacking;
		//print (controller.seq);
		if (controller.seq == 0) {
			//controller.animUtils.desiredFrames = move.startup;
			//controller.animator.SetBool ("airborne", false);
			//controller.animator.SetBool ("neutral", true);
			//controller.ChangeParameter (animatorParameterName, true);
			//controller.animator.SetBool ("neutral", false);
			//controller.ChangeParameter (animatorParameterName);
			//controller.animator.Play(animatorParameterName, -1, 0f);
			//interruptsWhenLands = true;


		}

		if (controller.seq >= minimumActivationFrames) 
			//controller.customLanding = true;
			interruptsWhenLands = false;
		else
			interruptsWhenLands = true;





        for (int j = 0; j < damageIntervals.Length; j++)
        {

            if (relatedMoves[damageIntervals[j].whichRelatedMove].move == null || relatedMoves[damageIntervals[j].whichRelatedMove].move.hitboxes.Length == 0)
            {
                print("NO");
                break;
            }

            for (int i = 1; i < move.hitProperties.Length; i++)
            {
                if (controller.seq == move.hitProperties[i].newHitFrame)
                {
                    relatedMoves[damageIntervals[j].whichRelatedMove].move.ModifyMultipleHitsFrameData(i, move);
                    /*
                    if (relatedMoves[damageIntervals[j].whichRelatedMove].move.alternativeHitboxes.Length > 0 &&
                            relatedMoves[damageIntervals[j].whichRelatedMove].move.alternativeHitboxes[0] != null &&
                            move.alternativeHitboxes.Length > 0 &&
                            move.alternativeHitboxes[0] != null)
                    */
                    if (relatedMoves[damageIntervals[j].whichRelatedMove].move.alternativeHitboxes.Length > 0)
                    {
                        relatedMoves[damageIntervals[j].whichRelatedMove].move.alternativeHitboxes[0].ModifyMultipleHitsFrameData(i, move.alternativeHitboxes[0]);
                        print("newdamage " + controller.seq);
                    }
                    break;
                }
            }

        }






        /*

        for (int i = 0; i < damageIntervals.Length; i++)
        {
            relatedMoves[damageIntervals[i].whichRelatedMove].move.damageOnHit = damageIntervals[i].defaultDamage;
            relatedMoves[damageIntervals[i].whichRelatedMove].move.alternativeHitboxes[0].damageOnHit = damageIntervals[i].defaultDamage;

            for (int k = 0; k < damageIntervals[i].Intervals.Length; k++)
            {
                if (relatedMoves.Length >= damageIntervals[i].whichRelatedMove && relatedMoves[damageIntervals[i].whichRelatedMove] != null)
                {
                    

                    if (controller.seq > damageIntervals[i].Intervals[k].frame)
                    {
                        
                        relatedMoves[damageIntervals[i].whichRelatedMove].move.damageOnHit = damageIntervals[i].Intervals[k].damage;
                        print("newdamage " + controller.seq);
                        if (relatedMoves[damageIntervals[i].whichRelatedMove].move.alternativeHitboxes.Length > 0 &&
                            relatedMoves[damageIntervals[i].whichRelatedMove].move.alternativeHitboxes[0] != null)
                            relatedMoves[damageIntervals[i].whichRelatedMove].move.alternativeHitboxes[0].damageOnHit = damageIntervals[i].Intervals[k].damage;
                        
                        //break;
                    }
                }
            }
        }
        */
        //print("k: " + relatedMoves[damageIntervals[0].whichRelatedMove].move.damageOnHit + " "
          //                  + relatedMoves[damageIntervals[0].whichRelatedMove].move.alternativeHitboxes[0].damageOnHit);


        //if (status.myPhysicsStatus == Status.physicsStatus.grounded && status.myPreviousPhysicsStatus == Status.physicsStatus.airborne) {

        {
				//if (interruptsWhenLands) 
				//	interruptsWhenLands = false;
				//StopSequence ();
				//controller.StartNewMove (landAttackSecondPart);
				status.mystatus = Status.status.attacking;
			}


        /*else {
            //StopSequence ();
            itsTimeToStopTheSequence = true;

        }
        */

        controller.seq++;


			
	}

    public override void MyStartSequence()
    {
        for (int i = 0; i < damageIntervals.Length; i++)
        {
            relatedMoves[damageIntervals[i].whichRelatedMove].move.MemorizeInitialFrameData();
            /*
            for (int k = 0; k < damageIntervals[i].Intervals.Length; k++)
            {
                if (relatedMoves[damageIntervals[i].whichRelatedMove].move.alternativeHitboxes.Length > 0 &&
                                    relatedMoves[damageIntervals[i].whichRelatedMove].move.alternativeHitboxes[0] != null)
                    relatedMoves[damageIntervals[i].whichRelatedMove].move.alternativeHitboxes[0].MemorizeInitialFrameData();

            }
            */
        }
    }

    public override void MyStopSequence () {
		
        /*
        for (int i = 0; i < damageIntervals.Length; i++)
        {
            relatedMoves[damageIntervals[i].whichRelatedMove].move.RestoreInitialFrameData();
            for (int k = 0; k < damageIntervals[i].Intervals.Length; k++)
            {
                if (relatedMoves[damageIntervals[i].whichRelatedMove].move.alternativeHitboxes.Length > 0 &&
                                    relatedMoves[damageIntervals[i].whichRelatedMove].move.alternativeHitboxes[0] != null)
                    relatedMoves[damageIntervals[i].whichRelatedMove].move.alternativeHitboxes[0].RestoreInitialFrameData();

            }

        }
        */

        controller.seq = 0;
		controller.isAffectedByGravity = true;
		//Debug.Log ("STOP!");
		status.mystatus = Status.status.neutral;
		//controller.animator.SetBool ("neutral", true);
		status.hyperArmor = 0;
		//interruptsWhenLands = true;
		//controller.customLanding = false;
		//StopCoroutine ("Attack");
		//StartCoroutine (status.WaitBeforeRecharge());
		controller.currentMove = null;

		//controller.ChangeParameter (controller.animParameter, false);
		//controller.animator.speed = 1;
		//print ("stop");

	}
}
