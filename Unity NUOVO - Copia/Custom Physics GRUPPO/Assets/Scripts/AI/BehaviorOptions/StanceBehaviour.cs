﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StanceBehaviour : BehaviorOption
{
    public int stanceStartMoveIndex;
    public int stanceMoveIndex;
    public int readyFrame = 20; //una volta in stance, da quale frame può iniziare a prendere decisioni
    public int stanceEndMoveIndex;
    public int averageActionInterval = 20; //ogni quanto, in media, decide di fare una mossa
    public bool stanceStarted = false;

    [System.Serializable]
    public struct FollowupProbability
    {
        public int moveIndex;
        public int probability;
    }

    

    [System.Serializable]
    public struct Range
    {
        public float minRange;
        public float maxRange;
        public int totProbability;
        public FollowupProbability[] followupProbabilities;
    }

    public Range[] ranges;


    public override void MyBehaviourInitialization()
    {
        base.MyBehaviourInitialization();
        for (int i = 0; i < ranges.Length; i++)
        {
            ranges[i].totProbability = 0;

            for (int j = 0; j < ranges[i].followupProbabilities.Length; j++)
            {
                ranges[i].totProbability += ranges[i].followupProbabilities[j].probability;
            }
        }
    }

    public override void MyStopBehaviour()
    {
        //print("ptso");
        base.MyStopBehaviour();
        if (controller.currentMove != controller.moves[stanceEndMoveIndex])
        {
            controller.enemyBuffers[stanceEndMoveIndex] = controller.bufferLength;
        }
    }
    public override void MyLoadInformations()
    {
        base.MyLoadInformations();
        
        stanceStarted = false;
    }

    public override void ExecuteBehaviour()
    {
        if (bSelector.currentDistance > maxDistance || bSelector.bseq >= currentExitTime)
        {
            /*
            if (controller.currentMove != controller.moves[stanceMoveIndex])
            {
                StopBehaviour();
                return;
            }
            else
            */
            if (controller.currentMove != controller.moves[stanceEndMoveIndex])
            {
                controller.enemyBuffers[stanceEndMoveIndex] = controller.bufferLength;
            }
        }

        if (!stanceStarted) 
        {
            if (controller.currentMove != controller.moves[stanceMoveIndex])
            { 
                controller.enemyBuffers[stanceStartMoveIndex] = controller.bufferLength;
                bSelector.bseq = -1;
            }
            /*
            else if (controller.seq >= readyFrame)
            {
                stanceStarted = true;
            }
            */
            if (controller.currentMove == controller.moves[stanceStartMoveIndex])
            {
                stanceStarted = true;
            }
        }

        if (stanceStarted)
        {
            bool stopBehavior = true;


            for (int i = 0; i < behaviorSpecificMoves.Length; i++)
            {

                if ((behaviorSpecificMoves[i].moveType == moveType.move && controller.currentMove == controller.moves[behaviorSpecificMoves[i].index]) ||
                    (behaviorSpecificMoves[i].moveType == moveType.animation &&
                    controller.currentMove == controller.animations[behaviorSpecificMoves[i].index]))
                {

                    stopBehavior = false;
                    break;
                }
            }

            if (stopBehavior)
            {
                //print("Stoppate" + gameObject + " " + controller.currentMove);
                StopBehaviour();
                return;
            }
        }

        if (controller.currentMove == controller.moves[stanceMoveIndex])
        {
            if (controller.seq >= readyFrame)
            {
                //prendi decisioni
                if (Random.Range(0, averageActionInterval) == 0)
                {
                    ChooseAction();
                }
            }
            /*
            else if (!stanceStarted)
            {
                bSelector.bseq = -1;
            }
            */
        }

        if (controller.hitstop <= 0)
            bSelector.bseq++;
    }

    private void ChooseAction()
    {
        int selectedRangeIndex = -1;
        for (int i = 0; i < ranges.Length; i++)
        {
            if (bSelector.currentDistance < ranges[i].maxRange && bSelector.currentDistance >= ranges[i].minRange)
            {
                selectedRangeIndex = i;
                break;
            }
        }
        if (selectedRangeIndex != -1)
        {
            int tot = 0;
            int selectedMoveIndex = -1;
            int p = Random.Range(0, ranges[selectedRangeIndex].totProbability);

            for (int i = 0; i < ranges[selectedRangeIndex].followupProbabilities.Length; i++)
            {
                tot += ranges[selectedRangeIndex].followupProbabilities[i].probability;
                if (p < tot)
                {
                    selectedMoveIndex = ranges[selectedRangeIndex].followupProbabilities[i].moveIndex;
                    break;
                }
            }

            if (selectedMoveIndex != -1)
            {
                controller.enemyBuffers[selectedMoveIndex] = controller.bufferLength;
            }
        }
        
    }
}
