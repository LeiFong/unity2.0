﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class FrameData : MonoBehaviour {
    [HideInInspector]
    public bool lastHit;
    [HideInInspector]
    public bool ghostHitboxIsActive;

    public HitProperties currentHitProperties;
    public int ghostHitboxDurationLeft;
    public string moveName;
	public float animationStartup = 10; //da settare nell'editor, è lo startup dell'animazione della mossa
    public float middleAninationSpeed = 1;
    public float recoveryAnimationSpeed = 1; //velocità dell'animazione durante il recovery
    public float recoveryOffset = 0;
	public bool isProjectile;
    public bool projectileWithDirection; //se vera, la direzione del pushback è data dalla direzione in cui si sta muovendo il proiettile; altrimenti si guarda la posizione 
	public bool facingRight;
	
	public int recovery = 10;
	//public int hitstun = 20;
    //public int blockstunDifference = 0; //esempio: se è -10, dà 10 frame in meno di blockstun rispetto all'hitstun
    
	//public int minimumUntechableHitstunWindow = 28;
	//public int hitstop = 6;
    //public bool positionDependantPushback; //se vera, il segno del pushback viene invertito se l'hitbox colpisce da dietro
	//public float pushbackHit = 1;
	//public int pushbackDuration = 15;

	//public float juggleHorizontalVelocity = 1; //velocita' che assume il nemico quando viene colpito in aria
	//public float juggleVertivalVelocity = 3;
	//public float launchHorizontalVelocity = 1; //velocita' quando viene lanciato in aria mentre è per terra, IGNORARE SE L'ATTACCO NON E' UN LAUNCHER
	//public float launchVertivalVelocity = 3;
    //public int wakeUpType = 0; //se la mossa lancia o mette in juggle, quale tipo di wakeup triggera una volta che il nemico atterra (per esempio groundbounce o hard knockdown)
    
    private void PAwake()
    {
        print("ONDISABLE ");// + gameObject + " " + transform.parent.transform.parent.parent.gameObject);

        CopyHitProperties();
    }
    
    public void CopyHitProperties()
    {
        /*
        hitProperties = new MultipleHitsFrameData[multipleHitsFrameData.Length + 1];
        hitProperties[0].hitProperties = defaultHitProperties;
        for (int i = 1; i < multipleHitsFrameData.Length + 1; i++)
        {
            hitProperties[i] = multipleHitsFrameData[i - 1];
        }
        
        defaultHitProperties.hitstun = hitstun;
        defaultHitProperties.blockstunDifference = blockstunDifference;
        defaultHitProperties.hitstop = hitstop;
        defaultHitProperties.pushbackHit = pushbackHit;
        defaultHitProperties.pushbackDuration = pushbackDuration;
        defaultHitProperties.positionDependantPushback = positionDependantPushback;
        defaultHitProperties.juggleHorizontalVelocity = juggleHorizontalVelocity;
        defaultHitProperties.juggleVertivalVelocity = juggleVertivalVelocity;
        defaultHitProperties.launchHorizontalVelocity = launchHorizontalVelocity;
        defaultHitProperties.launchVertivalVelocity = launchVertivalVelocity;
        defaultHitProperties.wakeUpType = wakeUpType;
        defaultHitProperties.damageOnHit = damageOnHit;
        defaultHitProperties.hyperArmorDamage = hyperArmorDamage;
        defaultHitProperties.meterGain = meterGain;
        defaultHitProperties.opponentMeterGain = opponentMeterGain;
        defaultHitProperties.staminaDamageOnHit = staminaDamageOnHit;
        defaultHitProperties.guardDamage = guardDamage;
        defaultHitProperties.unblockable = unblockable;
        defaultHitProperties.kick = kick;
        defaultHitProperties.launcher = launcher;
        defaultHitProperties.breaksHyperArmor = breaksHyperArmor;
        */

    }
    



    [System.Serializable]
    public struct HitProperties
    {
        public int hitstun;
        public int blockstunDifference; //esempio: se è -10, dà 10 frame in meno di blockstun rispetto all'hitstun
        
        public int minimumUntechableHitstunWindow;
        public int hitstop;

        public float pushbackHit;
        public int pushbackDuration;
        public bool positionDependantPushback; //se vera, il segno del pushback viene invertito se l'hitbox colpisce da dietro


        public float juggleHorizontalVelocity; //velocita' che assume il nemico quando viene colpito in aria
        public float juggleVertivalVelocity;
        public float launchHorizontalVelocity; //velocita' quando viene lanciato in aria mentre è per terra, IGNORARE SE L'ATTACCO NON E' UN LAUNCHER
        public float launchVertivalVelocity;
        public int wakeUpType; //se la mossa lancia o mette in juggle, quale tipo di wakeup triggera una volta che il nemico atterra (per esempio groundbounce o hard knockdown)

        public int damageOnHit;
        public int hyperArmorDamage;
        public int meterGain;
        public int opponentMeterGain;
        public int staminaDamageOnHit;
        public int guardDamage;
        public bool unblockable;
        public bool kick;
        public bool launcher;
        public bool breaksHyperArmor;
    }

    [System.Serializable]
    public struct MultipleHitsFrameData
    {
        //public bool sameAsPreviousHit; //se true, mantiene lo stesso frameData del colpo precedente, in modo da non stare a settare singolarmente tanti colpi simili

        public bool multiHitting; //se true, permette alla mossa di colpire più volte resettando gli objecthit del framedata
        public int newHitFrame;
        public bool fromBeginning; //se true, conta il newhitframe dal frame 0; se false, conta dal frame dello startup

        public HitProperties hitProperties;
        /*
        public int hitstun;
        
        public int minimumUntechableHitstunWindow;
        public int hitstop;

        public float pushbackHit;
        public int pushbackDuration;

        public float juggleHorizontalVelocity; //velocita' che assume il nemico quando viene colpito in aria
        public float juggleVertivalVelocity;
        public float launchHorizontalVelocity; //velocita' quando viene lanciato in aria mentre è per terra, IGNORARE SE L'ATTACCO NON E' UN LAUNCHER
        public float launchVertivalVelocity;
        public int wakeUpType; //se la mossa lancia o mette in juggle, quale tipo di wakeup triggera una volta che il nemico atterra (per esempio groundbounce o hard knockdown)

        public int damageOnHit;
        public int hyperArmorDamage;
        public int meterGain;
        public int opponentMeterGain;
        public int staminaDamageOnHit;
        public int guardDamage;
        public bool unblockable;
        public bool kick;
        public bool launcher;
        public bool breaksHyperArmor;
        */
    }

    //public HitProperties defaultHitProperties;
    public MultipleHitsFrameData[] hitProperties;
    //public MultipleHitsFrameData[] multipleHitsFrameData;
    //private MultipleHitsFrameData initialFrameData; //serve a tenere in memoria i valori iniziali nel caso venga modificato il framedata, come nel caso di multipleHits
    public FrameData[] alternativeHitboxes; //hitbox alternative, i cui objectsHit sono condivisi con questo frame data
    public int priority = 0; //priorità di questo frame data rispetto alle alternativeHitboxes; ignorare se non ce ne sono
    public HitstopBehaviour customHitstopBehaviour;
    

    public int faction = 0;
    public BoxCollider2D ghostHitbox;
    public float damageMultiplier = 1;
	[System.Serializable]
	public struct hitbox {
        
		public BoxCollider2D compositeCollider;
        public BoxCollider2D ghostHitbox; //hitbox fittizio uguale/simile all'hitbox della mossa, serve all'AI per capire se una mossa la colpirebbe o no
		public int startup, active;
        public int ghostHitboxFramesOffset; //quanti frame prima della comparsa dell'hitbox deve comparire il ghost Hitbox
        public bool customSizeGhostHitbox; //se vera, le dimensioni del ghost hitbox NON vengono settate automaticamente, ma a mano nell'editor
        public int staminaCost; //ogni hitbox ha un suo costo di stamina
        
	}

	[System.Serializable]
	public struct projectile {
		public Projectile proj;
        
		public int startup;
		public Transform projectileOrigin;
	}


    public Grab grab;
	public projectile[] projectiles;
	public hitbox[] hitboxes;

	

	public Status.status hitstunType = Status.status.hitstun1;
	public Status.status blockstunType = Status.status.blockstun1;

	//public int damageOnHit = 10;
    public int heatGainOnHit = 10000;
    
    
    //public int hyperArmorDamage = 100;
    //public int meterGain = 100;
    //public int opponentMeterGain = 50;
    //public int staminaDamageOnHit = 10;
	//public int guardDamage = 10;

	//public bool kick = false; 
	//public bool launcher = false;
	//public bool breaksHyperArmor = false;
    //public bool unblockable = false;

	public int hyperArmorPoints = 0;
	public int hyperArmorWindowStart = 10;
	public int hyperArmorWindowEnd = 20;

    public SoftArmorWindow [] softArmorWindows;

	public Controller2D owner;
	public MoveScript moveScript;
	public Projectile projectileScript;
	public List<GameObject> objectsHit;

   

    [System.Serializable]
    public struct SoftArmorWindow
    {
        public int hitboxIndex;
        [Range(0, 100)]
        public int offset;
        
    }



    bool gainedMeter = false;

    public bool GetGainedMeter ()
    {
        return gainedMeter;
    }

    public void SetGainedMeter (bool newGainedMeter)
    {
        gainedMeter = newGainedMeter;
    }
	


	public void MemorizeInitialFrameData ()
    {
        gainedMeter = false;
        //print("fd: " + gameObject);
        if (hitProperties.Length > 0)
            currentHitProperties = hitProperties[0].hitProperties;
        /*
        initialFrameData.hitstop = hitstop;
        initialFrameData.hitstun = hitstun;
        initialFrameData.juggleHorizontalVelocity = juggleHorizontalVelocity;
        initialFrameData.juggleVertivalVelocity = juggleVertivalVelocity;
        initialFrameData.launchHorizontalVelocity = launchHorizontalVelocity;
        initialFrameData.launchVertivalVelocity = launchVertivalVelocity;
        initialFrameData.wakeUpType = wakeUpType;
        initialFrameData.minimumUntechableHitstunWindow = minimumUntechableHitstunWindow;
        initialFrameData.pushbackDuration = pushbackDuration;
        initialFrameData.pushbackHit = pushbackHit;
        initialFrameData.hyperArmorDamage = hyperArmorDamage;
        initialFrameData.breaksHyperArmor = breaksHyperArmor;
        initialFrameData.damageOnHit = damageOnHit;
        initialFrameData.guardDamage = guardDamage;
        initialFrameData.kick = kick;
        initialFrameData.launcher = launcher;
        initialFrameData.unblockable = unblockable;
        initialFrameData.staminaDamageOnHit = staminaDamageOnHit;
        initialFrameData.meterGain = meterGain;
        initialFrameData.opponentMeterGain = opponentMeterGain;
        */
    }

    public void RestoreInitialFrameData ()
    {
        gainedMeter = false;
        if (hitProperties.Length > 0)
            currentHitProperties = hitProperties[0].hitProperties;

        /*
        hitstop = initialFrameData.hitstop;
        hitstun = initialFrameData.hitstun;
        juggleHorizontalVelocity = initialFrameData.juggleHorizontalVelocity;
        juggleVertivalVelocity = initialFrameData.juggleVertivalVelocity;
        launchHorizontalVelocity = initialFrameData.launchHorizontalVelocity;
        launchVertivalVelocity = initialFrameData.launchVertivalVelocity;
        wakeUpType = initialFrameData.wakeUpType;
        minimumUntechableHitstunWindow = initialFrameData.minimumUntechableHitstunWindow;
        pushbackDuration = initialFrameData.pushbackDuration;
        pushbackHit = initialFrameData.pushbackHit;
        hyperArmorDamage = initialFrameData.hyperArmorDamage;
        breaksHyperArmor = initialFrameData.breaksHyperArmor;
        damageOnHit = initialFrameData.damageOnHit;
        guardDamage = initialFrameData.guardDamage;
        kick = initialFrameData.kick;
        launcher = initialFrameData.launcher;
        unblockable = initialFrameData.unblockable;
        staminaDamageOnHit = initialFrameData.staminaDamageOnHit;
        meterGain = initialFrameData.meterGain;
        opponentMeterGain = initialFrameData.opponentMeterGain;
        */
    }

    public void ModifyMultipleHitsFrameData (int index, FrameData frameData)
    {
        gainedMeter = false;

        //if (!frameData.multipleHitsFrameData[index].sameAsPreviousHit)
        {
            currentHitProperties = frameData.hitProperties[index].hitProperties;
            /*
            hitstop = frameData.multipleHitsFrameData[index].hitstop;
            hitstun = frameData.multipleHitsFrameData[index].hitstun;
            juggleHorizontalVelocity = frameData.multipleHitsFrameData[index].juggleHorizontalVelocity;
            juggleVertivalVelocity = frameData.multipleHitsFrameData[index].juggleVertivalVelocity;
            launchHorizontalVelocity = frameData.multipleHitsFrameData[index].launchHorizontalVelocity;
            launchVertivalVelocity = frameData.multipleHitsFrameData[index].launchVertivalVelocity;
            wakeUpType = frameData.multipleHitsFrameData[index].wakeUpType;
            minimumUntechableHitstunWindow = frameData.multipleHitsFrameData[index].minimumUntechableHitstunWindow;
            pushbackDuration = frameData.multipleHitsFrameData[index].pushbackDuration;
            pushbackHit = frameData.multipleHitsFrameData[index].pushbackHit;
            hyperArmorDamage = frameData.multipleHitsFrameData[index].hyperArmorDamage;
            breaksHyperArmor = frameData.multipleHitsFrameData[index].breaksHyperArmor;
            damageOnHit = frameData.multipleHitsFrameData[index].damageOnHit;
            guardDamage = frameData.multipleHitsFrameData[index].guardDamage;
            kick = frameData.multipleHitsFrameData[index].kick;
            launcher = frameData.multipleHitsFrameData[index].launcher;
            unblockable = frameData.multipleHitsFrameData[index].unblockable;
            staminaDamageOnHit = frameData.multipleHitsFrameData[index].staminaDamageOnHit;
            meterGain = frameData.multipleHitsFrameData[index].meterGain;
            opponentMeterGain = frameData.multipleHitsFrameData[index].opponentMeterGain;
            */
        }
    }
}
