﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MoveScript
{

    public HurtboxHit hbh;
    public string frontAnimatorParameter, backAnimatorParameter;

    //public bool frontalDeath;
    public bool isWakeUpDead;

    public movement [] frontDeathMovements, backDeathMovements;

    
    public int lootDropFrame;

    protected override void MyOnEnable()
    {
        controller.frontalDeath = true;
        if (frontAnimatorParameter != "")
            animatorParameterName = frontAnimatorParameter;

        //animatorParameterIndex = Animator.StringToHash(animatorParameterName);

    }

    public override void MyStartSequence()
    {
        status.dead = true;
        status.mystatus = Status.status.dead;
        
        

        //animatorParameterName = frontAnimatorParameter;
        if (relatedMoves.Length > 0 && relatedMoves[0] != null)
        {
            string loopingParameter = frontAnimatorParameter;

            if (!controller.animator.GetBool(controller.dedIndex))
            {

                hbh = controller.lastCollisionHurtbox.hurtbox;
                FrameData tempFD = controller.lastCollisionHurtbox.move.frameData;
                animatorParameterName = frontAnimatorParameter;

                //string loopingParameter;// = frontAnimatorParameter;
                Death deadloop = (Death)relatedMoves[0];
                loopingParameter = deadloop.frontAnimatorParameter;
                
                controller.frontalDeath = true;

                if (backAnimatorParameter != "" && (tempFD != null || hbh != null))
                {
                    if (tempFD.owner != null)
                    {
                        bool direction = tempFD.owner.facingRight;

                        if ((direction && controller.facingRight) || (!direction && !controller.facingRight))
                        {
                            animatorParameterName = backAnimatorParameter;
                            controller.frontalDeath = false;
                            loopingParameter = deadloop.backAnimatorParameter;

                        }

                    }
                }
            }
            if (controller.hitsparkRetriever != null)
            {
                controller.hitsparkRetriever.GenerateSparksOnHit(3, controller.lastCollisionHurtbox.hurtbox.transform, (controller.facingRight ? -1 : 1), 1f);
            }
            else
            {
                print("no hitspark retriever");
            }

            controller.animator.SetBool(controller.dedIndex, true);
            //relatedMoves[0].animatorParameterName = loopingParameter;
            //print("looping " + loopingParameter);
        }
        else
        {
            if (controller.frontalDeath)
                animatorParameterName = frontAnimatorParameter;
            else
                animatorParameterName = backAnimatorParameter;

        }

        animatorParameterIndex = Animator.StringToHash(animatorParameterName);

    }


    public override void SetStatus()
    {
        status.mystatus = Status.status.dead;
    }

    public override void MyExecuteSequence()
    {
        status.dead = true;
        status.mystatus = Status.status.dead;
        controller.shaking = false;
        if (controller.frontalDeath)
        {
            //print("frontal death");
            for (int i = 0; i < frontDeathMovements.Length; i++)
            {
                if (controller.seq >= frontDeathMovements[i].windowStart && controller.seq <= frontDeathMovements[i].windowEnd)
                {
                    canWalkWhenAirborne = false;
                    canWalkWhenGrounded = false;
                    controller.targetVelocity.x = controller.gameObject.transform.localScale.x * (frontDeathMovements[i].velocity);

                    break;
                }
            }
        }
        else
        {
            //print("back death");
            for (int i = 0; i < backDeathMovements.Length; i++)
            {
                if (controller.seq >= backDeathMovements[i].windowStart && controller.seq <= backDeathMovements[i].windowEnd)
                {
                    canWalkWhenAirborne = false;
                    canWalkWhenGrounded = false;
                    controller.targetVelocity.x = controller.gameObject.transform.localScale.x * (backDeathMovements[i].velocity);

                    break;
                }
            }
        }

        if (controller.seq < 500)
        {
            controller.seq++;
        }
    }

    public override void MyStopSequence()
    {
        hbh = null;

    }
}
