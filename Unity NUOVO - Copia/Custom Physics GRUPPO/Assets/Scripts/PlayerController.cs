﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {

	public TargetableList targetList;
	public int turnOffTargetTimer = 0;
	public int TimeToTargetTurnOff = 10;


	//public AvoidCharacters avoid;

	protected override void HorizontalMovement() {


		//avoid.ExitCharacter ();

		if (status.mystatus == Status.status.neutral || (status.myPhysicsStatus == Status.physicsStatus.airborne 
			&& status.mystatus == Status.status.attacking)) {

		move.x = Input.GetAxis ("Horizontal");
		//Debug.Log (move.x);
		targetVelocity = move * walkspeed;

			if (targetVelocity.x != 0)
				animator.SetBool ("walking", true);
			else
				animator.SetBool ("walking", false);

			if (targetList.myTarget == null) {
				//Debug.Log("FLIP?");
				//Debug.Log(move.x);
				if (move.x > 0 && !facingRight)
					Flip ();
			//Debug.Log("FLIP");
			else if (move.x < 0 && facingRight)
					Flip ();

			}
		}

	}

	protected override void Custom () {
		if (targetList.myTarget != null) {
			if (targetList.myTarget.transform.position.x > transform.position.x && !facingRight)
				Flip ();
			else if (targetList.myTarget.transform.position.x < transform.position.x && facingRight)
				Flip ();
		}

	}

	protected override void Jump () {
		if (Input.GetButtonDown ("P1_jump") && status.myPhysicsStatus == Status.physicsStatus.grounded && (status.mystatus == Status.status.neutral || 
			status.mystatus == Status.status.blocking)) {
			


		velocity.y = jumpTakeOffSpeed;
		//status.mystatus = Status.status.jumping;
		}
	}

	protected override void Attack () {
		moveset.conditions.CheckConditions();

	}
		


	protected override void Target ()
	{
		if (Input.GetKeyUp (KeyCode.Tab) && turnOffTargetTimer < TimeToTargetTurnOff)
			targetList.Target ();
	}

	protected override void TurnOffTarget () {
		
		StartCoroutine (CheckForInput ());
		if (turnOffTargetTimer == TimeToTargetTurnOff)
		{
			targetList.TurnOffTarget ();
		}
			
	}

	IEnumerator CheckForInput() {
		if (Input.GetKey (KeyCode.Tab) && turnOffTargetTimer < TimeToTargetTurnOff) {
			turnOffTargetTimer++;
			yield return null;
		} 
		if (Input.GetKeyUp(KeyCode.Tab))
			turnOffTargetTimer = 0;
	}

	protected override void Block () {
		//float vertical = (Input.GetAxis ("Vertical"));
		if (Input.GetButtonDown ("Vertical") && status.myPhysicsStatus == Status.physicsStatus.grounded && status.mystatus == Status.status.neutral) {
			status.mystatus = Status.status.blocking;
			//Debug.Log ("blocco!");
		}
		else if (Input.GetButtonUp("Vertical")) {
			status.mystatus = Status.status.neutral;
		}
	}

	protected override void Dash ()
	{
		if (Input.GetButtonDown ("P1_dash")) {
			StartNewMove (animations [2]); 

	
		}




	}
}
