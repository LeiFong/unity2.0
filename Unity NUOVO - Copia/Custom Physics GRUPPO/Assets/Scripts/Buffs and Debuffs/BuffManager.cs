﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffManager : MonoBehaviour {

    [System.Serializable]
    public struct BuffOwner
    {
        public Controller2D owner;
        public string ownerID;
        public List<Buff> buffs;
    }


    [System.Serializable]
    public class BuffProperties
    {
        public Buff buff;
        public int durationTimer;
        public int currentStacks;
    }

    public List<BuffProperties> buffs = new List<BuffProperties>();

    //public List<BuffOwner> buffOwners = new List<BuffOwner>(); 
    //public Buff[] allBuffs; //array contenente tutti i possibili buff/debuff nel gioco, da tenere aggiornata dallo sviluppatore tramite prefab
	//public List<int> myBuffs = new List<int> (); //lista degli indici dei buff attivi sul personaggio
    public Player controller;

    private void Start()
    {
        /*
        controller = GetComponentInParent<Controller2D>();
        for (int i = 0; i < allBuffs.Length; i++)
        {
            allBuffs[i].SetBuffIndex(i);
        }
        */
    }

    private bool CheckExistingBuff (int checkedBuffIndex)
    {
        return true;
        
    }

    public void ManageBuffDurations ()
    {
        
            for (int j = 0; j < buffs.Count; j++)
            {
                buffs[j].buff.ApplyEffect(controller, buffs[j]);
                buffs[j].durationTimer--;
                if (buffs[j].durationTimer <= 0)
                {
                    buffs.RemoveAt(j);

                }
            }
            
        
    }

    public void OLDManageBuffDurations ()
    {
        /*
        for (int i = 0; i < buffOwners.Count; i++)
        {
            if (buffOwners[i].owner == null || !buffOwners[i].owner.gameObject.activeSelf)
            {
                for (int j = 0; j < buffOwners[i].buffs.Count; j++)
                {
                        Destroy(buffOwners[i].buffs[j].gameObject);
                        buffOwners[i].buffs.RemoveAt(j);
                }
                buffOwners.RemoveAt(i);
                i--;
                continue;
            }
            for (int j = 0; j < buffOwners[i].buffs.Count; j++)
            {
                buffOwners[i].buffs[j].ApplyEffect(controller);
                buffOwners[i].buffs[j].durationTimer--;
                if (buffOwners[i].buffs[j].durationTimer <= 0)
                {
                    Destroy(buffOwners[i].buffs[j].gameObject);
                    buffOwners[i].buffs.RemoveAt(j);
                    
                }
            }
            if (buffOwners[i].buffs.Count <= 0)
            {
                buffOwners.RemoveAt(i);
                i--;
            }
        }
        */
    }


    public void OLDRemoveAllBuffs ()
    {
        /*
        for (int i = 0; i < buffOwners.Count; i++)
        {
            for (int j = 0; j < buffOwners[i].buffs.Count; j++)
            {
                Destroy(buffOwners[i].buffs[j].gameObject);
                buffOwners[i].buffs.RemoveAt(j);
            }
            buffOwners.RemoveAt(i);
            i--;
        }
        */
    }
    /*
    public void RemoveAllBuffs()
    {
        for (int j = 0; j < buffs.Count; j++)
        {
            //Destroy(buffs[j].buff.gameObject);
            buffs.RemoveAt(j);
        }
    }
    */
    public void LoadBuffs (NPCData.BuffProperties [] data)
    {
        //RemoveAllBuffs();
        buffs.Clear();
        
            for (int j = 0; j < data.Length; j++)
            {
                
                
                    //GameObject buffObject = Instantiate(controller.pm.buffDatabase.buffs[buffData.buffs[j].buffID].gameObject);
                    //Buff newBuff = buffObject.GetComponent<Buff>();
                    Buff newBuff = controller.pm.buffDatabase.buffs[data[j].buffID];
                    //print("new buff: " + buffData.buffOwners[i].buffs[j].durationLeft);
                    AddBuff(newBuff, data[j].durationLeft, data[j].currentStacks);
                    //Destroy(buffObject);
                
                
            }
        
    }

    public void OLDLoadBuffs (BuffData buffData)
    {
        /*
        RemoveAllBuffs();

        for (int i = 0; i < buffData.buffOwners.Length; i++)
        {
            for (int j = 0; j < buffData.buffOwners[i].buffs.Length; j++)
            {
                if (controller.pm.gameManager.NPCs.ContainsKey(buffData.buffOwners[i].buffOwnerID) &&
                    controller.pm.gameManager.NPCs[buffData.buffOwners[i].buffOwnerID].controller != null)
                {
                    GameObject buffObject = Instantiate(controller.pm.buffDatabase.buffs[buffData.buffOwners[i].buffs[j].buffID].gameObject);
                    //Buff newBuff = buffObject.GetComponent<Buff>();
                    Buff newBuff = controller.pm.buffDatabase.buffs[buffData.buffOwners[i].buffs[j].buffID];
                    newBuff.owner = controller.pm.gameManager.NPCs[buffData.buffOwners[i].buffOwnerID].controller;
                    //print("new buff: " + buffData.buffOwners[i].buffs[j].durationLeft);
                    AddBuff(newBuff, buffData.buffOwners[i].buffs[j].durationLeft);
                    Destroy(buffObject);
                }
                else
                    print("nun c'è");
            }
        }
        */
    }

    public void OLDAddBuff (Buff newBuff, int duration)
    {
        /*

        //print("qualcunooo");
        if (newBuff == null)// || newBuff.owner == null)
        {
            print("no buff");
            return;

        }
        if (newBuff.owner == null)
        {
            print("no owner");
            return;
        }


        for (int i = 0; i < buffOwners.Count; i++)
        {
            if (newBuff.owner == buffOwners[i].owner)
            {
                for (int j = 0; j < buffOwners[i].buffs.Count; j++)
                {
                    if (buffOwners[i].buffs[j].buffName == newBuff.buffName)
                    //se esiste sia l'owner che il buff:
                    {
                        //riapplica il buff: refresha la durata e, se stackabile, incrementa le stack
                        buffOwners[i].buffs[j].durationTimer = buffOwners[i].buffs[j].buffTotalDuration;
                        //buffOwners[i].buffs[j].currentStacks++;
                        if (buffOwners[i].buffs[j].currentStacks < buffOwners[i].buffs[j].maxStacks)
                            buffOwners[i].buffs[j].currentStacks++;
                        return;
                    }
                }
                //se esiste l'owner, ma non il buff:
                GameObject newBuffGameObjectInstance = Instantiate(newBuff.gameObject);
                newBuffGameObjectInstance.transform.parent = transform;
                Buff buffInstance = newBuffGameObjectInstance.GetComponent<Buff>();
                buffInstance.durationTimer = duration;
                buffInstance.currentStacks = 1;

                buffOwners[i].buffs.Add(buffInstance);
                buffInstance.InitializeBuff(controller);
                print("NUOVO BUFF: " + buffOwners[i].buffs.Count);
                return;
            }
            
        }
        //se non esiste l'owner:
        GameObject buffGameObjectInstance = Instantiate(newBuff.gameObject);
        buffGameObjectInstance.transform.parent = transform;
        Buff newbuffInstance = buffGameObjectInstance.GetComponent<Buff>();
        BuffOwner newBuffOwner = new BuffOwner();
        newBuffOwner.owner = newbuffInstance.owner;
        newBuffOwner.ownerID = newBuffOwner.owner.myID;
        newbuffInstance.durationTimer = duration;
        newbuffInstance.currentStacks = 1;
        //print("durationtimer: " + duration);
        newBuffOwner.buffs = new List<Buff>();
        newBuffOwner.buffs.Add(newbuffInstance);
        newbuffInstance.InitializeBuff(controller);
        buffOwners.Add(newBuffOwner);

        */
    }

    public void AddBuff(Buff newBuff, int duration, int stacks)
    {

        //print("qualcunooo");
        if (newBuff == null || duration <= 0)// || newBuff.owner == null)
        {
            print("no buff: " + duration + " " + newBuff);
            return;

        }
        

        
            
                for (int j = 0; j < buffs.Count; j++)
                {
                    if (buffs[j].buff.buffScriptableObject.index == newBuff.buffScriptableObject.index)
                    //se esiste già il buff:
                    {
                        //riapplica il buff: refresha la durata e, se stackabile, incrementa le stack
                        buffs[j].durationTimer = buffs[j].buff.buffScriptableObject.buffTotalDuration;
                        //buffOwners[i].buffs[j].currentStacks++;
                        if (buffs[j].currentStacks < buffs[j].buff.buffScriptableObject.maxStacks)
                            buffs[j].currentStacks += Mathf.Min(stacks, buffs[j].buff.buffScriptableObject.maxStacks - buffs[j].currentStacks);
                        return;
                    }
                    
                }



        //se non esiste il buff:
        //GameObject newBuffGameObjectInstance = Instantiate(newBuff.gameObject);
                //newBuffGameObjectInstance.transform.parent = transform;
                BuffProperties buffInstance = new BuffProperties
                {
                    buff = newBuff,
                    currentStacks = stacks,
                    durationTimer = duration
                
                };

                

                buffs.Add(buffInstance);
                buffInstance.buff.InitializeBuff(controller);
                //print("NUOVO BUFF: " + buffs.Count);
                return;
            

    }

    private void CreateBuffInstance (Buff newBuff)
    {

    }
}
