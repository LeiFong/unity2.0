﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseFrameData : MonoBehaviour {
	//classe che contiene il frame data della mossa SENZA modificatori. La classe "FrameData" contiene i dati gia' modificati


	public string moveName;

	public int recovery = 10;
	public int hitstun = 20;
	public int blockstun = 10;
	public int hitstop = 6;
	public int guardDamage = 10;


	public float pushbackHit = 1;
	public float pushbackBlock = 2;
	//public int pushbackDuration = 15;

	public float juggleHorizontalVelocity = 1; //velocita' che assume il nemico quando viene colpito in aria
	public float juggleVertivalVelocity = 3;
	public float launchHorizontalVelocity = 1; //velocita' quando viene lanciato in aria mentre è per terra, IGNORARE SE L'ATTACCO NON E' UN LAUNCHER
	public float launchVertivalVelocity = 3;


	public int damageOnHit = 10;
	public int damageOnBlock = 2;
	public int staminaCost = 20;
	public int staminaDamageOnHit = 10;
	public int staminaDamageOnBlock = 5;

	public int hyperArmorPoints = 0;
	public int hyperArmorWindowStart = 10;
	public int hyperArmorWindowEnd = 20;

    [System.Serializable]
    public struct MultipleHitsFrameData
    {
        public bool sameAsPreviousHit; //se true, mantiene lo stesso frameData del colpo precedente, in modo da non stare a settare singolarmente tanti colpi simili

        public int newHitFrame;
        public int hitstun;
        public int minimumUntechableHitstunWindow;
        public int blockstun;
        public int hitstop;

        public float pushbackHit;
        public float pushbackBlock;
        public int pushbackDuration;

        public float juggleHorizontalVelocity; //velocita' che assume il nemico quando viene colpito in aria
        public float juggleVertivalVelocity;
        public float launchHorizontalVelocity; //velocita' quando viene lanciato in aria mentre è per terra, IGNORARE SE L'ATTACCO NON E' UN LAUNCHER
        public float launchVertivalVelocity;

    }

    
}
