﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreatHammerActivationConditions : StandardActivationConditions
{
    //public int armorBuffActive = 0;
    protected override void CustomCheckConditions()
    {
        
            if (controller.hitstop <= 0 && (controller.atk2Buffer > 0 || controller.atk1Buffer > 0 || controller.special1Buffer > 0))
            {
                if (CheckUsability(moves[20]))
                {
                    //print ("CIAO");
                    controller.StartNewMove(moves[20].move);
                    controller.atk2Buffer = 0;
                controller.atk1Buffer = 0;
                controller.special1Buffer = 0;
                }
            }
        if (controller.hitstop <= 0)
        {
            if (controller.atk1Buffer > 0)
            {
                if (CheckUsability(moves[21]))
                {
                    controller.StartNewMove(moves[21].move);
                    controller.atk1Buffer = 0;
                }
            }
            if (controller.atk2Buffer > 0)
            {
                if (CheckUsability(moves[22]))
                {
                    controller.StartNewMove(moves[22].move);
                    controller.atk2Buffer = 0;
                }
            }

            }
        
        
    }
}
