﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetGrabbed : MoveScript
{
    public int grabDuration;
    public Controller2D grabberController;
    private FrameData enemyFrameData;

    [System.Serializable]
    public struct FollowingMove
    {
        public moveType type;
        public int index;
    }
    
    public FollowingMove followingMove;
    Grab.DamageFrame[] damageFrames;
    Grab.EffectFrame[] effectFrames;

    Grab enemyGrab;

    public void GetGrabInfos (FrameData frameData)
    {
        if (frameData == null || frameData.grab == null)
        {
            print("no framedata or grab " + frameData + " " + frameData.grab);
            itsTimeToStopTheSequence = true;
            return;
        }
        else
        {
            enemyGrab = frameData.grab;
            damageFrames = enemyGrab.damageFrames;
            effectFrames = enemyGrab.effectFrames;
            grabberController = frameData.owner;
            enemyGrab.victimController = controller;
            enemyFrameData = frameData;
            //print("grab c'è " + enemyGrab.gameObject);
        }
    }

    public override void MyStopSequence()
    {

        if (controller.seq < grabDuration && enemyGrab != null && grabberController != null)
        {
            if (grabberController.currentMove != null)
            {
                grabberController.currentMove.StopSequence();
            }
        }
        //grabberController.animator.Rebind();
        enemyGrab = null;
        grabberController = null;
        //controller.animator.transform.parent = controller.ret.transform;
        
        //controller.animator.enabled = true;

    }

    public override void MyStartSequence()
    {
        if (enemyGrab == null || grabberController == null || grabberController.currentMove != enemyGrab || !grabberController.gameObject.activeInHierarchy)
        {
            print("no grab");
            itsTimeToStopTheSequence = true;
            return;
        }

        controller.shaking = false;
        status.mystatus = Status.status.stuckinanimation;
        grabDuration = enemyGrab.victimGrabDuration;
        animatorParameterName = enemyGrab.grabVictimAnimation;
        animatorParameterIndex = Animator.StringToHash(animatorParameterName);
        followingMove = enemyGrab.victimFollowupMove;

        //grabberController.animator.Play(enemyGrab.animatorParameterName, 0);
        //controller.animator.enabled = false;

        //grabberController.animator.Rebind();

        if (enemyFrameData.facingRight == controller.facingRight)
            controller.Flip();
        
        //RepositionCharacter();


    }


    
    void RepositionCharacter()
    {
        if (enemyGrab.victimEndingPosition == null)
            return;

        for (int i = 0; i < enemyGrab.stickyVictimPeriod.Length; i++)
        {
            if (controller.seq <= enemyGrab.stickyVictimPeriod[i].end && controller.seq >= enemyGrab.stickyVictimPeriod[i].start)
            {
                break;
            }
            else
            {
                if (i == enemyGrab.stickyVictimPeriod.Length - 1)
                {
                    return;
                }
            }
        }

        float horizontalOffset = (controller.transform.position.x - enemyGrab.victimEndingPosition.position.x);
        float verticalDistance = (controller.transform.position.y - enemyGrab.victimEndingPosition.position.y);

        //print("distance: " + horizontalOffset);
        //controller.targetVelocity.x = controller.facingRight ? (horizontalOffset) / Time.fixedDeltaTime
        //          : -(horizontalOffset) / Time.fixedDeltaTime;

        controller.targetVelocity.x = - horizontalOffset / Time.fixedDeltaTime;
        
        controller.velocity.y = - verticalDistance / Time.fixedDeltaTime;
        //if (Mathf.Abs(controller.velocity.y) < 0.0005f)
          //  controller.velocity.y = 0;

        //print("getgrabbed: " + controller.velocity.y);
    }


    public override void SetStatus()
    {
        status.mystatus = Status.status.grabbed;
    }

    public override void MyExecuteSequence()
    {
        status.invincible = true;
        status.mystatus = Status.status.grabbed;
        controller.avoidingCharacters = true;
        controller.forcedAirborne = true;
        //controller.forcedGrounded = true;

        if (enemyGrab == null || grabberController == null || grabberController.currentMove != enemyGrab
            || !grabberController.gameObject.activeInHierarchy)
        {
            print("no grab");
            controller.StartNewMove(controller.animations[10]);
            //itsTimeToStopTheSequence = true;
            return;
        }

        //print("rgab: " + enemyGrab + " eeeeee " + enemyGrab.move);

        RepositionCharacter();

        if (controller.hitstop <= 0) {
            for (int i = 0; i < damageFrames.Length; i++)
            {
                if (controller.seq == damageFrames[i].frame)
                {
                    controller.status.TakeDamage(WeaponDamageCalculator.CalculateWeaponDamage(damageFrames[i].damage,
                        controller, enemyGrab.controller), 0, 0, 0);
                }
            }
            for (int i = 0; i < effectFrames.Length; i++)
            {
                if (controller.seq == effectFrames[i].frame)
                {
                    if (effectFrames[i].hitstop > 0)
                    {
                        controller.hitstop = effectFrames[i].hitstop;
                    }
                    
                }
            }
        }

        if (controller.seq < grabDuration)
        {
            controller.seq++;
        }
        else
        {
            //print("stop getgrabbed");
            if (followingMove.index < 0)
                itsTimeToStopTheSequence = true;
            else
            {
                controller.StartNewMove(followingMove.type == moveType.animation ? controller.animations[followingMove.index] : controller.moves[followingMove.index]);
            }
        }
        //print("speed: " + controller.animator.speed);
    }
}
