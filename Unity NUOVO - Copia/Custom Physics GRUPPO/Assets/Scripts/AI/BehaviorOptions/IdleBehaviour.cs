﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleBehaviour : BehaviorOption {
    
    //public float walkBackAtDistance = 2.5f;

    public int changeDirectionPeriod = 100;
    public int changeDirectionVariance = 10;

    public int lastFramesStandStill = 0;
    public int lastFramesDropDirLock = 0;

    private int currentChangeDirectionPeriod; 

    //public float targetDistance;
    [Range(0, 1)]
    private float walkspeed = 0.5f;

	


	public override void MyExecuteBehaviour() {

        if (controller.status.mystatus != Status.status.neutral && controller.currentMove != null && !controller.currentMove.canWalkWhenGrounded)
        {
            bSelector.bseq--;
            //print("NOOOOOOOO");
        }
        if (currentIdleDirection == -3)
        {
            if (walkBackProbability == 0)
            {
                walkBackProbability = 0.5f;
            }
            int facing = controller.facingRight ? 1 : -1;
            //if (controller.facingRight)
            {
                if (walkForwardIdle && walkBackIdle)
                {
                    //float r = Random.Range(0.0f,1.0f);

                    if (Random.Range(0f, 1f) > walkBackProbability)
                        currentIdleDirection = facing * walkspeed;
                    else
                        currentIdleDirection = - facing * walkspeed;

                    //print("random: " + currentDirection);
                }
                else if (walkForwardIdle)
                    currentIdleDirection = facing * walkspeed;
                else if (walkBackIdle)
                    currentIdleDirection = -facing * walkspeed;
                else if (stillIdle)
                    currentIdleDirection = 0;
            }
            
            controller.horizontal = currentIdleDirection;
        }
        else
        {
            controller.horizontal = currentIdleDirection;
            if (bSelector.bseq != 0 && bSelector.bseq == currentChangeDirectionPeriod)
            {
                
                    currentIdleDirection = -currentIdleDirection;
                    controller.horizontal = currentIdleDirection;
                
                
                currentChangeDirectionPeriod += changeDirectionPeriod + Random.Range(-changeDirectionVariance, changeDirectionVariance);
                //print(currentChangeDirectionPeriod + " " + bSelector.bseq);

            }

            if (controller.targetList.myTarget != null)
            {
                if (bSelector.currentDistance < walkBackAtDistance && ((currentIdleDirection > 0 && controller.facingRight) ||
                    (currentIdleDirection < 0 && !controller.facingRight)))
                {
                    //print("COD");
                    currentIdleDirection = -currentIdleDirection;
                    controller.horizontal = currentIdleDirection;
                }
            }

            if (bSelector.bseq >= currentExitTime - lastFramesStandStill)
                controller.horizontal = 0;

            if (bSelector.bseq >= currentExitTime - lastFramesDropDirLock)
                controller.animator.SetBool(controller.directionLockIndex, false);
        }

	}

		public override void MyStopBehaviour ()
		{
			currentIdleDirection = -3;
            
		}

    public override void MyLoadInformations()
    {
        walkspeed = chaseSpeed;
        currentIdleDirection = -3;
        currentChangeDirectionPeriod = changeDirectionPeriod + Random.Range(-changeDirectionVariance, changeDirectionVariance);
        if (!walkBackIdle && !walkForwardIdle && !stillIdle)
        {
            stillIdle = true;
        }

    }
}

