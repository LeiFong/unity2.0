﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogCloudBehavior : PoolObject
{
    private Vector2 speed;
    private int timer;
    private int timeToLive = 600;
    public int timeToDissipate = 200;
    public Vector2 positionRandomness;
    [Range(0f, 1f)]
    public float scaleRandomness;
    private SpriteRenderer sprite;
    private Vector3 startPosition, startScale, targetScale;
    private float alphaDecreaseRate;
    private float startAlpha;
    private bool displaying;
    private Transform myTr;
    private Color c;
    private Vector3 newPos, newScale;
    private float rScale;

    // Start is called before the first frame update
    virtual protected void Start()
    {
        myTr = transform;
        startPosition = new Vector3(myTr.localPosition.x, myTr.localPosition.y, myTr.localPosition.z);
        startScale = new Vector3(myTr.localScale.x, myTr.localScale.y);
        sprite = GetComponent<SpriteRenderer>();
        startAlpha = sprite.color.a;
        alphaDecreaseRate = startAlpha / timeToDissipate;
        newPos = new Vector3();
        newScale = new Vector3();
        StopDisplaying();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (displaying)
        {
            if (timer < timeToDissipate) {
                c = sprite.color;
                c.a = c.a + alphaDecreaseRate;
                sprite.color = c;
                newScale.x = myTr.localScale.x + (targetScale.x - myTr.localScale.x) / (timeToDissipate);
                newScale.y = myTr.localScale.y + (targetScale.y - myTr.localScale.y) / (timeToDissipate);
                newScale.z = myTr.localScale.z;
                myTr.localScale = newScale;
            }

            newPos.x = myTr.localPosition.x + speed.x;
            newPos.y = myTr.localPosition.y + speed.y;
            newPos.z = myTr.localPosition.z;
            myTr.localPosition = newPos;
            timer++;
            if (timer >= timeToLive + timeToDissipate)
            {
                if (timer < timeToLive + timeToDissipate * 2)
                {
                    c = sprite.color;
                    c.a = c.a - alphaDecreaseRate;
                    sprite.color = c;
                    
                }
                else
                    StopDisplaying();


            }
        }
    }



    public void StartDisplaying(Vector2 speed, int time) {
        timeToLive = time;
        timer = 0;
        displaying = true;

        newPos.x = startPosition.x + Random.Range(-positionRandomness.x / 2, positionRandomness.x / 2);
        newPos.y = startPosition.y + Random.Range(-positionRandomness.y / 2, positionRandomness.y / 2);
        newPos.z = startPosition.z;
        myTr.localPosition = newPos;
                
        rScale = Random.Range(1f - scaleRandomness, 1f + scaleRandomness);

        targetScale.x = startScale.x * rScale * 0.75f;
        targetScale.y = startScale.y * rScale * 0.75f;
        targetScale.z = myTr.localScale.z;
        myTr.localScale = targetScale;
        
        this.speed = speed;
        c = sprite.color;
        c.a = 0;
        sprite.color = c;

    }

    public void StopDisplaying() {
        c = sprite.color;
        c.a = 0f;
        sprite.color = c;
        displaying = false;
    }
    
}
