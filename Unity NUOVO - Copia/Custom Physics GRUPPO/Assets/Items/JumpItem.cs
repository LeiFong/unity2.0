﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpItem : ItemEffect
{

    protected override void MyOnUse()
    {
        inventory.controller.StartNewMove(inventory.controller.animations[4]);
    }
}
