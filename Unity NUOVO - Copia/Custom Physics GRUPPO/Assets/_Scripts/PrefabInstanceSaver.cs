﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public static class PrefabInstanceSaver
{/*
    [Tooltip("Le istanze di prefab non salvano le modifiche apportate a editor-time. Metti questo componente nel root GO dell'istanza di prefab per forzare un salvataggio ogni volta" +
        "che disabiliti questo oggetto da editor.")]
    public bool tip;
    */
    public static void MarkDirtyObject(GameObject go)
    {
#if UNITY_EDITOR
        if (Application.isPlaying || !PrefabUtility.IsAnyPrefabInstanceRoot(go))
            return;

        PrefabUtility.RecordPrefabInstancePropertyModifications(go);

        EditorUtility.SetDirty(go);
        EditorSceneManager.MarkSceneDirty(go.scene);
#endif

    }
}
