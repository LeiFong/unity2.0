﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartDodgeBehaviour : BehaviorOption
{
    [System.Serializable]
    public struct SmartBehaviourProbability
    {
        public BehaviorOption behavior;
        public int probability;
    }


    int selectedPool;
    //public BehaviorOption smartBehaviour;
    public SmartBehaviourProbability[] smartBehaviourProbabilities;
    //public GhostHitboxDetector ghostDetector;

    
    //[Range(0, 10)]
    //public int defaultBadLuckProtection, hitstunBadLuckProtection = 1;
    //public const int poolTotal = 10;
    //public int currentDefaultPoolTotal = 10;
    //public int currentHitstunPoolTotal = 10;
    //public int currentdefaultDodgeProbabilityPercentage, currenthitstunDodgeProbabilityPercentage = 0;
    public int smartDodgeBuffer = 30;
    private int smartBehaviourTotProbability;
    public BehaviorOption chosenSmartBehaviour;

    [System.Serializable]
    public struct Pool
    {
        public Status.status status;
        [Range(0, 10)]
        public int poolDimension;
        [Range(0, 10)]
        public int numberOfSuccesses;
        public bool[] pool;
        public int index;
    }

    //public bool[] defaultPool = new bool[10];
    //public bool[] hitstunPool = new bool[10];

    public Pool [] dodgeProbabilities;
    //public int defaultIndex, hitstunIndex = 0;
    int r;
    bool element;
    /*
    private void RecalculatePooling()
    {
        currentDefaultPoolTotal = poolTotal;
        currentHitstunPoolTotal = poolTotal;
        currenthitstunDodgeProbabilityPercentage = hitstunDodgeProbabilityPercentage;
        currentdefaultDodgeProbabilityPercentage = defaultDodgeProbabilityPercentage;
    }
    */
    private void CreatePools ()
    {
        for (int j = 0; j < dodgeProbabilities.Length; j++)
        {
            dodgeProbabilities[j].pool = new bool[dodgeProbabilities[j].poolDimension];
            for (int i = 0; i < dodgeProbabilities[j].pool.Length; i++)
            {
                if (i < dodgeProbabilities[j].numberOfSuccesses)
                    dodgeProbabilities[j].pool[i] = true;
                else
                    dodgeProbabilities[j].pool[i] = false;
            }
            ShufflePool(dodgeProbabilities[j].pool, ref dodgeProbabilities[j].index);
            //ShufflePool(hitstunPool, ref hitstunIndex);

        }
    }

    private void ShufflePool(bool[] pool, ref int index)
    {
        index = 0;

        if (pool.Length == 1)
            return;
        for (int i = pool.Length - 1; i > 0; i--)
        {
            r = Random.Range(0, i);
            element = pool[i];
            pool[i] = pool[r];
            pool[r] = element;
        }
    }
    //public int smartMoveIndex;
    //public int customBuffer;
    public override void MyBehaviourInitialization()
    {
        //print("info?");
        CreatePools();
        smartDodgeBuffer = 0;
        
        smartBehaviourTotProbability = 0;
        for (int i = 0; i < smartBehaviourProbabilities.Length; i++)
        {
            smartBehaviourTotProbability += smartBehaviourProbabilities[i].probability;
        }

        RandomlySelectSmartBehaviour();
        /*
        baseProbability = Mathf.Min(defaultDodgeProbabilityPercentage, hitstunDodgeProbabilityPercentage);
        currentdefaultDodgeProbabilityPercentage = defaultDodgeProbabilityPercentage;
        currenthitstunDodgeProbabilityPercentage = hitstunDodgeProbabilityPercentage;
        probabilityDifference = defaultDodgeProbabilityPercentage - hitstunDodgeProbabilityPercentage;
        */
    }

    private void RandomlySelectSmartBehaviour ()
    {
        int r = Random.Range(1, smartBehaviourTotProbability + 1);
        //print ("r=" + r);
        int selectedBehaviour = -1;
        int probabilitySum = 0;

        //scorro la lista e seleziono il behaviour 
        for (int i = 0; i < smartBehaviourProbabilities.Length; i++)
        {
            probabilitySum += smartBehaviourProbabilities[i].probability;
            //print ("probabilitysum=" + probabilitySum);
            if (r <= probabilitySum)
            {

                selectedBehaviour = i;

                //selectedBehaviour = i;
                break;


            }

        }

        //print("R alla fine è " + r);


        if (selectedBehaviour != -1)
        {
            chosenSmartBehaviour = smartBehaviourProbabilities[selectedBehaviour].behavior;
            //StartNewBehaviour(smartBehaviourProbabilities[selectedBehaviour].behavior);
        }
    }





    public override void ExecuteBehaviour() {
        //print("smartdodge::::: " + controller.status.mystatus);
        /*
        if (currentDefaultPoolTotal <= 0)
        {
            //print("reset pool");
            currentDefaultPoolTotal = poolTotal;
            currentdefaultDodgeProbabilityPercentage = defaultDodgeProbabilityPercentage;
        }
        if (currentHitstunPoolTotal <= 0)
        {
            //print("reset hitstun pool");

            currentHitstunPoolTotal = poolTotal;
            currenthitstunDodgeProbabilityPercentage = hitstunDodgeProbabilityPercentage;
        }
        */

        for (int i = 0; i < dodgeProbabilities.Length; i++)
        {
            if (dodgeProbabilities[i].index >= dodgeProbabilities[i].pool.Length)
                ShufflePool(dodgeProbabilities[i].pool, ref dodgeProbabilities[i].index);
        }

        /*
        if (defaultIndex == defaultPool.Length)
        {
            ShufflePool(defaultPool, ref defaultIndex);
        }
        if (hitstunIndex == hitstunPool.Length)
        {
            //print("shuffle hitsun");
            ShufflePool(hitstunPool, ref hitstunIndex);
        }
        */
        if (bSelector.controller.targetList.myTarget != null && bSelector.canDefend) {

            
                if (controller.insideEnemyGhostHitbox)
                {
                //print("dodge: " + controller.status.mystatus);

                if (controller.targetList.myTarget != null && bSelector.targetController.currentMove != null && bSelector.targetController.currentMove.move != null
                && bSelector.targetController.currentMove.move.ghostHitboxIsActive)
                {
                    if (chosenSmartBehaviour.moves.Length > 0)
                    {


                        smartDodgeBuffer = bSelector.targetController.currentMove.move.ghostHitboxDurationLeft;

                    }
                }
                }
                controller.insideEnemyGhostHitbox = false;


                            //bSelector.StartNewBehaviour(smartBehaviour);
                        
        }
               

        if (smartDodgeBuffer > 0)
        {
            smartDodgeBuffer--;

            if ((chosenSmartBehaviour.moves[0].type == moveType.animation && controller.conditions.CheckUsability(controller.conditions.animations[chosenSmartBehaviour.moves[0].index]))
                || (chosenSmartBehaviour.moves[0].type == moveType.move && controller.conditions.CheckUsability(controller.conditions.moves[chosenSmartBehaviour.moves[0].index])))
            {
                //print("smartdidge");
                smartDodgeBuffer = 0;
               
                selectedPool = -1;

                for (int i = 0; i < dodgeProbabilities.Length; i++)
                {
                    if (controller.status.mystatus == dodgeProbabilities[i].status)
                    {
                        selectedPool = i;
                        //print("smart dodge: " + dodgeProbabilities[i].status);
                        break;
                    }
                }

                //if ((controller.status.mystatus == Status.status.hitstun1 && hitstunPool[hitstunIndex] == true)
                //  || controller.status.mystatus != Status.status.hitstun1 && defaultPool[defaultIndex] == true)
                if (selectedPool != -1)
                { 
                    if (dodgeProbabilities[selectedPool].pool[dodgeProbabilities[selectedPool].index] == true)
                    {

                        bSelector.StartNewBehaviour(chosenSmartBehaviour);
                        //print("START SMART DODGE");
                        RandomlySelectSmartBehaviour();
                    }

                    dodgeProbabilities[selectedPool].index++;
                }
            }
        }
        }
    }
   


