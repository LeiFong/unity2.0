﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int currentHealth;
    public int currentStamina;
    public int currentMeter;
    public bool slowStance;
    public float[] position;
    public bool facingRight;
    public NPCData.BuffProperties[] buffs;
    public PlayerData (Status status)
    {
        currentHealth = status.currentHealth;
        currentStamina = status.currentStamina;
        currentMeter = status.currentMeter;
        slowStance = status.currentStance == Status.Stances.slow ? true : false;

        position = new float[3];
        position[0] = status.controller.lastSerializablePosition.x;
        position[1] = status.controller.lastSerializablePosition.y;
        position[2] = status.controller.lastSerializablePosition.z;
        facingRight = status.controller.lastSerializableFacing;

        buffs = new NPCData.BuffProperties[status.controller.buffManager.buffs.Count];

        for (int k = 0; k < status.controller.buffManager.buffs.Count; k++)
        {
            buffs[k].buffID =
                status.controller.
                buffManager.
                buffs[k].buff.
                buffScriptableObject.index;

            //Debug.Log("savebuffs?");

            buffs[k].durationLeft =
                status.controller.
                buffManager.
                buffs[k].durationTimer;

            buffs[k].currentStacks =
                    status.controller.
                    buffManager.
                    buffs[k].currentStacks;

        }
    }
}
