﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeOfDirection : MoveScript {

	public int DashDuration;
	public float DashVelocity = 6;

	/*
	void OnEnable () {
		controller = transform.parent.transform.parent.GetComponent<Controller2D> ();
		status = transform.parent.transform.parent.GetComponent<Status> ();
		LoadFrameData ();
		controller.seq = 0;
	}
*/

	public override void SetStatus()
	{
		status.mystatus = Status.status.dashing;
	}

	public override void MyStopSequence() {

		controller.seq = 0;
		//Debug.Log ("STOP!");
		status.mystatus = Status.status.neutral;
		controller.avoidingCharacters = false;
		//StopCoroutine ("Attack");
		//StartCoroutine (status.WaitBeforeRecharge());
		//controller.animator.SetBool ("dash", false);
		//controller.animator.SetBool ("neutral", true);
		//controller.animator.speed = 1;
		controller.currentMove = null;

	}

	public override void MyExecuteSequence ()
	{



		//status.canRecharge = false;
		if (controller.seq == 0) {
			//status.currentStamina -= staminaCost;
			//SetAnimationSpeed (move.actualAnimationFrames [0], dashDuration);
			//controller.animator.SetBool ("neutral", false);
			//controller.ChangeParameter (animatorParameterName, true);
			//controller.ChangeParameter(animatorParameterName);
		} 




		//if (controller.attack)


		//status.mystatus = Status.status.attacking;
		//if (controller.seq < DashDuration)
        {
			status.mystatus = isCharging ? Status.status.dashing : Status.status.dashing;
			//controller.avoidsCharacters = true;
			//if (controller.seq < DashDuration) {
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * DashVelocity;
			//}
			//Debug.Log ("start");
			controller.seq++;

		} 





		//else {
        if (controller.seq >= DashDuration)
        { 
			//StopSequence ();
			//moveIsFinished = true;
			itsTimeToStopTheSequence = true;


		}


	}
}

