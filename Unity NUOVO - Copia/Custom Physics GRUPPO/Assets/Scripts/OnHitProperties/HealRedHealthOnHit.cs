﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealRedHealthOnHit : OnHitProperties
{
    public int healFrame = 60;
    bool activated;
    public override void OnHitApply()
    {
   
        activated = true;
    }

    public override void OnHitStart()
    {
        base.OnHitStart();
        activated = false;
    }

    private void FixedUpdate()
    {
        if (activated)
        {
            if (move.controller.currentMove != move)
            {
                activated = false;
                return;
            }

            if (move.controller.seq == healFrame)
            {
               

                move.status.currentHealth += move.status.currentRedHealth;
                move.status.currentRedHealth = 0;
                activated = false;
            }
        }
    }
}
