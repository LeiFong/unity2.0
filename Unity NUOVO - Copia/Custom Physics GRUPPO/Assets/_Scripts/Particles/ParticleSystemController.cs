﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemController : MonoBehaviour
{
    public string myName;
    public ParticleSystem myParticleSystem;

    public void EmissionBurst(int num)
    {
        myParticleSystem.Emit(num);
    }

    public void EmissionBurst(int num, float horizontalSpeedMultiplier) {
        var velocity = myParticleSystem.velocityOverLifetime;
        velocity.xMultiplier = horizontalSpeedMultiplier;
        myParticleSystem.Emit(num);
    }
    
    public void EmissionStart() {
        myParticleSystem.Play();
    }

    public void EmissionStop()
    {
        myParticleSystem.Stop();
    }
}
