﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LootNotificationBehavior : PoolObject
{
    public Image image;
    public Text text;
    public Animator animator;
    private float startY;
    private RectTransform rect;
    private int animDisplayTrigger;

    // Start is called before the first frame update
    void Start()
    {
        rect = GetComponent<RectTransform>();
        startY = rect.anchoredPosition.y;
        animDisplayTrigger = Animator.StringToHash("Display");
    }

    public void PushUp() {
        rect.anchoredPosition += new Vector2(0, 85f);
    }

    public void ResetYPosition() {
        rect.anchoredPosition = new Vector3(rect.anchoredPosition.x, startY);
    }

    public void Display(string s, SpriteRenderer spr, Color c) {
        text.text = s;
        text.color = c;
        if (spr != null)
            image.sprite = spr.sprite;
        else
        {
            image.sprite = null;
        }
        ResetYPosition();
        animator.SetTrigger(animDisplayTrigger);
        print("Displaying!");
    }
}
