﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LedgePositioner : MonoBehaviour
{
    public Collider2D rightLedge, leftLedge;
    public CapsuleCollider2D myCollider;
    public bool zulul;
    Transform myTransform, parentTransform;
    Vector3 ledgeScale;
    public float parentRotation;
    
    // Start is called before the first frame update
    void Start()
    {
        //myCollider = GetComponent<CapsuleCollider2D>();
        myTransform = transform;
        parentTransform = transform.parent;
        PositionLedges();
        //PositionLedgesBoxVersion();
    }
    /*
    public void PositionLedges()
    {
        float positionX = (myCollider.size.x/2 * transform.localScale.x - myCollider.size.y/2 * transform.localScale.y)
            * Mathf.Cos(gameObject.transform.eulerAngles.z * Mathf.Deg2Rad);
        float positionY = (myCollider.size.x / 2 * transform.localScale.x - myCollider.size.y / 2 * transform.localScale.y)
            * Mathf.Sin(gameObject.transform.eulerAngles.z * Mathf.Deg2Rad);
        rightLedge.transform.position = (new Vector2(myCollider.bounds.center.x + positionX, myCollider.bounds.center.y + positionY));
        leftLedge.transform.position = (new Vector2(myCollider.bounds.center.x - positionX, myCollider.bounds.center.y - positionY));

        
        //rightLedge.transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, -transform.rotation.z);
        //rightLedge.transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, -transform.eulerAngles.z);
        rightLedge.transform.Rotate(0, 0, -transform.eulerAngles.z);
        leftLedge.transform.Rotate(0, 0, -transform.eulerAngles.z);

        //leftLedge.transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, -transform.rotation.z);

        leftLedge.transform.localScale = new Vector2(transform.localScale.y + 4, transform.localScale.y + 0.4f);

        rightLedge.transform.localScale = new Vector2(transform.localScale.y +4, transform.localScale.y + 0.4f);

        //Debug.DrawRay(new Vector2(myCollider.bounds.center.x - positionX, myCollider.bounds.center.y - positionY), 100 * Vector2.up);

        //print("cos: " + Mathf.Cos(gameObject.transform.eulerAngles.z * Mathf.Deg2Rad));
    }
    */
    public void PositionLedgesBoxVersion()
    {
        ledgeScale = parentTransform.localScale;// = new Vector3(4 / parentTransform.localScale.x, 2 / parentTransform.localScale.y, 1);
        parentRotation = parentTransform.eulerAngles.z;
        parentTransform.eulerAngles = Vector3.zero;
        rightLedge.transform.eulerAngles = Vector3.zero;
        leftLedge.transform.eulerAngles = Vector3.zero;
        parentTransform.localScale = Vector3.one;

        if (zulul)
        {
            print("ledge: " + ledgeScale);
        }




        Vector2 leftEdge, rightEdge, lowerCenter, upperCenter;

        lowerCenter.x = myTransform.position.x + ledgeScale.y / 2 * Mathf.Sin(parentRotation * Mathf.Deg2Rad);
        lowerCenter.y = myTransform.position.y - ledgeScale.y / 2 * Mathf.Cos(parentRotation * Mathf.Deg2Rad);
        upperCenter.x = myTransform.position.x - ledgeScale.y / 2 * Mathf.Sin(parentRotation * Mathf.Deg2Rad);
        upperCenter.y = myTransform.position.y + ledgeScale.y / 2 * Mathf.Cos(parentRotation * Mathf.Deg2Rad);

        leftEdge.x = lowerCenter.x - ledgeScale.x / 2 * Mathf.Cos(parentRotation * Mathf.Deg2Rad);
        leftEdge.y = lowerCenter.y - ledgeScale.x / 2 * Mathf.Sin(parentRotation * Mathf.Deg2Rad);

        rightEdge.x = upperCenter.x - ledgeScale.x / 2 * Mathf.Cos(parentRotation * Mathf.Deg2Rad);
        rightEdge.y = upperCenter.y - ledgeScale.x / 2 * Mathf.Sin(parentRotation * Mathf.Deg2Rad);


        //TEST
        //leftEdge.y -= 0.5f;
        //rightEdge.y -= 0.5f;



        leftLedge.transform.position = leftEdge;
        rightLedge.transform.position = rightEdge;


        myTransform.localScale = ledgeScale;
        //ledgeScale.x = ledgeScale.y * Mathf.Sin(parentRotation * Mathf.Deg2Rad) + 0f;
        //ledgeScale.y = ledgeScale.y * Mathf.Cos(parentRotation * Mathf.Deg2Rad) + 0.3f;

        ledgeScale.x = 0.8f;
        ledgeScale.y = 0.3f;
        ledgeScale.z = 1;
        leftLedge.transform.localScale = ledgeScale;
        rightLedge.transform.localScale = ledgeScale;




        myTransform.Rotate(0, 0, parentRotation);
        //leftLedge.transform.Rotate(0, 0, parentRotation);
        //rightLedge.transform.Rotate(0, 0, parentRotation);

        

        ledgeScale = Vector3.one;
        
        /*
        float positionX = (myCollider.size.x / 2 * myTransform.localScale.x) * Mathf.Cos(parentRotation * Mathf.Deg2Rad);
        float positionY = (myCollider.size.x / 2 * myTransform.localScale.x) * Mathf.Sin(parentRotation * Mathf.Deg2Rad);
        rightLedge.transform.position = (new Vector2(myCollider.bounds.center.x + positionX, myCollider.bounds.center.y + positionY));
        leftLedge.transform.position = (new Vector2(myCollider.bounds.center.x - positionX, myCollider.bounds.center.y - positionY));
        */

    }

    public void PositionLedges()
    {

        ledgeScale = parentTransform.localScale;// = new Vector3(4 / parentTransform.localScale.x, 2 / parentTransform.localScale.y, 1);
        parentRotation = parentTransform.eulerAngles.z;
        parentTransform.eulerAngles = Vector3.zero;
        rightLedge.transform.eulerAngles = Vector3.zero;
        leftLedge.transform.eulerAngles = Vector3.zero;
        parentTransform.localScale = Vector3.one;

        myTransform.localScale = ledgeScale;
        ledgeScale.x = 4;
        ledgeScale.y = ledgeScale.y + 0.3f;// ledgeScale.y * Mathf.Cos(parentRotation * Mathf.Deg2Rad) + 0.3f;
        ledgeScale.z = 1;
        leftLedge.transform.localScale = ledgeScale;
        rightLedge.transform.localScale = ledgeScale;


        
        
        myTransform.Rotate(0, 0, parentRotation);

        //print("alora pppp");
        rightLedge.transform.localScale = ledgeScale;
        leftLedge.transform.localScale = ledgeScale;

        ledgeScale = Vector3.one;
        //rightLedge.transform.rotation = ledgeScale;
        //leftLedge.transform.rotation = ledgeScale;

        float positionX = (myCollider.size.x / 2 * myTransform.localScale.x - myCollider.size.y / 2 * myTransform.localScale.y)
          * Mathf.Cos(myTransform.eulerAngles.z * Mathf.Deg2Rad);
        float positionY = (myCollider.size.x / 2 * myTransform.localScale.x - myCollider.size.y / 2 * myTransform.localScale.y)
            * Mathf.Sin(myTransform.eulerAngles.z * Mathf.Deg2Rad);
        //float positionX = (myCollider.size.x / 2 * myTransform.localScale.x) * Mathf.Cos(parentRotation * Mathf.Deg2Rad);
        //float positionY = (myCollider.size.x / 2 * myTransform.localScale.x) * Mathf.Sin(parentRotation * Mathf.Deg2Rad);
        rightLedge.transform.position = (new Vector2(myCollider.bounds.center.x + positionX, myCollider.bounds.center.y + positionY));
        leftLedge.transform.position = (new Vector2(myCollider.bounds.center.x - positionX, myCollider.bounds.center.y - positionY));

        if (rightLedge.transform.position.y < leftLedge.transform.position.y)
        {
           
            rightLedge.transform.position = new Vector3(rightLedge.transform.position.x,
                rightLedge.transform.position.y - myTransform.localScale.y / 2 * Mathf.Abs(Mathf.Sin(parentRotation * Mathf.Deg2Rad)),
               0);
        }
        else if (rightLedge.transform.position.y > leftLedge.transform.position.y)
        {
            
            leftLedge.transform.position = new Vector3(leftLedge.transform.position.x,
                leftLedge.transform.position.y - myTransform.localScale.y / 2 * Mathf.Abs(Mathf.Sin(parentRotation * Mathf.Deg2Rad)),
               0);
        }
        
        //rightLedge.transform.Rotate(0, 0, -transform.eulerAngles.z);
        //leftLedge.transform.Rotate(0, 0, -transform.eulerAngles.z);


        //leftLedge.transform.localScale = new Vector2(transform.localScale.y + 4, transform.localScale.y + 0.4f);

        //rightLedge.transform.localScale = new Vector2(transform.localScale.y + 4, transform.localScale.y + 0.4f);

        //Debug.DrawRay(new Vector2(myCollider.bounds.center.x - positionX, myCollider.bounds.center.y - positionY), 100 * Vector2.up);

        //print("cos: " + Mathf.Cos(gameObject.transform.eulerAngles.z * Mathf.Deg2Rad));
    }
}
