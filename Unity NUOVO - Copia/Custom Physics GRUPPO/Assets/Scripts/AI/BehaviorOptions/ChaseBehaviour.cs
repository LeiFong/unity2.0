﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseBehaviour : BehaviorOption {

	public float targetDistance;
	public override void MyExecuteBehaviour ()
	{
		


		if (bSelector.currentDistance > targetDistance)
			controller.horizontal = controller.facingRight ? 1 : -1;
		else
			//controller.horizontal = 0;
			StopBehaviour ();
	}

	public override void MyStopBehaviour ()
	{
		controller.horizontal = 0;
	}
}

/* sì maaaa...
 * non è un check ovvio
 * metti che ho 
 * dist 0-3: B1
 * dist 3-5: B2
 * dist 5-200: chase
 * 
 * il chase deve testare quali raggi?
 * da 0-3 e da 3-5, ma non 5-200
 * come glielo dico?
 * 
 * */

