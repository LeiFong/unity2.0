﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemData
{
    public int ID;
    public int quantity;
    public Inventory.ItemType itemType;
    public ItemData (Item item)
    {
        ID = item.itemCode;
        itemType = item.itemType;
    }
}
