﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProximitySceneLoader : Event
{
    [Space(20)]
    
    
    public string myScenePath;
    public enum Priority { veryLow, low, normal };
    public Priority loadingPriority = Priority.normal;
    private int mySceneID;
    private Transform triggerPoint;
    public Transform playerTransform;
    public int activateSpeed = 1;

    private bool isInside = false;
    public float distanceForActivating = 5f;
    private AsyncOperation op;
    private float currentDistance = 0;
    public bool loading = false, unloading = false, activated = false, deactivated = true, doneLoading = false, doneUnloading = true, activating = false, deactivating = false;
    public bool dirty = false;
    private Scene loadedScene;
    private GameObject sceneRoot;
    private BackgroundAwaker awaker;
    private YieldInstruction waitTime;
    private Coroutine activateCoroutine;
    public float debugWaitTime = 0.06f;
    private CustomGameManager gm;
    private PlayersManager pm;

    public bool debugMessages = false;
    private List<Object> unloadables;


    protected override void InitializeEvent()
    {
        Application.backgroundLoadingPriority = ThreadPriority.Low;

        pm = PlayersManager.Instance ?? FindObjectOfType<PlayersManager>();
        gm = FindObjectOfType<CustomGameManager>();
        
        switch (loadingPriority) {
            case Priority.veryLow: waitTime = new WaitForSeconds(debugWaitTime);
                break;
            case Priority.low:
                waitTime = null;
                break;
            case Priority.normal:
                waitTime = new WaitForFixedUpdate();
                break;
        }
        
        triggerPoint = transform;
        mySceneID = SceneUtility.GetBuildIndexByScenePath(myScenePath);
    }
    /*
     * 
     * se isinside
     *      se !loading && !unloading && !doneloading
     *          startCoroutine(load)
     *      else se inactivationrange 
     *          se !unloading && !activated && doneloading
     *              activated = true
     *              sceneRoot.setactive(true)
     *      else
     *          se activated && !unloading
     *              activated = false
     *              sceneRoot.setactive(false)
     * else
     *      se !doneunloading && !loading && !unloading
     *          startcoroutine(unload)
     * 
     * load
     *      loading = true
     *      doneunloading = false
     *      ...
     *      loading = false
     *      doneloading = true
     *      
     * unload
     *      se activated
     *          sceneRoot.setactive(false)
     *          activated = false
     *      unloading = true
     *      doneloading = false
     *      ...
     *      unloading = false
     *      doneunloading = true
     * 
     * quando inizio a caricare? sono dentro, non sto già caricando, non sto già scaricando, non ho già caricato
     * quando attivo la scena? sono molto dentro, non sto gia attivando, non sto scaricando, non sono gia attivato, ho già caricato
     * quando disattivo la scena? sono dentro, non sono molto dentro, non sto gia disattivando, non sto scaricando, ho già caricato [oppure, quando sto per scaricare la scena]
     * quando scarico la scena? sono fuori, non sto già caricando, scaricando, non ho gia scaricato, non sto attivando
     * 
     * 
     * */

    protected override void MyUpdate()
    {
        if (mySceneID == 4)
        {
            if (Input.GetKeyDown(KeyCode.B))
                gm.ResetNPCs(awaker);

            if (Input.GetKeyDown(KeyCode.N))
            {
                awaker = sceneRoot.GetComponent<BackgroundAwaker>();

                print("myid: " + mySceneID + " " + awaker.gameObject + " " + gameObject);
                StartCoroutine(gm.SaveAllGradually(mySceneID, awaker));
            }
            if (Input.GetKeyDown(KeyCode.M))
            {
                awaker = sceneRoot.GetComponent<BackgroundAwaker>();

                print("myid: " + mySceneID);
                StartCoroutine(gm.LoadSceneGradually(mySceneID, awaker));
            }
        }
    }
    private void FixedUpdate()
    {
        if (isInside)
        {
            currentDistance = Mathf.Sqrt(Mathf.Pow(triggerPoint.position.x - playerTransform.position.x, 2) + Mathf.Pow(triggerPoint.position.y - playerTransform.position.y, 2));
            if (!loading && !unloading && !doneLoading)
            {
                LoadMyScene();
            }
            else if (currentDistance < distanceForActivating)
            {
                if (!unloading && !activated && doneLoading && !activating && sceneRoot != null)
                {
                    if (deactivating && activateCoroutine != null)
                    {
                        deactivating = false;
                        StopCoroutine(activateCoroutine);
                    }
                    sceneRoot.SetActive(true);
                    activateCoroutine = StartCoroutine(ActivateObjects());
                }
            }
            else if (!deactivating && !deactivated && !unloading && doneLoading && sceneRoot != null)
            {
                if (activating && activateCoroutine != null)
                {
                    activating = false;
                    if (debugMessages)
                        print("Deactivating, interrupting activation");
                    StopCoroutine(activateCoroutine);
                }
                activateCoroutine = StartCoroutine(DeactivateObjects());
                //sceneRoot.SetActive(false);


            }
        }
        else
        {
            if (!doneUnloading && !loading && !unloading && !activating && !deactivating)
            {
                
                UnloadMyScene();
            }
        }
        

    }

    protected override void OnPlayerIsInside()
    {
        isInside = true;
        if (debugMessages)
            print("Inside!");

    }
    protected override void OnPlayerIsOutside()
    {
        isInside = false;
        if (debugMessages)
            print("Outside!");
    }

    public void LoadMyScene()
    {
        StartCoroutine(LoadSceneAsync(mySceneID));
    }
    

    public void UnloadMyScene() {
        StartCoroutine(UnloadSceneAsync(mySceneID));
    }

    IEnumerator LoadSceneAsync(int id)
    {
        pm.scenesCurrentlyLoading++;

        loading = true;
        doneUnloading = false;

        dirty = false;

        op = SceneManager.LoadSceneAsync(id, LoadSceneMode.Additive);
        
        while (!op.isDone)
        {
            yield return waitTime;
        }

        loadedScene = SceneManager.GetSceneByBuildIndex(mySceneID);      
        sceneRoot = loadedScene.GetRootGameObjects()[0];
        awaker = sceneRoot.GetComponent<BackgroundAwaker>();
        awaker.mySceneLoader = this;
        if (awaker == null)
        {
            print("Scene was loaded but background awaker was not found in the root gameobject of the scene");
        }
        else
        {
            yield return null;
            awaker.StartPreAwakes();
            while (!awaker.ComputePreAwakes())
                yield return null;

            if (debugMessages)
                print("CustomGameManager sta per caricare, sceneID = " + mySceneID);
            StartCoroutine(gm.LoadSceneGradually(mySceneID, awaker));
            while (gm.isSerializing)
                yield return null;
            
            unloadables = awaker.objectsToManuallyUnload;

            doneLoading = true;
            loading = false;
            if (debugMessages)
                print("Scene loaded");
            pm.scenesCurrentlyLoading--;
            
        }
        
    }

    

    IEnumerator UnloadSceneAsync(int id)
    {
        if (activated) {
            activated = false;
            if (sceneRoot != null)
                sceneRoot.SetActive(false);
        }
        if (debugMessages)
            print("Unloading scene");

        unloading = true;
        doneLoading = false;

        if (dirty)
        {
            if (debugMessages)
                print("CustomGameManager sta per salvare, sceneID = " + mySceneID);
            StartCoroutine(gm.SaveAllGradually(mySceneID, awaker));
            while (gm.isSerializing)
                yield return null;
        }

        op = SceneManager.UnloadSceneAsync(id);        

        while (!op.isDone)
        {
            yield return null;
        }

        for (int i = 0; i < unloadables.Count; i++) {
            Resources.UnloadAsset(unloadables[i]);
            yield return null;
        }

        /*
        op = Resources.UnloadUnusedAssets();
        while (!op.isDone)
        {
            yield return null;
        }
        */

        unloading = false;
        doneUnloading = true;
    }

    IEnumerator ActivateObjects()
    {
        activating = true;
        deactivated = false;

        dirty = true;

        pm.scenesCurrentlyActivating++;

        awaker.StartActivateObjects();
        while (!awaker.ActivateObjects(activateSpeed))
            yield return waitTime;

        pm.scenesCurrentlyActivating--;
        if (pm.scenesCurrentlyActivating <= 0 && pm.scenesCurrentlyLoading <= 0)
            pm.doneLoading = true;

        activating = false;
        activated = true;

    }

    IEnumerator DeactivateObjects()
    {
        activated = false;
        deactivating = true;
        awaker.StartDeactivateObjects();
        while (!awaker.DeactivateObjects(activateSpeed))
            yield return waitTime;
        deactivating = false;
        deactivated = true;

    }
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, distanceForActivating);

    }
}
