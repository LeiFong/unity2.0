﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldSpaceScreenShaker : MonoBehaviour
{

    private CameraMovement cm;
    private Transform tr, cameraTr;
    private float distance;

    // Start is called before the first frame update
    void Start()
    {
        cm = CameraMovement.Instance ?? FindObjectOfType<CameraMovement>();
        cameraTr = cm.transform;
        tr = transform;
    }

    public void ScreenShake(float potency) {
        distance = Mathf.Sqrt(Mathf.Pow(cameraTr.position.x - tr.position.x, 2) + Mathf.Pow(cameraTr.position.y - tr.position.y, 2)) / 5f;
        if (distance < 1)
            distance = 1;
        potency /= distance;
        //print("distance: " + distance + ", potency: " + potency);
        if (potency < 0.01f)
            return;
        cm.Shake(10, potency);
    }

}
