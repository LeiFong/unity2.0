﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollScript : DashScript {

	//public int backrollEndFlipFrame = 50;
	public string frontRollAnimatorParameter;
	public string backrollAnimatorParameter;

	//[HideInInspector]
	//public bool hasFlipped;
	public override void MyStopSequence() {
		controller.seq = 0;
		//hasFlipped = false;
		//Debug.Log ("STOP!");
		status.mystatus = Status.status.neutral;
		controller.avoidingCharacters = false;
		//StopCoroutine ("Attack");
		//StartCoroutine (status.WaitBeforeRecharge ());
		//controller.animator.SetBool ("roll", false);
		//controller.animator.SetBool ("neutral", true);
		//controller.animator.speed = 1;
		controller.currentMove = null;
	}

    public override void MyStartSequence()
    {
		//hasFlipped = false;
		if (isCharging)
		{
			animatorParameterName = backrollAnimatorParameter;
			//print("supa backrollll");
		}
		else
        {
			animatorParameterName = frontRollAnimatorParameter;
        }
		animatorParameterIndex = Animator.StringToHash(animatorParameterName);

	}
	public override void SetStatus()
	{


		status.mystatus = Status.status.dashing;
		//status.canRecharge = false;




	}
	public override void MyExecuteSequence ()
	{

		//status.canRecharge = false;
		if (controller.seq == 0) {
			//status.currentStamina -= staminaCost;

			/*
			if (controller.facingRight && controller.horizontal < 0)
				controller.Flip ();
			else if (!controller.facingRight && controller.horizontal > 0)
				controller.Flip ();

*/


			//SetAnimationSpeed (move.actualAnimationFrames [0], dashDuration);
			//controller.animator.SetBool ("neutral", false);
			//controller.ChangeParameter (animatorParameterName, true);
			//controller.ChangeParameter (animatorParameterName);


			//controller.animator.Play(animatorParameterName, -1, 0f);
		}
		/*
		if (isCharging)
		{
			if (controller.seq == backrollEndFlipFrame && controller.directionLock)
			{
				controller.Flip();
				hasFlipped = true;
			}
		}
		*/
		if (isCharging)
		{
			controller.targetVelocity.x = -controller.targetVelocity.x;
		}
		
        //if (controller.seq < totalDashDuration) {
        { 
			status.mystatus = Status.status.dashing;
			//controller.avoidsCharacters = true;

/*
			if (controller.seq < startDashDuration) {
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * startDashVelocity;
			} else if (controller.seq < midDashDuration) {
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * midDashVelocity;
			} else {
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * recoveryDashVelocity;

			}
			//Debug.Log ("start");
*/

			controller.seq++;

		}

        //else {
        if (controller.seq >= totalDashDuration)
        {

            //StopSequence ();
            //moveIsFinished = true;
            itsTimeToStopTheSequence = true;


		}

	}
}
