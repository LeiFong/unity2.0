﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour {


	protected Vector2 velocity;

	public float gravitymodifier = 1f;

	protected Rigidbody2D rb2d;

	public float minimumDistance = 0.001f;

	protected RaycastHit2D[] hitbuffer = new RaycastHit2D [16];
	protected ContactFilter2D CF;

	public float shellRadius = 0.01f; //piccolo spazio da lasciare per il check delle collisioni onde evitare che i collider si intersichino


	// Use this for initialization
	void Start () {

		CF.useTriggers = false;
		CF.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
		CF.useLayerMask = true;
	}

	void OnEnable ()
	{
		rb2d = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {

		velocity += gravitymodifier * Physics2D.gravity * Time.deltaTime;

		Debug.Log (Physics2D.gravity);

		Vector2 deltaPosition = velocity * Time.deltaTime;

		Vector2 move = Vector2.up * deltaPosition.y;

		Movement (move);

	}


	void Movement (Vector2 move) {

		if (move.magnitude > minimumDistance) {
			int count = rb2d.Cast (move, CF, hitbuffer, move.magnitude + shellRadius);
		}		



		rb2d.position += move;

	}

}
