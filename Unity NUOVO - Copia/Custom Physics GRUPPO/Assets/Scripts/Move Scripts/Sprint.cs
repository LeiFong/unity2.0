﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sprint : MoveScript
{
    public int minimumDurationFrames = 48;

    public override void SetStatus()
    {


        status.mystatus = Status.status.dashing;
        //status.canRecharge = false;




    }
    public override void MyExecuteSequence()
    {
        status.mystatus = Status.status.dashing;

        status.SpendStamina(staminaCost);

        if (controller.seq < 500)
            controller.seq++;
    }
}
