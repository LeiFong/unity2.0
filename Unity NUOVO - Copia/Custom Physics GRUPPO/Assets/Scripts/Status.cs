﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Status : MonoBehaviour {

	public enum status {neutral, blockstun1, wakeup, guardBreak, blocking, hitstun1, hitstun2, grabbed, juggle, jumping, weaponBounce,
        dashing, attacking, landing, stuckinanimation, dead};
	public enum physicsStatus {grounded, airborne};

	public status mystatus, myPreviousStatus;
	public physicsStatus myPhysicsStatus, myPreviousPhysicsStatus;

	public bool isBlocking, previousIsBlocking;
	public bool guardCrushed = false;
	public int maxStamina, maxHealth, maxMeter, maxGuardMeter, currentStamina, currentHealth, currentHealthPercentage, currentMeter, currentRedHealth, currentGuardMeter;
	public int maxStagger, currentStagger, staggerRecoveryDelay, staggerRechargePeriod, staggerRecoverySpeed, staggerStatusDuration;
	public bool isStaggered;
	int staggerCounter;
	[HideInInspector]
	public int heatLevel;

	[Range(0, 100f)]
	public float redHealthRechargePercentagePerSecond = 0.1f;
	public int staminaChargeSpeed, staminaRechargeInterval, redHealthRechargeSpeed, guardMeterRechargeSpeed = 1;
	public int meterPassiveRechargeSpeed;
	public int meterGainOnParry, meterGainWhenHit;
	int staminaCounter;
	//public int redHealthDecayDelay = 60;
	//public int redHealthDecayInterval = 10;
	//public int redhealthDecaySpeed = 1;

    public int meterGainMultiplier = 1; //il metro guadagnato è ridotto/aumentato da questa percentuale
    public float currentMeterGainMultiplier;
	public float currentMeterGainOnHitReceivedMultiplier = 1;

	public int guardCrushDuration = 600; //periodo in cui non puoi bloccare dopo aver finito guardMeter
	public int guardCrushRemaining = 0; //ancora quanti frame di guardCrush rimangono

	public int faction = 0;
	public bool canRecharge = true;
    public bool exhausted = false; //stato che avviene quando vieni colpito mentre sei senza stamina
    public PlayerStats playerStats;

    public bool dead = false;
	public bool invincible = false;

    //HYPER ARMOR, da non confondere con l'armor
    public int maxHyperArmor = 0;
	public int hyperArmor = 0;
    public int hyperArmorRechargeRate = 10;
    public int fullHyperArmorRechargeFrames = 600; //numero frame dopo i quali l'armor si ricarica completamente all'istante
    public int hyperArmorRechargeCounter = 0;

    public int maxArmor = 100;
    public int currentArmor = 100;
	public float defaultRedHealthPercentage = 30;
	public float currentRedHealthPercentage = 30;
	public int redHealthRechargePeriod = 10;

	public int maxHeatGauge = 100000;
	public int heatGaugeThreshold = 5000;
	public int currentHeatAmount = 0;
	public int heatDecayAmount = 10;
	public int waitBeforeHeatDecay = 200;
	public int heatDecayCounter = 0;
	public float maxHeatDamageMultiplier = 2;

	public enum Stances { fast, slow };
    public Stances startingStance = Stances.fast;
    public Stances currentStance;

    public int waitBeforeStartRechargingStamina = 10;
	public int waitBeforeRechargeRedHealth = 120;
	//public int seq = 0;
	public bool jumpable = false;

    public int defensePercentage = 30;
    public float hitStaminaDamagePercentage = 1;
    public float blockDamagePercentage = 0.5f;
    public float blockStaminaDamagePercentage = 1;
	public float parryStaminaDamagePercentage = 0.25f;
	public float hitStaggerMultiplier, blockStaggerMultiplier = 1;
	public float blockRedHealthPercentage = 0.7f;
	public bool inRecoveryPeriod;
	[HideInInspector]
	public float recoveryDamageBonus = 0.5f;

	public Controller2D controller;

	int redHealthRegenCounter = 0;

	public int secondaryStamina;
	public int secondaryStaminaRechargeSpeed;

	public float counterhitBonusDamage = 0.3f;

	// Use this for initialization
	public void InitializeStatus (Controller2D c) 
	{
		//da aggiornare con opportuni modificatori
		recoveryDamageBonus = 0.2f;
		controller = c;
		if (playerStats != null)
			playerStats.status = this;

		currentHealth = maxHealth;
		currentStamina = maxStamina;
		secondaryStamina = maxStamina;
		currentMeter = 0;
		currentGuardMeter = maxGuardMeter;
        currentStance = startingStance;
		currentStagger = 0;

		

		CalculateStatusProperties();
	}

	public void CalculateStatusProperties ()
    {
		redHealthRechargeSpeed = Mathf.RoundToInt(redHealthRechargePercentagePerSecond * maxHealth * redHealthRechargePeriod / 100 / 60);
		if (redHealthRechargeSpeed == 0)
			redHealthRechargeSpeed = 1;
		currentRedHealthPercentage = defaultRedHealthPercentage;
    }

	public void GuardCrush () {
		if (0 < guardCrushRemaining) 
		{
			guardCrushed = true;
			guardCrushRemaining--;
		}
		else
			guardCrushed = false;
	}
	
	public void RechargeRedHealth()
	{
		if (mystatus == status.hitstun1 || mystatus == status.hitstun2 || mystatus == status.juggle || mystatus == status.blockstun1)
		//if (mystatus == status.hitstun1 || mystatus == status.hitstun2 || mystatus == status.juggle || mystatus == status.blockstun1 || !canRecharge)
		{
			redHealthRegenCounter = -1;
		}

		if ((redHealthRegenCounter == redHealthRechargePeriod + waitBeforeRechargeRedHealth ||
			redHealthRegenCounter == waitBeforeRechargeRedHealth))
		{
			currentHealth += Mathf.Min(redHealthRechargeSpeed, currentRedHealth);
			currentRedHealth -= Mathf.Min(redHealthRechargeSpeed, currentRedHealth);

			if (currentHealth > maxHealth)
				currentHealth = maxHealth;

			redHealthRegenCounter = waitBeforeRechargeRedHealth;
		}
		redHealthRegenCounter++;
	}

	public void RechargeStagger()
	{
		if (!isStaggered)
		{
			if (mystatus == status.hitstun1 || mystatus == status.hitstun2 || mystatus == status.juggle || mystatus == status.blockstun1)
			//if (mystatus == status.hitstun1 || mystatus == status.hitstun2 || mystatus == status.juggle || mystatus == status.blockstun1 || !canRecharge)
			{
				staggerCounter = -1;
			}

			if ((staggerCounter == staggerRechargePeriod + staggerRecoveryDelay ||
				staggerCounter == staggerRecoveryDelay))
			{
				currentStagger -= Mathf.Min(staggerRecoverySpeed, currentStagger);

				if (currentStagger < 0)
					currentStagger = 0;

				staggerCounter = staggerRecoveryDelay;
			}
			staggerCounter++;
		}
		else
        {
			staggerCounter = -1;
			currentStagger -= Mathf.RoundToInt(maxStagger / (staggerStatusDuration == 0 ? 100 : staggerStatusDuration));

			if (currentStagger <= 0)
            {
				currentStagger = 0;
				isStaggered = false;
            }
        }
	}
	public void RedHealthDecay()
	{
		if (mystatus == status.hitstun1 || mystatus == status.hitstun2 || mystatus == status.juggle || mystatus == status.blockstun1)
		{
			redHealthRegenCounter = 0;
		}

		/*
		if (redHealthRegenCounter == redHealthRegenInterval + waitBeforeRechargeRedHealth ||
			redHealthRegenCounter == waitBeforeRechargeRedHealth)
		{
			
			currentHealth += Mathf.Min(healthAmount, currentRedHealth);
			currentRedHealth -= Mathf.Min(healthAmount, currentRedHealth);

			if (currentHealth > maxHealth)
				currentHealth = maxHealth;

			redHealthRegenCounter = waitBeforeRechargeRedHealth;
		}
		*/

		if (redHealthRegenCounter == redHealthRechargePeriod + waitBeforeRechargeRedHealth ||
			redHealthRegenCounter == waitBeforeRechargeRedHealth)
		{
			//currentHealth += Mathf.Min(healthAmount, currentRedHealth);
			currentRedHealth -= Mathf.Min(redHealthRechargeSpeed, currentRedHealth);

			if (currentHealth > maxHealth)
				currentHealth = maxHealth;

			redHealthRegenCounter = waitBeforeRechargeRedHealth;
		}
		redHealthRegenCounter++;
	}

	public void RechargeStamina () {

        if (hyperArmor < maxHyperArmor && mystatus != status.hitstun1 && mystatus != status.hitstun2)
        {
            hyperArmor += hyperArmorRechargeRate;

            if (hyperArmorRechargeCounter >= fullHyperArmorRechargeFrames)
            {
                hyperArmor = maxHyperArmor;
                //armorRechargeCounter = fullArmorRechargeFrames;
                hyperArmorRechargeCounter = 0;
            }

            else
            {
                hyperArmorRechargeCounter++;

            }
        }
        else if (hyperArmor == maxHyperArmor)
            hyperArmorRechargeCounter = 0;

		int rechargeAmount = (staminaChargeSpeed < (maxStamina - currentStamina) ? staminaChargeSpeed : (maxStamina - currentStamina));

		if (canRecharge)
		{
			if (controller.currentMove != null && controller.currentMove.staminaRechargeMultiplier != 0)
			{
				rechargeAmount = Mathf.RoundToInt(rechargeAmount * controller.currentMove.staminaRechargeMultiplier);
			}
			if (!controller.enemySpecializer.isAI && controller.framesSpentWalking > 0)
			{
				rechargeAmount = Mathf.RoundToInt(rechargeAmount * 1f);
			}
			if (currentStamina < maxStamina && staminaCounter >= staminaRechargeInterval)
			{
				currentStamina += rechargeAmount;
				staminaCounter = 0;
			}
		}

		currentMeter += meterPassiveRechargeSpeed;

		if (currentMeter > maxMeter)
		{
			currentMeter = maxMeter;
		}

		staminaCounter++;
			if (currentStamina > maxStamina)
				currentStamina = maxStamina;
		
		if (secondaryStamina < maxStamina) 
        {
			rechargeAmount = secondaryStaminaRechargeSpeed;
			if (controller.currentMove != null && controller.currentMove.staminaRechargeMultiplier != 0)
			{
				rechargeAmount = Mathf.RoundToInt(rechargeAmount * controller.currentMove.staminaRechargeMultiplier);
			}

			if (canRecharge)
				secondaryStamina += rechargeAmount;
			else
				secondaryStamina += Mathf.RoundToInt(rechargeAmount / 4);
        }

		if (secondaryStamina > maxStamina)
			secondaryStamina = maxStamina;
	}

	public void SpendStamina (int staminaCost) 
	{
		currentStamina -= staminaCost;
        //LUL
	}

    public void SpendMeter (int meterCost)
    {
        currentMeter -= meterCost;
        //LULW
    }

	public void RechargeGuardMeter ()
	{
		if (currentGuardMeter < maxGuardMeter && (mystatus == status.blocking || mystatus == status.neutral))
			currentGuardMeter += (guardMeterRechargeSpeed < (maxGuardMeter - currentGuardMeter) ? guardMeterRechargeSpeed : (maxGuardMeter - currentGuardMeter));

		if (currentGuardMeter > maxGuardMeter)
			currentGuardMeter = maxGuardMeter;
	}

	public bool CheckForStamina ()
	{
			if (currentStamina > 0)
				return true;
			else
				return false;
	}

	public void HeatGaugeDecay ()
    {
		if (heatDecayCounter < waitBeforeHeatDecay)
        {
			heatDecayCounter++;
        }
		else
        {
			currentHeatAmount -= heatDecayAmount;
			if (currentHeatAmount < 0)
				currentHeatAmount = 0;
        }
    }
	
	public IEnumerator WaitBeforeRechargeStamina() 
	{
		int seq = 0;
		while (seq < waitBeforeStartRechargingStamina) {
			if (controller.currentMove == null || controller.currentMove.canRechargeStamina)
				seq++;
			else
			{
				//print("stop coroutine");
				yield break;
			}
			yield return new WaitForFixedUpdate();
		}

        if (controller.currentMove == null || controller.currentMove.canRechargeStamina)
        {
            canRecharge = true;
        }
	}

    //da aggiornare man mano si aggiungono fattori che influiscono sul danno ricevuto
    public int CalculateDamageTaken(int rawDamage)
    {
        int damageDone = rawDamage;
        damageDone -= (defensePercentage * damageDone / 100);
		
		

        return damageDone;
    }

    public void TakeDamage (HurtboxHit.HitInfo hitInfo) {
        
        //healthDamage = CalculateDamageTaken(healthDamage);

        currentHealth -= hitInfo.healthDamage;

		currentStagger += hitInfo.staggerDamage;
		if (currentStagger >= maxStagger)
		{
			currentStagger = maxStagger;
			isStaggered = true;
		}
		
		if (!exhausted)
			currentStamina -= hitInfo.staminaDamage;

		//if ((mystatus == status.blocking || isBlocking || mystatus == status.blockstun1) && hitInfo.hitType != HurtboxHit.HitType.guardBreak)
		if ((hitInfo.hitType == HurtboxHit.HitType.block || hitInfo.hitType == HurtboxHit.HitType.parry) && hitInfo.hitType != HurtboxHit.HitType.guardBreak)
		{
			currentRedHealth += Mathf.RoundToInt(blockRedHealthPercentage * hitInfo.healthDamage / 100);

		}
		else
		{
			currentRedHealth += Mathf.RoundToInt(currentRedHealthPercentage * hitInfo.healthDamage / 100);
			redHealthRegenCounter = 0;
		}


		//healthDamage /= Mathf.RoundToInt(WeaponDamageCalculator.globalCoefficient);
		if (controller.damageDisplayGenerator != null && hitInfo.healthDamage > 0)
		{
			controller.damageDisplayGenerator.Display(hitInfo.healthDamage.ToString());
			//print("thatsalotofdamage");
		}

		//currentRedHealth = Mathf.RoundToInt(healthDamage / 2);
		//print("DAMAGE: " + healthDamage);

	}
	/*
    public void TakeDamage (int healthDamage)
    {
        //print("take damageeeee");
        redHealthRegenCounter = 0;
        //healthDamage = CalculateDamageTaken(healthDamage);
        currentHealth -= healthDamage;

		//healthDamage /= Mathf.RoundToInt(WeaponDamageCalculator.globalCoefficient);

		if (controller.damageDisplayGenerator != null && healthDamage > 0)
            controller.damageDisplayGenerator.Display(healthDamage.ToString());

		//currentRedHealth = Mathf.RoundToInt(healthDamage / 2);
		//print("DAMAGE: " + healthDamage);

	}
	*/
	public void TakeDamage (int healthDamage, int staminaDamage, int whiteHealth, int guardDamage) 
	{
        //print("take damageeeee");
        redHealthRegenCounter = 0;
        //healthDamage = CalculateDamageTaken(healthDamage);
		currentHealth -= healthDamage;
		currentRedHealth += whiteHealth;

		if (!exhausted)
			currentStamina -= staminaDamage;

		currentGuardMeter -= guardDamage;

		//healthDamage /= Mathf.RoundToInt(WeaponDamageCalculator.globalCoefficient);

		if (controller.damageDisplayGenerator != null && healthDamage > 0)
		{
			controller.damageDisplayGenerator.Display(healthDamage.ToString());
		}
    }
	
}
