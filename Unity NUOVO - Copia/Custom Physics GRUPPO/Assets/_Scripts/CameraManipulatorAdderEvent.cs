﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManipulatorAdderEvent : Event
{
    [Space(20)]
    public CameraManipulator manipulator;

    protected override void OnPlayerIsInside()
    {
        print("inside manipulator");
        manipulator.AddMyselfToCameraMovement();
    }
    protected override void OnPlayerIsOutside()
    {
        print("outside manipulator");
        manipulator.RemoveMyselfToCameraMovement();
    }
}
