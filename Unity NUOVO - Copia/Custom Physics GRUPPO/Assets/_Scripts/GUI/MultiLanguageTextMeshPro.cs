﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MultiLanguageTextMeshPro : MonoBehaviour
{
    [Tooltip("If true, and if the field myTextMeshPro (see below) has not been set manually, it will try to set it by grabbing it on this GameObject on Start.")]
    public bool autoGrabTextComponent = true;
    [Tooltip("Can be null. If not null, it will automatically change the text based on UIManager's language.")]
    public TextMeshProUGUI myTextMeshPro;
    [Tooltip("Whether the strings differ based on player's gender. Scanning the strings is expensive, so leave it at false by default." +
        " To use this feature, mark the start of a gender-exclusive string with '@f' or '@m', and end it with '@'.")]
    public bool usingGenderMarkers = false;
    [TextArea(3, 10)]
    public string textEnglish;
    [TextArea(3, 10)]
    public string textItalian;
    private UIManager uiManager;


    // Start is called before the first frame update
    void Start()
    {
        if (UIManager.Instance == null)
            uiManager = FindObjectOfType<UIManager>();
        else
            uiManager = UIManager.Instance;

        if (uiManager == null)
            print("UIManager not found");

        if (autoGrabTextComponent && myTextMeshPro != null)
            myTextMeshPro = GetComponent<TextMeshProUGUI>();

        UpdateText();

    }

    public void UpdateText()
    {

        switch (uiManager.language)
        {
            case UIManager.Language.english:
                if (usingGenderMarkers)
                    textEnglish = uiManager.ChangeStringBasedOnGender(textEnglish);
                myTextMeshPro.text = textEnglish;
                break;
            case UIManager.Language.italian:
                if (usingGenderMarkers)
                    textItalian = uiManager.ChangeStringBasedOnGender(textItalian);
                myTextMeshPro.text = textItalian;
                break;

        }
    }
    
    
}
