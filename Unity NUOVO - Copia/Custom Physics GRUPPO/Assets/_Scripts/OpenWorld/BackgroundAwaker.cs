﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundAwaker : MonoBehaviour
{
    [SerializeField]
    public List<Component> preAwakes;
    public List<GameObject> objects;
    public List<Object> objectsToManuallyUnload;
    private int iteration = 0, subIteration=0;
    public int activateIndex = 0;
    private IPreAwake currentPreAwake;
    private int dim = 0;
    public ProximitySceneLoader mySceneLoader;

    public Dictionary<string, CustomGameManager.NPC> NPCs = new Dictionary<string, CustomGameManager.NPC>();
    public Dictionary<string, Event> events = new Dictionary<string, Event>();


    private void Awake()
    {       
        
    }

    public void StartPreAwakes() {
        iteration = 0;
        subIteration = 0;        
        dim = preAwakes.Count;
        if (dim > 0)
            currentPreAwake = (IPreAwake)preAwakes[iteration];
    }
    public void StartActivateObjects()
    {
        dim = objects.Count;
        if (activateIndex < 0)
            activateIndex = 0;
    }
    public void StartDeactivateObjects()
    {
        dim = objects.Count;
        if (activateIndex >= dim)
            activateIndex = dim - 1;
    }
    

    public bool ComputePreAwakes()
    {        
        if (iteration >= dim)
            return true;

        if (!currentPreAwake.PreAwake(subIteration, this))
        {
            subIteration++;
        }
        else
        {
            subIteration = 0;
            iteration++;
            if (iteration >= dim)
                return true;
            currentPreAwake = (IPreAwake)preAwakes[iteration];
        }
        return false;
    }

    public bool ActivateObjects(int speed)
    {
        for (int i = 0; i < speed; i++)
        {
            if (activateIndex >= dim)
            {
                activateIndex = dim - 1;
                return true;
            }

            //print("time: " + Time.deltaTime + ", iteration: " + activateIndex + ", item: " + objects[activateIndex].name);
            objects[activateIndex].SetActive(true);
            activateIndex++;
        }

        return false;
    }

    public bool DeactivateObjects(int speed)
    {
        for (int i = 0; i < speed; i++)
        {
            if (activateIndex < 0)
            {
                activateIndex = 0;
                return true;
            }

            //print("iteration: " + activateIndex);
            objects[activateIndex].SetActive(false);
            activateIndex--;
        }

        return false;
    }
}
