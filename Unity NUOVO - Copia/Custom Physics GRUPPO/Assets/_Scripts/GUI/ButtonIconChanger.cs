﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonIconChanger : MonoBehaviour
{
    public UIManager.ButtonName buttonName;
    private UIManager manager;

    // Start is called before the first frame update
    void Start()
    {
        if (UIManager.Instance == null)
            manager = FindObjectOfType<UIManager>();
        else
            manager = UIManager.Instance;

        Image image = GetComponent<Image>();
        if (manager != null && image != null) {
            manager.SetButtonIconImage(buttonName, image);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
