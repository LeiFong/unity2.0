﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleStaminaBarController : BarScaleController
{
    public ImageBasedProgressBar secondStaminaBar;
    StaminaBarController staminaBarControllerQuick, staminaBarControllerSlow;
    Status.Stances previousStance;
    private void Start()
    {
        
    }

    public override void BarInitialization()
    {
        base.BarInitialization();

        staminaBarControllerQuick = progressBar.GetComponent<StaminaBarController>();
        staminaBarControllerSlow = secondStaminaBar.GetComponent<StaminaBarController>();

        staminaBarControllerQuick.Initialize();
        staminaBarControllerSlow.Initialize();
        if (status.currentStance == Status.Stances.slow)
        {
            staminaBarControllerSlow.ActivateBar();
            staminaBarControllerQuick.DeactivateBar();
        }
        else
        {
            

            staminaBarControllerQuick.ActivateBar();
            staminaBarControllerSlow.DeactivateBar();
        }
    }
    protected override void ManageBars()
    {
        if (status != null && progressBar != null)
        {
            if (status.currentStance == Status.Stances.fast)
            { 
                if (previousStance == Status.Stances.slow)
                {
                    staminaBarControllerQuick.ActivateBar();
                    staminaBarControllerSlow.DeactivateBar();
                }
            }
            else
            {
                if (previousStance == Status.Stances.fast)
                {
                    staminaBarControllerSlow.ActivateBar();
                    staminaBarControllerQuick.DeactivateBar();
                }
            }

            SetSizeToSpecificBar(progressBar, status.currentStance == Status.Stances.fast ? status.currentStamina : status.secondaryStamina, status.maxStamina, 0);
            SetSizeToSpecificBar(secondStaminaBar, status.currentStance == Status.Stances.slow ? status.currentStamina : status.secondaryStamina, status.maxStamina, 0);

            //setSize(status.currentStance == Status.Stances.fast ? status.currentStamina : status.secondaryStamina, status.maxStamina, 0);
            
        }

        previousStance = status.currentStance;
    }

    void SetSizeToSpecificBar (ImageBasedProgressBar bar, int currentValue, int maxValue, int recoverable)
    {

        if (currentValue < 0)
            currentValue = 0;

        //print(currentValue + " " + maxValue + " " + currentValue / maxValue);

        float newValue = (float)currentValue / (float)maxValue;

        //print("newvalue: " + newValue);

        bar.SetValue(newValue);
        bar.SetRedBarValue((float)(currentValue + recoverable) / (float)maxValue);

        /*

		float sizeNormalized = ((float) currentValue) / ((float) maxValue);
		//print (maxValue);
		RedHealthPositionScale = ((float)(currentValue + recoverable)) / ((float)maxValue);
		bar.localScale = new Vector3 (sizeNormalized, bar.localScale.y);
        */

    }
}
