﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parry : MoveScript {

	public int parryHitstop = 10;
	public int parryDuration = 5;

	public override void MyStopSequence() {

		controller.seq = 0;
		status.mystatus = Status.status.neutral;
		//controller.animator.SetBool ("hitstun", false);
		//controller.animator.SetBool ("blockstun", false);

		//controller.animator.SetBool ("neutral", true);

		//parry = false;
		//StartCoroutine (status.WaitBeforeRecharge ());
		controller.currentMove = null;
		//controller.animator.speed = 1;
	}

	public override void SetStatus()
	{
		status.mystatus = Status.status.attacking;
	}
	public override void MyExecuteSequence ()
	{
		
	
		

		if (controller.seq == 0) {
			//controller.animator.SetBool ("hitstun", true);
			//controller.animator.Play (animatorParameterName, -1, 0f);
			//controller.ChangeParameter (animatorParameterName);



			//print (hurtboxHit.offenderController);
			//controller.animator.Play (animatorParameterName, -1, 0f);
			if (!hitstopAlreadyHappened) {
				controller.hitstop = parryHitstop;
				//controller.hurtboxhit.offenderController.hitstop = parryHitstop;

			}
			hitstopAlreadyHappened = true;



		}




		//if (status.mystatus == Status.status.hitstun1 || status.mystatus == Status.status.hitstun2) {
		if (controller.seq < parryDuration) {

			//print ("pushback duration: "+ hbh.offenderMoveSet.pushbackDuration);
			//if (controller.hitstop <= 0)

			//print (hbh.pushbackHit);
			//print (hbh.direction);
			//print (controller.targetVelocity.x);

			status.mystatus = Status.status.stuckinanimation;
			status.canRecharge = false;
			if (controller.hitstop <= 0)
				controller.seq++;


		}

	//else if (status.mystatus == Status.status.blockstun1 || status.mystatus == Status.status.blockstun2) {




	else {
			//StopSequence ();
			//moveIsFinished = true;
			itsTimeToStopTheSequence = true;
			//hbh.hit = false;
			//hbh.block = false;
			//hbh.juggle = false;

		}
	}

}
