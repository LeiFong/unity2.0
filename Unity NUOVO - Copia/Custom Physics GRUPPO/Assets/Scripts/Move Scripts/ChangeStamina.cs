﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeStamina : MoveScript {

	public int staminaChangeFrame = 20; //frame in cui avviene il cambio di stamina
	public int moveDuration = 40; //durata totale della mossa
    public float rechargePercentage; //percentuale di stamina ricaricata
    public int changeStaminaCooldown = 30; //frame di cooldown, durante i quali non può fare cambio stamina

    private Animator eyeAnimator;

    protected override void MyOnEnable()
    {
        //PlayerAnimatorRetriever ret = controller.GetComponentInChildren<PlayerAnimatorRetriever>();
        PlayerAnimatorRetriever ret = controller.animatorRetriever;
        if (ret != null) {
            eyeAnimator = ret.eyeEffects;

            if (eyeAnimator != null)
            eyeAnimator.Play(status.currentStance == Status.Stances.fast ? "Blue" : "Red", 1);
        }

    }

    public override void MyStartSequence ()
    {


        conditions.cantChangeStamina = 0;


    }

	public override void MyStopSequence() {

        conditions.cantChangeStamina = changeStaminaCooldown;
        

	}

    public override void SetStatus()
    {
        status.mystatus = Status.status.attacking;
    }
    public override void MyExecuteSequence ()
	{



		//status.canRecharge = false;
		if (controller.seq == 0) {
            //status.currentStamina -= staminaCost;
            //SetAnimationSpeed (move.actualAnimationFrames [0], dashDuration);
            //controller.animator.SetBool ("neutral", false);
            //controller.ChangeParameter (animatorParameterName, true);
            //controller.ChangeParameter(animatorParameterName);
            //StartCoroutine(status.WaitBeforeRecharge());
		}

        //conditions.cantChangeStamina = 0;

        
        status.mystatus = Status.status.attacking;
        


		if (controller.seq == staminaChangeFrame) {
            //status.currentStamina += (int) (rechargePercentage / 100 * status.maxStamina);
            SwitchStamina();
            if (status.currentStance == Status.Stances.slow)
            {
                status.currentStance = Status.Stances.fast;
                eyeAnimator.Play("Blue", 1);
                if (moveset.glowAnimationSetters.Length > 0)
                {
                    for (int i = 0; i < moveset.glowAnimationSetters.Length; i++)
                    {
                        moveset.glowAnimationSetters[i].SetStance(WeaponGlowAnimationSetter.Stance.quick);
                    }
                }
            }
            else
            {
                status.currentStance = Status.Stances.slow;
                eyeAnimator.Play("Red", 1);
                if (moveset.glowAnimationSetters.Length > 0)
                {
                    for (int i = 0; i < moveset.glowAnimationSetters.Length; i++)
                    {
                        moveset.glowAnimationSetters[i].SetStance(WeaponGlowAnimationSetter.Stance.power);
                    }
                }
            }
            eyeAnimator.Play("big glow", 0, 0f);
        }
        if (controller.seq > staminaChangeFrame)
        {
            //status.canRecharge = true;
        }
			

		//if (controller.attack)


		

		//if (moveDuration > controller.seq)
			controller.seq++;





        //else {
        if (moveDuration <= controller.seq)
        { 

            //StopSequence ();
            //moveIsFinished = true;
            itsTimeToStopTheSequence = true;


		}


	}

    private void SwitchStamina()
    {
        int s = status.currentStamina;
        status.currentStamina = status.secondaryStamina;
        status.secondaryStamina = s;

        //switchare le barre attive nella gui
    }
}
