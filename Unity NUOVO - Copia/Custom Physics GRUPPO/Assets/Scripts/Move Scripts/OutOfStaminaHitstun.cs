﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfStaminaHitstun : Hitstun
{
    //HurtboxHit hbh;
    public override void MyStartSequence()
    {
        controller.status.exhausted = true;
        //controller.shaking = true;

        status.currentStamina = status.maxStamina;

        controller.hitstop = 0;
        //controller.inHitstop = false;
        //print("HITSTOPPPP");

        if (hbh == null)
        {
            hbh = controller.lastCollisionHurtbox.hurtbox;
            FrameData tempFD = controller.lastCollisionHurtbox.move.frameData;

            if (hbh == null || tempFD == null)
            {
                //SetDefaultJuggleValues();
                //StopSequence();
                return;
            }
            //enemyMove = controller.lastCollisionHurtbox.move;
            if (controller.lastCollisionHurtbox.move.frameData.owner != null)
                controller.lastCollisionHurtbox.move.frameData.owner.hitstop = 0;
            controller.lastCollisionHurtbox.hurtbox = null;
            controller.lastCollisionHurtbox.move.frameData = null;

            

            //if (tempFD.hitstop > 7)
              //  controller.shaking = true;




            controller.cameraMovement.Shake(20, 0.2f);



            if (tempFD.isProjectile)
            {
                Vector2 enemyPosition = tempFD.projectileScript.transform.position;
                float positionDifference = Mathf.Sign(hbh.hurtbox.bounds.center.x - enemyPosition.x);



                if ((controller.facingRight && positionDifference > 0) || (!controller.facingRight && positionDifference <= 0))
                {
                    //print("da dietro");
                    controller.Flip();
                    //controller.animator.Play(fromBehindAnimatorParameter, 0, 0);
                    
                }
            }
            else
            {
                direction = tempFD.owner.facingRight ? 1 : -1;
                



                if ((controller.facingRight && direction > 0) || (!controller.facingRight && direction < 0))
                {
                    //print("da dietro");
                    controller.Flip();
                    //controller.animator.Play(fromBehindAnimatorParameter, 0, 0);
                }
            }
            if (controller.hitsparkRetriever != null)
            {
                if (status.myPreviousStatus == Status.status.blocking || status.previousIsBlocking || status.myPreviousStatus == Status.status.blockstun1 ||
                    status.myPreviousStatus == Status.status.guardBreak)
                    controller.hitsparkRetriever.GenerateSparksOnBlock(3, hbh.transform, 1.4f * direction, 0.5f);
                else
                    controller.hitsparkRetriever.GenerateSparksOnHit(5, hbh.transform, 1.4f * direction, 0.5f);
            }
            else
            {
                print("no hitspark retriever");
            }


        }
    }

    public override void MyStopSequence()
    {
        base.MyStopSequence();
        controller.status.exhausted = false;
    }


    public override void SetStatus()
    {
        status.mystatus = stunType;
    }

    public override void MyExecuteSequence()
    {
        //base.MyExecuteSequence();
        status.mystatus = stunType;
        status.canRecharge = true;

        //status.defensePercentage

        if (controller.seq >= hitstun)
        {
            //StopSequence ();
            //moveIsFinished = true;
            //print ("STOP");
            itsTimeToStopTheSequence = true;
            //hbh.hit = false;
            //hbh.block = false;
            //hbh.juggle = false;

        }
        else
        {
            controller.seq++;
            //status.currentStamina += status.staminaChargeSpeed * 3;
        }

    }





}
