﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loot : Event
{
    [System.Serializable]
    public struct LootQuantity
    {
        public Item item;
        public int quantity;
    }
    [System.Serializable]
    public struct WeaponLoot
    {
        public WeaponItem weapon;
        public int upgrade;
    }

    public bool dropsFromEnemy = false;
    public GameObject owner;
    public SpriteRenderer sprite;
    public LootQuantity quantifiableLoot;
    public WeaponLoot weaponLoot;

    public override void TriggerEvent()
    {
        playersManager.inventoryP1.LootItem(this);
        if (deactivatesOnEventTrigger)
        {
            if (sprite != null)
                sprite.enabled = false;
        }
        base.TriggerEvent();
    }
    
    protected MultiLanguageText FindPromptText()
    {
        if (quantifiableLoot.item != null)
        {
            if (quantifiableLoot.item is ConsumableItem) {
                promptType = PromptType.consumableLoot;
                return playersManager.inventoryP1.itemDatabase.consumableItemsDatabase[quantifiableLoot.item.itemCode].itemName;
            }
            else if (quantifiableLoot.item.itemType == Inventory.ItemType.material)
            {
                promptType = PromptType.materialLoot;

                return playersManager.inventoryP1.itemDatabase.materialsDatabase[quantifiableLoot.item.itemCode].itemName;
            }
        }
        else
        {
            if (weaponLoot.weapon != null)
            {
                promptType = PromptType.weaponLoot;

                return playersManager.inventoryP1.itemDatabase.weaponsDatabase[weaponLoot.weapon.weaponIndex].weaponWrapper.itemName;
            }
        }
        print("niente loot");
        return null;
    }

    public override MultiLanguageText GetPromptText()
    {
        promptText = FindPromptText();
        //print("getprompt");
        return base.GetPromptText();

    }

    protected override void InitializeEvent()
    {
        base.InitializeEvent();
        //print("event startttt");

        sprite = GetComponent<SpriteRenderer>();
        resetsOnLoad = false;

        if (dropsFromEnemy)
            serializableEvent = false;
        else
            serializableEvent = true;
    }

    public override void ActivateEvent()
    {
        //print("activate event");
        transform.parent = null;
        base.ActivateEvent();
        if (sprite != null)
        {
            sprite.enabled = true;
        }
    }

    public override void DeactivateEvent()
    {
        base.DeactivateEvent();
        //print("deacteivate event");
        if (sprite != null)
        {
            sprite.enabled = false;
        }

        if (dropsFromEnemy)
        {
            //eventHappened = false;

            if (owner != null)
                transform.parent = owner.transform;

            //ActivateEvent();

            //gameObject.SetActive(false);
            //Destroy(gameObject);
            //return;
        }
        
        
    }
}
