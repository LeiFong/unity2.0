﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EventData
{
    public string eventID;
    public bool hasHappened;
    public float [] position = new float[3];

    public EventData (Event newEvent)
    {
        
        eventID = newEvent.eventID;
        hasHappened = newEvent.eventHappened;
        position[0] = newEvent.transform.position.x;
        position[1] = newEvent.transform.position.y;
        position[2] = newEvent.transform.position.z;
        //Debug.Log("newevent " + position);
    }

}
