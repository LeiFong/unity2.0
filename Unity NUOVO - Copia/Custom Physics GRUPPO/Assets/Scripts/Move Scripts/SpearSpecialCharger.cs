﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpearSpecialCharger : MoveScript
{
    public SpearActivationConditions spearActivationConditions;

    public int chargeFrame;
    public int totFrames;
    public override void MyExecuteSequence()
    {
        if (controller.seq == chargeFrame)
        {
            spearActivationConditions.sniperGaming = true;

            spearActivationConditions.particleSystemAnimator.SetActiveCategory("Spear");
            spearActivationConditions.particleSystemAnimator.SetActiveIndex(0);
            spearActivationConditions.particleSystemAnimator.ParticleStart();
            spearActivationConditions.particleSystemAnimator.SetActiveIndex(1);
            spearActivationConditions.particleSystemAnimator.ParticleStart();
        }

        if (controller.seq < totFrames)
        {
            controller.seq++;
        }
        else
        {
            itsTimeToStopTheSequence = true;
        }
    }
}
