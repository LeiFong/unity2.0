﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hook : AttackScript {

	public float targetDistance = 8;
	public int hookFollowupStart = 20;

	public override void MyOnHit ()
	{
		move.currentHitProperties.pushbackHit = ((targetDistance - Mathf.Abs( controller.gameObject.transform.position.x - move.objectsHit[0].transform.position.x)) / (move.currentHitProperties.hitstun < move.currentHitProperties.pushbackDuration ?
			move.currentHitProperties.hitstun : move.currentHitProperties.pushbackDuration) / Time.deltaTime);



		//conditions.moves [followups [0].followupMoveIndex].usable = true;
		followups[0].cancelWindowStart = hookFollowupStart;
		//print (controller.gameObject.transform.position.x - objectHit.transform.position.x);
	}

	public override void MyStopSequence() {
		

		followups[0].cancelWindowStart = 100;
		controller.seq = 0;
		//Debug.Log ("STOP!");
		status.mystatus = Status.status.neutral;
		//controller.animator.SetBool ("neutral", true);
		//status.armor = 0;
		isCancelling = false;
		//controller.animator.SetTrigger(animatorParameterName);
		//StopCoroutine ("Attack");
		//StartCoroutine (status.WaitBeforeRecharge());
		controller.currentMove = null;
		//controller.ChangeParameter (controller.animParameter, false);
		//controller.animator.speed = 1;
		//print ("stop");

	}





}
