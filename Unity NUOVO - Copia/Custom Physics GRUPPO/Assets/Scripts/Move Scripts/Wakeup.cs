﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wakeup : MoveScript {

	public int wakeupTotalFrames = 60;
    public int deadLoopFrame = 200;
    float deceleration;
    float startingHorizontalVelocity;
    //public int wakeupAnimationTrigger = 20; //frame in cui parte l'animazione del rimettersi in piedi (fino ad allora sta a terra, o rimbalza etc)

    public override void MyStartSequence()
    {
        isCharging = false;
        //print("wakeup ded");
        
        startingHorizontalVelocity = horizontalMovements[0].velocity;
        deceleration = Mathf.Abs(startingHorizontalVelocity) / 90;

        if (status.currentHealth <= 0)
        {
            status.dead = true;
            status.mystatus = Status.status.dead;
            controller.frontalDeath = true;

            if (!controller.animator.GetBool(controller.dedIndex))
            {
                controller.animator.SetBool(controller.dieingIndex, true);
                
            }
            else
            {
                controller.animator.SetBool(controller.dieingIndex, false);
            }

        }
    }

    public override void MyExecuteSequence ()
	{
        //if (controller.seq < wakeupFrames) 
        {
			if (controller.seq == 0) {
				//print ("primo frame di wakeup");
				//controller.animator.SetBool ("neutral", false);
				//controller.ChangeParameter (animatorParameterName, true);
				//controller.ChangeParameter (animatorParameterName);
				//controller.animator.Play(animatorParameterName, -1, 0f);
			}
            if (startingHorizontalVelocity > 0)
            {
                horizontalMovements[0].velocity -= deceleration;
                if (horizontalMovements[0].velocity < 0)
                    horizontalMovements[0].velocity = 0;

            }
            else
            {
                horizontalMovements[0].velocity += deceleration;
                if (horizontalMovements[0].velocity > 0)
                    horizontalMovements[0].velocity = 0;
            }
            if (status.currentHealth > 0)
                status.mystatus = Status.status.wakeup;
            else
            {
                status.mystatus = Status.status.dead;
                if (relatedMoves.Length > 0)
                {
                    if (controller.seq >= deadLoopFrame)
                    {

                        controller.StartNewMove(relatedMoves[0]);
                        return;
                    }
                }
            }

			status.invincible = true;
            //controller.avoidingCharacters = true;
			controller.seq++;

		}

        //if (controller.seq == wakeupAnimationTrigger)
        {
            //controller.animator.SetTrigger("wakeup");
        }

        if (status.currentHealth > 0 && controller.seq >= wakeupTotalFrames)
        {

            //StopSequence ();
            //moveIsFinished = true;
            itsTimeToStopTheSequence = true;
        }
		
	}

	public override void MyStopSequence () {
		controller.seq = 0;
        isCharging = false;
		//Debug.Log ("STOP!");
		//controller.animator.SetBool ("land", false);
		//controller.animator.SetBool ("neutral", true);
		status.mystatus = Status.status.neutral;
		status.invincible = false;

		controller.currentMove = null;
	}

}