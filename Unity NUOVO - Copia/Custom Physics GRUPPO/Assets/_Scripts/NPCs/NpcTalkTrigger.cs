﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcTalkTrigger : Event
{
    [Space(20)]
    public NpcInteraction npc;
    
    public override void TriggerEvent()
    {
        npc.StartInteraction();
    }
}
