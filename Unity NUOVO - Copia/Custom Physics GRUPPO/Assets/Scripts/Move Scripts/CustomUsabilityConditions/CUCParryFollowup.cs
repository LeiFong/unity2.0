﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CUCParryFollowup : CustomUsabilityConditions
{
    public int startFrameFromParry, startFrameFromBlock;
    public override bool CheckMyUsabilityConditions()
    {
        /*
        if (relativeMove.status.mystatus == Status.status.blockstun1)
        {
            if (relativeMove.controller.hasParried)
            {
                if (relativeMove.controller.seq >= startFrameFromParry)
                {
                    return true;
                }
                
            }
        }
        else
        */
        {
            if (relativeMove.status.myPreviousStatus == Status.status.blockstun1)
            {
                return true;
            }
        }

        return false;
    }
}
