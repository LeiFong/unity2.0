﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackScript : MoveScript {

    public override void SetStatus()
    {
		status.mystatus = Status.status.attacking;
	}

    public override void MyStopSequence() {
		

		controller.seq = 0;
		//Debug.Log ("STOP!");
		status.mystatus = Status.status.neutral;
		//controller.animator.SetBool ("neutral", true);
		//status.armor = 0;
		isCancelling = false;
		//controller.animator.SetTrigger(animatorParameterName);
		//StopCoroutine ("Attack");
		//StartCoroutine (status.WaitBeforeRecharge());
		controller.currentMove = null;
		//controller.ChangeParameter (controller.animParameter, false);
		//controller.animator.speed = 1;
		//print ("stop");

	}
		

	public override void MyExecuteSequence ()
	{
		//print (controller.seq);

		//move = LoadFrameData (controller.moveIndex);
		//status.canRecharge = false;
		


		for (int i = 0; i < move.projectiles.Length; i++)
		{
			move.projectiles[i].proj.startingPosition = move.projectiles[i].projectileOrigin.position;

			if (controller.seq == move.projectiles[i].startup - 1)
			{
				

				Shoot(move.projectiles[i].proj, move.projectiles[i].projectileOrigin.position);
			}
		}


		/*
		if (move.hitboxes.Length > 0 && move.hitboxes[0].startup <= move.midFrames) {
			move.midFrames = 0;
			//recoil = 0;
		}
		*/

		//if (controller.attack)
		{
			//print ("BAUUU");
			

			/*
			if (controller.seq < move.hitboxes[0].startup - move.midFrames) {
				startupPhase = true;
				activePhase = false;
				recoveryPhase = false;
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * (move.startupVelocity);
				//Debug.Log ("start");
			} else if (move.hitboxes[0].startup - move.midFrames <= controller.seq && controller.seq < move.hitboxes[0].startup) {
				startupPhase = true;
				recoveryPhase = false;
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * (move.activeVelocity);

				//Debug.Log ("active");
			} else if (controller.seq >= move.hitboxes[0].startup) {
				startupPhase = false;
				recoveryPhase = true;
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * (move.recoveryVelocity);

			*/

            /*
			for (int i = 0; i < hitboxes.Length; i++)
            {
				//print (hitboxes.Length);
				if (move.hitboxes [i].startup + move.hitboxes [i].active -1 > controller.seq && controller.seq >= move.hitboxes [i].startup -1) {
					//print ("sono attackscript, gameobject: " + controller.gameObject + "frame: " + Time.frameCount);

					EnableHitbox (i);
					break;
					//controller.hitstop = 30;
				}
					else
						DisableHitbox (i);
			}
            */
				
				//Debug.Log("recovery");
		}

		



			//Debug.Log (controller.seq);
			//Debug.Log (attack);

			

			//Debug.Log (controller.targetVelocity);
			controller.seq++;
		//print ("BAUUU");

			controller.startupCounter++;
			/*
			if (move.followups.Length != 0) {

				int a = move.cancelWindowStart + move.cancelWindowEnd;
				//Debug.Log (a);
				Debug.Log (controller.seq);
				if (controller.seq >= move.cancelWindowStart && controller.seq <= a)
				{
					for (int i = 0; i < move.followups.Length; i++) {
						move.followups [i].GetComponent<FrameData> ().actionUsable = true;
						Debug.Log (move.followups [i].GetComponent<FrameData> ().actionUsable);
						//controller.StartNewMove (move.followupAttack);
					}
				}


				else 
				{
					for (int i = 0; i < move.followups.Length; i++) {
						move.followups [i].GetComponent<FrameData> ().actionUsable = false;
						Debug.Log (move.followups [i].GetComponent<FrameData> ().actionUsable);
						//controller.StartNewMove (move.followupAttack);
					}
				}


			}
			*/
			if (controller.seq >= totalFrames) {
			//print (totalFrames);
				//targetVelocity.x = 0;
				//status.mystatus = Status.status.standing;
				//Debug.Log("FINITO! " + totalFrames);
				//StopSequence ();
			//moveIsFinished = true;
			itsTimeToStopTheSequence = true;

			}




	}

	public void Shoot(Projectile projectileScript, Vector2 position)
	{
		//print ("pewpew");
		/*
		GameObject newProj = Instantiate(projectile, position, Quaternion.identity);
		newProj.SetActive(true);
		Projectile projScript = newProj.GetComponent<Projectile>();
		projScript.startingPosition = position;
		projScript.move.faction = move.faction;
		projScript.move.owner = move.owner;
		projScript.move.facingRight = controller.facingRight;
		*/

		//projectileScript.startingPosition = position;
		projectileScript.move.faction = move.faction;
		projectileScript.move.owner = move.owner;
		projectileScript.move.facingRight = controller.facingRight;

		GameObject newProj = Instantiate(projectileScript.gameObject, position, Quaternion.identity);
		newProj.SetActive(true);
		//print("pew " + controller.seq);
	}
}
