﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ConsumableItem : Item
{
    //public int maxQuantity;
    public enum Subcategory
    {
        potions, soos, ammo, misc
    }
    public bool removeOnDepletion = true;
    public Subcategory subcategory;
    public int subCategoryIndex;

}
