﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour
{
    public ItemEffect[] consumableItemsDatabase;
    public ItemWrapper[] materialsDatabase;
    public MoveSet[] weaponsDatabase;

    private void Start()
    {
        GenerateItemID();
    }

    public void GenerateItemID ()
    {
        for (int i = 0; i < consumableItemsDatabase.Length; i++)
        {
            //itemDatabase[i].item = itemDatabase[i].GetComponent<Item>();
            consumableItemsDatabase[i].item.itemCode = i;
        }

        for (int i = 0; i < materialsDatabase.Length; i++)
        {
            materialsDatabase[i].item.itemCode = i;
        }

        
    }

    public ItemWrapper GetItemFromDatabase (Item item)
    {

        if (item is ConsumableItem)
        {
            if (item.itemCode < consumableItemsDatabase.Length)
            {
                return consumableItemsDatabase[item.itemCode];
            }
           
        }
        if (item is MaterialItem)
        {
            if (item.itemCode < materialsDatabase.Length)
            {
                return materialsDatabase[item.itemCode];
            }
        }

        

        print("oggetto non trovato");
        return null;
    }

    public ItemWrapper GetItemFromDatabase(int itemCode, Inventory.ItemType itemType)
    {

        if (itemType == Inventory.ItemType.consumable)
        {
            if (itemCode < consumableItemsDatabase.Length)
            {
                return consumableItemsDatabase[itemCode];
            }

        }
        if (itemType == Inventory.ItemType.material)
        {
            if (itemCode < materialsDatabase.Length)
            {
                return materialsDatabase[itemCode];
            }
        }



        print("oggetto non trovato");
        return null;
    }

}
