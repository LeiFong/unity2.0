﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIDescription : MonoBehaviour
{
    public Selectable selectable;
    public int timeToAppear = 0;
    private Animator animator;
    private EventSystem eventSystem;
    private bool show = false;
    private int timer = 0;
    private CanvasGroup canvasGroup;
    

    // Start is called before the first frame update
    void Start()
    {
        eventSystem = Object.FindObjectOfType<EventSystem>();
        animator = GetComponent<Animator>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (selectable.gameObject == eventSystem.currentSelectedGameObject)
        {
            if (timer < timeToAppear)
                timer++;
            else if (!show)
            {
                show = true;
                if (animator != null)
                    animator.SetTrigger("Open Quick");
            }

        }
        else {
            timer = 0;
            if (show) {
                show = false;
                if (animator != null)
                    animator.SetTrigger("Close Quick");
            }
        }

        if (!show) {
            if (canvasGroup != null) {
                canvasGroup.alpha = 0;
                
            }
        }


    }
}
