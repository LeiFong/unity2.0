﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Airdash : MoveScript {

	public int totalDashDuration = 20;
	public int startDashDuration;
	public int midDashDuration;
	public float startDashVelocity = 6;
	public float midDashVelocity;
	public float recoveryDashVelocity;
	//public int staminaCost;

	/*
	void OnEnable () {
		controller = transform.parent.transform.parent.GetComponent<Controller2D> ();
		status = transform.parent.transform.parent.GetComponent<Status> ();
		LoadFrameData ();
		controller.seq = 0;
	}
*/

	public override void MyStopSequence() {

		controller.horizontalMomentum = 0;

		controller.seq = 0;
		//Debug.Log ("STOP!");
		status.mystatus = Status.status.neutral;
		controller.avoidingCharacters = false;
		controller.isAffectedByGravity = true;

		//StopCoroutine ("Attack");
		//StartCoroutine (status.WaitBeforeRecharge());
		//controller.animator.SetBool ("dash", false);
		//controller.animator.SetBool ("neutral", true);
		//controller.animator.speed = 1;
		controller.currentMove = null;

	}

	public override void MyExecuteSequence ()
	{


		//status.canRecharge = false;
		if (controller.seq == 0) {
			//status.currentStamina -= staminaCost;
			controller.startupCounter = 0;
			//SetAnimationSpeed (move.actualAnimationFrames [0], dashDuration);
			//controller.animator.SetBool ("neutral", false);
			//controller.ChangeParameter (animatorParameterName, true);
			//controller.ChangeParameter(animatorParameterName);

		} 




		//if (controller.attack)


		//status.mystatus = Status.status.attacking;
		//if (controller.seq < totalDashDuration)
        {
			controller.startupCounter++;
			controller.isAffectedByGravity = false;
			controller.velocity.y = 0;
			status.mystatus = Status.status.dashing;
			//controller.avoidsCharacters = true;
			if (controller.seq < startDashDuration) {
				controller.horizontalMomentum = controller.gameObject.transform.localScale.x * startDashVelocity;
			} else if (controller.seq < midDashDuration) {
				controller.horizontalMomentum = controller.gameObject.transform.localScale.x * midDashVelocity;
			} else {
				controller.horizontalMomentum = controller.gameObject.transform.localScale.x * recoveryDashVelocity;

			}
			//Debug.Log ("start");
			controller.seq++;

		} 





		//else {
        if (controller.seq >= totalDashDuration)
        { 
			//StopSequence ();
			//moveIsFinished = true;
			itsTimeToStopTheSequence = true;


		}


	}
}
