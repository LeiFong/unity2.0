﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryItemFiller : MonoBehaviour
{
    //public bool mouseOverForTutorial;
    [Tooltip("These text fields will be voided when you have no items in the inventory")]
    public List<Text> descriptionTextsToBeMadeBlank;
    private List<ItemUIManager> buttons;
    private Inventory inventory;
    private int listDim = 0;
    private List<Inventory.ConsumablesQuantity> consumables;
    private List<Inventory.MaterialsQuantity> materials;
     
    private UIManager uiManager;

    // Start is called before the first frame update
    void Start()
    {
        inventory = FindObjectOfType<PlayersManager>().inventoryP1;
        consumables = inventory.consumables;
        materials = inventory.materials;
        buttons = new List<ItemUIManager>(GetComponentsInChildren<ItemUIManager>());
        for (int i = 0; i < buttons.Count; i++) {
            buttons[i].SetID(i);
            buttons[i].SetInventory(inventory);
            buttons[i].SetInventoryFiller(this);
        }

        uiManager = FindObjectOfType<UIManager>();
    }

    public void LoadConsumableItems() {

        if (buttons.Count < consumables.Count) {
            print("Can't load all the consumable items! Add buttons with ItemUIManager components as children of this.");
        }
        int dim = Mathf.Min(buttons.Count, consumables.Count);
        for (int i = 0; i < dim; i++) {
            

            buttons[i].gameObject.SetActive(true);
            buttons[i].SetReferenceItem(consumables[i].item, consumables[i].quantity);
            buttons[i].SetItemNameText();
            buttons[i].SetQuantityText();
            
        }
        for (int i = dim; i < buttons.Count; i++) {
            buttons[i].gameObject.SetActive(false);
        }
        if (dim == 0)
            BlankDescriptionTexts();

        listDim = dim;
    }

    public void LoadMaterialItems() {
        if (buttons.Count < materials.Count)
        {
            print("Can't load all the material items! Add buttons with ItemUIManager components as children of this.");
        }
        int dim = Mathf.Min(buttons.Count, materials.Count);
        for (int i = 0; i < dim; i++)
        {
            buttons[i].gameObject.SetActive(true);
            buttons[i].SetReferenceItem(materials[i].item, materials[i].quantity);
            buttons[i].SetItemNameText();
            buttons[i].SetQuantityText();

        }
        for (int i = dim; i < buttons.Count; i++)
        {
            buttons[i].gameObject.SetActive(false);
        }
        if (dim == 0)
            BlankDescriptionTexts();

        listDim = dim;
    }

    public void SetSelectedIndex(int index) {
        if (listDim == 0)
        {
            BlankDescriptionTexts();
            return;
        }
        int i = index;
        if (i >= listDim)
            i = listDim - 1;
        
        EventSystem.current.SetSelectedGameObject(buttons[i].gameObject);
    }

    public void SetSelectedButtonAnimationAsHighlighted(int index) {
        if (listDim == 0)
        {
            BlankDescriptionTexts();
            return;
        }
        int i = index;
        if (i >= listDim)        
            i = listDim - 1;
        buttons[i].SetButtonAnimationAsHighlighted();
    }

    public void BlankDescriptionTexts() {
        for (int i = 0; i < descriptionTextsToBeMadeBlank.Count; i++) {
            descriptionTextsToBeMadeBlank[i].text = "";
        }
    }
}
