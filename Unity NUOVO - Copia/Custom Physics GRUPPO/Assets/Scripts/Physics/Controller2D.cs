﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


[System.Serializable]
public class Controller2D : CastPhysics {
    [HideInInspector]
    public int walkBlendIndex, vspeedIndex, walkspeedIndex, directionLockIndex, directionLockFloatIndex, walkTriggerIndex, fastWalkTriggerIndex, slowWalkTriggerIndex, loopStateIndex, walkingIndex,
        walkForwardIndex, walkBackwardsIndex, neutralIndex, neutraltriggerIndex, airbourneIndex, animationSpeedIndex, dedIndex, dieingIndex;
    [HideInInspector]
    public float animatorSpeed;
    [HideInInspector]
    public bool unscaledAnimatorSpeed;
    [HideInInspector]
    public int recoveryPeriod = 30; //tempo in frame subuto dopo aver swingato in cui prendi danno in più
    [HideInInspector]
    public float shakerWeight;
    public int recoveryCounter = 0;
    int eyesFlamesLayerIndex;
    protected Transform targetingTransform;
    [HideInInspector]
    public bool forcedAirborne, forcedGrounded, frontalDeath;
    [HideInInspector]
    public bool hitWhileInvincible = false;
    public string myID; //stringa utilizzata per individuare e serializzare gli NPC
    public bool initialFacingRight;
    public bool hitstopEnabled = true;
    //[HideInInspector]
    public int hitDelay = 0;
    public Vector3 lastSerializablePosition; //ultima posizione "buona", usata per serializzare
    public bool lastSerializableFacing;
    public Vector3 startingPosition;
	public EnemySpecializer enemySpecializer;
	public bool gachiBass = false;
	public int recentlyJumpedFrames = 0; //frames trascorsi dall'ultimo salto
	public int jumpLeniency = 5; //se sei in stato aereo da questi frames e non perchè hai saltato, allora è ancora possibile eseguire un salto
    public EnemyCanvas enemyCanvas; // il canvas usato per la gui dei nemici
	//protected List<HurtboxHit> hurtboxesHitList;
	public bool directionLock = false;
    public bool rightDirectionLock, leftDirectionLock;
    public string activeWeaponParameter;
    public GameObject activeWeaponSprite;

    public float currentTimeScale = 1;
    public DamageDisplayGenerator damageDisplayGenerator;

    public GetGrabbed getGrabbed;

    [HideInInspector]
    public float frontVisibilityModifier;
    [HideInInspector]
    public float backVisibilityModifier;

    public bool holdingBlock;
    public bool counterhitState;
    public List<TimedFrameData> timedFrameDatas = new List<TimedFrameData>();
    public PlayerAnimatorRetriever animatorRetriever;
    public GameObject animationsObject;
    [HideInInspector]
    public bool insideEnemyGhostHitbox;
    protected Transform specialMovesTransform;
    Transform movesetTransform;

    //AnimatorUpdateMode animatorUpdateMode;

    /*
	public float jumpHeight = 4;
	public float timeToJumpApex = .4f;
	float accelerationTimeAirbourne = .2f;
	float acceleratinTimeGrounded =.1f;
	public float moveSpeed = 6;

	float jumpVelocity;
	float gravity;
	Vector3 velocity;
	float velocityXSmoothing; 

	Controller2D controller;	

	void Start(){
		controller = GetComponent<Controller2D>();
		gravity = -(2 * jumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex; 	
		print ("gravity: " + gravity + " Jump Velocity: " + jumpVelocity);
	}

	void Update(){
		if (controller.collisions.below || controller.collisions.above) {
			Debug.Log ("BUUH");
			
		velocity.y = 0;
		}

		Vector2 input = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));

		if (Input.GetKeyDown (KeyCode.Space) && controller.collisions.below) {
			velocity.y = jumpVelocity;
		}

		float targetVelocityX = input.x * moveSpeed;
		velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below)?acceleratinTimeGrounded:accelerationTimeAirbourne);
		velocity.y += gravity * Time.deltaTime;
		controller.Move(velocity * Time.deltaTime);
	}

*/
    public TargetableList targetList;

	public int hitstop = 0;
	//public int minimumBlockstunFramesToStartCounter = 8; //devi essere almeno questi frame in blockstun per fare un counter
    
	public string horizontalmovementCMD = "P1_horizontal";
	public string atk1CMD = "P1_attack1";
	public string atk2CMD = "P1_attack2";
	public string jumpCMD = "P1_jump";
	public string dashCMD = "P1_dash";
	public string blockCMD = "P1_kick";
	public string targetCMD = "P1_target";
	public string verticalmovementCMD = "P1_vertical";
	public string startCMD = "P1_start";
	public string selectCMD = "P1_select";
	public string special1CMD = "P1_special1";
	public string special2CMD = "P1_special2";
	public string special3CMD = "P1_special3";
	public string special4CMD = "P1_special4";
    

	public int jumpstartup = 0;
	public bool currentlyUsingJoystick = false; //settare a false se stai usando la tastiera, settare a true se stai usando joystick

    public int atk1Buffer, dashBuffer, atk2Buffer, jumpBuffer, blockBuffer, parryBuffer, targetBuffer, actionBuffer, special1Buffer, special2Buffer, special3Buffer, special4Buffer;
    public float horizontal, vertical;
	public bool atk1pressed, atk1released, dashpressed, dashreleased, atk2pressed, atk2released, jumppressed, jumpreleased, blockpressed, blockreleased;
	public bool targetpressed, targetreleased, actionpressed, actionreleased, special1pressed, special1released, special2pressed, special2released,
		special3pressed, special3released, special4pressed, special4released;
	public bool upPressed, upreleased, downpressed, downreleased, rightpressed, rightreleased, leftpressed, leftreleased;
	public float up, down, left, right, verticalprevious, horizontalprevious;
	//private int jumpDuration = 0;

	public AnimationUtilities animUtils;
	public string animParameter;
	public Animator animator, shakerAnimator;
	public MoveScript currentMove, previousMove;
	public float movespeed = 6;
	public float runSpeed = 10;
	//public float jumpVelocity = 5;
	//public bool customLanding = false;

	public List<int> enemyBuffers = new List<int>(); //lista dei buffer dei nemici, ne usiamo uno per ogni mossa invece dei pochi del giocatore; da settare in loadinputs
    public List<int> enemyAnimationsBuffers = new List<int>(); //lista delle animazioni del nemico
    //public LayerMask HitboxMask;
    //public ContactFilter2D hitboxContactFilter;
    public Vector2 move = Vector2.zero;
	public Status status;

    
	//public Status status;
	//public GameObject weapon;
	//public MoveSet move;

	//protected int attack;
	//public MoveScript landing, wakeup;
	//public HurtboxHit hurtboxhit;

	public ActivationConditions conditions;
    public WeaponSwitcher weaponSwitcher;
    public HitsparkRetriever hitsparkRetriever;

	public MoveSet moveset, animationset, specialMoves;
	public MoveScript [] moves, animations;
	public bool skipExecution;
	public int moveIndex;
	public int seq = 0;
    public int previousMoveSeq;
	private int juggleFramesCount = 0;

	public bool facingRight = true;
	public bool previousFacingRight;

	public int startupCounter = 0;
    //public List<Collider2D> untestedHitboxes = new List<Collider2D>();
	public bool runToggle = false;
	public bool previousRunToggle = false;
	public int aerialFrames = 0;
    public EquipmentManager equipmentManager;
	public delegate void DeathEvent (GameObject obj);
	public static event DeathEvent death;
    public CharacterContainer characterContainer;

    public struct TimedFrameData
    {
        public HurtboxHit.HitInfo hitInfo;
        public HurtboxHit hurtbox;
        public int timer;
    }

    //public int hitDelay = 30;

    //public Dictionary<FrameData, HurtboxHit> collisionHurtbox = new Dictionary<FrameData, HurtboxHit>();
    public Dictionary<HurtboxHit.HitInfo, HurtboxHit> collisionHurtbox = new Dictionary<HurtboxHit.HitInfo, HurtboxHit>();


    int randomInterval;
    int randomCounter;
    public int walkLayerIndex, runLayerIndex, walkBackwardsLayerIndex, juggleLayerIndex, armsLayerIndex, variantLayerIndex;
    public float variantLayerWeight;
	public BuffManager buffManager;
	public struct collisionHurtboxStruct {
		public HurtboxHit.HitInfo move;
		public HurtboxHit hurtbox;
	}

    public CameraMovement cameraMovement;

    public collisionHurtboxStruct lastCollisionHurtbox; //dove viene salvata l'ultima collisione tra hurtbox e mossa nemica; viene usata da player per decidere quale collisione usare

    [System.Serializable]
    public struct MyBuff
    {
        public Buff buff;
        public int buffTimer;

    }

    public List<MyBuff> myBuffs = new List<MyBuff> ();

	protected List<FrameData> offenders = new List<FrameData>();
	public bool isParrying = false; //vero se è nella parry window
	public List<FrameData> projectileOffenders = new List<FrameData>();
    public List<projectileDuration> projectileDurations = new List<projectileDuration>();

	public int framesSpentWalking = 0;
	public int minimumWalkFramesBeforeRunning = 10;
	public int minimumJumpDurationBeforeAirdash = 8;
	public float minimumGroundDistanceBeforeAirAction = 3;

    public float variantAnimatorLayerWeightLossDefault = 0.1f;
    public float variantAnimatorLayerWeightGainDefault = 0;

    public float variantAnimatorLayerWeightLossCurrent = 0.1f;
    public float variantAnimatorLayerWeightGainCurrent = 0;
    public bool shaking = false;

    [System.Serializable]
    public struct HurtboxParameters
    {
        public float offsetX, offsetY, sizeX, sizeY;

    }

    public HurtboxParameters [] hurtboxParameters;

    [System.Serializable]
    public struct projectileDuration
    {
       public FrameData frameData;
       public int hitFrequency;
    }

    /*
	public void AddBuff (Buff buff) {
		buff.durationTimer = buff.buffTotalDuration;
        MyBuff newBuff = new MyBuff
        {
            buff = buff,
            buffTimer = buff.buffTotalDuration
        };
        myBuffs.Add (newBuff);
	}
    */
	public void ResetProjectileOffenders () {
		//if (status.mystatus == Status.status.neutral) 
		{
			//print ("reset");
			for (int i = 0; i < projectileOffenders.Count; i++) {
				if (projectileOffenders [i].objectsHit.Contains (gameObject)) {
					projectileOffenders [i].objectsHit.Remove (gameObject);
				}
			}
			projectileOffenders.Clear ();
		}
	}

    //metodo per resettare vari valori, chiamato all'inizio di ogni frame 
    public void SetDefaultValues ()
    {
        status.currentMeterGainMultiplier = status.meterGainMultiplier;
        conditions.specialMoveUsable = 0;
        //forcedAirborne = false;

    }


    public void ApplyBuffEffects () {

        if (buffManager != null)
        {
            buffManager.ManageBuffDurations();
        }
	}

    public void ManageEnemyHealthBar(bool enabled)
    {
        if (enemyCanvas != null)
        {
            //print("enemyhealthbar " + gameObject);

            for (int i = 0; i < enemyCanvas.bars.Count; i++)
            {
                enemyCanvas.bars[i].gameObject.SetActive(enabled);
            }
        }
    }

    public void ManageProjectileCollisionHurtboxes (FrameData enemyMove, HurtboxHit hurtbox) {

	}

	public void ManageCollisionHurtboxes (ref HurtboxHit.HitInfo enemyMove, HurtboxHit hurtbox) {
        //print("ADD " + enemyMove.frameData.gameObject + " " + hurtbox.gameObject);

        //aggiungo me stesso alla lista degli oggetti colpiti dall'avversario, e se ci sono già, ritorna
        if (!enemyMove.frameData.objectsHit.Contains (gameObject))
        //&& (enemyMove.alternativeHitboxes.Length == 0 || !enemyMove.alternativeHitboxes[0].objectsHit.Contains(gameObject)))
        {
            //print("ADD111 " + enemyMove);
			enemyMove.frameData.objectsHit.Add (gameObject);
            for (int i = 0; i < enemyMove.frameData.alternativeHitboxes.Length; i++)
            {
                if (enemyMove.frameData.alternativeHitboxes[i].priority < enemyMove.frameData.priority)
                {
                    enemyMove.frameData.alternativeHitboxes[i].objectsHit.Add(gameObject);
                    print("prioooo");
                }
            }
			//if (enemyMove.isProjectile && !projectileOffenders.Contains(enemyMove))
				//projectileOffenders.Add (enemyMove);
            if (enemyMove.frameData.isProjectile)
            {
                for (int i = 0; i < projectileDurations.Count; i++)
                {
                    if (projectileDurations[i].frameData == enemyMove.frameData)
                    {
                        //print("gia colpito");
                        return;
                    }

                }
                projectileDuration newProjectile;
                newProjectile.frameData = enemyMove.frameData;
                newProjectile.hitFrequency = enemyMove.frameData.projectileScript.hitFrequency;
                projectileDurations.Add(newProjectile);
            }
                
                
		}
		else {

            if (collisionHurtbox.ContainsKey(enemyMove) && collisionHurtbox[enemyMove].hurtboxPriority < hurtbox.hurtboxPriority)
            {
                collisionHurtbox.Remove(enemyMove);
                collisionHurtbox.Add(enemyMove, hurtbox);
                print("priority");
            }

                return;

           
		}
        //print("enemymove " + enemyMove.frameData.gameObject);
        //se la mossa non mi ha colpito, aggiungimi, altrimenti controlla se l'hurtbox colpito ha priorità maggiore
        if (!collisionHurtbox.ContainsKey (enemyMove)) {
            //print("non contiene: " + enemyMove.frameData.gameObject);
            if (!enemyMove.frameData.isProjectile) {

                //collisionHurtbox.Add(enemyMove, hurtbox);
                //print("added: " + enemyMove.hitType + ", sono " + gameObject);

                for (int i = 0; i < enemyMove.frameData.alternativeHitboxes.Length; i++)
                {
                    foreach (HurtboxHit.HitInfo hitinfo in collisionHurtbox.Keys)
                    {
                        if (hitinfo.frameData == (enemyMove.frameData.alternativeHitboxes[i]))
                        {
                            if (enemyMove.frameData.priority > enemyMove.frameData.alternativeHitboxes[i].priority)
                            {
                                collisionHurtbox.Remove(hitinfo);
                                print("removed1: " + hitinfo.frameData.gameObject);
                                break;
                                
                            }
                            else
                            {
                                collisionHurtbox.Remove(enemyMove);
                                print("removed2: " + enemyMove);
                                break;
                                
                            }
                            //return; //da aggiustare
                        }
                        else
                        {
                            /*
                            collisionHurtbox.Add(enemyMove, hurtbox);
                            if (!enemyMove.owner.inHitstop)
                                enemyMove.owner.hitstop = status.currentStamina > 0 ? enemyMove.hitstop : 40;
                            if (!inHitstop)
                                hitstop = status.currentStamina > 0 ? enemyMove.hitstop : 40;
                                */
                        }
                    }
                }
                
                //if (!enemyMove.owner.inHitstop)
                if (hitDelay == 0)
                if (enemyMove.frameData.owner.hitstopEnabled)
                {
                    
                    if (enemyMove.hitType != HurtboxHit.HitType.parry)
                    {
                            //if (currentMove == null || (currentMove.move != null && currentMove.move.customHitstopBehaviour == null))
                        if (getGrabbed == null || enemyMove.frameData.grab == null ||
                                (enemyMove.hitType != HurtboxHit.HitType.hit && enemyMove.hitType != HurtboxHit.HitType.juggle) ||
                                !enemyMove.frameData.grab.CheckGrabConditions(this))
                                //NO GRAB
                        {
                                if (enemyMove.frameData.currentHitProperties.hitstun > 0)
                                {
                                    
                                    //if (enemyMove.hitType != HurtboxHit.HitType.block
                                      //  || (status.currentStamina > 0 && status.currentStamina > enemyMove.staminaDamage))
                                    if (enemyMove.hitType != HurtboxHit.HitType.guardBreak || enemyMove.deathBlow)
                                    {
                                        //print("guardbreak: " + enemyMove.hitType + " deathblow: " + enemyMove.deathBlow);
                                        
                                        if (enemyMove.frameData.customHitstopBehaviour == null)
                                        {

                                            
                                            if (enemyMove.hitType != HurtboxHit.HitType.block)
                                            {
                                                shaking = true;
                                                enemyMove.frameData.owner.hitstop = enemyMove.frameData.currentHitProperties.hitstop;
                                                hitstop = enemyMove.frameData.currentHitProperties.hitstop;

                                                if (enemyMove.hitType == HurtboxHit.HitType.armor)
                                                {
                                                    if (hitsparkRetriever != null)
                                                    {
                                                        hitsparkRetriever.GenerateSparksOnHit(3,
                                                            hurtboxes[0].transform, facingRight ? 1 : -1, 1f);
                                                    }
                                                    else
                                                    {
                                                        print("no hitspark retriever");
                                                    }
                                                }
                                            }
                                            
                                            else
                                            {
                                                if (status.myPhysicsStatus == Status.physicsStatus.grounded &&
                                                    enemyMove.frameData.owner.conditions.weaponBounceAnimation != null &&
                                                    status.currentHealth > enemyMove.healthDamage)
                                                /*
                                                if (!enemyMove.frameData.currentHitProperties.kick && status.myPhysicsStatus == Status.physicsStatus.grounded &&
                                                    enemyMove.frameData.owner.conditions.weaponBounceAnimation != null &&
                                                    status.currentHealth > enemyMove.healthDamage)
                                                */
                                                {
                                                    {
                                                        enemyMove.frameData.owner.hitstop = 12;
                                                        hitstop = 12;
                                                    }
                                                    //enemyMove.frameData.owner.StartNewMove(enemyMove.frameData.owner.conditions.weaponBounceAnimation);
                                                    
                                                    
                                                    
                                                }
                                                else
                                                {
                                                    enemyMove.frameData.owner.hitstop = enemyMove.frameData.currentHitProperties.hitstop;
                                                    hitstop = enemyMove.frameData.currentHitProperties.hitstop;
                                                    //print("muoriiiii");
                                                }



                                            }
                                            
                                            //print("hitstop " + hitstop + " " + enemyMove.frameData.owner.hitstop);
                                        }
                                        else
                                        {
                                            //print("custom hitstop");
                                            enemyMove.frameData.customHitstopBehaviour.SetHitstop(enemyMove.frameData.owner, this);
                                        }

                                        if (enemyMove.counterhit && hitstop > 0)
                                        {
                                            hitstop += 0;
                                            enemyMove.frameData.owner.hitstop += 0;

                                        }
                                    }
                                    else
                                    {
                                        //da testare hitstop
                                        enemyMove.frameData.owner.hitstop = 0;
                                        hitstop = 0;
                                        //print("no hitstop");
                                        shaking = false;
                                    }
                                }

                                if (!enemySpecializer.isAI)
                                {
                                    //if (enemyMove.healthDamage >= status.currentHealth)
                                    if (enemyMove.deathBlow)
                                    {
                                        //enemyMove.frameData.owner.hitstop = 0;
                                        //hitstop = 0;
                                    }
                                }

                            }
                        else //GRAB
                        {
                                //print("owner grab!");
                                status.invincible = true;
                                enemyMove.hitType = HurtboxHit.HitType.grab;
                                //ownerMoveSet.owner.status.invincible = true;
                                enemyMove.frameData.owner.timedFrameDatas.Clear();
                                //ownerMoveSet.owner.StartNewMove(ownerMoveSet.grab);
                                enemyMove.frameData.owner.status.invincible = true;
                                enemyMove.frameData.owner.lastCollisionHurtbox.move.frameData = null;
                                enemyMove.frameData.owner.lastCollisionHurtbox.hurtbox = null;
                                enemyMove.frameData.owner.collisionHurtbox.Clear();
                                hitDelay = 0;
                                enemyMove.frameData.owner.hitDelay = 0;
                        }
                    }
                    else //PARRY
                    {
                            if ((status.mystatus != Status.status.blocking && !status.isBlocking && status.mystatus != Status.status.blockstun1)
                                        || (status.currentStamina > 0 && status.currentStamina > enemyMove.staminaDamage))
                            {
                                if (currentMove.move != null && currentMove.move.customHitstopBehaviour != null)
                                {
                                    currentMove.move.customHitstopBehaviour.SetHitstop(this, enemyMove.frameData.owner);
                                    print("CUSTOM HITSTOP");
                                }
                                else
                                {
                                    enemyMove.frameData.owner.hitstop = 0;
                                    hitstop = 0;
                                }
                            }
                            else
                            {
                                //da testare hitstop
                                enemyMove.frameData.owner.hitstop = 0;
                                hitstop = 0;
                            }


                            if (!enemyMove.frameData.currentHitProperties.kick && status.myPhysicsStatus == Status.physicsStatus.grounded &&
                                enemyMove.frameData.owner.conditions.weaponBounceAnimation != null &&
                                status.currentHealth > enemyMove.healthDamage)
                            {
                                
                                enemyMove.frameData.owner.StartNewMove(enemyMove.frameData.owner.conditions.weaponBounceAnimation);
                                
                                //enemyMove.frameData.owner.shaking = false;
                            }
                            /*
                            else
                            {
                                enemyMove.frameData.owner.hitstop = enemyMove.frameData.currentHitProperties.hitstop;
                                hitstop = enemyMove.frameData.currentHitProperties.hitstop;
                                //print("muoriiiii");
                            }
                            */
                        }


                    }
                if (hitDelay > 0)
                    hitstopEnabled = false;
                /*
                if (enemyMove.frameData.grab != null && enemyMove.hitType == HurtboxHit.HitType.hit && enemyMove.frameData.grab.CheckGrabConditions(this))
                {
                    print("owner grab!");
                    status.invincible = true;
                    enemyMove.hitType = HurtboxHit.HitType.grab;
                    //ownerMoveSet.owner.status.invincible = true;
                    enemyMove.frameData.owner.timedFrameDatas.Clear();
                    //ownerMoveSet.owner.StartNewMove(ownerMoveSet.grab);
                    enemyMove.frameData.owner.status.invincible = true;
                    enemyMove.frameData.owner.lastCollisionHurtbox.move.frameData = null;
                    enemyMove.frameData.owner.lastCollisionHurtbox.hurtbox = null;
                    enemyMove.frameData.owner.collisionHurtbox.Clear();
                    hitDelay = 0;
                    enemyMove.frameData.owner.hitDelay = 0;
                }
                */
                collisionHurtbox.Add(enemyMove, hurtbox);

            } else {
                
				collisionHurtbox.Add (enemyMove, hurtbox);

                if (enemyMove.frameData.currentHitProperties.hitstun > 0)
                {
                    if ((status.mystatus != Status.status.blocking && !status.isBlocking && status.mystatus != Status.status.blockstun1)
                        || (status.currentStamina > 0 && status.currentStamina > enemyMove.staminaDamage))
                    {
                        if (currentMove != null && currentMove.move != null)
                        {
                            if (currentMove.move.customHitstopBehaviour != null)
                            {
                                currentMove.move.customHitstopBehaviour.SetHitstop(this, enemyMove.frameData.owner);

                            }
                            else
                            {
                                //enemyMove.frameData.owner.hitstop = enemyMove.frameData.hitstop;
                                hitstop = enemyMove.frameData.currentHitProperties.hitstop;
                                print("HSTOPPPPP");
                            }
                        }
                        else
                        {
                            hitstop = enemyMove.frameData.currentHitProperties.hitstop;
                        }
                    }
                    else
                    {
                        //da testare hitstop
                        enemyMove.frameData.owner.hitstop = 0;
                        hitstop = 0;
                    }
                }
               
                
            }

			

		} else { //se non sbaglio non si dovrebbe mai arrivare in questa parte di codice ma vabbe
			//print ("YES");

			if (collisionHurtbox [enemyMove].hurtboxPriority < hurtbox.hurtboxPriority) {
				//print ("YES");
				if (!enemyMove.frameData.isProjectile) {
                    if (hitDelay == 0)
                    if (enemyMove.frameData.owner.hitstopEnabled)
                    {
                        if (isParrying)
                        {
                            float positionDifference = Mathf.Sign(hurtbox.hurtbox.bounds.center.x - hurtbox.enemyPosition.bounds.center.x);
                            
                            if (enemyMove.frameData.currentHitProperties.kick || ((positionDifference < 0 && !facingRight) || (positionDifference > 0 && facingRight)))
                            {
                                isParrying = false;
                            }
                        }
                        if (!isParrying)
                        {
                            //if (currentMove == null || (currentMove.move != null && currentMove.move.customHitstopBehaviour == null))
                            if (enemyMove.frameData.grab == null)
                            {
                                if (enemyMove.frameData.customHitstopBehaviour == null)
                                {

                                    enemyMove.frameData.owner.hitstop = status.currentStamina > 0 ? enemyMove.frameData.currentHitProperties.hitstop : 20;
                                    hitstop = status.currentStamina > 0 ? enemyMove.frameData.currentHitProperties.hitstop : 20;
                                }
                                else
                                {
                                    shaking = true;

                                    enemyMove.frameData.customHitstopBehaviour.SetHitstop(enemyMove.frameData.owner, this);
                                }
                            }
                        }
                        else
                        {

                            if (currentMove.move != null && currentMove.move.customHitstopBehaviour != null)
                            {
                                shaking = true;

                                currentMove.move.customHitstopBehaviour.SetHitstop(this, enemyMove.frameData.owner);

                            }
                        }
                        
                    }
                    if (hitDelay > 0)
                        hitstopEnabled = false;
                } else {
					collisionHurtbox.Add (enemyMove, hurtbox);
                    hitstop = status.currentStamina > 0 ? enemyMove.frameData.currentHitProperties.hitstop : 0;
                }
				//lastCollisionHurtbox.hurtbox = hurtbox;
				//lastCollisionHurtbox.move = enemyMove;
			}
		}
		//print (hitstop);
	}

	void OnDisable () {

        //print ("disable");
        //animator.Play ("neutral", -1, 0f);
        //animator.keepAnimatorControllerStateOnDisable = true;


        MyOnDisable();
	}



    public virtual void MyOnDisable ()
    {
        ManageEnemyHealthBar(false);

        //if (currentMove != null)
          //  currentMove.StopSequence();
        //seq = 0;

        if (animator != null)
        animator.SetTrigger(neutraltriggerIndex);
    }

    protected override void MyStart(int preAwakeIndex, BackgroundAwaker bgAwaker) {

        if (preAwakeIndex == 2)
        {
            grounded = true;
            recoveryPeriod = 40;

            if (animatorRetriever != null)
            {
                shakerAnimator = animatorRetriever.shaker;
                animator = animatorRetriever.skeleton;
                if (animatorRetriever.eyesFlames != null)
                {
                    eyesFlamesLayerIndex = animatorRetriever.eyesFlames.GetLayerIndex("Intensity");
                }
            }
            animator.keepAnimatorControllerStateOnDisable = true;
            //animator.Play("neutral", -1, 0f);
            seq = 0;
            if (currentMove != null)
            {
                print("timetostop");
                currentMove.itsTimeToStopTheSequence = true;
            }

            //cameraMovement = FindObjectOfType<CameraMovement>();
            if (cameraMovement == null)
            {
                if (CameraMovement.Instance == null)
                {
                    cameraMovement = FindObjectOfType<CameraMovement>();
                    if (cameraMovement == null)
                    {
                        print("NO CAMERAMOVEMENT");
                        return;
                    }
                    //print("find");
                }
                else
                {
                    cameraMovement = CameraMovement.Instance;
                    //print("instance");
                }
            }



            if (enemyCanvas != null)
            {
                enemyCanvas.EnemyCanvasInitialize(this);

            }

            if (hitsparkRetriever != null)
            {
                hitsparkRetriever.InitializeSparks();
            }
            else
            {
                print("NO HITSPARK RETRIEVER");
            }

            /*
            if (!changesGravityWhenDescending) {
                descendingGravity = originalGravity;
            }
            
            variantLayerIndex = animator.GetLayerIndex("Variants");
            walkLayerIndex = animator.GetLayerIndex("walk");
            runLayerIndex = animator.GetLayerIndex("run");
            walkBackwardsLayerIndex = animator.GetLayerIndex("walkbackwards");
            juggleLayerIndex = animator.GetLayerIndex("juggle");
            armsLayerIndex = animator.GetLayerIndex("Arms");
            */
            //status = GetComponent<Status> ();
            status.InitializeStatus(this);
            //animUtils = transform.Find ("Skeleton").GetComponent<AnimationUtilities>();
            //animUtils = GetComponentInChildren<AnimationUtilities>();

            myID = (characterContainer.gameObject.name + " " + transform.position.x + " " + transform.position.y + " " + transform.position.z);

            if (transform.localScale.x < 0)
            {
                facingRight = false;
            }
            else
                facingRight = true;

            initialFacingRight = facingRight;

            lastSerializableFacing = facingRight;
            lastSerializablePosition = tr.position;
            startingPosition = tr.position;




            //getGrabbed = GetComponentInChildren<GetGrabbed>();

            //LoadInputs();
            backgroundAwaker = bgAwaker;
            LoadInfos();
            return;
        }
        else if (preAwakeIndex >= 3 && preAwakeIndex < 7)
        {
            LoadWeapon(preAwakeIndex);
            return;
        }
        else if (preAwakeIndex == 7)
        {
            LoadInputs();


            //if (enemySpecializer != null && enemySpecializer.isAI)
              //  LoadInputs();

            targetList.Initialize();
            targetingTransform = targetList.transform;
            status.hyperArmor = status.maxHyperArmor;

            SetDefaultValues();

            hurtboxParameters = new HurtboxParameters[hurtboxes.Length];

            return;
        }

        else if (preAwakeIndex == 8)
        {

            for (int i = 0; i < hurtboxParameters.Length; i++)
            {
                hurtboxParameters[i].sizeX = hurtboxes[i].hurtbox.size.x;
                hurtboxParameters[i].sizeY = hurtboxes[i].hurtbox.size.y;
                hurtboxParameters[i].offsetX = hurtboxes[i].hurtbox.offset.x;
                hurtboxParameters[i].offsetY = hurtboxes[i].hurtbox.offset.y;

            }

            
            //characterContainer = GetComponentInParent<CharacterContainer>();
            //if (characterContainer != null)
            {
                if (characterContainer.disableObjectOnStart)
                {
                    characterContainer.gameObject.SetActive(false);
                }
            }
            return;
        }
    }

    private void Start()
    {
        pm = PlayersManager.Instance ?? FindObjectOfType<PlayersManager>();
        
        if (pm.debug)
        {
            if (pm.player1 == this)
                print("ATTENZIONE, TOGLIERE DONELOADING");
            int index = 0;
            while (!PreAwake(index, null))
            {
                index++;
            }
            pm.doneLoading = true; //DEBUG!!!
            
        }

        collidersOffset = Mathf.Abs(verticalCollider.bounds.center.y - verticalCollider.bounds.extents.y
        - (utilityCollider.bounds.center.y - utilityCollider.bounds.extents.y));

        verticalColliderBounds = verticalCollider.bounds;

        capsuleColliderInfo.r = verticalCollider.bounds.extents.x;
        capsuleColliderInfo.cylinderHeight = verticalCollider.bounds.size.y - verticalCollider.bounds.size.x;
        capsuleColliderInfo.lowerCenter = new Vector2(0, -capsuleColliderInfo.cylinderHeight / 2);
        capsuleColliderInfo.upperCenter = new Vector2(0, capsuleColliderInfo.cylinderHeight / 2);

        lineOfSightOrigin = new Vector2(verticalCollider.bounds.center.x, verticalCollider.bounds.max.y - 0.2f);
        lineOfSightTarget = new Vector2(verticalCollider.bounds.center.x, verticalCollider.bounds.min.y + 0.2f);


        variantLayerIndex = animator.GetLayerIndex("Variants");
        walkLayerIndex = animator.GetLayerIndex("walk");
        runLayerIndex = animator.GetLayerIndex("run");
        walkBackwardsLayerIndex = animator.GetLayerIndex("walkbackwards");
        juggleLayerIndex = animator.GetLayerIndex("juggle");
        armsLayerIndex = animator.GetLayerIndex("Arms");

        //animator.SetBool(activeWeaponParameter, false);
        activeWeaponParameter = moveset.weaponAnimatorParameter;
        animator.SetBool(activeWeaponParameter, true);

        
    }

    public virtual void LoadInfos () {

	}

	public virtual void LoadInputs () {
		
	}

	//funzione che rimette a falso le bool settate a vero in Update
	protected void ResetInputs () {
		atk1pressed = atk2pressed = jumppressed = dashpressed = blockpressed = atk1released = atk2released = jumpreleased = dashreleased = blockreleased = false;


	}

	public void InputDeviceSwitch () {
		currentlyUsingJoystick = !currentlyUsingJoystick;
		LoadInputs ();
	}

	protected virtual void InputCheck() {

	}

	protected virtual void InputCombinationCheck() {

	}

	public void FlushBuffers() {
        //if (status.faction == 0)
        //print("FLUSHH");
		atk1Buffer=dashBuffer=atk2Buffer=jumpBuffer=blockBuffer=special1Buffer=parryBuffer=special2Buffer=special3Buffer=0;
		for (int i = 0; i < enemyBuffers.Count; i++) {
			enemyBuffers [i] = 0;
		}

        horizontal = 0;
        vertical = 0;

        for (int i = 0; i < enemyAnimationsBuffers.Count; i++)
        {
            enemyAnimationsBuffers[i] = 0;
        }
    }

    public void FlushDefensiveBuffers()
    {
        //if (status.faction == 0)

        dashBuffer = blockBuffer = special1Buffer = parryBuffer = 0;
        
    }



    public void ResetAllHurtboxesSize ()
    {
        for (int i = 0; i < hurtboxParameters.Length; i++)
        {
            hurtboxes[i].hurtbox.size = new Vector2(hurtboxParameters[i].sizeX, hurtboxParameters[i].sizeY);
            hurtboxes[i].hurtbox.offset = new Vector2(hurtboxParameters[i].offsetX, hurtboxParameters[i].offsetY);
        }
    }

    public void ResetHurtboxSize (int hurtboxIndex)
    {
        hurtboxes[hurtboxIndex].hurtbox.size = new Vector2(hurtboxParameters[hurtboxIndex].sizeX, hurtboxParameters[hurtboxIndex].sizeY);
        hurtboxes[hurtboxIndex].hurtbox.offset = new Vector2(hurtboxParameters[hurtboxIndex].offsetX, hurtboxParameters[hurtboxIndex].offsetY);
    }

	//Debug.Log (targetVelocity); 


	protected override void MyUpdate ()
    {

		if (enemySpecializer == null || !enemySpecializer.isAI)
        {

		    InputCheck ();
		    InputCombinationCheck ();
		}


        if ((status.faction != 0 || (enemySpecializer != null && enemySpecializer.isAI)) && enemyCanvas != null && enemySpecializer.healthBarPosition != null)
        {
            enemyCanvas.transform.position = enemySpecializer.healthBarPosition.position;
        }

    }

    protected override void UpdateAnimatorMode(bool onFixedUpdate)
    {
        if (onFixedUpdate)
        {
            animator.updateMode = AnimatorUpdateMode.AnimatePhysics;
        }
        else
        {
            if (animator.updateMode != AnimatorUpdateMode.UnscaledTime)
                animator.updateMode = AnimatorUpdateMode.Normal;
        }
    }

    protected void InputBufferDecrease() {
        if (!enemySpecializer.isAI)
        {
            if (dashBuffer > 0)
                dashBuffer--;
            if (atk1Buffer > 0)
                atk1Buffer--;
            if (atk2Buffer > 0)
                atk2Buffer--;
            if (jumpBuffer > 0)
                jumpBuffer--;
            if (blockBuffer > 0)
                blockBuffer--;
            if (parryBuffer > 0)
                parryBuffer--;
            if (targetBuffer > 0)
                targetBuffer--;
            if (special1Buffer > 0)
                special1Buffer--;
            if (special2Buffer > 0)
                special2Buffer--;
            if (special3Buffer > 0)
                special3Buffer--;
            if (special4Buffer > 0)
                special4Buffer--;
        }
        /*
        else
        {
            for (int i = 0; i < enemyBuffers.Count; i++)
            {
                if (enemyBuffers[i] > 0)
                    enemyBuffers[i]--;
            }
            for (int i = 0; i < enemyAnimationsBuffers.Count; i++)
            {
                if (enemyAnimationsBuffers[i] > 0)
                    enemyAnimationsBuffers[i]--;
            }
        }
        */


		//da aggiornare con tutti i tasti released!!!
		if (targetreleased)
			targetreleased = false;


		

    }

    public void MoveTargetingBox()
    {
        
        if (randomCounter == randomInterval)
        {
            targetingTransform.position = tr.position;
            targetingTransform.localScale = tr.localScale;
            randomCounter = -1;
            randomInterval = Random.Range(0, 19);
            //if (status.faction == 1)
            //print("randominterval: " + randomInterval);
        }
        randomCounter++;
    }

    protected override void ManageTimeChanges (bool startOfFrame)
    {
        //if (TimeVariables.canExecuteLogic)
        {
            if (startOfFrame)
            {
                //if (TimeVariables.timeHasChanged)
                {

                    animator.speed = animatorSpeed;// / TimeVariables.defaultTimeScale;

                    //unscaledAnimatorSpeed = false;
                }
            }
            else
            {
                //if (TimeVariables.currentTimeScale != timeManager.defaultTimeScale)
                {
                    if (!unscaledAnimatorSpeed && TimeVariables.currentTimeScale != timeManager.defaultTimeScale)
                        animator.speed /= TimeVariables.currentTimeScale;
                    else
                    {
                        animator.speed /= timeManager.defaultTimeScale;
                    }

                    animatorRetriever.AdjustAnimatorsSpeedBasedOnTimeScale();
                }
            }

            
        }
        if (hitstop > 0 && TimeVariables.currentTimeScale > 1)
        {
            //animator.speed = 0;
            //print("stop animatorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
        }


    }
    public bool hasParried;
    protected override void ComputeVelocity ()
	{
        unscaledAnimatorSpeed = false;
        frontVisibilityModifier = 0;
        backVisibilityModifier = 0;
        counterhitState = false;
        //hasParried = false;

        //if (status.faction == 0)
          //  print("status: " + status.mystatus);

        if (collisions.isIgnoringPlatforms > 0)
            collisions.isIgnoringPlatforms--;

        if (collisions.isIgnoringStairs > 0)
            collisions.isIgnoringStairs--;

        MoveTargetingBox();
        
        //InputSystem.Update();
        //Debug.DrawRay(transform.position, Vector2.right, Color.white);
        SetDefaultValues();

        ApplyBuffEffects();


        if (enemySpecializer != null && enemySpecializer.isAI)
        {
            InputCheck();
        }


        if (variantLayerIndex > 0)
            variantLayerWeight = animator.GetLayerWeight(variantLayerIndex);
        animator.SetBool(neutralIndex, true);
        variantAnimatorLayerWeightGainCurrent = variantAnimatorLayerWeightGainDefault;
        variantAnimatorLayerWeightLossCurrent = variantAnimatorLayerWeightLossDefault;

        //SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        //renderer.sprite = 
        if (!enemySpecializer.isAI)
        {
            //print("FLUSHHHH");
            //FlushBuffers();
            
            if (status.mystatus != Status.status.guardBreak && 
                (currentMove == null || (seq < currentMove.canBlockInterval.end && seq >= currentMove.canBlockInterval.start)))
            {
                if (directionLock && status.currentStamina > 0)
                //if (holdingBlock)
                {
                    status.isBlocking = true;
                
                    //horizontal *= 0.7f;
                }
                else
                {
                    status.isBlocking = false;
                }
            }
            else
            {
                status.isBlocking = false;
                
            }
            
        }

		
		if (jumpstartup > 0) {
			jumpstartup++;
			//print ("jumpstartup: " + jumpstartup + ", gameobj: " + gameObject);
		}
		/*
		if (changesGravityWhenDescending) {
			if (status.myPhysicsStatus == Status.physicsStatus.airborne) {
				if (velocity.y < changingGravityVelocity) {
					currentGravity = descendingGravity;
				}
			} else
				currentGravity = originalGravity;
		}
	*/		

		/*
		if (!status.invincible) {
			foreach (MoveScript em in collisionHurtbox.Keys) {
				HurtboxHit hurtb = collisionHurtbox [em];
				hurtb.GetHit (em, em.move);
			}
		}
		*/

		if (status.invincible) {
			/*
			foreach (HurtboxHit hb in hurtboxes)
				hb.hurtbox.enabled = false;

		} else {
			foreach (HurtboxHit hb in hurtboxes)
				hb.hurtbox.enabled = true;
				*/
		}

        //ManageHurtboxCollisions ();

        //InputBufferCheck ();
        //InputBufferDecrease();


        //targetVelocity.x = 0;

        //Hitstun();
        /*
        if (hitDelay > 0)
        {
            if (!inHitstop)
                hitDelay--;

        }
        else
        {
            hitDelay = 0;
            hitstopEnabled = true;
        }
        */
		if (currentMove != null && currentMove.itsTimeToStopTheSequence) {
			
			currentMove.itsTimeToStopTheSequence = false;
            if (currentMove.autoFollowup.followupMove != null && currentMove.autoFollowup.cancelStart <= 0)
            {
                print("currentmove: " + currentMove + " autofollowup: " + currentMove.autoFollowup.followupMove);
                StartNewMove(currentMove.autoFollowup.followupMove);
            }
            else 
                currentMove.StopSequence();

        }

        //print (status.mystatus);
        //if (status.mystatus == Status.status.neutral)
        //	ResetProjectileOffenders ();

        {
            foreach (HurtboxHit.HitInfo em in collisionHurtbox.Keys)
            {
                if (em.frameData.grab != null && em.hitType == HurtboxHit.HitType.grab && getGrabbed != null)
                {
                    timedFrameDatas.Clear();
                    lastCollisionHurtbox.hurtbox = collisionHurtbox[em];
                    lastCollisionHurtbox.move = em;
                    //print("prima: " + lastCollisionHurtbox.move.hitType);
                    //lastCollisionHurtbox.move.hitType = HurtboxHit.HitType.grab;
                    //lastCollisionHurtbox.hurtbox.GetHit(em.moveScript, em.frameData);
                    break;
                }
                //print("em: " + em);
                for (int k = 0; k < em.frameData.alternativeHitboxes.Length; k++)
                {
                    em.frameData.alternativeHitboxes[k].objectsHit.Add(gameObject);
                }

                HurtboxHit hurtb = collisionHurtbox[em];

                {

                    TimedFrameData newTimedFD = new TimedFrameData
                    {
                        hitInfo = em,
                        timer = status.mystatus == Status.status.blocking || status.mystatus == Status.status.blockstun1 || status.mystatus == Status.status.guardBreak ?
                        0 : hitDelay,
                        hurtbox = hurtb
                    };

                    timedFrameDatas.Add(newTimedFD);
                    if (!em.frameData.isProjectile)
                    {
                        
                        //em.moveScript.OnHit();

                    }
                }


            }
        }
        

        for (int i = 0; i < timedFrameDatas.Count; i++)
        {
            if (!inHitstop)
            {
                TimedFrameData timedFD = new TimedFrameData
                {
                    hitInfo = timedFrameDatas[i].hitInfo,
                    timer = timedFrameDatas[i].timer - 1,
                    hurtbox = timedFrameDatas[i].hurtbox


                };
                timedFrameDatas[i] = timedFD;
            }
            //print("timer " + i + " " + timedFrameDatas[i].timer + " framedata " + timedFrameDatas[i].frameData.gameObject);
            
            if (timedFrameDatas[i].timer <= 0)
            {
                //timedFrameDatas[i].frameData.owner.hitstopEnabled = true;

                status.currentMeter += Mathf.RoundToInt((float)timedFrameDatas[i].hitInfo.frameData.currentHitProperties.opponentMeterGain * (float)status.currentMeterGainMultiplier 
                    * (float)status.currentMeterGainOnHitReceivedMultiplier);
                if (status.currentMeter > status.maxMeter)
                    status.currentMeter = status.maxMeter;
                //print("got hit and got meter " + timedFrameDatas[i].hitInfo.frameData.opponentMeterGain * status.currentMeterGainPercentage / 100);

                {
                    if (timedFrameDatas[i].hitInfo.frameData.isProjectile)
                    {
                        timedFrameDatas[i].hurtbox.GetHitByProjectile(timedFrameDatas[i].hitInfo.frameData);
                    }

                    else
                    {
                        //timedFrameDatas[i].hurtbox.GetHit(timedFrameDatas[i].hitInfo.moveScript, timedFrameDatas[i].hitInfo.frameData);
                        timedFrameDatas[i].hurtbox.GetHit(timedFrameDatas[i].hitInfo);

                    }

                    lastCollisionHurtbox.hurtbox = timedFrameDatas[i].hurtbox;
                    lastCollisionHurtbox.move = timedFrameDatas[i].hitInfo;

                    //collisionHurtbox.Remove(em);
                }

                timedFrameDatas.RemoveAt(i);
                i--;

                //break;
            }

        }
        //if (!status.invincible) 

        status.currentHealthPercentage = Mathf.Max(0, Mathf.RoundToInt(100*status.currentHealth / status.maxHealth));
        //Land();
        if (status.currentHealth <= 0)
        {
            FlushBuffers();
            if (enemySpecializer != null && !enemySpecializer.hasDroppedLoot && (status.faction != 0 || enemySpecializer.isAI)
                && !inHitstop && seq >= enemySpecializer.lootDropFrame && status.myPhysicsStatus == Status.physicsStatus.grounded)
            {
                enemySpecializer.hasDroppedLoot = true;
                Vector2 dropPosition = lastSerializablePosition;

                for (int i = 0; i < enemySpecializer.lootDropProbabilities.Length; i++)
                {
                    if (enemySpecializer.lootDropProbabilities[i].loot == null)
                    {
                        continue;
                    }

                    int p = Random.Range(0, 100);

                    if (enemySpecializer.lootDropProbabilities[i].dropProbability >= p)
                    {
                        //enemySpecializer.lootDropProbabilities[i].loot.gameObject.SetActive(true);
                        enemySpecializer.lootDropProbabilities[i].loot.ActivateEvent();
                        enemySpecializer.lootDropProbabilities[i].loot.transform.parent = null;

                        enemySpecializer.lootDropProbabilities[i].loot.transform.position = dropPosition;
                        dropPosition.x = transform.position.x + Random.Range(-1f, +1f);
                    }
                }
            }
            if (status.mystatus != Status.status.dead && status.mystatus != Status.status.juggle)
            {
                //animator.SetBool("ded", true);
                //print("ded: " + status.mystatus);
                //status.invincible = true;
                status.mystatus = Status.status.dead;
                status.dead = true;

                ManageEnemyHealthBar(false);

                //print("ded");
                if (!status.invincible)
                {
                    //print("die cheater");
                    if (lastCollisionHurtbox.hurtbox != null && lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.juggle)
                    {
                        StartNewMove(animations[10]);
                    }
                    else
                        StartNewMove(animations[21]);
                }
                //status.invincible = true;
            }

        }
        else
        {
            
            status.dead = false;
            animator.SetBool(dedIndex, false);
            animator.SetBool(dieingIndex, false);
            
        }
        
        Hitstun();

		collisionHurtbox.Clear ();

        if (hitDelay > 0)
        {
            if (!inHitstop)
                hitDelay--;

        }
        else
        {
            hitDelay = 0;
            hitstopEnabled = true;
        }

        //lastCollisionHurtbox.move = null; 
        //lastCollisionHurtbox.hurtbox = null;
        /*
		if (status.mystatus == Status.status.neutral) {

			for (int i = 0; i < projectileOffenders.Count; i++) {
				if (projectileOffenders [i].objectsHit.Contains (gameObject)) {
					projectileOffenders [i].objectsHit.Remove (gameObject);
				}
			}
			projectileOffenders.Clear ();
		}
			*/

        /*
        if (status.mystatus == Status.status.neutral)
        {
            animator.SetBool("neutral", true);

        }
        else
        {
            animator.SetBool("neutral", false);

        }
        */

        if (projectileDurations.Count > 0)
        {
            List<projectileDuration> newProjectileDurations = new List<projectileDuration>();

            for (int i = 0; i < projectileDurations.Count; i++)
            {
                if (projectileDurations[i].frameData != null)
                {

                    if (projectileDurations[i].hitFrequency > 0)
                    {

                        projectileDuration newPD = projectileDurations[i];
                        newPD.hitFrequency--;

                        newProjectileDurations.Add(newPD);

                    }
                    else
                    {
                        projectileDurations[i].frameData.objectsHit.Remove(gameObject);
                    }

                }
            }

            projectileDurations = newProjectileDurations;

        }

        /*
        if (status.mystatus == Status.status.neutral)
        {
            //ResetProjectileOffenders();
           
        }
        */

        if (grounded && (status.mystatus != Status.status.juggle || (status.mystatus == Status.status.juggle && seq > 1)))
        {
            status.myPhysicsStatus = Status.physicsStatus.grounded;
            //if (status.faction == 0)
              //  print("grounded " + grounded + " " + status.mystatus + " " + seq);

        }
        else
        {
            status.myPhysicsStatus = Status.physicsStatus.airborne;
            //if (status.faction == 0)
              //  print("airborne " + grounded + " " + status.mystatus + " " + seq);
        }

        if (forcedAirborne)
        {
            status.myPhysicsStatus = Status.physicsStatus.airborne;
            //print("forcedairborne");

        }
        else if (forcedGrounded)
        {
            status.myPhysicsStatus = Status.physicsStatus.grounded;
        }

        forcedAirborne = false;
        forcedGrounded = false;


        if (status.myPhysicsStatus == Status.physicsStatus.airborne)
			jumpDuration++;


        if (status.myPhysicsStatus == Status.physicsStatus.airborne && status.mystatus == Status.status.neutral && currentMove == null &&
            !animator.GetNextAnimatorStateInfo(0).IsTag("Jump")
            && !animator.GetCurrentAnimatorStateInfo(0).IsTag("Jump"))
        {
            animator.Play("jump rising", 0, 0f);
            
        }

		//Debug.Log (status.myPhysicsStatus);

		if (status.mystatus == Status.status.juggle && hitstop > 0) {
            //print("juggle airborne");
			status.myPhysicsStatus = Status.physicsStatus.airborne;
		}

		if (status.myPhysicsStatus == Status.physicsStatus.airborne) {
            animator.SetBool (airbourneIndex, true);
            
            //animator.Play("jump rising");
            if (status.mystatus == Status.status.neutral && currentMove == null && !animator.GetCurrentAnimatorStateInfo(0).IsTag("Jump")) {
                //animator.SetLayerWeight (juggleLayerIndex, 0);
                //animator.Play("jump rising", -1);
                //animator.SetLayerWeight(juggleLayerIndex, 1);
                //animator.SetLayerWeight(layerinde)

            }
			else if (status.mystatus == Status.status.juggle) {
                //animator.SetBool ("airbourne", true);
                //animator.SetLayerWeight (juggleLayerIndex, 1);
                //if (inHitstop)
                if (hitstop > 0)
                {
                    animator.Play("juggle takeoff", 0, 0f);
                }

			}





		}
		else if (status.myPhysicsStatus == Status.physicsStatus.grounded) {
			animator.SetBool (airbourneIndex, false);
            //animator.SetLayerWeight (juggleLayerIndex, 0);
            

        }




        //print (status.mystatus);








        //Debug.Log (velocity.y);
        //Hitstun();
        //HorizontalMovement();


        /*
		if (hurtboxhit.gotHit) {

			//print ("HIT");
			StartNewMove (hurtboxhit);
			hurtboxhit.gotHit = false;
		}
		*/


        if (status.mystatus == Status.status.juggle && !inHitstop)
			juggleFramesCount++;
		else
			juggleFramesCount = 0;
		//print (juggleFramesCount);
			
		/*

		if (status.myPhysicsStatus == Status.physicsStatus.grounded && status.myPreviousPhysicsStatus == Status.physicsStatus.airborne) {
			//print (jumpDuration);
			jumpDuration = 0;
			if (status.mystatus != Status.status.juggle && !customLanding) {
				//status.mystatus = Status.status.landing;
					StartNewMove (animations [0]);	

			}
		}



		if (status.mystatus == Status.status.juggle && status.myPhysicsStatus == Status.physicsStatus.grounded && juggleFramesCount > 10) {
			//status.mystatus = Status.status.wakeup;
			StartNewMove (animations [1]);

		}
*/


		//interrompe l'animazione corrente se diventi airborne
		if (status.myPhysicsStatus == Status.physicsStatus.airborne && status.myPreviousPhysicsStatus == Status.physicsStatus.grounded &&
			status.mystatus != Status.status.juggle) {
            if (currentMove != null && currentMove.interruptsWhenAirborne && status.mystatus != Status.status.hitstun1 &&
                status.mystatus != Status.status.wakeup)
            {
                //print("miaooo");
                currentMove.StopSequence();
                print("uuuuh miaoh?");
                animator.Play("jump rising", 0, 0f);
            }
            //se sono in hitstun e divento airborne, entro in stato di juggle
            else if (status.mystatus == Status.status.hitstun1 || status.mystatus == Status.status.wakeup || status.mystatus == Status.status.dead)
            {
                //print("bauuuu" + status.mystatus + " " + targetVelocity.x);

                if (animations[10] is Juggle)
                {
                    Juggle juggle = (Juggle)animations[10];
                    juggle.savedHorizontalVelocity = targetVelocity.x;
                    animations[10] = juggle;
                }
                //currentMove.StopSequence();
                StartNewMove(animations[10]);
                //print("juggle? " + currentMove);

            }

        }
        if (status.myPhysicsStatus == Status.physicsStatus.grounded && status.myPreviousPhysicsStatus == Status.physicsStatus.airborne)
        {
            //print(jumpDuration);
        }


        //attualeeee
        Land();


		/*
		status.RechargeStamina ();
		status.RechargeWhiteHealth ();
		status.RechargeGuardMeter ();
*/

		//Hitstun();
		//HorizontalMovement();


		//Debug.Log("land");
		//le seguenti funzioni NON implementano le mosse, ma specificano QUANDO eseguirle; nel caso di un giocatore, quando premi il pulsante

		//target attualeeeeeeeeeeeeeeeeee
		//Target();

        

        //custom script, per comportamenti non compresi tra le altre funzioni
        Custom();

        //salto
        //Jump();
        

        //scatto
        Dash();

        
        //if (status.faction == 1)
        //print("dashing? " + status.mystatus);
        //attacco
        Attack();

        

        //moveset.conditions.CheckConditions();

        //Turn off target
        //TurnOffTarget();

		//HorizontalMovement();


		//blocco
		Block();

        /*
		if (!status.invincible) 
		{
			foreach (MoveScript em in collisionHurtbox.Keys) {
				HurtboxHit hurtb = collisionHurtbox [em];
				hurtb.GetHit (em, em.move);
			}
		}

		Hitstun ();


		
		collisionHurtbox.Clear ();
		*/


        //Land();

        if (status.myPhysicsStatus == Status.physicsStatus.grounded)
            recentlyJumpedFrames = 0;

        //if (status.myPhysicsStatus == Status.physicsStatus.airborne && status.myPreviousPhysicsStatus == Status.physicsStatus.grounded)
          //  jumpBuffer = 40;
        Jump();

        
        /*
		if (!status.invincible) 
		{
			foreach (MoveScript em in collisionHurtbox.Keys) {
				HurtboxHit hurtb = collisionHurtbox [em];
				hurtb.GetHit (em, em.move);
			}
		}


		Hitstun ();

		collisionHurtbox.Clear ();
*/

        //attuale
        //HorizontalMovement();
        

        if (recentlyJumpedFrames > 0)
			recentlyJumpedFrames--;

        //Hitstun ();

        //conditions.specialMoveUsable = false;

		//ApplyBuffEffects ();

        



        if (status.currentGuardMeter <= 0) {
			status.guardCrushRemaining = status.guardCrushDuration;
			status.currentGuardMeter = status.maxGuardMeter;

		}
			
		status.GuardCrush ();

        animator.SetFloat(vspeedIndex, velocity.y);


        //animatorUpdateMode = AnimatorUpdateMode.AnimatePhysics;
        //animator.updateMode = animatorUpdateMode;

        //currentTimeScale = 1;

        if (currentMove != null) { //se ha già iniziato una sequenza durante questo frame, allora skippa; altrimenti, esegui la sequence
			if (!skipExecution)
            {
                //if (status.faction == 0)
				  //  print ("execute " + currentMove);
				currentMove.ExecuteSequence ();

			}
			//else
			//	skipExecution = false;
		}

        skipExecution = false;

        //provvisorio, test
        /*
        if (enemySpecializer.isAI)
        {
            if (status.mystatus == Status.status.attacking)
                if (currentMove != null && currentMove.move != null && currentMove.move.hitboxes.Length > 0 &&
                    seq < currentMove.move.hitboxes[currentMove.firstActualHitboxIndex].startup - 15)
                {
                    currentMove.canFlip = true;
                }
        }
        */





        if (velocity.y > 0 && climbedPlatform == null && !forcedGrounded)
        {
            //print("airbonre????");
            status.myPhysicsStatus = Status.physicsStatus.airborne;

        }

        
        


        //movimento 	orizzontale
        HorizontalMovement();
        


 
        
        if (enemySpecializer.isAI && targetList.aiMovements != null)
        {
            if (status.mystatus != Status.status.hitstun1 && status.mystatus != Status.status.hitstun2 && status.mystatus != Status.status.blockstun1
                    && status.mystatus != Status.status.guardBreak && status.mystatus != Status.status.juggle)
            {
                if (targetVelocity.x < 0 && targetList.aiMovements.distanceFromLeftEdge < 1)
                {
                    targetVelocity.x = 0;
                }
                else if (targetVelocity.x > 0 && targetList.aiMovements.distanceFromRightEdge < 1)
                {
                    targetVelocity.x = 0;
                }
            }
        }

        //Debug.Log("FLIP?");

        //Debug.Log (seq);

        //attualeeeeee
        /*
        if (enemySpecializer != null && enemySpecializer.isAI)
        {
            InputCheck();
        }
        */
        if (enemySpecializer.inputRecorder != null)
        {

            enemySpecializer.inputRecorder.SetBuffers();
            //return;

        }
        Target();

        //print ("previous: " + status.myPreviousStatus + " current: " + status.mystatus);

        if (status.currentHealth > 0)
        {
            status.RechargeStamina();
            status.RechargeRedHealth();
            status.RechargeStagger();
            status.HeatGaugeDecay();
            //status.RedHealthDecay();
            status.RechargeGuardMeter();
        }


        if (status.myPreviousStatus != Status.status.neutral && status.mystatus == Status.status.neutral)
        {
            //print("maybe? MAYBE?!");
            if (!status.canRecharge)
            {
                //StartCoroutine(status.WaitBeforeRecharge());
                //print("RECHARGE");
            }
		}

        //print (status.mystatus);

        



       


		//Debug.Log (status.mystatus);

		//if (currentMove == null)
		//	animator.SetTrigger ("neutraltrigger");


		//Debug.Log(seq);
		//Debug.Log (animator.parameters);
		InputBufferDecrease();

		ResetInputs ();

        //print(animator.GetLayerWeight(armsLayerIndex));

        variantLayerWeight += variantAnimatorLayerWeightGainCurrent;
        variantLayerWeight -= variantAnimatorLayerWeightLossCurrent;

        if (variantLayerWeight < 0)
            variantLayerWeight = 0;
        else if (variantLayerWeight > 1)
            variantLayerWeight = 1;

        animator.SetLayerWeight(variantLayerIndex, variantLayerWeight);

        if (shakerAnimator != null)
        {
            if (shakerWeight == 0)
                shakerWeight = shakerAnimator.GetLayerWeight(1);
           
            //if (hitstop > 0 && shaking) 
            if (shaking && status.currentHealth > 0)
            {
                if (shakerWeight <= 0)
                {
                    shakerAnimator.SetLayerWeight(1, 0.35f);
                    
                }
                
            }
            else
                shakerAnimator.SetLayerWeight(1, 0f);

            //shakerWeight = shakerAnimator.GetLayerWeight(1);
            
            if (shakerWeight > 0)
            {
                if (shakerWeight <= 0.35f)
                    shakerWeight -= 0.025f;
                else
                    shakerWeight -= 0.15f;
                if (shakerWeight < 0)
                    shakerWeight = 0;
                
                shakerAnimator.SetLayerWeight(1, shakerWeight);
                if (shakerWeight == 0)
                    shaking = false;
            }
            

        }
        

        if (status.faction == 0)
        {
            if (status.myPhysicsStatus == Status.physicsStatus.airborne && jumpDuration == 10)
            {
                Debug.DrawRay(hurtboxes[0].hurtbox.bounds.min, Vector2.right, Color.yellow, 5);
            }
        }


        if (moveset != null && moveset.glowAnimationSetters.Length > 0)
        {
            //if ((float)status.currentHeatAmount / (float)status.maxHeatGauge < 0.25f)
            if (status.currentHeatAmount < status.heatGaugeThreshold)
            {
                for (int i = 0; i < moveset.glowAnimationSetters.Length; i++) {
                    moveset.glowAnimationSetters[i].SetLowHeat();

                }
                
            }
            else
            {
                for (int i = 0; i < moveset.glowAnimationSetters.Length; i++)
                {
                    moveset.glowAnimationSetters[i].SetHighHeat();

                }
            }
        }
        


        if (animatorRetriever.eyesFlames != null)
        {

            animatorRetriever.eyesFlames.SetLayerWeight(eyesFlamesLayerIndex, (float)(status.heatLevel) / 4);
        }

        if (recoveryCounter > 0)
            recoveryCounter--;

        /*
        if (velocity.y > 0 && climbedPlatform == null)
        {
            //print("airbonre????");
            status.myPhysicsStatus = Status.physicsStatus.airborne;

        }
        */

        

        
        status.myPreviousStatus = status.mystatus;
        status.myPreviousPhysicsStatus = status.myPhysicsStatus;

        if ((status.mystatus == Status.status.neutral || status.mystatus == Status.status.dead)
            && status.myPhysicsStatus == Status.physicsStatus.grounded)
        {
            lastSerializablePosition = transform.position;
            lastSerializableFacing = facingRight;
        }

        /*
        if ((status.faction != 0 || (enemySpecializer != null && enemySpecializer.isAI)) && enemyCanvas != null && enemySpecializer.healthBarPosition != null)
        {
            enemyCanvas.transform.position = enemySpecializer.healthBarPosition.position;
        }
        */



    }



    public void Flip() {
		
		facingRight = !facingRight;
		if (currentMove != null && currentMove.move != null) {
			currentMove.move.facingRight = !currentMove.move.facingRight;
            currentMove.SyncFrameDataFacing();
		}
        
		Vector3 myscale = gameObject.transform.localScale;
		myscale.x *= -1;
		gameObject.transform.localScale = myscale;

        
        
        //framesSpentWalking = 0;

	}

    void Land ()
    {

        if (status.myPhysicsStatus == Status.physicsStatus.grounded && status.myPreviousPhysicsStatus == Status.physicsStatus.airborne && !inHitstop)
        {

            RestoreOriginalGravity();

            //if (status.faction == 0)
              //  print ("land " + jumpstartup);

            //7 * 39 + 5 * 2 + 6 * 2 = 295
            //6 * 50 = 300
            jumpingState = false;
            //print ("verticalspeed: " + velocity.y);
            //jumpstartup = 0;
            jumpDuration = 0;
            /*
            if (status.mystatus == Status.status.dead)
            {

                print("wake up deeeeed " + seq);
                StartNewMove(animations[10].relatedMoves[3]);

            }
            else
            */
            {
            
                if (status.mystatus != Status.status.juggle && (currentMove == null || (currentMove != null && currentMove.interruptsWhenLands && currentMove.customLanding == null)))
                {               //if (!customLanding)
                                //status.mystatus = Status.status.landing;
                    jumpstartup = 0;

                    StartNewMove(animations[0]);



                }
                else if (status.mystatus != Status.status.juggle && currentMove != null && currentMove.customLanding == null)
                {
                    jumpstartup = 0;

                }

                else if (status.mystatus != Status.status.juggle && currentMove != null && currentMove.customLanding != null)
                {
                    //print("customlanding");
                    StartNewMove(currentMove.customLanding);
                    jumpstartup = 0;
                }

                else if (status.mystatus == Status.status.juggle && currentMove == animations[10])
                {
                    Juggle juggleMove;
                    if (juggleMove = (Juggle)animations[10])
                    {
                            if (juggleMove.wakeupType > juggleMove.relatedMoves.Length - 1 || juggleMove.wakeupType == 0)
                                StartNewMove(juggleMove.relatedMoves[0]);
                            else if (juggleMove.wakeupType == 1)
                            {
                                StartNewMove(juggleMove.relatedMoves[1]);
                            }
                            else if (juggleMove.wakeupType == 2)
                            {
                                StartNewMove(juggleMove.relatedMoves[2]);
                            }
                    }
                    else
                    {
                        StartNewMove(animations[1]);
                    }
                }
            }
        }
    }

	protected virtual void HorizontalMovement () {

	}

	protected virtual void Hitstun () {

	}

	protected virtual void Jump () {

	}

	protected virtual void Attack () {

	}

	public void StartNewMove (MoveScript newMove) {

		//ResetProjectileOffenders ();
        //if (status.faction == 1)
		  //  print ("newmove: " + newMove + " " + seq);
		//if (currentMove == null || !newMove.skipExecution)
		{

            if (currentMove != null)
            {
                //print ("current: " + currentMove + ", nuova mossa: " + newMove);

                //ResetProjectileOffenders ();

                previousMove = currentMove;
                currentMove.StopSequence();
                //print ("current: " + currentMove + ", nuova mossa: " + newMove);
            }
            else
                previousMove = null;
			//animator.SetBool ("neutral", true);
			currentMove = newMove;
            //print (newMove);
            //print (currentMove);
            //FrameData fd = currentMove.gameObject.GetComponent<FrameData> ();
            
            
            //FrameData fd = currentMove.move;
			//if (fd != null)
			//	fd.facingRight = facingRight;
			seq = 0;
            ///if (currentMove.slowMotionWindows.Length == 1 && currentMove.slowMotionWindows[0].duration > 0)
               // currentMove.SetTimeFreeze();
            //else
            
            {
                currentMove.StartSequence();
                //currentMove.ExecuteSequence();
            }
			
			skipExecution = true;
		} 

	}
	/*
	public void ManageHurtboxCollisions () {
		for (int i = 0; i < hurtboxes.Length; i++) {
			if (hurtboxes [i].gotHit) {
				hurtboxes [i].GetHit ();
				for (int j = i + 1; j < hurtboxes.Length; j++) {
					hurtboxes [j].gotHit = false;
				}
				break;
			} else
				continue;
		}
	}
*/
    protected void ClearHurtboxes ()
    {

        //ATTENZIONE, DA TESTARE
        /*
        hurtboxesHitList = new List<HurtboxHit>(collisionHurtbox.Values);
        for (int i = 0; i < hurtboxesHitList.Count; i++)
        {


            hurtboxesHitList[i].hit = false;
            hurtboxesHitList[i].block = false;
            hurtboxesHitList[i].juggle = false;
            hurtboxesHitList[i].parry = false;
            hurtboxesHitList[i].gotHit = false;


        }
        lastCollisionHurtbox.move.frameData = null;
        lastCollisionHurtbox.move.gotHit = false;
        lastCollisionHurtbox.hurtbox = null;
        hurtboxesHitList.Clear();
        */
        foreach (HurtboxHit hbHit in collisionHurtbox.Values)
        {
            hbHit.hit = false;
            hbHit.block = false;
            hbHit.juggle = false;
            hbHit.parry = false;
            hbHit.gotHit = false;
        }
        lastCollisionHurtbox.move.frameData = null;
        lastCollisionHurtbox.move.gotHit = false;
        lastCollisionHurtbox.hurtbox = null;
        //hurtboxesHitList.Clear();
    }

	public void LoadWeapon (int preAwakeIndex) {
        //se preawakeindex = -1, fa tutto in un singolo frame

        //3-6


        if (preAwakeIndex == -1 || preAwakeIndex == 3)
        {
            //Debug.Log (weapon);
            //GameObject mymoves = transform.Find ("Moves").gameObject;
            //moves = weapon.GetComponent<MoveSet> ().moves.Concat(mymoves.GetComponent<MoveSet>().moves).ToArray();
            weaponSwitcher.controller = this;

            moveset = weaponSwitcher.weapons[weaponSwitcher.i];
            moves = moveset.moves;// weapon.GetComponent<MoveSet> ().moves;
            moveset.conditions.controller = (Player)this;
            moveset.conditions.status = status;
            movesetTransform = moveset.transform;
            moveset.controller = this;

            //Debug.Log (moves);


            //conditions = moveset.GetComponent<ActivationConditions> ();
            conditions = moveset.conditions;

            if (weaponSwitcher.wallHitSpark != null)
            {
                weaponSwitcher.wallHitSpark.moveset = moveset;
            }

            if (pm.player1 == this || pm.player2 == this)
            {
                activeWeaponParameter = moveset.weaponAnimatorParameter;
                //print("activeParam: " + activeWeaponParameter);

                animator.SetBool(activeWeaponParameter, true);

                if (specialMoves != null)
                {
                    specialMovesTransform = specialMoves.transform;
                    specialMoves.conditions.LoadMoveInfo();

                    for (int i = 0; i < specialMoves.moves.Length; i++)
                    {
                        specialMoves.moves[i].Initialize((Player)this, specialMovesTransform, specialMoves);
                        if (specialMoves.moves[i].relatedMoves.Length > 0)
                        {
                            for (int k = 0; k < specialMoves.moves[i].relatedMoves.Length; k++)
                            {
                                if (specialMoves.moves[i].relatedMoves[k] != null)
                                    specialMoves.moves[i].relatedMoves[k].Initialize((Player)this, specialMovesTransform, specialMoves);
                            }
                        }
                    }
                }
            }

            if (activeWeaponSprite != null)
                activeWeaponSprite.SetActive(false);
            if (moveset.weaponSprite != null)
                activeWeaponSprite = moveset.weaponSprite;
            if (activeWeaponSprite != null)
                activeWeaponSprite.SetActive(true);

            if (moveset.glowAnimationSetters.Length > 0)
            {
                for (int i = 0; i < moveset.glowAnimationSetters.Length; i++)
                {
                    if (status.currentStance == Status.Stances.slow)
                        moveset.glowAnimationSetters[i].SetStance(WeaponGlowAnimationSetter.Stance.power);
                    else
                        moveset.glowAnimationSetters[i].SetStance(WeaponGlowAnimationSetter.Stance.quick);

                }
            }
           
        }

        if (preAwakeIndex == -1 || preAwakeIndex == 4)
        {
            LoadMoves();
        }

        if (preAwakeIndex == -1 || preAwakeIndex == 5)
        {
            for (int i = 0; i < moves.Length; i++)
            {
                moves[i].Initialize((Player)this, movesetTransform, moveset);
                if (moves[i].relatedMoves.Length > 0)
                {
                    for (int k = 0; k < moves[i].relatedMoves.Length; k++)
                    {
                        if (moves[i].relatedMoves[k] != null)
                            moves[i].relatedMoves[k].Initialize((Player)this, movesetTransform, moveset);
                    }
                }

                //print (i);
            }
        }

        if (preAwakeIndex == -1 || preAwakeIndex == 6)
        {
            conditions.LoadMoveInfo();
        }

		//print ("Weapon Loaded");
		//Debug.Log (moveset);

	}

	public void LoadMoves () {
        //GameObject mymoves = transform.Find ("Animations").gameObject;
        //GameObject mymoves = transform.Find ("Moves").gameObject;
        //moves = weapon.GetComponent<MoveSet> ().moves.Concat(mymoves.GetComponent<MoveSet>().moves).ToArray();

        //animations = animationsObject.GetComponent<MoveSet> ().moves;
        animations = animationset.moves;
        Transform animationsTransform = animationsObject.transform;

        //print("animations: " + gameObject + " " + animations.Length);

		for (int i = 0; i < animations.Length; i++) {
            if (animations[i] == null)
                continue;
			animations [i].Initialize ((Player)this, animationsTransform, animationset);
			if (animations [i].relatedMoves.Length > 0) {
				for (int k = 0; k < animations [i].relatedMoves.Length; k++) {
					if (animations [i].relatedMoves [k] != null)
						animations [i].relatedMoves [k].Initialize ((Player)this, animationsTransform, animationset);
				}
			}
		}
        int hb0Index = LayerMask.NameToLayer("Hitbox0");
        int hb1Index = LayerMask.NameToLayer("Hitbox1");
        int hb2Index = LayerMask.NameToLayer("Hitbox2");

        for (int i = 0; i < hurtboxes.Length; i++) {
            hurtboxes[i].hb0Index = hb0Index;
            hurtboxes[i].hb1Index = hb1Index;
            hurtboxes[i].hb2Index = hb2Index;
            hurtboxes[i].controller = (Player)this;
            hurtboxes[i].Initialize();
        }	

        if (getGrabbed != null)
        {
            getGrabbed.Initialize((Player)this, animationsTransform, animationset);
        }
    }

	public void ChangeParameter (string param, bool value) {
		animParameter = param;
		//animator.SetBool (animParameter, value);
	}

   

	public void ChangeParameter (string param) {
		animParameter = param;
		animator.SetTrigger (animParameter);
	}

	protected virtual void Attack2 () {

	}

	protected virtual void Kick () {

	}

	protected virtual void Block () {

	}

	protected virtual void Target () {

	}

	protected virtual void TurnOffTarget () {

	}

	protected virtual void Custom () {

	}

	protected virtual void Dash () {

	}

	protected virtual void Death () {

	}

    



}
