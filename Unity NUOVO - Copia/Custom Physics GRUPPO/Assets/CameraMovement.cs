﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraMovement : MonoBehaviour {

    public static CameraMovement Instance { get; private set; }
    
    public Transform FollowedObject;
    private Camera camera;
	private float obx, oby, focus, TargetFocus, prevx, ShakePotency, ShakePotencyDecreaseRate;
    private float maxSpeed = 100;
	private bool shakeDirectionX, shakeDirectionY, shakeDirectionZ;
	private int shakeTimer, shakeFrequency;
	private Vector2 ObjPos;
    private Vector3 MyPos;
    private Transform tr;
    
    public enum DirectionCalculation {positionBased, xScaleBased, manual };
    [Tooltip("Position based: the camera faces the last direction the followed GameObject moved to. " +
        "X-Scale based: the direction is determined by the transform.localScale.x of the followed GameObject. " +
        "Manual: the direction is not computed automatically; you have to set it by calling the SetDirection() method; " +
        "as an argument, use a positive integer to face right, negative to face left.")]
    public DirectionCalculation directionCalculation = DirectionCalculation.positionBased;
    [Tooltip("The default zoom level of the camera")]
    [Range(0f, 180f)]
    public float zoomLevel = 50f;
    public Vector2 offset = new Vector2(0, 0.3f);
    [Tooltip("Movement speed")]
    public Vector2 MoveSpeed = new Vector2(40, 50);
    [Tooltip("How fast it changes the offset when the followed object changes direction")]
    [Range(0f, 100f)]
    public float FocusShiftSpeed = 50;
    [Tooltip("How much it changes the offset when the followed object changes direction. Set it to 0 to cancel any offset change.")]
    [Range(0f, 5f)]
    public float FocusDistance = 1f;

    [Tooltip("Maximum possible priority for camera manipulators.")]
    public int maxManipulatorPriority = 5;

    public struct ManipulatorData {
        public CameraManipulator manipulator;
        public Vector2 offset, center;
        public float zoom;
        public float timer, reachTime;
        public bool removed;
        public float slowDownThreshold;
    }
    public List<ManipulatorData>[] manipulators;
    private Vector2 currentOb;
    private float targetZoom;
    private Animator animator;
    private int animatorLayerShakeX, animatorLayerShakeY;
    private Vector2 addedOffset, currentAddedOffset;

    //variabili anti-garbage
    private float velx, vely, velz;
    private Vector3 quantoManca;
    private Vector2 factor, totalOffset, totalCenter;
    private float filtercurrent, filterbelow, totalZoom, timerFactor;
    private float soglia, roundTimer;
    
    

	// Use this for initialization
	public void InitializeCameraMovement () {
        //Time.timeScale = 0.3f;
        Instance = this;

        tr = transform;
        ObjPos.x = FollowedObject.position.x;
        ObjPos.y = FollowedObject.position.y;
		tr.position = new Vector3(FollowedObject.position.x, FollowedObject.position.y, -10);
		MyPos = new Vector3 (tr.position.x, tr.position.y, -10f);
		
		obx = 0;
		oby = 0;
        prevx = 0;
		focus = 0;
		ShakePotency = 0f;
		shakeTimer = 0;
		TargetFocus = 1.8f;

        camera = GetComponentInChildren<Camera>();

		if (MoveSpeed.x > maxSpeed)
			MoveSpeed.x = maxSpeed;
		if (MoveSpeed.y > maxSpeed)
			MoveSpeed.y = maxSpeed;
		if (FocusShiftSpeed > maxSpeed)
			FocusShiftSpeed = maxSpeed;

        manipulators = new List<ManipulatorData>[maxManipulatorPriority+1];
        for (int i = 0; i < maxManipulatorPriority + 1; i++)
        {
            manipulators[i] = new List<ManipulatorData>();
            //print("manipulators??");
        }

        animator = GetComponentInChildren<Animator>();
        animator.speed = 0.55f;
        animatorLayerShakeX = animator.GetLayerIndex("Shake X");
        animatorLayerShakeY = animator.GetLayerIndex("Shake Y");

        quantoManca = new Vector3();
        factor = new Vector2();
    }

	void Update(){
        if (Time.timeScale < 1)
            UpdateCamera();
	
	}
    void FixedUpdate()
    {
        if (Time.timeScale == 1)
            UpdateCamera();

    }

    public void UpdateCamera () {

        if (FollowedObject == null)
            return;

        //estrae le coordinate dell'oggetto seguito
        ObjPos.x = FollowedObject.position.x;
        ObjPos.y = FollowedObject.position.y;
        oby = ObjPos.y + offset.y;
		obx = ObjPos.x + offset.x;

        //determina la direzione della telecamera

        switch (directionCalculation) {
            case DirectionCalculation.positionBased:
                if (ObjPos.x != prevx)
                    TargetFocus = Mathf.Sign(ObjPos.x - prevx);
                else
                    TargetFocus = Mathf.Sign(TargetFocus);
                break;
            case DirectionCalculation.xScaleBased:
                if (FollowedObject.transform.localScale.x > 0)
                    TargetFocus = 1;
                else
                    TargetFocus = -1;
                break;
        }
        
        focus += (TargetFocus * FocusDistance - focus) / (maxSpeed + 1 - FocusShiftSpeed); //70-v 40
        obx = obx + focus;


        //gestisci i camera manipulator

        /*
                
                 * //pseudocodice
                 * 
                 * filtercurrent=100;
                 * filterbelow=100
                 * for(i=maxpriority; i>0; i--)
                 *      filtercurrent=filterbelow
                 *      filterbelow=100 //NO!! Il filtro di una priorità deve ereditarsi a TUTTE quelle inferiori
                 *      for(j=0; j<count(i); j++)
                 *          t += offset[j] * filtercurrent/100;
                 *          filterbelow -= (timer[j]*timer[j])/100
                 *          
                 *          if filterbelow<0
                 *              filterbelow=0
                 * 
                 * */
        totalOffset.x = 0;
        totalOffset.y = 0;
        totalCenter.x = 0;
        totalCenter.y = 0;
        totalZoom = 0;
        filtercurrent = 1f;
        filterbelow = 1f;

        //scorri le priorità, partendo dalla più alta
        for (int i = maxManipulatorPriority; i >= 0; i--) {

            filtercurrent = filterbelow;
            

            //scorri i manipulatorData della i-esima priorità
            for (int j = 0; j < manipulators[i].Count; j++) {
                
                //calcola il timerFactor; indica a che punto siamo arrivati nel tragitto verso la manipolazione
                timerFactor = manipulators[i][j].timer / manipulators[i][j].reachTime;
                
                //aggiorna tutti i dati del manipulatorData, prendendoli dal manipulator a cui si sta riferendo
                ManipulatorData md = manipulators[i][j];
                if (manipulators[i][j].manipulator != null && !manipulators[i][j].removed)
                {
                    md.offset.x = manipulators[i][j].manipulator.offset.x;
                    md.offset.y = manipulators[i][j].manipulator.offset.y;

                    if (manipulators[i][j].manipulator.center != null)
                    {
                        md.center.x = (manipulators[i][j].manipulator.center.position.x - obx) * manipulators[i][j].manipulator.strength.x;
                        md.center.y = (manipulators[i][j].manipulator.center.position.y - oby) * manipulators[i][j].manipulator.strength.y;
                    }

                    md.zoom = manipulators[i][j].manipulator.zoomOffset;

                    manipulators[i][j] = md;
                }
               
                //aggiunge ai totali le varie manipolazioni
                totalOffset.x += manipulators[i][j].offset.x * timerFactor * filtercurrent;
                totalOffset.y += manipulators[i][j].offset.y * timerFactor * filtercurrent;
                totalCenter.x += manipulators[i][j].center.x * timerFactor * filtercurrent;
                totalCenter.y += manipulators[i][j].center.y * timerFactor * filtercurrent;
                totalZoom += manipulators[i][j].zoom * timerFactor * filtercurrent;

                //le priorità inferiori non si devono vedere se il timerFactor di questa priorità è alto
                filterbelow -= timerFactor;
                if (filterbelow < 0)
                    filterbelow = 0;

                if (md.manipulator != null)
                {
                    Vector3 p = md.manipulator.transform.position;
                    Debug.DrawLine(new Vector3(p.x, p.y), new Vector3(p.x + totalZoom / 90, p.y));
                    Debug.DrawLine(new Vector3(p.x, p.y + 0.5f), new Vector3(p.x + filtercurrent, p.y + 0.5f));
                    Debug.DrawLine(new Vector3(p.x, p.y + 0.6f), new Vector3(p.x + timerFactor, p.y + 0.6f));
                }

                //aumenta o diminuisci i timer
                if (!md.removed && md.manipulator != null)
                {
                    if (md.timer < md.reachTime)
                    {
                        if (timerFactor < md.slowDownThreshold)
                        {
                            roundTimer = (int)md.timer;
                            md.timer = roundTimer;
                            md.timer = md.timer + 1f;
                        }
                        else
                        {
                            //nell'ultima parte di tragitto, aumenta più lentamente (da slowDownThreshold in poi)
                            soglia = md.reachTime * md.slowDownThreshold;
                            //float prevTimer = md.timer;
                            md.timer = md.timer + 1f - (md.timer - soglia) / (md.reachTime - soglia);
                            
                        }
                            
                        manipulators[i][j] = md;
                    }
                }
                else {
                    if (md.timer > 0)
                    {
                        if (timerFactor > 1f - md.slowDownThreshold)
                        {
                            roundTimer = (int)md.timer;
                            md.timer = roundTimer;
                            md.timer--;
                            if (md.timer < 0)
                                md.timer = 0;
                        }
                        else
                        {
                            soglia = md.reachTime * (1 - md.slowDownThreshold);
                            md.timer -= 1f - (soglia - md.timer) / soglia;
                            if (md.timer < 0.1f)
                                md.timer = 0;
                        }
                        manipulators[i][j] = md;
                    }
                    else
                    {
                        //se è a 0, rimuovi il manipolatore
                        manipulators[i].RemoveAt(j);
                        j--;
                    }
                }
               
            }

        }

        //regolazione telecamera da giocatore
        currentAddedOffset.x += (addedOffset.x - currentAddedOffset.x) / 10f;
        currentAddedOffset.y += (addedOffset.y - currentAddedOffset.y) / 10f;
        addedOffset.x = 0;
        addedOffset.y = 0;

        //l'obiettivo corrente sarà obx (dove si trova l'oggetto seguito + focus) + il totale delle manipolazioni
        currentOb.x = obx + totalOffset.x + totalCenter.x + currentAddedOffset.x;
        currentOb.y = oby + totalOffset.y + totalCenter.y + currentAddedOffset.y;
        targetZoom = zoomLevel + totalZoom;



        Debug.DrawLine(new Vector3(obx, oby, 0), new Vector3(obx, oby + 0.1f, 0), Color.yellow);
        Debug.DrawLine(new Vector3(currentOb.x, currentOb.y, 0), new Vector3(currentOb.x, currentOb.y + 0.1f, 0));

        //imposta la velocita'  
        if (targetZoom < 1)
            targetZoom = 1;
        if (targetZoom > 180)
            targetZoom = 180;
        quantoManca.x = currentOb.x - MyPos.x;
        quantoManca.y = currentOb.y - MyPos.y;
        quantoManca.z = targetZoom - camera.fieldOfView;
        factor.x = MoveSpeed.x / maxSpeed;
        factor.y = MoveSpeed.y / maxSpeed;

        
        velx = quantoManca.x * Mathf.Pow(factor.x, 3);
        vely = quantoManca.y * Mathf.Pow(factor.y, 3);
        velz = quantoManca.z * Mathf.Pow(zoomLevel / 180f, 3);
        //velz = quantoManca.z;
       


        //screen shake
        if (shakeTimer >= 0)
        {

            float shakeXWeight = animator.GetLayerWeight(animatorLayerShakeX);
            if (shakeXWeight > ShakePotencyDecreaseRate)
                shakeXWeight -= ShakePotencyDecreaseRate;
            else
                shakeXWeight = 0;
            float shakeYWeight = animator.GetLayerWeight(animatorLayerShakeY);
            if (shakeYWeight > ShakePotencyDecreaseRate)
                shakeYWeight -= ShakePotencyDecreaseRate;
            else
                shakeYWeight = 0;
            animator.SetLayerWeight(animatorLayerShakeX, shakeXWeight);
            animator.SetLayerWeight(animatorLayerShakeY, shakeYWeight);

            ShakePotency -= 1f / 15f;
            
            shakeTimer--;
        }
        else {
            animator.SetLayerWeight(animatorLayerShakeX, 0);
            animator.SetLayerWeight(animatorLayerShakeY, 0);
        }



        /*if (shakeTimer > 0) {
			if (shakeFrequency > 18)
				shakeFrequency = 18;

            if (shakeDirectionY)
                ShakeY = ShakePotency / 100 * Mathf.Sin(shakeTimer * Mathf.PI / (20 - shakeFrequency));
            if(shakeDirectionX)
                ShakeX = ShakePotency / 100 * Mathf.Sin(shakeTimer * Mathf.PI / (20 - shakeFrequency));
            if (shakeDirectionZ)
                ShakeZ = ShakePotency / 10 * Mathf.Sin(shakeTimer * Mathf.PI / (20 - shakeFrequency));

            ShakePotency -= ShakePotencyDecreaseRate;
			shakeTimer--;

			//forza la velocita' alla fine dello shake onde evitare che la telecamera si sposti lentamente 
			//verso la posizione normale dopo ogni shake
			if (shakeTimer == 0) {
				ShakeX = 0;
				ShakeY = 0;
                ShakeZ = 0;
			}
		}
        */
        MyPos.x += velx;
        MyPos.y += vely;
        //la mia posizione sara' la somma di MyPos e dell'eventuale screen shake
        //transform.position = new Vector3 (MyPos.x + ShakeX, MyPos.y + ShakeY, -10);
        tr.position = MyPos;
        //camera.fieldOfView += velz + ShakeZ;
        camera.fieldOfView += velz;
        

		//salva la x precedente, in modo da poter determinare la direzione in cui sta andando l'oggetto seguito
		prevx = ObjPos.x;
			
	}

    public void SetDirection(int direction) {
        if (direction >= 0)
            TargetFocus = 1;
        else
            TargetFocus = -1;
        
    }

    /*public void Shake (int time, float potency, int frequency){
		Shake (time, potency, frequency, false, true, false);
	}

	public void Shake (int time, float potency, int frequency, bool xBased, bool yBased, bool zBased){
		shakeTimer = time;
		ShakePotency = potency;
		shakeFrequency = frequency;
		ShakePotencyDecreaseRate = ShakePotency / (shakeTimer + 1);
		ShakeX = 0f;
		ShakeY = 0f;
        ShakeZ = 0f;
		shakeDirectionX = xBased;
        shakeDirectionY = yBased;
        shakeDirectionZ = zBased;
	}
    */
    public void Shake(int time, float potency, bool xBased, bool yBased, bool zBased) {
        shakeTimer = time;
        ShakePotency = potency;
        ShakePotencyDecreaseRate = ShakePotency / (shakeTimer + 1);
        if (xBased) {
            animator.SetLayerWeight(animatorLayerShakeX, potency);
        }
        if (yBased)
        {
            animator.SetLayerWeight(animatorLayerShakeY, potency);
        }
    }
    public void Shake(int time, float potency)
    {
        Shake(time, potency, false, true, false);
    }

    public void SetAddedOffset(float addedX, float addedY, bool additive = true)
    {
        if (additive)
        {
            addedOffset.x = addedX;
            addedOffset.y = addedY;
        }
        else
        {
            addedOffset.x = addedX - offset.x;
            addedOffset.y = addedY - offset.y;
        }
    }

    public void AddManipulator(CameraManipulator cm) {
        int p = cm.priority;

        if (p < 0 || p > maxManipulatorPriority)
            return;

        //controlla che non ci sia già
        for (int i = 0; i < maxManipulatorPriority + 1; i++)
        {
            for (int j = 0; j < manipulators[i].Count; j++)
            {
                if (manipulators[i][j].manipulator == cm)
                {
                    ManipulatorData md = manipulators[i][j];
                    md.removed = false;
                    float timerFactor = (float)md.timer / (float)md.reachTime;
                    md.reachTime = md.manipulator.reachTime;
                    md.timer = (int)((float)md.reachTime * timerFactor);

                    manipulators[i][j] = md;
                    return;
                }
            }
        }

       

        ManipulatorData m = new ManipulatorData
        {
            manipulator = cm,
            timer = 0,
            reachTime = cm.reachTime,
            slowDownThreshold = cm.slowDownThreshold
        };
        manipulators[p].Add(m);
    }


    public void RemoveManipulator(CameraManipulator cm)
    {
        for (int i = 0; i < maxManipulatorPriority + 1; i++) {
            for (int j = 0; j < manipulators[i].Count; j++) {
                if (manipulators[i][j].manipulator == cm)
                {
                    ManipulatorData md = manipulators[i][j];
                    md.removed = true;
                    float timerFactor = md.timer / md.reachTime;
                    md.reachTime = md.manipulator.releaseTime;
                    md.timer = md.reachTime * timerFactor;
                    manipulators[i][j] = md;
                    break;
                }
            }
        }
       

    }
}
