﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class QuantitySelector : MonoBehaviour
{
    private int quantity, maxCap = 1;
    public MenuManager referenceMenuManager;

    public Text selectedQuantityText, itemNameText, maxQuantityText;

    // Start is called before the first frame update
    void Start()
    {
        if (referenceMenuManager == null)
            referenceMenuManager = GetComponentInParent<MenuManager>();
    }

    private void Update()
    {
        if (referenceMenuManager.IsMenuCurrentlySelected())
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
                ChangeQuantity(1);
            if (Input.GetKeyDown(KeyCode.DownArrow))
                ChangeQuantity(-1);
            if (Input.GetKeyDown(KeyCode.RightArrow))
                ChangeQuantity(+5);
            if (Input.GetKeyDown(KeyCode.LeftArrow))
                ChangeQuantity(-5);
        }
    }
    public void SetMaximumCap(int max) {
        maxCap = max;
    }
    public void ChangeQuantity(int delta) {
        SetQuantity(quantity + delta);
    }
    public void SetQuantity(int q) {
        int prevQ = quantity;
        if (prevQ == maxCap && q == maxCap + 1)
            quantity = 1;
        else if (prevQ == 1 && q == 0)
            quantity = maxCap;
        else
            quantity = q;

        if (quantity < 1)
            quantity = 1;
        if (quantity > maxCap)
            quantity = maxCap;

        UpdateDisplayText();
    }

    public int GetQuantity() {
        return quantity;
    }

    public void UpdateDisplayText() {
        if (selectedQuantityText != null)
            selectedQuantityText.text = quantity.ToString();
        if (maxQuantityText != null)
            maxQuantityText.text = maxCap.ToString();
        
    }

    public void SetItemNameText(string name) {
        if (itemNameText != null)
            itemNameText.text = name;
    }

   
    
}
