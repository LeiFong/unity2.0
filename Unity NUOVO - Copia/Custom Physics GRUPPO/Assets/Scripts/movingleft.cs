﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movingleft : MonoBehaviour {

	Transform tr;
	public float velocity;
	public Collider2D myCollider, myCollider1, myCollider2;
	//public float timeScale = 1;
	Vector2 newOffset = new Vector2();
	RaycastHit2D[] results = new RaycastHit2D[20];

	void Start () {
		tr = GetComponent<Transform>();
		myCollider = GetComponent<Collider2D>();
		//Time.timeScale = timeScale;
	}
	
	void OffsetCast () {
		newOffset = Vector2.zero;
		myCollider.offset = newOffset;
		myCollider.Cast(Vector2.zero, results);
		newOffset.y += 1;
		myCollider.offset = newOffset;
		myCollider.Cast(Vector2.zero, results);
		newOffset.y += 1;
		myCollider.offset = newOffset;
		myCollider.Cast(Vector2.zero, results);
		tr.Translate(velocity * Time.fixedDeltaTime, 0, 0);


	}

	void MultiColliderCast()
    {
		myCollider.Cast(Vector2.zero, results);
		myCollider1.Cast(Vector2.zero, results);
		myCollider2.Cast(Vector2.zero, results);
		tr.Translate(velocity * Time.fixedDeltaTime, 0, 0);


	}

    private void FixedUpdate()
    {
		OffsetCast();
		//MultiColliderCast();
    }
}
