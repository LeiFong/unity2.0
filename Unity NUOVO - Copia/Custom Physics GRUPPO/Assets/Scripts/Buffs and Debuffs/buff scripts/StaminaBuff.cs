﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaminaBuff : Buff
{
    [Range(0,100)]
    public int staminaRechargeBuffPercentage = 100; 
    public override void ApplyEffect(Controller2D victim, BuffManager.BuffProperties buffProperties)
    {
        //print("durationnnnnnnnn " + durationTimer);
        //if (victim.status.canRecharge)
            victim.status.currentStamina += victim.status.staminaChargeSpeed * staminaRechargeBuffPercentage / 100;
        //victim.conditions.specialMoveUsable = durationTimer;
        //victim.status.meterGainPercentage = 0;
    }

    public override void InitializeBuff(Controller2D victim)
    {
        //durationTimer = victim.conditions.specialMoveBuffDuration;
    }
}
