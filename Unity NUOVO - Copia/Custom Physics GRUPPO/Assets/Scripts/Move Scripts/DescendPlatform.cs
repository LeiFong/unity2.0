﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DescendPlatform : MoveScript
{
    public int moveDuration = 60;
    public Collider2D objectUnderMeAtStart;

    public override void MyStartSequence()
    {
        objectUnderMeAtStart = controller.objectUnderMe;
       

    }


    public override void MyExecuteSequence()
    {
        //print("seq: " + controller.seq);
        
        controller.climbedPlatform = objectUnderMeAtStart;
        controller.descendingPlatform = true;
        if (controller.seq < 100)
        {
            //print("ignoreplat");
            controller.collisions.isIgnoringPlatforms = 1;
            controller.isAffectedByGravity = false;
        }
        else
            controller.isAffectedByGravity = true;
        status.mystatus = Status.status.stuckinanimation;


        if (controller.seq >= moveDuration)
            itsTimeToStopTheSequence = true;
        else
            controller.seq++;
    }


    public override void SetStatus()
    {
        status.mystatus = Status.status.stuckinanimation;
    }

    public override void MyStopSequence()
    {
        controller.collisions.isIgnoringPlatforms = 0;
        controller.climbedPlatform = null;
        controller.isAffectedByGravity = true;
        //interruptsWhenLands = false;
    }
}
