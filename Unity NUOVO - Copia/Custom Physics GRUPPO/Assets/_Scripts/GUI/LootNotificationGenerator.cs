﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootNotificationGenerator : GeneratorFromPool
{
    
    public void Display(string s, Color color, SpriteRenderer sprite) {
        for (int i = 0; i < components.Count; i++)
        {
            LootNotificationBehavior notif = (LootNotificationBehavior)components[i];
            if (i != activeIndex)
                notif.PushUp();
        }
        LootNotificationBehavior notification = (LootNotificationBehavior)Generate();
        notification.Display(s, sprite, color);

        
    }
    /*
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M)) {
            string s = "";
            int wordnum = (int)Random.Range(1, 4);
            for (int i = 0; i < wordnum; i++) {
                if (i != 0)
                    s += " ";
                int wordlength = (int)Random.Range(1, 20);
                for (int j = 0; j < wordlength; j++) {

                    int rand = (int)Random.Range(0f, 25f);
                    char c = 'A';
                    c += (char)rand;
                    s += c;
                }
            }
            Display(s, Color.white, null);
        }
    }
    */
}
