﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvoidCharacters : MonoBehaviour {

	public Collider2D characterAvoider;

	public Controller physics;
	protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];


	void OnEnable () {
		physics = GetComponent<Controller> ();
		characterAvoider = GetComponent<Collider2D> ();
	}

	public void ExitCharacter () {

		int count = characterAvoider.Cast (Vector2.zero, hitBuffer);

		for (int i = 0; i < count; i++) {
	

			if (!physics.avoidsCharacters && hitBuffer[i].collider.gameObject.layer == LayerMask.NameToLayer ("Character")) {
				physics.environmentVelocity.x = Mathf.Sign ((characterAvoider.bounds.center.x - hitBuffer[i].collider.bounds.center.x)) * 4;

			}
		}
	}
}
