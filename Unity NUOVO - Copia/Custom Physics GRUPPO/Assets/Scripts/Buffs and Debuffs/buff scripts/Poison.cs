﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poison : Buff {

	public int damageValue;
	public int tickPeriod; //ogni quanto ticca il danno

	public override void ApplyEffect (Controller2D victim, BuffManager.BuffProperties buffProperties) {
        //lastTick = Time.time;
        if (buffProperties.durationTimer != buffScriptableObject.buffTotalDuration && buffProperties.durationTimer % tickPeriod == 0)
        {
            victim.status.TakeDamage(damageValue * buffProperties.currentStacks, 0, 0, 0);
            //print("skidiki");
        }
		//victim.status.TakeDamage (damageValue, true);
		//print("veleno");
		}

	}
