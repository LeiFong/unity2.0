﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomOnPlayerManipulator : MonoBehaviour
{

    private PlayersManager playersManager;
    public CameraManipulator manipulator;

    // Start is called before the first frame update
    void Start()
    {
        playersManager = FindObjectOfType<PlayersManager>();
    }

    public void SetPlayerAsFollowedObject() {
        if (manipulator == null || playersManager == null || playersManager.player1 == null)
            return;
        manipulator.SetFollowedObject(playersManager.player1.transform);
    }
}
