﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class UnloadableAssetsRetriever : MonoBehaviour
{
    public List<Object> textures;

    private void OnDisable()
    {
        if (Application.isPlaying)
            return;

        textures = new List<Object>();
        foreach (Object go in Resources.FindObjectsOfTypeAll(typeof(Object)) as Object[])
        {
            
        }

    }
}
