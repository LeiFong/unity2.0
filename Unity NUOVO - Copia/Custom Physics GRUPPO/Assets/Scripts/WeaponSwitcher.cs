﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitcher : MonoBehaviour {

	public MoveSet[] weapons;
	public Controller2D controller;
	public int equippedSpecialMove = 0;
    public WallHitSpark wallHitSpark;

	public int i = 0;
	// Use this for initialization
	void Start () {

		InitializeWeaponDatabase();
	}

	public void InitializeWeaponDatabase()
	{
		for (int i = 0; i < weapons.Length; i++)
		{
			weapons[i].weaponItem.weaponIndex = i;
		}
	}
	/*
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyDown(KeyCode.P) && controller.status.faction == 0)
		{
			i = 2;
            SwitchWeapon(i);
		}
					
	}
	*/
    public void SwitchWeapon(int index)
    {
        //i++;
        i = index % weapons.Length;
        controller.animator.SetBool(controller.activeWeaponParameter, false);

        controller.LoadWeapon(-1); //-1 significa che esegue tutta la funzione in un solo frame
    }

    //VECCHIO! NON USARE
	IEnumerator ChangeWeapon () {
		i++;
		i = i % weapons.Length;
		Component[] comp = controller.gameObject.GetComponentsInChildren<Transform>();

		int j;
		for (j = 0; j < comp.Length; j++) {
			if (comp[j].name.StartsWith("Weapon"))
				break;
			else continue;
		}
		Destroy(comp[j].gameObject);
		Instantiate (weapons [i], controller.transform);

		yield return null;
		//controller.LoadWeapon ();
	}
}
