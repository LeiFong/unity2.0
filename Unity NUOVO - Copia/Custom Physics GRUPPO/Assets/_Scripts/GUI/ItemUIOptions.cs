﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemUIOptions : MonoBehaviour
{

    private ItemUIManager selectedItemUiManager;
    public Text headerText;
    private int currentlySelectedIndex;
    private UIManager uiManager;
    private InventoryItemFiller filler;
    private RectTransform rect;

    // Start is called before the first frame update
    void Start()
    {
        uiManager = UIManager.Instance ?? FindObjectOfType<UIManager>();
        rect = GetComponent<RectTransform>();
    }

    public void SetSelectedItemUiManager(ItemUIManager item) {
        selectedItemUiManager = item;
        currentlySelectedIndex = item.GetID();
        filler = item.GetInventoryFiller();
        if (headerText == null)
            return;
        switch (uiManager.language)
        {
            case UIManager.Language.english: headerText.text = item.GetReferenceItem().itemWrapper.itemName.textEnglish; break;
            case UIManager.Language.italian: headerText.text = item.GetReferenceItem().itemWrapper.itemName.textItalian; break;
        }
        
    }

    public void UseItem() {
        if (selectedItemUiManager == null)
            return;
        selectedItemUiManager.UseItem();
    }

    public void UseMultipleItems(QuantitySelector quantitySelector) {
        if (selectedItemUiManager == null)
            return;
        selectedItemUiManager.UseItem(quantitySelector.GetQuantity());
    }

    public void SetQuantitySelectorMaxCapToSelectedItemQuantity(QuantitySelector quantitySelector) {
        if (selectedItemUiManager == null)
            return;
        quantitySelector.SetMaximumCap(selectedItemUiManager.GetReferenceItem().quantity);
    }
    public void SetQuantitySelectorItemNameText(QuantitySelector quantitySelector) {
        if (selectedItemUiManager == null)
            return;
        quantitySelector.SetItemNameText(selectedItemUiManager.GetItemName());
    }

    public void SetLatestSelectionAsSelected() {
        if (filler == null)
            return;
        filler.SetSelectedIndex(currentlySelectedIndex);
    }
    public void SetLatestSelectionAsHighlighted()
    {
        if (filler == null)        
            return;
        
        filler.SetSelectedButtonAnimationAsHighlighted(currentlySelectedIndex);        
    }

    public void AnchorToCurrentlySelectedItemUIManager() {
        if (selectedItemUiManager != null)
            rect.position = new Vector2(rect.position.x, selectedItemUiManager.GetVerticalPosition());
        else
            print("no selected itemUiManager found");
    }
}
