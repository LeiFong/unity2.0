﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    // Update is called once per frame

    public Collider2D myCollider;
    public ContactFilter2D contactFilter;
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(new Vector3(-2 * Time.deltaTime, 0, 0));
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(new Vector3(2 * Time.deltaTime, 0, 0));
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(new Vector3(0, 2 * Time.deltaTime, 0));
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(new Vector3(0, -2 * Time.deltaTime, 0));
        }

        RaycastHit2D[] results = new RaycastHit2D[10];

        int count = myCollider.Cast(Vector2.zero, contactFilter, results);

        for (int i = 0; i < count; i++)
        {
            results[i].collider.transform.Translate(1, 0, 0);
            print("boooom");
        }
    }
}
