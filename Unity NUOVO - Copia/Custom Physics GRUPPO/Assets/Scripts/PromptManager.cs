﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PromptManager : MonoBehaviour
{
    public static PromptManager Instance;
    public bool promptsEnabled = true;
    public PlayersManager playersManager;
    public Text promptText;
    public CanvasGroup promptCanvasGroup;
    public WorldToCanvasPositioner positioner;
    public Color actionColor, dialogueColor, armorLootColor, weaponLootColor, consumableLootColor, materialLootColor, keyItemLootColor;
    public List<Event> eventList = new List<Event>();
    private UIManager uiManager;
    public int interactCounter;
    public int interactTimerDuration = 20;
    public int initialBoostInteractTimer = 4;

    //public int listIndex = 0;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        uiManager = UIManager.Instance ?? FindObjectOfType<UIManager>();
    }


    private void Update()
    {
        
        
        if (eventList.Count > 1) {
            for (int i = 1; i < eventList.Count; i++)
            {
                if (Mathf.Abs(playersManager.player1.transform.position.x - eventList[i].transform.position.x) <
                    Mathf.Abs(playersManager.player1.transform.position.x - eventList[0].transform.position.x))
                {
                    Event auxEvent = eventList[0];
                    eventList[0] = eventList[i];
                    eventList[i] = auxEvent;
                }
            }
        }

        if (eventList.Count > 0 && promptsEnabled)
        {
            positioner.SetObjectToFollow(eventList[0].transform);
            positioner.SetPosition();
            SetPromptText(eventList[0].promptText, eventList[0].promptType);
            promptCanvasGroup.alpha = 1;

            if (playersManager.player1.controls.Player.Interact.ReadValue<float>() > 0)
            {
                //print("loot");
                if (interactCounter == 0)
                {
                    interactCounter += initialBoostInteractTimer;
                }
                interactCounter++;
                if (interactCounter >= interactTimerDuration + initialBoostInteractTimer)
                {
                    interactCounter = 0;
                    eventList[0].TriggerEvent();
                }
            }
            else
                interactCounter = 0;
        }
        else {
            interactCounter = 0;
            promptCanvasGroup.alpha = 0;
        }
    }


    private void SetPromptText(MultiLanguageText multiLanguageText, Event.PromptType type) {
        if (multiLanguageText == null)
            return;
        string ret = "";
        switch (uiManager.language) {
            case UIManager.Language.english:
                ret = multiLanguageText.textEnglish;
                break;
            case UIManager.Language.italian:
                ret = multiLanguageText.textItalian;
                break;
        }
        SetPromptText(ret, type);
    }

    private void SetPromptText(string text, Event.PromptType type) {
        promptText.text = text;
        switch (type) {
            case Event.PromptType.action:
                promptText.color = actionColor;
                break;
            case Event.PromptType.dialogue:
                promptText.color = dialogueColor;
                break;
            case Event.PromptType.armorLoot:
                promptText.color = armorLootColor;
                break;
            case Event.PromptType.weaponLoot:
                promptText.color = weaponLootColor;
                break;
            case Event.PromptType.consumableLoot:
                promptText.color = consumableLootColor;
                break;
            case Event.PromptType.materialLoot:
                promptText.color = materialLootColor;
                break;
            case Event.PromptType.keyItemLoot:
                promptText.color = keyItemLootColor;
                break;
        }
    }
}
