﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequencer : MonoBehaviour {

	protected int seq = 0;

	public virtual void MyUpdate () {
		
	}

	public virtual void StopSequence () {

	}

	public void Interrupt () {
		StopSequence ();
	}
}
