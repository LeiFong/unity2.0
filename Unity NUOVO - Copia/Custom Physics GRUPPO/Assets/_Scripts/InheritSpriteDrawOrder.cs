﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InheritSpriteDrawOrder : MonoBehaviour
{
    public SpriteRenderer parentSprite;
    public SpriteRenderer mySprite;
    [Tooltip("Sorting order relativo al parent sprite. Se ad esempio il parent sprite ha un sorting order = 20, e il mio relative order = 2, verrà mantenuto" +
        " sorting order = 22.")]
    public int relativeOrder = 0;

    private void Start()
    {
        if (mySprite == null)
            mySprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (parentSprite != null)//&& parentSprite.sortingOrder + relativeOrder != mySprite.sortingOrder)
            mySprite.sortingOrder = parentSprite.sortingOrder + relativeOrder;
    }
}
