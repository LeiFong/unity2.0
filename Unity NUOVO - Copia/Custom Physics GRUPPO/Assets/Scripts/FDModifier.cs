﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FDModifier : MonoBehaviour {

	FrameData fd;
	BaseFrameData bfd;


	public float juggleHorizontalVelocityModifier = 1; //velocita' che assume il nemico quando viene colpito in aria
	public float juggleVertivalVelocityModifier = 1;
	public float launchHorizontalVelocityModifier = 1; //velocita' quando viene lanciato in aria mentre è per terra, IGNORARE SE L'ATTACCO NON E' UN LAUNCHER
	public float launchVertivalVelocityModifier = 1;


	public float damageOnHitModifier = 1;
	public float damageOnBlockModifier = 1;
	public float staminaCostModifier = 1;
	public float staminaDamageOnHitModifier = 1;
	public float staminaDamageOnBlockModifier = 1;
	public float guardDamageModifier = 1;

	void Start() {
		
	}



	//scrive le modifiche sul framedata
	public void LoadFrameData () {
		fd = GetComponent<FrameData> ();
		bfd = GetComponent<BaseFrameData> ();
        if (fd == null || bfd == null)
            print("no frame data");
        else
        {



            fd.currentHitProperties.damageOnHit = Mathf.RoundToInt(bfd.damageOnHit * damageOnHitModifier);
            //fd.damageOnBlock = Mathf.RoundToInt(bfd.damageOnBlock * damageOnBlockModifier);
            if (fd.moveScript != null)
                fd.moveScript.staminaCost = Mathf.RoundToInt(bfd.staminaCost * staminaCostModifier);
            fd.currentHitProperties.staminaDamageOnHit = Mathf.RoundToInt(bfd.staminaDamageOnHit * staminaDamageOnHitModifier);
            //fd.staminaDamageOnBlock = Mathf.RoundToInt(bfd.staminaDamageOnBlock * staminaDamageOnBlockModifier);
            fd.currentHitProperties.guardDamage = Mathf.RoundToInt(bfd.guardDamage * guardDamageModifier);


            fd.currentHitProperties.juggleHorizontalVelocity = bfd.juggleHorizontalVelocity * juggleHorizontalVelocityModifier;
            fd.currentHitProperties.juggleVertivalVelocity = bfd.juggleVertivalVelocity * juggleVertivalVelocityModifier;
            fd.currentHitProperties.launchHorizontalVelocity = bfd.launchHorizontalVelocity * launchHorizontalVelocityModifier;
            fd.currentHitProperties.launchVertivalVelocity = bfd.launchVertivalVelocity * launchVertivalVelocityModifier;
        }
	}

}
