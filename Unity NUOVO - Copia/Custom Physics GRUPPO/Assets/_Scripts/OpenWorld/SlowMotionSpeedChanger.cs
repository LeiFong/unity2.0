﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SlowMotionSpeedChanger : MonoBehaviour
{
    [Tooltip("Mettimi nel root GameObject di una scena. A build time (su disable) retrievo tutti gli animatori e tutti i particle system della scena; a run time" +
        " setto la loro velocità di riproduzione a seconda del time scale.")]
    public bool whatAmI = false;
    public enum RetrieveMode {allOfThem, none, setManually}
    public RetrieveMode retrieveParticleSystems = RetrieveMode.allOfThem;
    public RetrieveMode retrieveAnimators = RetrieveMode.allOfThem;
    public bool excludeAnimatorsWithSpeedSetters = true, retrieveAnimatorSpeedSetters = true;
    [SerializeField]
    public List<ParticleSystem> particleSystems;
    [SerializeField]
    public List<Animator> animators;
    [SerializeField]
    public List<AnimatorSpeedSetter> animatorSpeedSetters;

    private float previousTimeScale = 1f;


    private void OnDisable()
    {
        if (Application.isPlaying)
            return;

        switch (retrieveParticleSystems)
        {
            case RetrieveMode.allOfThem:
                particleSystems = new List<ParticleSystem>(GetComponentsInChildren<ParticleSystem>(true));
                break;
            case RetrieveMode.none:
                particleSystems = new List<ParticleSystem>();
                break;
            case RetrieveMode.setManually:
                if (particleSystems == null)
                    particleSystems = new List<ParticleSystem>();
                break;
        }
        switch (retrieveAnimators)
        {
            case RetrieveMode.allOfThem:
                animators = new List<Animator>(GetComponentsInChildren<Animator>(true));
                break;
            case RetrieveMode.none:
                animators = new List<Animator>();
                break;
            case RetrieveMode.setManually:
                if (animators == null)
                    animators = new List<Animator>();
                break;
        }

        animatorSpeedSetters = new List<AnimatorSpeedSetter>();
        AnimatorSpeedSetter setter;
        for (int i = 0; i < animators.Count; i++)
        {
            setter = animators[i].gameObject.GetComponent<AnimatorSpeedSetter>();

            if (setter != null)
            {
                
                if (excludeAnimatorsWithSpeedSetters)
                {
                    animators.RemoveAt(i);
                    i--;
                }
                if (retrieveAnimatorSpeedSetters)
                {
                    
                    animatorSpeedSetters.Add(setter);
                }
            }
        }
            
        
    }

    private void FixedUpdate()
    {
        if (!Application.isPlaying)
            return;

        if (TimeVariables.currentTimeScale != previousTimeScale)
        {
            ParticleSystem.MainModule mm;
            for (int i = 0; i < particleSystems.Count; i++)
            {
                mm = particleSystems[i].main;
                mm.simulationSpeed = 1f / TimeVariables.currentTimeScale;
            }
            for (int i = 0; i < animators.Count; i++)
            {
                animators[i].speed = 1f / TimeVariables.currentTimeScale;
            }
            for (int i = 0; i < animatorSpeedSetters.Count; i++)
                animatorSpeedSetters[i].SetSpeed();
        }
        previousTimeScale = TimeVariables.currentTimeScale;
    }

}
