﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaminaBarController : MonoBehaviour
{
    private bool initialized = false;
    private Animator animator;
    private int activateParam, deactivateParam;

    private void Awake()
    {
        if (!initialized)
            Initialize();
    }
    public void Initialize()
    {
        initialized = true;
        animator = GetComponent<Animator>();
        activateParam = Animator.StringToHash("Activate");
        deactivateParam = Animator.StringToHash("Deactivate");
    }

    public void ActivateBar() {
        animator.SetTrigger(activateParam);
    }
    public void DeactivateBar() {
        animator.SetTrigger(deactivateParam);
    }
}
