﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterDash : MoveScript
{


    public override void SetStatus()
    {
        status.mystatus = Status.status.attacking;
    }

    public override void MyExecuteSequence()
    {
        status.mystatus = Status.status.attacking;
        if (controller.seq < maxDurationFrames)
            controller.seq++;
        if (controller.hitWhileInvincible)
        {
            
            
            if (controller.horizontal > 0)
            {
                if (!controller.facingRight)
                {
                    controller.Flip();
                    relatedMoves[0].isCharging = true;
                }
            }
            else if (controller.horizontal < 0)
            {
                if (controller.facingRight)
                {
                    controller.Flip();

                    relatedMoves[0].isCharging = true;
                }
            }
            controller.StartNewMove(relatedMoves[0]);

            return;
        }
    }
}
