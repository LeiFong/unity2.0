﻿using UnityEngine;
using System.Collections;

public class DustCloudMaker : GeneratorFromPool {

    [HideInInspector]
    public DustCloudController dcController;
    [Tooltip("Dove appariranno le dust cloud.")]
    public Transform positioner;
    [Tooltip("Ciò che scalerà le dust cloud (es. se devono andare a destra o sinistra).")]
    public Transform scaler;
    [Tooltip("Mi serve per vedere se sono in transizione")]
    public Animator controllerAnimator;    
    private Transform generatedTransform;
    private Animator generatedAnimator;
    private DustCloudBehavior generatedDustCloud;

    private void Awake()
    {
        if (controllerAnimator != null && dcController == null)
            dcController = controllerAnimator.gameObject.GetComponent<DustCloudController>();

    }

    private bool CreateClouds() {
        if (!dcController.executeDuringAnimationTransitions || !controllerAnimator.IsInTransition(0))
        {
            generatedDustCloud = (DustCloudBehavior)Generate();
            generatedTransform = generatedDustCloud.transform;
            generatedTransform.position = new Vector3(positioner.position.x, positioner.position.y);
            generatedTransform.localScale = scaler.localScale;
            generatedAnimator = generatedDustCloud.animator;
            return true;            
        }
        
        return false;
    }
    

    public void CreateDustBackwards()
    {
        if (CreateClouds())
            generatedAnimator.Play("Backwards");


    }
    public void CreateDustBackwardsBig()
    {
        if (CreateClouds())
            generatedAnimator.Play("Backwards big");


    }

    public void CreateDustForward()
    {
        if (CreateClouds())
            generatedAnimator.Play("Forward");


    }
    public void CreateExplosionSmall()
    {
        if (CreateClouds())
            generatedAnimator.Play("ExplosionSmall");


    }
    public void CreateExplosionBig()
    {
        if (CreateClouds())
            generatedAnimator.Play("ExplosionBig");


    }
    public void CreateDustUpwards()
    {
        if (CreateClouds())
            generatedAnimator.Play("Upwards");


    }
    public void CreateDustForSteps() {
        if (CreateClouds())
            generatedAnimator.Play("Step");
    }
}
