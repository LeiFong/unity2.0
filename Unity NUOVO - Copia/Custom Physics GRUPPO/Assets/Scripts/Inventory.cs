﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    [System.Serializable]
    public struct ConsumablesQuantity
    {
        public ConsumableItem item;
        public ItemWrapper itemWrapper;
        public int quantity;
    }

   

    [System.Serializable]
    public struct MaterialsQuantity
    {
        public MaterialItem item;
        public ItemWrapper itemWrapper;
        public int quantity;
    }

    [System.Serializable]
    public struct ItemQuantity
    {
        public Item item;
        public ItemWrapper itemWrapper;
        public int quantity;
        //[HideInInspector]
        public int listIndex; //serve per avere un riferimento veloce alla lista quando usato dal quickItemSelection
    }


    [System.Serializable]
    public struct WeaponInfo
    {
        public WeaponItem weapon;
        public int currentUpgrade;
    }

    [System.Serializable]
    public struct QuickItemSelection
    {
        public ItemQuantity[] items;
        public int currentIndex;
    }

	public List<ConsumablesQuantity> consumables = new List<ConsumablesQuantity> ();
    public QuickItemSelection quickItemSelection = new QuickItemSelection
    {
        items = new ItemQuantity[5],
        currentIndex = 0

    };

    //public int [] quickConsumables = 

    public List<MaterialsQuantity> materials = new List<MaterialsQuantity>();
    public List<WeaponInfo> weapons = new List<WeaponInfo>();
    
    public int playerNumber;
    public Controller2D controller;

    public ItemDatabase itemDatabase;
    public LootNotificationGenerator lootNotificationGenerator;


    public enum ItemType {consumable, special, equipment, weapon, key, material}; //da aggiornare con nuovi tipi
    //public enum NonUsableItemType {equipment, weapon, key, material};

    public void LootItem(Loot loot)
    {
        if (loot.quantifiableLoot.item != null)
        {
            AddItem(loot.quantifiableLoot.item, loot.quantifiableLoot.quantity);
            lootNotificationGenerator.Display(loot.quantifiableLoot.item.itemName, Color.white, null);

        }
        else if (loot.weaponLoot.weapon != null)
        {
            AddWeaponItem(loot.weaponLoot.weapon, loot.weaponLoot.upgrade);
            lootNotificationGenerator.Display(loot.weaponLoot.weapon.itemName, Color.white, null);
        }
    }

    public void AddWeaponItem (WeaponItem weapon, int upgrade)
    {
        WeaponInfo newWeapon = new WeaponInfo();
        newWeapon.currentUpgrade = upgrade;
        newWeapon.weapon = weapon;

        weapons.Add(newWeapon);
        
    }

    public void EquipWeapon (int index)
    {
        if (index >= weapons.Count)
        {
            print("no weapons");
            return;
        }

        controller.weaponSwitcher.weapons[weapons[index].weapon.weaponIndex].currentWeaponUpgrade = weapons[index].currentUpgrade;
        controller.weaponSwitcher.SwitchWeapon(weapons[index].weapon.weaponIndex);
    }

    public void AddQuickConsumable (Item item, int index)
    {
        for (int i = 0; i < quickItemSelection.items.Length; i++)
        {
            if (quickItemSelection.items[i].item != null && quickItemSelection.items[i].item.itemCode == item.itemCode)
            {
                //print("oggetto gia presente");
                quickItemSelection.items[i].item = null;
                quickItemSelection.items[i].itemWrapper = null;
                quickItemSelection.items[i].quantity = 0;
                //return;
            }
        }

        //quickConsumables[index] = (consumables[consumables.IndexOf(newConsumable)]);
        ItemQuantity newItem = new ItemQuantity
        {
            item = item,
            quantity = 0,
            itemWrapper = itemDatabase.GetItemFromDatabase(item)
        };
        quickItemSelection.items[index] = newItem;
        UpdateQuickConsumables();
    }

    public void UseItemFromQuickSelectionMenu ()
    {
        if (quickItemSelection.items[quickItemSelection.currentIndex].item == null ||
            quickItemSelection.items[quickItemSelection.currentIndex].itemWrapper == null ||
            quickItemSelection.items[quickItemSelection.currentIndex].quantity == 0)
        {
            print("no item in the currently selected slot");
            return;
        }
        UseItem(quickItemSelection.items[quickItemSelection.currentIndex].listIndex, 1, quickItemSelection.items[quickItemSelection.currentIndex].item.itemType);
    }

    public void UpdateQuickConsumables()
    {
        //print("update");
        for (int i = 0; i < quickItemSelection.items.Length; i++)
        {
            if (quickItemSelection.items[i].item is ConsumableItem)
            {
                quickItemSelection.items[i].quantity = 0;
                for (int k = 0; k < consumables.Count; k++)
                {
                    if (consumables[k].item.itemCode == quickItemSelection.items[i].item.itemCode)
                    {
                        quickItemSelection.items[i].quantity = consumables[k].quantity;
                        quickItemSelection.items[i].listIndex = k;
                        break;
                    }
                   
                }
            }
        }
    }

    public void AddItem(Item itemToAdd, int quantity)
    {
        AddItemToList(itemToAdd, quantity);
        UpdateQuickConsumables();
    }
    private void AddItemToList(Item itemToAdd, int quantity) {

        if (itemToAdd is ConsumableItem)
        {
            ConsumableItem consumable = (ConsumableItem)itemToAdd;

            //cerco se ho già quell'oggetto nell'inventario dei consumabili
            for (int i = 0; i < consumables.Count; i++)
            {
                if (consumables[i].item.itemCode == itemToAdd.itemCode)
                {
                    //print("trovato: " + itemToAdd.GetType());
                    ConsumablesQuantity addedItem = new ConsumablesQuantity();
                    addedItem.item = consumable;
                    addedItem.itemWrapper = itemDatabase.GetItemFromDatabase(addedItem.item);

                    addedItem.quantity = Mathf.Min(consumables[i].quantity + quantity, consumable.maxQuantity);
                    consumables[i] = addedItem;
                    Debug.Log("Added");
                    return;
                }
            }

            //se non lo trova
            ConsumablesQuantity newItem = new ConsumablesQuantity();
            newItem.item = consumable;
            newItem.quantity = Mathf.Min(quantity, consumable.maxQuantity);
            newItem.itemWrapper = itemDatabase.GetItemFromDatabase(newItem.item);

            //esploro la lista, e aggiungo in una posizione che dipende dalla sua sottocategoria
            for (int i = 0; i < consumables.Count; i++)
            {
                //if (consumables[i].item)
                if (consumables[i].item.subcategory == consumable.subcategory)
                {
                    if (consumables[i].item.subCategoryIndex >= consumable.subCategoryIndex)
                    {
                        consumables.Insert(i, newItem);
                        return;
                    }

                }
                else
                {
                    if (consumables[i].item.subcategory > consumable.subcategory)
                    {
                        consumables.Insert(i, newItem);
                        return;
                    }
                }
            }

            consumables.Add(newItem);

            //break;
        }
        else if (itemToAdd is MaterialItem)
        {
            MaterialItem material = (MaterialItem)itemToAdd;
                for (int i = 0; i < materials.Count; i++)
                {
                    if (materials[i].item.itemCode == itemToAdd.itemCode)
                    {
                        //print("trovato: " + itemToAdd.GetType());
                        MaterialsQuantity addedItem = new MaterialsQuantity();
                        addedItem.item = material;
                    addedItem.itemWrapper = itemDatabase.GetItemFromDatabase(addedItem.item);

                    addedItem.quantity = Mathf.Min(materials[i].quantity + quantity, itemToAdd.maxQuantity);
                        materials[i] = addedItem;
                        Debug.Log("Added");
                        return;
                    }
                }

                MaterialsQuantity newItem = new MaterialsQuantity();
                newItem.item = material;
                newItem.quantity = Mathf.Min(quantity, itemToAdd.maxQuantity);
            newItem.itemWrapper = itemDatabase.GetItemFromDatabase(newItem.item);
            //esploro la lista, e aggiungo in una posizione che dipende dalla sua sottocategoria
            for (int i = 0; i < materials.Count; i++)
            {
                //if (consumables[i].item)
                if (materials[i].item.subcategory == material.subcategory)
                {
                    if (materials[i].item.subCategoryIndex >= material.subCategoryIndex)
                    {
                        materials.Insert(i, newItem);
                        return;
                    }

                }
                else
                {
                    if (materials[i].item.subcategory > material.subcategory)
                    {
                        materials.Insert(i, newItem);
                        return;
                    }
                }
            }

            materials.Add(newItem);

        }

        
		Debug.Log ("Added");
	}

	public void RemoveItem(int ind, int quantity, ItemType itemType) {

        switch (itemType)
        {
            case ItemType.consumable:
                {
                    if (ind < consumables.Count)
                    {
                        print("remove: " + quantity);
                        ConsumablesQuantity newItem;
                        newItem.item = consumables[ind].item;
                        newItem.quantity = consumables[ind].quantity - quantity;
                        newItem.itemWrapper = itemDatabase.GetItemFromDatabase(newItem.item);

                        consumables[ind] = newItem;

                        if (consumables[ind].quantity <= 0 && consumables[ind].item.removeOnDepletion)
                        {
                            consumables.Remove(consumables[ind]);
                        }

                        
                    }

                    UpdateQuickConsumables();
                    break;

                }
            default:
                break;
        }
        

	}

    


	public void UseItem(int ind, int quantity, ItemType itemType) {
        switch (itemType)
        {
            case ItemType.consumable:
                {
                    if (ind < consumables.Count)
                    {
                        if (consumables[ind].quantity > 0)
                        {
                            UseMultiple(ind, Mathf.Min(consumables[ind].quantity, quantity));
                            //if (consumables[ind].itemEffect.item.itemType == ItemType.consumable)
                            //RemoveItem(ind, Mathf.Min(consumables[ind].quantity, quantity), ItemType.consumable);
                        }
                    }
                    else
                        Debug.Log("no objects");
                    break;
                }
            default:
                break;
        }
        //UpdateQuickConsumables();
    }

    public virtual void UseMultiple(int ind, int quantity)
    {
        for (int i = 0; i < quantity; i++)
        {
            print("usemulti: " + quantity);

            itemDatabase.consumableItemsDatabase[consumables[ind].item.itemCode].OnUse(this);
        }
        RemoveItem(ind, quantity, ItemType.consumable);
    }


    public void PrintInventory() {
		for (int i = 0; i < consumables.Count; i++) {
			print(consumables [i] + ", " + consumables[i].quantity);
		}
	}

	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.F)) {
            print("f");
            //FuckYouer f = ScriptableObject.CreateInstance (( itemDatabase.itemDatabase[0])); //ScriptableObject.Instantiate(itemDatabase.itemDatabase[0]);
            //Item f = ScriptableObject.Instantiate(itemDatabase.itemDatabase[0]);
            //f.InitializeItem(this);
            
			AddItem (itemDatabase.consumableItemsDatabase[0].item, 2);
		}
        
        if (Input.GetKeyDown(KeyCode.G))
        {
            print("g");
            //FuckYouer f = ScriptableObject.CreateInstance (( itemDatabase.itemDatabase[0])); //ScriptableObject.Instantiate(itemDatabase.itemDatabase[0]);
            //Item f = ScriptableObject.Instantiate(itemDatabase.itemDatabase[2]);
            //f.InitializeItem(this);
            AddItem(itemDatabase.consumableItemsDatabase[1].item, 2);
        }


        if (Input.GetKeyDown (KeyCode.H)) {
            //UseItem (0, 3, ItemType.consumable);
            //UseItemFromQuickSelectionMenu();
		}

		if (Input.GetKeyDown (KeyCode.J)) {
            
            //AddQuickConsumable(consumables[0].item, quickItemSelection.currentIndex);
		}

        if (Input.GetKeyDown(KeyCode.L))
        {

            //AddQuickConsumable(consumables[1].item, quickItemSelection.currentIndex);
        }

        if (Input.GetKeyDown(KeyCode.K))
        {

            quickItemSelection.currentIndex++;
            quickItemSelection.currentIndex %= quickItemSelection.items.Length;
        }

        if (Input.GetKeyDown (KeyCode.P)) {


            if (controller.weaponSwitcher.i == 0)
                EquipWeapon(2);
            else if (controller.weaponSwitcher.i == 2)
                EquipWeapon(3);
            else
                EquipWeapon(0);
            print("equip?????? sono " + controller.characterContainer + " " + controller.weaponSwitcher.i);

        }

    }
}
