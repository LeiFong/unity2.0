﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LedgeClimb : MoveScript {

	public int maximumDuration = 60;
    public int endingFrames = 20;
    public int walkingFrames = 20;
	public float initialVerticalVelocity;
	public float finalVerticalVelocity;
	public Collider2D [] hitBuffer = new Collider2D[30];
    public Collider2D climbChecker;
	private int mySeq = 0;
	private bool startedClimbing = false;
	//public Collider2D ledgeGrabber;
    public int climbingAnimationEndFrame;
	public LayerMask ledgeClimbLayerMask, platformClimbLayerMask;
	public ContactFilter2D CF;
    public Collider2D climbedPlatform;
    GenericWallMover movingPlatform;
    public Vector2 hitPosition;
    public bool isPlatform;
    public float finalHorizontalVelocity = 3; //velocità orizzontale finale che aiuta a salire meglio sulla piattaforma
    private float platformRotation;
    private bool startedWalkingForward;
    public GameObject climbedLedge;
    private void RepositionCharacter ()
    {
        controller.gameObject.transform.Translate(0, -finalVerticalVelocity * Time.fixedDeltaTime, 0);
    }

    public override void MyExecuteSequence ()
	{
        controller.velocity.y = 0;
        controller.isAffectedByGravity = false;
        //print("exe");
        /*
        Vector2 colliderPoint = new Vector2(controller.verticalCollider.bounds.center.x, controller.verticalCollider.bounds.center.y -
            controller.verticalCollider.bounds.extents.y);

        if (Vector2.Distance(colliderPoint, hitPosition) > 0.2f)
        {


            float newHorizontalOffset = -controller.verticalCollider.bounds.center.x + hitPosition.x;
            float newVerticalOffset = -(controller.verticalCollider.bounds.center.y - controller.verticalCollider.bounds.extents.y) + hitPosition.y;
            Vector2 normalizedVelocity = new Vector2(newHorizontalOffset, newVerticalOffset).normalized;

            controller.targetVelocity.x = normalizedVelocity.x * 15;
            controller.velocity.y = normalizedVelocity.y * 15;
            Debug.DrawRay(controller.verticalCollider.bounds.center, normalizedVelocity * 100, Color.red);
        }

        else
        {
            itsTimeToStopTheSequence = true;
            controller.velocity.y = 0;
        }
        */



        
		if (climbedPlatform == null)
        {
            controller.climbedPlatform = null;
            itsTimeToStopTheSequence = true;
            return;
        }
        else
        {
            controller.climbedPlatform = climbedPlatform;
            /*
            movingPlatform = climbedPlatform.GetComponent<GenericWallMover>();
            if (movingPlatform != null && !movingPlatform.publicL.Contains(controller.verticalCollider))
            {
                movingPlatform.publicL.Add(controller.verticalCollider);
            }
            */
        }
        //int count = ledgeGrabber.OverlapCollider (CF, hitBuffer);
        //int count = controller.verticalCollider.OverlapCollider(CF, hitBuffer);
        int count = climbChecker.OverlapCollider(CF, hitBuffer);


        //print("count: " + count);
        //soluzione un po' di merda, ma non ho voglia di riscrivere tutto
        if (isPlatform)
        {
            for (int i = 0; i < count; i++)
            {
                if (hitBuffer[i].gameObject == climbedLedge)
                {
                    count = 1;
                    break;
                }
                else if (i == count - 1)
                {
                    count = 0;
                }
            }

        }
        else
        {
            //print("sì ma stiamo facendo qualcosa o cosa");
            for (int i = 0; i < count; i++)
            {
                //print("i: " + i + " " + hitBuffer[i].transform.parent + " climbed: " + climbedLedge);
                if (hitBuffer[i].transform.parent.gameObject == climbedLedge)
                {
                    //print("trovato");
                    count = 1;
                    break;
                }
                else if (i == count - 1)
                {
                    count = 0;
                }



            }
        }

        bool isInsideClimbedPlatform = count > 0 ? true : false;




        if (count > 0 && !isPlatform)
        {
            //platformRotation = hitBuffer[0].transform.eulerAngles.z * Mathf.Deg2Rad;
            //print("rotation: " + platformRotation * Mathf.Rad2Deg);
            //print("transfomr: " + hitBuffer[0].gameObject);
        }
        //print("climbedplatform: " + climbedPlatform + " count: " + count);

        /*
        for (int i = 0; i < count; i++)
        {
            if (hitBuffer[i] == climbedPlatform)
            {
                //print("climbedplatform: " + climbedPlatform);
                isInsideClimbedPlatform = true;
                break;
            }
        }
        */
        //print("INSIDE: " + isInsideClimbedPlatform);
		if (!isInsideClimbedPlatform) {
			//controller.targetVelocity.x = controller.facingRight ? 10: -10;
            //controller.velocity.y = finalVerticalVelocity;
            controller.velocity.y = 0;
            //if (!isPlatform)
                //controller.isAffectedByGravity = false;
            //RepositionCharacter();
			//itsTimeToStopTheSequence = true;
            //status.myPhysicsStatus = Status.physicsStatus.grounded;
            //print("noplatforms");
            //return;
			//print ("noplatforms");
		}

        if (count == 0 && startedClimbing)
        {
            if (!startedWalkingForward)
                controller.seq = 0;
            startedWalkingForward = true;
            //controller.seq = 0;
        }
        //if (startedClimbing)
        if (startedWalkingForward)
        {
            //controller.velocity.y = -100;
            //itsTimeToStopTheSequence = true;
            controller.isAffectedByGravity = true; //DA TESTARE
            float newHorizontal = finalHorizontalVelocity * Mathf.Abs(Mathf.Cos(platformRotation));
            /*
            controller.targetVelocity.x = controller.facingRight ? newHorizontal : -newHorizontal;
            controller.velocity.y = Mathf.Abs(finalHorizontalVelocity * Mathf.Sin(Mathf.Abs( platformRotation)));
            print("vertical: " + controller.velocity.y + " horizontal: " + controller.targetVelocity.x + " " + platformRotation);
            Debug.DrawRay(controller.transform.position, new Vector2(controller.targetVelocity.x, controller.velocity.y), Color.red);
            */

            
            if (controller.seq <= walkingFrames)
                controller.targetVelocity.x = controller.facingRight ? finalHorizontalVelocity : -finalHorizontalVelocity;
            else
                controller.targetVelocity.x = 0;
            //print("eh ma allora");
            enableHurtboxChanging = false;
            controller.velocity.y = 0;
            if (status.myPhysicsStatus == Status.physicsStatus.grounded && controller.seq >= endingFrames)
                itsTimeToStopTheSequence = true;
        }
        

		if (controller.seq < maximumDuration) {
			if (controller.seq == 0) {
				//print ("primo frame di wakeup");
				//controller.animator.SetBool ("neutral", false);
				//controller.ChangeParameter (animatorParameterName, true);
				//controller.ChangeParameter (animatorParameterName);
				//controller.animator.Play(animatorParameterName, -1, 0f);
			}
			//int count = controller.ledgeGrabber.OverlapCollider (controller.platformCheckerCF, hitBuffer);
			if (!startedClimbing) {
                //print("alora");
				//count = controller.ledgeGrabber.OverlapCollider (controller.platformCheckerCF, hitBuffer);
				if (count == 0) {
                    //print("alooooooooooora");
					startedClimbing = true;
                    controller.seq = maximumDuration - 50;
				} else {
					//for (int i = 0; i < count; i++)
                    {
						//if (hitBuffer [i].gameObject.layer == LayerMask.NameToLayer ("Platform") && hitBuffer [i].tag != "Stairs") {
						//if (hitBuffer [i].tag != "Stairs")
                        {

                            //controller.targetVelocity.x = controller.facingRight ? 5 : -5; //lo sposto leggermente in avanti per assicurare di atterrare sulla piattaforma
                            //controller.transform.Translate(controller.facingRight ? 5 : -5, 0, 0);
                            //print("suuuu");
                            controller.velocity.y = initialVerticalVelocity;
                            //controller.transform.Translate(0, initialVerticalVelocity * Time.fixedDeltaTime, 0);
							
						}
					}
				}
			} else {
                /*
                if (mySeq < waitingFrames)
                {

                    controller.velocity.y = 0;
                    //controller.velocity.y = finalVerticalVelocity;

                }
                
                else
                */
                {
                    //controller.targetVelocity.x = 10f;
                    //print("startedclimbing");
                    if (count != 0)
                        controller.velocity.y = 0;
                        //controller.velocity.y = finalVerticalVelocity;
                    //controller.transform.Translate(0, finalVerticalVelocity * Time.fixedDeltaTime, 0);
                }
				mySeq++;

    
			}



            
            //controller.isAffectedByGravity = false;
			status.mystatus = Status.status.stuckinanimation;
            //status.myPhysicsStatus = Status.physicsStatus.airborne;
            //status.myPhysicsStatus = Status.physicsStatus.grounded;
            //status.myPreviousPhysicsStatus = Status.physicsStatus.grounded;

            //status.invincible = true;
            controller.seq++;



		} else {
            //StopSequence ();
            //moveIsFinished = true;
            itsTimeToStopTheSequence = true;
			print ("finita sequenza");
		}
        //print("count_: " + count);
        
    }


    public override void SetStatus()
    {
        status.mystatus = Status.status.stuckinanimation;
    }

    public override void MyStopSequence () {
		controller.seq = 0;
		mySeq = 0;
		startedClimbing = false;
        startedWalkingForward = false;
        climbedPlatform = null;
        controller.climbedPlatform = null;
        controller.movingPlatform = null;
        /*
        if (movingPlatform != null && movingPlatform.publicL.Contains(controller.verticalCollider))
        {
            movingPlatform.publicL.Remove(controller.verticalCollider);
        }
        */
		//Debug.Log ("STOP!");
		//controller.animator.SetBool ("land", false);
		//controller.animator.SetBool ("neutral", true);
		status.mystatus = Status.status.neutral;
		status.invincible = false;
        controller.isAffectedByGravity = true;
		controller.currentMove = null;
	}

    

    public override void MyStartSequence()
    {
        startedClimbing = false;
        startedWalkingForward = false;
        enableHurtboxChanging = true;
        controller.activeWeaponSprite.SetActive(false);
        if (climbedPlatform != null)
        {
            if (!isPlatform)
            {
                climbedLedge = climbedPlatform.transform.parent.gameObject;

            }
            else
            {
                climbedLedge = climbedPlatform.gameObject;
            }
            //print("ledge: " + climbedLedge);

        }
    }

}