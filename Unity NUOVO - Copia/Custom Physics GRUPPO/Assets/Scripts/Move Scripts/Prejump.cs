﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prejump : MoveScript {

	public int preJumpFrames;
	public float jumpingVelocity;
	public Controller2D.GravitySections[] customGravitySections;
	public override void MyExecuteSequence ()
	{

		//if (controller.seq < preJumpFrames) {
		if (controller.seq == 0) 
		{
			controller.jumpstartup++;
			//print("JUMP!");
			
			/*
			controller.jumpingGravitySections[0].thresholdVelocity = 1000;
			controller.jumpingGravitySections[0].customGravity = -100;
			controller.jumpingGravitySections[1].thresholdVelocity = 15;
			controller.jumpingGravitySections[1].customGravity = -80;
			controller.jumpingGravitySections[2].thresholdVelocity = 6;
			controller.jumpingGravitySections[2].customGravity = -30;
			controller.jumpingGravitySections[3].thresholdVelocity = 0;
			controller.jumpingGravitySections[3].customGravity = -120;
			*/

			if (customGravitySections.Length == 0)
            {
				controller.jumpingGravitySections = controller.defaultJumpParameters.gravitySections;
            }
			else
				controller.jumpingGravitySections = customGravitySections;
		}
			status.mystatus = Status.status.stuckinanimation;
			
        
			//print ("SALTA!");

		//} else {
			//StopSequence ();
			//moveIsFinished = true;
        if (controller.seq == preJumpFrames)
        {
            //controller.jumpingState = true;
            //itsTimeToStopTheSequence = true;
			controller.aerialMomentum = controller.horizontal * controller.movespeed;
			//print("jump: " + controller.aerialMomentum);
			controller.jumpBuffer = 0;
			//controller.status.myPhysicsStatus = Status.physicsStatus.airborne;
			if (jumpingVelocity == 0)
				controller.velocity.y = controller.jumpVelocity;
			else
				controller.velocity.y = jumpingVelocity;

			controller.recentlyJumpedFrames = controller.jumpLeniency;
			//print("JUMP! " + controller.jumpVelocity);
			controller.animator.Play("jump rising", 0, 0f);
		}

		controller.seq++;
	}

	public override void SetStatus()
	{
		status.mystatus = Status.status.stuckinanimation;
		//status.canRecharge = false;
	}
	public override void MyStopSequence () {
		controller.seq = 0;
		//Debug.Log ("STOP!");
		//status.mystatus = Status.status.neutral;
		//controller.animator.SetBool (animatorParameterName, false);
		//controller.animator.SetBool ("neutral", true);
		//controller.animator.speed = 1;
		controller.currentMove = null;
        controller.jumpDuration = 0;
        //controller.jumpingState = true;

    }

}
