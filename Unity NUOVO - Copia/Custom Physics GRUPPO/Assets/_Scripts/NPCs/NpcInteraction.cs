﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class NpcInteraction : MonoBehaviour
{
    public bool interacting = false;
    public Transform center;
    public Animator animator;
    private CameraManipulator cameraManipulator;
    public NpcTalkTrigger npcEvent;
    protected DialogueManager dialogueManager;

    protected virtual void Start()
    {
        dialogueManager = DialogueManager.Instance ?? FindObjectOfType<DialogueManager>();
        cameraManipulator = GlobalNpcCameraManipulator.Instance == null ?
            FindObjectOfType<GlobalNpcCameraManipulator>().cameraManipulator : GlobalNpcCameraManipulator.Instance.cameraManipulator;
    }

    public virtual void StartInteraction() {
        interacting = true;
        npcEvent.DeactivateEvent();
        if (cameraManipulator != null) {
            cameraManipulator.center = center;
            cameraManipulator.AddMyselfToCameraMovement();
        }
        dialogueManager.currentNpc = this;
    }
    public virtual void EndInteraction() {
        interacting = false;
        npcEvent.ActivateEvent();
        if (cameraManipulator != null)
            cameraManipulator.RemoveMyselfToCameraMovement();
        dialogueManager.currentNpc = null;
    }
    
}
