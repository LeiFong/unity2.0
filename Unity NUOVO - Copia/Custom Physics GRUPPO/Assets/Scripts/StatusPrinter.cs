﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusPrinter : MonoBehaviour {
	public Status status;

	void Start () {
		status = GetComponent<Status> ();
	}
	// Update is called once per frame
	void Update () {
		Debug.Log (status.mystatus);
		Debug.Log (status.myPhysicsStatus);
	}
}
