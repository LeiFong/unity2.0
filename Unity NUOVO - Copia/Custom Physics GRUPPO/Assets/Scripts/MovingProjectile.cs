﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingProjectile : Projectile {
	public float moveSpeed = 2;
	public int directionChangeInterval = 20;
	public float initialDirectionAngle = 0;
	public float directionChangeAngle = 90;
    public float gravity = 0;

	public int seq = 0;
	public float currentAngle = 0;
	bool firstFrame = false;

	protected override void MyOnEnable () {
		firstFrame = true;




	}

	protected override void ComputeVelocity () {
		if (firstFrame) {
			if (!move.facingRight) {
                //print("prima: " + initialDirectionAngle);
                initialDirectionAngle = 180 - initialDirectionAngle;
				directionChangeAngle = - directionChangeAngle;
                //print(initialDirectionAngle);
			}

			currentAngle = initialDirectionAngle;
			firstFrame = false;
		}

		if (directionChangeInterval > 0 && seq >= directionChangeInterval) {
			currentAngle += directionChangeAngle;
			seq = 0;
			print("change direc");
		}

		if (currentAngle >= 360)
			currentAngle -= 360;
		if (currentAngle <= -360)
			currentAngle += 360;

		targetVelocity.x = Mathf.Cos (currentAngle * Mathf.Deg2Rad) * moveSpeed;
		targetVelocity.y = Mathf.Sin (currentAngle * Mathf.Deg2Rad) * moveSpeed;

        targetVelocity.y -= gravity * seq * Time.fixedDeltaTime;
        //if (targetVelocity.y < 0)
            targetVelocity.y = Mathf.Max(targetVelocity.y, -30);
		//print("projectile: " + targetVelocity.y);


		seq++;
	}

	public override void OnCollisionBehaviour ()
	{
        base.OnCollisionBehaviour();

	}
}
