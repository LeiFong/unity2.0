﻿using UnityEngine;
using System.Collections;

public class BarScaleController : MonoBehaviour {

	
	public Status status;
	private float RedHealthPositionScale;
    public ImageBasedProgressBar progressBar;
	//public GameObject activeIndicator; //parte dell'interfaccia che evidenzia quando la stamina attuale è quella attiva

	public int relativePlayerNumber; //numero giocatore al quale la barra appartiene

	public bool trackingHealth, trackingStamina, trackingSecondaryStamina, trackingMeter, trackingStagger;
    private Animator meterBarAnimator;
	//[SerializeField] private SpriteRenderer barSprite;

	// Use this for initialization
	void Start () {
		
        if ((trackingMeter) && progressBar != null) {
            meterBarAnimator = progressBar.gameObject.GetComponent<Animator>();
        }
        
		//status = GetComponentInParent<Status> ();

		RedHealthPositionScale = 1;
	}

    public virtual void BarInitialization()
    {

    }

    // Update is called once per frame
    void Update() {
        ManageBars();

        /*
		//vita rossa
		//se non è nella posizione giusta, vai verso la posizione giusta lentamente
		if (damagetakenbar.localScale.x > RedHealthPositionScale) {			
			float speed = 0.012f;
			//se c'è vita recuperabile, vai alla tua posizione più rapidamente
			if (RedHealthPositionScale > bar.localScale.x)
				speed = 0.03f;

			//setta la vita rossa
			damagetakenbar.localScale = new Vector3 (damagetakenbar.localScale.x - speed, damagetakenbar.localScale.y);
		}
		else {
			damagetakenbar.localScale = new Vector3 (RedHealthPositionScale, bar.localScale.y);
		}
        */
    }

    protected virtual void ManageBars() { 

		if (status != null && progressBar != null) {

            if (trackingHealth)
                setSize(status.currentHealth, status.maxHealth, status.currentRedHealth);
            else if (trackingStamina)
            {
                Color c = progressBar.progressBar.color;
                //c.a = status.currentStance == Status.Stances.fast ? 1 : 0.5f;
                c.a = 1;
                progressBar.progressBar.color = c;
                //setSize(status.currentStance == Status.Stances.fast ? status.currentStamina : status.secondaryStamina, status.maxStamina, 0);
                setSize(status.currentStamina, status.maxStamina, 0);
            }
            else if (trackingSecondaryStamina)
            {
                Color c = progressBar.progressBar.color;
                c.a = status.currentStance == Status.Stances.slow ? 1 : 0.5f;
                progressBar.progressBar.color = c;
                setSize(status.currentStance == Status.Stances.slow ? status.currentStamina : status.secondaryStamina, status.maxStamina, 0);
            }
            else if (trackingMeter)
            {
                setSize(status.currentMeter, status.maxMeter, 0);
            }
            else if (trackingStagger)
            {
                setSize(status.currentStagger, status.maxStagger, 0);

                if (!status.isStaggered)
                    progressBar.progressBar.color = Color.blue;
                else
                    progressBar.progressBar.color = Color.red;

            }


        }
		
	}

    private void ManageMeterBar (int currentMeter, int maxMeter)
    {
        
        return;
        /*
        setSize(currentMeter, maxMeter, 0);
        if (meterBarAnimator != null)
        {
            if (status.controller.conditions.specialMoveUsable > 0 && ((trackingMeter1 && currentMeter < maxMeter)
                || (trackingMeter2 && currentMeter == 0)))
            {
                
                meterBarAnimator.SetBool("active", true);
                meterBarAnimator.SetBool("neutral", false);
                meterBarAnimator.SetBool("filled", false);

                setSize(status.controller.conditions.specialMoveUsable, status.controller.conditions.specialMoveBuffDuration, 0);


            }
            else
            {
                meterBarAnimator.SetBool("active", false);

                if (currentMeter >= maxMeter)
                {
                    meterBarAnimator.SetBool("filled", true);
                    meterBarAnimator.SetBool("neutral", false);

                }
                else
                {
                    meterBarAnimator.SetBool("filled", false);
                    meterBarAnimator.SetBool("neutral", true);


                }
            }
        }
        */
    }

	public void setSize(int currentValue, int maxValue, int recoverable){

		if (currentValue < 0)
			currentValue = 0;

        //print(currentValue + " " + maxValue + " " + currentValue / maxValue);

        float newValue = (float) currentValue / (float) maxValue;

        //print("newvalue: " + newValue);

        progressBar.SetValue(newValue);
        progressBar.SetRedBarValue((float)(currentValue + recoverable) / (float)maxValue);

        /*

		float sizeNormalized = ((float) currentValue) / ((float) maxValue);
		//print (maxValue);
		RedHealthPositionScale = ((float)(currentValue + recoverable)) / ((float)maxValue);
		bar.localScale = new Vector3 (sizeNormalized, bar.localScale.y);
        */

	}


}
