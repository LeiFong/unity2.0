﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetableList : MonoBehaviour {

    public int numberofTargetables = 0;
    public LayerMask losMask;
    public ContactFilter2D targetCF;
    //public List<GameObject> targetableEnemies = new List<GameObject> ();
    //public Enemy myTargetEnemy;
    //public List<GameObject> potentialEnemies = new List<GameObject> ();
    public List<Enemy> potentialEnemiesList = new List<Enemy> ();
    //public List<GameObject> objectsInsideBox = new List<GameObject>();
    //public EnemyDetector enemyDetector;
    public AIMovements aiMovements;
    public Collider2D myCollider;
    public Collider2D myTargetableCollider;

    public bool disengageWhenTargetIsInDifferentComptetenceZone = true;
    public int maxFramesForDisengagement = 300; //quanti frame deve rimanere fuori dalla mia competenceZone il bersaglio perchè io disingaggi?
    public float maxDistanceForTargeting = 12; //distanza a partire dalla quale rimuove il bersaglio dalla lista
    public float maxVerticalDistanceForTargeting = 4; //distanza verticale massima 
    public float maxDistanceForDisengagement = 12; //distanza a partire dalla quale inizia il countdown del disengagement
    public int minSwitchTargetFrames = 100;
    public int maxSwitchTargetFrames = 200;
    public int currentSwitchTargetFrame = 0;
    public int switchTargetCounter = 0;
    public bool aboutToSwitchTarget = false;
    public bool hasDetected;


    [System.Serializable]
    public struct Enemy
    {
        public TargetableObject potentialTargetableObject;
        public int framesUntilDisengagement;
        public bool isTargetable;
    }

	public GameObject myTarget;
	public int index = 0;
	public Transform targetLockPosition;
	public Status status;
    public bool newEngagement = false;
    public Controller2D controller;
    CharacterContainer characterContainer;


    public void Initialize () {
        /*
        characterContainer = GetComponentInParent<CharacterContainer>();

        status = characterContainer.GetComponentInChildren<Status>();
        controller = status.controller;
        enemyDetector = GetComponentInChildren<EnemyDetector>();
        aiMovements = GetComponentInParent<AIMovements>();
        myCollider = GetComponent<Collider2D>();
        myTargetableCollider = GetComponentInChildren<TargetableObject>().gameObject.GetComponent<Collider2D>();
        */
        //objectsInsideBox.Clear();
        switchTargetCounter = 0;
        aboutToSwitchTarget = false;
        losMask = controller.platformLayerMask;
        losMask |= 1 << controller.defaultLayerIndex;

        targetCF.useLayerMask = true;
        targetCF.layerMask &= 0;

        switch (controller.status.faction) {
            case 0:
                {
                    targetCF.layerMask |= 1 << controller.character1LayerIndex;
                    targetCF.layerMask |= 1 << controller.character2LayerIndex;

                    break;
                }
            case 1:
                {
                    targetCF.layerMask |= 1 << controller.character0LayerIndex;
                    targetCF.layerMask |= 1 << controller.character2LayerIndex;

                    break;
                }
            default:
                {
                    targetCF.layerMask |= 1 << controller.character0LayerIndex;
                    targetCF.layerMask |= 1 << controller.character1LayerIndex;
                    break;
                }
        }

        //horizontalCollisionMask |= 1 << platformLayerIndex;

        /*
        enemyDetector.cf.useTriggers = true;
        enemyDetector.cf.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        enemyDetector.cf.useLayerMask = true;
        */
        //enemyDetector.controller = controller;
        //enemyDetector.targetableList = this;
    }

    public void ResetTargetingList ()
    {
        //targetableEnemies.Clear();
        //myTargetEnemy.potentialTargetableObject = null;
        potentialEnemiesList.Clear();
        index = 0;

    }


    void OnTriggerEnter2D (Collider2D hitbox) {
        //print ("CIAO");
        if (myTargetableCollider != hitbox)// && !objectsInsideBox.Contains(hitbox.gameObject))// && CheckLineOfSight(hitbox.gameObject))
        {
            //objectsInsideBox.Add(hitbox.gameObject);
            //if (controller.enemySpecializer.isAI)
            AddObjectToPotentialList(hitbox.gameObject);
        }
	}

    private void OnTriggerExit2D(Collider2D collision)
    {
        /*
        if (objectsInsideBox.Contains(collision.gameObject))
        {
            objectsInsideBox.Remove(collision.gameObject);
            
            //if (!objectsInsideBox.Contains(potentialEnemy.potentialTargetableObject.gameObject))
            //{
            //if (!targetableEnemies.Contains(potentialEnemy.potentialTargetableObject.gameObject)) 
            //if (myTarget != potentialEnemy.potentialTargetableObject.gameObject)
            //  return false;
            }
            
        }
        */
        
        for (int i = 0; i < potentialEnemiesList.Count; i++)
        {
            if (collision.gameObject == potentialEnemiesList[i].potentialTargetableObject.gameObject)
            {
                
                if (!potentialEnemiesList[i].isTargetable)
                {
                    RemoveTargetableObjectFromList(i);
                }
                break;
            }
        }
    }



    /*
	void OnTriggerExit2D (Collider2D hitbox) {
		RemoveObjectFromList (hitbox.gameObject);
	}
    */

    public void CheckTargetCollisions ()
    {
        //myCollider.Cast()
    }

    public void AddObjectToPotentialList(GameObject obj) {
        TargetableObject targetableObject = obj.GetComponent<TargetableObject>();
        //print("sono " + controller.gameObject + " targetable object: " + targetableObject);
        //if (!potentialEnemies.Contains(obj) && objStatus.faction != status.faction)
        for (int i = 0; i < potentialEnemiesList.Count; i++)
        {
            if (potentialEnemiesList[i].potentialTargetableObject == targetableObject)
            {
                //print("c'è già!");
                
                return;
            }
        }

        if (targetableObject.status.faction != status.faction)
        {
            Enemy newEnemy = new Enemy();
            InitializeNewEnemy(ref newEnemy, ref targetableObject);
            //print("new enemy: " + newEnemy.framesUntilDisengagement + " " + newEnemy.potentialTargetableObject);

            potentialEnemiesList.Add(newEnemy);
            //print("list: " + potentialEnemiesList.Count);
        }
	}

    private void InitializeNewEnemy (ref Enemy newEnemy, ref TargetableObject obj)
    {
        newEnemy.framesUntilDisengagement = maxFramesForDisengagement;
        newEnemy.potentialTargetableObject = obj;
        
        
    }



	public void RemoveTargetableObjectFromList(int i) {

        potentialEnemiesList.RemoveAt(i);
        if (index > i)
        {
            index--;
            myTarget = potentialEnemiesList[index].potentialTargetableObject.gameObject;
        }
        else if (index == i)
        {
            Target();
        }
		//if (myTarget == obj)
		//	myTarget = null;
        /*
        //for (int i = 0; i < potentialEnemiesList.Count; i++)
        {
            //if (potentialEnemiesList[i].potentialTargetableObject.gameObject == obj)
            {
                
                if (potentialEnemiesList[i].isTargetable)
                {
                    Enemy e = potentialEnemiesList[i];
                    e.isTargetable = false;
                    potentialEnemiesList[i] = e;
                    numberofTargetables--;
                }
                
                if (myTarget != null)
                {
                    if (myTarget == potentialEnemiesList[i].potentialTargetableObject.gameObject)
                    {
                        Target();
                    }
                }
                
            }
        }

        
		if (targetableEnemies.Contains (obj)) {
			
			targetableEnemies.Remove (obj);
		}

        //objectList.Sort (SortByDistance);
        if (myTarget != null) {
            if (myTarget != obj)
                index = targetableEnemies.IndexOf(myTarget);
            else
                Target();
        }
        else {
            //myTarget = null;
            //Target ();
        }

		if (targetableEnemies.Count == 0)
			index = 0;
        */
	}

	/*
	 * 
	 * A C
	 * 0 1 2
	 * 
     * 
     * 
     * nuovo algoritmo per targeting!
     * solo un collider per l'enemydetection
     * due liste, potentialEnemiesList e targetetableEnemiesList
     * 
     * quando un nemico entra nell'enemydetection, lo inserisco in potentialEnemiesList
     * occhio: se esce dall'enemydetection, NON lo rimuovo dalla lista!
     * ogni frame, scorro potentialEnemiesList e determino la bersagliabilità di ogni elemento secondo vari criteri con CheckTargetCompatibility()
     *  se restituisce vero, lo aggiungo alla lista targetableEnemiesList
     *  se restituisce falso, lo rimuovo dalla lista targetableEnemiesList
     * ogni volta che viene chiamato Target(), la lista da scorrere è targetableEnemiesList
     * 
     * 
	 * 
	 */

    private bool CheckForRemovableObjects (Enemy potentialEnemy)
    {
        if (potentialEnemy.potentialTargetableObject == null || !potentialEnemy.potentialTargetableObject.gameObject.activeInHierarchy)
        {
            print("nun c'è");
            return false;

        }
        //if (potentialEnemy.framesUntilDisengagement <= 0)
          //  return false;
        if (Vector2.Distance(potentialEnemy.potentialTargetableObject.transform.position, transform.position) > maxDistanceForTargeting)
            //|| Mathf.Abs(potentialEnemy.potentialTargetableObject.transform.position.y - transform.position.y) > maxVerticalDistanceForTargeting)
        {
            print("too far");
            return false;
        }

        //probabilmente pesante!!
        /*
        if (!objectsInsideBox.Contains(potentialEnemy.potentialTargetableObject.gameObject)) {
            //if (!targetableEnemies.Contains(potentialEnemy.potentialTargetableObject.gameObject)) 
            if (myTarget != potentialEnemy.potentialTargetableObject.gameObject)
                return false;
        }
        */
        if (potentialEnemy.framesUntilDisengagement == 0)
            return false;

        return true;
    }

    public void CheckForTargetableEnemies ()
    {

        numberofTargetables = 0;
        for (int i = 0; i < potentialEnemiesList.Count; i++)
        {
            //print("checktargetable");
            bool decrement = false;
            //Enemy checkedEnemy = potentialEnemiesList[i];
            
            if (!CheckForRemovableObjects(potentialEnemiesList[i]))
            {
                //if (targetableEnemies.Contains(potentialEnemiesList[i].potentialTargetableObject.gameObject))
                //RemoveTargetableObjectFromList(i);
                if (potentialEnemiesList[i].isTargetable)
                    numberofTargetables--;

                //potentialEnemiesList.RemoveAt(i);
                RemoveTargetableObjectFromList(i);

                i--;
                //print("remove " + i);

                //resetta il triggerstate delle collisioni
                if (potentialEnemiesList.Count == 0)
                {
                    
                    myCollider.enabled = false;
                    myCollider.enabled = true;
                    
                }

                continue;
            }
            if (CheckTargetCompatibility(i))
            {
                numberofTargetables++;
                //if (!targetableEnemies.Contains(potentialEnemiesList[i].potentialTargetableObject.gameObject))
                //    targetableEnemies.Add(potentialEnemiesList[i].potentialTargetableObject.gameObject);
                if (!potentialEnemiesList[i].isTargetable)
                {
                    Enemy e = potentialEnemiesList[i];
                    e.isTargetable = true;
                    potentialEnemiesList[i] = e;

                }
            }
            else
            {
                /*
                if (targetableEnemies.Contains(potentialEnemiesList[i].potentialTargetableObject.gameObject))
                {
                    
                    RemoveTargetableObjectFromList(potentialEnemiesList[i].potentialTargetableObject.gameObject);
                    
                    decrement = true;
                }
                */
                if (potentialEnemiesList[i].isTargetable)
                {
                    Enemy e = potentialEnemiesList[i];
                    e.isTargetable = false;
                    potentialEnemiesList[i] = e;
                }
            }

            if (myTarget == potentialEnemiesList[i].potentialTargetableObject.gameObject && !potentialEnemiesList[i].isTargetable)
            {
                Target();
            }

            /*
            {
                if (myTarget == potentialEnemiesList[i].potentialTargetableObject.gameObject)
                    myTargetEnemy = potentialEnemiesList[i];
            }
            */

            if (decrement)
                i--;
        }
        if (myTarget == null)
        {
            //myTargetEnemy.potentialTargetableObject = null;
            //myTargetEnemy.framesUntilDisengagement = 0;
            
        }

        PeriodicTargetSwitch();
        
    }

    void UpdateTargetInformations ()
    {

    }

    protected void PeriodicTargetSwitch()
    {
        if (numberofTargetables <= 1)
        {
            switchTargetCounter = 0;
            currentSwitchTargetFrame = 0;
            return;
        }
        else
        {
            if (currentSwitchTargetFrame == 0)
            {
                currentSwitchTargetFrame = Random.Range(minSwitchTargetFrames, maxSwitchTargetFrames);
                switchTargetCounter = 0;
            }
            if (switchTargetCounter >= currentSwitchTargetFrame)
            {
                Target();
                currentSwitchTargetFrame = Random.Range(minSwitchTargetFrames, maxSwitchTargetFrames);
                switchTargetCounter = 0;
            }
            switchTargetCounter++;
        }

    }
    private bool CheckTargetCompatibility (int listIndex)
    {
        
        Enemy potentialEnemy = potentialEnemiesList[listIndex];
        //if (controller.status.faction == 1)
        //print("prontooooooooooooooo " + listIndex + " " + potentialEnemy.potentialTargetableObject.controller.gameObject);
        /*
        if (targetableEnemies.Count == 0 && !objectsInsideBox.Contains(potentialEnemiesList[listIndex].potentialTargetableObject.gameObject))
        {
            //if (controller.status.faction == 1)
            //print(" sono " + controller.gameObject + " ma perchè " + potentialEnemiesList[listIndex].potentialTargetableObject.controller.gameObject);
            return false;
        }
        */
        if (potentialEnemy.potentialTargetableObject.status.faction == status.faction || potentialEnemy.potentialTargetableObject.status.dead)
        {
            //print("ded");
            return false;
        }

        {
            if ((disengageWhenTargetIsInDifferentComptetenceZone && aiMovements.currentCompetenceZone != null 
                && potentialEnemiesList[listIndex].potentialTargetableObject.aiMovements.currentCompetenceZone != aiMovements.currentCompetenceZone)
                || (!CheckLineOfSight(potentialEnemiesList[listIndex].potentialTargetableObject.controller.lineOfSightTarget))
                || (Vector2.Distance(potentialEnemy.potentialTargetableObject.transform.position, transform.position) > maxDistanceForDisengagement)
)
            {
                //if (targetableEnemies.Contains(potentialEnemy.potentialTargetableObject.gameObject))
                if (potentialEnemy.isTargetable)
                {
                    potentialEnemy.framesUntilDisengagement--;
                    if (potentialEnemy.framesUntilDisengagement <= 0)
                    {
                        potentialEnemiesList[listIndex] = potentialEnemy;
                        print("disengage");
                        return false;
                    }
                }
                else
                {
                    //if (controller.status.faction == 1)
                    //print("sono " + controller.gameObject + " ma chi ti conosce: " + potentialEnemy.potentialTargetableObject.controller.gameObject);
                    return false;

                }

            }
            else
                potentialEnemy.framesUntilDisengagement = maxFramesForDisengagement;

            potentialEnemiesList[listIndex] = potentialEnemy;
        }
       
        
        

        return true;
    }

	public void Target () {
        //print ("Target! sono " + controller.gameObject);
        bool hadNoTarget = false;
		if (numberofTargetables > 0) {
            
			if (myTarget == null) {
                index = 0;
                hadNoTarget = true;
				potentialEnemiesList.Sort (SortByDistance);
                
                for (int i = 0; i < potentialEnemiesList.Count; i++)
                {
                    if (potentialEnemiesList[i].isTargetable)
                    {
                        index = i;
                        break;
                    }
                }
				
			} else {
                if (numberofTargetables > 1)
                {
                    //print("aaaa");
                    for (int i = index; i < potentialEnemiesList.Count; i++)
                    {
                        i++;
                        i %= potentialEnemiesList.Count;


                        if (potentialEnemiesList[i].isTargetable)
                        {
                            index = i;
                            break;
                        }
                    }
                    //index++;
                    //index = index % potentialEnemiesList.Count;
                }
                else
                    index = 0;
			}
			myTarget = potentialEnemiesList [index].potentialTargetableObject.gameObject;
            //myTargetEnemy = index;

            if (myTarget != null)
            {
                //print("TARGETTTTT");
                TargetableObject target = myTarget.GetComponent<TargetableObject>();
                //targetLockPosition = target.targetLockPosition.transform;
                //controller.enemySpecializer.bSelector.targetsAIMovements = target.status.GetComponentInChildren<AIMovements>();
                if (hadNoTarget)
                {
                    newEngagement = true;
                    
                }
            }

		} else {
            //Debug.Log ("I don't have a target!");
            myTarget = null;
			return;
		}
			

		
	}

	public void TurnOffTarget () {
		index = 0;
		myTarget = null;

	}
		

	int SortByDistance (Enemy obj1, Enemy obj2)
	{
		return Vector2.Distance (obj1.potentialTargetableObject.transform.position, obj2.potentialTargetableObject.transform.position)
            .CompareTo(Vector2.Distance (transform.position, obj2.potentialTargetableObject.transform.position));
			

			//(new Vector2(gameObject.transform.position.x, transform.position.y), new Vector2(obj1.transform.position.x, obj1.transform.position.y));
			//.CompareTo(Vector2.Distance (transform.position, obj2.transform.position));

	}


    public bool CheckLineOfSight(Vector2 targetPosition)
    {
        Vector2 heading = targetPosition - controller.lineOfSightOrigin;
        //print(heading.magnitude);
        RaycastHit2D hit = Physics2D.Raycast(controller.lineOfSightOrigin, heading, heading.magnitude, losMask);
        Debug.DrawRay(controller.lineOfSightOrigin, heading, Color.red);
        //print("LOS: " + hit);
        if (hit)
        {
            //print("non in LOS");
            return false;

        }
        else
        {
            //print("in LOS");
            return true;
        }
    }

    public void EnemyDetect()
    {
        CheckForTargetableEnemies();
        if (numberofTargetables > 0)
            hasDetected = true;
        else
            hasDetected = false;

    }




    }
