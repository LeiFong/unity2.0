﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CastPhysics : CustomPhysics
{
    //float exitCharactersVelocity;
    bool collisionRight, collisionLeft;
    bool hasHitWall = false;
    bool acceptableAngle = false;
    //Vector2 tempVel = new Vector2();
    [HideInInspector]
    public Vector2 lineOfSightOrigin, lineOfSightTarget; //la prima è la posizione da cui parte il check del los verso altri oggetti; la seconda è dove guardano altri oggetti
    Vector2 raycastOrigin;
    int collisionLayer;
    float raycastLength;
    protected Bounds verticalColliderBounds;

    //int count = Physics2D.RaycastNonAlloc(raycastOrigin, Vector2.right, hit, 2 * raycastLength, characterAvoiderCF.layerMask);
    RaycastHit2D raycastHit;
    Vector2 exitObjectPosition = Vector2.zero;
    float exitMinDistance;
    protected bool colliderIsInside;
    RaycastHit2D[] hit = new RaycastHit2D[40];
    Rigidbody2D rb;
    //string otherLabel;
    int otherLayer;
    string objectLabel;
    int objectLayer;
    float angMinDistance;
    //Vector2 newoffset = new Vector2();
    //Vector2 originalOffset;
    int ignoringCharactersLayerIndex, currentCharacterLayer;// characterExiter0LayerIndex, characterExiter1LayerIndex,
    public bool hasGroundUnder;
    public bool descendingPlatform;
    public float slopeAngle;
    public MyCollider[] myColliders;
    //public CapsuleCollider2D verticalCollider, utilityCollider;

    public Collider2D verticalCollider, utilityCollider;
    protected float collidersOffset = 0.3f; //distanza tra i due collider usati per le collisioni; il collider piccolo deve castare fin dove arriva quello grande
    bool firstVertical;
    public float aerialMomentum;
    [HideInInspector]
    public float maxAerialMomentum;
    public float defaultMaxAerialMomentum = 6.6f;
    public float aerialMomentumDeceleration;
    public float aerialMomentumAcceleration;
    [HideInInspector]
    public Transform tr;
    public GenericWallMover movingPlatform;

    //int characterLayerIndex;
    protected struct CapsuleColliderInfo
    {
        public float r;
        public float cylinderHeight;
        public Vector2 upperCenter;
        public Vector2 lowerCenter;

    }

    [System.Serializable]
    public struct MyCollider
    {
        public Collider2D physicalCollider;
        public Collider2D exitCollider;
    }

    protected CapsuleColliderInfo capsuleColliderInfo;

    protected override void PhysicsInitialization()
    {
        
        //base.PhysicsInitialization();
        tr = GetComponent<Transform>();
        rb = GetComponent<Rigidbody2D>();


        /*
        collidersOffset = Mathf.Abs(verticalCollider.bounds.center.y - verticalCollider.bounds.extents.y
        - (utilityCollider.bounds.center.y - utilityCollider.bounds.extents.y));
        
        newoffset = verticalCollider.offset;
        originalOffset = new Vector2(verticalCollider.offset.x, verticalCollider.offset.y);
        

        capsuleColliderInfo.r = verticalCollider.bounds.extents.x;
        capsuleColliderInfo.cylinderHeight = verticalCollider.bounds.size.y - verticalCollider.bounds.size.x;
        capsuleColliderInfo.lowerCenter = new Vector2(0, -capsuleColliderInfo.cylinderHeight / 2);
        capsuleColliderInfo.upperCenter = new Vector2(0, capsuleColliderInfo.cylinderHeight / 2);
        */


        //print("r = " + capsuleColliderInfo.r + " height = " + capsuleColliderInfo.cylinderHeight);
        //print("lowerCenter: " + capsuleColliderInfo.lowerCenter);

        
        ignoringCharactersLayerIndex = LayerMask.NameToLayer("Ignore Raycast");
        
        

        
        if (isCharacter)
        {
            //horizontalExitCollider.gameObject.layer = currentCharacterExiterLayerIndex;
            switch (mystatus.faction)
            {
                case 0:
                    {
                        currentCharacterLayer = character0LayerIndex;
                        for (int i = 0; i < myColliders.Length; i++)
                        {
                            if (myColliders[i].physicalCollider != null)
                            {
                                myColliders[i].physicalCollider.gameObject.layer = character0LayerIndex;

                            }
                            if (myColliders[i].exitCollider != null)
                            {
                                //myColliders[i].exitCollider.gameObject.tag = "faction0";
                                
                                myColliders[i].exitCollider.enabled = true;
                                myColliders[i].exitCollider.gameObject.layer = currentCharacterExiterLayerIndex;

                            }

                        }

                        gameObject.layer = character0LayerIndex;
                        verticalCollider.gameObject.layer = character0LayerIndex;
                        //horizontalExitCollider.gameObject.tag = "faction0";
                        //fastHorizontalExitCollider.gameObject.tag = "faction0";

                        break;
                    }

                case 1:
                    {
                        currentCharacterLayer = character1LayerIndex;

                        for (int i = 0; i < myColliders.Length; i++)
                        {
                            if (myColliders[i].physicalCollider != null)
                            {
                                myColliders[i].physicalCollider.gameObject.layer = character1LayerIndex;

                            }
                            if (myColliders[i].exitCollider != null)
                            {
                                //myColliders[i].exitCollider.gameObject.tag = "faction1";
                                myColliders[i].exitCollider.enabled = true;
                                myColliders[i].exitCollider.gameObject.layer = currentCharacterExiterLayerIndex;

                            }

                        }

                        gameObject.layer = character1LayerIndex;
                        verticalCollider.gameObject.layer = character1LayerIndex;
                        //horizontalExitCollider.gameObject.tag = "faction1";
                        //fastHorizontalExitCollider.gameObject.tag = "faction1";
                        break;
                    }

                default:
                    {
                        currentCharacterLayer = character2LayerIndex;

                        for (int i = 0; i < myColliders.Length; i++)
                        {
                            if (myColliders[i].physicalCollider != null)
                            {
                                myColliders[i].physicalCollider.gameObject.layer = character2LayerIndex;

                            }
                            if (myColliders[i].exitCollider != null)
                            {
                                //myColliders[i].exitCollider.gameObject.tag = "faction2";
                                myColliders[i].exitCollider.gameObject.layer = currentCharacterExiterLayerIndex;

                                myColliders[i].exitCollider.enabled = true;
                            }

                        }

                        gameObject.layer = character2LayerIndex;
                        verticalCollider.gameObject.layer = character2LayerIndex;
                        //horizontalExitCollider.gameObject.tag = "faction2";
                        //fastHorizontalExitCollider.gameObject.tag = "faction2";
                        break;
                    }
            }
            UpdateLayer();
        }

    }

    protected override void ExitCharacters(ref Vector2 velocity)
    {


        /*
        int count = horizontalExitCollider.OverlapCollider(characterAvoiderCF, hitBuffer);
        Vector3 movement;
        //print ("sono " + gameObject + " " + count);
        for (int i = 0; i < count; i++)
        {
            
            //if ((otherLayer == "CharacterAvoider" && !avoidsCharacters)) {
            if ((!avoidingCharacters))
            {
                //la tag attaccata all'object rappresenta la fazione, senza ricorrere a getcomponent
                bool sameFaction = hitBuffer[i].gameObject.CompareTag(horizontalExitCollider.tag);

                //Controller2D otherPhysics = hitBuffer[i].gameObject.GetComponentInParent<Controller2D>();
                if (isCharacter)
                {

                    //if (!otherPhysics.avoidsCharacters && otherPhysics.mystatus.faction != mystatus.faction) 
                    if (!sameFaction)
                    {
                        //movement = new Vector3(Mathf.Sign (horizontalExitCollider.bounds.center.x - hitBuffer [i].bounds.center.x) * exitVelocity, 0);
                        movement = new Vector3(Mathf.Sign(gameObject.transform.position.x - hitBuffer[i].gameObject.transform.position.x) * exitVelocity * Time.fixedDeltaTime, 0);
                        //print ("sono " + gameObject + " " + (gameObject.transform.position.x - hitBuffer [i].gameObject.transform.position.x));
                        //print (movement);

                        if (Mathf.Abs(gameObject.transform.position.x - hitBuffer[i].gameObject.transform.position.x) <= 0.01f)
                        {
                            float randomValue = Mathf.Sign(Random.value - 0.5f);
                            //print ("sono " + gameObject + " " + randomValue);
                            movement.x = randomValue * (exitVelocity * Time.fixedDeltaTime);
                            //print ("CIao");
                        }
                        //movement.x = - 30;
                        velocity.x += movement.x;
                        isExiting = true;
                    }
                }
                else //if (!otherPhysics.avoidsCharacters)
                {
                    //transform.Translate (new Vector2 (Mathf.Sign ((myCollider.bounds.center.x - hitBuffer [i].bounds.center.x)) * 0.01f, 0));
                    //movement = new Vector3(Mathf.Sign (horizontalExitCollider.bounds.center.x - hitBuffer [i].bounds.center.x) * exitVelocity, 0);
                    movement = new Vector3(Mathf.Sign(gameObject.transform.position.x - hitBuffer[i].gameObject.transform.position.x) * exitVelocity * Time.fixedDeltaTime, 0);

                    //print (movement);

                    if (Mathf.Abs(gameObject.transform.position.x - hitBuffer[i].gameObject.transform.position.x) <= 0.1f)
                    {
                        movement.x = Mathf.Sign(Random.value - 0.5f) * (exitVelocity * Time.fixedDeltaTime);
                        print("CIOA");
                    }
                    //movement.x = - 30;
                    velocity.x += movement.x;
                    isExiting = true;

                }
            }
        }
        */

        //ATTENZIONE: DA TESTARE
        int count = verticalCollider.Cast(Vector2.zero, characterAvoiderCF, hit);
        Vector3 movement;

        //for (int i = 0; i < count; i++)
        int i = 0; //da testare

        if (count > 0)
        {
            //print ("Bau");

            //string otherLayer = LayerMask.LayerToName (hitBuffer[i].gameObject.layer);


            //if ((otherLayer == "CharacterAvoider" && !avoidsCharacters)) {
            if ((!avoidingCharacters))
            {

                //bool sameFaction = hitBuffer[i].gameObject.CompareTag(horizontalExitCollider.tag);
                if (isCharacter)
                {

                    //if (!sameFaction)
                    {
                        float dist = (tr.position.x - hit[i].transform.position.x);

                        float thisExitVelocity = Mathf.Sign(dist) * (Mathf.Abs(dist) < 2f ? fastExitVelocity : exitVelocity) * Time.fixedDeltaTime / TimeVariables.currentTimeScale;
                        //print("sto uscendo: " + thisExitVelocity + " " + fastExitVelocity + " " + exitVelocity);
                        //movement = new Vector3(Mathf.Sign(dist) * fastExitVelocity * Time.fixedDeltaTime, 0);
                        
                        if (Mathf.Abs(dist) <= 0.2f)//0.01f)
                        {
                            dist = Mathf.Sign(Random.value - 0.5f);
                            thisExitVelocity *= dist;
                            //print ("sono " + gameObject + " " + randomValue);
                            //movement.x = randomValue * (fastExitVelocity);
                            //print ("CIao");
                        }
                        //movement.x = - 30;
                        velocity.x = thisExitVelocity;
                        isExiting = true;
                    }
                }
                else// if (!otherPhysics.avoidsCharacters)
                {
                    //transform.Translate (new Vector2 (Mathf.Sign ((myCollider.bounds.center.x - hitBuffer [i].bounds.center.x)) * 0.01f, 0));
                    //movement = new Vector3(Mathf.Sign (horizontalExitCollider.bounds.center.x - hitBuffer [i].bounds.center.x) * exitVelocity, 0);
                    movement = new Vector3(Mathf.Sign(gameObject.transform.position.x - hit[i].collider.gameObject.transform.position.x) * fastExitVelocity * Time.fixedDeltaTime / TimeVariables.currentTimeScale, 0);

                    //print (movement);

                    if (Mathf.Abs(gameObject.transform.position.x - hit[i].collider.gameObject.transform.position.x) <= 0.1f)
                    {
                        movement.x = Mathf.Sign(Random.value - 0.5f) * (fastExitVelocity * Time.fixedDeltaTime / TimeVariables.currentTimeScale);
                        //print ("CIOA");
                    }
                    //movement.x = - 30;
                    velocity.x += movement.x;
                    isExiting = true;

                }
            }
        }



    }

    protected void RaycastExitCharacters (ref Vector2 velocity)
    {

        //ATTENZIONE: DA TESTARE
        //verticalColliderBounds = verticalCollider.bounds;
        raycastOrigin = new Vector2(verticalColliderBounds.center.x - capsuleColliderInfo.r - skinWidth - characterAvoidSkinwidth, verticalColliderBounds.min.y + 1);
        raycastLength = Mathf.Abs(verticalColliderBounds.center.x - raycastOrigin.x);
        //int count = Physics2D.RaycastNonAlloc(raycastOrigin, Vector2.right, hit, 2 * raycastLength, characterAvoiderCF.layerMask);
        //raycastHit = Physics2D.Raycast(raycastOrigin, Vector2.right, 2 * raycastLength, characterAvoiderCF.layerMask);

        //Debug.DrawRay(raycastOrigin, new Vector2(2 * raycastLength, 0), Color.yellow);
        //Vector3 movement;

        //for (int i = 0; i < count; i++)
        //int i = 0; //da testare

        //if (count > 0)
        if (raycastHit = Physics2D.Raycast(raycastOrigin, Vector2.right, 2 * raycastLength, characterAvoiderCF.layerMask))
        {
            //print ("Bau");

            //string otherLayer = LayerMask.LayerToName (hitBuffer[i].gameObject.layer);


            //if ((otherLayer == "CharacterAvoider" && !avoidsCharacters)) {
            //if ((!avoidingCharacters))
            {

                //bool sameFaction = hitBuffer[i].gameObject.CompareTag(horizontalExitCollider.tag);
                
                {

                    //if (!sameFaction)
                    {
                        float dist = (tr.position.x - raycastHit.transform.position.x);
                        //bool veryInside = Mathf.Abs(dist) < capsuleColliderInfo.r + skinWidth;
                        //float thisExitVelocity = Mathf.Sign(dist) * (veryInside ? fastExitVelocity : exitVelocity) * Time.fixedDeltaTime;
                        //inizio test
                        float imontheright = dist > 0 ? 1 : -1;
                        
                        if (imontheright == 1)
                        {
                            dist = verticalCollider.bounds.min.x - raycastHit.collider.bounds.max.x;
                            //print("im right, " + dist);
                        }
                        else
                        {
                            dist = raycastHit.collider.bounds.min.x - verticalCollider.bounds.max.x;
                            //print("im left, " + dist);
                        }

                        float thisExitVelocity = (raycastLength - verticalColliderBounds.extents.x - dist) * (imontheright);// * Time.fixedDeltaTime;
                        //float signOfVelocity = Mathf.Sign(velocity.x);
                        //float signOfThisExitVelocity = Mathf.Sign(thisExitVelocity);
                        //print("velocity: " + velocity.x + " " + thisExitVelocity);

                        /*
                        if (((imontheright && signOfVelocity != signOfThisExitVelocity) ||
                            (!imontheright && (signOfVelocity == signOfThisExitVelocity || thisExitVelocity != 0))) //artificio del cavolo perchè il segno di 0 è +
                            || Mathf.Abs(velocity.x) <= Mathf.Abs(thisExitVelocity))
                        */
                        if (imontheright * velocity.x <= 0)
                        {
                            //print("1111");
                            velocity.x = thisExitVelocity;
                        }
                        else
                        {
                            velocity.x += thisExitVelocity;
                            //print("2222");
                        }
                        //print("vel: " + velocity.x + " " + (raycastLength - verticalColliderBounds.extents.x));
                        //fine test

                        /* previously on dbz
                        float thisExitVelocity = Mathf.Sign(dist) * exitVelocity * Time.fixedDeltaTime;
                        
                        if (Mathf.Sign(velocity.x) != Mathf.Sign(thisExitVelocity) || Mathf.Abs(velocity.x) < Mathf.Abs(thisExitVelocity))
                            velocity.x = thisExitVelocity;
                        */
                    }
                }
                
            }
        }



    }


    protected override void Move(Vector2 velocity)
   {
        
        UpdateRayCastOrigins();
        collisions.Reset();
        slopeAngle = 0;
        /*
        if (avoidingCharacters != previousAvoidingCharacters)
        {
            //print("aaaaaa");
            UpdateLayer();
        }
        

        previousAvoidingCharacters = avoidingCharacters;
        */

        colliderIsInside = false;
        //ComputeAngle(ref velocity, hit);

        isExiting = false;
        hasGroundUnder = false;
        
        UtilityColliderComputeAngle(ref velocity, hit);
        verticalColliderBounds = verticalCollider.bounds;

        if (exitsCharacters && getsPushedByCharacters && !isExiting && !inHitstop && !avoidingCharacters)
        {

            //ExitCharacters(ref velocity);
            RaycastExitCharacters(ref velocity);

        }


        //if (colliderIsInside)
        //  ExitColliders();

        //GenericWallMover movingPlatform = null;
        if (!simplePhysics)
        {
            //if ((objectUnderMe != null && objectUnderMe.tag == "moving platform") || (climbedPlatform != null && climbedPlatform.tag == "moving platform"))
            if ((objectUnderMe != null && velocity.y <= 0 && objectUnderMe.gameObject.layer == movingPlatformLayerIndex) ||
                (climbedPlatform != null && climbedPlatform.gameObject.layer == movingPlatformLayerIndex)
                || movingPlatform != null)

            {
                if (movingPlatform == null)
                {
                    if (climbedPlatform == null)
                        movingPlatform = objectUnderMe.GetComponent<GenericWallMover>();
                    else
                        movingPlatform = climbedPlatform.GetComponent<GenericWallMover>();
                }

                //print("movingplatform: " + movingPlatform);
                if (movingPlatform != null)
                {
                    //print("under: " + movingPlatform);

                    //if (velocity.y <= 0)
                    {
                        //print("Ci muovevamoooooo: " + aerialMomentum + " " + base.velocity.x);
                        velocity.x += movingPlatform.totVelocity.x * (Time.fixedDeltaTime);


                        if (Mathf.Sign(velocity.x) == Mathf.Sign(movingPlatform.totVelocity.x))
                            aerialMomentum = (movingPlatform.totVelocity.x) * TimeVariables.currentTimeScale;
                        //print("ci muoviamo: " + aerialMomentum + " " + base.velocity.x);
                    }
                    //tempVelocity.y += movingPlatform.totVelocity.y;

                    //if (movingPlatform.totVelocity.y > 0)// && tempVelocity.y < movingPlatform.totVelocity.y)
                    {
                        //velocity.y += movingPlatform.totVelocity.y * Time.fixedDeltaTime;
                        //if (climbedPlatform != null)
                        {
                            //print("climbing");
                            tr.Translate(0, movingPlatform.totVelocity.y * (Time.timeScale == 1 ?
                                Time.fixedDeltaTime : Time.deltaTime), 0);
                            //Physics2D.SyncTransforms();

                        }
                        if (movingPlatform.totVelocity.y > 0 && (velocity.y > 0 || (inHitstop && mystatus.mystatus == Status.status.juggle)))
                            velocity.y += movingPlatform.totVelocity.y * (Time.timeScale == 1 ? Time.fixedDeltaTime : Time.deltaTime); //DA TESTARE


                        //UpdateRayCastOrigins();
                        //collisions.Reset();
                        //print("c'è qualcuno?");

                    }
                    /*
                    else
                    {
                        if (velocity.y <= 0)
                        {
                            velocity.y += movingPlatform.totVelocity.y * (Time.timeScale == 1 ? Time.fixedDeltaTime : Time.deltaTime);
                            //print("vado giu");
                        }
                    }
                    */
                    //print("velocità dopo: " + velocity.y / Time.fixedDeltaTime);

                }

            }
            
        }
        //else
        {
            //movingPlatform = null;
        }
        

        collisions.velocityOld = velocity;

        Vector2 tempVel = new Vector3(velocity.x, velocity.y);
        
        firstVertical = true;
        //TESTColliderCastHorizontalCollisions(ref tempVelocity);
        if (!simplePhysics)
            ColliderCastHorizontalCollisions(ref tempVel);
        else
            SimpleColliderCastHorizontalCollisions(ref tempVel);

        if (!simplePhysics)
            SingleCastVerticalCollisions(ref tempVel);
        else
            SimpleSingleCastVerticalCollisions(ref tempVel);

        movingPlatform = null;
        descendingPlatform = false;
        /*
        CollisionInfo tempCollisions = collisions;
        collisions.Reset(); // attenzione Da testare!!!

        firstVertical = false;

        
        if (grounded && velocity.y <= 0)
        {
            //print("GIUUUU");
            //Vector2 newVelocity = new Vector2(0, - Mathf.Abs(tempVelocity.y));
            //SingleCastVerticalCollisions(ref newVelocity);
        }
        
        collisions = tempCollisions;
        */

        //ATTENZIONE, DA TESTARE SENZA
        //ExitColliders();
        
        if (collisions.below)// || grounded)
        {

            //print("zero");
            if (!collisions.slidingDownMaxSlope && climbedPlatform == null)
            {
                //print("zero");
                base.velocity.y = 0;
                velocity.y = 0;
            }
        }
        //if (mystatus.faction == 0)

          //  print("222 " + velocity.y);

        if (movingParts != null)
        {
            movingParts.position = tr.position;
            movingParts.localScale = tr.localScale;
            
            lineOfSightOrigin.x = verticalColliderBounds.center.x;
            lineOfSightOrigin.y = verticalColliderBounds.max.y - 0.2f;
            lineOfSightTarget.x = verticalColliderBounds.center.x;
            lineOfSightTarget.y = verticalColliderBounds.min.y + 0.2f;

            //Debug.DrawRay(lineOfSightOrigin, 100 * Vector2.right, Color.blue);
            //Debug.DrawRay(lineOfSightTarget, 100 * Vector2.right, Color.blue);

        }

        //Vector2 translation = new Vector2(verticalCollider.offset.x - originalOffset.x, verticalCollider.offset.y - originalOffset.y);

        //tr.Translate(translation);
        //rb.MovePosition(translation);
        //print("fine");

        //verticalCollider.offset = originalOffset;

        //print("fine: " + velocity.y);

    }

    void ColliderCastHorizontalCollisions(ref Vector2 velocity)
    {
        //velocity.x = Mathf.Cos(slopeAngle) * velocity.x;

        //RaycastHit2D[] hit = new RaycastHit2D[30];
        collisionLeft = false;
        collisionRight = false;
        
        //if (climbedPlatform == null)
        {
            if (velocity.y <= 0 || (climbedPlatform != null && velocity.x != 0))
            {
                //ComputeAngle(ref velocity, hit);
                
                CalculateAngularVelocity(angMinDistance, ref velocity, objectLayer);
            }
            else if (checksUpperAngle && grounded == false)
            {
                //if (mystatus.faction == 0)
                    //print("SUUUU");
                ComputeUpperAngle(ref velocity, hit);
            }
            
        }
        if (avoidingCharacters != previousAvoidingCharacters)
        {
            //print("aaaaaa");
            UpdateLayer();
        }
        previousAvoidingCharacters = avoidingCharacters;

        //if (velocity.x == 0)
        //  return;
        float minDistance;
        if (getsPushedByCharacters)
            minDistance = Mathf.Abs(velocity.x) + skinWidth + characterAvoidSkinwidth;
        else
            minDistance = Mathf.Abs(velocity.x) + skinWidth;
        //float slopeAngle = 0;
        //int count = 0;
        acceptableAngle = false;

        //if (!avoidingCharacters)
        if (mystatus.faction == 0 && velocity.x == 0)
        {
            //print("OOOOO " + (velocity.x != 0 ? Mathf.Sign(velocity.x) : Mathf.Sign(tr.lossyScale.x)) + " "+Time.frameCount);
        }
         
        int count = verticalCollider.Cast(Vector2.right * ((velocity.x != 0 ? Mathf.Sign(velocity.x) : Mathf.Sign(tr.localScale.x))), horizontalCF, hit, 
            minDistance);
        //else
        //count = verticalCollider.Cast(Vector2.right * Mathf.Sign(velocity.x), upwardCF, hit, minDistance);

        //count = verticalCollider.Cast(Vector2.zero, horizontalCF, hit, minDistance);
        //Debug.DrawRay(new Vector3(tr.position.x, tr.position.y -1.5f), new Vector3(3*velocity.x, 3*velocity.y, 0), Color.yellow);
        //Debug.DrawRay(tr.position, Vector3.up * 100, Color.yellow);

        //bool hasHitHor = false;
        hasHitWall = false;
        //exitCharactersVelocity = 0;
        if (count > 0)
        {
            if (mystatus.faction == 0)
            {
                //print("count: " + count + " " + Time.frameCount);
            }
            for (int i = 0; i < count; i++)
            {

                if (hit[i].distance == 0)
                {
                    //print("trooooppo dentro " + Time.frameCount);
                    continue;

                }
                acceptableAngle = !grounded || Vector2.Angle(hit[i].normal, Vector2.up) > maxSlopeAngle;
                if (hit[i].distance < Mathf.Abs(velocity.x) + skinWidth)
                {
                    hasHitWall = true;
                    //if (getsPushedByCharacters)
                      //  print("WEEEEEEEEEEEEEE");
                }
                //if (!grounded || Vector2.Angle(hit[i].normal, Vector2.up) > maxSlopeAngle)
                {
                    if (getsPushedByCharacters)
                    {
                        collisionLayer = hit[i].collider.gameObject.layer;
                        {
                            if (collisionLayer != character0LayerIndex && collisionLayer != character1LayerIndex && collisionLayer != character2LayerIndex)
                            {
                                if (hit[i].distance >= Mathf.Abs(velocity.x) + skinWidth || !acceptableAngle)
                                {
                                    continue;
                                }
                                else
                                {
                                    hasHitWall = true;
                                    if (velocity.x > 0)
                                        collisionRight = true;
                                    else if (velocity.x < 0)
                                        collisionLeft = true;
                                }

                            }
                            else
                            {
                                //print("WEE");

                                
                                /*
                                exitCharactersVelocity = - minDistance + hit[i].distance;

                                exitCharactersVelocity *= (velocity.x != 0 ? Mathf.Sign(velocity.x) : Mathf.Sign(tr.localScale.x));
                                */
                                //continue;
                            }

                        }

                    }


                    if (hit[i].distance <= minDistance)
                    {
                        if (getsPushedByCharacters || acceptableAngle)
                        {
                            //print("HHHHH");

                            Debug.DrawRay(hit[i].point, hit[i].normal, Color.blue);
                            
                            minDistance = hit[i].distance;
                            /*
                            if (mystatus.faction == 0)
                            {
                                print("hor: " + minDistance + " " + Time.frameCount);
                            }
                            */

                            //hasHitHor = true;
                            if (collisions.climbingSlope)
                            {
                                velocity.y = -Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * (minDistance - skinWidth);
                                //print("newvel: " + velocity.y);
                            }
                        }
                    }
                }
            }
        }

        float newDistance;
        if (getsPushedByCharacters)
        {
            //newDistance = minDistance - skinWidth - (!hasHitWall ? characterAvoidSkinwidth : 0);
            newDistance = minDistance - skinWidth -  characterAvoidSkinwidth; //da testare

        }
        else
            newDistance = minDistance - skinWidth;


        //if (mystatus.faction == 0)
        //  print("vel: " + newDistance + " " + Time.frameCount);
        //Debug.DrawRay(new Vector3(tr.position.x, tr.position.y - 1.7f), new Vector3(30 * velocity.x, 30 * velocity.y, 0), Color.red);

        //azzero spostamenti negativi, altrimenti fa uno spostamento non testato e clippa nei muri
        if (newDistance < 0)
            newDistance = 0;

        

        velocity.x = (velocity.x == 0 ? Mathf.Sign(tr.localScale.x) : Mathf.Sign(velocity.x)) * newDistance;

        

        Debug.DrawRay(new Vector3(tr.position.x, tr.position.y - 1.5f), new Vector3(30 * velocity.x, 30 * velocity.y, 0), Color.yellow);



        if (velocity.x != 0)
        {
            tr.Translate(velocity.x, 0, 0);
            //rb.MovePosition(new Vector2 (tr.position.x + velocity.x, 0));
            
            Physics2D.SyncTransforms();
        }



    }

    void OLDColliderCastHorizontalCollisions (ref Vector2 velocity)
    {
        //velocity.x = Mathf.Cos(slopeAngle) * velocity.x;

        //RaycastHit2D[] hit = new RaycastHit2D[30];
        print("hor1: " + velocity.x);
        //if (climbedPlatform == null)
        {
            if (velocity.y < 0 || climbedPlatform != null)
            {
                //ComputeAngle(ref velocity, hit);
                //if (mystatus.faction == 0)
                //print("giuuuuu " + Time.renderedFrameCount);
                CalculateAngularVelocity(angMinDistance, ref velocity, objectLayer);
            }
            else if (checksUpperAngle && grounded == false)
            {
                //print("SUUUU");
                ComputeUpperAngle(ref velocity, hit);
            }
            print("hor2: " + velocity.x);


        }
        if (velocity.x == 0)
            return;
        //ATTENZIONE, DA TESTARE SENZA UPPERANGLE
        if (velocity.y > 0 && (grounded == false || climbedPlatform != null))
        {
            //ComputeUpperAngle(ref velocity, hit);
            //slopeAngle = 0; //da testare!
        }
        float minDistance = Mathf.Abs(velocity.x) + skinWidth;
        //float slopeAngle = 0;
        int count = 0;

        
        //if (!avoidingCharacters)
        count = verticalCollider.Cast(Vector2.right * Mathf.Sign(velocity.x), horizontalCF, hit, minDistance);
        //else
        //count = verticalCollider.Cast(Vector2.right * Mathf.Sign(velocity.x), upwardCF, hit, minDistance);

        //count = verticalCollider.Cast(Vector2.zero, horizontalCF, hit, minDistance);

        //bool hasHitHor = false;
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                if (hit[i].distance == 0)
                    continue;


                //bool otherIsCharacter = hit[i].collider.gameObject.layer == characterLayerIndex ? true : false;
                //bool otherIsCharacter = false;

                /*
                bool otherIsCharacter = hit[i].collider.CompareTag("Character");


                bool zulul = false;
                bool otherAvoidsCharacters = false;
                
                if (isCharacter)
                {
                    if (otherIsCharacter)
                    {
                        print("otherischaracter");
                        CustomPhysics otherPhysics = hit[i].collider.gameObject.GetComponentInParent<CustomPhysics>();

                        if (otherPhysics != null)
                            otherAvoidsCharacters = otherPhysics.avoidsCharacters;
                        else
                            otherAvoidsCharacters = false;

                        if (!avoidsCharacters && !otherAvoidsCharacters)// && otherPhysics.mystatus.faction != mystatus.faction)
                        {
                            zulul = true;
                            //print ("frame: " + Time.frameCount + " 1");
                            //if (isExiting && hit[i].distance < 0.01f)
                              //  zulul = false;
                            //if (mystatus.faction == 1)
                            //print ("1: " + zulul);
                        }
                    }
                    else
                    {
                        if (!otherAvoidsCharacters)
                        {
                            zulul = true;
                            //print ("frame: " + Time.frameCount + " 2");
                            //if (mystatus.faction == 1)

                              //  print("2: " + zulul);
                        }

                    }
                }
                else
                {
                    if (!otherIsCharacter)
                        zulul = true;
                    else
                    {
                        if (!avoidsCharacters)
                            zulul = true;
                    }
                }
                */

                //if (mystatus.faction == 1)
                //print("zulul: " + zulul);

                //if (zulul && hit[i].distance <= minDistance)
                if (hit[i].distance <= minDistance)

                {
                    //print("HHHHH");
                    if ((!grounded || Vector2.Angle(hit[i].normal, Vector2.up) > maxSlopeAngle))
                    {
                        //slopeAngle = Vector2.Angle(hit[i].normal, Vector2.up) * Mathf.Deg2Rad;
                        //print("HHHHH");

                        Debug.DrawRay(hit[i].point, hit[i].normal, Color.blue);
                        //if (mystatus.faction == 1)
                        minDistance = hit[i].distance;
                        //hasHitHor = true;
                        if (collisions.climbingSlope)
                        {
                            velocity.y = -Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * (minDistance - skinWidth);
                            //print("newvel: " + velocity.y);
                        }       
                    }
                }
            }
        }

        float newDistance = minDistance - skinWidth;
        //print("hor: " + newDistance);
        if (newDistance < 0)
            newDistance = 0;
        velocity.x = Mathf.Sign(velocity.x) * newDistance;
        print("hor3: " + velocity.x);



        //if (hasHitHor && ((collisions.descendingSlope && velocity.y <= 0) || collisions.slidingDownMaxSlope))
        //if (hasHitHor && collisions.slidingDownMaxSlope)
        {
            //transform.Translate(0, (Mathf.Abs(velocity.y) + skinWidth), 0);
            //print("vai sopra");
        }

        //ATTENZIONE, DA TESTARE
        //newoffset = verticalCollider.offset;
        //newoffset.x += velocity.x;
        //verticalCollider.offset = newoffset;
        if (velocity.x != 0)
        {
            tr.Translate(velocity.x, 0, 0);
            Physics2D.SyncTransforms();
        }



    }

    protected override void CheckPlatformCollision()
    {

        //ATTENZIONE, DA TESTARE
        //newoffset = verticalCollider.offset;
        //newoffset.y += collidersOffset;
        //verticalCollider.offset = newoffset;
        //int counter = verticalCollider.Cast(Vector2.zero, platformCheckerCF, hit);
        int counter = utilityCollider.Cast(Vector2.zero, platformCheckerCF, hit);

        //newoffset.y -= collidersOffset;
        //verticalCollider.offset = newoffset;

        for (int i = 0; i < counter; i++)
        {
            //if (hit[i].collider.gameObject.layer == platformLayerIndex)
            {
                platformsHit.Add(hit[i].collider);
                //print("added");
            }
            //if (hit[i].collider.gameObject.tag == "Stairs")
              //  stairsHit.Add(hit[i].collider);
        }
        //return false;
    }
    

    void ExitColliders ()
    {
        if (climbedPlatform != null)
            return;
        //if (objectHit != -1)
        
            
            //ATTENZIONE, DA TESTARE
        //newoffset = verticalCollider.offset;
          //  newoffset.y += collidersOffset;
            //verticalCollider.offset = newoffset;
            //int result = verticalCollider.Cast(-Vector2.up, groundCF, hit, collidersOffset);
        int result = utilityCollider.Cast(-Vector2.up, groundCF, hit, collidersOffset);
        //newoffset.y -= collidersOffset;
        //verticalCollider.offset = newoffset;

        float minDistance = collidersOffset;
            Vector2 objectPosition = new Vector2();
            //print("result: " + result);
            //Debug.DrawRay(horizontalCollider.bounds.min, 10 * Vector3.right);
            if (result > 0) //&& hit[0].distance > 0)
            {
               
                //print("exit");
                for (int i = 0; i < result; i++)
                {
                    if (hit[i].distance == 0)
                        continue;

                    //string otherLabel = hit[i].collider.tag;
                    if (hit[i].collider.gameObject.layer == platformLayerIndex || hit[i].collider.gameObject.layer == movingPlatformLayerIndex)
                    {
                        float objectAngle = Vector2.Angle(hit[i].normal, Vector2.up);
                        if (objectAngle > maxSlopeAngle)
                            continue;
                        //print ("porattoforumu");
                        if (collisions.isIgnoringPlatforms > 0 || (platformsHit.Contains(hit[i].collider) || !grounded))
                            continue;
                    }
                    /*
                    if (collisions.isIgnoringStairs > 0 && otherLabel == "Stairs")
                    {
                        continue;
                    }
                    */
                    

                    if (hit[i].distance < minDistance)
                    {
                        minDistance = hit[i].distance;
                        objectPosition = hit[i].point;
                    }
                    
                }
                if (minDistance < collidersOffset)
                {
                    //transform.Translate(new Vector3(0, velocity.y >= 0 ? (minDistance - skinWidth) : - (minDistance - skinWidth), 0));
                    float translationDistance = 0;
                //Ray ray = new Ray(hit[0].point, new Vector3(0, 1, 0));
                Debug.DrawRay(objectPosition, Vector2.down, Color.yellow, 100);

                    Vector2 hitPointRelativeToCollider = new Vector2(-verticalCollider.bounds.center.x + objectPosition.x,
                        -verticalCollider.bounds.center.y + objectPosition.y);

                    //float distanceRelativeCollider = Vector2.Distance(verticalCollider.bounds.center, hit[0].point);

                    //print("hit relative: " + hitPointRelativeToCollider.x + " " + hitPointRelativeToCollider.y);
                    float destinationPoint;

                    //x^2 + x0^2 - 2*x*x0 + y^2 + y0^2 - 2*y*y0 = r^2   ==>   
                    float x = hitPointRelativeToCollider.x;
                    float x0 = capsuleColliderInfo.lowerCenter.x;
                    float y0 = capsuleColliderInfo.lowerCenter.y;
                    float r = capsuleColliderInfo.r;
                    float c = x * x + x0 * x0 - 2 * x * x0 + y0 * y0 - r * r;
                    float b = -2 * y0;
                    float y = (-b - Mathf.Sqrt((b * b) - (4 * c))) / 2;

                    destinationPoint = y;



                    //destinationPoint^2 + hitPointRelativeToCollider.x^2 + capsuleColliderInfo.lowerCenter.x ^2 + capsuleColliderInfo.lowerCenter.y ^2
                    //- 2*hitPointRelativeToCollider.x*capsuleColliderInfo.lowerCenter.x - 2*destinationPoint*capsuleColliderInfo.lowerCenter.y
                    //= capsuleColliderInfo.r^2

                    //destinationPoint = Mathf.Sqrt(4);
                    //print("destinationPoint: " + destinationPoint);

                    /*
                    Debug.DrawRay(new Vector2(verticalCollider.bounds.center.x + hitPointRelativeToCollider.x, verticalCollider.bounds.center.y + hitPointRelativeToCollider.y),
                        1000 * Vector2.right, Color.blue);
                    Debug.DrawRay(new Vector2(verticalCollider.bounds.center.x + hitPointRelativeToCollider.x, verticalCollider.bounds.center.y + hitPointRelativeToCollider.y),
                        1000 * Vector2.up, Color.blue);
                    */

                    //Debug.DrawRay(hit[0].point, Vector2.right, Color.red, 10);
                    //Debug.DrawRay(hit[0].point, 100*Vector2.up, Color.red, 10);

                    //if (verticalCollider.bounds.IntersectRay(ray, out translationDistance))
                    {
                        //Debug.DrawRay(hit[0].point, -Vector2.up * translationDistance, Color.green);

                        //translationDistance = verticalCollider.bounds.size.y /2 - hit[0].distance + skinWidth;
                        translationDistance = -destinationPoint + hitPointRelativeToCollider.y;
                    /*
                    Debug.DrawRay(new Vector2(verticalCollider.bounds.center.x + hitPointRelativeToCollider.x, verticalCollider.bounds.center.y
                        + hitPointRelativeToCollider.y),
                    -1000 * Vector2.right, Color.green, 10);

                    Debug.DrawRay(new Vector2(verticalCollider.bounds.center.x + hitPointRelativeToCollider.x, verticalCollider.bounds.center.y
                        - destinationPoint),
                    -1000 * Vector2.right, Color.red, 10);
                    */

                    //ATTENZIONE, DA TESTARE
                    tr.Translate(new Vector3(0, translationDistance + skinWidth, 0));
                    Physics2D.SyncTransforms();

                    //newoffset = verticalCollider.offset;
                    //newoffset.y += translationDistance;
                    //verticalCollider.offset = newoffset;
                    print("sono dentro: " + translationDistance);
                    }
                }
            }
        

    }


    void SingleCastVerticalCollisions(ref Vector2 velocity)
    {
        //if (mystatus.faction == 0)
          //  print("inizio: " + velocity.y + " " + transform.parent.gameObject);
        //print("Velocity.x = " + velocity.x + " ricavo: " + (velocity.y / Mathf.Sin(Mathf.Abs(slopeAngle)) * Mathf.Cos(Mathf.Abs(slopeAngle))));
        //RaycastHit2D[] hit = new RaycastHit2D[30];
        //Debug.DrawRay(new Vector2 (verticalCollider.bounds.center.x + capsuleColliderInfo.lowerCenter.x,
          //  verticalCollider.bounds.center.y + capsuleColliderInfo.lowerCenter.y), Vector2.right, Color.red);

        //Debug.DrawRay(new Vector2(horizontalCollider.bounds.min.x, horizontalCollider.bounds.min.y + collidersOffset), 10 * Vector3.right);
        //Debug.Log("centro: " + verticalCollider.bounds.center);

        //float minDistance = myCollider.bounds.size.y / 2 + Mathf.Abs(velocity.y) + skinWidth;
        float minDistance = Mathf.Abs(velocity.y) + skinWidth;
        //print("mindistance: " + minDistance);
        //Debug.DrawRay(horizontalCollider.bounds.min, 10 * Vector3.right);

        //Debug.DrawRay(myCollider.bounds.center, Mathf.Sign(velocity.y) * Vector2.up * minDistance, Color.blue);
        //Debug.DrawRay(new Vector3(tr.position.x, tr.position.y - 1.5f), new Vector3(3 * velocity.x, 3 * velocity.y, 0), Color.yellow);

        //int count = verticalCollider.Cast(myCollider.bounds.center, velocity.y > 0 ? Vector2.up : -Vector2.up, minDistance, groundMask);
        int count;
        if (velocity.y > 0)
        {
            //ATTENZIONE, DA TESTARE
            /*
            newoffset = verticalCollider.offset;
            newoffset.y += collidersOffset;
            verticalCollider.offset = newoffset;
            Vector2 newsize = verticalCollider.size;
            newsize.y -= collidersOffset * 2;
            verticalCollider.size = newsize;
            count = verticalCollider.Cast(Vector2.up, checksPlatforms ? upwardCF : groundCF, hit, minDistance);
            */

            count = utilityCollider.Cast(Vector2.up, checksPlatforms ? upwardCF : groundCF, hit, minDistance);
            //Debug.DrawRay(new Vector2(verticalCollider.bounds.center.x, verticalCollider.bounds.max.y), Vector2.right, Color.red);

            /*
            newoffset.y -= collidersOffset;
            newsize.y += collidersOffset * 2;
            verticalCollider.size = newsize;
            verticalCollider.offset = newoffset;
            */
        }

        else
        {
            
            count = verticalCollider.Cast(-Vector2.up, groundCF, hit, minDistance);

        }

        //float slopeAngle = 0f;
        //print("count: " + collisions.climbingSlope);
        //if (mystatus.faction == 0)
        //print("ver: " + count);
        if (count > 0)
        {
            //print("ver: " + count);
            if (velocity.y > 0 || (slopeAngle != 90 && slopeAngle != -90))
            {
                for (int i = 0; i < count; i++)
                {
                    
                    if (hit[i].distance == 0)
                    {
                        
                        continue;

                    }

                    //string otherLabel = hit[i].collider.tag;
                    //string otherLayer = LayerMask.LayerToName(hit[i].collider.gameObject.layer);
                    //print("Slopeangle: " + slopeAngle);
                    if ((velocity.y > 0 && climbedPlatform == hit[i].collider) ||
                        (platformsHit.Contains(hit[i].collider) || (((hit[i].collider.gameObject.layer == platformLayerIndex
                        || hit[i].collider.gameObject.layer == movingPlatformLayerIndex) && ((Mathf.Abs(Vector2.SignedAngle(hit[i].normal, Vector2.up)) > maxSlopeAngle)
                        || collisions.isIgnoringPlatforms > 0)))))
                    //|| (collisions.isIgnoringStairs > 0 && otherLabel == "Stairs")))))
                    {

                        //print("BAU");
                        continue;

                    }
                    /*
                    if (stairsHit.Contains(hit[i].collider))
                    {
                        continue;
                    }
                    */

                    if (hit[i].distance <= minDistance)
                    {
                        //print("aloraaa");
                        minDistance = hit[i].distance;
                        if (velocity.y <= 0)
                        {// || collisions.climbingSlope)
                            collisions.below = true;
                            //print("below " + velocity.y);
                            //print(hit[i].collider.gameObject);
                            //Debug.DrawLine(hit[i].point, new Vector2(hit[i].point.x + 100, hit[i].point.y), Color.blue, 10000);
                            //Debug.DrawLine(hit[i].point, new Vector2(hit[i].point.x, hit[i].point.y + hit[i].distance), Color.blue, 10000);

                        }
                        else
                        {  // if (velocity.y > 0)
                            collisions.above = true;
                            //print("above");
                        }
                        //slopeAngle = Vector2.Angle(hit[i].normal, Vector2.up) * Mathf.Deg2Rad;
                        //print("collisions.below " + collisions.below + " climb: " + collisions.climbingSlope);
                    }
                }
            }

            //minDistance -= myCollider.bounds.size.y / 2;
            //print("mindistance: " + minDistance);

            //if (minDistance > 0)
            {

                if (velocity.y <= 0)
                {
                    velocity.y = -(minDistance - skinWidth);
                    //if (mystatus.faction == 0)
                        //print("vert: " + minDistance + " " + velocity.y);
                }
                else if (velocity.y > 0)
                {
                    //se stai andando in su e il muro è dentro la skinwidth, non ti sposta in giu ma sta fermo
                    velocity.y = Mathf.Sign(velocity.y) * Mathf.Max(minDistance - skinWidth, 0);

                }

                //velocity.y = Mathf.Sign(velocity.y) * (minDistance - skinWidth);

                //print("slope: " + slopeAngle + " collisions.below: " + collisions.below + " climbingslope: " + collisions.climbingSlope);
                //if (slopeAngle != 0 && (collisions.above || collisions.below))
                if (collisions.above || collisions.below)
                    //if (collisions.above)
                {
                    if (collisions.climbingSlope)// || !grounded)
                    //if (slopeAngle != 0 && velocity.x != 0)
                    {

                        //if (!grounded)
                        //print("climbing " + velocity.y / Time.fixedDeltaTime + " angle " + slopeAngle);
                        //float oldHorizontalVelocity = grounded ? Mathf.Abs(velocity.x) : Mathf.Abs(velocity.x);
                        // * Mathf.Cos(Mathf.Abs(slopeAngle * Mathf.Deg2Rad));
                        //float newHorizontalVelocity = (velocity.y) / Mathf.Sin(Mathf.Abs(slopeAngle * Mathf.Deg2Rad))
                        //   * Mathf.Cos(Mathf.Abs(slopeAngle * Mathf.Deg2Rad));
                        //newHorizontalVelocity = 0;
                        //transform.Translate(-Mathf.Sign(velocity.x) * (oldHorizontalVelocity - newHorizontalVelocity), 0, 0);

                        //ATTENZIONE, DA TESTARE
                        //newoffset = verticalCollider.offset;
                        //newoffset.x += -velocity.x;
                        //verticalCollider.offset = newoffset;
                        if (velocity.x != 0)
                        {
                            tr.Translate(-velocity.x, 0, 0);
                            //Physics2D.SyncTransforms();

                            //print("traslo: " + -velocity.x);
                        }
                        
                    }
                    //else
                      //  print("no climbing");
                }
                

                //print("ho toccato");
            }

          

            //faccio un cast per ricavare l'inclinazione del terreno

            //RaycastHit2D terrainResult;
            
            
            

        }



        //if (mystatus.faction == 0)
          //  print("fine vertical: " + velocity.y + " " + transform.parent.gameObject);
        //Debug.DrawLine(new Vector2(verticalCollider.bounds.center.x, verticalCollider.bounds.center.y - verticalCollider.bounds.extents.y), new Vector2(verticalCollider.bounds.center.x, verticalCollider.bounds.center.y - verticalCollider.bounds.extents.y + velocity.y), Color.green, 0.001f);



        //ATTENZIONE, DA TESTARE
        //newoffset = verticalCollider.offset;
        //newoffset.y += velocity.y;
        //verticalCollider.offset = newoffset;
        if (velocity.y != 0)
        {
            
            tr.Translate(0, velocity.y, 0);
            //Physics2D.SyncTransforms();
        }
        //-0.04158401
    }

    void ComputeUpperAngle (ref Vector2 velocity, RaycastHit2D[] hit)
    {
        bool isTouchingGround = false;
        float minDistance = Mathf.Abs(velocity.y) + 2*skinWidth;

        int terrainResult = utilityCollider.Cast(Vector2.up, checksPlatforms ? upwardCF : groundCF, hit, minDistance);
        //string objectLabel = "";
        //string objectLayer = "";
        //print("uppaer");

        /*
         * v: 10 + 1
         * d: 5
         * nv: (11 - 5 - 1) + 5 *()
         * 
         * 
         * */

        float slopeAngle = 0;
        if (terrainResult > 0)
        {
            for (int i = 0; i < terrainResult; i++)
            {
                if (hit[i].distance == 0)
                    continue;
                //print("c'è qualcunoooo");
                //string otherLabel = hit[i].collider.tag;
                //string otherLayer = LayerMask.LayerToName(hit[i].collider.gameObject.layer);

                

                if (hit[i].distance < minDistance)
                {
                    //objectLabel = otherLabel;
                    //objectLayer = otherLayer;
                    minDistance = hit[i].distance;
                    slopeAngle = Vector2.SignedAngle(hit[i].normal, -Vector2.up);
                    //print("slllllooope angle: " + slopeAngle);
                }
            }


            //if (minDistance - skinWidth <= Mathf.Tan(Mathf.Abs(slopeAngle) * Mathf.Deg2Rad) * Mathf.Abs(velocity.x))
                isTouchingGround = true;
            
        }


        //print("slopeAngle: " + slopeAngle);




        if (Mathf.Abs(slopeAngle) >= maxUpperSlopeAngle || climbedPlatform != null)
        {
            //if (velocity.x != 0 && isTouchingGround && slopeAngle != 0)
                if (isTouchingGround && slopeAngle != 0)
            {
                //print("slopeAngle: " + slopeAngle);
                
                Vector2 oldVel = new Vector2(velocity.x, velocity.y);

                


                float newHorizontal = -Mathf.Sign(slopeAngle) * Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * (oldVel.y);

                if (velocity.x == 0 || (Mathf.Sign(velocity.x) != Mathf.Sign(newHorizontal) || Mathf.Abs(velocity.x) <= Mathf.Abs(newHorizontal)))
                    velocity.x = newHorizontal;
                //velocity.y = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * (oldVel.y);
                velocity.y = ((oldVel.y) * Mathf.Sin(Mathf.Abs(slopeAngle) * Mathf.Deg2Rad));
                //velocity.x = oldVel.x - (minDistance + skinWidth) + (minDistance * -Mathf.Cos(slopeAngle * Mathf.Deg2Rad));

                //print("new velocity: " + velocity.x + " " + velocity.y + " angolo: " + slopeAngle + "old: " + oldVel);
                Debug.DrawRay(verticalCollider.bounds.center, 10000* velocity, Color.white);



                //Debug.DrawRay(hit[0].point, velocity, Color.yellow);
            }
        }
       
    }

    void UtilityColliderComputeAngle(ref Vector2 velocity, RaycastHit2D[] hit)
    {


        //float minDistance = 10*skinWidth;
        //float minDistance = groundShell;
        slopeAngle = 0;

        //if (velocity.y <0)
        //print("velocity: " + velocity.y);
        float currentGroundShell = velocity.y >= 0 ? groundShell : Mathf.Max(groundShell, Mathf.Abs(velocity.y + skinWidth));
        currentGroundShell += collidersOffset;
        float minDistance = currentGroundShell;
        angMinDistance = minDistance;
        //int terrainResult = verticalCollider.Cast(-Vector2.up, groundCF, hit, minDistance);

        
        int terrainResult;
        if (collisions.isIgnoringPlatforms <= 0)
        {
            terrainResult = utilityCollider.Cast(-Vector2.up, groundCheckCF, hit, currentGroundShell);
            //print("checkkkkkk");
            
        }
        else
            terrainResult = utilityCollider.Cast(-Vector2.up, noPlatformsGroundCheckCF, hit, currentGroundShell);


        objectUnderMe = null;
        currentGroundDistance = groundDistanceMax;

        float minDistanceFound = currentGroundShell;
        int closestObjectLayer = -1;
        objectLayer = -1;
        bool groundCheckResult = false;
        Collider2D closestObject = null;
        float currentDistance;
        exitMinDistance = collidersOffset;
        //RaycastHit2D exitObjectHit;
        
        //if (terrainResult > 0)
        {
            for (int i = 0; i < terrainResult; i++)
            {
                
                otherLayer = (hit[i].collider.gameObject.layer);
                currentDistance = hit[i].distance;
                if (otherLayer == character2LayerIndex || otherLayer == character1LayerIndex || otherLayer == character0LayerIndex)
                {
                    if (exitsCharacters && !isExiting && !inHitstop && !avoidingCharacters && currentDistance < collidersOffset)
                    {


                        float dist = tr.position.x - hit[i].transform.position.x;
                        float thisExitVelocity = Mathf.Sign(dist) * fastExitVelocity * Time.fixedDeltaTime / TimeVariables.currentTimeScale;
                        //print("sto uscendo: " + thisExitVelocity / Time.fixedDeltaTime + " " + fastExitVelocity + " " + exitVelocity);
                        //print("sto uscendo: " + Time.time / Time.fixedDeltaTime);

                        if (Mathf.Abs(dist) <= 0.2f)//0.01f)
                        {
                            dist = Mathf.Sign(Random.value - 0.5f);
                            thisExitVelocity *= dist;
                            //if (mystatus.faction == 0)
                            //print("random");
                        }

                        

                        velocity.x = thisExitVelocity;
                        //print("spostatiiiii");
                        isExiting = true;
                        //continue;
                    }
                    continue;
                }
                if (currentDistance < collidersOffset)
                {
                    
                    if (currentDistance == 0)
                    {
                        //print("dentro");
                        //colliderIsInside = true;
                        continue;
                    }
                    if (otherLayer == defaultLayerIndex)
                        hasGroundUnder = true;
                    
                    if (otherLayer == platformLayerIndex || otherLayer == movingPlatformLayerIndex)
                    {
                        if (!simplePhysics)
                        {
                            float objectAngle = Vector2.Angle(hit[i].normal, Vector2.up);
                            if (objectAngle == 90)
                                continue;
                            if (objectAngle > maxSlopeAngle)
                            {
                                //print("1");
                                continue;

                            }
                        }
                        //print ("porattoforumu");
                        if (collisions.isIgnoringPlatforms > 0 || (platformsHit.Contains(hit[i].collider) || !grounded))
                        {
                            //print("2");
                            continue;
                        }
                    }
                    /*
                    if (collisions.isIgnoringStairs > 0 && otherLabel == "Stairs")
                    {
                        continue;
                    }
                    */


                    if (hit[i].distance < exitMinDistance)
                    {
                        exitMinDistance = hit[i].distance;
                        exitObjectPosition = hit[i].point;
                    }




                    continue;
                    
                }
                //print("c'è qualcunoooo");
                //otherLabel = hit[i].collider.tag;

                //if (platformsHit.Contains(hit[i].collider) || (otherLayer == platformLayerIndex && collisions.isIgnoringPlatforms > 0))
                //|| (otherLabel == "Stairs" && collisions.isIgnoringStairs > 0))
                //continue;
                if (otherLayer == defaultLayerIndex)
                    hasGroundUnder = true;

                if (currentDistance < minDistance)
                {
                    //objectLabel = otherLabel;
                    objectLayer = otherLayer;
                    minDistance = currentDistance;
                    if (!simplePhysics)
                        slopeAngle = Vector2.SignedAngle(hit[i].normal, Vector2.up);
                    else
                        slopeAngle = 0;
                    closestObjectLayer = otherLayer;
                    //print("slllllooope angle: " + slopeAngle);


                    //if (otherLabel != "moving platform")
                    if (otherLayer != movingPlatformLayerIndex)
                    {

                        

                        //if (minDistance < minDistanceFound)
                        {
                            minDistanceFound = minDistance;
                            closestObject = hit[i].collider;
                            if (Mathf.Abs(slopeAngle) < maxSlopeAngle)
                                currentGroundDistance = minDistanceFound;
                        }

                    }

                    //if (hit[j].collider.tag == "moving platform")
                    else
                    {
                        //print("movingplatform found");
                        closestObject = hit[i].collider;
                        minDistanceFound = hit[i].distance;
                        currentGroundDistance = minDistanceFound;

                    }

                    if (velocity.y <= 0 && Mathf.Abs(slopeAngle) < maxSlopeAngle)

                    {
                        //if (mystatus.faction == 0)

                          //  print("grounded true " + velocity.y);
                            groundCheckResult = true;

                    }
                    else
                    {
                        //if (mystatus.faction == 0)
                          //  print("grounded false " + velocity.y);

                    }
                }







                //inizio test




                //print("closest: " + closestObject);
                /*
                if (velocity.y <= 0 && Mathf.Abs(slopeAngle) < maxSlopeAngle)

                {

                    groundCheckResult = true;

                }
                */



                //fine test



            }


        }
        objectUnderMe = closestObject;

        //print("objectunderme " + LayerMask.LayerToName(closestObjectLayer));
        if (closestObjectLayer == platformLayerIndex || closestObjectLayer == movingPlatformLayerIndex)
        {

            collisions.standingOnPlatform = true;
            if (platformsHit.Contains(closestObject))
            {
                platformsHit.Remove(closestObject);
                print("removed");

            }
        }

        if (collisions.slidingDownMaxSlope || (mystatus.mystatus == Status.status.juggle && inHitstop))
        {
            grounded = false;
            
        }
        else
        {
            //print("11111");
            if (groundCheckResult)
            {
                grounded = true;
                //if (mystatus.faction == 0)
                //print("22222");
            }
            else
            {
                grounded = false;
                //if (mystatus.faction == 0)
                    //print("333333");
            }
        }

        angMinDistance = minDistance;
        //objectLayer = 







        //esco dai collider 
        if (exitMinDistance < collidersOffset && climbedPlatform == null)
        {
            float translationDistance;
            //Ray ray = new Ray(hit[0].point, new Vector3(0, 1, 0));
            //Debug.DrawRay(exitObjectPosition, Vector2.down, Color.yellow, 100);

            Vector2 hitPointRelativeToCollider = new Vector2(-verticalCollider.bounds.center.x + exitObjectPosition.x,
                -verticalCollider.bounds.center.y + exitObjectPosition.y);

            //float distanceRelativeCollider = Vector2.Distance(verticalCollider.bounds.center, hit[0].point);

            //print("hit relative: " + hitPointRelativeToCollider.x + " " + hitPointRelativeToCollider.y);
            float destinationPoint;

            //x^2 + x0^2 - 2*x*x0 + y^2 + y0^2 - 2*y*y0 = r^2   ==>   
            float x = hitPointRelativeToCollider.x;
            float x0 = capsuleColliderInfo.lowerCenter.x;
            float y0 = capsuleColliderInfo.lowerCenter.y;
            float r = capsuleColliderInfo.r;
            float c = x * x + x0 * x0 - 2 * x * x0 + y0 * y0 - r * r;
            float b = -2 * y0;
            float y = (-b - Mathf.Sqrt((b * b) - (4 * c))) / 2;

            destinationPoint = y;



            //destinationPoint^2 + hitPointRelativeToCollider.x^2 + capsuleColliderInfo.lowerCenter.x ^2 + capsuleColliderInfo.lowerCenter.y ^2
            //- 2*hitPointRelativeToCollider.x*capsuleColliderInfo.lowerCenter.x - 2*destinationPoint*capsuleColliderInfo.lowerCenter.y
            //= capsuleColliderInfo.r^2

            //destinationPoint = Mathf.Sqrt(4);
            //print("destinationPoint: " + destinationPoint);

            /*
            Debug.DrawRay(new Vector2(verticalCollider.bounds.center.x + hitPointRelativeToCollider.x, verticalCollider.bounds.center.y + hitPointRelativeToCollider.y),
                1000 * Vector2.right, Color.blue);
            Debug.DrawRay(new Vector2(verticalCollider.bounds.center.x + hitPointRelativeToCollider.x, verticalCollider.bounds.center.y + hitPointRelativeToCollider.y),
                1000 * Vector2.up, Color.blue);
            */

            //Debug.DrawRay(hit[0].point, Vector2.right, Color.red, 10);
            //Debug.DrawRay(hit[0].point, 100*Vector2.up, Color.red, 10);

            //if (verticalCollider.bounds.IntersectRay(ray, out translationDistance))
            {
                //Debug.DrawRay(hit[0].point, -Vector2.up * translationDistance, Color.green);

                //translationDistance = verticalCollider.bounds.size.y /2 - hit[0].distance + skinWidth;
                translationDistance = -destinationPoint + hitPointRelativeToCollider.y;
                /*
                Debug.DrawRay(new Vector2(verticalCollider.bounds.center.x + hitPointRelativeToCollider.x, verticalCollider.bounds.center.y
                    + hitPointRelativeToCollider.y),
                -1000 * Vector2.right, Color.green, 10);

                Debug.DrawRay(new Vector2(verticalCollider.bounds.center.x + hitPointRelativeToCollider.x, verticalCollider.bounds.center.y
                    - destinationPoint),
                -1000 * Vector2.right, Color.red, 10);
                */

                //ATTENZIONE, DA TESTARE
                
                    tr.Translate(new Vector3(0, translationDistance + skinWidth, 0));
                Physics2D.SyncTransforms();

                print("sono dentro: " + translationDistance + " " + slopeAngle);
                
                //newoffset = verticalCollider.offset;
                //newoffset.y += translationDistance;
                //verticalCollider.offset = newoffset;
                
            }
        }

    }

    void ComputeAngle(ref Vector2 velocity, RaycastHit2D[] hit)
    {
        
            
            //float minDistance = 10*skinWidth;
            //float minDistance = groundShell;
            slopeAngle = 0;

        //if (velocity.y <0)
        //print("velocity: " + velocity.y);
            float currentGroundShell = velocity.y >= 0 ? groundShell : Mathf.Max(groundShell, Mathf.Abs(velocity.y + skinWidth + 0.01f));
            float minDistance = currentGroundShell;
        angMinDistance = minDistance;
            //int terrainResult = verticalCollider.Cast(-Vector2.up, groundCF, hit, minDistance);


            int terrainResult;
            if (collisions.isIgnoringPlatforms > 0)
                terrainResult = verticalCollider.Cast(-Vector2.up, upwardCF, hit, currentGroundShell);
            else
                terrainResult = verticalCollider.Cast(-Vector2.up, groundCF, hit, currentGroundShell);


            objectUnderMe = null;
            currentGroundDistance = groundDistanceMax;

            float minDistanceFound = currentGroundShell;
            int closestObjectLayer = -1;
        objectLayer = -1;
            bool groundCheckResult = false;
            Collider2D closestObject = null;


            if (terrainResult > 0)
            {
                for (int i = 0; i < terrainResult; i++)
                {
                if (hit[i].distance == 0)
                {
                    //print("dentro");
                    colliderIsInside = true;
                    continue;
                }
                    //print("c'è qualcunoooo");
                    //otherLabel = hit[i].collider.tag;
                    otherLayer = (hit[i].collider.gameObject.layer);

                    //if (platformsHit.Contains(hit[i].collider) || (otherLayer == platformLayerIndex && collisions.isIgnoringPlatforms > 0))
                    //|| (otherLabel == "Stairs" && collisions.isIgnoringStairs > 0))
                    //continue;

                    if (hit[i].distance < minDistance)
                    {
                        //objectLabel = otherLabel;
                        objectLayer = otherLayer;
                        minDistance = hit[i].distance;
                        slopeAngle = Vector2.SignedAngle(hit[i].normal, Vector2.up);
                    closestObjectLayer = otherLayer;
                    //print("slllllooope angle: " + slopeAngle);


                    //if (otherLabel != "moving platform")
                    if (otherLayer != movingPlatformLayerIndex)
                    {



                        //if (minDistance < minDistanceFound)
                        {
                            minDistanceFound = minDistance;
                            closestObject = hit[i].collider;
                            currentGroundDistance = minDistanceFound;
                        }

                    }

                    //if (hit[j].collider.tag == "moving platform")
                    else
                    {
                        //print("movingplatform found");
                        closestObject = hit[i].collider;
                        minDistanceFound = hit[i].distance;
                        currentGroundDistance = minDistanceFound;

                    }
                }







                    //inizio test
                    



                    //print("closest: " + closestObject);

                    if (velocity.y <= 0 && Mathf.Abs(slopeAngle) < maxSlopeAngle)

                    {

                        groundCheckResult = true;

                    }




                    //fine test



                }

                
            }
        objectUnderMe = closestObject;

        //print("objectunderme " + LayerMask.LayerToName(closestObjectLayer));
        if (closestObjectLayer == platformLayerIndex || closestObjectLayer == movingPlatformLayerIndex)
        {
            
            collisions.standingOnPlatform = true;
            if (platformsHit.Contains(closestObject))
            {
                platformsHit.Remove(closestObject);
                print("removed");

            }
        }

        if (collisions.slidingDownMaxSlope || (mystatus.mystatus == Status.status.juggle && inHitstop))
            grounded = false;
        else
        {
            if (groundCheckResult)
                grounded = true;
            else
                grounded = false;
        }

        angMinDistance = minDistance;
        //objectLayer = 


    }


    void CalculateAngularVelocity (float minDistance, ref Vector2 velocity, int objectLayer)
    {
        
        if (slopeAngle == 0 || slopeAngle == 90 || slopeAngle == -90)// || objectLayer == movingPlatformLayerIndex)
        {
            //if (slopeAngle != 0)
            //print("EH VOLEVI");
            return;
        }

        if (movingPlatform != null)
            velocity.x -= movingPlatform.totVelocity.x * Time.fixedDeltaTime;
        //Debug.DrawRay(hit[0].point, 10 * hit[0].normal, Color.red);
        bool isTouchingGround = false;
        
        //if (mystatus.faction == 0)
        //print("ang: " + (minDistance - skinWidth - collidersOffset) + " " + Mathf.Tan(Mathf.Abs(slopeAngle) * Mathf.Deg2Rad) * Mathf.Abs(velocity.x));
        //print("differenza: " + (hit[0].distance - skinWidth - Mathf.Tan(Mathf.Abs(slopeAngle) * Mathf.Deg2Rad) * Mathf.Abs(velocity.x)));
        if (minDistance - collidersOffset - skinWidth <= Mathf.Tan(Mathf.Abs(slopeAngle) * Mathf.Deg2Rad) * Mathf.Abs(velocity.x))
                    //if (hit[0].distance - skinWidth <= Mathf.Sin(Mathf.Abs(slopeAngle) * Mathf.Deg2Rad) * Mathf.Abs(velocity.x))
                    //if (minDistance <= skinWidth)
                    isTouchingGround = true;
                //else
                //  slopeAngle = 0;
        



        //if (velocity.x != 0)
        //print("velocity.x = " + velocity.x + " slopeAngle = " + slopeAngle + " " + isTouchingGround);
        //print("angle: " + slopeAngle);
        
        
        if (Mathf.Abs(slopeAngle) < maxSlopeAngle || (climbedPlatform != null && !descendingPlatform))
        {
            //if (slopeAngle != 0)
            
            if (velocity.x != 0 && slopeAngle != 0 && (isTouchingGround))

            {
                //if (mystatus.faction == 0)
                  //  print("aloraaa");
                /*
                if (Mathf.Sign(velocity.x) != Mathf.Sign(slopeAngle))
                {
                    //print("climbbb " + velocity.x + " " + slopeAngle);
                    collisions.climbingSlope = true;
                    //collisions.below = true;
                }
                else
                {
                    collisions.descendingSlope = true;
                    //print("descend " + velocity.x + " " + slopeAngle);
                }
                */
                //print("allooooooraaa");
                if (velocity.y <= 0)
                {
                    //print("angle");
                    collisions.below = true;
                    Vector2 oldVel = new Vector2(velocity.x, velocity.y);
                    //float newHorizontal = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * oldVel.x;
                    {
                        velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * oldVel.x;
                        //if (velocity.x > 0)
                        //print("scivoloooo " + velocity.x);
                    }
                    velocity.y = -Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * (oldVel.x);
                    //velocity.x = velocity.y / Mathf.Sin(Mathf.Abs(slopeAngle * Mathf.Deg2Rad)) * Mathf.Cos(Mathf.Abs(slopeAngle * Mathf.Deg2Rad));
                    //print(TimeVariables.totalFramesSinceStart + " new velocity: " + velocity.x + " " + velocity.y + " angolo slope velocità: " + Vector2.Angle(hit[0].normal, velocity));
                    Debug.DrawRay(verticalCollider.bounds.center, 50 * velocity, Color.red);
                }


                //Debug.DrawRay(hit[0].point, velocity, Color.yellow);
            }
        }
        else
        {
            //print("uuuh?");
            //if (isTouchingGround)
            //if ()
            //if (objectLabel != "moving platform")
            if (objectLayer != platformLayerIndex && objectLayer != movingPlatformLayerIndex && velocity.y <= 0)// || objectLabel == "moving platform")
            {
                avoidingCharacters = true;
                
                Vector2 oldVel = new Vector2(velocity.x, Mathf.Max(Mathf.Abs(velocity.y), 0.2f / TimeVariables.currentTimeScale * Time.timeScale));
                //float newHorizontal = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * (Mathf.Sign(slopeAngle) * 0.2f);
                //float newHorizontal = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * (Mathf.Sign(slopeAngle) * 0.2f * Time.timeScale);
                //print("vel: " + oldVel.y);

                float cosSlope = Mathf.Cos(slopeAngle * Mathf.Deg2Rad);
                if (Mathf.Abs(cosSlope) < 0.00001f)
                    cosSlope = 0;
                float newHorizontal = cosSlope * (Mathf.Sign(slopeAngle) * Mathf.Abs(oldVel.y)); //* Time.timeScale);

                //if (velocity.x == 0 || (Mathf.Sign(velocity.x) != Mathf.Sign(newHorizontal) || Mathf.Abs(velocity.x) <= Mathf.Abs(newHorizontal)))
                if (velocity.x == 0 || (velocity.x * newHorizontal <= 0 || Mathf.Abs(velocity.x) <= Mathf.Abs(newHorizontal)))
                    velocity.x = newHorizontal;

                //destra: vel = 10, newvel = 5
                //sinistra: vel = -10, newvel = -5


                //velocity.y = -Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * (Mathf.Sign(slopeAngle) * (Mathf.Abs(oldVel.y) * Time.timeScale - minDistance)) - minDistance;
                velocity.y = -Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * (Mathf.Sign(slopeAngle) * (Mathf.Abs(oldVel.y)));// * Time.timeScale);
                collisions.descendingSlope = true;
                collisions.slidingDownMaxSlope = true;
                //print("slidingggg " + velocity);
                /*
                if (velocity.x >= 0)
                {
                    print("slidingggg " + velocity.x + " " + slopeAngle + " " + Mathf.Cos(slopeAngle * Mathf.Deg2Rad) + " " + cosSlope);
                    print("old vel: " + oldVel.x + " " + newHorizontal + " " + (Mathf.Abs(velocity.x) <= Mathf.Abs(newHorizontal)));
                    print("y: " + velocity.y + " " + Mathf.Sin(slopeAngle * Mathf.Deg2Rad));
                }
                */
                grounded = false;
                Debug.DrawRay(verticalCollider.bounds.center, 50 * velocity, Color.green);
            }
            
            
        }
        if (Mathf.Abs(velocity.x) > 0.0000001f && slopeAngle != 0 && isTouchingGround)
        {
            if (Mathf.Sign(velocity.x) != Mathf.Sign(slopeAngle))
            {
                //print("climbbb " + Mathf.Sign(velocity.x) + " " + velocity.x + " " + slopeAngle);
                collisions.climbingSlope = true;
                //collisions.below = true;
                
            }
            else
            {
                collisions.descendingSlope = true;
                //print("descend " + velocity.x + " " + slopeAngle);
            }
        }

        if (movingPlatform != null)
        {
            velocity.x += movingPlatform.totVelocity.x * Time.fixedDeltaTime;
            //print("eh sì perchèèèè sì insomma");
        }
        
        Debug.DrawRay(verticalCollider.bounds.center, 5000 * velocity, Color.red);

    }

    protected override bool GroundCheck(Vector2 velocity)
    {
        //print("gc");
        //if (Time.timeScale != 1 )
        {
            //RaycastHit2D[] hit = new RaycastHit2D[30];

            objectUnderMe = null;
            currentGroundDistance = groundDistanceMax;

            float minDistanceFound = groundShell;
            int closestObjectLayer = -1;
            bool groundCheckResult = false;
            Collider2D closestObject = null;

            //for (int i = 0; i < verticalRayCount; i++)
            {


                //Vector2 rayOrigin = rayCastOrigins.bottomLeft;
                float currentGroundShell = velocity.y >= 0 ? groundShell : Mathf.Max(groundShell, Mathf.Abs(velocity.y));

                //rayOrigin += Vector2.right * (verticalRaySpacing * i);
                
                
                int count = 0;
                if (collisions.isIgnoringPlatforms > 0)
                    count = verticalCollider.Cast(-Vector2.up, upwardCF, hit, currentGroundShell);
                else
                    count = verticalCollider.Cast(-Vector2.up, groundCF, hit, currentGroundShell);
                //print("groundcheck: " + hit.Length + ", shell: " + currentGroundShell);

                //print (collisions.standingOnPlatform);


                float minDistance = currentGroundShell;

                //Debug.DrawRay(verticalCollider.bounds.min, -Vector2.up * currentGroundShell, Color.blue);
                if (count > 0)
                {
                    for (int j = 0; j < count; j++)
                    {


                        if (platformsHit.Contains(hit[j].collider))
                        {
                            //print ("removed!");

                            //platformsHit.Remove (hit [j].collider);
                            continue;


                        }

                        float slopeAngle = Vector2.Angle(hit[j].normal, Vector2.up);
                        if (slopeAngle >= maxSlopeAngle)
                        {
                            continue;
                        }

                        //FANCULO LE SCALE, DOVETE MORIRE
                        /*
                        if (collisions.isIgnoringStairs > 0 && hit[j].collider.tag == "Stairs")
                            continue;

                        if (stairsHit.Contains(hit[j].collider))
                        {
                            //print ("BUA");
                            continue;
                        }
                        */

                        //if (collisions.isIgnoringStairs > 0 && hit [j].collider.tag == "Stairs")
                        //	continue;

                        //float slopeAngle = Vector2.Angle(hit[j].normal, Vector2.up);
                        //print (velocity.y);
                        string colliderTag = hit[j].collider.tag;
                        if (colliderTag != "moving platform")
                        {
                            if (hit[j].distance < minDistance)
                            {
                                minDistance = hit[j].distance;

                                if (minDistance < minDistanceFound)
                                {
                                    minDistanceFound = minDistance;
                                    closestObject = hit[j].collider;
                                    closestObjectLayer = (hit[j].collider.gameObject.layer);
                                    currentGroundDistance = minDistanceFound;
                                }
                            }
                        }

                        //if (hit[j].collider.tag == "moving platform")
                        else
                        {
                            //print("movingplatform found");
                            closestObject = hit[j].collider;
                            minDistanceFound = hit[j].distance;
                            closestObjectLayer = (hit[j].collider.gameObject.layer);
                            currentGroundDistance = minDistanceFound;

                        }



                        //print("closest: " + closestObject);

                        //if (slopeAngle < maxSlopeAngle && velocity.y <= 0)
                        if (velocity.y <= 0)
                        //&& ((closestObject != null && closestObject.tag == "moving platform") || velocity.y <= 0)) 
                        //if (slopeAngle < maxSlopeAngle && velocity.y <= 0)  
                        {
                           

                                //print("closest: " + closestObject);
                                groundCheckResult = true;
                            
                            //objectUnderMe = closestObject;
                        }
                        else
                        {
                            //print("no, slopeangle: " + slopeAngle + " closest: " + closestObject + " velocity: " + velocity.y);
                            //objectUnderMe = null;
                        }
                    }

                    //return false;
                }
                else
                {
                    //print("NO?");
                    //objectUnderMe = null;
                }

            }
            objectUnderMe = closestObject;

            if (closestObjectLayer == platformLayerIndex)
            {
                //print("platform");
                collisions.standingOnPlatform = true;
                if (platformsHit.Contains(closestObject))
                {
                    platformsHit.Remove(closestObject);
                    print("removed");

                }
            }
            /*
            if (closestObject != null && closestObject.tag == "Stairs")
            {
                print("stairs");
                collisions.standingOnStairs = true;
                if (stairsHit.Contains(closestObject))
                {
                    stairsHit.Remove(closestObject);
                    print("no stairs");
                }
            }
            */


            //print (collisions.standingOnPlatform);

            if (collisions.slidingDownMaxSlope)
                return false;

            if (groundCheckResult)
                return true;
            return false;








            //print ("groundDistance = " + currentGroundDistance);
        }
    }

    protected void UpdateLayer()
    {
        if (avoidingCharacters)
        {
            for (int i = 0; i < myColliders.Length; i++)
            {
                if (myColliders[i].physicalCollider != null)
                    myColliders[i].physicalCollider.gameObject.layer = ignoringCharactersLayerIndex;
                if (myColliders[i].exitCollider != null)
                    myColliders[i].exitCollider.enabled = false;
                
            }
            //gameObject.layer = ignoringCharactersLayerIndex;
            verticalCollider.gameObject.layer = ignoringCharactersLayerIndex;
            horizontalCollisionMask = upwardCF.layerMask;


        }
        else
        {
            
           
            verticalCollider.gameObject.layer = currentCharacterLayer;
            horizontalCollisionMask = Physics2D.GetLayerCollisionMask(currentCharacterLayer);
            

            //gameObject.layer = currentCharacterLayer;


        }

        /*
        horizontalCollisionMask = Physics2D.GetLayerCollisionMask(verticalCollider.gameObject.layer);
        if (!checksPlatforms)
        {
            horizontalCollisionMask |= 1 << platformLayerIndex;
            horizontalCollisionMask |= 1 << movingPlatformLayerIndex;

        }
        */
        /*
        if (avoidingCharacters || !exitsColliders)
            horizontalCollisionMask = upwardCF.layerMask;
        else 
            horizontalCollisionMask = Physics2D.GetLayerCollisionMask(verticalCollider.gameObject.layer);
        */
        if (!checksPlatforms)
        {
            horizontalCollisionMask |= 1 << platformLayerIndex;
            horizontalCollisionMask |= 1 << movingPlatformLayerIndex;

        }


        horizontalCF.SetLayerMask(horizontalCollisionMask);
        


    }

    void SingleRayCastVerticalCollisions(ref Vector2 velocity)
    {
        /*
        RaycastHit2D[] hit;

        float minDistance = myCollider.bounds.size.y / 2 + Mathf.Abs(velocity.y) + skinWidth;

        //Debug.DrawRay(myCollider.bounds.center, Mathf.Sign(velocity.y) * Vector2.up * minDistance, Color.blue);

        hit = Physics2D.RaycastAll(myCollider.bounds.center, velocity.y > 0 ? Vector2.up : -Vector2.up, minDistance, groundMask);
        //float slopeAngle = 0f;

        if (hit.Length > 0)
        {
            for (int i = 0; i < hit.Length; i++)
            {
                if (hit[i].distance <= minDistance)
                {
                    minDistance = hit[i].distance;
                    //slopeAngle = Vector2.Angle(hit[i].normal, Vector2.up) * Mathf.Deg2Rad;
                }
            }

            minDistance -= myCollider.bounds.size.y / 2;

            if (minDistance >= 0)
            {
                velocity.y = Mathf.Sign(velocity.y) * (minDistance - skinWidth);
                //print("ho toccato");
            }

            else
            {
                //transform.Translate(new Vector3(0, velocity.y >= 0 ? (minDistance - skinWidth) : - (minDistance - skinWidth), 0));
                float translationDistance = -minDistance + skinWidth;
                transform.Translate(new Vector3(0, translationDistance, 0));
                velocity.y = 0;

                print("sono dentro: " + translationDistance);
            }

            //faccio un raycast per ricavare l'inclinazione del terreno

            RaycastHit2D terrainResult;
            bool isTouchingGround = false;
            terrainResult = Physics2D.Raycast(new Vector2(myCollider.bounds.center.x, myCollider.bounds.center.y - myCollider.bounds.size.y / 2),
                -Vector2.up, skinWidth, groundMask);
            if (terrainResult)
            {
                slopeAngle = Vector2.Angle(terrainResult.normal, Vector2.up);
                isTouchingGround = true;
            }
            else
                slopeAngle = 0;


            //print(slopeAngle * Mathf.Rad2Deg);


            if (slopeAngle < maxSlopeAngle && isTouchingGround)
            {
                print("slopeAngle: " + slopeAngle);
                Vector2 oldVel = new Vector2(velocity.x, velocity.y);
                velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * oldVel.x;
                velocity.y = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * oldVel.x;
                transform.Translate(velocity);
                //print("new velocity: " + velocity);

            }


        }
        */
    }


    //SIMPLE PHYSICS
    void SimpleColliderCastHorizontalCollisions(ref Vector2 velocity)
    {
        /*
            if (velocity.y < 0 || (climbedPlatform != null && velocity.x != 0))
            {
                //ComputeAngle(ref velocity, hit);
                //if (mystatus.faction == 0)
                //print("giuuuuu " + climbedPlatform + " " + velocity);
                CalculateAngularVelocity(angMinDistance, ref velocity, objectLayer);
            }
            else if (checksUpperAngle && grounded == false)
            {
                //if (mystatus.faction == 0)
                //print("SUUUU");
                ComputeUpperAngle(ref velocity, hit);
            }
        */
        
        if (avoidingCharacters != previousAvoidingCharacters)
        {
            //print("aaaaaa");
            UpdateLayer();
        }
        previousAvoidingCharacters = avoidingCharacters;

        //if (velocity.x == 0)
        //  return;
        float minDistance;
        if (getsPushedByCharacters)
            minDistance = Mathf.Abs(velocity.x) + skinWidth + characterAvoidSkinwidth;
        else
            minDistance = Mathf.Abs(velocity.x) + skinWidth;
        //float slopeAngle = 0;
        //int count = 0;
        acceptableAngle = false;

        //if (!avoidingCharacters)
        if (mystatus.faction == 0 && velocity.x == 0)
        {
            //print("OOOOO " + (velocity.x != 0 ? Mathf.Sign(velocity.x) : Mathf.Sign(tr.lossyScale.x)) + " "+Time.frameCount);
        }

        int count = verticalCollider.Cast(Vector2.right * ((velocity.x != 0 ? Mathf.Sign(velocity.x) : Mathf.Sign(tr.localScale.x))), horizontalCF, hit,
            minDistance);
        //else
        //count = verticalCollider.Cast(Vector2.right * Mathf.Sign(velocity.x), upwardCF, hit, minDistance);

        //count = verticalCollider.Cast(Vector2.zero, horizontalCF, hit, minDistance);
        Debug.DrawRay(new Vector3(tr.position.x, tr.position.y + 5), new Vector3(velocity.x, 0, 0), Color.yellow);
        Debug.DrawRay(tr.position, Vector3.up * 100, Color.yellow);

        //bool hasHitHor = false;
        hasHitWall = false;
        //exitCharactersVelocity = 0;
        if (count > 0)
        {
            if (mystatus.faction == 0)
            {
                //print("count: " + count + " " + Time.frameCount);
            }
            for (int i = 0; i < count; i++)
            {

                if (hit[i].distance == 0)
                {
                    //print("trooooppo dentro " + Time.frameCount);
                    continue;

                }
                //acceptableAngle = !grounded || Vector2.Angle(hit[i].normal, Vector2.up) > maxSlopeAngle;
                acceptableAngle = true;

                if (hit[i].distance < Mathf.Abs(velocity.x) + skinWidth)
                {
                    hasHitWall = true;
                    //if (getsPushedByCharacters)
                    //  print("WEEEEEEEEEEEEEE");
                }
                //if (!grounded || Vector2.Angle(hit[i].normal, Vector2.up) > maxSlopeAngle)
                {
                    if (getsPushedByCharacters)
                    {
                        collisionLayer = hit[i].collider.gameObject.layer;
                        {
                            if (collisionLayer != character0LayerIndex && collisionLayer != character1LayerIndex && collisionLayer != character2LayerIndex)
                            {
                                if (hit[i].distance >= Mathf.Abs(velocity.x) + skinWidth || !acceptableAngle)
                                {
                                    continue;
                                }
                                else
                                {
                                    hasHitWall = true;
                                }

                            }
                            else
                            {
                                //print("WEE");


                                /*
                                exitCharactersVelocity = - minDistance + hit[i].distance;

                                exitCharactersVelocity *= (velocity.x != 0 ? Mathf.Sign(velocity.x) : Mathf.Sign(tr.localScale.x));
                                */
                                //continue;
                            }

                        }

                    }


                    if (hit[i].distance <= minDistance)
                    {
                        if (getsPushedByCharacters || acceptableAngle)
                        {
                            //print("HHHHH");

                            Debug.DrawRay(hit[i].point, hit[i].normal, Color.blue);


                            minDistance = hit[i].distance;

                            if (mystatus.faction == 0)
                            {
                                //print("hor: " + minDistance + " " + Time.frameCount);
                            }

                        }
                    }
                }
            }
        }

        float newDistance;
        if (getsPushedByCharacters)
        {
            newDistance = minDistance - skinWidth - (!hasHitWall ? characterAvoidSkinwidth : 0);
            
        }
        else
            newDistance = minDistance - skinWidth;

        //if (mystatus.faction == 0)
        //  print("vel: " + newDistance + " " + Time.frameCount);
        if (newDistance < 0)
            newDistance = 0;
        velocity.x = (velocity.x == 0 ? Mathf.Sign(tr.localScale.x) : Mathf.Sign(velocity.x)) * newDistance;




        if (velocity.x != 0)
        {
            tr.Translate(velocity.x, 0, 0);
            //rb.MovePosition(new Vector2 (tr.position.x + velocity.x, 0));

            Physics2D.SyncTransforms();
        }



    }


    void SimpleSingleCastVerticalCollisions(ref Vector2 velocity)
    {
        //print("inizio: " + velocity.y);
        //print("Velocity.x = " + velocity.x + " ricavo: " + (velocity.y / Mathf.Sin(Mathf.Abs(slopeAngle)) * Mathf.Cos(Mathf.Abs(slopeAngle))));
        //RaycastHit2D[] hit = new RaycastHit2D[30];
        //Debug.DrawRay(new Vector2 (verticalCollider.bounds.center.x + capsuleColliderInfo.lowerCenter.x,
        //  verticalCollider.bounds.center.y + capsuleColliderInfo.lowerCenter.y), Vector2.right, Color.red);

        //Debug.DrawRay(new Vector2(horizontalCollider.bounds.min.x, horizontalCollider.bounds.min.y + collidersOffset), 10 * Vector3.right);
        //Debug.Log("centro: " + verticalCollider.bounds.center);

        //float minDistance = myCollider.bounds.size.y / 2 + Mathf.Abs(velocity.y) + skinWidth;
        float minDistance = Mathf.Abs(velocity.y) + skinWidth;
        //print("mindistance: " + minDistance);
        //Debug.DrawRay(horizontalCollider.bounds.min, 10 * Vector3.right);

        //Debug.DrawRay(myCollider.bounds.center, Mathf.Sign(velocity.y) * Vector2.up * minDistance, Color.blue);

        //int count = verticalCollider.Cast(myCollider.bounds.center, velocity.y > 0 ? Vector2.up : -Vector2.up, minDistance, groundMask);
        int count;
        if (velocity.y > 0)
        {
            //ATTENZIONE, DA TESTARE
            /*
            newoffset = verticalCollider.offset;
            newoffset.y += collidersOffset;
            verticalCollider.offset = newoffset;
            Vector2 newsize = verticalCollider.size;
            newsize.y -= collidersOffset * 2;
            verticalCollider.size = newsize;
            count = verticalCollider.Cast(Vector2.up, checksPlatforms ? upwardCF : groundCF, hit, minDistance);
            */

            count = utilityCollider.Cast(Vector2.up, checksPlatforms ? upwardCF : groundCF, hit, minDistance);
            //Debug.DrawRay(new Vector2(verticalCollider.bounds.center.x, verticalCollider.bounds.max.y), Vector2.right, Color.red);

            /*
            newoffset.y -= collidersOffset;
            newsize.y += collidersOffset * 2;
            verticalCollider.size = newsize;
            verticalCollider.offset = newoffset;
            */
        }

        else
        {

            count = verticalCollider.Cast(-Vector2.up, groundCF, hit, minDistance);

        }

        //float slopeAngle = 0f;
        //print("count: " + collisions.climbingSlope);

        if (count > 0)
        {
            if (velocity.y > 0 || (slopeAngle != 90 && slopeAngle != -90))
            {
                for (int i = 0; i < count; i++)
                {
                    if (hit[i].distance == 0)
                        continue;

                    //string otherLabel = hit[i].collider.tag;
                    //string otherLayer = LayerMask.LayerToName(hit[i].collider.gameObject.layer);
                    //print("Slopeangle: " + slopeAngle);
                    if ((velocity.y > 0 && climbedPlatform == hit[i].collider) ||
                        (platformsHit.Contains(hit[i].collider) || (((hit[i].collider.gameObject.layer == platformLayerIndex
                        || hit[i].collider.gameObject.layer == movingPlatformLayerIndex) && ((Mathf.Abs(slopeAngle) > maxSlopeAngle)
                        || collisions.isIgnoringPlatforms > 0)))))
                    //|| (collisions.isIgnoringStairs > 0 && otherLabel == "Stairs")))))
                    {

                        //print("BAU");
                        continue;

                    }
                    /*
                    if (stairsHit.Contains(hit[i].collider))
                    {
                        continue;
                    }
                    */

                    if (hit[i].distance <= minDistance)
                    {
                        //print("aloraaa");
                        minDistance = hit[i].distance;
                        if (velocity.y <= 0)
                        {// || collisions.climbingSlope)
                            collisions.below = true;
                            //print("below " + velocity.y);
                            //print(hit[i].collider.gameObject);
                            //Debug.DrawLine(hit[i].point, new Vector2(hit[i].point.x + 100, hit[i].point.y), Color.blue, 10000);
                            //Debug.DrawLine(hit[i].point, new Vector2(hit[i].point.x, hit[i].point.y + hit[i].distance), Color.blue, 10000);

                        }
                        else
                        {  // if (velocity.y > 0)
                            collisions.above = true;
                            //print("above");
                        }
                        //slopeAngle = Vector2.Angle(hit[i].normal, Vector2.up) * Mathf.Deg2Rad;
                        //print("collisions.below " + collisions.below + " climb: " + collisions.climbingSlope);
                    }
                }
            }

            //minDistance -= myCollider.bounds.size.y / 2;
            //print("mindistance: " + minDistance);

            
                if (velocity.y <= 0)
                {
                    velocity.y = -(minDistance - skinWidth);
                    //if (mystatus.faction == 0 && velocity.y > 0)
                    //  print("vert: " + minDistance + " " + velocity.y);
                }
                else if (velocity.y > 0)
                {
                    //se stai andando in su e il muro è dentro la skinwidth, non ti sposta in giu ma sta fermo
                    velocity.y = Mathf.Sign(velocity.y) * Mathf.Max(minDistance - skinWidth, 0);

                }


        }




        //print("fine vertical: " + velocity.y);
        //Debug.DrawLine(new Vector2(verticalCollider.bounds.center.x, verticalCollider.bounds.center.y - verticalCollider.bounds.extents.y), new Vector2(verticalCollider.bounds.center.x, verticalCollider.bounds.center.y - verticalCollider.bounds.extents.y + velocity.y), Color.green, 0.001f);



        //ATTENZIONE, DA TESTARE
        //newoffset = verticalCollider.offset;
        //newoffset.y += velocity.y;
        //verticalCollider.offset = newoffset;
        if (velocity.y != 0)
        {
            tr.Translate(0, velocity.y, 0);
            //Physics2D.SyncTransforms();
        }
        //-0.04158401
    }
}
