﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class LightIntensitySetterBasedOnGlobalLight : MonoBehaviour
{
    [Tooltip("Serve a settare l'intensità della luce in base all'intensità della global light. Serve per quando AlphaBlend della luce è vero, per evitare che si creino ombre al posto di luci.")]
    public bool whatAmI = false;
    private GlobalLightController gl;
    public Light2D myLight;
    public float additiveIntensity = 0.3f;

    private void Start()
    {
        gl = GlobalLightController.Instance ?? FindObjectOfType<GlobalLightController>();
        if (myLight != null)
            myLight.intensity = gl.globalLight.intensity + additiveIntensity;
    }
}
