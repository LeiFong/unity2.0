﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorOption : MonoBehaviour {

    int myfacing;
    bool timeOut;
    float previousDistance;
    protected float currentIdleDirection = -3;

    [System.Serializable]
	public struct exitTime { //l'exitTime viene scelto a caso all'interno dell'intervallo, in modo da rendere un po' più imprevedbile il comportmento 
		public int exitTimeMin;
		public int exitTimeMax;
	}

    [System.Serializable]
    public struct PotentialFollowup
    {
        public moveType moveType;
        public int movesIndex;
        public int probability;
    }

    [System.Serializable]
    public struct AlternateFollowup
    {
        public int arrayIndex;
        [HideInInspector]
        public int totProbability;
        [HideInInspector]
        public int roll;

        public float minRange;
        public float maxRange;
        public PotentialFollowup[] potentialFollowups;
    }

    //private float currentIdleDirection;
    public int cooldownFrames = 0;
    public int cooldownCounter = 0;
    public int timingVarianceMin, timingVarianceMax;
    public float maxDistance = 12;
    public int minimumIdleFrames = 0; //se non insegue, quanto deve stare in idle prima di iniziare a controllare il raggio

    public bool requiresTarget = true;
    public bool chaseWhenNotInRange;

    public Move[] behaviorSpecificMoves; //mosse comprese nel behavior; va usato per specificare l'esecuzione di quali mosse NON deve stoppare il behavior
    public BehaviorOption[] cooldownSharingBehaviors; //behavior che condividono il cooldown con questo behavior
    public BehaviorOption behaviourFollowupOnEnd;

    [Range(0, 1)]
    public float chaseSpeed = 1;
    public bool randomChaseSpeed = false;
    [Range(0, 1)]
    public float minChaseSpeed;
    [Range(0, 1)]
    public float maxChaseSpeed;
    [Range(0, 1)]
    public float minSpeedProbability;
    //public ContinuousChaseSpeedInterval continuousChaseSpeedInterval;
	public bool walkForwardIdle, walkBackIdle, stillIdle; //quando il behaviour è in idle, se cammina avanti o indietro; se entrambi veri, a caso
    [Range(0, 1)]
    public float walkBackProbability;
    public bool defendsWhileWalking = true;
    public float walkBackAtDistance;


    public float currentTargetDistance = -1;
    public float currentAdjustedTargetDistance;
	//public float targetDistance;
	public exitTime exitTimeInterval;
    public bool hardStopAtExitTime = false; //se vera, ferma sempre il behavior se raggiunto l'exittime
    
	public int currentExitTime; //numero di frame dopo i quali resetta il behaviour

	public Player controller;
	public BehaviourSelector bSelector;
    public int executedMovesIndex = 0;

    [System.Serializable]
    public struct ContinuousRangeInterval
    {
        [Range(0, 10)]
        public int minDistanceProbability;
        public float minDistance;
        [Range(0, 10)]
        public int maxDistanceProbability;
        public float maxDistance;
        //public bool setContinuousDistances;
        public float continuousMinDistance;
        public float continuousMaxDistance;
        public float bias; // > 1: tende a scegliere distanze più vicine alla maxdistance; < 1: tende a mindistance; = 1: uniforme

    }

    [System.Serializable]
    public struct ContinuousChaseSpeedInterval
    {
        [Range(0, 10)]
        public int minSpeedProbability;
        [Range(0, 1)]
        public float minSpeed;
        [Range(0, 10)]
        public int maxSpeedProbability;
        [Range(0, 1)]
        public float maxSpeed;
    }


    [System.Serializable]
    public struct Move
    {
        public moveType moveType;
        public int index;
    }

	public enum moveType {move, animation}; //se è una mossa nell'array di un'arma o se è una mossa nell'array delle animazioni generiche

	[System.Serializable]
	public struct moveIndex {
        //public bool hasInitiated;
		public int bseq;
        public int checkInterval;
		public int index;
		public moveType type;
        public BehaviorOption behaviourFollowup;
        public int activationPercentage;
        public int activationRoll;
        public bool continuousRangeChoice;
        public float targetMinDistance;
		public float targetMaxDistance;
        [Range(0, 9)]
        public int minDistanceProbability;
        //public Status.status[] forbiddenEnemyStatusConditions; //la mossa si attiva solo se il bersaglio NON è in nessuno di questi stati
        public Status.status[] requiredEnemyStatusConditions; //la mossa si attiva solo se il bersaglio è in uno di questi stati 
        public int customBuffer;
	}

    //public moveIndex move;
    public ContinuousRangeInterval continuousRangeInterval;
	public moveIndex[] moves;
    public AlternateFollowup[] alternateFollowups;
	public int movesIndex = 0;
    int currentTimingVariance;
    public bool chaseOver = false;
    float yuorAngle;
    private void AdjustTargetDistanceForSlopes()
    {
        currentAdjustedTargetDistance = currentTargetDistance;
        if (bSelector.targetController == null || controller.targetList.myTarget == null)
            return;

        if (moves.Length == 0 || controller.moves[moves[0].index].move == null || controller.moves[moves[0].index].move.hitboxes.Length == 0)
            return;

        yuorAngle = Mathf.Cos(Mathf.Deg2Rad * bSelector.targetAngle);
        if (yuorAngle == 0)
            yuorAngle = 1;

        //print("mov: " + controller.moves[moves[0].index].totalHorizontalMovements + " cos: " + yuorAngle);
        //currentAdjustedTargetDistance = (Mathf.Max(0, currentTargetDistance - controller.moves[moves[0].index].totalHorizontalMovements) /
        //  yuorAngle) + controller.moves[moves[0].index].totalHorizontalMovements;

        currentAdjustedTargetDistance = Mathf.Max(0, currentTargetDistance - controller.moves[moves[0].index].totalHorizontalMovements)
            + Mathf.Min(currentTargetDistance, controller.moves[moves[0].index].totalHorizontalMovements) * yuorAngle;

        //print("adjusted: " + currentAdjustedTargetDistance);
    }

    public void SetAlternateFollowups ()
    {
        for (int i = 0; i < alternateFollowups.Length; i++)
        {
            if (alternateFollowups[i].arrayIndex < 0 || alternateFollowups[i].arrayIndex > moves.Length - 1)
            {
                print("errore alternate followups");
                continue;
            }

            if (bSelector.currentDistance < alternateFollowups[i].minRange || bSelector.currentDistance > alternateFollowups[i].maxRange)
                continue;

            alternateFollowups[i].roll = Random.Range(0, alternateFollowups[i].totProbability);
            int probabilitySum = 0;
            for (int j = 0; j < alternateFollowups[i].potentialFollowups.Length; j++)
            {
                probabilitySum += alternateFollowups[i].potentialFollowups[j].probability;
                if (alternateFollowups[i].roll < probabilitySum)
                {
                    moves[alternateFollowups[i].arrayIndex].type = alternateFollowups[i].potentialFollowups[j].moveType;
                    moves[alternateFollowups[i].arrayIndex].index = alternateFollowups[i].potentialFollowups[j].movesIndex;
                    break;
                }

            }
        }
    }
    public void LoadInformations () {
        //controller = bSelector.controller;
        //print("loadinfos " + gameObject);
        chaseOver = false;
		controller.FlushBuffers ();
        currentExitTime = Random.Range (exitTimeInterval.exitTimeMin, exitTimeInterval.exitTimeMax + 1);
        //currentExitTime = Random.Range(0f, 1f) < 0.5f ? exitTimeInterval.exitTimeMin : exitTimeInterval.exitTimeMax;
        //print("exittime: " + currentExitTime);
        SetAlternateFollowups();

        if (timingVarianceMax != 0)
        {
            currentTimingVariance = Random.Range(timingVarianceMin, timingVarianceMax + 1);
            //print("currente variance: " + currentTimingVariance);
        }
        for (int i = 0; i < moves.Length; i++)
        {
            if (moves[i].activationPercentage == 0)
                moves[i].activationPercentage = 100;
            if (moves[i].minDistanceProbability == 0)
                moves[i].minDistanceProbability = 5;
            moves[i].activationRoll = Random.Range(0, 100); //per indicare che non ha ancora rollato

        }


        currentTargetDistance = -1;
        if (chaseWhenNotInRange && randomChaseSpeed)
        {
            float rand = Random.Range(0f, 1f);


            chaseSpeed = rand < minSpeedProbability ? minChaseSpeed : maxChaseSpeed;
            //print("loaded " + rand);

        }

        if (!chaseWhenNotInRange)
        {
            if (walkBackIdle && walkForwardIdle)
                currentIdleDirection = (Random.Range(0, 1f) > walkBackProbability ? 1 : -1) * (controller.facingRight ? 1 : -1);
            else
            {
                if (walkForwardIdle)
                    currentIdleDirection = controller.facingRight ? 1 : -1;
                else
                    currentIdleDirection = controller.facingRight ? -1 : 1;
            }
        }
        //print ("exittime: " + currentExitTime);
        movesIndex = 0;
        executedMovesIndex = -1;
        MyLoadInformations();
	}

    public void BehaviourInitialization ()
    {
        for (int i = 0; i < alternateFollowups.Length; i++)
        {
            alternateFollowups[i].totProbability = 0;
            for (int j = 0; j < alternateFollowups[i].potentialFollowups.Length; j++)
            {
                alternateFollowups[i].totProbability += alternateFollowups[i].potentialFollowups[j].probability;
            }
        }
        MyBehaviourInitialization();
    }
    private bool CheckEnemyStatusConditions (int optionIndex)
    {
        if (bSelector.targetController != null)
        {
            
            for (int i = 0; i < moves[optionIndex].requiredEnemyStatusConditions.Length; i++)
            {
                if (bSelector.targetController.status.mystatus == moves[optionIndex].requiredEnemyStatusConditions[i])
                {
                    return true;
                }
            }
        }
        else
            return true;
        return false;
    }

    public virtual void MyBehaviourInitialization ()
    {

    }

    public virtual void MyLoadInformations()
    {

    }
    

	public virtual void ExecuteBehaviour() {


        if (requiresTarget && bSelector.currentDistance > maxDistance)
        {
            StopBehaviour();
            return;
        }

        if (controller.currentMove == null)
        {
            if (defendsWhileWalking)
                bSelector.canDefend = true;
            else
            {
                bSelector.canDefend = false;
                controller.animator.SetBool(controller.directionLockIndex, false);

            }
        }
        else
        {
            bSelector.canDefend = true;
            controller.animator.SetBool(controller.directionLockIndex, true);
        }

        if (moves.Length > 0) {

            //if (moves.Length > 0)
            {
                if ((moves[movesIndex].type == moveType.move && controller.currentMove == controller.moves[moves[movesIndex].index]) ||
                    (moves[movesIndex].type == moveType.animation && controller.currentMove == controller.animations[moves[movesIndex].index]))
                {
                    //if (!moves[movesIndex].hasInitiated)
                    {
                        executedMovesIndex = movesIndex;
                        //moves[movesIndex].hasInitiated = true;
                        if (moves.Length > movesIndex + 1)
                            movesIndex++;
                    }
                }



                if (executedMovesIndex != -1)
                {
                    //controllo se sta facendo mosse non comprese nel vettore
                    if ((moves[executedMovesIndex].type == moveType.move && controller.currentMove != controller.moves[moves[executedMovesIndex].index]) ||
                    (moves[executedMovesIndex].type == moveType.animation && controller.currentMove != controller.animations[moves[executedMovesIndex].index]))
                    {
                        bool stopBehavior = true;

                        
                        for (int i = 0; i < behaviorSpecificMoves.Length; i++)
                        {
                            
                            if ((behaviorSpecificMoves[i].moveType == moveType.move && controller.currentMove == controller.moves[behaviorSpecificMoves[i].index]) ||
                                (behaviorSpecificMoves[i].moveType == moveType.animation &&
                                controller.currentMove == controller.animations[behaviorSpecificMoves[i].index]))
                            {
                                stopBehavior = false;
                                break;
                            }
                        }

                        if (stopBehavior)
                        {
                            //print("Stoppate" + gameObject + " " + bSelector.bseq);
                            StopBehaviour();
                            return;
                        }

                    }
                }

            }


            //se è la prima mossa da eseguire
            if (movesIndex == 0) {

                if (currentTargetDistance == -1)
                {
                    if (!moves[movesIndex].continuousRangeChoice)
                    {
                        //currentTargetDistance = Random.Range(moves[movesIndex].targetMinDistance, moves[movesIndex].targetMaxDistance);
                        currentTargetDistance = Random.Range(0, 10) < moves[movesIndex].minDistanceProbability ? moves[movesIndex].targetMinDistance : moves[movesIndex].targetMaxDistance;
                    }
                    else
                    {
                        int r = Random.Range(0, 10);
                        //print("r: " + r);
                        if (continuousRangeInterval.minDistanceProbability != 0 && r < continuousRangeInterval.minDistanceProbability)
                        {
                            currentTargetDistance = continuousRangeInterval.minDistance;
                            //print("minima: " + currentTargetDistance + " - " + gameObject);
                        }
                        else if (continuousRangeInterval.maxDistanceProbability != 0 && r >= 10 - continuousRangeInterval.maxDistanceProbability)
                        {
                            currentTargetDistance = continuousRangeInterval.maxDistance;
                            //print("massima: " + currentTargetDistance + " - " + gameObject);

                        }
                        else
                        {
                            //float pepe = 0;
                            //for (int g = 0; g < 100; g++)
                            {

                                float minDist, maxDist;
                                minDist = continuousRangeInterval.continuousMinDistance != 0 ? continuousRangeInterval.continuousMinDistance : continuousRangeInterval.minDistance;
                                maxDist = continuousRangeInterval.continuousMaxDistance != 0 ? continuousRangeInterval.continuousMaxDistance : continuousRangeInterval.maxDistance;

                                if (continuousRangeInterval.bias == 0)
                                    continuousRangeInterval.bias = 1;

                                float interval = Mathf.Pow((maxDist - minDist), continuousRangeInterval.bias);
                                currentTargetDistance = Random.Range(0, interval);
                                currentTargetDistance = Mathf.Pow(currentTargetDistance, 1 / continuousRangeInterval.bias);
                                currentTargetDistance += minDist;

                                //pepe += currentTargetDistance;
                                
                                //print("continua: " + currentTargetDistance + " (min: " + minDist + " max: " + maxDist + " - " + gameObject);
                            }
                            
                            //pepe /= 100;
                            //print("media: " + pepe);
                            
                        }
                    }
                    //print("current: " + currentTargetDistance + " " + gameObject);
                }

                AdjustTargetDistanceForSlopes();

                if (chaseWhenNotInRange && !chaseOver)
                {
                    //controller.horizontal = controller.facingRight ? -chaseSpeed : chaseSpeed;
                    controller.horizontal = 0;

                    //print("vai indietro" + gameObject);

                }

                /*
                if (!chaseWhenNotInRange)
                {
                    if (bSelector.bseq >= minimumIdleFrames)
                    {
                        controller.animator.SetBool(controller.directionLockIndex, false);
                    }
                }
                */

                //insegui se non sei in raggio
                //if (chaseWhenNotInRange || bSelector.bseq >= minimumIdleFrames)
                //if (requiresTarget)
                {
                    if (requiresTarget && !chaseOver && ((chaseWhenNotInRange && bSelector.currentDistance > currentAdjustedTargetDistance) 
                        || (!chaseWhenNotInRange && (bSelector.currentDistance > currentAdjustedTargetDistance || bSelector.bseq < minimumIdleFrames))))
                    {
                        if (chaseWhenNotInRange)
                        {
                            controller.horizontal = controller.facingRight ? chaseSpeed : -chaseSpeed;
                            //print("sono " + controller.characterContainer + " " + bSelector.currentDistance + " " + (previousDistance - bSelector.currentDistance));
                            previousDistance = bSelector.currentDistance;
                            /*
                            //se sei troppo vicino, allontanati
                            if (bSelector.currentDistance < walkBackAtDistance)
                            {
                                controller.horizontal = controller.facingRight ? -0.5f : 0.5f;
                                //chaseOver = true;
                            }
                            */
                        }
                        else
                        {
                            myfacing = controller.facingRight ? 1 : -1;
                            if (walkForwardIdle && walkBackIdle)
                            {

                                controller.horizontal = currentIdleDirection * chaseSpeed;

                            }
                            else if (walkForwardIdle)
                            {
                                if (bSelector.currentDistance < walkBackAtDistance && ((myfacing == 1 && currentIdleDirection > 0)
                                    || (myfacing == -1 && currentIdleDirection < 0)))
                                    currentIdleDirection = -currentIdleDirection;

                                controller.horizontal = currentIdleDirection * chaseSpeed;

                            }
                            else if (walkBackIdle)
                                controller.horizontal = -myfacing * chaseSpeed;

                        }
                    }




                    //riempi il buffer della mossa/animazione se invece sei in raggio
                    else
                    {
                        if (!chaseOver)
                            bSelector.bseq = 0;
                        chaseOver = true;
                        if (bSelector.bseq >= moves[movesIndex].bseq + currentTimingVariance)
                        {
                            //print("vado: " + gameObject + " " + moves[movesIndex].bseq + " " + currentTimingVariance + " , " + bSelector.bseq);

                            if (moves[movesIndex].requiredEnemyStatusConditions.Length > 0 && !CheckEnemyStatusConditions(movesIndex))
                            {
                                StopBehaviour();
                                //return;
                            }
                            else
                            {
                                //bSelector.bseq = 0;
                                if (moves[movesIndex].behaviourFollowup == null)
                                {
                                    if (moves[movesIndex].type == moveType.move)
                                    {

                                        if (executedMovesIndex < movesIndex && controller.enemyBuffers[moves[movesIndex].index] == 0)
                                        {

                                            controller.enemyBuffers[moves[movesIndex].index] = moves[movesIndex].customBuffer == 0 ? controller.bufferLength : moves[movesIndex].customBuffer;
                                            bSelector.bseq = 0;
                                            controller.horizontal = 0;
                                            //print("arrivato: sono " + controller.characterContainer + " " + bSelector.currentDistance + " " + currentAdjustedTargetDistance);// + " " + (previousDistance - bSelector.currentDistance));
                                            Debug.DrawRay(controller.hurtboxes[0].hurtbox.bounds.min, Vector2.down, Color.green, 1000);
                                            //firstMovestarted = true;
                                            //print("1");
                                            if (movesIndex + 1 == moves.Length)
                                            {

                                                {
                                                    //print("frame 11" + Time.fixedTime);
                                                    //StopBehaviour();
                                                }
                                            }



                                        }
                                    }
                                    else
                                    {
                                        if (executedMovesIndex < movesIndex && controller.enemyAnimationsBuffers[moves[movesIndex].index] == 0)
                                        {
                                            controller.enemyAnimationsBuffers[moves[movesIndex].index] = moves[movesIndex].customBuffer == 0 ? controller.bufferLength : moves[movesIndex].customBuffer;
                                            bSelector.bseq = 0;
                                            controller.horizontal = 0;
                                            //firstMovestarted = true;
                                            //print("2 " + gameObject);
                                            if (movesIndex + 1 == moves.Length)
                                            {

                                                {
                                                    //print("frame 22" + Time.fixedTime);
                                                    //StopBehaviour();
                                                }
                                            }



                                        }
                                    }
                                }
                                else
                                {
                                    //StopBehaviour();
                                    bSelector.StartNewBehaviour(moves[movesIndex].behaviourFollowup);
                                    
                                    return;
                                }
                            }
                        }
                        else
                        {
                            //currentDirection = -currentDirection;
                            //print("torna indetro cazzo");
                            //if (bSelector.currentDistance < walkBackAtDistance)
                            if (chaseWhenNotInRange)
                                controller.horizontal = controller.facingRight ? -chaseSpeed : chaseSpeed;

                        }
                    }
                }

		}
        //se invece non è la prima mossa da eseguire
        else
        {

                //if (bSelector.bseq == moves [movesIndex].bseq + currentTimingVariance)
                if (bSelector.bseq >= moves[movesIndex].bseq + currentTimingVariance
                    && bSelector.bseq <= moves[movesIndex].bseq + currentTimingVariance + moves[movesIndex].checkInterval)
                {
                    //print("checkcheck " + bSelector.bseq);
                    //fermati/passa al prossimo followup se sei troppo vicino/lontano o se fallisce l'activation roll
                    if (moves[movesIndex].activationRoll > moves[movesIndex].activationPercentage || (bSelector.currentDistance > moves [movesIndex].targetMaxDistance || bSelector.currentDistance < moves[movesIndex].targetMinDistance ||
                        (moves[movesIndex].requiredEnemyStatusConditions.Length > 0 && !CheckEnemyStatusConditions(movesIndex)))) 
                    {
                        //print("alora");

                        if (bSelector.bseq == moves[movesIndex].bseq + currentTimingVariance + moves[movesIndex].checkInterval)
                        {
                            //print("boooooh " + bSelector.bseq);
                            if (movesIndex == moves.Length - 1)
                            {
                                //StopBehaviour();
                            }
                            else
                                movesIndex++;
                        }
				    }
                    else
                    {
                    if (moves[movesIndex].behaviourFollowup == null)
                    {
                            if (moves[movesIndex].type == moveType.move)
                            {

                                if (executedMovesIndex < movesIndex && controller.enemyBuffers[moves[movesIndex].index] == 0)
                                {
                                    controller.enemyBuffers[moves[movesIndex].index] = moves[movesIndex].customBuffer == 0 ? controller.bufferLength : moves[movesIndex].customBuffer;
                                    bSelector.bseq = 0;

                                    //firstMovestarted = true;
                                    //print("3");
                                    if (movesIndex + 1 == moves.Length)
                                    {

                                        {
                                            //print("frame 33" + Time.fixedTime);
                                            //StopBehaviour();
                                        }
                                    }

                                }
                            }
                            else
                            {
                                if (executedMovesIndex < movesIndex && controller.enemyAnimationsBuffers[moves[movesIndex].index] == 0)
                                {
                                    controller.enemyAnimationsBuffers[moves[movesIndex].index] = moves[movesIndex].customBuffer == 0 ? controller.bufferLength : moves[movesIndex].customBuffer;
                                    bSelector.bseq = 0;
                                    //firstMovestarted = true;
                                    print("4");
                                    if (movesIndex + 1 == moves.Length)
                                    {

                                        {
                                            //print("frame 44" + Time.fixedTime);
                                            //StopBehaviour();
                                        }
                                    }

                                }

                            }
                    }
                    else
                    {
                            if (moves[movesIndex].behaviourFollowup.cooldownCounter <= 0)
                                bSelector.StartNewBehaviour(moves[movesIndex].behaviourFollowup);
                            else
                            {
                                if (movesIndex == moves.Length - 1)
                                {
                                    //StopBehaviour();
                                }
                                else
                                    movesIndex++;
                            }
                            return;

                    }

				    }
			}
		}

		}









        //print("myexe" + Time.frameCount);

        MyExecuteBehaviour();

		//if ((bSelector.constantBehaviour != null && bSelector.currentBehaviour != this) || bSelector.constantBehaviour == null) {

		if (controller.hitstop <= 0)
			bSelector.bseq++;

        
            if ((hardStopAtExitTime || !chaseOver) && bSelector.bseq >= currentExitTime + currentTimingVariance && bSelector.currentBehaviour != null)
            {
            //print ("time to stop beh, currentexitTime = " + gameObject +" " + currentExitTime);
            timeOut = true;
            bSelector.currentBehaviour.StopBehaviour();
            }
        


        
        //}

        //bSelector.BehaviourSelect ();

    }

	public virtual void MyExecuteBehaviour () {
		//print ("la mia variabile è " + variabile);

		/*
		if (bSelector.bseq == 60) {
			controller.atk1Buffer = controller.bufferLength;


			bSelector.bseq = 0;
		}
		*/

		

		/* 
		sei alla prima mossa dell'array?
			sì:
				in raggio?
					no: chase
					si: resetto bseq, esegui mossa, scorri array
			no:
			se bseq > tot
				in raggio? 
					no: stop
					sì: 
						esegui mossa
						è l'ultima mossa dell'array?
							no: scorri array
							sì: stop





		*/

        //se è la prima mossa della sequenza e non sei in raggio, insegui/idle; se non è la prima mossa della sequenza
        //e non sei in raggio, ferma il behaviour
		//if (moves.Length > 0)
		




		/*
			if (bSelector.currentDistance > targetDistance)
				controller.horizontal = controller.facingRight ? 1 : -1;
			else {
				if (controller.enemyBuffers [move.index] == 0)
					controller.enemyBuffers [move.index] = controller.bufferLength;
				StopBehaviour ();
			}
		*/
							


	

		



	}

	public void StopBehaviour() {
        
        //firstMovestarted = false;
        executedMovesIndex = -1;
		bSelector.currentBehaviour = null;
		bSelector.bseq = 0;
		movesIndex = 0;
        cooldownCounter = cooldownFrames;
        for (int i = 0; i < cooldownSharingBehaviors.Length; i++)
        {
            cooldownSharingBehaviors[i].cooldownCounter = cooldownSharingBehaviors[i].cooldownFrames;
        }
        //print("stop " + gameObject + " " + cooldownCounter);
        currentTargetDistance = -1;
        
        MyStopBehaviour ();

        if (timeOut && behaviourFollowupOnEnd != null)
        {
            timeOut = false;
            bSelector.StartNewBehaviour(behaviourFollowupOnEnd);

        }
        
	}

	public virtual void MyStopBehaviour() {
		//controller.horizontal = 0;
		//controller.FlushBuffers ();
	}
}
