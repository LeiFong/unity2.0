﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HitsparkGenerator : MonoBehaviour {

    
    public Transform sparkOriginPosition;
    [Range(0, 120)]
    public int defaultTimeToLive = 30;
    [Range(0f, 360f)]
    public float defaultAngleSpread = 30f;
    [Range(0f, 10f)]
    public float defaultGravity = 5f;
    [Range(0f, 10f)]
    public float positionSpread = 1f;
    
    public List<HitsparkBehavior> hitSparks;
    private int activeIndex;

	// Use this for initialization
	void Start () {
        hitSparks = new List<HitsparkBehavior>(GetComponentsInChildren<HitsparkBehavior>());
        //print("spark: " + hitSparks.Count);
	}
	
	// Update is called once per frame
	

    public void GenerateHitsparks(Vector2 speed, int quantity, int timeToLive, float angleSpread, float gravity) {

        if (hitSparks.Count == 0)
        {
            print("no hitspark");

            return;

        }

        print("hitspark");

        for (int i = 0; i < quantity; i++) {

            activeIndex = (activeIndex + 1) % hitSparks.Count;
            Quaternion rq = Quaternion.AngleAxis(Random.Range(-angleSpread / 2f, angleSpread / 2f), Vector3.forward);
            Vector2 randomizedSpeed = rq * speed;
            randomizedSpeed = new Vector2(Random.Range(randomizedSpeed.x / 3f, randomizedSpeed.x), Random.Range(randomizedSpeed.y / 3f, randomizedSpeed.y));

            hitSparks[activeIndex].DisplaySparks(new Vector2(randomizedSpeed.x / 100, randomizedSpeed.y / 100), timeToLive);
            hitSparks[activeIndex].transform.position = new Vector2(sparkOriginPosition.position.x + positionSpread * randomizedSpeed.x / 100,
               sparkOriginPosition.position.y + positionSpread * randomizedSpeed.y / 100);
            hitSparks[activeIndex].gravity = gravity / 100;
            
        }
        
	}
    public void GenerateHitsparks(Vector2 speed, int quantity) {
        GenerateHitsparks(speed, quantity, defaultTimeToLive, defaultAngleSpread, defaultGravity);
    }
    public void GenerateHitsparks(Vector2 speed, int quantity, int timeToLive)
    {
        GenerateHitsparks(speed, quantity, timeToLive, defaultAngleSpread, defaultGravity);
    }
    public void GenerateHitsparks(Vector2 speed, int quantity, float angleSpread)
    {
        GenerateHitsparks(speed, quantity, defaultTimeToLive, angleSpread, defaultGravity);
    }


}
