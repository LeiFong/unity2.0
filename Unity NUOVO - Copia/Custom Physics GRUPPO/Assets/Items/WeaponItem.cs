﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class WeaponItem : Item
{
    public enum WeaponType
    {
        sword, axe, hammer, daggers, etc
    }
    public int weaponIndex; 
    
   
}
