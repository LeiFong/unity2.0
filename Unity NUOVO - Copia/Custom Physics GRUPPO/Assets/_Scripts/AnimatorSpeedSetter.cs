﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorSpeedSetter : MonoBehaviour
{

    public Animator animator;
    public float speed = 1f;
    [Range(0f, 2f)]
    public float randomizer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        SetSpeed();
    }

    public void SetSpeed() {
        if (animator != null)
            animator.speed = (speed + Random.Range(-randomizer / 2, randomizer / 2)) / TimeVariables.currentTimeScale;
    }
}
