﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileLauncherBehaviour : BehaviorOption
{
    public MovingProjectile projectile;
    public bool aimsTarget;
    public int adjustedAngle;
    public int positionSnaphotFrame;

    public override void MyBehaviourInitialization()
    {
        projectile = (MovingProjectile) controller.moves[moves[0].index].move.projectiles[0].proj;
    }


    public override void MyExecuteBehaviour()
    {
        if (aimsTarget && controller.seq == positionSnaphotFrame)
            AdjustProjectileAngle();
    }

    void AdjustProjectileAngle()
    {
        if (controller.targetList.myTarget == null)
            return;

        
        //calcolo l'angolo tra me e il bersaglio
        
        projectile.initialDirectionAngle = (Mathf.Atan2((controller.targetList.myTarget.transform.position.y - projectile.startingPosition.y),
            Mathf.Abs(controller.targetList.myTarget.transform.position.x - projectile.startingPosition.x)) * Mathf.Rad2Deg) + adjustedAngle;

        Debug.DrawRay(projectile.startingPosition, new Vector3(Mathf.Cos(projectile.initialDirectionAngle), Mathf.Sin(projectile.initialDirectionAngle)), Color.yellow, 1);
        //print(projectile.initialDirectionAngle);

    }
}
