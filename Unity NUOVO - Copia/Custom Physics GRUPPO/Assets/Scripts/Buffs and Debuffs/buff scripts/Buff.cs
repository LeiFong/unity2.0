﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buff : MonoBehaviour {

	
    //public string buffName; //chiave insieme all'owner; un personaggio non può avere due buff/debuff con lo stesso nome E stesso owner
    public BuffScriptableObject buffScriptableObject;
    //public int durationTimer;
    //public int buffTotalDuration;
    //public int maxStacks = 1;
    //public int currentStacks = 0;
    //public bool active = false;
    //public Controller2D owner;
    //private int buffIndex = 0; //la posizione del buff nell'array

    public virtual void InitializeBuff(Controller2D victim)
    {

    }

	public virtual void ApplyEffect (Controller2D victim, BuffManager.BuffProperties buffProperties) {
        //print("buffed " + durationTimer);
	}

    
		
}

