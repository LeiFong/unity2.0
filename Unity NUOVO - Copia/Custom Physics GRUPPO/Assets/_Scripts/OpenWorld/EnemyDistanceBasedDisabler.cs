﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EnemyDistanceBasedDisabler : MonoBehaviour
{
    [Tooltip("Mettimi nel root GameObject di una scena contenente i nemici. Io a build time (su disable) retrievo i Controller2D presenti nella scena; poi a run time" +
        " controllo la distanza di distanceChecksPerFrame nemici dal giocatore, e se sono più distanti di checkedDistance allora ne disabilito la logica e la fisica." +
        " Questo serve per fare sì che i nemici possano camminare liberamente fra diverse scene, senza preoccuparsi che il pavimento gli venga scaricato dalla ram da sotto" +
        " i piedi. A proposito: il requisito mandatorio di questo è che la checkedDistance sia MINORE della distanza necessaria per scaricare una scena; se così non è," +
        " allora può capitare che la scena su cui sta un nemico venga scaricata dalla ram mentre il nemico è ancora attivo e sta ancora eseguendo. In poche parole," +
        " PRIMA devo disabilitare il nemico, e POI scarico la scena.")]
    public bool whatAmI = false;
    [Space(10)]
    public int distanceChecksPerFrame = 20;
    public float checkedDistance = 20f;
    public bool previewCheckedDistance = false;
    private int currentIndex = 0;
    [SerializeField]
    public List<CharacterContainer> enemies;
    private PlayersManager pm;
    private Transform playerTransform;

    private void Awake()
    {
        if (!Application.isPlaying)
            return;

        pm = PlayersManager.Instance ?? FindObjectOfType<PlayersManager>();
        playerTransform = pm.player1.tr;
    }

    private void OnDisable()
    {
        if (Application.isPlaying)        
            return;

        enemies = new List<CharacterContainer>(GetComponentsInChildren<CharacterContainer>());

        
    }
    
    void FixedUpdate()
    {
        if (!Application.isPlaying)
            return;

        if (currentIndex >= enemies.Count)
            currentIndex = 0;

        for (int i = 0; i < distanceChecksPerFrame; i++) {

            if (Mathf.Abs(playerTransform.position.x - enemies[currentIndex].controller.tr.position.x) < checkedDistance &&
                Mathf.Abs(playerTransform.position.y - enemies[currentIndex].controller.tr.position.y) < checkedDistance)
            {
                //ABILITA LOGICA QUI
                if (!enemies[currentIndex].gameObject.activeSelf)
                    enemies[currentIndex].gameObject.SetActive(true);
            }
            else {
                //DISABILITA LOGICA QUI
                if (enemies[currentIndex].gameObject.activeSelf)
                    enemies[currentIndex].gameObject.SetActive(false);

            }

            currentIndex++;
            
        }

    }

    private void OnDrawGizmosSelected()
    {
        if (previewCheckedDistance)
            Gizmos.DrawWireSphere(transform.position, checkedDistance);

    }
}
