﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourSelector : MonoBehaviour {
    Status.status previousStatus;
	public bool skipBehaviourExecution = false;
    //[HideInInspector]
    public bool canDefend;
    bool lul, poggers;
    //public EnemyDetector enemyDetector;
	public BehaviorOption onEngageBehaviour;
    public BehaviorOption onEndBehaviour; //cosa fa prima di disingaggiare
    public BehaviorOption disengagedBehaviour; //cosa fa se non è in combat
    public bool disangagedBehaviourOnlyInsideSpawnZone; //se deve essere nella sua spawnZone per poter fare il disengagedBehaviour
    public BehaviorOption differentCompetenceZoneBehavior; //cosa fa quando è ingaggiato con un bersaglio in una diversa competenceZone; se lasciato vuoto, continua con la normale scelta dei behavior
    public bool isEngaged; //se è in combat con qualcuno
	public BehaviorOption constantBehaviour; //codice che viene eseguito sempre, a prescindere dal currentBehaviour selezionato
    public Status.status[] behaviourInterruptingStatuses; //array di status che interrompono il behaviour una volta che ci si trova in essi
    //public AIMovements targetsAIMovements;
    public bool canReturnToSpawnZone = false;
    public bool newEngagement = false;
	[System.Serializable]
	public struct behaviourProbability {
		public BehaviorOption behavior;
		public int probability;
	}

	[System.Serializable]
	public struct range {
		public float rangeStart, rangeEnd;
		public behaviourProbability[] behaviorProbabilities;
	}

	[System.Serializable]
	public struct statusConditions {
		public Status.status statusCondition;
		public range[] ranges;
	}

    [System.Serializable]
    public struct LowStaminaBehaviours
    {
        [Range(0, 1)]
        public float staminaPercentage;
        public BehaviorOption behaviour;
    }

	public statusConditions[] rangesPerStatus;
    public LowStaminaBehaviours[] lowStaminaBehaviours;

    public bool isDisengaging;


    public Player controller;
    public Player targetController;

	//public range[] ranges;
	public int bseq = 0;

	/* il problema è che un giocatore bravo non userà mai le mosse con metro
	 * perchè perfectblocckerà tutto
	 * 
	 * però il problema un po' rimane, un giocatore bravo ne avrà sempre meno bisogno
	 * a me piaerebbe che le mosse con metro non siano dominate se non hai vita bianca, 
	 * fanno comunque più danno dell'opzione corrispettiva
	 * se hai vita bianca ancora meglio
	 * 
	 * guarda bloodborne, non è che la pistola sia inutile se non hai vita bianca
	 * 
	 * okay
	 * 
	 * ma
	 * 
	 * il FLAVOR
	 * 
	 * idea
	 * se fai che il metro non fa altro che curarti della vita bianca
	 * non è una mossa con un hitbox, non è un attacco
	 * però ci sono mosse che magari fanno cacheiru normalmente, MA se colpiscono ti fanno curare safe
	 * magari perchè danno knockdown
	 * 
	 * è un flash of light
	 * 
	 * idea
	 * niente vita bianca
	 * blocchi con il braccio
	 * bloccare col braccio carica una barra, che piano piano si depleta
	 * praticamente bloccare serve a "sovraccaricare" il braccio
	 * la barra la puoi spendere per fare mosse DEVASTANTIIIII
	 * perfect bloccare carica di più
	 * giustificherebbe il fatto che i nemici non bloccano: non hanno il braccio
	 * la mossa che usa il metro fa più danno quanto più metro hai accumulato
	 * 
	 * allo stesso modo, se non blocchi non hai mai vita bianca?
	 * ho solo sostituito la vita bianca con qualcos'altro
	 * la funzione è uguale, cambia solo il flavor
	 * 
	 * non mi dispiace...
	 * 
	 * rispondo a ciascuna riga
	 * 
	 * non capisco perchè
	 * neanche la vita bianca lo è
	 * anche con la vita bianca
	 * anche con la vita bianca. se perfect blocchi non fai vita bianca, che è la stessa cosa di esserti curato istantaneamente
	 * 
	 * 
	 * fermati
	 * non fai metro se vieni colpito, ne se colpisci
	 * non c'è il metro
	 * c'è solo questa barra
	 * 
	 * cioè, il punto è: 
	 * è identico al tuo sistema
	 * l'unica cosa che cambia è che invece di curarti, fai più danno
	 * 
	 * no, non è la stessa cosa
	 * con la vita bianca prendi effettivamente danno a bloccare
	 * 
	 * 
	 * 
	 * 
	 * */



	public Vector2 myPosition, targetPosition;
	public float currentDistance;
    public float targetAngle;

    public BehaviorOption currentBehaviour;
	public BehaviourInitializer bInitializer;
    public AIMovements aiMovements;

	public void InitializeBehaviours () {
        //controller = GetComponentInParent<EnemyPlayer> ();
        //print (controller);
        isDisengaging = false;
        //aiMovements = GetComponentInChildren<AIMovements>();
		//bInitializer = GetComponentInChildren<BehaviourInitializer>();
		if (bInitializer != null) {
			//bInitializer.InitializeBehaviours ();
			bInitializer.controller = controller;
			bInitializer.bselector = this;
			bInitializer.InitializeBehaviours ();


		

		}
			
	}

    public void CheckInterruptingStatuses ()
    {

        //if (currentBehaviour == null)
          //  return;

        //print("stopppp: " + controller.status.mystatus + " prev: " + controller.status.myPreviousStatus + " " + currentBehaviour);

        for (int i=0; i < behaviourInterruptingStatuses.Length; i++)
        {
            //print ("ma alora: " + controller.status.mystatus + " prev: " + controller.status.myPreviousStatus + " " + currentBehaviour);
            //if (controller.status.mystatus == behaviourInterruptingStatuses[i] && previousStatus != behaviourInterruptingStatuses[i])
            if (controller.status.mystatus == behaviourInterruptingStatuses[i] && controller.seq == 0) //da testare
            {
                //print("stoppp " + currentBehaviour);
                if (currentBehaviour != null)
                currentBehaviour.StopBehaviour();
               

                break;
            }
        }

        previousStatus = controller.status.mystatus;
    }

    private void OnDrawGizmosSelected()
    {
        if (controller == null)
        {
            return;
        }
        for (int i = 0; i < rangesPerStatus[0].ranges.Length; i++) {

            Gizmos.DrawWireSphere(controller.transform.position, rangesPerStatus[0].ranges[i].rangeEnd);

        }
    }

    public void BehaviourSelect ()
	{


        if (currentBehaviour == null || controller.status.myPhysicsStatus == Status.physicsStatus.airborne ||
            (controller.currentMove != null && !controller.currentMove.canWalkWhenGrounded))
        {
            controller.horizontal = 0;
        }
        //CheckInterruptingStatuses();
        //controller.targetList.myTargetEnemy = controller.targetList.potentialEnemiesList[controller.targetList.potentialEnemiesList.IndexOf(]
        //print ("PPP");
        if (controller.targetList.hasDetected && controller.targetList.myTarget == null)// && (aiMovements == null || aiMovements.returningToSpawnZone == false))
        {
            //print("sono: " + controller.gameObject);
            controller.targetpressed = true;
        }
        else
        {
            controller.targetpressed = false;
            controller.animator.SetBool(controller.directionLockIndex, true);
        }

        if (controller.targetList.myTarget != null && targetController == null)
        {
            targetController = controller.targetList.myTarget.GetComponentInParent<Player>();
        }

        for (int i = 0; i < bInitializer.behaviours.Length; i++)
        {
            if (bInitializer.behaviours[i] != null && bInitializer.behaviours[i].cooldownCounter > 0)
            {
                bInitializer.behaviours[i].cooldownCounter--;
            }
        }
        /*
        if (newEngagement)
        {
            newEngagement = false;
            isEngaged = true;
            if (aiMovements != null)
                aiMovements.isInsideSpawnZone = false;
            if (onEngageBehaviour != null)
            {
                //print("engage");
                StartNewBehaviour(onEngageBehaviour);
                //return;
            }
        }
        */
		myPosition = controller.tr.position;
        //myPosition = (controller.tr.position.x -  controller.hurtboxes[0].hurtbox.bounds.max : controller.hurtboxes[0].hurtbox.bounds.min)

        if (controller.targetList.myTarget != null)
        {
            bool targetIsOnTheLeft = controller.targetList.myTarget.transform.position.x < controller.tr.position.x;
            
            if (targetController == null || targetController.hurtboxes.Length == 0)
			    targetPosition = controller.targetList.myTarget.transform.position;
            else
            {
                targetPosition.x = targetIsOnTheLeft ? targetController.hurtboxes[0].hurtbox.bounds.max.x : targetController.hurtboxes[0].hurtbox.bounds.min.x;
                //targetPosition.y = targetController.tr.position.y;
                targetPosition.y = targetController.hurtboxes[0].hurtbox.bounds.min.y + 1;
            }
            if (controller.hurtboxes.Length > 0)
            {
                myPosition.x = targetIsOnTheLeft ?
                    controller.hurtboxes[0].hurtbox.bounds.min.x : controller.hurtboxes[0].hurtbox.bounds.max.x;
                myPosition.y = controller.hurtboxes[0].hurtbox.bounds.min.y + 1;

                Debug.DrawRay(myPosition, targetPosition - myPosition, Color.green);
            }

            //currentDistance = Mathf.Abs(myPosition.x - targetPosition.x);

            if (targetController.status.myPhysicsStatus == Status.physicsStatus.airborne)
            {
                targetAngle = 0;
                targetAngle = controller.slopeAngle;
                //targetAngle = Vector2.Angle(new Vector2(1, 0), new Vector2(Mathf.Abs(myPosition.x - targetPosition.x), Mathf.Abs(myPosition.y - targetPosition.y)));


            }
            else
            {
                //print ("dist: " + Vector2.Distance(myPosition, targetPosition));
                targetAngle = controller.slopeAngle;

                //targetAngle = Vector2.Angle(new Vector2(1, 0), new Vector2(Mathf.Abs(myPosition.x - targetPosition.x), Mathf.Abs(myPosition.y - targetPosition.y)));
            }

            currentDistance = Mathf.Abs(myPosition.x - targetPosition.x);// / Mathf.Cos(Mathf.Deg2Rad * (targetAngle));

            if (targetController != null)
            {
                if (targetIsOnTheLeft)
                {
                    if (targetController.facingRight)
                        currentDistance -= targetController.frontVisibilityModifier;
                    else
                        currentDistance -= targetController.backVisibilityModifier;
                        
                }
                else
                {
                    if (targetController.facingRight)
                        currentDistance -= targetController.backVisibilityModifier;
                    else
                        currentDistance -= targetController.frontVisibilityModifier;
                }
            }

            if (currentDistance < 0)
                currentDistance = 0;
            
        }

        if (aiMovements != null)
        {

            if (controller.targetList.myTarget != null)// && targetsAIMovements != null)
            {
                if (controller.targetList.potentialEnemiesList[controller.targetList.index].potentialTargetableObject != null &&
                    controller.targetList.potentialEnemiesList[controller.targetList.index].framesUntilDisengagement < controller.targetList.maxFramesForDisengagement)
                   //&& !canReturnToSpawnZone)
                {
                    //print("disengageeee");
                    //aiMovements.returningToSpawnZone = true;
                    isDisengaging = true;
                    //if ((currentBehaviour == null && controller.status.mystatus == Status.status.neutral) && onEndBehaviour != null && currentBehaviour != onEndBehaviour)
                    if (onEndBehaviour != null)
                    {
                        if (currentBehaviour != onEndBehaviour)
                        {
                            //if (onEndBehaviour != null) 

                            print("ma perchè");
                            StartNewBehaviour(onEndBehaviour);


                        }
                    }
                    else
                    {
                        if (currentBehaviour != null)
                            currentBehaviour.StopBehaviour();
                        print("ma percheeee");
                    }
                }
                else
                {
                    
                    if (currentBehaviour != null && currentBehaviour == onEndBehaviour && isDisengaging)
                    {
                        currentBehaviour.StopBehaviour();
                        
                        print("bruhhh");
                    }
                    isDisengaging = false;
                }
            }
            if (controller.targetList.numberofTargetables == 0)
            {
                isEngaged = false;
                isDisengaging = false;

                //ritorno alla spawnzone
                if (!aiMovements.isInsideSpawnZone && (disangagedBehaviourOnlyInsideSpawnZone || currentBehaviour != disengagedBehaviour))
                {


                    controller.horizontal = 0;
                    controller.targetList.myTarget = null;

                    if (currentBehaviour != null)
                        currentBehaviour.StopBehaviour();
                    //print("STOP 1111");
                    //aiMovements.returningToSpawnZone = true;
                    //if (aiMovements.hasCompetenceZone)

                    //print("si parte");
                    //aiMovements.MoveToReachDestination(aiMovements.mySpawnPosition.bounds.center, 1);
                    if (aiMovements.currentCompetenceZone != null)// && aiMovements.currentCompetenceZone == aiMovements.myCompetenceZone)
                        aiMovements.MoveToReachSpawnZone(aiMovements.currentCompetenceZone.transform.position, 1);



                }
            }

        }

        if (newEngagement)
        {
            
            newEngagement = false;
            isEngaged = true;
            if (aiMovements != null)
                aiMovements.isInsideSpawnZone = false;
            if (onEngageBehaviour != null && onEngageBehaviour.cooldownCounter <= 0)
            {
                //print("engage");
                StartNewBehaviour(onEngageBehaviour);
                //return;
            }
        }

        if (isEngaged)
        {
            //isDisengaging = false;
            canReturnToSpawnZone = false;
            if ((currentBehaviour == null) && controller.targetList.myTarget != null)
            {

                if (aiMovements == null || (differentCompetenceZoneBehavior == null) ||
                    aiMovements.currentCompetenceZone == controller.targetList.potentialEnemiesList[controller.targetList.index].potentialTargetableObject.aiMovements.currentCompetenceZone)
                {
                    //if (controller.targetList.myTarget != null) {
                    if (lowStaminaBehaviours.Length > 0)
                    {
                        for (int j = 0; j < lowStaminaBehaviours.Length; j++)
                        {
                            float currentStaminaPercentage = (float)controller.status.currentStamina / (float)controller.status.maxStamina;
                            if (currentStaminaPercentage <= lowStaminaBehaviours[j].staminaPercentage)
                            {
                                //print("lowstamina " + currentStaminaPercentage);
                                StartNewBehaviour(lowStaminaBehaviours[j].behaviour);
                                return;
                            }
                        }
                    }

                    poggers = true;
                    Status.status st = controller.status.mystatus;
                    Status.status ps = previousStatus;
                    if (st == Status.status.blockstun1 || st == Status.status.guardBreak
                            || st == Status.status.hitstun1 || st == Status.status.hitstun2)
                    {
                        poggers = false;
                       
                    }

                    lul = false;
                    if (ps == Status.status.hitstun1 || ps == Status.status.hitstun2 || ps == Status.status.blockstun1 || ps == Status.status.guardBreak)
                    {
                        lul = true;
                    }


                    for (int j = 0; j < rangesPerStatus.Length; j++)
                    {
                            
                            if (poggers == true && ((lul == false && st == rangesPerStatus[j].statusCondition) || 
                               (lul == true && ps == rangesPerStatus[j].statusCondition)))
                            {
                            //print("poggers: " + poggers + " " + st + " " + controller.status.myPreviousStatus);
                                int selectedRangeIndex = -1;


                                for (int i = 0; i < rangesPerStatus[j].ranges.Length; i++)
                                {
                                    if (currentDistance < rangesPerStatus[j].ranges[i].rangeEnd && currentDistance >= rangesPerStatus[j].ranges[i].rangeStart)
                                    {
                                        selectedRangeIndex = i;
                                        //print ("range: " + rangesPerStatus[j].ranges[selectedRangeIndex].rangeEnd);
                                        break;
                                    }
                                    else
                                        continue;

                                }
                                //print (selectedRangeIndex);




                                if (selectedRangeIndex != -1)
                                {
                                    int totalProbability = 0;
                                    int nonCooldownTotProbability = 0; //la somma delle probabilità solo dei behaviour non in cooldown
                                    List<behaviourProbability> nonCDList = new List<behaviourProbability>();

                                    //sommo le varie probabilità e inserisco in una lista i behaviour non in cooldown
                                    for (int i = 0; i < rangesPerStatus[j].ranges[selectedRangeIndex].behaviorProbabilities.Length; i++)
                                    {
                                        totalProbability += rangesPerStatus[j].ranges[selectedRangeIndex].behaviorProbabilities[i].probability;
                                        if (rangesPerStatus[j].ranges[selectedRangeIndex].behaviorProbabilities[i].behavior.cooldownCounter <= 0)
                                        {

                                            if (rangesPerStatus[j].ranges[selectedRangeIndex].behaviorProbabilities[i].probability > 0)
                                            {
                                                nonCooldownTotProbability += rangesPerStatus[j].ranges[selectedRangeIndex].behaviorProbabilities[i].probability;
                                                nonCDList.Add(rangesPerStatus[j].ranges[selectedRangeIndex].behaviorProbabilities[i]);
                                            }
                                        }

                                    }

                                    if (nonCooldownTotProbability == 0)
                                    {
                                        //fa qualcosa
                                        return;
                                    }


                                    int r = Random.Range(1, nonCooldownTotProbability + 1);
                                    //print ("r=" + r);
                                    int selectedBehaviour = -1;
                                    int probabilitySum = 0;

                                    //scorro la lista e seleziono il behaviour 
                                    for (int i = 0; i < nonCDList.Count; i++)
                                    {
                                        probabilitySum += nonCDList[i].probability;
                                        //print ("probabilitysum=" + probabilitySum);
                                        if (r <= probabilitySum)
                                        {

                                            selectedBehaviour = i;

                                            //selectedBehaviour = i;
                                            break;


                                        }

                                    }

                                    //print("R alla fine è " + r);


                                    if (selectedBehaviour != -1)
                                    {
                                        //print("startttt");
                                        StartNewBehaviour(nonCDList[selectedBehaviour].behavior);
                                    }

                                }

                                //break;
                                return;

                            }
                        


                    }
                }
                else
                {
                    if (currentBehaviour != differentCompetenceZoneBehavior)
                    {
                        StartNewBehaviour(differentCompetenceZoneBehavior);
                    }
                }

            }
        }
        else
        {
            if (disengagedBehaviour != null && (!disangagedBehaviourOnlyInsideSpawnZone || aiMovements.isInsideSpawnZone) && currentBehaviour != disengagedBehaviour)
            {
                //print("patrol??? " + currentBehaviour);
                StartNewBehaviour(disengagedBehaviour); 
            }
        }

        CheckInterruptingStatuses();
        //print("CHECK?????");

    }


    public void StartNewBehaviour (BehaviorOption newBehaviour) {

        //print(newBehaviour + " " + newBehaviour.gameObject + " " + controller.status.mystatus);
        if (controller.targetList.aboutToSwitchTarget)
        {
            controller.targetList.Target();
            controller.targetList.aboutToSwitchTarget = false;
        }
        if (currentBehaviour != null) {
			currentBehaviour.StopBehaviour ();
		}
		currentBehaviour = newBehaviour;
        
		newBehaviour.LoadInformations ();
        //newBehaviour.ExecuteBehaviour();
		skipBehaviourExecution = true;
			
	}
}
