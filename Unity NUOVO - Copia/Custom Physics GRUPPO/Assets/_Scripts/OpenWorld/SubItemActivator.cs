﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubItemActivator : MonoBehaviour
{
    [Tooltip("If I'm a leaf, this GameObject will be added to the list of items that will be retrieved by the PreAwakeRetriever and then gradually enabled by the ProximitySceneLoader. " +
        "Normally, the PreAwakeRetriever only retrieves its root children. With this option, it can search with more depth.")]
    public bool leaf = true;

    [Tooltip("If I'm a node, my children GameObjects will be added to the list of items that will be retrieved by the PreAwakeRetriever and then gradually enabled by the ProximitySceneLoader. " +
       "Normally, the PreAwakeRetriever only retrieves its root children. With this option, it can retrieve with more depth, as if my children were leaves.")]
    public bool node = false;
    
}
