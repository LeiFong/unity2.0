﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldToCanvasPositioner : MonoBehaviour
{
    [Tooltip("If false, you need to call SetPosition() manually")]
    public bool automaticPositioning = true;
    public Transform objectToFollow;
    [Tooltip("If null, it will try to grab it from the parents")]
    public Canvas referenceCanvas;
    private RectTransform rect, canvasRect;
    private Vector2 uiOffset;

    // Start is called before the first frame update
    protected void Start()
    {
        
        rect = GetComponent<RectTransform>();
        if (referenceCanvas == null)
            referenceCanvas = GetComponentInParent<Canvas>();
        canvasRect = referenceCanvas.GetComponent<RectTransform>();

        uiOffset = new Vector2((float)canvasRect.sizeDelta.x / 2f, (float)canvasRect.sizeDelta.y / 2f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (automaticPositioning)
            SetPosition();
    }

    public void SetPosition() {
        
        if (objectToFollow == null)
            return;

        
        Vector2 viewportPosition = Camera.main.WorldToViewportPoint(objectToFollow.position);       
        Vector2 proportionalPosition = new Vector2(viewportPosition.x * canvasRect.sizeDelta.x, viewportPosition.y * canvasRect.sizeDelta.y);
        rect.anchoredPosition = proportionalPosition - uiOffset;
    }

    public void SetObjectToFollow(Transform t) {
        objectToFollow = t;
    }
}
