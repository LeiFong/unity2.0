﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CUCCanChangeStamina : CustomUsabilityConditions
{
    public override bool CheckMyUsabilityConditions()
    {
        //print("test????" + relativeMove.conditions.cantChangeStamina);
        return relativeMove.conditions.cantChangeStamina > 0 ? false : true;
    }
}
