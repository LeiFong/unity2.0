﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputRecorder : NewInputCheck
{
    public enum Inputs {neutral, attack1, attack2, dash, jump, run};
    public enum DirectionInputs {neutral, left, right, up, down};

    public Inputaria[] inputSequence;

    [System.Serializable]
    public struct Inputaria
    {
        public Inputs button;
        public DirectionInputs direction;
    }
    public int sequenceIndex = 0;

    public override void SetBuffers()
    {
        //print("setbuffers");
        player.horizontal = 0;
        player.vertical = 0;

        switch (inputSequence[sequenceIndex].button)
        {
            case Inputs.attack1 : {
                    player.atk1Buffer = player.bufferLength;
                    break;
            }

            case Inputs.attack2:
                {
                    player.atk2Buffer = player.bufferLength;
                    break;
                }

            
            case Inputs.jump:
                {
                    player.jumpBuffer = player.bufferLength;
                    break;
                }
            case Inputs.dash:
                {
                    player.dashBuffer = player.bufferLength;
                    break;
                }
            case Inputs.run:
                {
                    player.runToggle = true;
                    break;
                }
            case Inputs.neutral:
                {
                    break;
                }


        }
        switch (inputSequence[sequenceIndex].direction)
        {
            case DirectionInputs.left:
                {
                player.horizontal = -1;
                break;
            }
            case DirectionInputs.right:
                {
                player.horizontal = 1;
                break;
            }
            case DirectionInputs.up:
                {
                    player.vertical = 1;
                    break;
                }
            case DirectionInputs.down:
                {
                    player.vertical = -1;
                    break;
                }
            case DirectionInputs.neutral:
                {
                    break;
                }
        }
        if (sequenceIndex < inputSequence.Length -1)
            sequenceIndex++;
        else
            sequenceIndex = 0;
    }
}
