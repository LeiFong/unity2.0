﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MultiLanguageText : MonoBehaviour
{
    [Tooltip("If true, and if the field myText (see below) has not been set manually, it will try to set it by grabbing it on this GameObject on Start.")]
    public bool autoGrabTextComponent = true;
    [Tooltip("Can be null. If not null, it will automatically change the text based on UIManager's language")]
    public Text myText;
    [Tooltip("Whether the strings differ based on player's gender. Scanning the strings is expensive, so leave it at false by default." +
        " To use this feature, mark the start of a gender-exclusive string with '@f' or '@m', and end it with '@'.")]
    public bool usingGenderMarkers = false;
    [TextArea(3, 10)]
    public string textEnglish;
    [TextArea(3, 10)]
    public string textItalian;
    private UIManager uiManager;
    

    // Start is called before the first frame update
    void Start()
    {
        uiManager = UIManager.Instance ?? FindObjectOfType<UIManager>();

        if (uiManager == null)
            print("UIManager not found");
        if (autoGrabTextComponent && myText == null)
            myText = GetComponent<Text>();
        
        UpdateText();
        
    }

    public void UpdateText(Text t) {
        
        switch (uiManager.language) {
            case UIManager.Language.english:
                if (usingGenderMarkers)
                    textEnglish = uiManager.ChangeStringBasedOnGender(textEnglish);
                t.text = textEnglish;
                break;
            case UIManager.Language.italian:
                if (usingGenderMarkers)
                    textItalian = uiManager.ChangeStringBasedOnGender(textItalian);
                t.text = textItalian;
                break;

        }
    }
    public void UpdateText() {
       if (myText == null)
            return;

        UpdateText(myText);
    }

    public void SetTextForLanguage(UIManager.Language language, string newText) {
        switch(language)
        {
            case UIManager.Language.english:
                textEnglish = newText;
                break;
            case UIManager.Language.italian:
                textItalian = newText;
                break;

        }
    }


    public void SetUIManager(UIManager manager) {
        uiManager = manager;
    }
    
}
