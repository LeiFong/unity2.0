﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;


public class Dialogue : MonoBehaviour
{
    private DialogueManager dialogueManager;

    [System.Serializable]
    public struct DialogueElement 
    {
        public bool functionOnly;
        public EventTrigger.TriggerEvent customFunction;
        [TextArea(3,10)]
        public string sentenceEnglish;
        [TextArea(3, 10)]
        public string sentenceItalian;
        
        
    }
    [TextArea(3, 10)]
    public string Description;
    public List<DialogueElement> dialogueElements;
    public EventTrigger.TriggerEvent endDialogueFunction;

    private void Start()
    {
        dialogueManager = DialogueManager.Instance ?? FindObjectOfType<DialogueManager>();
    }

    public void StartDialogue() {
        dialogueManager.StartDialogue(this);
    }
    public void EndDialogue() {
        dialogueManager.EndDialogue();
    }

    
    
}
