﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScript : MonoBehaviour {
    //public bool omegalul;
    [System.Serializable]
    public struct GenericInterval
    {
        public int start;
        public int end;
    }

    //public float totRange, hitboxRange;
    public int lastHitIndex;
    //bool lastHit;
    public bool debugPrintReach;
    bool alreadyPrintedReach;
    float debugHurtboxPositionAtStart;
    float accum;
    float startupAnimationSpeed;
    float difference;
    float animationSpeedMultiplier;
    Vector2 ghostHitboxInitialPosition = new Vector2();
    public int firstActualHitboxIndex = 0;
    public int lastActualHitboxIndex = 0;
    int currentStartup = 0;
    public int maxDurationFrames = 1000000; //durata massima della sequenza in frame
    Transform tr;
    public string[] activatedAnimationLayers;
    protected int[] activatedAnimationLayerIndexes;
    protected Transform originalParent;
    //public float variantWeightGain, variantWeightLoss = -1; //modificare nell'editor, settare a -1 per usare il valore di default
    public float variantWeightGain = -1; //modificare nell'editor, settare a -1 per usare il valore di default
    public float variantWeightLoss = -1; //modificare nell'editor, settare a -1 per usare il valore di default
    protected float totalAnimationTime; //tempo totale della mossa in secondi
    protected bool hitstopAlreadyHappened;
    public bool canRechargeStamina; //se può ricaricare la stamina durante l'esecuzione della mossa
    public float staminaRechargeMultiplier = 0;
    public int rechargeStaminaFrame = 0;
	public bool requiresStamina = false;
	public int staminaCost;

    [System.Serializable]
    public struct ContinuousStaminaSpendingInterval
    {
        public int start;
        public int end;
        public int staminaCost;
        public MoveScript optionalFollowupOnStaminaOver;
    }

    public ContinuousStaminaSpendingInterval[] continuousStaminaSpendingIntervals;

    public bool requiresMeter = false;
    public int meterCostFraction;
	public bool canWalkWhenGrounded = false;
	public bool canWalkWhenAirborne = false;
	public bool usesNormalWalkspeed = true; //solo se canWalk è vero
    public GenericInterval canWalkAirborneInterval, canWalkGroundedInterval;
    public WalkingAnimationChanger walkingAnimationChanger;
    public bool stopsAerialMomentum = true; 
	public float walkVelocity = 0; //solo se può camminare, e se usesNormalWalkSpeed è falso
    public bool walkingLegs = false; //se può camminare, anima le gambe che camminano
	public bool canBufferRun = false;
    public int maxBufferableRunFrames = 1000;
    private List<GameObject> colliders;

    //[HideInInspector]
    public float totalHorizontalMovements;
    /*
	public bool maintainsDashMomentum = false; //se hai cancellato questa mossa durante un dash, mantiene o no il movimento orizzontale
	public float initialDashMomentum = 4; //velocità da conservare nella mossa corrente se è iniziata durante un dash
	public float momentumReductionSpeed = 0.5f; //quanto velocemente il momentum diminuisce nel tempo
	protected float currentDashMomentum = 0;
    */
    public float currentAnimationSpeed = 1;
	public bool usingMovementsFunction = false;
	public bool interruptsWhenAirborne = true; //se diventi airborne durante la mossa, la interrompe
	public bool interruptsWhenLands = true; //se si interrompe se tocchi terra 
    public bool jumpHeightLimitation = true; //se è una mossa aerea, se usa le limitazioni d'altezza
    public float minimumAerialHeight = 0; //se jumpHeightLimitation è vera, testa questa altezza
    public int minimumAerialDuration = 0; //se è una mossa aerea, quanti frame bisogna essere stati in aria perchè venga iniziata
    public bool keepsAerialMomentum = false; //se è un mossa aerea, se mantiene la velocità orizzontale precedente
	//public bool isMultiHitting = false;
	//flag per indicare che l'input di cancel è stato ricevuto, e che la mossa si cancellerà appena entra nella finestra di cancel
	public bool isCancelling = false;

	//public List<GameObject> objectsHit;

	//indice del vettore di mosse della classe MoveSet per indicare in quale mossa questa animazione si sta cancellando, settato da ActivationConditions
	public int followupMoveIndex = 0;

	//protected int seq = 0;
	public string moveName;
	public string animatorParameterName;
    public string variantAnimatorParameterName;
    protected int animatorParameterIndex, variantAnimatorParameterIndex;
    public AnimatorTriggerCondition [] animatorTriggerConditions;
    public bool usingAnimatorTrigger = false; //se settato a vero nell'editor, userà un trigger per una transizione più fluida
    public bool smoothTransitionToNeutral = true;
    public bool endAnimationOnStopSequence = false; //se settato a vero nell'editor, l'animazione finisce al termine della sequenza
    public bool loopingAnimation = false;
    public bool dontTransitionIfAlreadyTransitioning = true;
    public bool dontTransitionToWalkAnimation;


    public LayerMask collisionMask;
	public float shellRadius = 0.1f;
	public bool itsTimeToStopTheSequence; //da settare alla fine dell'executesequence, così la sequence si ferma all'inizio del frame successivo 

	protected int totalFrames;
	protected bool startupPhase;
	protected bool activePhase;
	protected bool recoveryPhase;

    public CustomUsabilityConditions [] customUsabilityConditions;
    public CustomStatusPeriod[] customStatusPeriods;

    [System.Serializable]
    public struct CustomStatusPeriod
    {
        public int start;
        public int finish;
        public Status.status status;
    }
   

	//public OnHitProperties [] onHitProperties;
    public List<OnHitProperties> onHitProperties;
    [HideInInspector]
    public List<ApplyDebuff> applyDebuffs;
    //public ApplyDebuff[] applyDebuffs; //buff o debuff applicati, su hit o su attivazione


	public Player controller;
	//public Collider2D hurtbox;
	public Status status;
	protected HurtboxHit hurtboxHit;
	public float savedVerticalSpeed = 0;
    public float aerialMomentum;

	public MoveSet moveset;
	public FrameData move;
	public FDModifier moveModifier;

	//public BoxCollider2D[] hitboxes;
	//public CompositeCollider2D[] compositeHitboxes;
	public ActivationConditions conditions;

	//public bool actionUsable = false;
	public Status.status [] statusConditions;
	public Status.physicsStatus [] physicsStatusConditions;


	public enum moveType {move, animation}; //se è una mossa nell'array di un'arma o se è una mossa nell'array delle animazioni generiche

	[System.Serializable]
	public struct movement {
        public bool fromRecovery;
		public int windowStart;
		public int windowEnd;
		public float velocity; 
	}

    [System.Serializable]
    public struct AvoidsCharacterWindow
    {
        public bool fromRecovery;
        public int windowStart;
        public int windowEnd;
    }

    [System.Serializable]
	public struct adjustableSpeedInterval {
		public int windowStart;
		public int windowEnd;
		public float positiveVelocity;
        public float neutralVelocity;
		public float negativeVelocity;
        public float maxVelocity;
        public float minVelocity;
	}

    protected bool mayExecuteNow = true; //variabile ausiliaria, serve a decidere se l'executeSequence può partire subito dallo startsequence o dal frame dopo
    [System.Serializable]
    public struct HurtboxSizeInterval
    {
        public int hurtboxNumber;
        public bool fromRecovery;
        public int windowStart, windowEnd;
        public bool originalSizeX, originalSizeY, originalOffsetX, originalOffsetY;
        public Vector2 size;
        public Vector2 offset;
        public BoxCollider2D referenceCollider;
    }



        [System.Serializable]
	public struct followup {
        public bool fromRecovery;
		public moveType type;
		public int cancelWindowStart;
		public int cancelWindowEnd;
		public int followupMoveIndex; //indice dell'array di mosse contenuto in ActivationConditions
	}

	[System.Serializable]
	public struct automaticFollowup {
		public int cancelStart;
		public MoveScript followupMove; //indice dell'array di mosse contenuto in ActivationConditions
	}

	[System.Serializable]
	public struct invincibilityPeriod {
		public int invincibilityWindowStart;
		public int invincibilityWindowEnd;
	}

	[System.Serializable]
	public struct canFlipPeriod {
		public int flipWindowStart;
		public int flipWindowEnd;
	}

    [System.Serializable]
    public struct ChargedFollowups
    {
        public moveType moveType;
        public int windowStart;
        public int windowEnd;
        public int moveIndex;
    }

    [System.Serializable]
    public struct AnimationSpeedInterval
    {
        public int windowEnd;
        public float animationSpeed;
    }

    [System.Serializable]
    public struct AerialMomentum
    {
        public int frame;
        public float aerialMomentum;
    }

    [System.Serializable]
    public struct AnimatorTriggerCondition
    {
        public enum MoveType { none, move, animation };
        public MoveType moveType;
        public int moveIndex;
        public int windowStart;
        public int windowEnd;
        public bool usesStatusConditions;
        public Status.status statusConditions;
    }

    [System.Serializable]
    public struct TimeFreezeWindow
    {
        public int windowStart;
        public int windowEnd;
        //public string animatorParameter;
        //public int freezeSeq;
        //public bool timeFreezeActive;
        public bool unscaledAnimatorSpeed; //se vera, durante lo slowmotion il mio animatore mantiene velocità non scalata
        //public bool timeFreezeEnded;
        public int timeScale;
    }

    [System.Serializable]
    public struct VisibilityModifier
    {
        public GenericInterval interval;
        public float frontVisibilityModifier;
        public float backVisibilityModifier;
    }

    public followup[] followups;
	public automaticFollowup autoFollowup; //si attiva automaticamente appena entra nella finestra di cancel. Da settare solo se è multihitting
	public MoveScript [] relatedMoves; //serve a inizializzare altre mosse senza doverle inserire nell'array dell'arma o delle animations
	public invincibilityPeriod[] invincibilityWindows;
    public GenericInterval canBlockInterval;
    public GenericInterval[] counterHitPeriod;
	public movement[] horizontalMovements;
	public movement[] verticalMovements;
    public AerialMomentum[] aerialMomentums; //se è in aria, la spinta che riceve
	public adjustableSpeedInterval[] adjustableSpeedIntervals; //intervalli in cui premere destra o sinistra aumenta o diminuisce la velocita' orizzontale, e di quanto
	public canFlipPeriod[] canFlipPeriods;
    public bool enableHurtboxChanging = true;
    public HurtboxSizeInterval[] hurtboxSizeIntervals; //intervalli in cui le dimensioni delle hurtbox vengono modificate
    public VisibilityModifier[] visibilityModifiers; //intervalli in cui il nemico ti può vedere da più vicino/lontano
    public AvoidsCharacterWindow[] avoidsCharacterWindows;
    public ChargedFollowups[] chargedFollowups;
    public TimeFreezeWindow[] slowMotionWindows;
    public AnimationSpeedInterval[] animationSpeedIntervals;
    public bool isCharging;
    public int noGravityFrames = 0; //frames in cui non si applica la gravità, finiti i quali ricomincia a cadere
	public MoveScript customLanding;
    


	public bool canFlip = false;

	//public int hitStunType = 1; //DA SETTARE NELL'EDITOR
	//public int blockStunType = 1; 

	protected bool resumingFromHitstop; //vera solo il frame successivo alla fine dell'hitstop
	bool originalInterruptsWhenAirborne; 
	bool originalCanWalkWhenAirborne;
	bool originalCanWalkWhenGrounded;




	// Use this for initialization
	public void Initialize (Player c, Transform parentTransform, MoveSet ms) {


        //controller = transform.parent.transform.parent.GetComponent<Player> ();
        //status = transform.parent.transform.parent.GetComponent<Status> ();
        //if (controller == null)
        //controller = transform.parent.transform.parent.transform.parent.GetComponent<Player>();
        //controller = GetComponentInParent<Player>();
        tr = transform;
        originalParent = parentTransform;
        controller = c;
        status = controller.status;
        //conditions = transform.parent.GetComponent<ActivationConditions> ();
        conditions = controller.conditions;
		hitstopAlreadyHappened = false;
		originalInterruptsWhenAirborne = interruptsWhenAirborne;
		originalCanWalkWhenAirborne = canWalkWhenAirborne;
		originalCanWalkWhenGrounded = canWalkWhenGrounded;

        moveset = ms;

        if (canBufferRun && maxBufferableRunFrames <= 0)
        {
            maxBufferableRunFrames = controller.timeToRunToggle;
        }


		MyOnEnable ();

        totalHorizontalMovements = 0;
		LoadFrameData ();

        animatorParameterIndex = Animator.StringToHash(animatorParameterName);
        if (variantAnimatorParameterName != "")
        {
            variantAnimatorParameterIndex = Animator.StringToHash(variantAnimatorParameterName);

        }

        if (walkingAnimationChanger != null)
            walkingAnimationChanger.Initialize(controller);

        //ATTENZIONE, DA TESTARE
        /*
        OnHitProperties[] tempProperties = GetComponentsInChildren<OnHitProperties>();

        for (int i = 0; i < tempProperties.Length; i++)
        {
            tempProperties[i].move = this;
            tempProperties[i].Initialize();

            if (tempProperties[i] is ApplyDebuff)
            {
                applyDebuffs.Add((ApplyDebuff)tempProperties[i]);
            }
            else
            {
                onHitProperties.Add(tempProperties[i]);
            }
        }
        */

        for (int i = 0; i < onHitProperties.Count; i++)
        {
            onHitProperties[i].move = this;
            onHitProperties[i].Initialize();

            if (onHitProperties[i] is ApplyDebuff)
            {
                applyDebuffs.Add((ApplyDebuff)onHitProperties[i]);
                onHitProperties.RemoveAt(i);
                i--;
            }
            
        }

        

        activatedAnimationLayerIndexes = new int[activatedAnimationLayers.Length];
        for (int i = 0; i < activatedAnimationLayerIndexes.Length; i++)
        {
            //activatedAnimationLayerIndexes[i] = controller.animator.GetLayerIndex(activatedAnimationLayers[i]);

        }

        InitializeHurtboxModifiers();

        controller.seq = 0;
		if ((canWalkWhenAirborne || canWalkWhenGrounded) && usesNormalWalkspeed)
			walkVelocity = controller.movespeed;

	}

    void InitializeHurtboxModifiers ()
    {
        for (int i = 0; i < hurtboxSizeIntervals.Length; i++)
        {
            if (hurtboxSizeIntervals[i].referenceCollider != null)
            {
                hurtboxSizeIntervals[i].size = hurtboxSizeIntervals[i].referenceCollider.size;
                hurtboxSizeIntervals[i].offset = hurtboxSizeIntervals[i].referenceCollider.offset;
            }
        }
    }

	protected virtual void MyOnEnable() {
		
	}

    protected void ModifyHurtboxesDimensions ()
    {
        controller.ResetAllHurtboxesSize();

        if (!enableHurtboxChanging)
            return;

        for (int i = 0; i < hurtboxSizeIntervals.Length; i++)
        {
            
            if (hurtboxSizeIntervals[i].hurtboxNumber > controller.hurtboxes.Length - 1)
            {
                print("too big");
                continue;
            }

            if (((!hurtboxSizeIntervals[i].fromRecovery || move == null || move.hitboxes.Length <= 0)
                && (controller.seq >= hurtboxSizeIntervals[i].windowStart && controller.seq <= hurtboxSizeIntervals[i].windowEnd))
                || (hurtboxSizeIntervals[i].fromRecovery && move != null && move.hitboxes.Length > 0 &&
                (controller.seq - move.hitboxes[lastActualHitboxIndex].startup >= hurtboxSizeIntervals[i].windowStart &&
                 controller.seq - move.hitboxes[lastActualHitboxIndex].startup <= hurtboxSizeIntervals[i].windowEnd)))
            {
                
                //if (controller.seq >= hurtboxSizeIntervals[i].windowStart && controller.seq <= hurtboxSizeIntervals[i].windowEnd)
                {
                    //SIZE
                    if (!hurtboxSizeIntervals[i].originalSizeX && !hurtboxSizeIntervals[i].originalSizeY) //nessuno dei due
                        controller.hurtboxes[hurtboxSizeIntervals[i].hurtboxNumber].hurtbox.size = hurtboxSizeIntervals[i].size;
                    else
                    {
                        if (hurtboxSizeIntervals[i].originalSizeX && hurtboxSizeIntervals[i].originalSizeY) //entrambi
                            controller.hurtboxes[hurtboxSizeIntervals[i].hurtboxNumber].hurtbox.size =
                            new Vector2(controller.hurtboxParameters[hurtboxSizeIntervals[i].hurtboxNumber].sizeX,
                            controller.hurtboxParameters[hurtboxSizeIntervals[i].hurtboxNumber].sizeY);
                        else //uno dei due
                        {
                            if (hurtboxSizeIntervals[i].originalSizeX)
                                controller.hurtboxes[hurtboxSizeIntervals[i].hurtboxNumber].hurtbox.size =
                                new Vector2(controller.hurtboxParameters[hurtboxSizeIntervals[i].hurtboxNumber].sizeX, hurtboxSizeIntervals[i].size.y);
                            else
                                controller.hurtboxes[hurtboxSizeIntervals[i].hurtboxNumber].hurtbox.size =
                                new Vector2(hurtboxSizeIntervals[i].size.x, controller.hurtboxParameters[hurtboxSizeIntervals[i].hurtboxNumber].sizeY);


                        }
                    }


                    //OFFSET
                    if (!hurtboxSizeIntervals[i].originalOffsetX && !hurtboxSizeIntervals[i].originalOffsetY) //nessuno dei due
                        controller.hurtboxes[hurtboxSizeIntervals[i].hurtboxNumber].hurtbox.offset = hurtboxSizeIntervals[i].offset;
                    else
                    {
                        if (hurtboxSizeIntervals[i].originalOffsetX && hurtboxSizeIntervals[i].originalOffsetY) //entrambi
                            controller.hurtboxes[hurtboxSizeIntervals[i].hurtboxNumber].hurtbox.offset =
                            new Vector2(controller.hurtboxParameters[hurtboxSizeIntervals[i].hurtboxNumber].offsetX,
                            controller.hurtboxParameters[hurtboxSizeIntervals[i].hurtboxNumber].offsetY);
                        else //uno dei due
                        {
                            if (hurtboxSizeIntervals[i].originalOffsetX)
                                controller.hurtboxes[hurtboxSizeIntervals[i].hurtboxNumber].hurtbox.offset =
                                new Vector2(controller.hurtboxParameters[hurtboxSizeIntervals[i].hurtboxNumber].offsetX, hurtboxSizeIntervals[i].offset.y);
                            else
                                controller.hurtboxes[hurtboxSizeIntervals[i].hurtboxNumber].hurtbox.offset =
                                new Vector2(hurtboxSizeIntervals[i].offset.x, controller.hurtboxParameters[hurtboxSizeIntervals[i].hurtboxNumber].offsetY);


                        }
                    }
                }

            }
            
        }
    }

    public void TestSetTimeFreeze ()
    {
        for (int i = 0; i < slowMotionWindows.Length; i++)
        {
            if (slowMotionWindows[i].windowEnd >= controller.seq && controller.seq >= slowMotionWindows[i].windowStart)
            {
                //print("SLowmomomo");
                //status.mystatus = Status.status.stuckinanimation;
                //StartSequence();
                if (slowMotionWindows[i].timeScale > 0)
                {
                    controller.timeManager.AlterTimeRequest(slowMotionWindows[i].timeScale);
                    controller.currentTimeScale = slowMotionWindows[i].timeScale;
                    //controller.animator.updateMode = AnimatorUpdateMode.UnscaledTime;
                    //controller.animator.Play(slowMotionWindows[0].animatorParameter, 0, 0);
                    //slowMotionWindows[i].timeFreezeActive = true;
                    if (slowMotionWindows[i].unscaledAnimatorSpeed)
                        controller.animatorSpeed = currentAnimationSpeed * slowMotionWindows[i].timeScale;
                    //slowMotionWindows[0].freezeSeq = slowMotionWindows[0].duration;
                    break;
                }
            }
            else
            {
                //Time.timeScale = 1;
                //slowMotionWindows[i].timeFreezeActive = false;
                controller.animatorSpeed = currentAnimationSpeed;

            }
        }
    }

    public void SetTimeFreeze ()
    {
        for (int i = 0; i < slowMotionWindows.Length; i++)
        {
            if (slowMotionWindows[i].windowEnd >= controller.seq && controller.seq >= slowMotionWindows[i].windowStart)
            {
                //print("SLowmomomo");
                //status.mystatus = Status.status.stuckinanimation;
                //StartSequence();
                if (slowMotionWindows[i].timeScale > 0)
                {
                    //Time.timeScale = slowMotionWindows[i].timeScale;
                    //print("request, sono " + gameObject + " " + Time.frameCount);
                    controller.timeManager.AlterTimeRequest(slowMotionWindows[i].timeScale);
                    controller.currentTimeScale = slowMotionWindows[i].timeScale;
                    //controller.animator.updateMode = AnimatorUpdateMode.UnscaledTime;
                    //controller.animator.Play(slowMotionWindows[0].animatorParameter, 0, 0);
                    //slowMotionWindows[i].timeFreezeActive = true;
                    if (slowMotionWindows[i].unscaledAnimatorSpeed)
                        controller.unscaledAnimatorSpeed = true;
                        //controller.animator.speed = currentAnimationSpeed * slowMotionWindows[i].timeScale;
                        //controller.animator.updateMode = AnimatorUpdateMode.UnscaledTime;
                    //slowMotionWindows[0].freezeSeq = slowMotionWindows[0].duration;
                    break;
                }
            }
            else
            {

                //Time.timeScale = 1;
                //slowMotionWindows[i].timeFreezeActive = false;
                //controller.animator.speed = currentAnimationSpeed;
                if (controller.seq == slowMotionWindows[i].windowEnd + 1)// + 2) //non chiedermi perchè
                    controller.animator.updateMode = AnimatorUpdateMode.Normal;

            }
        }
    }

    protected void AvoidsCharacterWindows ()
    {
        for (int i = 0; i < avoidsCharacterWindows.Length; i++)
        {
            //print (movements.Length);

            if (!avoidsCharacterWindows[i].fromRecovery || (move == null || move.hitboxes.Length <= 0))
            {
                if (controller.seq >= avoidsCharacterWindows[i].windowStart && controller.seq <= avoidsCharacterWindows[i].windowEnd)
                {
                    controller.avoidingCharacters = true;

                    break;
                }
                else
                {
                    controller.avoidingCharacters = false;
                }
            }
            else
            {
                if (controller.seq - move.hitboxes[firstActualHitboxIndex].startup >= avoidsCharacterWindows[i].windowStart && 
                    controller.seq - move.hitboxes[firstActualHitboxIndex].startup <= avoidsCharacterWindows[i].windowEnd)
                {
                    controller.avoidingCharacters = true;
                    break;
                }
                else
                {
                    controller.avoidingCharacters = false;
                }

            }

        }
    }

	protected void Movements () {

		
        //print (currentDashMomentum);
        for (int i = 0; i < verticalMovements.Length; i++)
        {
            //print (movements.Length);

            if (!verticalMovements[i].fromRecovery || (move == null || move.hitboxes.Length <= 0))
            {


                if (controller.seq >= verticalMovements[i].windowStart && controller.seq <= verticalMovements[i].windowEnd)
                {
                    controller.velocity.y = (verticalMovements[i].velocity);
                    //status.myPhysicsStatus = Status.physicsStatus.airborne;
                    if (interruptsWhenAirborne)
                        interruptsWhenAirborne = false;
                    break;
                }
                else
                {
                    interruptsWhenAirborne = originalInterruptsWhenAirborne;
                    //controller.targetVelocity.x = 0;
                }
            }
            else
            {
                if (controller.seq - move.hitboxes[lastActualHitboxIndex].startup >= verticalMovements[i].windowStart &&
                    controller.seq - move.hitboxes[lastActualHitboxIndex].startup <= verticalMovements[i].windowEnd)
                {
                    controller.velocity.y = (verticalMovements[i].velocity);
                    //status.myPhysicsStatus = Status.physicsStatus.airborne;
                    if (interruptsWhenAirborne)
                        interruptsWhenAirborne = false;
                    break;
                }
                else
                {
                    interruptsWhenAirborne = originalInterruptsWhenAirborne;
                    //controller.targetVelocity.x = 0;
                }
            }
        }

        //if ((!canWalkWhenAirborne && status.myPhysicsStatus == Status.physicsStatus.airborne) || (!canWalkWhenGrounded && status.myPhysicsStatus == Status.physicsStatus.grounded)) {
        for (int i = 0; i < horizontalMovements.Length; i++) {
            //print (movements.Length);

            if (!horizontalMovements[i].fromRecovery || (move == null || move.hitboxes.Length <= 0))
            {
                if (controller.seq >= horizontalMovements[i].windowStart && controller.seq < horizontalMovements[i].windowEnd)
                {
                    //canWalkWhenAirborne = false;
                    canWalkWhenGrounded = false;
                    if (!walkingLegs)
                        controller.targetVelocity.x = controller.gameObject.transform.localScale.x * (horizontalMovements[i].velocity);
                    else
                    {
                        canWalkWhenGrounded = true;
                        controller.horizontal = controller.gameObject.transform.localScale.x * horizontalMovements[i].velocity / controller.movespeed;
                    }
                    //print("SEQQQ " + controller.seq);
                    accum += horizontalMovements[i].velocity;
                    break;
                }
                else
                {
                    //print("nope");
                    canWalkWhenAirborne = originalCanWalkWhenAirborne;
                    canWalkWhenGrounded = originalCanWalkWhenGrounded;
                    if ((!canWalkWhenAirborne && status.myPhysicsStatus == Status.physicsStatus.airborne) ||
                        (!canWalkWhenGrounded && status.myPhysicsStatus == Status.physicsStatus.grounded))
                    {
                        controller.targetVelocity.x = 0;
                        controller.aerialMomentum = 0;
                    }
                }
            }
            else
            {
                if (controller.seq - (move.hitboxes[lastActualHitboxIndex].startup -1) >= horizontalMovements[i].windowStart
                    && controller.seq - (move.hitboxes[lastActualHitboxIndex].startup -1) < horizontalMovements[i].windowEnd)
                {
                    //canWalkWhenAirborne = false;
                    canWalkWhenGrounded = false;
                    if (!walkingLegs)
                        controller.targetVelocity.x = controller.tr.localScale.x * (horizontalMovements[i].velocity);
                     else
                    {
                        canWalkWhenGrounded = true;
                        controller.horizontal = controller.gameObject.transform.localScale.x * horizontalMovements[i].velocity / controller.movespeed;
                    }

                    break;
                }
                else
                {
                    canWalkWhenAirborne = originalCanWalkWhenAirborne;
                    canWalkWhenGrounded = originalCanWalkWhenGrounded;
                    if ((!canWalkWhenAirborne && status.myPhysicsStatus == Status.physicsStatus.airborne) ||
                        (!canWalkWhenGrounded && status.myPhysicsStatus == Status.physicsStatus.grounded))
                    {
                        controller.targetVelocity.x = 0;
                        if (walkingLegs)
                            controller.horizontal = 0;
                        controller.aerialMomentum = 0;
                    }
                }

            }

		}

        if (controller.velocity.y > 0 || status.myPhysicsStatus == Status.physicsStatus.airborne)
        {
            for (int i = 0; i < aerialMomentums.Length; i++)
            {
                if (controller.seq == aerialMomentums[i].frame)
                {
                    //print("spintaaaa");
                    controller.aerialMomentum = controller.facingRight ? aerialMomentums[i].aerialMomentum : - aerialMomentums[i].aerialMomentum;
                }
            }
        }
        //bool originalInterruptsWhenAirborne = interruptsWhenAirborne;




        //print (controller.status.myPreviousStatus);
        if (horizontalMovements.Length == 0 && ((!canWalkWhenAirborne && status.myPhysicsStatus == Status.physicsStatus.airborne) ||
            (!canWalkWhenGrounded && status.myPhysicsStatus == Status.physicsStatus.grounded)))
        {
            controller.targetVelocity.x = 0;
            controller.aerialMomentum = 0;
        }

        if (status.myPhysicsStatus == Status.physicsStatus.grounded) //quando sei airborne questa sezione viene gestito dal Player!
        {
            for (int i = 0; i < adjustableSpeedIntervals.Length; i++)
            {
                if (controller.seq >= adjustableSpeedIntervals[i].windowStart && controller.seq <= adjustableSpeedIntervals[i].windowEnd)
                {

                    if ((controller.horizontal > 0 && controller.facingRight) || (controller.horizontal < 0 && !controller.facingRight))
                    {

                        controller.targetVelocity.x += controller.gameObject.transform.localScale.x * adjustableSpeedIntervals[i].positiveVelocity;
                        
                    }
                    else if ((controller.horizontal < 0 && controller.facingRight) || (controller.horizontal > 0 && !controller.facingRight))
                    {
                        controller.targetVelocity.x -= controller.gameObject.transform.localScale.x * adjustableSpeedIntervals[i].negativeVelocity;

                        
                    }
                    break;
                }
            }
        }
        
		
	}

    protected void ManageHitboxes (FrameData theFrameData)
    {
        

        if (theFrameData.ghostHitboxDurationLeft > 0)
            theFrameData.ghostHitboxDurationLeft--;

        bool didthething = false;


        if (firstActualHitboxIndex != -1 && theFrameData.hitboxes.Length > 0)
        {

            if (status.hyperArmor == 0 && controller.seq >= 22 && controller.seq <= move.hitboxes[lastActualHitboxIndex].startup)
            {
                status.hyperArmor = 10;
            }

            if (controller.seq <= theFrameData.hitboxes[firstActualHitboxIndex].startup && controller.seq >= theFrameData.hitboxes[firstActualHitboxIndex].startup - 20)
            {
                controller.counterhitState = true;
            }

        }
        //if (requiresMeter && meterCost > 0)
        {
            //theFrameData.damageMultiplier = 1 + ((float)((float)status.currentHeatAmount / (float)status.maxHeatGauge) * (status.maxHeatDamageMultiplier - 1));

            /*
            if (status.heatLevel == 0) 
                theFrameData.damageMultiplier = 1f;
            else if (status.heatLevel == 1)
                theFrameData.damageMultiplier = 1 + status.maxHeatDamageMultiplier / 2;
            else
                theFrameData.damageMultiplier = 1 + status.maxHeatDamageMultiplier;
            */

            //theFrameData.damageMultiplier = 1f + (Mathf.Min(1f, (float)status.currentHeatAmount / (float)(status.maxHeatGauge - status.heatGaugeBuffer)) * status.maxHeatDamageMultiplier);

            //if ((float)status.currentHeatAmount / (float)(status.maxHeatGauge) > 0.25f)
            if (status.currentHeatAmount > status.heatGaugeThreshold)
            {
                theFrameData.damageMultiplier = 1f + status.maxHeatDamageMultiplier;

            }
            else
            {
                theFrameData.damageMultiplier = 1f;
            }

            //print("medic gaming: " + status.currentHeatAmount + " " + (Mathf.Min(1f, (float)status.currentHeatAmount / (float)(status.maxHeatGauge - 10000f))));
            if (!controller.enemySpecializer.isAI && status.currentStance == Status.Stances.fast)
            {
                theFrameData.damageMultiplier *= controller.moveset.lowStanceDamageMultiplier;
            }
        }

        if (theFrameData.hitProperties.Length > 1)
        {
            ManageMultipleHits();
        }
        else
        {
            theFrameData.lastHit = true;
        }

        //DisableAllHitboxes();

        for (int i = 0; i < theFrameData.softArmorWindows.Length; i++)
        {
            /*
            if (move.hitboxes.Length < move.softArmorWindows[i].hitboxIndex || !move.softArmorWindows[i].fromStartup)
            {
                if (controller.seq == move.softArmorWindows[i].windowStart)
                {
                    controller.hitDelay = move.softArmorWindows[i].hitDelay;
                }
                if (controller.seq == move.softArmorWindows[i].windowStart + move.softArmorWindows[i].windowDuration)
                {
                    controller.hitDelay = 0;
                }
            }
            
            else*/
            {
                if (controller.seq == - theFrameData.softArmorWindows[i].offset + theFrameData.hitboxes[theFrameData.softArmorWindows[i].hitboxIndex].startup)
                {
                    controller.hitDelay = theFrameData.softArmorWindows[i].offset + 1;
                    //print("delay: " + controller.hitDelay);

                }
                if (controller.seq == theFrameData.hitboxes[theFrameData.softArmorWindows[i].hitboxIndex].startup)
                {
                    //print("nodelay");
                    controller.hitDelay = 0;
                }
            }
        }

        for (int i = 0; i < theFrameData.hitboxes.Length; i++)
        {
            if (theFrameData.hitboxes[i].ghostHitbox != null)
            {
                if (controller.seq >= theFrameData.hitboxes[i].startup - theFrameData.hitboxes[i].ghostHitboxFramesOffset && controller.seq < theFrameData.hitboxes[i].startup + theFrameData.hitboxes[i].active)
                {

                    {
                        theFrameData.hitboxes[i].ghostHitbox.gameObject.SetActive(true);
                        theFrameData.hitboxes[i].ghostHitbox.transform.parent = null;
                        theFrameData.ghostHitboxIsActive = true;
                        if (theFrameData.ghostHitboxDurationLeft == 0)
                            theFrameData.ghostHitboxDurationLeft = theFrameData.hitboxes[i].ghostHitboxFramesOffset;
                        //theFrameData.hitboxes[i].enableGhostHitbox = true;
                        if (debugPrintReach && !alreadyPrintedReach)
                        {
                            alreadyPrintedReach = true;
                            //Debug.DrawRay(new Vector2(theFrameData.hitboxes[i].ghostHitbox.bounds.max.x, controller.hurtboxes[0].hurtbox.bounds.max.y), Vector2.up, Color.red, 1000);
                            float totRange = Mathf.Round((theFrameData.hitboxes[i].ghostHitbox.bounds.max.x - debugHurtboxPositionAtStart) * 100) / 100;
                            float hitboxRange = Mathf.Round((theFrameData.hitboxes[i].ghostHitbox.bounds.max.x - controller.hurtboxes[0].hurtbox.bounds.max.x) * 100) / 100;


                            {
                                if (controller.facingRight)
                                {
                                    print(gameObject + " - tot range: " + totRange + " hitbox range: " + hitboxRange + " startup: " + (theFrameData.hitboxes[i].startup));

                                    Debug.DrawRay(new Vector2(debugHurtboxPositionAtStart + totRange, controller.hurtboxes[0].hurtbox.bounds.center.y),
                                        Vector2.up, Color.red, 1000);

                                    Debug.DrawRay(new Vector2(debugHurtboxPositionAtStart + hitboxRange, controller.hurtboxes[0].hurtbox.bounds.center.y),
                                       1 * Vector2.up, Color.yellow, 1000);
                                }
                            }

                            if (theFrameData.hitboxes[i].startup < 28)
                            {
                                float walkingReach = (28 - theFrameData.hitboxes[i].startup) * controller.movespeed * Time.fixedDeltaTime;
                                //print("range con camminata: " + (range + walkingReach) + "reach con camminata: " + ((range + walkingReach) / theFrameData.hitboxes[i].startup));
                            }


                        }
                    }
                    /*
                    if (controller.seq == theFrameData.hitboxes[i].startup - theFrameData.hitboxes[i].ghostHitboxFramesOffset + 1)
                    {
                        //print("sono quaaa: " + controller.seq);
                        theFrameData.hitboxes[i].ghostHitbox.transform.position = theFrameData.hitboxes[i].compositeCollider.transform.position;
                        ghostHitboxInitialPosition = theFrameData.hitboxes[i].ghostHitbox.transform.position;
                    }
                    else
                    {
                        //print("NOOOOOON sono quaaa: " + controller.seq);

                        theFrameData.hitboxes[i].ghostHitbox.transform.position = ghostHitboxInitialPosition;
                    }
                    */
                }
                else
                {
                    {
                        //move.hitboxes[i].ghostHitbox.SetActive(false);

                    }
                }
            }
            //print (hitboxes.Length);

            if (controller.seq == theFrameData.hitboxes[i].startup - 2 && requiresStamina && !controller.inHitstop)
            {
                status.SpendStamina(theFrameData.hitboxes[i].staminaCost);
                //if (controller.status.faction == 0 && i == 0)
                  //print("stamina: " + theFrameData.hitboxes[i].staminaCost);
            }
            if (!didthething) 
            if (theFrameData.hitboxes[i].startup + theFrameData.hitboxes[i].active - 1 >= controller.seq && controller.seq > theFrameData.hitboxes[i].startup - 1)
            {
                    //print("enable: " + i + " " + controller.seq);
                EnableHitbox(theFrameData, i);
                //theFrameData.hitboxes[i].enableHitbox = true;
                
                //print("alorraararaaara" + controller.seq + " " + theFrameData.hitboxes[i].enableHitbox);

                if (theFrameData.hitboxes[i].ghostHitbox != null)
                {
                    if (controller.seq >= theFrameData.hitboxes[i].startup)
                        controller.recoveryCounter = controller.recoveryPeriod;
                }

                didthething = true;
                //break;
            }
            
            else
            {

                DisableHitbox(theFrameData, i);
                
            }
            
            
            if (theFrameData.hitboxes[i].ghostHitbox != null && controller.seq > theFrameData.hitboxes[i].startup + theFrameData.hitboxes[i].active - 1)
            {
                theFrameData.hitboxes[i].ghostHitbox.gameObject.SetActive(false);
                theFrameData.ghostHitboxIsActive = false;
                //print("ghost: " + theFrameData.ghostHitboxDurationLeft);
                theFrameData.ghostHitboxDurationLeft = 0;

            }
        }

        
        /*
        for (int i = 0; i < theFrameData.hitboxes.Length; i++)
        {
            if (theFrameData.hitboxes[i].enableHitbox)
            {
                print("enableeeee " + controller.seq);
                EnableHitbox(theFrameData, i);
            }
            else
            {
                DisableHitbox(theFrameData, i);
                print("disableeee " + controller.seq);
            }

            if (theFrameData.hitboxes[i].ghostHitbox != null)
            {
                if (theFrameData.hitboxes[i].enableGhostHitbox)
                {
                    theFrameData.hitboxes[i].ghostHitbox.gameObject.SetActive(true);
                }
                else
                {
                    theFrameData.hitboxes[i].ghostHitbox.gameObject.SetActive(false);
                }
            }

            theFrameData.hitboxes[i].enableHitbox = false;
            theFrameData.hitboxes[i].enableGhostHitbox = false;
        }
        */
    }


    public void ManageMultipleHits()
    {
        if (move == null || move.hitboxes.Length == 0)
            return;

        currentStartup = move.hitboxes[firstActualHitboxIndex].startup;
        //print("currentstartup: " + currentStartup + " " + firstActualHitboxIndex);
        for (int i = 1; i < move.hitProperties.Length; i++)
        {
            
            if (!move.hitProperties[i].fromBeginning)
            {
                currentStartup += move.hitProperties[i].newHitFrame;
            }
            
           
            
                
            //10 + 5
            //5
            //5
            //exp: 10, 15, 
            //if ((!move.multipleHitsFrameData[i].fromBeginning && move.hitboxes[0].compositeCollider != null &&
            //  controller.seq == move.multipleHitsFrameData[i].newHitFrame + move.hitboxes[0].startup) ||
            //(move.multipleHitsFrameData[i].fromBeginning && controller.seq == move.multipleHitsFrameData[i].newHitFrame))
            if ((!move.hitProperties[i].fromBeginning && controller.seq == currentStartup) ||
            (move.hitProperties[i].fromBeginning && controller.seq == move.hitProperties[i].newHitFrame))
            {
                //print("multi: " + currentStartup);
                if (i >= lastHitIndex)
                {
                    move.lastHit = true;
                }
               
                move.ModifyMultipleHitsFrameData(i, move);
                if (move.hitProperties[i].multiHitting)
                {
                    move.objectsHit.Clear();
                    
                }
                break;
            }
        }
    }

    protected void SetHitstopKeyFrame ()
    {

        //totalAnimationTime = move == null ? 1 : ((move.animationStartup * Time.fixedDeltaTime) * currentAnimationSpeed
        //   + controller.animator.GetCurrentAnimatorClipInfo(0)[0].clip.length - move.animationStartup * Time.fixedDeltaTime);
        if (move != null && move.hitboxes.Length > 0 && controller.seq <= move.hitboxes[move.hitboxes.Length - 1].startup + move.hitboxes[move.hitboxes.Length - 1].active)
        {

            totalAnimationTime = controller.animator.GetCurrentAnimatorClipInfo(0).Length == 0 ? 1 : controller.animator.GetCurrentAnimatorClipInfo(0)[0].clip.length;
            float normalizedAnimationTime;
            float slowInterval = 0;
            int slowFrames = 0;

            if (slowMotionWindows.Length > 0)
            {
                //if (move != null)
                {
                    for (int i = 0; i < slowMotionWindows.Length; i++)
                    {
                        if (slowMotionWindows[i].unscaledAnimatorSpeed && slowMotionWindows[i].windowStart <= controller.seq)
                        {
                            int currentSlowInterval = Mathf.Min(controller.seq, slowMotionWindows[i].windowEnd) - (slowMotionWindows[i].windowStart) + 1;// + 2;
                            slowFrames += currentSlowInterval;

                            slowInterval += (currentSlowInterval * (slowMotionWindows[i].timeScale));
                            //print("currentslowinterval: " + currentSlowInterval + " slowinterval: " + slowInterval + " slowframes: " + slowFrames);
                        }
                    }

                }
            }

            //float animationSpeedMultiplier = currentAnimationSpeed;

            //test
            difference = Mathf.Max(controller.seq - (move.hitboxes[firstActualHitboxIndex].startup + move.hitboxes[firstActualHitboxIndex].active), 0);

            animationSpeedMultiplier = (Mathf.Min((move.hitboxes[firstActualHitboxIndex].startup + move.hitboxes[firstActualHitboxIndex].active), controller.seq) * startupAnimationSpeed
                + difference * move.middleAninationSpeed) / controller.seq;


            //fine test

            if (animationSpeedIntervals.Length > 0)
            {
                float speedratio = 0;
                int checkedIntervalLength = 0;
                for (int i = 0; i < animationSpeedIntervals.Length; i++)
                {
                    checkedIntervalLength = Mathf.Min(controller.seq, animationSpeedIntervals[i].windowEnd) -
                        (i == 0 ? 0 : animationSpeedIntervals[i - 1].windowEnd);
                    speedratio += checkedIntervalLength * animationSpeedIntervals[i].animationSpeed;
                    if (controller.seq < animationSpeedIntervals[i].windowEnd)
                    {
                        //print("break " + speedratio);
                        break;
                    }
                    if (i == animationSpeedIntervals.Length - 1 && controller.seq > animationSpeedIntervals[i].windowEnd)
                    {
                        speedratio += (controller.seq - animationSpeedIntervals[i].windowEnd);
                        //print("ALLOOOORA: " + speedratio);

                    }
                }
                //print("maaaa..." + controller.seq);
                speedratio /= controller.seq != 0 ? controller.seq : 1;
                animationSpeedMultiplier = speedratio;

            }
            ////print("seq: " + (controller.seq)+ " totstartup: " + ((controller.seq - slowFrames) + slowInterval));
            //print("animationtime: " + totalAnimationTime);

            normalizedAnimationTime = ((controller.seq - 0 - slowFrames) + slowInterval) * animationSpeedMultiplier * Time.fixedDeltaTime / totalAnimationTime;
            if (status.faction == 1 && resumingFromHitstop == false)
            {
                //print("animationspeedmultiplier: " + animationSpeedMultiplier);
                //print("ALORA: " + normalizedAnimationTime + " " + 
                //((controller.seq - slowFrames) + slowInterval) * animationSpeedMultiplier * Time.fixedDeltaTime + " " + totalAnimationTime);
                //print("skrrra: " + controller.seq * Time.fixedDeltaTime * animationSpeedMultiplier);
                //print(controller.animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
            }
            //if (status.faction == 0)
            //  print("porei " + (controller.seq - slowFrames + slowInterval) + " " + animationSpeedMultiplier * Time.fixedDeltaTime + " " + totalAnimationTime);
            controller.animator.Play(animatorParameterIndex, 0, normalizedAnimationTime);
        }


    }

    protected void Hitstop () {
		
		if (controller.hitstop > 0) {

			controller.inHitstop = true;
			//controller.seq--;
			controller.targetVelocity.x = 0;

            //salvo la velocita' verticale se sono al primo frame di hitstop
            if (!resumingFromHitstop) {
				savedVerticalSpeed = controller.velocity.y;
				//print ("savedverticalspeed: " + savedVerticalSpeed);
			}
            //else
                controller.animatorSpeed = 0;


            //se sei in aria, ferma la velocità verticale
            //if (controller.status.myPhysicsStatus == Status.physicsStatus.airborne)
            controller.velocity.y = 0;

            //controller.animator.updateMode = AnimatorUpdateMode.AnimatePhysics;
            
			//controller.animator.speed = 0;

            SetHitstopKeyFrame();





			//controller.hitstop--;
            //print("hitstop: " + controller.hitstop + " " + status.faction + " " + gameObject);

            resumingFromHitstop = true;
            controller.animator.SetFloat(controller.vspeedIndex, savedVerticalSpeed);
        }
        else if (resumingFromHitstop) {
            //controller.animator.updateMode = AnimatorUpdateMode.Normal;
			controller.animatorSpeed = currentAnimationSpeed;
			//print ("finito hitstop: " + Time.frameCount + " " + status.faction);
			//if (status.myPhysicsStatus == Status.physicsStatus.airborne)
				//&& status.mystatus != Status.status.juggle)
				controller.velocity.y = savedVerticalSpeed;
            //if(status.faction==1)
            //print("vert " + savedVerticalSpeed);
			controller.inHitstop = false;
			resumingFromHitstop = false;
            //controller.currentGravity = controller.originalGravity;
            controller.animator.SetFloat(controller.vspeedIndex, savedVerticalSpeed);


        }

    }




		
	/*
	public void MyUpdate () {
		if (controller.attack) 
			Attack ();
		if (hurtboxHit.hit)
			StopSequence ();



	}

*/


        
	public void SetAnimationSpeed (float actual, int desired) {
		float speed = (float) actual / (float) desired;
		//Debug.Log ("actual = " + actual + " desired = " + desired);
		//Debug.Log (speed);
        
		controller.animatorSpeed = speed;
		currentAnimationSpeed = speed;
	}

	public virtual void StopSequence() {
        //print ("seq: " + controller.seq);
        //Time.timeScale = 1;
        alreadyPrintedReach = false;
        //controller.animator.updateMode = AnimatorUpdateMode.AnimatePhysics;
        if (originalParent.gameObject.activeInHierarchy)
        {
            tr.parent = originalParent;
            tr.localPosition = Vector2.zero;
            tr.localScale = Vector3.one;

            //Physics2D.SyncTransforms();
        }

        if (controller.activeWeaponSprite != null)
        {
            controller.activeWeaponSprite.SetActive(true);
        }

        controller.hitWhileInvincible = false;

        isCharging = false;
        controller.inHitstop = false;
        controller.isParrying = false;

        controller.frontVisibilityModifier = 0;
        controller.backVisibilityModifier = 0;

        //for (int i = 0; i < activatedAnimationLayerIndexes.Length; i++)
        {
            //controller.animator.SetLayerWeight(controller.variantLayerIndex, 0);
            if (status.myPhysicsStatus == Status.physicsStatus.airborne)
                controller.variantLayerWeight = 0;
        }
        if (controller.shakerAnimator != null)
            controller.shakerAnimator.SetLayerWeight(1, 0);
        controller.animator.SetBool(controller.loopStateIndex, false);

        if (endAnimationOnStopSequence)
        {
            if (!smoothTransitionToNeutral && animatorParameterName != null && autoFollowup.followupMove == null)
                controller.animator.Play(controller.neutralIndex, 0, 0f);
            else
            {
                
                controller.animator.SetTrigger(controller.neutraltriggerIndex);
                //print("neutral" + Time.frameCount);
            }
        }
        if (walkingAnimationChanger != null)
        {
            walkingAnimationChanger.Stop();
        }

        if (dontTransitionToWalkAnimation)
            controller.dontTransitionToWalkAnimation = true;


        /*
        if (status.myPhysicsStatus == Status.physicsStatus.airborne && (!controller.animator.GetNextAnimatorStateInfo(0).IsName("jump rising")
            && !controller.animator.GetCurrentAnimatorStateInfo(0).IsName("jump rising")))
            controller.animator.Play("jump rising", 0, 0f);
            */
        controller.avoidingCharacters = false;
        controller.ResetAllHurtboxesSize();

        if (moveset != null && moveset.wallHitSpark != null)
        {
            moveset.wallHitSpark.attackIsActive = false;
            moveset.wallHitSpark.hasEmitted = false;
        }
        controller.previousMoveSeq = controller.seq;
        MyStopSequence();
		controller.targetVelocity.x = 0;
		//currentDashMomentum = 0;

        
        //controller.customLanding = false;
        //savedVerticalSpeed = 0;
        //print ("STOP");
        //recoil = 0;
        //objectHit = null;
        hitstopAlreadyHappened = false;
        if (move != null)
        {
            //if (move.multipleHitsFrameData.Length > 0)
                move.RestoreInitialFrameData();
            //move.lastHit = false;

            move.objectsHit.Clear();
            for (int i = 0; i<move.alternativeHitboxes.Length; i++)
            {
                move.alternativeHitboxes[i].objectsHit.Clear();
                move.alternativeHitboxes[i].RestoreInitialFrameData();
            }

            DisableAllHitboxes();

        }
		controller.animatorSpeed = 1;
        currentAnimationSpeed = 1;
		//controller.hitstop = 0;
		resumingFromHitstop = false;
		controller.conditions.ResetUsable ();
        
		controller.seq = 0;
		//Debug.Log ("STOP!");
		status.mystatus = Status.status.neutral;
		controller.isAffectedByGravity = true;
		interruptsWhenAirborne = originalInterruptsWhenAirborne;
		canWalkWhenAirborne = originalCanWalkWhenAirborne;
		canWalkWhenGrounded = originalCanWalkWhenGrounded;


		//if (animatorParameterName != null && autoFollowup.followupMove == null)
			//controller.animator.Play("neutral", -1, 0f);

		canFlip = false;
        if (status.maxHyperArmor == 0)
         status.hyperArmor = 0;
        //status.armor = status.maxArmor;
		status.invincible = false;
		isCancelling = false;

        //if (status.faction == 0)
          //  print("stoppppp " + controller.currentMove);
		controller.currentMove = null;
        Physics2D.SyncTransforms();

        //print("startcoroutine");
        if (gameObject.activeInHierarchy == true)
        {
            //print("strtcoroutine");
            StartCoroutine(status.WaitBeforeRechargeStamina());
        }

        
    }

	public virtual void MyStopSequence() {

	}

	public void ExecuteAutomaticFollowups () {
		if (autoFollowup.followupMove == null && continuousStaminaSpendingIntervals.Length == 0)
			return;
		if (autoFollowup.followupMove != null && autoFollowup.cancelStart > 0 && controller.seq >= autoFollowup.cancelStart - 1)
        {
			controller.StartNewMove (autoFollowup.followupMove);
            return;
		}
        if (continuousStaminaSpendingIntervals.Length > 0)
        {
            for (int i = 0; i < continuousStaminaSpendingIntervals.Length; i++)
            {
                if (status.currentStamina <= 0 && continuousStaminaSpendingIntervals[i].optionalFollowupOnStaminaOver != null &&
                    controller.seq >= continuousStaminaSpendingIntervals[i].start && controller.seq < continuousStaminaSpendingIntervals[i].end)
                {
                    controller.StartNewMove(continuousStaminaSpendingIntervals[i].optionalFollowupOnStaminaOver);
                    return;
                }
            }
        }
	}

	public void SetFollowupUsability () {
		if (followups.Length <= 0)
			return;

        for (int i = 0; i < followups.Length; i++)
        {
            if (followups[i].type == moveType.move)
                conditions.moves[followups[i].followupMoveIndex].usable = false;
            else
                conditions.animations[followups[i].followupMoveIndex].usable = false;
        }

        for (int i = 0; i < followups.Length; i++) {

            if (!followups[i].fromRecovery || (move == null || move.hitboxes.Length <= 0))
            {
                if (controller.seq <= followups[i].cancelWindowEnd - 2 && controller.seq >= followups[i].cancelWindowStart - 2) //da testare: -2 perchè a) tengo conto dello zero, e b) perchè una volta resa usabile in realtà la mossa può partire solo dal frame successivo
                {
                    if (followups[i].type == moveType.move)
                        conditions.moves[followups[i].followupMoveIndex].usable = true;
                    else
                        conditions.animations[followups[i].followupMoveIndex].usable = true;
                }
                else
                {
                    /*
                    if (followups[i].type == moveType.move)
                    {
                        conditions.moves[followups[i].followupMoveIndex].usable = false;
                    }
                    else
                        conditions.animations[followups[i].followupMoveIndex].usable = false;
                        */
                }
            }
            else 
            {
                
                {
                    if (controller.seq - move.hitboxes[lastActualHitboxIndex].startup <= followups[i].cancelWindowEnd - 2 &&
                        controller.seq - move.hitboxes[lastActualHitboxIndex].startup >= followups[i].cancelWindowStart - 2)//da testare: -2 perchè a) tengo conto dello zero, e b) perchè una volta resa usabile in realtà la mossa può partire solo dal frame successivo
                    {
                        if (followups[i].type == moveType.move)
                            conditions.moves[followups[i].followupMoveIndex].usable = true;
                        else
                            conditions.animations[followups[i].followupMoveIndex].usable = true;
                    }
                    else
                    {/*
                        if (followups[i].type == moveType.move)
                        {
                            conditions.moves[followups[i].followupMoveIndex].usable = false;
                        }
                        else
                            conditions.animations[followups[i].followupMoveIndex].usable = false;
                            */

                    }
                }
            }
		}
	}

	public void SetFlipWindows () {
        canFlip = false;
		for (int i = 0; i < canFlipPeriods.Length; i++) {
			if (controller.seq <= canFlipPeriods [i].flipWindowEnd - 1 && controller.seq >= canFlipPeriods [i].flipWindowStart - 1) {
				canFlip = true;
                break;
			}
		}
	}

    

    public void BaseCustom ()
    {

        controller.animator.SetBool(controller.neutralIndex, false);

        controller.variantAnimatorLayerWeightGainCurrent = variantWeightGain == -1 ? controller.variantAnimatorLayerWeightGainCurrent
            : variantWeightGain;

        controller.variantAnimatorLayerWeightLossCurrent = variantWeightLoss == -1 ? 1 : variantWeightLoss;

        

        /*
        if (controller.seq == 0)
        {
            
            if (loopingAnimation)
            {
                controller.animator.SetBool("loopstate", true);
            }
            else
                controller.animator.SetBool("loopstate", false);



            if (controller.hitstop == 0 && !resumingFromHitstop)
            {
                if (usingAnimatorTrigger)
                    controller.animator.SetTrigger(animatorParameterName);
                else
                    controller.animator.Play(animatorParameterName, 0, 0);
            }
                

        }
        */

        SetStatus();
    }

    //viene chiamata una volta sola quando inizia la mossa
    public void StartSequence ()
    {
        print("start: " + gameObject + " at frame: " + TimeVariables.logicFramesSinceStart);

        canBufferRun = true;
        //if (maxBufferableRunFrames == 0)
            maxBufferableRunFrames = 8;

        SetStatus();
        //se trovo la corsa nei followups, setto il maxBufferableRunFrames a infinito
        for (int i = 0; i < followups.Length; i++)
        {
            if (followups[i].type == moveType.animation && followups[i].followupMoveIndex == 16)
            {
                maxBufferableRunFrames = 100000;
                break;
            }
        }

        MyStartSequence();

        
        tr.parent = controller.tr;
        tr.localPosition = Vector2.zero;
        tr.localScale = Vector3.one;
        //tr.localScale = controller.tr.localScale;
        Physics2D.SyncTransforms();
        //print("start " + gameObject);
        debugHurtboxPositionAtStart = controller.hurtboxes[0].hurtbox.bounds.max.x;
        if (loopingAnimation)
        {
            controller.animator.SetBool(controller.loopStateIndex, true);
        }
        else
            controller.animator.SetBool(controller.loopStateIndex, false);

        if (!walkingLegs)
            controller.framesSpentWalking = 0;

        controller.animator.ResetTrigger(controller.neutraltriggerIndex);
        controller.animator.ResetTrigger(controller.walkTriggerIndex);

        

        if (!walkingLegs || (canWalkWhenGrounded && controller.horizontal == 0))
        {
            
            if (usingAnimatorTrigger && (!dontTransitionIfAlreadyTransitioning || !controller.animator.IsInTransition(0)))
            {
                
                if (animatorTriggerConditions.Length > 0)
                {
                    bool conditionsMet = false;
                    for (int i = 0; i < animatorTriggerConditions.Length; i++)
                    {
                        conditionsMet = true;
                        if (animatorTriggerConditions[i].moveType != AnimatorTriggerCondition.MoveType.none)
                        {
                            if ((animatorTriggerConditions[i].moveType == AnimatorTriggerCondition.MoveType.animation
                                && controller.previousMove != conditions.animations[animatorTriggerConditions[i].moveIndex].move)
                                ||
                                (animatorTriggerConditions[i].moveType == AnimatorTriggerCondition.MoveType.move
                                && controller.previousMove != conditions.moves[animatorTriggerConditions[i].moveIndex].move))
                            {
                                //print("falso: " + controller.previousMove + " " + conditions.moves[animatorTriggerConditions[i].moveIndex].move);
                                conditionsMet = false;
                                continue;
                            }
                        }
                        if (animatorTriggerConditions[i].windowEnd != 0 && (controller.previousMoveSeq >= animatorTriggerConditions[i].windowEnd || 
                            controller.previousMoveSeq < animatorTriggerConditions[i].windowStart))
                        {
                            //print("transition: " + controller.previousMoveSeq);
                            conditionsMet = false;
                            continue;
                        }
                        if (animatorTriggerConditions[i].usesStatusConditions)
                        {
                            //print(status.mystatus);
                            if (status.myPreviousStatus != animatorTriggerConditions[i].statusConditions)
                            {

                                conditionsMet = false;
                                continue;
                            }
                        }
                        if (conditionsMet)
                        {
                            //print("sono arrivato");
                            controller.animator.SetTrigger(animatorParameterIndex);
                            break;
                        }
                    }
                    if (!conditionsMet)
                    {
                        controller.animator.Play(animatorParameterIndex, 0, 0);
                    }
                }

                else
                {
                    controller.animator.SetTrigger(animatorParameterIndex);
                    
                }

            }
            else
            {
                if (animatorParameterName != "")
                    controller.animator.Play(animatorParameterIndex, 0, 0);
            }
        }
        else
        {
            /*
            if (controller.horizontal == 0)
            {
                if (usingAnimatorTrigger)
                    controller.animator.SetTrigger(animatorParameterName);
                else
                {
                    if (animatorParameterName != "")
                        controller.animator.Play(animatorParameterName, 0, 0);
                }
            }
            */
            
        }

        if (variantAnimatorParameterName != "")
        {
            controller.animator.Play(variantAnimatorParameterIndex, controller.variantLayerIndex, 0);
            //print("variant: " + variantAnimatorParameterName);
        }
        if (stopsAerialMomentum)
        {
            if (canWalkWhenAirborne)
            {
                controller.maxAerialMomentum = usesNormalWalkspeed ? controller.movespeed : walkVelocity;
                //print("max aerial mom: " + controller.maxAerialMomentum);

                if (Mathf.Abs(controller.aerialMomentum) > controller.maxAerialMomentum)
                {
                    controller.aerialMomentum = controller.aerialMomentum > 0 ? controller.maxAerialMomentum : -controller.maxAerialMomentum;
                    //print("aerial mom: " + controller.aerialMomentum);
                }
            }
            else
                controller.aerialMomentum = 0;
        }
        else
        {
            //controller.aerialMomentum = controller.maxAerialMomentum; //WHAT???
            controller.aerialMomentum = controller.targetVelocity.x; //da testare
        }

        controller.currentTimeScale = 1;
        //controller.jumpingState = false;
        //MyStartSequence();
        if (move != null)
        {
            move.facingRight = controller.facingRight;
            move.lastHit = false;
        }
        SyncFrameDataFacing();
        //print("alora: " + controller);
        if (mayExecuteNow)
            controller.currentMove.ExecuteSequence();

        
        
    }

    public void SyncFrameDataFacing()
    {
        if (move == null)
            return;
        if (move.alternativeHitboxes.Length > 0)
        {
            for (int i = 0; i < move.alternativeHitboxes.Length; i++)
            {
                move.alternativeHitboxes[i].facingRight = move.facingRight;
            }
        }
    }

    public virtual void MyStartSequence ()
    {

    }

	public virtual void SetStatus () {
        
	}

	public virtual void ExecuteSequence () {

        
            
        for (int i = 0; i < activatedAnimationLayerIndexes.Length; i++)
        {
            //controller.animator.SetLayerWeight(activatedAnimationLayerIndexes[i], 1);
        }

		BaseCustom ();

		SetFollowupUsability ();
		SetInvincibility ();

        


		SetFlipWindows ();
        AvoidsCharacterWindows();
        //SetSlowMotion();
        //SetTimeFreeze();
		controller.previousRunToggle = false;

        //if (controller.seq == 0 && animatorParameterName != "")
        //  controller.animator.Play(animatorParameterName, -1, 0f);

        for (int i = 0; i < applyDebuffs.Count; i++)
        {
            if (applyDebuffs[i].buffConditions == ApplyDebuff.BuffConditions.onActivation && controller.seq == applyDebuffs[i].activationFrame)
                applyDebuffs[i].OnActivationApply();
        }

        controller.frontVisibilityModifier = 0;
        controller.backVisibilityModifier = 0;

        for (int i = 0; i < visibilityModifiers.Length; i++)
        {
            if (controller.seq >= visibilityModifiers[i].interval.start && controller.seq < visibilityModifiers[i].interval.end)
            {
                controller.frontVisibilityModifier = visibilityModifiers[i].frontVisibilityModifier;
                controller.backVisibilityModifier = visibilityModifiers[i].backVisibilityModifier;

            }
        }


        if (!canRechargeStamina || (rechargeStaminaFrame > 0 && controller.seq < rechargeStaminaFrame))
        {
            status.canRecharge = false;
        }
        else// if (rechargeStaminaFrame != 0 && controller.seq >= rechargeStaminaFrame)
        {
            status.canRecharge = true;
        }


        bool a = (move != null && move.hitboxes.Length > 0);

        if ((!a && controller.seq == 0))// || (a && controller.seq == move.hitboxes[0].startup - 1))
        {
			if (requiresStamina && !controller.inHitstop) {
				
				status.SpendStamina (staminaCost);
               
			    
                
				
			}

		}

        if (controller.seq == 0 && requiresMeter && !controller.inHitstop && meterCostFraction != 0)
        {
            //status.SpendMeter(status.maxMeter / 2);
            //print(Mathf.RoundToInt(((float) status.maxMeter / 100)) * meterCostPercentage);
            //print(Mathf.RoundToInt((float)status.maxMeter / meterCostFraction));

            status.SpendMeter(Mathf.RoundToInt((float)status.maxMeter / meterCostFraction));
        }

        //ExecuteAutomaticFollowups ();
        /*
        if (autoFollowup.followupMove != null && controller.seq >= autoFollowup.cancelStart - 1)
        {
            controller.StartNewMove(autoFollowup.followupMove);
            return;
        }
        */

        //se è un attacco caricabile:
        if (controller.hitstop <= 0)
        {
            for (int i = 0; i < chargedFollowups.Length; i++)
            {
                
                if (controller.seq >= chargedFollowups[i].windowStart && controller.seq <= chargedFollowups[i].windowEnd && !isCharging)
                {
                    
                    controller.StartNewMove(chargedFollowups[i].moveType == moveType.move ? controller.moves[chargedFollowups[i].moveIndex] : controller.animations[chargedFollowups[i].moveIndex]);
                    return;
                }
            }
        }

        //if (move != null && move.hitboxes.Length != 0)
        {
            if (animationSpeedIntervals.Length == 0) {
                if (move != null && move.hitboxes.Length != 0)
                {
                    if (controller.seq <= move.hitboxes[firstActualHitboxIndex].startup + move.hitboxes[firstActualHitboxIndex].active)
                    {
                        SetAnimationSpeed(move.animationStartup - 1, move.hitboxes[firstActualHitboxIndex].startup);
                        startupAnimationSpeed = currentAnimationSpeed;
                    }
                    else
                    {
                        currentAnimationSpeed = move.middleAninationSpeed;
                        controller.animatorSpeed = currentAnimationSpeed;
                    }
                }
                else
                {
                    currentAnimationSpeed = 1;
                    //currentAnimationSpeed = 1;
                    controller.animatorSpeed = currentAnimationSpeed;
                }
            }
            else
            {
                for (int i = 0; i < animationSpeedIntervals.Length; i++)
                {
                    if (controller.seq <= animationSpeedIntervals[i].windowEnd)
                    {
                        currentAnimationSpeed = animationSpeedIntervals[i].animationSpeed;

                        controller.animatorSpeed = currentAnimationSpeed;
                        break;
                    }
                    else
                    {
                        currentAnimationSpeed = 1;
                        controller.animatorSpeed = 1;
                    }
                }
            }


            if (animationSpeedIntervals.Length == 0 && move != null && move.hitboxes.Length > 0 && move.hitboxes[move.hitboxes.Length - 1].startup + move.hitboxes[move.hitboxes.Length - 1].active + move.recoveryOffset < controller.seq)
            {
                //controller.animator.speed = 1;
                //print("recovery");
                currentAnimationSpeed = move.recoveryAnimationSpeed <= 0 ? 1 : move.recoveryAnimationSpeed;
                //currentAnimationSpeed = 1;
                controller.animatorSpeed = currentAnimationSpeed;
            }
        }
        controller.currentTimeScale = 1;
        SetTimeFreeze();



        //if (status.myPhysicsStatus == Status.physicsStatus.grounded && 
        controller.targetVelocity.x = 0;


        if (usingMovementsFunction)
			Movements ();

        if (canWalkAirborneInterval.start != canWalkAirborneInterval.end)
        {
            if (controller.seq >= canWalkAirborneInterval.start && controller.seq < canWalkAirborneInterval.end)
            {
                canWalkWhenAirborne = true;
            }
            else
            {
                canWalkWhenAirborne = false;
            }
        }

        if (canWalkGroundedInterval.start != canWalkGroundedInterval.end)
        {
            if (controller.seq >= canWalkGroundedInterval.start && controller.seq < canWalkGroundedInterval.end)
            {
                canWalkWhenGrounded = true;
            }
            else
            {
                canWalkWhenGrounded = false;
            }
        }
        


        ModifyHurtboxesDimensions();

		//if (status.myPhysicsStatus == Status.physicsStatus.airborne)
        {
            //controller.seq++;
            if (controller.velocity.y <= 0 && noGravityFrames > 0 && controller.seq <= noGravityFrames)
            //if (noGravityFrames > 0 && controller.seq <= noGravityFrames)
            {

                controller.isAffectedByGravity = false;
                controller.velocity.y = 0;
            }
            else
            {
                controller.isAffectedByGravity = true;
                
            }
		}

        /*
		if (currentDashMomentum > 0)
			currentDashMomentum -= momentumReductionSpeed;
		if (currentDashMomentum < 0)
			currentDashMomentum = 0;
		if (controller.status.myPreviousStatus == Status.status.dashing && maintainsDashMomentum) {
			currentDashMomentum = initialDashMomentum;
		}
        

		controller.targetVelocity.x += controller.gameObject.transform.localScale.x * currentDashMomentum;
        */

        if (move != null && move.hyperArmorPoints > 0)
        {
            if (controller.seq < move.hyperArmorWindowStart || controller.seq >= move.hyperArmorWindowEnd)
                status.hyperArmor = 0;
            else if (controller.seq == move.hyperArmorWindowStart)
                status.hyperArmor = move.hyperArmorPoints;

        }

        //print("executesequence " + Time.frameCount + " " + status.faction);

        if (walkingAnimationChanger != null)
            walkingAnimationChanger.SetWalkingAnimation();

        Hitstop ();
		if (controller.hitstop <= 0)
		{

            

            if (move != null)
            {
                ManageHitboxes(move);

                for (int i = 0; i < move.alternativeHitboxes.Length; i++)
                {
                    //print("manage!" + i);
                    ManageHitboxes(move.alternativeHitboxes[i]);
                }

                


                    /*
                    if (move.multipleHitsFrameData.Length > 0)
                    {
                        ManageMultipleHits();
                    } 







                    for (int i = 0; i < move.hitboxes.Length; i++)
                    {
                        if (move.hitboxes[i].ghostHitbox != null)
                        {
                            if (controller.seq > move.hitboxes[i].startup - move.hitboxes[i].ghostHitboxFramesOffset && controller.seq <= move.hitboxes[i].startup)
                            {

                                {
                                    move.hitboxes[i].ghostHitbox.SetActive(true);

                                }


                            }
                            else
                            {
                                {
                                    //move.hitboxes[i].ghostHitbox.SetActive(false);

                                }
                            }
                        }
                        //print (hitboxes.Length);
                        if (move.hitboxes[i].startup + move.hitboxes[i].active - 1 > controller.seq && controller.seq >= move.hitboxes[i].startup - 1)
                        {
                            //print ("sono attackscript, gameobject: " + controller.gameObject + "frame: " + Time.frameCount);

                            if (controller.seq == move.hitboxes[i].startup - 1 && requiresStamina && !controller.inHitstop)
                                status.SpendStamina(move.hitboxes[i].staminaCost);
                            EnableHitbox(i);
                            break;
                            //controller.hitstop = 30;
                        }
                        else
                        {
                            //move.hitboxes[i].ghostHitbox.SetActive(false);

                            DisableHitbox(i);
                        }
                        if (controller.seq > move.hitboxes[i].startup + move.hitboxes[i].active - 1)
                        {
                            move.hitboxes[i].ghostHitbox.SetActive(false);
                        }
                    }
                    */
            }

            if (controller.seq >= maxDurationFrames)
                itsTimeToStopTheSequence = true;

            MyExecuteSequence();
            
            //controller.animator.Play(animatorParameterName, 0, controller.seq * Time.fixedDeltaTime);
        }
        else
        {
            controller.hitstop--;
        }


        //if (controller.status.faction == 0)
        //print("SEQ: " + controller.seq + " stop? " + itsTimeToStopTheSequence + " " + Time.fixedTime);

        for (int i = 0; i < customStatusPeriods.Length; i++)
        {
            if (controller.seq <= customStatusPeriods[i].finish && controller.seq >= customStatusPeriods[i].start)
            {
                status.mystatus = customStatusPeriods[i].status;
                break;
            }
        }

        //Hitstop ();
        //print (controller.gameObject + " velocity: " + controller.targetVelocity.x);
        if (autoFollowup.followupMove != null && autoFollowup.cancelStart > 0 && controller.seq >= autoFollowup.cancelStart - 1)
        {
            controller.StartNewMove(autoFollowup.followupMove);
            return;
        }
        if (continuousStaminaSpendingIntervals.Length > 0)
        {
            for (int i = 0; i < continuousStaminaSpendingIntervals.Length; i++)
            {
                if (controller.seq >= continuousStaminaSpendingIntervals[i].start && controller.seq < continuousStaminaSpendingIntervals[i].end)
                {
                    status.currentStamina -= continuousStaminaSpendingIntervals[i].staminaCost;
                    if (status.currentStamina <= 0 && continuousStaminaSpendingIntervals[i].optionalFollowupOnStaminaOver != null)
                    {
                        controller.StartNewMove(continuousStaminaSpendingIntervals[i].optionalFollowupOnStaminaOver);
                        return;
                    }
                }
            }
        }


    }

	public void SetInvincibility () {
		if (invincibilityWindows.Length <= 0)
			return;

		for (int i = 0; i < invincibilityWindows.Length; i++) {
			if (controller.seq < invincibilityWindows [i].invincibilityWindowEnd && controller.seq >= invincibilityWindows [i].invincibilityWindowStart) {
				controller.status.invincible = true;
			} else {
				
				controller.status.invincible = false;
					}
				
			}
		}


	public virtual void MyExecuteSequence () {



	}

	public void OnHit () {
		//utilizzare solo se l'animazione è un attacco

		//if (controller.gachiBass)
        {
			//print ("jump startup: " + controller.jumpstartup);
			//print ("jump startup: " + Time.fixedTime);
		}

        status.heatDecayCounter = 0;
		
		controller.jumpstartup = 0;
        if (move != null)
        {
            //controller.hitstop = move.hitstop;

            if (moveset.myHitsparkGenerator != null)
            {
                //da parametrizzare!


                //scintille lunghe
                //moveset.myHitsparkGenerator.GenerateHitsparks(new Vector2((controller.facingRight ? 80 : -80), 20), 20);
                //GlobalBloodGenerator.Instance.GenerateBlood(6, )
                
            }

            //screen shake
            controller.cameraMovement.Shake(Mathf.Min(20, Mathf.Max(10, move.currentHitProperties.hitstop)), 
                move.currentHitProperties.hitstop > 16 ? 0.25f : move.currentHitProperties.hitstop >= 7 ? 0.3f : 0.27f);
            

            if (!move.GetGainedMeter())
            {
                if (!requiresMeter || meterCostFraction <= 0)
                {
                    if (moveset != null)
                        for (int i = 0; i < moveset.glowAnimationSetters.Length; i++)
                        {
                            moveset.glowAnimationSetters[i].Glow();
                        }
                    status.currentHeatAmount += move.heatGainOnHit;
                    if (status.currentHeatAmount > status.maxHeatGauge)
                        status.currentHeatAmount = status.maxHeatGauge;
                }
                
                status.currentMeter += Mathf.RoundToInt( move.currentHitProperties.meterGain * status.currentMeterGainMultiplier);
                if (status.currentMeter > status.maxMeter)
                    status.currentMeter = status.maxMeter;
                move.SetGainedMeter(false);
            }
        
			//print ("COLPO");
			//if (status.mystatus != Status.status.juggle)
				//savedVerticalSpeed = controller.velocity.y;
			for (int i = 0; i < onHitProperties.Count; i++) {
                onHitProperties[i].OnHitStart();
				onHitProperties [i].OnHitApply ();
			}

            for (int i = 0; i < applyDebuffs.Count; i++)
            {
                if (applyDebuffs[i].buffConditions == ApplyDebuff.BuffConditions.onHit)
                {
                    applyDebuffs[i].OnHitStart();

                    applyDebuffs[i].OnHitApply();

                }
            }


            //print ("CIAO");
        }

        

		MyOnHit ();
	}

	public virtual void MyOnHit () {

	}

	public virtual void OnBlock () {
        //utilizzare solo se l'animazione è un attacco
        //print("block");
        OnHit();
        controller.cameraMovement.Shake(10, 0.35f);
        
    }


    protected virtual void Attack2 ()
	{

	}

	protected virtual void Kick ()
	{

	}

	protected void EnableHitbox (FrameData move, int i) {
		/*
		if (hitboxes[i] == null) {
			hitboxes[i] = gameObject.AddComponent<BoxCollider2D> ();
			hitboxes[i].offset = new Vector2 (move.hitboxes[i].hitboxOffsetX, move.hitboxes[i].hitboxOffsetY);
			hitboxes[i].size = new Vector2 (move.hitboxes[i].hitboxSizeX, move.hitboxes[i].hitboxSizeY);
		}
		*/

        

		if (move.hitboxes [i].compositeCollider != null) {
			move.hitboxes [i].compositeCollider.gameObject.SetActive(true);
            //transform.parent = controller.tr;
			return;
		}

        /*
		float directionX = Mathf.Sign( controller.gameObject.transform.localScale.x);
		RaycastHit2D horizontalHit = Physics2D.Raycast (move.hitboxes[i].hitboxOrigin.position, Vector2.right * directionX, 
			move.hitboxes[i].hitboxLength - shellRadius, collisionMask);

		RaycastHit2D verticalHit = Physics2D.Raycast (move.hitboxes[i].hitboxOrigin.position, Vector2.up, 
			move.hitboxes[i].hitboxHeight - shellRadius, collisionMask);

		float newHorizontalDistance = horizontalHit ? horizontalHit.distance : move.hitboxes [i].hitboxLength;
		float newVerticalDistance = verticalHit ? verticalHit.distance : move.hitboxes [i].hitboxHeight;


		horizontalHit = Physics2D.Raycast (new Vector2(move.hitboxes[i].hitboxOrigin.position.x, 
			move.hitboxes[i].hitboxOrigin.position.y + newVerticalDistance - shellRadius), Vector2.right * directionX, 
			move.hitboxes[i].hitboxLength - shellRadius, collisionMask);

		if (horizontalHit)
			newHorizontalDistance = newHorizontalDistance < horizontalHit.distance ? newHorizontalDistance : horizontalHit.distance;

		verticalHit = Physics2D.Raycast (new Vector2(move.hitboxes[i].hitboxOrigin.position.x + directionX * newHorizontalDistance - shellRadius,
			move.hitboxes[i].hitboxOrigin.position.y), Vector2.up, 
			move.hitboxes[i].hitboxHeight - shellRadius, collisionMask);

		if (verticalHit)
			newVerticalDistance = newVerticalDistance < verticalHit.distance ? newVerticalDistance : verticalHit.distance;

		if (hitboxes [i] == null) {
			hitboxes [i] = gameObject.AddComponent<BoxCollider2D> ();
		}

		hitboxes [i].offset = new Vector2 (transform.InverseTransformPoint(move.hitboxes [i].hitboxOrigin.position).x + (newHorizontalDistance) / 2, 
			transform.InverseTransformPoint(move.hitboxes [i].hitboxOrigin.position).y + (newVerticalDistance) / 2);
		hitboxes [i].size = new Vector2 (newHorizontalDistance, newVerticalDistance);
        */

	}

	protected void DisableHitbox(FrameData move, int i) {
        //hitbox.enabled = false;
        
        if (move.hitboxes [i].compositeCollider != null) {
			move.hitboxes [i].compositeCollider.gameObject.SetActive(false);
            move.hitboxes[i].compositeCollider.transform.parent = move.transform;

        }


    }

	protected void DisableAllHitboxes() {
        if (move == null)
            return;
		for (int i = 0; i < move.hitboxes.Length; i++) {
			DisableHitbox (move, i);
            if (move.hitboxes[i].ghostHitbox != null)
            {
                move.hitboxes[i].ghostHitbox.gameObject.SetActive(false);
                move.ghostHitboxIsActive = false;
                move.hitboxes[i].ghostHitbox.transform.parent = tr;
                if (gameObject.activeInHierarchy)
                {
                    move.hitboxes[i].ghostHitbox.transform.localPosition = Vector2.zero;
                    move.hitboxes[i].ghostHitbox.transform.localScale = Vector3.one;

                    //Physics2D.SyncTransforms();
                }
            }
            //move.hitboxes[i].enableHitbox = false;
            //move.hitboxes[i].enableGhostHitbox = false;
        }

        for (int j = 0; j < move.alternativeHitboxes.Length; j++)
        {
            for (int i = 0; i < move.alternativeHitboxes[j].hitboxes.Length; i++)
            {
                DisableHitbox(move.alternativeHitboxes[j], i);
                if (move.alternativeHitboxes[j].hitboxes[i].ghostHitbox != null)
                {
                    move.alternativeHitboxes[j].hitboxes[i].ghostHitbox.gameObject.SetActive(false);
                    move.ghostHitboxIsActive = false;
                    move.alternativeHitboxes[j].hitboxes[i].ghostHitbox.transform.parent = tr;
                    if (gameObject.activeInHierarchy)
                    {
                        move.hitboxes[i].ghostHitbox.transform.localPosition = Vector2.zero;
                        move.hitboxes[i].ghostHitbox.transform.localScale = Vector3.one;

                        //Physics2D.SyncTransforms();
                    }
                }
                //move.alternativeHitboxes[j].hitboxes[i].enableHitbox = false;
                //move.alternativeHitboxes[j].hitboxes[i].enableGhostHitbox = false;
            }
           
        }
	}
    //metodo per l'inizializzazione del ghost Hitbox, che serve all'AI per determinare se la mossa la sta per colpire o no
    public void InitializeGhostHitbox (FrameData move)
    {
        if (move != null && move.hitboxes.Length > 0)
        {
            move.ghostHitboxIsActive = false;

            colliders = new List<GameObject>();

            for (int i = 0; i < move.hitboxes.Length; i++)
            {
                if (move.hitboxes[i].ghostHitbox != null)
                {
                    if (firstActualHitboxIndex == -1)
                        firstActualHitboxIndex = i;
                    lastActualHitboxIndex = i;
                }
            }

            for (int i = 0; i<move.hitboxes.Length; i++)
            {
                float movementBasedOffset = 0;
                int ghostStartup = move.hitboxes[i].startup - (move.hitboxes[i].ghostHitboxFramesOffset == 0 ?
                        moveset.defaultGhostHitboxFramesOffset : move.hitboxes[i].ghostHitboxFramesOffset);

                for (int k = 0; k < horizontalMovements.Length; k++)
                {
                    if ((!horizontalMovements[k].fromRecovery && ((horizontalMovements[k].windowStart <= ghostStartup &&
                        horizontalMovements[k].windowEnd > ghostStartup)
                        || (horizontalMovements[k].windowStart >= ghostStartup &&
                        move.hitboxes[i].startup + move.hitboxes[i].active - 1 >= horizontalMovements[k].windowStart))))
                    //move.hitboxes[lastActualHitboxIndex].startup + move.hitboxes[lastActualHitboxIndex].active - 1 >= horizontalMovements[k].windowStart))))

                    /*
                    ||
                    (horizontalMovements[k].fromRecovery && ((horizontalMovements[k].windowStart + move.hitboxes[firstActualHitboxIndex].startup <= ghostStartup
                    && horizontalMovements[k].windowEnd + move.hitboxes[firstActualHitboxIndex].startup >= ghostStartup)
                    || (horizontalMovements[k].windowStart + move.hitboxes[firstActualHitboxIndex].startup >= ghostStartup &&
                    move.hitboxes[i].startup + move.hitboxes[i].active >= horizontalMovements[k].windowStart + move.hitboxes[firstActualHitboxIndex].startup))))
                    */
                    {

                        movementBasedOffset += (Mathf.Min(move.hitboxes[i].startup + move.hitboxes[i].active, horizontalMovements[k].windowEnd) -
                                Mathf.Max(ghostStartup, horizontalMovements[k].windowStart)) * horizontalMovements[k].velocity;


                    }
                    else
                    if ((horizontalMovements[k].fromRecovery && ((horizontalMovements[k].windowStart + move.hitboxes[lastActualHitboxIndex].startup - 1 <= ghostStartup
                        && horizontalMovements[k].windowEnd + move.hitboxes[lastActualHitboxIndex].startup - 1 > ghostStartup)
                        || (horizontalMovements[k].windowStart + move.hitboxes[lastActualHitboxIndex].startup - 1 >= ghostStartup &&
                        move.hitboxes[i].startup + move.hitboxes[i].active - 1 >= horizontalMovements[k].windowStart + move.hitboxes[lastActualHitboxIndex].startup - 1))))
                    //if ((horizontalMovements[k].fromRecovery && ((horizontalMovements[k].windowStart + move.hitboxes[lastActualHitboxIndex].startup - 1 <= ghostStartup
                    //  && horizontalMovements[k].windowEnd + move.hitboxes[lastActualHitboxIndex].startup - 1 >= ghostStartup)
                    //|| (horizontalMovements[k].windowStart + move.hitboxes[lastActualHitboxIndex].startup - 1 >= ghostStartup &&
                    //move.hitboxes[i].startup + move.hitboxes[i].active - 1 >= horizontalMovements[k].windowStart + move.hitboxes[lastActualHitboxIndex].startup - 1))))
                    {
                        movementBasedOffset += (Mathf.Min(move.hitboxes[i].startup + move.hitboxes[i].active,
                            horizontalMovements[k].windowEnd + move.hitboxes[lastActualHitboxIndex].startup - 1) -
                                Mathf.Max(ghostStartup, horizontalMovements[k].windowStart + move.hitboxes[lastActualHitboxIndex].startup - 1))
                                * horizontalMovements[k].velocity;

                        
                        /*
                         * startup: 46, 75
                         * lastactualindex: 2 (75)
                         * ghoststartup: 36
                         * 
                         * movements: (-38, -30), (-30, 0) >> (37, 45), (45, 75)
                         * 
                         * (-38 + 75 -1 <= 36) && (-30 + 75 -1 >= 36)
                         * 
                         * min(46+1, -30+74) - max(36, -38+74) 
                         * 
                         * 
                         * 
                         * */
                    }

                }



                if (move.hitboxes[i].ghostHitbox != null)
                {

                    if (moveset != null && move.hitboxes[i].ghostHitboxFramesOffset == 0)
                        move.hitboxes[i].ghostHitboxFramesOffset = moveset.defaultGhostHitboxFramesOffset;

                    
                    move.ghostHitbox = move.hitboxes[i].ghostHitbox;
                    //print("ghost: " + gameObject);
                    move.hitboxes[i].ghostHitbox.gameObject.SetActive(true);
                    move.hitboxes[i].compositeCollider.gameObject.SetActive(true);

                    if (i == firstActualHitboxIndex)
                    {

                        for (int h = 0; h < horizontalMovements.Length; h++)
                        {
                            int endFrame;

                            if (horizontalMovements[h].fromRecovery)
                            {
                                if (horizontalMovements[h].windowStart + move.hitboxes[lastActualHitboxIndex].startup - 1>
                                    move.hitboxes[firstActualHitboxIndex].startup + move.hitboxes[firstActualHitboxIndex].active - 1)
                                    continue;

                                //46 > 65

                                endFrame = Mathf.Min(horizontalMovements[h].windowEnd + move.hitboxes[lastActualHitboxIndex].startup - 1,
                                    move.hitboxes[firstActualHitboxIndex].startup + move.hitboxes[firstActualHitboxIndex].active - 1);

                                //min(51, 66) = 51

                                totalHorizontalMovements += (endFrame - (horizontalMovements[h].windowStart + move.hitboxes[lastActualHitboxIndex].startup - 1))
                                    * horizontalMovements[h].velocity;

                                //
                            }
                            else
                            {
                                if (horizontalMovements[h].windowStart >
                                    move.hitboxes[firstActualHitboxIndex].startup + move.hitboxes[firstActualHitboxIndex].active - 1)
                                    continue;

                                endFrame = Mathf.Min(horizontalMovements[h].windowEnd,
                                    move.hitboxes[firstActualHitboxIndex].startup + move.hitboxes[firstActualHitboxIndex].active - 1);

                                totalHorizontalMovements += (endFrame - horizontalMovements[h].windowStart)
                                    * horizontalMovements[h].velocity;
                            }
                        }

                        totalHorizontalMovements *= 0.01666667f;
                    }
                    

                    movementBasedOffset *= 0.01666667f;


                    //TESTARE
                    movementBasedOffset /= 2;

                    


                    if (!move.hitboxes[i].customSizeGhostHitbox && move.hitboxes[i].compositeCollider != null)
                    {
                        
                        move.hitboxes[i].ghostHitbox.size = new Vector2(move.hitboxes[i].compositeCollider.size.x + 2*movementBasedOffset,
                            move.hitboxes[i].compositeCollider.size.y + 0f);

                        
                        //move.hitboxes[i].ghostHitbox.offset = move.hitboxes[i].compositeCollider.offset;
                        move.hitboxes[i].ghostHitbox.offset = new Vector2(move.hitboxes[i].compositeCollider.offset.x + movementBasedOffset, 0);

                    }
                    move.hitboxes[i].ghostHitbox.gameObject.SetActive(false);
                    move.hitboxes[i].compositeCollider.gameObject.SetActive(false);
                    
                }
            }
        }
        if (firstActualHitboxIndex == -1)
            firstActualHitboxIndex = 0;
    }

    private void InitializeFrameData(FrameData move)
    {
        firstActualHitboxIndex = -1;

        
        InitializeGhostHitbox(move);
        
        move.moveScript = this;

        for (int i = 0; i < move.hitProperties.Length; i++)
        {
            if (move.hitProperties[i].hitProperties.minimumUntechableHitstunWindow == 0)
                move.hitProperties[i].hitProperties.minimumUntechableHitstunWindow = move.hitProperties[i].hitProperties.hitstun;
        }

        //if (move.multipleHitsFrameData.Length > 0)
        {
            move.MemorizeInitialFrameData();
        }

        if (move.hitProperties.Length > 1)
        {
            for (int i = move.hitProperties.Length - 1; i >= 1; i--)
            {
                if (move.hitProperties[i].multiHitting)
                {
                    lastHitIndex = i;
                    break;
                }

            }
        }
        else
        {
            lastHitIndex = 0;
        }

        moveModifier = GetComponent<FDModifier>();
        /*
        OnHitProperties[] tempProperties = GetComponentsInChildren<OnHitProperties>();
        print("properties: " + gameObject);

        for (int i = 0; i < tempProperties.Length; i++)
        {
            //print("properties: " + tempProperties[i].gameObject);
            tempProperties[i].move = this;
            tempProperties[i].Initialize();

            if (tempProperties[i] is ApplyDebuff)
            {
                applyDebuffs.Add((ApplyDebuff)tempProperties[i]);
            }
            else
            {
                onHitProperties.Add(tempProperties[i]);
            }
        }


        for (int i = 0; i < onHitProperties.Count; i++)
        {
            onHitProperties[i].move = this;
        }
        */
        if (this is Grab)
        {
            move.grab = (Grab) this;
        }


        
        //move.customHitstopBehaviour = GetComponentInChildren<HitstopBehaviour>();
        //hitboxes = new BoxCollider2D[move.hitboxes.Length];
        //compositeHitboxes = new CompositeCollider2D[move.hitboxes.Length];

        if (move.hitboxes.Length > 0)
            totalFrames = move.hitboxes[lastActualHitboxIndex].startup + move.recovery;
        else
        {
            totalFrames = move.recovery;
        }
            if (move.projectiles.Length > 0)
            {
                totalFrames = move.projectiles[move.projectiles.Length - 1].startup + move.recovery;
                for (int i = 0; i < move.projectiles.Length; i++)
                {
                    if (move.projectiles[i].proj != null)
                    {
                    //print("initproj");
                    //Projectile newProj = move.projectiles[i].proj.GetComponent<Projectile>();
                    //move.projectiles[i].projScript = newProj;
                    //move.projectiles[i].proj.GetComponent<Projectile>().InitializeProjectile();
                    move.projectiles[i].proj.InitializeProjectile();

                    //newProj.startingPosition = move.projectiles[i].projectileOrigin.position;
                    }
                }
            }
        

        move.faction = status.faction;
        move.owner = controller;



        if (moveModifier != null)
        {
            moveModifier.LoadFrameData();
        }

        for (int i = 0; i < move.softArmorWindows.Length; i++)
        {
            if (move.softArmorWindows[i].hitboxIndex < 0)
                move.softArmorWindows[i].hitboxIndex = 0;

            if (move.softArmorWindows[i].hitboxIndex >= move.hitboxes.Length)
            {
                move.softArmorWindows[i].hitboxIndex = move.hitboxes.Length - 1;
            }

            if (move.softArmorWindows[i].offset < 0)
            {
                move.softArmorWindows[i].offset = -move.softArmorWindows[i].offset;
            }


        }
    }


	public void LoadFrameData () {

		move = GetComponent<FrameData> ();


		//moveset= GetComponentInParent<MoveSet> ();
		if (move != null) {
            for (int i = 0; i < move.alternativeHitboxes.Length; i++)
            {
                InitializeFrameData(move.alternativeHitboxes[i]);
            }
            InitializeFrameData(move);
           
		}


			
			
		//hitbox = GetComponent<BoxCollider2D> ();

		/*
		startup = move.startup;
		active = move.active;
		recovery = move.recovery;
		midFrames = move.midFrames;
		startupVelocity = move.startupVelocity;
		activeVelocity = move.activeVelocity;
		recoveryVelocity = move.recoveryVelocity;
		totalFrames = startup + recovery;
		staminaCost = move.staminaCost;
		//hitbox.offset = new Vector2 (move.hitboxOffsetX, move.hitboxOffsetY);
		//hitbox.size = new Vector2 (move.hitboxSizeX, move.hitboxSizeY);
		*/
	}

}



