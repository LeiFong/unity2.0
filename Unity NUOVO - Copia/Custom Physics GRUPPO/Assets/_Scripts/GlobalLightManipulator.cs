﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalLightManipulator : Event
{
    [Space(20)]
    public float newIntensity = 0.4f;
    public int inTime = 60, outTime = 60;
    private GlobalLightController lightController;

    // Start is called before the first frame update
    protected override void InitializeEvent()
    {        
        lightController = GlobalLightController.Instance ?? FindObjectOfType<GlobalLightController>();
    }

    protected override void OnPlayerIsInside()
    {
        lightController.UpdateIntensity(newIntensity, inTime);
    }
    protected override void OnPlayerIsOutside()
    {
        lightController.ResetIntensityToDefaultValue(outTime);
    }
}
