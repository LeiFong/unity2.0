﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Experimental.Rendering.Universal;

public class HitsparkBehavior : MonoBehaviour {


    private Transform startingParent;
    private bool isDisplaying = false;
    public float gravity = 0.01f;

    private int timeToLive;
    private Light2D light;
    private float lightInitialIntensity;
    private Vector2 startingPosition, startingScale;
    private SpriteRenderer sprite;

    private Vector2 velocity;
    private int timer;
    private float decreaseRateX, decreaseRateY;

	// Use this for initialization
	void Start () {
        light = GetComponent<Light2D>();
        if (light != null)
        {
            lightInitialIntensity = light.intensity;
            light.enabled = false;
        }
        sprite = GetComponent<SpriteRenderer>();
        
        startingParent = transform.parent;
        startingPosition = transform.localPosition;
        startingScale = transform.localScale;

        StopDisplaying();

            
	}
	
	// Update is called once per frame
	void TESTFixedUpdate () {
        if (isDisplaying) {
            //print("spark");
            transform.position = new Vector2(transform.position.x + velocity.x, transform.position.y + velocity.y);
            transform.eulerAngles = new Vector3(0f, 0f, Mathf.Rad2Deg * Mathf.Atan2(velocity.y, velocity.x));
            transform.localScale = new Vector3(transform.localScale.x * decreaseRateX,
                transform.localScale.y * decreaseRateY, transform.localScale.z);
            velocity = new Vector2(velocity.x, velocity.y - gravity);

            if (light != null) {
                light.intensity -= lightInitialIntensity / timeToLive;
            }

            timer--;
            if (timer <= 0) {
                StopDisplaying();
            }
            
            
        }
    }

    public void DisplaySparks(Vector2 speed, int timeToLive) {

        StopDisplaying();

        this.timeToLive = timeToLive;

        if (light != null)
        {
            light.enabled = true;
            light.intensity = lightInitialIntensity;
        }
        if (sprite != null)
            sprite.enabled = true;     

        velocity = speed;
        timer = timeToLive;
        decreaseRateX = (1f - 0.02f * 100f / (float)timeToLive);
        decreaseRateY = (1f - 0.01f * 100f / (float)timeToLive);
        transform.parent = null;
        transform.localScale = startingScale;
        transform.localEulerAngles = new Vector3(0, 0, Mathf.Rad2Deg * Mathf.Atan2(velocity.y, velocity.x));
        isDisplaying = true;


    }

    public void StopDisplaying() {
        isDisplaying = false;
        if (light != null)
            light.enabled = false;
        if (sprite != null)
            sprite.enabled = false;
        
        transform.parent = startingParent;
        transform.localPosition = startingPosition;
        transform.localScale = startingScale;
        transform.localEulerAngles = new Vector3(0, 0, 0);
    }


    public bool IsDisplaying() {
        return isDisplaying;
    }
}
