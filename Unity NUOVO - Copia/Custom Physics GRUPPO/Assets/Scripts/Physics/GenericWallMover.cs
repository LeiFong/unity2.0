﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenericWallMover : MonoBehaviour
{

    public int Timer, MaxTimer = 30, TimePhase = 0, direction = 1;
    public float hspeed = 0.025f, vspeed = 0.025f;
    public bool vMove = true, hMove = false;
    public Vector2 PrevPosition, deltaMovement;
    private Rigidbody2D rb;
    public ContactFilter2D movableCF; //contact filter che contiene i layer degli oggetti muovibili dalla piattaforma
    Collider2D myCollider;
    //public List<Collider2D> publicL = new List<Collider2D>();
    public float shellRadius = 0.15f;
    public Vector2 totVelocity;
    //public List <>

    //public string WallLayer = "Walls";
    //public LayerMask MovableObjects;
    //private ContactFilter2D CF;

    public bool Debugging = false;

    // Use this for initialization
    void Start()
    {
        movableCF.SetLayerMask((Physics2D.GetLayerCollisionMask(LayerMask.NameToLayer("MovingPlatform"))));
        movableCF.useLayerMask = true;
        movableCF.useTriggers = true;

        Timer = 0;
        Timer += TimePhase;


        rb = GetComponent<Rigidbody2D>();
        myCollider = GetComponent<Collider2D>();


    }

    void Update()
    {
        if (!TimeVariables.canExecuteLogic)
            return;
        if (Time.timeScale < 1)
            Move();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (TimeVariables.canExecuteLogic)
          //  return;
            Timer++;
        if (Timer >= MaxTimer)
        {
            Timer = 0;
            direction *= -1;
            //print("cambioooooooooooooooooooooooooo");
        }

        if (Time.timeScale == 1)
            Move();
    }

    void Move()
    {

        //print("MOVING");

        PrevPosition = new Vector2(transform.position.x, transform.position.y);
        totVelocity = Vector2.zero;

        if (vMove)
        {
            //arrotonda
            vspeed = Mathf.Round(vspeed * 100f) / 100f;
            totVelocity.y = vspeed * direction * Time.timeScale / TimeVariables.currentTimeScale;
            //gameObject.transform.position += new Vector3(0, vspeed * direction * Time.fixedDeltaTime, 0);
        }
        
        if (hMove)
        {
            //arrotonda
            hspeed = Mathf.Round(hspeed * 100f) / 100f;
            totVelocity.x = hspeed * direction * Time.timeScale / TimeVariables.currentTimeScale;
            //gameObject.transform.position += new Vector3(hspeed * direction * Time.fixedDeltaTime, 0, 0);
        }
        //print("mp: " + totVelocity.y);

        //print("sono platform: " + Time.frameCount);
        gameObject.transform.Translate(totVelocity * (Time.timeScale == 1 ? Time.fixedDeltaTime : Time.deltaTime));
        deltaMovement = new Vector2(transform.position.x - PrevPosition.x, transform.position.y - PrevPosition.y);
        //print("deltamov: " + deltaMovement + " prev: " + PrevPosition + " position: " + transform.position);
        //MovePassenger();
        //transform.Translate(deltaMovement * Time.fixedDeltaTime);

    }

    void MovePassenger()
    {
/*
        //Guarda sopra se ci sono personaggi
        RaycastHit2D[] collisions = new RaycastHit2D[16];
        int dim = rb.Cast(new Vector2(0,1), movableCF, collisions, 5f);

        List<Collider2D> l = new List<Collider2D>();

        for (int i = 0; i < dim; i++)
        {
            //if (collisions[i].rigidbody.gameObject.layer == LayerMask.NameToLayer("Character"))
            //print("dentro: " + LayerMask.LayerToName(collisions[i].collider.gameObject.layer));
            CustomPhysics physics = collisions[i].collider.gameObject.GetComponentInParent<CustomPhysics>();
            if (physics != null && (physics.objectUnderMe == myCollider || physics.climbedPlatform == myCollider))
            {
                l.Add(collisions[i].collider);
                //print("ADD");
            }
        }

        for (int i = 0; i < publicL.Count; i++)
        {
            //print("LLL");
            if (!l.Contains(publicL[i]))
            {
                l.Add(publicL[i]);
            }
        }

      

        //sposta i giocatori
        dim = l.Count;
        //print(dim);


        for (int i = 0; i < dim; i++)
        {

            if (Debugging)
            {
                BoxCollider2D hit = (BoxCollider2D)l[i];
                float h = hit.bounds.extents.y; //metà dell'altezza del collider
                float hitY = hit.bounds.center.y - h; //y del bordo inferiore del collider
                print("About to move the player: his y=" + hitY + ", about to move him by deltaMovement: " + deltaMovement.y);
            }


            Collider2D playerCollider = l[i];
            RaycastHit2D[] arr = new RaycastHit2D[16];
            Vector2 direction = new Vector2(deltaMovement.x, deltaMovement.y);


            
            //CAST ORIZZONTALE
            float minDistance = Mathf.Abs(deltaMovement.x) + shellRadius;
            print("mind1: " + minDistance);

            int k = 0;
            if (deltaMovement.x != 0)
            {
                k = playerCollider.Cast(Mathf.Sign(deltaMovement.x) * Vector2.right, arr, minDistance, true);

                for (int j = 0; j < k; j++)
                {
                    //if (deltaMovement.x > 0)
                    //print("k: " + k + " " + arr[j].collider.gameObject);
                    if (arr[j].collider.gameObject.layer == LayerMask.NameToLayer("Default") && arr[j].collider.gameObject != this.gameObject && arr[j].distance < minDistance)
                    {   //numOfWalls++;
                        //print("helloooo");

                        minDistance = arr[j].distance;
                        //foundWall = true;
                        //print("FOUNDWLL");
                        //if (minDistance >= 0.5f)
                        //minDistance -= 0.5f;
                        //if (minDistance < 0)
                        //  minDistance = 0;

                    }
                }
                print("mind2: " + minDistance);
                direction.x = Mathf.Sign(deltaMovement.x) * (minDistance - shellRadius);
                print("dirx: " + direction.x);
                l[i].transform.parent.transform.position += (new Vector3(direction.x, 0));
            }

            //CAST VERTICALE
            minDistance = Mathf.Abs(deltaMovement.y) + shellRadius;
            k = playerCollider.Cast(Mathf.Sign(deltaMovement.y) * Vector2.up, arr, minDistance, true);
            for (int j = 0; j < k; j++)
            {
                //if (deltaMovement.x > 0)
                //print("k: " + k + " " + arr[j].collider.gameObject);
                if (arr[j].collider.gameObject.layer == LayerMask.NameToLayer("Default") && arr[j].collider.gameObject != this.gameObject && arr[j].distance < minDistance)
                {   //numOfWalls++;
                    //print("helloooo");

                    minDistance = arr[j].distance;
                    //foundWall = true;
                    //print("FOUNDWLL");
                    //if (minDistance >= 0.5f)
                    //minDistance -= 0.5f;
                    //if (minDistance < 0)
                    //  minDistance = 0;

                }
            }
            if (deltaMovement.y < 0)
                direction.y = - (minDistance - shellRadius);
            else
                direction.y = deltaMovement.y;
            l[i].transform.parent.transform.position += (new Vector3(0, direction.y));

            //print("direzione: " + direction + " mindist: " + minDistance + "deltam: " + deltaMovement);

            //l[i].collider.gameObject.GetComponentInParent<Transform>().position += new Vector3(direction.x, direction.y);
            //l[i].collider.transform.parent.gameObject.transform.Translate(new Vector3(direction.x, direction.y));
            //if (minDistance > 0)


            if (Debugging)
            {
                float myH = GetComponent<BoxCollider2D>().bounds.extents.y;
                float myY = GetComponent<BoxCollider2D>().bounds.center.y + myH;
                print("Moving player: myY = " + myY);
                BoxCollider2D hit = (BoxCollider2D)l[i];
                float h = hit.bounds.extents.y; //metà dell'altezza del collider
                float hitY = hit.bounds.center.y - h; //y del bordo inferiore del collider
                print("...moved player to " + hitY);
            }

        }

        //publicL.Clear();
*/
    }

}
