﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPreAwake
{
    bool PreAwake(int iteration, BackgroundAwaker backgroundAwaker);
}
