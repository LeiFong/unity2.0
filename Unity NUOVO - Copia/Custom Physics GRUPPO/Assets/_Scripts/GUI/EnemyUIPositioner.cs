﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUIPositioner : WorldToCanvasPositioner
{
    [Tooltip("The RectTransform need to have pivot = (0.5, 0.5), anchorMin (0.5, 0.5) and anchorMax (0.5, 0.5). " +
        "DO NOT SET Object To Follow, it's useless, it always takes the transform of the parent.")]
    public bool mouseOverForTutorial;
    private GameObject UIAnchor;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();

        objectToFollow = transform.parent;
        EnemyUIElements c = FindObjectOfType<EnemyUIElements>();
        UIAnchor = c.gameObject;
        transform.SetParent(UIAnchor.transform);
        transform.localScale = new Vector3(1, 1, 1);
        transform.localPosition = new Vector3(0, 0, 0);
        
    }


    private void Update()
    {
        if (Time.timeScale != 1)
        {
            SetPosition();
            
        }
    }
    
    // Update is called once per frame
    void FixedUpdate()
    {
        if (Time.timeScale == 1)
        {
            SetPosition();
            
        }
    }

    public void SetFollowedObject(Transform transform) {
        
        objectToFollow = transform;
    }

    public Transform GetFollowedObject() {
        return objectToFollow;
    }
}
