﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SectorEvent : Event
{
    public int sectorNumber;
    public SectorEvent connectedSectors;

    public override void TriggerEvent()
    {
        playersManager.gameManager.currentSector = sectorNumber;
    }
}
