﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MaterialItem : Item
{
    public enum Subcategory
    {
        red, blue, yellow, green
    }

    public Subcategory subcategory;

    public int subCategoryIndex;
    
}
