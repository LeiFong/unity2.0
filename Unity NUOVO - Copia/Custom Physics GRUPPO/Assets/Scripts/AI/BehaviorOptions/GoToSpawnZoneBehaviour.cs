﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToSpawnZoneBehaviour : BehaviorOption
{

    public override void MyExecuteBehaviour()
    {
        //bSelector.aiMovements.MoveToReachDestination(bSelector.aiMovements.mySpawnPosition.transform.position, 0.3f);
        if (bSelector.aiMovements.currentCompetenceZone != null)
            bSelector.aiMovements.MoveToReachSpawnZone(bSelector.aiMovements.currentCompetenceZone.transform.position, chaseSpeed);

    }
}
