﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCalculator : MonoBehaviour
{
    Controller2D controller;
    Status status;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<Controller2D>();
        status = GetComponent<Status>();
    }

    public int CalculateDamageTaken (int rawDamage)
    {
        
        int damageDone = rawDamage;

        damageDone -= (status.defensePercentage * damageDone);

        return damageDone;
    }

   
}
