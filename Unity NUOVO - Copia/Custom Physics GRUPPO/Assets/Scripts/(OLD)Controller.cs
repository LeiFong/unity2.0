﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Controller : MyPhysics {

	public string horizontalmovementCMD, atk1CMD, atk2CMD, jumpCMD, dashCMD, kickCMD, targetCMD, verticalmovementCMD;

	public AnimationUtilities animUtils;
	public string animParameter;
	public Animator animator;
	public MoveScript currentMove;
	public float walkspeed = 3;
	public float jumpTakeOffSpeed = 5;

	protected Vector2 move = Vector2.zero;
	//public Status status;
	//public GameObject weapon;
	//public MoveSet move;

	//protected int attack;
	public MoveScript landing, wakeup;
	public HurtboxHit hurtboxhit;

	public MoveSet moveset, animationset;
	public MoveScript [] moves, animations;
	public int moveIndex;
	public int seq = 0;
	protected int totalFrames;
	protected bool startupPhase;
	protected bool activePhase;
	protected bool recoveryPhase;

	public Transform groundCheck;

	//bool grounded = false;



	public bool facingRight = true;

	//public Status status;

	private int startup;
	private int active;
	private int recovery;
	private float startupVelocity;
	private float activeVelocity;
	private float recoveryVelocity;



	//public bool attack, attack2, kick, block;

	public delegate void DeathEvent (GameObject obj);
	public static event DeathEvent death;


	protected override void MyOnEnable () {
		LoadWeapon ();
		LoadMoves ();
		animUtils = transform.Find ("Skeleton").GetComponent<AnimationUtilities>();


	}

	// Use this for initialization

	/*
	void Start () {


		move = transform.Find("Weapon").GetComponent<Move> ();
		LoadWeapon ();
	}
*/

		//Debug.Log (targetVelocity);

	protected override void ComputeVelocity ()
	{
		//grounded = (Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround) && velocity.y <= 0);
		/*
		int count = RB.Cast (Vector2.down, groundContactFilter, groundHitBuffer, shellRadius);
		if (count != 0)
			grounded = true;
		else
			grounded = false;
*/
		if (grounded) {
			status.myPhysicsStatus = Status.physicsStatus.grounded;


		}
		else
			status.myPhysicsStatus = Status.physicsStatus.airborne;

		if (status.myPhysicsStatus == Status.physicsStatus.airborne) {
			animator.SetBool ("airbourne", true);
			animator.SetBool ("neutral", false);
		}
		else 
			animator.SetBool ("airbourne", false);

		if (status.currentHealth <= 0 && !status.dead) {
			status.dead = true;
			death (gameObject);
			Death ();
		}



		//Debug.Log (velocity.y);

		if (hurtboxhit.gotHit) {


			StartNewMove (hurtboxhit);
			hurtboxhit.gotHit = false;
		}

		if (status.myPhysicsStatus == Status.physicsStatus.grounded && status.myPreviousPhysicsStatus == Status.physicsStatus.airborne) {
			if (status.mystatus != Status.status.juggle)
				StartNewMove (animations[0]);
			else
				StartNewMove (animations[1]);
		}

		if (status.myPhysicsStatus == Status.physicsStatus.airborne && status.myPreviousPhysicsStatus == Status.physicsStatus.grounded &&
			status.mystatus != Status.status.juggle) {
			if (currentMove != null)
				currentMove.StopSequence ();
		}

		animator.SetFloat ("vspeed", velocity.y);


			//Debug.Log("land");
		//le seguenti funzioni NON implementano le mosse, ma specificano QUANDO eseguirle; nel caso di un giocatore, quando premi il pulsante

		//target
		Target();

		//custom script, per comportamenti non compresi tra le altre funzioni
		Custom();

		//salto
		Jump();

		//attacco1
		Attack();

		//moveset.conditions.CheckConditions();

		//Turn off target
		TurnOffTarget();

		//blocco
		Block();

		//scatto
		Dash();

	

		if (currentMove != null)
		currentMove.MyExecuteSequence ();



		//movimento 	orizzontale
		HorizontalMovement();

		//Debug.Log("FLIP?");
	
		//Debug.Log (seq);

		status.myPreviousStatus = status.mystatus;
		status.myPreviousPhysicsStatus = status.myPhysicsStatus;

		Debug.Log (animator.parameters);

	}
	
	public void Flip() {
		
		facingRight = !facingRight;
		Vector3 myscale = gameObject.transform.localScale;
		myscale.x *= -1;
		gameObject.transform.localScale = myscale;
	}

	protected virtual void HorizontalMovement () {

	}

	protected virtual void Jump () {

	}

	protected virtual void Attack () {

	}
	/*
	protected void HurtboxCheck () {
		StartNewMove (hurtboxhit);
	}
*/
	public void StartNewMove (MoveScript newMove) {
		

		/*
			else {
				if (status.myPhysicsStatus == fd.physicsConditions)
					for (int i = 0; i < fd.conditions.Length; i++) {
						if (status.mystatus == fd.conditions [i]) {

					
							moveUsable = true;
							break;
					
						} else
							continue;
					}
			}
*/

				if (currentMove != null)
					currentMove.StopSequence ();
		animator.SetBool ("neutral", true);
				currentMove = newMove;
				seq = 0;
				return;
			
		}

	public void LoadWeapon () {
		GameObject weapon;
		Component[] comp = GetComponentsInChildren<Transform>();

		int j;
		for (j = 0; j < comp.Length; j++) {
			if (comp[j].name.StartsWith("Weapon"))
				break;
			else continue;
		}
		weapon = comp [j].gameObject;
		Debug.Log (weapon);
		//GameObject mymoves = transform.Find ("Moves").gameObject;
		//moves = weapon.GetComponent<MoveSet> ().moves.Concat(mymoves.GetComponent<MoveSet>().moves).ToArray();
		moves = weapon.GetComponent<MoveSet> ().moves;
		Debug.Log (moves);

		moveset = weapon.GetComponent<MoveSet> ();
		Debug.Log (moveset);
	 
	}

	public void LoadMoves () {
		GameObject mymoves = transform.Find ("Moves").gameObject;
		//GameObject mymoves = transform.Find ("Moves").gameObject;
		//moves = weapon.GetComponent<MoveSet> ().moves.Concat(mymoves.GetComponent<MoveSet>().moves).ToArray();

		animations = mymoves.GetComponent<MoveSet> ().moves;
	}

	public void ChangeParameter (string param, bool value) {
		animParameter = param;
		animator.SetBool (animParameter, value);
	}

	protected virtual void Attack2 () {

	}

	protected virtual void Kick () {

	}

	protected virtual void Block () {

	}

	protected virtual void Target () {

	}

	protected virtual void TurnOffTarget () {

	}

	protected virtual void Custom () {

	}

	protected virtual void Dash () {

	}

	protected virtual void Death () {

	}

/*
	void OnDisable () {
		disable (gameObject);
	}
*/
	//aggiungere qui sotto eventuali altre mosse




	}




	/*
	// Update is called once per frame
	protected override void Attack ()
	{

		totalFrames = startup + active + recovery;
		if (attack)
		{
			if (seq <= startup) {
				startupPhase = true;
				activePhase = false;
				recoveryPhase = false;
				targetVelocity.x = startupVelocity;
				//Debug.Log ("start");
			} else if (seq < totalFrames - recovery) {
				startupPhase = false;
				activePhase = true;
				recoveryPhase = false;
				targetVelocity.x = activeVelocity;
				//Debug.Log ("active");
			} else {
				startupPhase = false;
				activePhase = false;
				recoveryPhase = true;
				targetVelocity.x = recoveryVelocity;
				//Debug.Log("recovery");
			}

			Debug.Log (seq);
			//Debug.Log (attack);

			//Debug.Log (targetVelocity);
			seq++;
			if (seq == totalFrames) {
				//targetVelocity.x = 0;
				seq = 0;
				attack = false;
			}

		}
	}

	*/

	/*
	void LoadWeapon () {
		

		startup = move.startup;
		active = move.active;
		recovery = move.recovery;
		startupVelocity = move.startupVelocity;
		activeVelocity = move.activeVelocity;
		recoveryVelocity = move.recoveryVelocity;
			
	}
*/


