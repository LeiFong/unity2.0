// GENERATED AUTOMATICALLY FROM 'Assets/Input/Player/PlayerControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControls"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""9bc3adf6-948d-4a12-bcaa-d25a7cfab0f1"",
            ""actions"": [
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""8ad5597c-4b5e-464c-a28b-5795426586d3"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump Charged"",
                    ""type"": ""Button"",
                    ""id"": ""67ef77e7-30b2-46da-af05-11fb423a24be"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Attack1"",
                    ""type"": ""Button"",
                    ""id"": ""1539d6c4-e97a-449f-a4bd-7d1c2f86e825"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""7799a84e-09f2-4ecf-8156-cd02fa29722d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Parry"",
                    ""type"": ""Button"",
                    ""id"": ""af3cc5cf-b41d-47cb-bbec-e7f556ecf6e9"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Run"",
                    ""type"": ""Button"",
                    ""id"": ""7adfcf18-1a53-44fd-b0ec-4894b3086349"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Target"",
                    ""type"": ""Button"",
                    ""id"": ""cefc3c9f-f898-40ac-ad65-033b6b40ec33"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Special1"",
                    ""type"": ""Button"",
                    ""id"": ""74ec4a10-7b8e-493f-b75c-c3372d9cb5b1"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Special2"",
                    ""type"": ""Button"",
                    ""id"": ""e4d5d749-695d-4b88-9693-62ea3d4ae608"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Attack2"",
                    ""type"": ""Button"",
                    ""id"": ""796fb304-5df5-49bd-88f3-ed40276420cb"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Horizontal"",
                    ""type"": ""PassThrough"",
                    ""id"": ""3b4978e9-5f25-4f0f-87c7-77843c4a7f8c"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Vertical"",
                    ""type"": ""Value"",
                    ""id"": ""6fe1131d-d9d3-4767-b549-95195d081c60"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Attack1 Charged"",
                    ""type"": ""Button"",
                    ""id"": ""f2defa08-7530-45aa-a6d1-22fdf641dfab"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Attack2 Charged"",
                    ""type"": ""Button"",
                    ""id"": ""e1640ff4-8035-486c-9bdd-b1c98420d2a0"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Block (Hold)"",
                    ""type"": ""Button"",
                    ""id"": ""b989ef04-3fd7-42d7-8d4b-a0a7655af126"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""1647e2ee-f453-45d4-875b-f85bababf72a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""b80be37d-4bbb-45b1-a246-345fd1f1d8b0"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4ed9bd75-0047-46a1-b733-05f408f79996"",
                    ""path"": ""<Gamepad>{Player1}/buttonSouth"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0304b9f6-c512-4682-b8e6-fd6a4449f4df"",
                    ""path"": ""<Gamepad>{Player2}/buttonSouth"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cbf76918-e03e-4710-a7fe-2e723f64b4f7"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Attack1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7d03add4-43b1-431a-b217-d3e88e94596c"",
                    ""path"": ""<Gamepad>{Player1}/buttonWest"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Attack1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1f92d142-22ef-48c4-82f4-daff2597428f"",
                    ""path"": ""<Gamepad>{Player2}/buttonWest"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Attack1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b1fd79f7-58c6-449f-b04e-20f9bb0239f7"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c7ed5d7d-77a2-4035-8089-99d72b0f58c6"",
                    ""path"": ""<Gamepad>{Player1}/rightShoulder"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f3131b7d-3549-47a2-8d82-243f9ed1d5d8"",
                    ""path"": ""<Gamepad>{Player2}/rightShoulder"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d38a53b1-8047-403a-b78b-58d6b7ef24ab"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Special1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7b378c1b-5d09-4eab-b284-277895baae41"",
                    ""path"": ""<Gamepad>{Player1}/rightTrigger"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Special1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7c186d5d-af8a-4061-9d65-fb06853d0b81"",
                    ""path"": ""<Gamepad>{Player2}/rightTrigger"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Special1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5425193b-3021-4cd6-b214-2937a763a239"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Attack2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6da2df8f-d4a4-48cc-b74b-a3fd106931a2"",
                    ""path"": ""<Gamepad>{Player1}/buttonNorth"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Attack2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0329be71-09a3-4d8b-9b4d-a7f47761a3ca"",
                    ""path"": ""<Gamepad>{Player2}/buttonNorth"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Attack2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Horizontal"",
                    ""id"": ""41bf4f10-b2e6-46ab-8108-37fa011bc1ac"",
                    ""path"": ""1DAxis(whichSideWins=2)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""0b2cbf94-d688-480e-9f5f-cc9f0fc3b85a"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""3afd491a-cfcc-416a-8191-bc6eaf4798e7"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Horizontal"",
                    ""id"": ""6ee3351f-5ad1-445c-8b7c-5ba044a0177f"",
                    ""path"": ""1DAxis(whichSideWins=2)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""0e852834-44c5-4ed2-9647-63e2bdaa6c21"",
                    ""path"": ""<Gamepad>{Player1}/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""3cc295be-82ce-4f74-8782-ffedf7cf74ed"",
                    ""path"": ""<Gamepad>{Player1}/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Horizontal"",
                    ""id"": ""95328d43-ab3c-42e3-822d-90b157dfb4f5"",
                    ""path"": ""1DAxis(whichSideWins=2)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""135337d1-e4ea-459f-9c26-208e21d69d85"",
                    ""path"": ""<Gamepad>{Player2}/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""dc8e72eb-7e44-4590-9ccd-2f0da48c4e00"",
                    ""path"": ""<Gamepad>{Player2}/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Vertical"",
                    ""id"": ""6c10e383-e898-48d7-bc31-8dd4af6dc6a4"",
                    ""path"": ""1DAxis(whichSideWins=1)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Vertical"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""b89cfea9-55dc-4826-99b8-31311847954a"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""521f00e3-23d8-4bb4-be11-0db84f046f17"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Vertical"",
                    ""id"": ""87213a20-6794-46d5-8eab-a681c75a8e94"",
                    ""path"": ""1DAxis(whichSideWins=1)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Vertical"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""92f3775d-f558-4fe5-98a3-b8e40220b281"",
                    ""path"": ""<Gamepad>{Player1}/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""c0a7e4d2-a1d5-44c4-9997-a8ae21d1885f"",
                    ""path"": ""<Gamepad>{Player1}/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Vertical"",
                    ""id"": ""88290092-5483-4f4e-b7c0-e63a5ccef7bf"",
                    ""path"": ""1DAxis(whichSideWins=1)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Vertical"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""564ddbcc-8ea8-4b44-85c2-c9f4d6f3eb10"",
                    ""path"": ""<Gamepad>{Player2}/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""bb105dfa-2cbf-4d85-9e4c-e2c8e30963cc"",
                    ""path"": ""<Gamepad>{Player2}/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Run"",
                    ""id"": ""735644ec-9c09-44f1-858a-f4a88eff2c50"",
                    ""path"": ""1DAxis(minValue=0)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Run"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""5bfafb8f-45b7-47e8-a1c2-28c5e4fef410"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Run"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""482bc73f-fc39-4352-807c-628076a881e3"",
                    ""path"": ""<Gamepad>{Player1}/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Run"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""3a54d77f-eb58-47f8-9f3a-12102ee79eb0"",
                    ""path"": ""<Gamepad>{Player2}/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Run"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Charge"",
                    ""id"": ""6899f218-828a-4aab-a528-822a71b0d3a3"",
                    ""path"": ""1DAxis(minValue=0)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Attack2 Charged"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""896d2bfa-241f-432f-9ee6-fc68de3cdccb"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Attack2 Charged"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""b3c29cab-091f-4890-a7cc-acc1da17e0ac"",
                    ""path"": ""<Gamepad>{Player1}/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Attack2 Charged"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""65b3d10a-b8a0-4204-bb9b-3fc61ff3d367"",
                    ""path"": ""<Gamepad>{Player2}/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Attack2 Charged"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Block"",
                    ""id"": ""9aa30231-3776-4ffb-8d6e-ab88a621cb6b"",
                    ""path"": ""1DAxis(minValue=0)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Block (Hold)"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""99a40913-65a2-44a5-95b3-1b6e61bb1406"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Block (Hold)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""d4d14a2d-87dc-43f1-a124-567df0e9e2a0"",
                    ""path"": ""<Gamepad>{Player1}/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Block (Hold)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""72b06895-0a26-42ed-9f0f-e46616feec39"",
                    ""path"": ""<Gamepad>{Player2}/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Block (Hold)"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""target hold"",
                    ""id"": ""9d72a7da-54cd-4eee-86b5-baaac04c2cbe"",
                    ""path"": ""1DAxis(minValue=0,whichSideWins=2)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Target"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""e9663bcd-d421-41ac-acfa-3ee452f972fb"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Target"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""e73b6434-4051-4773-9672-c744e6fbf56c"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Target"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""9bd3c3ca-7657-4ffb-85a0-2d0acbe7a88a"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Parry"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ac4e9348-40f0-46e4-9817-ff0e1f78ffe1"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Parry"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0166dd20-2fc7-4384-9796-0e872f715c32"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Special2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""41e0a28a-0490-4aa4-bdb3-bf88917ebaea"",
                    ""path"": ""<Gamepad>{Player1}/leftTrigger"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Special2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cc23f4a0-a094-40d4-b8d7-82fe40b4ea0a"",
                    ""path"": ""<Gamepad>{Player2}/leftTrigger"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Special2"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""757ca05d-e79e-4d02-8d04-471868b0cb0b"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""def5a4ae-38fd-4082-bcdf-97a9c0c0c330"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""dc28e2f1-9ac7-4267-8ad4-6ff10ce41e5e"",
                    ""path"": ""<Gamepad>{Player1}/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""4d237be8-1804-411a-894a-b4b3e065e4ce"",
                    ""path"": ""<Gamepad>{Player2}/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Charge"",
                    ""id"": ""e4354e61-d62b-4eac-beff-afbc0bf3115d"",
                    ""path"": ""1DAxis(minValue=0)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Attack1 Charged"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""11f9a192-295c-4fe4-b1f1-4109951b2f72"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Attack1 Charged"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""63183f6f-1988-42dc-8b1d-f0ec35498792"",
                    ""path"": ""<Gamepad>{Player1}/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Attack1 Charged"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""5edc5090-1bd2-4217-a25d-484174026f30"",
                    ""path"": ""<Gamepad>{Player2}/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Attack1 Charged"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Charge"",
                    ""id"": ""90b7530c-ade6-410a-85be-98ccf6a1b344"",
                    ""path"": ""1DAxis(minValue=0)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump Charged"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""3fe56226-640d-4901-840c-586b3c4c99a6"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Jump Charged"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""04735128-1a22-426e-b96d-2e796460bafc"",
                    ""path"": ""<Gamepad>{Player1}/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamepadP1"",
                    ""action"": ""Jump Charged"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""add8303e-d420-4b86-9e2d-01a8f8262752"",
                    ""path"": ""<Gamepad>{Player1}/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""GamePadP2"",
                    ""action"": ""Jump Charged"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""GamepadP1"",
            ""bindingGroup"": ""GamepadP1"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>{Player1}"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""GamePadP2"",
            ""bindingGroup"": ""GamePadP2"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>{Player2}"",
                    ""isOptional"": true,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_Jump = m_Player.FindAction("Jump", throwIfNotFound: true);
        m_Player_JumpCharged = m_Player.FindAction("Jump Charged", throwIfNotFound: true);
        m_Player_Attack1 = m_Player.FindAction("Attack1", throwIfNotFound: true);
        m_Player_Dash = m_Player.FindAction("Dash", throwIfNotFound: true);
        m_Player_Parry = m_Player.FindAction("Parry", throwIfNotFound: true);
        m_Player_Run = m_Player.FindAction("Run", throwIfNotFound: true);
        m_Player_Target = m_Player.FindAction("Target", throwIfNotFound: true);
        m_Player_Special1 = m_Player.FindAction("Special1", throwIfNotFound: true);
        m_Player_Special2 = m_Player.FindAction("Special2", throwIfNotFound: true);
        m_Player_Attack2 = m_Player.FindAction("Attack2", throwIfNotFound: true);
        m_Player_Horizontal = m_Player.FindAction("Horizontal", throwIfNotFound: true);
        m_Player_Vertical = m_Player.FindAction("Vertical", throwIfNotFound: true);
        m_Player_Attack1Charged = m_Player.FindAction("Attack1 Charged", throwIfNotFound: true);
        m_Player_Attack2Charged = m_Player.FindAction("Attack2 Charged", throwIfNotFound: true);
        m_Player_BlockHold = m_Player.FindAction("Block (Hold)", throwIfNotFound: true);
        m_Player_Interact = m_Player.FindAction("Interact", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_Jump;
    private readonly InputAction m_Player_JumpCharged;
    private readonly InputAction m_Player_Attack1;
    private readonly InputAction m_Player_Dash;
    private readonly InputAction m_Player_Parry;
    private readonly InputAction m_Player_Run;
    private readonly InputAction m_Player_Target;
    private readonly InputAction m_Player_Special1;
    private readonly InputAction m_Player_Special2;
    private readonly InputAction m_Player_Attack2;
    private readonly InputAction m_Player_Horizontal;
    private readonly InputAction m_Player_Vertical;
    private readonly InputAction m_Player_Attack1Charged;
    private readonly InputAction m_Player_Attack2Charged;
    private readonly InputAction m_Player_BlockHold;
    private readonly InputAction m_Player_Interact;
    public struct PlayerActions
    {
        private @PlayerControls m_Wrapper;
        public PlayerActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Jump => m_Wrapper.m_Player_Jump;
        public InputAction @JumpCharged => m_Wrapper.m_Player_JumpCharged;
        public InputAction @Attack1 => m_Wrapper.m_Player_Attack1;
        public InputAction @Dash => m_Wrapper.m_Player_Dash;
        public InputAction @Parry => m_Wrapper.m_Player_Parry;
        public InputAction @Run => m_Wrapper.m_Player_Run;
        public InputAction @Target => m_Wrapper.m_Player_Target;
        public InputAction @Special1 => m_Wrapper.m_Player_Special1;
        public InputAction @Special2 => m_Wrapper.m_Player_Special2;
        public InputAction @Attack2 => m_Wrapper.m_Player_Attack2;
        public InputAction @Horizontal => m_Wrapper.m_Player_Horizontal;
        public InputAction @Vertical => m_Wrapper.m_Player_Vertical;
        public InputAction @Attack1Charged => m_Wrapper.m_Player_Attack1Charged;
        public InputAction @Attack2Charged => m_Wrapper.m_Player_Attack2Charged;
        public InputAction @BlockHold => m_Wrapper.m_Player_BlockHold;
        public InputAction @Interact => m_Wrapper.m_Player_Interact;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @Jump.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @JumpCharged.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJumpCharged;
                @JumpCharged.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJumpCharged;
                @JumpCharged.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJumpCharged;
                @Attack1.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack1;
                @Attack1.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack1;
                @Attack1.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack1;
                @Dash.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                @Parry.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnParry;
                @Parry.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnParry;
                @Parry.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnParry;
                @Run.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRun;
                @Run.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRun;
                @Run.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRun;
                @Target.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnTarget;
                @Target.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnTarget;
                @Target.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnTarget;
                @Special1.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpecial1;
                @Special1.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpecial1;
                @Special1.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpecial1;
                @Special2.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpecial2;
                @Special2.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpecial2;
                @Special2.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSpecial2;
                @Attack2.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack2;
                @Attack2.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack2;
                @Attack2.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack2;
                @Horizontal.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHorizontal;
                @Horizontal.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHorizontal;
                @Horizontal.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHorizontal;
                @Vertical.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnVertical;
                @Vertical.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnVertical;
                @Vertical.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnVertical;
                @Attack1Charged.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack1Charged;
                @Attack1Charged.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack1Charged;
                @Attack1Charged.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack1Charged;
                @Attack2Charged.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack2Charged;
                @Attack2Charged.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack2Charged;
                @Attack2Charged.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack2Charged;
                @BlockHold.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnBlockHold;
                @BlockHold.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnBlockHold;
                @BlockHold.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnBlockHold;
                @Interact.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnInteract;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @JumpCharged.started += instance.OnJumpCharged;
                @JumpCharged.performed += instance.OnJumpCharged;
                @JumpCharged.canceled += instance.OnJumpCharged;
                @Attack1.started += instance.OnAttack1;
                @Attack1.performed += instance.OnAttack1;
                @Attack1.canceled += instance.OnAttack1;
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
                @Parry.started += instance.OnParry;
                @Parry.performed += instance.OnParry;
                @Parry.canceled += instance.OnParry;
                @Run.started += instance.OnRun;
                @Run.performed += instance.OnRun;
                @Run.canceled += instance.OnRun;
                @Target.started += instance.OnTarget;
                @Target.performed += instance.OnTarget;
                @Target.canceled += instance.OnTarget;
                @Special1.started += instance.OnSpecial1;
                @Special1.performed += instance.OnSpecial1;
                @Special1.canceled += instance.OnSpecial1;
                @Special2.started += instance.OnSpecial2;
                @Special2.performed += instance.OnSpecial2;
                @Special2.canceled += instance.OnSpecial2;
                @Attack2.started += instance.OnAttack2;
                @Attack2.performed += instance.OnAttack2;
                @Attack2.canceled += instance.OnAttack2;
                @Horizontal.started += instance.OnHorizontal;
                @Horizontal.performed += instance.OnHorizontal;
                @Horizontal.canceled += instance.OnHorizontal;
                @Vertical.started += instance.OnVertical;
                @Vertical.performed += instance.OnVertical;
                @Vertical.canceled += instance.OnVertical;
                @Attack1Charged.started += instance.OnAttack1Charged;
                @Attack1Charged.performed += instance.OnAttack1Charged;
                @Attack1Charged.canceled += instance.OnAttack1Charged;
                @Attack2Charged.started += instance.OnAttack2Charged;
                @Attack2Charged.performed += instance.OnAttack2Charged;
                @Attack2Charged.canceled += instance.OnAttack2Charged;
                @BlockHold.started += instance.OnBlockHold;
                @BlockHold.performed += instance.OnBlockHold;
                @BlockHold.canceled += instance.OnBlockHold;
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);
    private int m_KeyboardSchemeIndex = -1;
    public InputControlScheme KeyboardScheme
    {
        get
        {
            if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
            return asset.controlSchemes[m_KeyboardSchemeIndex];
        }
    }
    private int m_GamepadP1SchemeIndex = -1;
    public InputControlScheme GamepadP1Scheme
    {
        get
        {
            if (m_GamepadP1SchemeIndex == -1) m_GamepadP1SchemeIndex = asset.FindControlSchemeIndex("GamepadP1");
            return asset.controlSchemes[m_GamepadP1SchemeIndex];
        }
    }
    private int m_GamePadP2SchemeIndex = -1;
    public InputControlScheme GamePadP2Scheme
    {
        get
        {
            if (m_GamePadP2SchemeIndex == -1) m_GamePadP2SchemeIndex = asset.FindControlSchemeIndex("GamePadP2");
            return asset.controlSchemes[m_GamePadP2SchemeIndex];
        }
    }
    public interface IPlayerActions
    {
        void OnJump(InputAction.CallbackContext context);
        void OnJumpCharged(InputAction.CallbackContext context);
        void OnAttack1(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
        void OnParry(InputAction.CallbackContext context);
        void OnRun(InputAction.CallbackContext context);
        void OnTarget(InputAction.CallbackContext context);
        void OnSpecial1(InputAction.CallbackContext context);
        void OnSpecial2(InputAction.CallbackContext context);
        void OnAttack2(InputAction.CallbackContext context);
        void OnHorizontal(InputAction.CallbackContext context);
        void OnVertical(InputAction.CallbackContext context);
        void OnAttack1Charged(InputAction.CallbackContext context);
        void OnAttack2Charged(InputAction.CallbackContext context);
        void OnBlockHold(InputAction.CallbackContext context);
        void OnInteract(InputAction.CallbackContext context);
    }
}
