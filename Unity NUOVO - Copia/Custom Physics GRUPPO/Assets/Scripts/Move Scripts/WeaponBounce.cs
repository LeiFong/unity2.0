﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBounce : MoveScript
{
    public int duration;
    public int flushBuffersDuration = 20;

    public override void MyStartSequence()
    {
        base.MyStartSequence();
        //status.mystatus = Status.status.hitstun1;

    }
    public override void MyExecuteSequence()
    {
        base.MyExecuteSequence();
        if (controller.seq < flushBuffersDuration)
        {
            //controller.FlushBuffers();
        }

        //print("bounceeee " + controller.hitstop);
        status.mystatus = Status.status.weaponBounce;

        if (controller.seq >= duration)
            itsTimeToStopTheSequence = true;
        else
            controller.seq++;
    }

    public override void MyStopSequence()
    {
        base.MyStopSequence();
        //TimeVariables.StopWatch();
    }

    public override void SetStatus()
    {


        status.mystatus = Status.status.hitstun1;
        //status.canRecharge = false;




    }
}
