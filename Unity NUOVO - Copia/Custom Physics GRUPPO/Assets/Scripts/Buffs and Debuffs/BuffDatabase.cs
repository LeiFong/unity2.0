﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffDatabase : MonoBehaviour
{
    public Buff[] buffs;

    private void Start()
    {
        GenerateBuffID();
    }

    void GenerateBuffID ()
    {
        for (int i = 0; i < buffs.Length; i++)
        {
            buffs[i].buffScriptableObject.index = i;
        }
    }

}
