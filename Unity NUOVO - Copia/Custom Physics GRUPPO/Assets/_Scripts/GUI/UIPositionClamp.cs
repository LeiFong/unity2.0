﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPositionClamp : MonoBehaviour
{
    
    private RectTransform myRect;

    // Start is called before the first frame update
    void Start()
    {
        myRect = GetComponent<RectTransform>();
    }

    public void Clamp(RectTransform boundaries) {
        Vector3[] myCorners = new Vector3[4];
        myRect.GetWorldCorners(myCorners);
        Vector3[] boundaryCorners = new Vector3[4];
        boundaries.GetWorldCorners(boundaryCorners);

        /* Corners:     
         * 1    2
         * 0    3
        */

        float delta = myCorners[0].x - boundaryCorners[0].x;
        if (delta < 0)
        {
            myRect.position -= new Vector3(delta, 0, 0);
            myRect.GetWorldCorners(myCorners);
            //print("Moving to the right by " + (-delta));
        }


        delta = myCorners[0].y - boundaryCorners[0].y;
        if (delta < 0)
        {
            myRect.position -= new Vector3(0, delta, 0);
            myRect.GetWorldCorners(myCorners);
            //print("Moving up by " + (-delta));
        }

        delta = myCorners[1].y - boundaryCorners[1].y;
        if (delta > 0)
        {
            myRect.position -= new Vector3(0, delta, 0);
            myRect.GetWorldCorners(myCorners);
            //print("Moving down by " + (delta));
        }

        delta = myCorners[2].x - boundaryCorners[2].x;
        if (delta > 0)
        {
            myRect.position -= new Vector3(delta, 0, 0);
            //print("Moving to the left by " + (delta));
        }

        
    }

    public void MatchUpperBound(RectTransform boundaries) {
        Vector3[] myCorners = new Vector3[4];
        myRect.GetWorldCorners(myCorners);
        Vector3[] boundaryCorners = new Vector3[4];
        boundaries.GetWorldCorners(boundaryCorners);

        float delta = myCorners[1].y - boundaryCorners[1].y;

        myRect.position -= new Vector3(0, delta, 0);
    }
    
}
