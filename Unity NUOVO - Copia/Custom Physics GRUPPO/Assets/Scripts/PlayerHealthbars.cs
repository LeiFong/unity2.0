﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthbars : MonoBehaviour {

	public BarScaleController [] healthBars; 
	public BarScaleController[] staminaBars;
    public BarScaleController[] meterBars;

	public BarScaleController[] heatBars;
	public BarScaleController[] staggerBars;

	public PlayersManager myPM;
	
	public void InitializeBars (PlayersManager pm) {
		myPM = pm;
        int i, j = 0;
		if (myPM.player1 != null)
			j++;
		if (myPM.player2 != null)
			j++;
		for (i = 0; i < 2; i++) {
			if (healthBars [i].status == null) {

				//print ("players count: " + myPM.players.Count);
				//print ("CIAOOOOO");

				if (healthBars [i].relativePlayerNumber > j) {
					healthBars [i].gameObject.SetActive (false);
                    //healthBars[i].progressBar.transform.parent.gameObject.SetActive(false);
					continue;
				}
                healthBars[i].gameObject.SetActive(true);
                //healthBars[i].progressBar.transform.parent.gameObject.SetActive(true);

				if (i == 0)
				{
					if (myPM.player1 != null)
						healthBars[i].status = myPM.player1.status;
				}
				else
                {
					if (myPM.player2 != null)
						healthBars[i].status = myPM.player2.status;
				}
			}
		}
        for (i = 0; i < 2; i++)
        {
            if (meterBars[i].relativePlayerNumber > j)
            {
                //print("sono " + meterBars[i]);
                meterBars[i].gameObject.SetActive(false);
                continue;
            }
            meterBars[i].gameObject.SetActive(true);

			if (i == 0 || i == 1)
			{
				if (myPM.player1 != null)
					meterBars[i].status = myPM.player1.status;
			}
			else
			{
				if (myPM.player2 != null)
					meterBars[i].status = myPM.player2.status;
			}

			
            
        }

		for (i = 0; i < 2; i++) {
			if (staminaBars [i].status == null) {


				if (staminaBars [i].relativePlayerNumber > j) {
					staminaBars [i].gameObject.SetActive (false);
					continue;
				}
                //print ("CIAOOOOO");
                staminaBars[i].gameObject.SetActive(true);

				if (i == 0)
				{
					if (myPM.player1 != null)
					{
						staminaBars[i].status = myPM.player1.status;
						staminaBars[i].BarInitialization();
					}
				}
				else
				{
					if (myPM.player2 != null)
					{
						staminaBars[i].status = myPM.player2.status;
						staminaBars[i].BarInitialization();

					}
				}
				
			}

		}

		for (i = 0; i < 2; i++)
		{
			if (i > heatBars.Length - 1)
				continue;
			if (heatBars[i].status == null)
			{


				if (heatBars[i].relativePlayerNumber > j)
				{
					heatBars[i].gameObject.SetActive(false);
					continue;
				}
				//print ("CIAOOOOO");
				heatBars[i].gameObject.SetActive(true);

				if (i == 0)
				{
					if (myPM.player1 != null)
					{
						heatBars[i].status = myPM.player1.status;
						heatBars[i].BarInitialization();
					}
				}
				else
				{
					if (myPM.player2 != null)
					{
						heatBars[i].status = myPM.player2.status;
						heatBars[i].BarInitialization();

					}
				}

			}

		}

		for (i = 0; i < 2; i++)
		{
			if (i > staggerBars.Length - 1)
				continue;
			if (staggerBars[i].status == null)
			{

				if (staggerBars[i].relativePlayerNumber > j)
				{
					staggerBars[i].gameObject.SetActive(false);
					continue;
				}
				//print ("CIAOOOOO");
				staggerBars[i].gameObject.SetActive(true);

				if (i == 0)
				{
					if (myPM.player1 != null)
					{
						staggerBars[i].status = myPM.player1.status;
						staggerBars[i].BarInitialization();
					}
				}
				else
				{
					if (myPM.player2 != null)
					{
						staggerBars[i].status = myPM.player2.status;
						staggerBars[i].BarInitialization();

					}
				}

			}

		}

		/*
		while (i < healthBars.Length) {
			healthBars [i].gameObject.SetActive (false);
			staminaBars [i].gameObject.SetActive (false);
			i++;

		}
*/





	}
}
