﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreAwakeTester : MonoBehaviour, IPreAwake
{
    public bool PreAwake(int iteration, BackgroundAwaker backgroundAwaker)
    {
        if (iteration == 0)
        {
            print("Preawake iteration 0 at frame "+Time.frameCount);
            return false;
        }
        else if (iteration == 1)
        {
            print("Preawake iteration 1 at frame " + Time.frameCount);
            return false;
        }
        else if (iteration == 2)
        {
            print("Preawake iteration 2 at frame " + Time.frameCount);
            return false;
        }
        else if (iteration == 3)
        {
            print("Preawake iteration 3 at frame " + Time.frameCount);
        }

        return true;
    }

    // Start is called before the first frame update
    void Awake()
    {
        print("ACTUAL AWAKE");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
