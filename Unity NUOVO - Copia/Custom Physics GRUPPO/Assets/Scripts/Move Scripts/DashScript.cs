﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashScript : MoveScript {

	public int totalDashDuration = 20;

	
	/*
	void OnEnable () {
		controller = transform.parent.transform.parent.GetComponent<Controller2D> ();
		status = transform.parent.transform.parent.GetComponent<Status> ();
		LoadFrameData ();
		controller.seq = 0;
	}
*/

	public override void MyStopSequence() {
		
		controller.seq = 0;
		//Debug.Log ("STOP!");
		//status.mystatus = Status.status.neutral;
		controller.avoidingCharacters = false;
		//StopCoroutine ("Attack");
		//StartCoroutine (status.WaitBeforeRecharge());
		//controller.animator.SetBool ("dash", false);
		//controller.animator.SetBool ("neutral", true);
		//controller.animator.speed = 1;
		controller.currentMove = null;

	}

	public override void MyExecuteSequence ()
	{



		//status.canRecharge = false;
		if (controller.seq == 0) {
			//status.currentStamina -= staminaCost;
			controller.startupCounter = 0;
			//print ("DASH");

			/*
			if (controller.targetList.myTarget == null) {
				if (controller.facingRight && controller.horizontal < 0)
					controller.Flip ();
				else if (!controller.facingRight && controller.horizontal > 0)
					controller.Flip ();
			}
			*/
			//SetAnimationSpeed (move.actualAnimationFrames [0], dashDuration);
			//controller.animator.SetBool ("neutral", false);
			//controller.ChangeParameter (animatorParameterName, true);
			//controller.ChangeParameter(animatorParameterName);

		}


		if (controller.hitWhileInvincible && controller.seq >= 2 && relatedMoves.Length > 0)
        {
			//print("dodge: " + controller.seq);	
			relatedMoves[0].invincibilityWindows[0].invincibilityWindowEnd = invincibilityWindows[0].invincibilityWindowEnd - controller.seq;
			controller.StartNewMove(relatedMoves[0]);
			return;
        }

        //if (controller.attack)



        /*
			//status.mystatus = Status.status.attacking;
		if (controller.seq < totalDashDuration) {
			controller.startupCounter++;
			status.mystatus = Status.status.dashing;
			//controller.avoidsCharacters = true;
			if (controller.seq < startDashDuration) {
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * startDashVelocity;
			} else if (controller.seq < midDashDuration) {
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * midDashVelocity;
			} else {
				controller.targetVelocity.x = controller.gameObject.transform.localScale.x * recoveryDashVelocity;

			}



				//Debug.Log ("start");
				

			} 
		*/
        //print("DASHing" + status.mystatus);
        status.mystatus = Status.status.dashing;
        
		controller.seq++;	

			

		if (controller.seq >= totalDashDuration) {
				
				//StopSequence ();
			//moveIsFinished = true;
			itsTimeToStopTheSequence = true;
            


			}


		}

    public override void MyStartSequence()
    {
        if (controller.moveset != null)
        {
            if (controller.moveset.customDash != null && controller.moveset.customDash != this)
            {
                mayExecuteNow = false;
                controller.StartNewMove(controller.moveset.customDash);
            }
        }
    }


	public override void SetStatus()
	{
		status.mystatus = Status.status.dashing;
	}
}

