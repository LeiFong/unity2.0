﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public bool saveOnSceneChange = true;
    public Animator animator;
    public float minimumWaitTime = 4f;
    private CustomGameManager man;
    private AsyncOperation op;
    private float progress = 0;

    private void Start()
    {
        man = FindObjectOfType<CustomGameManager>();
        
    }

    public void LoadLevel(string name) {
        StartCoroutine(LoadAsynchronously(name));
    }
    public void LoadSceneNoInterruption(string name) {
        StartCoroutine(LoadAsynchronouslyNoInterruption(name));
    }
    public void UnloadSceneNoInterruption(string name) {
        StartCoroutine(UnloadAsynchronouslyNoInterruption(name));
    }

    IEnumerator LoadAsynchronously(string name) {
        if (animator != null)
            animator.SetTrigger("start");
        progress = 0;
        yield return new WaitForSeconds(minimumWaitTime);
        //if (saveOnSceneChange)
          //  man.SaveAll();
        op = SceneManager.LoadSceneAsync(name);
        while (!op.isDone) {
            progress = Mathf.Clamp01(op.progress / 0.9f);
            if (progress >= 0.9f && animator != null)
                animator.SetTrigger("end");

            yield return null;
        }
    }
    IEnumerator LoadAsynchronouslyNoInterruption(string name)
    {
        
        
        op = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
        while (!op.isDone)
        {
            
            yield return null;
        }
    }
    IEnumerator UnloadAsynchronouslyNoInterruption(string name)
    {


        op = SceneManager.UnloadSceneAsync(name);
        while (!op.isDone)
        {

            yield return null;
        }
    }
}
