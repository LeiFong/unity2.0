﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ScaledTimeSpeedSetter : MonoBehaviour
{
    public List<Animator> animators;
    public List<ParticleSystem> particleSystems;

    private void OnDisable()
    {
        if (Application.isPlaying)
        {
            return;
        }
        
        animators = new List<Animator>(GetComponentsInChildren<Animator>(true));
        particleSystems = new List<ParticleSystem>(GetComponentsInChildren<ParticleSystem>(true));
    }

    private void FixedUpdate()
    {
        
    }

    private void SetSpeed(float newSpeed) {
        for (int i = 0; i < animators.Count; i++) {
            animators[i].speed = newSpeed;
        }
        for (int i = 0; i < particleSystems.Count; i++) {
            ParticleSystem.MainModule main = particleSystems[i].main;
            main.simulationSpeed = newSpeed;
        }
    }
}
