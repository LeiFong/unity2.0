﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogCloudGenerator : GeneratorFromPool
{
    public int frequency = 300;
    public Vector2 speed;
    public Vector2 speedRandomizer = new Vector2(50, 0);
    public int timeToLive = 600;
    private int timer = 0;
    private Vector2 normalizedSpeed = new Vector2();
    private Vector2 rSpeed = new Vector2();
    private FogCloudBehavior fogCloud;
    
    // Update is called once per frame
    void FixedUpdate()
    {
        timer = (timer + 1) % frequency;
        
        if (timer == 0) {
            
            fogCloud = (FogCloudBehavior) Generate();

            normalizedSpeed.x = speed.x / 100f;
            normalizedSpeed.y = speed.y / 100f;
            rSpeed.x = normalizedSpeed.x + Random.Range(-speedRandomizer.x / 100 / 2, speedRandomizer.x / 100 / 2);
            rSpeed.y = normalizedSpeed.y + Random.Range(-speedRandomizer.y / 100 / 2, speedRandomizer.y / 100 / 2);

            if (fogCloud != null && fogCloud.gameObject.activeInHierarchy)
                fogCloud.StartDisplaying(rSpeed, timeToLive + (int)Random.Range(-timeToLive / 2, timeToLive / 2));
            
        }
    }
}
