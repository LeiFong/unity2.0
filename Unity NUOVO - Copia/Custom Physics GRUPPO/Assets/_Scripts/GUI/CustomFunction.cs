﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class CustomFunction : MonoBehaviour
{
    public EventTrigger.TriggerEvent customFunction;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void InvokeCustomFunction() {
        BaseEventData b = new BaseEventData(EventSystem.current);
        customFunction.Invoke(b);
    }
}
