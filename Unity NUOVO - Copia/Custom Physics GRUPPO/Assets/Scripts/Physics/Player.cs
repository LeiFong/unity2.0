﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;

public class Player : Controller2D {

    //public Collider2D ledgeGrabber;
    


    //[HideInInspector]
    public PlayerControls controls;
    public Transform ledgeCheckPosition;
	//private EnemySpecializer enemySpecializer;
	//public bool gachiBass = false; //variabile test
	public NewInputCheck inputCheck;
	public int playerNumber = 1;
	public int turnOffTargetTimer = 0;
	public int TimeToTargetTurnOff = 10;
	//bool checkingForInputs = false;
	public int timeToRunToggle = 20;
    public int minimumSprintFrames = 38;
	public int runToggleTimer = 0;
	//public bool runToggle = false;
	public float previousMoveSpeed;
    //bool previousRunToggle = false;
    //public DamageDisplayGenerator damageDisplayGenerator; 
    public bool dontTransitionToWalkAnimation;

	//public PlayersManager pm;

	public int bufferLength = 30;
	public bool holdingDash = false;
	public int framesHoldingDash = 0;
	public int runLockTimer = 120; //quando finisci la stamina mentre stai correndo, non puoi più iniziare una corsa per questo numero di frame
	public int runLockCounter = 0;
		
	public int leniency = 8; //quanto può passare da un input all'altro
    [Range(0, 1)]
    public float runAnimationRatioThreshold = 1; //da quando inizia l'animazione della corsa, e ad applicare runAnimationSpeedScaler
    public float runAnimationSpeedScaler = 1; //durante l'animazione della corsa, questo consente di regolare la velocità dell'animatore (per esempio se è strano che corra troppo veloce)
    public float dirLockWalkAnimationSpeedScaler = 1; //per quando è in direction lock, se si vuole cambiare la vel dell'animazione
    public float dirLockRunAnimationSpeedScaler = 1; 


    public bool debugGottaGoFast;
    [Range(-1, 1)]
    public float debugHorizontal;
    public bool debugDirLock;
	[System.Serializable]
	public struct inputCombination {
		public int counter;
		public int inputSequenceCounter;
		public bool inputSequenceCounterIncrease;
	}

	//public int jumpLeniency = 5; //se sei in stato aereo da questi frames e non perchè hai saltato, allora è ancora possibile eseguire un salto

	//public inputCombination[] inputCombinations = new inputCombination[2];

	protected override void InputCombinationCheck () {
		/*
		for (int i = 0; i < inputCombinations.Length; i++) {
			if (inputCombinations [i].counter == 0) {
				inputCombinations [i].inputSequenceCounter = 0;
				inputCombinations [i].counter = leniency;
			}
		}



		if (inputCombinations [0].inputSequenceCounter == 0 && leftpressed) {
			inputCombinations [0].inputSequenceCounterIncrease = true;
			inputCombinations [0].counter = leniency;
			//print ("A");
		}

		if (inputCombinations [0].inputSequenceCounter == 1 && leftreleased) {
			inputCombinations [0].inputSequenceCounterIncrease = true;
			inputCombinations [0].counter = leniency;
			//print ("B");

		}

		if (inputCombinations [0].inputSequenceCounter == 2 && leftpressed) {
			inputCombinations [0].inputSequenceCounterIncrease = true;
			inputCombinations [0].counter = leniency;
			//print ("C");
			//runToggle = true;
			//runBuffer = true;
		}

	



		//print (inputCombinations.Length);


		if (inputCombinations [1].inputSequenceCounter == 0 && rightpressed) {
			inputCombinations [1].inputSequenceCounterIncrease = true;
			inputCombinations [1].counter = leniency;
			//print ("A");
		}

		if (inputCombinations [1].inputSequenceCounter == 1 && rightreleased) {
			inputCombinations [1].inputSequenceCounterIncrease = true;
			inputCombinations [1].counter = leniency;
			//print ("B");

		}

		if (inputCombinations [1].inputSequenceCounter == 2 && rightpressed) {
			inputCombinations [1].inputSequenceCounterIncrease = true;
			inputCombinations [1].counter = leniency;
			//print ("C");
			//if (status.myPhysicsStatus == Status.physicsStatus.grounded) runToggle = true;

		}

        */

		if (facingRight && rightreleased && previousRunToggle) {
			runToggle = false;
			//StartNewMove (animations [12]);
		}
		if (!facingRight && leftreleased && previousRunToggle) {
			runToggle = false;
			//StartNewMove (animations [12]);

		}

        /*

		for (int i = 0; i < inputCombinations.Length; i++) {
			if (inputCombinations [i].inputSequenceCounter > 0)
				inputCombinations [i].counter--;
			if (inputCombinations [i].inputSequenceCounterIncrease == true) {
				inputCombinations [i].inputSequenceCounter++;
				inputCombinations [i].inputSequenceCounterIncrease = false;
			}
		}
       */

	}



    public override void MyOnEnable()
    {
        //LoadInfos();


        if (status != null)
        {
            if (status.currentHealth <= 0)
            {
                StartNewMove(animations[21].autoFollowup.followupMove);
                ManageEnemyHealthBar(false);
                //print("DEEEED");
            }
            else
            {
                //MyOnDisable(); //uso questa funzione per stoppare tutto, meglio raffinare
                if (currentMove != null)
                  currentMove.StopSequence();
                seq = 0;
                ManageEnemyHealthBar(true);
            }
            if (controls != null)
                controls.Enable();
        }

        

    }

    public override void MyOnDisable()
    {
        base.MyOnDisable();
        if (controls != null)
            controls.Disable();
    }

    public override void LoadInfos () {
        
        if (status != null)
        {
            {

                //OLD
                /*
                if (pm == null)
                {
                    PlayersManager pmObject;
                    if (!(pmObject = FindObjectOfType<PlayersManager>()))
                    {
                        print("ERROR: coulnd't find PlayersManager");
                        return;
                    }
                    else
                        pm = pmObject;
                    //print("PM: " + pm);
                }
                */

                if (pm == null)
                {
                    if (PlayersManager.Instance == null)
                    {
                        pm = FindObjectOfType<PlayersManager>();
                        if (pm == null)
                        {
                            print("NO PLAYERSMANAGER");
                            return;
                        }
                        print("find");
                    }
                    else
                    {
                        pm = PlayersManager.Instance;
                        //print("instance");
                    }
                }

                timeManager = pm.timeManager;

                if ((enemySpecializer == null || !enemySpecializer.isAI))
                //se sono un giocatore
                {
                    //print("sono un player " + gameObject);

                    if (pm.player1 == this)
                    {
                        //pm.players.Add(this);
                        //controls = new PlayerControls(); //da testare
                        pm.InitializePlayersManager();
                        pm.gameManager.player = pm.player1;
                        pm.InitializePlayer(this);
                        //pm.gameManager.InizializeNPC(this, backgroundAwaker);
                    }
                    else if (pm.player2 == this)
                    {
                        //pm.gameManager.InizializeNPC(this, backgroundAwaker);
                        //controls = new PlayerControls(); //da testare
                        pm.InitializePlayer(this);
                    }
                    //pm.playerHBs.InitializeBars();


                }
                else
                {
                    pm.gameManager.InitializeNPC(this, backgroundAwaker);

                    if (damageDisplayGenerator != null && enemyCanvas != null)
                    {
                        if ((status.faction != 0 || (enemySpecializer.isAI)))// 
                                                                                                         //&& damageDisplayGenerator == null && enemyCanvas == null) //se sono un NPC e non sono ancora stato inizializzato
                        {

                            //damageDisplayGenerator = GetComponentInChildren<DamageDisplayGenerator>();
                            //enemyCanvas = GetComponentInChildren<EnemyCanvas>();

                            ManageEnemyHealthBar(true);


                        }
                        else
                            ManageEnemyHealthBar(false);
                    }
                }

                //pm.playerHBs.InitializeBars();
            }
            InitializeAnimatorParameters();
        }


			

	}

	protected void InitializeAnimatorParameters ()
    {

        walkBlendIndex = Animator.StringToHash("walkBlend");
        vspeedIndex = Animator.StringToHash("vspeed");
        walkspeedIndex = Animator.StringToHash("walkspeed");
        directionLockIndex = Animator.StringToHash("directionlock");
        directionLockFloatIndex = Animator.StringToHash("directionlockFLOAT");
        walkTriggerIndex = Animator.StringToHash("walktrigger");
        fastWalkTriggerIndex = Animator.StringToHash("fast walktrigger");
        slowWalkTriggerIndex = Animator.StringToHash("slow walktrigger");
        loopStateIndex = Animator.StringToHash("loopstate");
        walkingIndex = Animator.StringToHash("walking");
        walkForwardIndex = Animator.StringToHash("walkforward");
        walkBackwardsIndex = Animator.StringToHash("walkbackwards");
        neutralIndex = Animator.StringToHash("neutral");
        neutraltriggerIndex = Animator.StringToHash("neutraltrigger");
        airbourneIndex = Animator.StringToHash("airbourne");
        animationSpeedIndex = Animator.StringToHash("animationSpeed");
        dedIndex = Animator.StringToHash("ded");
        dieingIndex = Animator.StringToHash("dieing");
    }

    public override void LoadInputs () {
		//enemySpecializer = GetComponent<EnemySpecializer> ();

        if (enemySpecializer != null && enemySpecializer.isAI) {
			enemySpecializer.controller = this;
			enemySpecializer.LoadInputs ();
		} 
        else 
        {
            //inputCheck = GetComponent<NewInputCheck>();
            
            if (inputCheck != null && pm != null)
            {
                //print("inputs " + gameObject);
                if (enemySpecializer.inputRecorder != null)
                {
                    enemySpecializer.inputRecorder.player = this;
                }

                inputCheck.player = this;
                controls = new PlayerControls();
                inputCheck.controls = controls;
                controls.Enable();
                inputCheck.InitializeControls();

                if (!currentlyUsingJoystick)
                {
                    //print("nousingjoystick" + gameObject);
                    inputCheck.controls.bindingMask = InputBinding.MaskByGroup(inputCheck.controls.controlSchemes[0].bindingGroup);
                    //playerInput.SwitchCurrentControlScheme(controls.controlSchemes[0].bindingGroup);
                    //playerInput.user.ActivateControlScheme(controls.controlSchemes[0].bindingGroup);
                }
                else
                {
                    //print("yesusingjoystick" + gameObject);
                    if (pm.player1 == this)
                        inputCheck.controls.bindingMask = InputBinding.MaskByGroup(inputCheck.controls.controlSchemes[1].bindingGroup);
                    else if (pm.player2 == this)
                        inputCheck.controls.bindingMask = InputBinding.MaskByGroup(inputCheck.controls.controlSchemes[2].bindingGroup);

                    //playerInput.SwitchCurrentControlScheme(controls.controlSchemes[1].bindingGroup);
                    //playerInput.SwitchCurrentControlScheme(controls.controlSchemes[1].bindingGroup);
                    //playerInput.user.ActivateControlScheme(controls.controlSchemes[1].bindingGroup);
                }
            }
			

		}

       
        if (!currentlyUsingJoystick) {
				if (playerNumber == 1) {
					horizontalmovementCMD = "P1_horizontal";
					atk1CMD = "P1_attack1";
					atk2CMD = "P1_attack2";
					special1CMD = "P1_special1";
					jumpCMD = "P1_jump";
					dashCMD = "P1_dash";
					blockCMD = "P1_block";
					targetCMD = "P1_target";
					verticalmovementCMD = "P1_vertical";
					startCMD = "P1_start";
					selectCMD = "P1_select";
				} else if (playerNumber == 2) {

					horizontalmovementCMD = "P2_horizontal";
					atk1CMD = "P2_attack1";
					atk2CMD = "P2_attack2";
					jumpCMD = "P2_jump";
					dashCMD = "P2_dash";
					blockCMD = "P2_block";
					targetCMD = "P2_target";
					verticalmovementCMD = "P2_vertical";
					startCMD = "P2_start";
					selectCMD = "P2_select";
				}
				
			} else {
				if (playerNumber == 1) {
					horizontalmovementCMD = "P1_horizontal_JS";
					atk1CMD = "P1_attack1_JS";
					atk2CMD = "P1_attack2_JS";
					special1CMD = "P1_special1_JS";
				special2CMD = "P1_special2_JS";
				special3CMD = "P1_special3_JS";
				special4CMD = "P1_special4_JS";
				jumpCMD = "P1_jump_JS";
					dashCMD = "P1_dash_JS";
					blockCMD = "P1_block_JS";
					targetCMD = "P1_target_JS";
					verticalmovementCMD = "P1_vertical_JS";
					startCMD = "P1_start_JS";
					selectCMD = "P1_select_JS";
				} else if (playerNumber == 2) {
					horizontalmovementCMD = "P2_horizontal_JS";
					atk1CMD = "P2_attack1_JS";
					atk2CMD = "P2_attack2_JS";
					jumpCMD = "P2_jump_JS";
					dashCMD = "P2_dash_JS";
					blockCMD = "P2_block_JS";
					targetCMD = "P2_target_JS";
					verticalmovementCMD = "P2_vertical_JS";
					startCMD = "P2_start_JS";
					selectCMD = "P2_select_JS";
				}

			}

	}

    

    protected override void HorizontalMovement() {

        //if (!gachiBass)
        //print ("frame: " + Time.frameCount + " tempo: " + Time.realtimeSinceStartup);

        //animator.SetLayerWeight(armsLayerIndex, 1);
        //if (status.faction == 1)
        //print("previous: " + previousMoveSpeed + " move: " + move.x + " " + Time.fixedTime);
        

        if (holdingDash)
        {
            if (framesHoldingDash < 100)
                framesHoldingDash++;
        }
        else
            framesHoldingDash = 0;

        //flippa l'oggetto per affaccairsi al target
        if (targetList.myTarget != null) {
            
            if (targetList.myTarget.transform.position.x > transform.position.x && !facingRight &&
                ((currentMove == null) || (currentMove != null && currentMove.canFlip)))
                //((status.mystatus == Status.status.neutral && currentMove == null) || (currentMove != null && currentMove.canFlip))) //da testare beneee
            {
                if ((currentMove == animations[16] && seq >= minimumSprintFrames) || (currentMove == animations[12]))// && seq <= 10))
                {
                    
                    StartNewMove(animations[9]);
                    animations[9].isCharging = true;
                }
                Flip();
            }
            else if (targetList.myTarget.transform.position.x < transform.position.x && facingRight &&
                ((currentMove == null) || (currentMove != null && currentMove.canFlip)))
            //((status.mystatus == Status.status.neutral && currentMove == null) || (currentMove != null && currentMove.canFlip))) //da testare beneee

            {
                if ((currentMove == animations[16] && seq >= minimumSprintFrames) || (currentMove == animations[12]))// && seq <= 10))
                {
                    StartNewMove(animations[9]);
                    animations[9].isCharging = true;

                }
                Flip();
            }
		}


        //if ((status.myPhysicsStatus == Status.physicsStatus.airborne
        //	&& status.mystatus == Status.status.attacking)) 

        //if (!enemySpecializer.isAI) { 
        if (currentMove != animations[16] && (currentMove != null && !currentMove.canBufferRun)) 
            //if (currentMove != animations[16] && (status.mystatus != Status.status.neutral && currentMove != null && !currentMove.canBufferRun)) //da testare beneee
            runToggleTimer = 0;

			//if (!runToggle) 
		if (horizontal != 0) 
        {
            /*TEST
            if (!enemySpecializer.isAI)
            {
                if ((status.myPhysicsStatus == Status.physicsStatus.grounded && currentMove == null)
                //if ((status.myPhysicsStatus == Status.physicsStatus.grounded && status.mystatus == Status.status.neutral) //da testare beneee
                || (currentMove != null && currentMove.canBufferRun))
                {
                    //print ("CIAOOO");
                    if (holdingDash)
                    {

                        if (currentMove == null)
                            runToggleTimer++;
                        else if (runToggleTimer < currentMove.maxBufferableRunFrames)
                            runToggleTimer++;

                    }
                    else
                    {
                        runToggleTimer = 0;
                    }
                    if (runToggleTimer > timeToRunToggle)
                    {
                        runToggle = true;
                    }
                }
            }
            */



                
                if (currentMove == null)
				//if (status.mystatus == Status.status.neutral) //da testare beneee
					move.x = horizontal * movespeed;
				else if (currentMove != null && ((currentMove.canWalkWhenAirborne && status.myPhysicsStatus == Status.physicsStatus.airborne)
					|| (currentMove.canWalkWhenGrounded && status.myPhysicsStatus == Status.physicsStatus.grounded)))
					move.x = horizontal * currentMove.walkVelocity;
				else
					move.x = 0;


                //test
            if ((previousMoveSpeed < 0 && move.x > 0) || (previousMoveSpeed > 0 && move.x < 0))
            {
                framesSpentWalking = 0;
            }


            /*TEST

            if ((previousMoveSpeed < 0 && move.x > 0) || (previousMoveSpeed > 0 && move.x < 0))
            {
                framesSpentWalking = 0;
                runToggleTimer = 0;
            }

        if (horizontal != 1 && horizontal != -1)
        {
            //framesSpentWalking = 0;
            runToggleTimer = 0;
        }

            */


            //if (!enemySpecializer.isAI)
            {
                if (status.myPhysicsStatus == Status.physicsStatus.grounded && (currentMove == null
                //if (status.myPhysicsStatus == Status.physicsStatus.grounded && (status.mystatus == Status.status.neutral //da testare beneee
                || (currentMove != null && currentMove.canWalkWhenGrounded && currentMove.walkingLegs)))
                {
                    if (framesSpentWalking < 200)
                        framesSpentWalking++;
                }
                else
                {
                    framesSpentWalking = 0;
                }
                }

		}
        
        else {
				move.x = 0;
				framesSpentWalking = 0;
				//runToggleTimer = 0;
		}
			

        if (!enemySpecializer.isAI)
        {
            if ((status.myPhysicsStatus == Status.physicsStatus.grounded && currentMove == null)
            //if ((status.myPhysicsStatus == Status.physicsStatus.grounded && status.mystatus == Status.status.neutral) //da testare beneee
            || (currentMove != null && currentMove.canBufferRun))
            {
                //print ("CIAOOO");
                if (holdingDash)
                {

                    if (currentMove == null)
                        runToggleTimer++;
                    else if (runToggleTimer < currentMove.maxBufferableRunFrames)
                        runToggleTimer++;

                }
                else
                {
                    runToggleTimer = 0;
                }
                if (runToggleTimer > timeToRunToggle)
                {
                    runToggle = true;
                }
            }
        }


        

        //se non ha il bersaglio, flippa ogn volta che si muove in direzione opposta al facing
        if ((targetList.myTarget == null && !directionLock)) 
        {
				//Debug.Log("FLIP?");
				//Debug.Log(move.x);
				//print ("seq = " + seq);
				//if (move.x > 0 && !facingRight)
		    if (horizontal > 0 && !facingRight) 
            {
				if ((currentMove == animations [16] && (seq == 1 || seq >= minimumSprintFrames)) || (currentMove == animations [12]))
                {
					Flip ();


                    StartNewMove(animations [9]);
                    animations[9].isCharging = true;
                }
                else if (currentMove == null ||  currentMove.canFlip)
                //else if (status.mystatus == Status.status.neutral || (currentMove!= null && currentMove.canFlip)) //da testare beneeee
                {
						Flip ();


			    }

			}


				//Debug.Log("FLIP");
				//else if (move.x < 0 && facingRight)
				else if (horizontal < 0 && facingRight) {
					if ((currentMove == animations [16] && (seq == 1 || seq >= minimumSprintFrames)) || (currentMove == animations [12]))
                {

                    Flip();
						StartNewMove (animations [9]);
                    animations[9].isCharging = true;
                }
                else if (currentMove == null || currentMove.canFlip) 
                    //if (status.mystatus == Status.status.neutral || (currentMove!= null && currentMove.canFlip)) //da testare beneeee
                { 
                    Flip ();
				}
				}
		}

            if (targetList.myTarget == null)
        {
            if (currentMove == null || currentMove.canFlip) //da testare beneee
            //if (status.mystatus == Status.status.neutral || (currentMove != null && currentMove.canFlip)) 
            {
                if (facingRight && leftDirectionLock)
                {
                    Flip();
                }
                else if (!facingRight && rightDirectionLock)
                {
                    Flip();

                }


            }
        }

			
			//runstop lento
			if (runToggle == false && currentMove == animations [16] && status.myPhysicsStatus == Status.physicsStatus.grounded)
            {

            //print ("CIAO");
                if (conditions.CheckUsability(conditions.animations[12]))
                    StartNewMove (animations [12]);
                else
                {
                if (seq >= minimumSprintFrames)
                    currentMove.itsTimeToStopTheSequence = true;
                //runLockCounter = runLockTimer;
                }

			}


        if (currentMove == animations[16] && seq >= minimumSprintFrames && !status.CheckForStamina())
        {
            //StartNewMove (animations [12]);
            currentMove.itsTimeToStopTheSequence = true;
            runLockCounter = runLockTimer;

        }

        //if (currentMove == animations[16] && seq >= minimumSprintFrames && jumpDuration > jumpLeniency)
        if (currentMove == animations[16] && jumpDuration > jumpLeniency) //da testare
        {
            currentMove.itsTimeToStopTheSequence = true;
            
        }

        if (!enemySpecializer.isAI)
        {

            if (runToggle && currentMove != animations[16] && status.CheckForStamina() && conditions.CheckUsability(conditions.animations[16])
                && (currentMove == null && framesSpentWalking > minimumWalkFramesBeforeRunning || (currentMove != null && currentMove.canBufferRun))
                && runLockCounter == 0)
            {
                //print("run, player: " +runToggle);
                //print("run: " + status.mystatus);

                if (horizontal != 0) //test
                    StartNewMove(animations[16]);
            }

            {
                if (status.currentStamina == status.maxStamina)
                    runLockCounter = 0;

            }


            if (runLockCounter > 0)
                runLockCounter--;

            //if (!status.CheckForStamina ())
            //runLockCounter = runLockTimer;
        }








        //setta le variabili dell'animator
        if (((currentMove == null) || (currentMove != null && currentMove.canWalkWhenGrounded //da testare beneee
        //if (((status.mystatus == Status.status.neutral && currentMove == null) || (currentMove != null && currentMove.canWalkWhenGrounded
        && currentMove.walkingLegs)) && status.myPhysicsStatus == Status.physicsStatus.grounded && move.x != 0)
            {
                animator.SetBool(neutralIndex, false);
                animator.SetBool(walkingIndex, true);
            
                if (framesSpentWalking == 1)
                {
                //se non è un cambio direzione, setta il trigger
                if ((move.x > 0 && previousMoveSpeed >= 0) || (move.x < 0 && previousMoveSpeed <= 0))
                {
                    if (!dontTransitionToWalkAnimation)
                    {
                        animator.SetTrigger(walkTriggerIndex);
                    }
                    else
                    {
                        animator.SetTrigger(fastWalkTriggerIndex);
                    }
                }
                


            }

            //print("facing: " + facingRight + " move: " + move.x);

            if ((facingRight && move.x > 0) || (!facingRight && move.x < 0))
                    animator.SetBool(walkForwardIndex, true);
                else
                    animator.SetBool(walkForwardIndex, false);

                //if (framesSpentWalking == 1)
                {
                    //print("Qualcosa");
                    animator.SetBool(loopStateIndex, true);
                    //animator.SetTrigger("walkingTrigger");
                }
            dontTransitionToWalkAnimation = false;
            }
            else if (status.mystatus != Status.status.dashing)
            {
                animator.SetBool(walkingIndex, false);
            animator.SetFloat(walkspeedIndex, 1);
            //if (currentMove == null)
              //  animator.SetFloat(walkBlendIndex, 0);
            if (currentMove != null && !currentMove.loopingAnimation)
                animator.SetBool(loopStateIndex, false);

            animator.SetBool(walkForwardIndex, false);

            

            }


        //animator.SetFloat("walkspeed", Mathf.Abs(move.x / movespeed));
        
        if (move.x != 0 && status.mystatus != Status.status.dashing)
        {
            //print("ma quanto è move.x??? ecco: " + move.x);
            float walkRatio = Mathf.Abs(move.x / movespeed);

            

            if (walkRatio >= runAnimationRatioThreshold)
            {
                
                float wb = animator.GetFloat(walkBlendIndex);
                
                if (wb >= 1)
                {
                    wb -= Mathf.Min(0.04f, Mathf.Abs(1 - wb));
                }
                else
                {
                    if (wb == 0)
                        wb += Mathf.Min(0.8f, Mathf.Abs(1 - wb));
                    else
                        wb += Mathf.Min(0.04f, Mathf.Abs(1 - wb));

                }

                animator.SetFloat(walkBlendIndex, wb);
                //animator.SetFloat("walkspeed", 1);

            }
            else// if (Mathf.Abs(move.x / movespeed) <= 0.5f)
            {
                
                float ws = animator.GetFloat(walkBlendIndex);
                
                if (ws >= 0.7f)
                {
                    ws -= Mathf.Min(0.04f, Mathf.Abs(0.7f - ws));
                }
                else
                {
                    ws += Mathf.Min(0.04f, Mathf.Abs(0.7f - ws));

                }
                animator.SetFloat(walkBlendIndex, ws);
                //animator.SetFloat("walkspeed", 0.7f);
            }

            if (walkRatio >= runAnimationRatioThreshold)
            {
                if (directionLock)
                {
                    walkRatio *= dirLockRunAnimationSpeedScaler;
                }
                
                walkRatio *= runAnimationSpeedScaler;
            }
            else
            {
                if (directionLock)
                {
                    walkRatio *= dirLockWalkAnimationSpeedScaler;
                }
                
            }

            
            animator.SetFloat(walkspeedIndex, walkRatio);
        }
        
        /*
        if (move.x != 0 && status.mystatus != Status.status.dashing)
        {
            //print("ma quanto è move.x??? ecco: " + move.x);
            float walkspeedRatio = Mathf.Abs(move.x / movespeed);
            if (walkspeedRatio >= runAnimationRatioThreshold)
            {
                walkspeedRatio *= runAnimationSpeedScaler;
            }
            //if (Mathf.Abs(move.x / movespeed) > 0.5f)
            {
                float ws = animator.GetFloat("walkspeed");

                if (ws > walkspeedRatio)
                {
                    print("troooooppo grande: " + ws);
                    ws -= Mathf.Min(0.04f, Mathf.Abs(walkspeedRatio - ws));
                }
                else
                {
                    print("troooooppo piccolo: " + ws);
                    if (ws <= 0.4)
                        ws += Mathf.Min(0.4f, Mathf.Abs(walkspeedRatio - ws));
                    else
                        ws += Mathf.Min(0.04f, Mathf.Abs(walkspeedRatio - ws));

                }
                

                animator.SetFloat("walkspeed", ws);
                //animator.SetFloat("walkspeed", 1);

            }
            

        }
        */

        //print ("runtoggle = " + runToggle);
        //previousMoveSpeed = targetVelocity.x;
        previousMoveSpeed = (currentMove == null || currentMove.walkingLegs) ? move.x : 0;
        //previousMoveSpeed = move.x;
			previousFacingRight = facingRight;
            //if (status.faction == 1)
            //print("dopo: previous: " + previousMoveSpeed + " " + Time.fixedTime);
			//previousRunToggle = runToggle;
		

        //if (status.mystatus == Status.status.neutral && move.x != 0 && animator.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
        
        /*
		else {
			move.x = 0;
			//animator.SetBool ("walking", false);
		}
*/

        AerialMovement();
        //print("prima: " + targetVelocity.x);
        if (inHitstop == false && (currentMove == null || //da testare beneee
        //if (inHitstop == false && (status.mystatus == Status.status.neutral || 
            (currentMove != null && status.mystatus != Status.status.dashing && (
            (currentMove.canWalkWhenGrounded && status.myPhysicsStatus == Status.physicsStatus.grounded)
            || currentMove.canWalkWhenAirborne && status.myPhysicsStatus == Status.physicsStatus.airborne))))
                targetVelocity.x = move.x;
        //print("dopo: " + targetVelocity.x);


        //if (playerNumber == 2)
        //print (status.mystatus);
    }

    protected void AerialMovement ()
    {
        if (currentMove == null || (currentMove.canWalkWhenAirborne && currentMove.usesNormalWalkspeed))
            //maxAerialMomentum = movespeed;
            maxAerialMomentum = defaultMaxAerialMomentum;
        else
        {
            maxAerialMomentum = currentMove.walkVelocity;
        }
        if (status.myPhysicsStatus != Status.physicsStatus.airborne || (currentMove != null && !currentMove.canWalkWhenAirborne))
        {
            //print("reset momentum " + status.myPhysicsStatus + " " + velocity.y + " ");
            //aerialMomentum = 0;
            return;

        }
        float deceleration = aerialMomentumDeceleration;
        float acceleration = aerialMomentumAcceleration;

        if (currentMove != null)
        {
            for (int i = 0; i < currentMove.adjustableSpeedIntervals.Length; i++)
            {
                if (seq >= currentMove.adjustableSpeedIntervals[i].windowStart && seq <= currentMove.adjustableSpeedIntervals[i].windowEnd)
                {

                    //if ((horizontal > 0 && facingRight) || (horizontal < 0 && !facingRight))
                        acceleration = currentMove.adjustableSpeedIntervals[i].positiveVelocity;
                    //else if ((horizontal < 0 && facingRight) || (horizontal > 0 && !facingRight))
                    {
                        
                        deceleration = horizontal == 0 ? currentMove.adjustableSpeedIntervals[i].neutralVelocity :
                            currentMove.adjustableSpeedIntervals[i].negativeVelocity;
                        maxAerialMomentum = currentMove.adjustableSpeedIntervals[i].maxVelocity;
                    }
                    //print("acceleration: " + acceleration + " " + deceleration);
                    break;
                }
            }
        }

        if (horizontal != 0)
        {
            //aerialMomentum += Mathf.Sign(horizontal) * Mathf.Min(aerialMomentumAcceleration, maxAerialMomentum - Mathf.Abs(aerialMomentum));
            
            
            float addedMomentum = Mathf.Sign(horizontal) * acceleration;
            if (addedMomentum > 0)
            {
                if (aerialMomentum + addedMomentum > maxAerialMomentum)
                {
                    addedMomentum = Mathf.Max(0, maxAerialMomentum - aerialMomentum);
                }
            }
            else
            {
                if (aerialMomentum + addedMomentum < -maxAerialMomentum)
                {
                    addedMomentum = Mathf.Min(0, -maxAerialMomentum - aerialMomentum);
                }
            }
            //print("aerialprima: " + aerialMomentum + "added: " + addedMomentum);
            aerialMomentum += addedMomentum;
            //print("aerial: " + aerialMomentum);

            //aerialMomentum = Mathf.Sign(aerialMomentum) * Mathf.Min(maxAerialMomentum, Mathf.Abs(aerialMomentum));
        }
        else
        {
            //aerialMomentum -= facingRight ? Mathf.Min(Mathf.Abs(aerialMomentum), deceleration)
            //  : -Mathf.Min(Mathf.Abs(aerialMomentum), deceleration);
            float thisDeceleration = Mathf.Min(Mathf.Abs(aerialMomentum), deceleration);
            aerialMomentum -= aerialMomentum > 0 ? thisDeceleration : -thisDeceleration;
        }

        move.x = aerialMomentum;

        if (currentMove != null)
        {
            for (int i = 0; i < currentMove.adjustableSpeedIntervals.Length; i++)
            {
                if (seq >= currentMove.adjustableSpeedIntervals[i].windowStart && seq <= currentMove.adjustableSpeedIntervals[i].windowEnd)
                {
                    if ((facingRight && move.x < currentMove.adjustableSpeedIntervals[i].minVelocity)
                        || (!facingRight && move.x > -currentMove.adjustableSpeedIntervals[i].minVelocity))
                    {
                        move.x = gameObject.transform.localScale.x * currentMove.adjustableSpeedIntervals[i].minVelocity;
                        //print("BEBEBEEB " + move.x + " " + currentMove.adjustableSpeedIntervals[i].minVelocity);

                    }
                    else if ((facingRight && move.x > currentMove.adjustableSpeedIntervals[i].maxVelocity)
                        || (!facingRight && move.x < -currentMove.adjustableSpeedIntervals[i].maxVelocity))
                    {
                        
                        move.x = gameObject.transform.localScale.x * currentMove.adjustableSpeedIntervals[i].maxVelocity;
                        //print("BoBoBEEB " + move.x);
                    }
                    break;
                }

            }
        }
    }
		
	

	protected override void Custom () {
        

	}

	protected override void Jump () {


        if (currentMove == animations[4])
        {
            /*
            if (inputCheck.controls.Player.JumpCharged.ReadValue<float>() > 0)
            {
                //print("chaaarge");
                animations[4].isCharging = true;
            }
            else
                animations[4].isCharging = false;

            */
        }

        if (jumpBuffer > 0)
        {
            if (vertical < 0 && status.mystatus == Status.status.neutral
            && status.myPhysicsStatus == Status.physicsStatus.grounded && (collisions.standingOnPlatform) && !hasGroundUnder)
            {// && collisions.standingOnPlatform) {
             //collisions.isIgnoringPlatforms = 10;
             //print("alora");
                StartNewMove(animations[20]);
                jumpBuffer = 0;
                //animator.Play("jump rising", 0, 0);
            }
            else
            {
                if (conditions.CheckUsability(conditions.animations[17]))
                    /*
                    || (status.myPhysicsStatus == Status.physicsStatus.airborne &&
                    status.mystatus == Status.status.neutral &&
                    jumpDuration <= jumpLeniency && recentlyJumpedFrames == 0 ))
                    */
                {
                    StartNewMove(conditions.animations[17].move);
                    //print("MEDIC GAMING");
                    //recentlyJumpedFrames = jumpLeniency;
                    jumpBuffer = 0;
                }

                //print("status: " + status.mystatus + " pStatus: " + status.myPhysicsStatus + " jumpduration: " + jumpDuration + " recentlyjumped: " + recentlyJumpedFrames);
                if ((conditions.CheckUsability(conditions.animations[4]) || (status.myPhysicsStatus == Status.physicsStatus.airborne &&
                    status.mystatus == Status.status.neutral &&
                    jumpDuration <= jumpLeniency && recentlyJumpedFrames == 0)))
                {
                    //print("JUMPPPPP");
                    StartNewMove(conditions.animations[4].move);

                    //recentlyJumpedFrames = jumpLeniency;
                    jumpBuffer = 0;
                }

                

                /*
                //if (vertical < 0 && !collisions.standingOnPlatform) {
                if (vertical <= 0 && status.myPhysicsStatus == Status.physicsStatus.grounded && !collisions.standingOnStairs && collisions.isIgnoringStairs <= 0)
                {

                    collisions.isIgnoringStairs = 1;
                    //print ("WHAT " + collisions.standingOnStairs);
                }

                if (vertical < 0 && status.myPhysicsStatus == Status.physicsStatus.airborne && !collisions.standingOnStairs
                    && collisions.isIgnoringPlatforms <= 0 && status.mystatus == Status.status.neutral)
                {
                    collisions.isIgnoringPlatforms = 1;

                }
                */
                /*
                if (jumpBuffer > 0 && vertical < 0 && status.mystatus == Status.status.neutral 
                    && status.myPhysicsStatus == Status.physicsStatus.grounded) {// && collisions.standingOnPlatform) {
                    collisions.isIgnoringPlatforms = 10;
                    jumpBuffer = 0;
                    //animator.Play("jump rising", 0, 0);
                }
                */
                //print("standingonplatform: " + collisions.standingOnPlatform);





                //PROVA CODICE ARRAMPICATA SU LEDGE
                //if (status.mystatus == Status.status.neutral && status.myPhysicsStatus == Status.physicsStatus.airborne
                //&& collisions.isIgnoringPlatforms <= 0 && jumpBuffer > 0) {
                if (jumpBuffer > 0 && conditions.CheckUsability(conditions.animations[18]) && collisions.isIgnoringPlatforms <= 0)
                {
                    StartNewMove(animations[18]);
                    jumpBuffer = 0;


                }

                /*
                if (Input.GetKey(KeyCode.C))
                {
                    equipmentManager.Equip(equipmentManager.equipmentPieces[0].boneName, equipmentManager.equipmentPieces[0].equipSprite);
                }
                */



















                /* il salto è anti-airabile su reazione
                 * batte l'attacco forte, viene battuto dal non fare niente
                 * il non fare niente batte l'attacco ma per meno danno
                 * l'attacco forte batte l'attacco per molto danno
                 * /

                    /*
                if (jumpBuffer > 0 && status.myPhysicsStatus == Status.physicsStatus.grounded && (status.mystatus == Status.status.neutral || 
                    status.mystatus == Status.status.blocking)) {

                    //print ("JUMP!");
                    StartNewMove (animations [4]);
                    jumpBuffer = 0;
                    //status.mystatus = Status.status.neutral;
                }

        */
            }
        }
        
	}

	protected override void Attack () {
		conditions.CheckConditions();

	}



	protected override void Target ()
	{

        /*
		if (targetpressed && turnOffTargetTimer < TimeToTargetTurnOff) {
			targetList.Target ();
		}
		*/

        //if (!currentlyUsingJoystick || (enemySpecializer != null && enemySpecializer.isEnemy)) {
        if (enemySpecializer != null && enemySpecializer.isAI)
        {

            //if (targetpressed)
            //	directionLock = !directionLock;
            if (targetpressed) {
				//print ("targetpress, sono " + gameObject);

				if (turnOffTargetTimer < TimeToTargetTurnOff) {


				    targetList.Target ();

				    turnOffTargetTimer = 0;

                    if (targetList.newEngagement)
                    {
                        targetList.newEngagement = false;
                        if (enemySpecializer != null && enemySpecializer.isAI)
                        {
                            enemySpecializer.bSelector.newEngagement = true;
                        }
                    }
			    }
			}
		} else {
            //if (currentlyUsingJoystick)
            {
                if (targetpressed)
                {

                    directionLock = true;
                    animator.SetBool(directionLockIndex, true);
                }
                else
                {
                    directionLock = false;
                    animator.SetBool(directionLockIndex, false);
                    leftDirectionLock = false;
                    rightDirectionLock = false;

                }
            }
            /*
            else
            {
				

				if (targetreleased)
				{ 
					if (turnOffTargetTimer < TimeToTargetTurnOff)
					{

						if (directionLock == false)
							directionLock = true;
						else
						{
							if (status.mystatus == Status.status.neutral || (currentMove != null && currentMove.canFlip))
								Flip();
						}
					}
					turnOffTargetTimer = 0;
				}
				TurnOffTarget();


			}
            */
		}
        float animDirectionLock = animator.GetFloat(directionLockFloatIndex);


        if (animator.GetBool(directionLockIndex))
        {
            
            //if (!enemySpecializer.isAI)
            {
                if (animDirectionLock < 1)
                    animDirectionLock += 0.1f;
                if (animDirectionLock > 1)
                    animDirectionLock = 1;
                
            }
            animator.SetFloat(directionLockFloatIndex, animDirectionLock);
        }
        else
        {

            //animator.SetBool(directionLockIndex, false);
            if (animDirectionLock > 0)
                animDirectionLock -= 0.1f;
            if (animDirectionLock < 0)
                animDirectionLock = 0;
            animator.SetFloat(directionLockFloatIndex, animDirectionLock);

        }

        //targetpressed = false;
    }

	protected override void TurnOffTarget () {

        //CheckForInput ();
        if (inputCheck.controls.Player.Target.ReadValue<float>() > 0)
        {
			if (turnOffTargetTimer < TimeToTargetTurnOff)
			{
				turnOffTargetTimer++;
			}
		}
		//else
			//turnOffTargetTimer = 0;

		if (turnOffTargetTimer == TimeToTargetTurnOff)
		{
			targetList.TurnOffTarget ();
			if (!currentlyUsingJoystick && (enemySpecializer == null || !enemySpecializer.isAI))
				directionLock = false;
		}

	}



	protected override void Block () {
		

	}

	protected override void Hitstun ()
	{


        //if (collisionHurtbox.Count > 0){
        /*
        if (hitWhileDashing)
        {
            StartNewMove(animations[23]);
            hitWhileDashing = false;
        }
        */


        //hurtboxesHitList = new List<HurtboxHit>(collisionHurtbox.Values);

        if (lastCollisionHurtbox.move.frameData != null && lastCollisionHurtbox.hurtbox != null) {

			//print ("EEEEYOOOO " + Time.frameCount);


			


			//HurtboxHit hurtboxhit = lastCollisionHurtbox.hurtbox;

            
            
            if (targetList != null && lastCollisionHurtbox.move.frameData.owner != null && lastCollisionHurtbox.move.frameData.owner.targetList != null
                && lastCollisionHurtbox.move.frameData.owner.targetList.myTargetableCollider != null)
            {
                //print("aggiungi");
                targetList.AddObjectToPotentialList(lastCollisionHurtbox.move.frameData.owner.targetList.myTargetableCollider.gameObject);
            }
            
            /*
			if (hurtboxhit.parry) {
				StartNewMove (animations [15]);
			}


			else if (hurtboxhit.hit && hurtboxhit.offenderMove.hitStunType == 1) {
				//if (hurtboxhit.hitstunType == Status.status.hitstun1)
				//animator.Play(animations[5].animatorParameterName, -1, 0f);
				//print ("A");

					StartNewMove (animations [5]);
				//print ("gameobject: " + gameObject);
				//print ("offender: " + hurtboxhit.offender + "frame: " + Time.frameCount);
				//print ("Sono " + gameObject + " " + hurtboxhit.offenderController.jumpstartup);
				hurtboxhit.offenderController.jumpstartup = 0;
				//print ("gameobject: " + gameObject);
				//print ("offender: " + hurtboxhit.offenderController);
			} else if (hurtboxhit.block && hurtboxhit.offenderMove.blockStunType == 1) {
				//if (hurtboxhit.hitstunType == Status.status.blockstun1)
				//animator.Play(animations[7].animatorParameterName, -1, 0f);

					StartNewMove (animations [7]);
			} else if (hurtboxhit.block && hurtboxhit.offenderMove.blockStunType == 2) {
			//if (hurtboxhit.hitstunType == Status.status.blockstun1)
				//animator.Play(animations[13].animatorParameterName, -1, 0f);

					StartNewMove (animations [13]);
		}
			else if (hurtboxhit.juggle) {
				//if (hurtboxhit.hitstunType == Status.status.blockstun1)
				//animator.Play(animations[10].animatorParameterName, -1, 0f);

				StartNewMove (animations [10]);
			}

			for (int i = 0; i < hurtboxesHitList.Count; i++) {
				hurtboxesHitList[i].hit = false;
				hurtboxesHitList[i].block = false;
				hurtboxesHitList[i].juggle = false;
				hurtboxesHitList[i].parry = false;
				hurtboxesHitList[i].gotHit = false;
			}

*/



            /*
            if (lastCollisionHurtbox.move.grab != null && getGrabbed != null)
            {
                if (hurtboxhit.hit)
                {
                    print("getgrabbeddd");
                    lastCollisionHurtbox.move.owner.StartNewMove(lastCollisionHurtbox.move.grab);

                    getGrabbed.GetGrabInfos(lastCollisionHurtbox.move);
                    StartNewMove(getGrabbed);
                }
            }
            else
            */

            if (status.currentHealth > 0)
            {
                { 
                    
                    //test, potenzialmente provvisorio
                    /*
                    if (lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.parry)
                    {
                        
                        {
                            if (currentMove != null && currentMove.relatedMoves.Length > 0 && currentMove.relatedMoves[0] != null)//is ParryActivator)
                            {
                                //print("isparry");
                                //ParryActivator parry = (ParryActivator)currentMove;
                                StartNewMove(currentMove.relatedMoves[0]);

                            }
                            else
                            {
                                //StartNewMove(animations[15]);
                                print("no parry no party " + currentMove.gameObject);
                            }
                        }
                    }
                    else
                    */

                    {
                        /*
                        if (lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.hit && status.exhausted)
                        {
                            //print("exhausted");
                            StartNewMove(animations[19]);
                            //StartNewMove(animations[10]);
                            //status.exhausted = !status.exhausted;
                            //startnewmove exaustedhitstun
                        }

                        else
                        */
                        {

                            /*
                            if (status.currentStamina <= 0)
                            {
                                if (hurtboxhit.hit)
                                {
                                    StartNewMove(animations[14]);
                                }



                            }

                            else
                            */

                            {
                                if (lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.hit)
                                {

                                    {
                                        if (lastCollisionHurtbox.move.frameData.hitstunType == Status.status.hitstun1)
                                        {
                                            //if (hurtboxhit.hitstunType == Status.status.hitstun1)
                                            //animator.Play(animations[5].animatorParameterName, -1, 0f);
                                            //print ("A");
                                            if (lastCollisionHurtbox.move.frameData.owner != null)
                                                lastCollisionHurtbox.move.frameData.owner.jumpstartup = 0;
                                            /*
                                            if (status.currentStamina <= 0)
                                            {
                                                StartNewMove(animations[14]);
                                            }
                                            else
                                            */
                                            StartNewMove(animations[5]);
                                            //print ("gameobject: " + gameObject);
                                            //print ("offender: " + hurtboxhit.offender + "frame: " + Time.frameCount);
                                            //print ("Sono " + gameObject + " " + hurtboxhit.offenderController.jumpstartup);
                                            //print ("gameobject: " + gameObject);
                                            //print ("offender: " + hurtboxhit.offenderController);
                                        }
                                        else if (lastCollisionHurtbox.move.frameData.hitstunType == Status.status.hitstun2)
                                        {
                                            //if (hurtboxhit.hitstunType == Status.status.hitstun1)
                                            //animator.Play(animations[5].animatorParameterName, -1, 0f);
                                            //print ("A");
                                            if (lastCollisionHurtbox.move.frameData.owner != null)
                                                lastCollisionHurtbox.move.frameData.owner.jumpstartup = 0;
                                            /*
                                            if (status.currentStamina <= 0)
                                            {
                                                StartNewMove(animations[14]);
                                            }
                                            else
                                            */
                                            StartNewMove(animations[15]);
                                            //print ("gameobject: " + gameObject);
                                            //print ("offender: " + hurtboxhit.offender + "frame: " + Time.frameCount);
                                            //print ("Sono " + gameObject + " " + hurtboxhit.offenderController.jumpstartup);
                                            //print ("gameobject: " + gameObject);
                                            //print ("offender: " + hurtboxhit.offenderController);
                                        }
                                    }
                                }
                                else if (lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.guardBreak)
                                {
                                        StartNewMove(animations[14]);
                                }


                                else
                                if ((lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.block || lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.parry) 
                                    && lastCollisionHurtbox.move.frameData.blockstunType == Status.status.blockstun1)
                                {
                                    //if (hurtboxhit.hitstunType == Status.status.blockstun1)
                                    //animator.Play(animations[7].animatorParameterName, -1, 0f);
                                    /*
                                    if (status.currentStamina <= 0)
                                    {
                                        StartNewMove(animations[14]);
                                    }
                                    else
                                    */
                                    {
                                        StartNewMove(animations[7]);
                                    }
                                }
                                else
                                if ((lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.block || lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.parry) &&
                                    lastCollisionHurtbox.move.frameData.blockstunType == Status.status.guardBreak)
                                {
                                    //if (hurtboxhit.hitstunType == Status.status.blockstun1)
                                    //animator.Play(animations[13].animatorParameterName, -1, 0f);
                                    /*
                                    if (status.currentStamina <= 0)
                                    {
                                        print("AAAAAAAAAAAAAAAAAAAAHHHHH");
                                        StartNewMove(animations[14]);
                                    }
                                    else
                                    */
                                    StartNewMove(animations[13]);
                                }
                                else
                                if (lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.juggle)
                                {
                                    //if (hurtboxhit.hitstunType == Status.status.blockstun1)
                                    //animator.Play(animations[10].animatorParameterName, -1, 0f);
                                    //print ("player: juggle");
                                    /*
                                    if (status.currentStamina <= 0)
                                    {
                                        StartNewMove(animations[14]);
                                    }
                                    else
                                    */
                                    StartNewMove(animations[10]);
                                }
                                else
                                if (lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.grab && lastCollisionHurtbox.move.frameData.grab != null && getGrabbed != null)
                                {

                                    //print("getgrabbeddd");
                                    getGrabbed.GetGrabInfos(lastCollisionHurtbox.move.frameData);

                                    lastCollisionHurtbox.move.frameData.owner.StartNewMove(lastCollisionHurtbox.move.frameData.grab);
                                    StartNewMove(getGrabbed);


                                }

                            }
                        }
                    }
                }
            }
            else
            {
                if (lastCollisionHurtbox.move.hitType == HurtboxHit.HitType.grab)
                {
                    if (lastCollisionHurtbox.move.frameData.grab != null && getGrabbed != null)
                    {

                        print("getgrabbeddd");
                        getGrabbed.GetGrabInfos(lastCollisionHurtbox.move.frameData);
                        
                        lastCollisionHurtbox.move.frameData.owner.StartNewMove(lastCollisionHurtbox.move.frameData.grab);
                        StartNewMove(getGrabbed);

                    }
                }
            }

		}

        ClearHurtboxes();

        /*
        lastCollisionHurtbox.move = null;
        lastCollisionHurtbox.hurtbox = null;

        for (int i = 0; i < hurtboxesHitList.Count; i++) {

            
			hurtboxesHitList[i].hit = false;
			hurtboxesHitList[i].block = false;
			hurtboxesHitList[i].juggle = false;
			hurtboxesHitList[i].parry = false;
			hurtboxesHitList[i].gotHit = false;
			

		}

        hurtboxesHitList.Clear();
        */

    }
		

	protected override void Dash ()
	{
		
		if (dashBuffer > 0 && hitstop <= 0) {
            //print("DASH");
		//if (dashBuffer > 0 && !blockpressed && !Input.GetButton(blockCMD)) {
			//if (runToggleTimer < timeToRunToggle)
            //if (framesHoldingDash < 15)
            //if (vertical >= 0)
			{
                //print("dosh");
				runToggleTimer = 0;

                //if ((directionLock || targetList.myTarget != null) && ((horizontal > 0 && !facingRight) || (horizontal < 0 && facingRight))) 
                if (((directionLock) && ((horizontal > 0 && leftDirectionLock) || (horizontal < 0 && rightDirectionLock))))
                    //|| (currentMove != null && ((horizontal > 0 && !facingRight) || (horizontal < 0 && facingRight))))
                    //DA TESTARE: se sei in un'animazione, allora fai backdash a prescindere dal directionlock
                {
                    if (conditions.CheckUsability (conditions.animations [2])) 
                    {

                        if (horizontal != 0 && (horizontal > 0 && facingRight || horizontal < 0 && !facingRight))
                        {
                            Flip();
                            print("flip");
                        }
                        StartNewMove (conditions.animations [6].move);
							dashBuffer = 0;
                        
					}
				}


		        else if (conditions.CheckUsability(conditions.animations[2]))// && horizontal != 0)
                {
                    if (horizontal != 0 && (horizontal > 0 && !facingRight || horizontal < 0 && facingRight))
                    {
                        Flip();
                        print("flip");
                    }
						
                    StartNewMove (conditions.animations [2].move);
						
                    dashBuffer = 0;
		        }
                /*
				else if (conditions.CheckUsability(conditions.animations[14]) && jumpDuration > minimumJumpDurationBeforeAirdash
					&& minimumGroundDistanceBeforeAirAction < currentGroundDistance) {
					StartNewMove (conditions.animations [14].move);
					dashBuffer = 0;
                    
				}
                */

			}
            /*
            else
            {
                runToggleTimer = 0;
                StartNewMove(animations[12]);
                dashBuffer = 0;
            }
            */
			//runToggleTimer = 0;	
		}

		//ROLL
		if (dashBuffer > 0 && hitstop <= 0) {

			if (conditions.CheckUsability(conditions.animations[3])) {

                //se sta facendo backdash
				if (currentMove == animations [6] || (moveset.customBackDash != null && currentMove == moveset.customBackDash)) {
                    //rolla solo se punta nella stessa direzione del dash precedente
                    //print("roll?");
					if (horizontal == 0 || ((horizontal > 0 && !facingRight) || (horizontal < 0 && facingRight))) {

                        //Flip ();
                        //print("pronto?");
                        conditions.animations[3].move.isCharging = true;

                        //Flip();
						StartNewMove (conditions.animations [3].move);
						dashBuffer = 0;
					}
                    //altrimenti se sta facendo dash
				} else if (currentMove == animations [2] || (moveset.customDash != null && currentMove == moveset.customDash))
                {
					if (horizontal == 0 || ((horizontal > 0 && facingRight) || (horizontal < 0 && !facingRight))) {
						StartNewMove (conditions.animations [3].move);
						dashBuffer = 0;
					}
                    //altrimenti fa roll nella direzione in cui punta
				} else
                {
                    if (horizontal == 0 || ((horizontal > 0 && facingRight) || (horizontal < 0 && !facingRight)))
                    {
                        print("rolll");
                        StartNewMove(conditions.animations[3].move);
                        dashBuffer = 0;
                    }
                    else
                    {
                        //Flip();
                        print("bcakrolll");

                        conditions.animations[3].move.isCharging = true;
                        StartNewMove(conditions.animations[3].move);
                        dashBuffer = 0;
                    }
                }

			}
				
				
		}
	}
    


    protected override void InputCheck () {

        if (status.currentHealth <= 0)
        {
            //FlushBuffers();
            return;
        }
        if (enemySpecializer.inputRecorder != null)
        {
            
                //enemySpecializer.inputRecorder.SetBuffers();
                return;
            
        }
        //enemySpecializer.MoveTargetingBox();
        if (enemySpecializer != null && enemySpecializer.isAI) {
            if (!enemySpecializer.isDummy)
                enemySpecializer.InputCheck();
            else
            {
                //horizontal = 1f;
                if (debugGottaGoFast)
                {
                    directionLock = debugDirLock;
                    horizontal = debugHorizontal;

                }
                else
                {
                    //horizontal = 0.5f;

                    //directionLock = false;
                }
            }
            enemySpecializer.bSelector.aiMovements.CalculatePositionRelativeToCompetenceZone();
        }
        else
        {
            if (enemySpecializer.inputRecorder == null)
                inputCheck.SetBuffers();
            
        }

	}

    protected override void SimpleMovement() //funzione di debug
    {
        status.invincible = true;
        if (horizontal > 0)
        {

            tr.Translate((holdingDash ? 100 : 20) * Time.fixedDeltaTime, 0, 0);
        }
        else if (horizontal < 0)
        {
            tr.Translate((holdingDash ? -100 : -20) * Time.fixedDeltaTime, 0, 0);

        }
        if (vertical < 0)
        {
            tr.Translate(0, (holdingDash ? -100 : -20) * Time.fixedDeltaTime, 0);
        }
        else if (vertical > 0)
        {
            tr.Translate(0, (holdingDash ? 100 : 20) * Time.fixedDeltaTime, 0);
        }

        if (movingParts != null)
        {
            movingParts.position = tr.position;
            movingParts.localScale = tr.localScale;
        }
    }
}
