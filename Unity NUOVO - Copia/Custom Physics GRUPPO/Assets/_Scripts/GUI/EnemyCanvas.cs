﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCanvas : MonoBehaviour
{
    private Transform objectToFollow;
    private Controller2D controller;
    private Status status;
    
    public List<BarScaleController> bars = new List<BarScaleController>();
    public void EnemyCanvasInitialize(Controller2D cont)
    {

        controller = cont;
            status = controller.status;
        

        //if (controller != null)
        objectToFollow = controller.transform;
        
        //GetComponentsInChildren<BarScale>(true, bars);
        if (status != null)
        {
            for (int i = 0; i < bars.Count; i++)
            {
                //print("statuuuus");
                bars[i].status = status;
            }
        }
    }

  
}
