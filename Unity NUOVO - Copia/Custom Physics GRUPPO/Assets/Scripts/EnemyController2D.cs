﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController2D : CustomPhysics {
    //DEPRECATA!!!
	public EnemyTargetableList targetList;

		public int hitstop = 0;
		public int minimumBlockstunFramesToStartCounter = 8; //devi essere almeno questi frame in blockstun per fare un counter

		public string horizontalmovementCMD = "P1_horizontal";
		public string atk1CMD = "P1_attack1";
		public string atk2CMD = "P1_attack2";
		public string jumpCMD = "P1_jump";
		public string dashCMD = "P1_dash";
		public string blockCMD = "P1_kick";
		public string targetCMD = "P1_target";
		public string verticalmovementCMD = "P1_vertical";
		public string startCMD = "P1_start";
		public string selectCMD = "P1_select";



		public bool currentlyUsingJoystick = false; //settare a false se stai usando la tastiera, settare a true se stai usando joystick

		public int atk1Buffer, dashBuffer, atk2Buffer, jumpBuffer, blockBuffer, targetBuffer, actionBuffer, specialBuffer, horizontal, vertical;
		public bool atk1pressed, atk1released, dashpressed, dashreleased, atk2pressed, atk2released, jumppressed, jumpreleased, blockpressed, blockreleased;
		public bool targetpressed, targetreleased, actionpressed, actionreleased, specialpressed, specialreleased;
		public bool upPressed, upreleased, downpressed, downreleased, rightpressed, rightreleased, leftpressed, leftreleased;
		public float up, down, left, right, verticalprevious, horizontalprevious;
		//private int jumpDuration = 0;

		public AnimationUtilities animUtils;
		public string animParameter;
		public Animator animator;
		public MoveScript currentMove;
		public float movespeed = 6;
		public float runSpeed = 10;
		//public float jumpVelocity = 5;
		public bool customLanding = false;

		protected Vector2 move = Vector2.zero;
		public Status status;
		//public Status status;
		//public GameObject weapon;
		//public MoveSet move;

		//protected int attack;
		public MoveScript landing, wakeup;
		//public HurtboxHit hurtboxhit;

		public ActivationConditions conditions;

		public MoveSet moveset, animationset;
		public MoveScript [] moves, animations;
		public bool skipExecution;
		public int moveIndex;
		public int seq = 0;
		private int juggleFramesCount = 0;

		public bool facingRight = true;
		public bool previousFacingRight;

		public int startupCounter = 0;

		public delegate void DeathEvent (GameObject obj);
		public static event DeathEvent death;

		protected int walkLayerIndex, runLayerIndex, walkBackwardsLayerIndex, juggleLayerIndex;


		public override void MyOnEnable () {
			LoadInfos ();
			LoadWeapon ();
			//LoadMoves ();
			LoadInputs ();


			walkLayerIndex = animator.GetLayerIndex("walk");
			runLayerIndex = animator.GetLayerIndex ("run");
			walkBackwardsLayerIndex = animator.GetLayerIndex ("walkbackwards");
			juggleLayerIndex = animator.GetLayerIndex ("juggle");

			status = GetComponent<Status> ();
			animUtils = transform.Find ("Skeleton").GetComponent<AnimationUtilities>();



		}

		public virtual void LoadInfos () {

		}

		public virtual void LoadInputs () {

		}

		//funzione che rimette a falso le bool settate a vero in Update
		protected void ResetInputs () {
			atk1pressed = atk2pressed = jumppressed = dashpressed = blockpressed = atk1released = atk2released = jumpreleased = dashreleased = blockreleased = false;
		}

		public void InputDeviceSwitch () {
			currentlyUsingJoystick = !currentlyUsingJoystick;
			LoadInputs ();
		}

		protected virtual void InputCheck() {

		}

		protected virtual void InputCombinationCheck() {

		}

		// Use this for initialization
		public void FlushBuffers() {
			atk1Buffer=dashBuffer=atk2Buffer=jumpBuffer=blockBuffer=0;
		}

		//Debug.Log (targetVelocity); 


		void Update () {
			InputCheck ();
			InputCombinationCheck ();
		}

		protected void InputBufferDecrease() {
			if (dashBuffer > 0)
				dashBuffer--;
			if (atk1Buffer > 0)
				atk1Buffer--;
			if (atk2Buffer > 0)
				atk2Buffer--;
			if (jumpBuffer > 0)
				jumpBuffer--;
			if (blockBuffer > 0)
				blockBuffer--;
			if (targetBuffer > 0)
				targetBuffer--;
		}

		protected override void ComputeVelocity ()
		{
		if (status.myPreviousStatus != Status.status.neutral && status.mystatus == Status.status.neutral) {
			StartCoroutine (status.WaitBeforeRechargeStamina ());
			print ("RECHARGE");
		}


		for (int i = 0; i < hurtboxes.Length; i++) {

			if (status.invincible) {
				hurtboxes [i].enabled = false;

			} else {
				hurtboxes[i].enabled = true;
			}
		}

			//InputBufferCheck ();
			InputBufferDecrease();


			targetVelocity.x = 0;

			Hitstun();

			if (currentMove != null && currentMove.itsTimeToStopTheSequence) {
				//print ("STOP!");
				currentMove.itsTimeToStopTheSequence = false;
				currentMove.StopSequence ();
			}



			if (grounded) {
				status.myPhysicsStatus = Status.physicsStatus.grounded;


			}
			else
				status.myPhysicsStatus = Status.physicsStatus.airborne;

			if (status.myPhysicsStatus == Status.physicsStatus.airborne)
				jumpDuration++;

			//Debug.Log (status.myPhysicsStatus);

			if (status.myPhysicsStatus == Status.physicsStatus.airborne) {
				animator.SetBool ("airbourne", true);
				if (status.mystatus == Status.status.neutral) {
					animator.SetLayerWeight (juggleLayerIndex, 0);

					//animator.SetBool ("neutral", false);

				} else if (status.mystatus == Status.status.juggle) {
					animator.SetLayerWeight (juggleLayerIndex, 1);
                if (inHitstop)
                {
                    //animator.Play("jump rising", -1, 0f);
                }

				}

			} else if (status.myPhysicsStatus == Status.physicsStatus.grounded) {
				animator.SetBool ("airbourne", false);
				animator.SetLayerWeight (juggleLayerIndex, 0);

			}

			if (status.currentHealth <= 0 && !status.dead) {
				//status.dead = true;
				//death (gameObject);
				//Death ();
			}


			//Debug.Log (velocity.y);
			//Hitstun();
			//HorizontalMovement();


			/*
		if (hurtboxhit.gotHit) {

			//print ("HIT");
			StartNewMove (hurtboxhit);
			hurtboxhit.gotHit = false;
		}
		*/


			if (status.mystatus == Status.status.juggle && !inHitstop)
				juggleFramesCount++;
			else
				juggleFramesCount = 0;
			//print (juggleFramesCount);


			if (status.myPhysicsStatus == Status.physicsStatus.grounded && status.myPreviousPhysicsStatus == Status.physicsStatus.airborne) {
				//print (jumpDuration);
				jumpDuration = 0;
				if (status.mystatus != Status.status.juggle && !customLanding) {
					//status.mystatus = Status.status.landing;
					StartNewMove (animations [0]);	

				}
			}

			if (status.mystatus == Status.status.juggle && status.myPhysicsStatus == Status.physicsStatus.grounded && juggleFramesCount > 10) {
				//status.mystatus = Status.status.wakeup;
				StartNewMove (animations [1]);

			}

			if (status.myPhysicsStatus == Status.physicsStatus.airborne && status.myPreviousPhysicsStatus == Status.physicsStatus.grounded &&
				status.mystatus != Status.status.juggle) {
				if (currentMove != null)
					currentMove.StopSequence ();
			}

			animator.SetFloat ("vspeed", velocity.y);

			//Hitstun();
			HorizontalMovement();


			//Debug.Log("land");
			//le seguenti funzioni NON implementano le mosse, ma specificano QUANDO eseguirle; nel caso di un giocatore, quando premi il pulsante

			//target
			Target();

			//custom script, per comportamenti non compresi tra le altre funzioni
			Custom();

			//salto
			Jump();

			//attacco
			Attack();

			//moveset.conditions.CheckConditions();

			//Turn off target
			TurnOffTarget();

			//blocco
			Block();

			//scatto
			Dash();






			if (currentMove != null) { //se ha già iniziato una sequenza durante questo frame, allora skippa; altrimenti, esegui la sequence
				if (!skipExecution)
					currentMove.ExecuteSequence ();
				else
					skipExecution = false;
			}


			//movimento 	orizzontale
			//HorizontalMovement();

			//Debug.Log("FLIP?");

			//Debug.Log (seq);




			status.myPreviousStatus = status.mystatus;
			status.myPreviousPhysicsStatus = status.myPhysicsStatus;
			//Debug.Log (status.mystatus);

			//if (currentMove == null)
			//	animator.SetTrigger ("neutraltrigger");


			//Debug.Log(seq);
			//Debug.Log (animator.parameters);

			ResetInputs ();


		}

		public void Flip() {

			facingRight = !facingRight;
			Vector3 myscale = gameObject.transform.localScale;
			myscale.x *= -1;
			gameObject.transform.localScale = myscale;
		}

		protected virtual void HorizontalMovement () {

		}

		protected virtual void Hitstun () {

		}

		protected virtual void Jump () {

		}

		protected virtual void Attack () {

		}

		public void StartNewMove (MoveScript newMove) {


			//print ("HIT");
			if (!skipExecution) {

				if (currentMove != null)
					currentMove.StopSequence ();
				//animator.SetBool ("neutral", true);
				currentMove = newMove;
				//print (newMove);
				//print (currentMove);
				FrameData fd = currentMove.gameObject.GetComponent<FrameData> ();
				if (fd != null)
					fd.facingRight = facingRight;
				seq = 0;
				currentMove.ExecuteSequence ();
				skipExecution = true;
			} 

		}

		public void LoadWeapon () {
			GameObject weapon;
			Component[] comp = GetComponentsInChildren<Transform>();

			int j;
			for (j = 0; j < comp.Length; j++) {
				//if (comp[j].name.StartsWith("Weapon"))
				if (comp[j].name.StartsWith("(WEAPON)"))
					break;
				else continue;
			}
			weapon = comp [j].gameObject;
			//Debug.Log (weapon);
			//GameObject mymoves = transform.Find ("Moves").gameObject;
			//moves = weapon.GetComponent<MoveSet> ().moves.Concat(mymoves.GetComponent<MoveSet>().moves).ToArray();
			moves = weapon.GetComponent<MoveSet> ().moves;
			//Debug.Log (moves);

			moveset = weapon.GetComponent<MoveSet> ();
			conditions = weapon.GetComponent<ActivationConditions> ();

			LoadMoves ();

			for (int i = 0; i < moves.Length; i++) {
				//moves [i].Initialize ();
				//print (i);
			}

			conditions.LoadMoveInfo ();

			print ("Weapon Loaded");
			//Debug.Log (moveset);

		}

		public void LoadMoves () {
			GameObject mymoves = transform.Find ("Animations").gameObject;
			//GameObject mymoves = transform.Find ("Moves").gameObject;
			//moves = weapon.GetComponent<MoveSet> ().moves.Concat(mymoves.GetComponent<MoveSet>().moves).ToArray();

			animations = mymoves.GetComponent<MoveSet> ().moves;

			for (int i = 0; i < animations.Length; i++) {
			
				//animations [i].Initialize ();


			}
		for (int i = 0; i < hurtboxes.Length; i++) {
			hurtboxes[i].Initialize ();
		}
		}

		public void ChangeParameter (string param, bool value) {
			animParameter = param;
			//animator.SetBool (animParameter, value);
		}

		public void ChangeParameter (string param) {
			animParameter = param;
			animator.SetTrigger (animParameter);
		}

		protected virtual void Attack2 () {

		}

		protected virtual void Kick () {

		}

		protected virtual void Block () {

		}

		protected virtual void Target () {

		}

		protected virtual void TurnOffTarget () {

		}

		protected virtual void Custom () {

		}

		protected virtual void Dash () {

		}

		protected virtual void Death () {

		}

	}

