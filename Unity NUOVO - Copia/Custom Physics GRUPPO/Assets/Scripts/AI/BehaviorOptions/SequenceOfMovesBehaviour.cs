﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequenceOfMovesBehaviour : BehaviorOption {

	[System.Serializable]
	public struct moveInSequence
	{
		public int bufferNumber; //indice della mossa da eseguire
		public int startFrame; //frame in cui deve partire la mossa
	}

	public moveInSequence[] sequenceOfMoves;
	public override void MyExecuteBehaviour ()
	{
		for (int i = 0; i < sequenceOfMoves.Length; i++) {
			if (bSelector.bseq == sequenceOfMoves [i].startFrame) {
				controller.enemyBuffers[sequenceOfMoves[i].bufferNumber] = controller.bufferLength;
			}

		}
	}
}
