﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivationConditions : MoveScript {

	//struct con mossa e flag "usable", da settare dal MoveScript per i cancel
	[System.Serializable]
	public struct moveInfo {
		public MoveScript move;
		public bool usable;
	}

	public WeaponBounce weaponBounceAnimation;
	public moveInfo[] moves, animations, specialMoves;

	public bool parry = false;
    public int specialMoveUsable = 0;
    public int specialMoveBuffDuration = 600;

    public int cantChangeStamina = 0;
	/*
	void Start () {


        cantChangeStamina = 0;
		controller = GetComponentInParent<CharacterContainer> ().controller;
		//print (controller);

		status = controller.status;
		//LoadMoveInfo();


	}
	*/
	public virtual void CheckConditions () {
		
	}

    protected virtual void CustomCheckConditions ()
    {

    }

	public bool CheckUsability(moveInfo move) {
		

		//if (move.usable)
			//return true;

		

		bool conditionsMet = false;

		for (int i = 0; i < move.move.statusConditions.Length; i++) {
			if (status.mystatus != move.move.statusConditions [i])
				continue;
			else {
				for (int j = 0; j < move.move.physicsStatusConditions.Length; j++) {
					if (status.myPhysicsStatus != move.move.physicsStatusConditions [j])
						continue;
					else {

						if (move.move.physicsStatusConditions[j] == Status.physicsStatus.airborne
							&& ((move.move.jumpHeightLimitation && move.move.minimumAerialHeight > controller.currentGroundDistance)
                            || (move.move.minimumAerialDuration > controller.jumpDuration)))
						{
							//print("NOOOOO");
							continue;
						}

						//print (status.mystatus);
						//return true;
						conditionsMet = true;
					}
				}
			}

		}

		if (move.move.customUsabilityConditions.Length > 0)
		{
			for (int i = 0; i < move.move.customUsabilityConditions.Length; i++)
			{
				if (!move.move.customUsabilityConditions[i].CheckMyUsabilityConditions())
				{
					if (move.move.customUsabilityConditions[i].logic == CustomUsabilityConditions.LogicOption.and)
					{
						//print("itsfallseeeeee");
						return false;
					}
					
				}
				else
                {
					if (move.move.customUsabilityConditions[i].logic == CustomUsabilityConditions.LogicOption.or)
                    {
						conditionsMet = true;
						//print("itstrueeeee");
                    }

				}
			}
		}


		if (!conditionsMet)
		{
			//if (move.move.statusConditions.Length == 0 || move.move.physicsStatusConditions.Length == 0)
			{
				if (!move.usable)
					return false;
			}
		}
		//print ("FALSE");
			//return false;

		//if (!status.usingDoubleStaminaSystem)
		{
			//if (move.move.requiresStamina && move.move.move != null && status.currentStamina <= 0) {
			if (move.move.requiresStamina && status.currentStamina <= 0)
			{
				//controller.FlushBuffers();
				//print("not usable: " + move.move);
				return false;
			}
		}
		
		if (move.move.requiresMeter && status.currentMeter < Mathf.RoundToInt(status.maxMeter / move.move.meterCostFraction))
			return false;



		return true;


	}

	public void ResetUsable () {
		for (int i = 0; i < moves.Length; i++) {
			moves [i].usable = false;
		}

		for (int i = 0; i < animations.Length; i++) {
			animations [i].usable = false;
		}
	}
		

	public virtual void LoadMoveInfo () {
		//controller = transform.parent.transform.parent.GetComponent<Player> ();


		MoveScript [] moveset = controller.moves;
		MoveScript[] animationsSet = controller.animations;
		
		//MoveScript[] movescripts = moveset.moves;

		
		if (controller.specialMoves != null)
		{
			MoveScript[] specialMoveSet = controller.specialMoves.moves;
			specialMoves = new moveInfo[specialMoveSet.Length];
			for (int i = 0; i < specialMoves.Length; i++)
			{
				specialMoves[i].usable = false;
				specialMoves[i].move = specialMoveSet[i];
			}
		}
		
		//print (animationsSet.Length);
		moves = new moveInfo[moveset.Length];


		for (int i = 0; i < moveset.Length; i++) {
			moves [i].usable = false;
			moves [i].move = moveset[i];

		}

		
		animations = new moveInfo[animationsSet.Length];

		for (int i = 0; i < animations.Length; i++) {
			animations [i].usable = false;
			animations [i].move = animationsSet [i];
		}
		
	}
}
