﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatGaugeBarScaleController : BarScaleController
{
    public HeatGaugeAnimator heatGaugeAnimator;
    int previousHeat;
    int previousHeatLevel;
    public override void BarInitialization()
    {
        base.BarInitialization();
        if (heatGaugeAnimator == null)
            return;
        heatGaugeAnimator.Init();

        if (status.heatLevel == 0)
            heatGaugeAnimator.SetHeat(HeatGaugeAnimator.Heat.lowHeat);
        else if (status.heatLevel == 1)
        {
            heatGaugeAnimator.SetHeat(HeatGaugeAnimator.Heat.mediumHeat);
        }
        else
        {
            heatGaugeAnimator.SetHeat(HeatGaugeAnimator.Heat.highHeat);
        }
    }

    protected override void ManageBars()
    {
        if (heatGaugeAnimator == null)
            return;
        if (status.currentHeatAmount > previousHeat)
        {
            heatGaugeAnimator.StartHeatIncreaseAnimation();
        }

        if (status.heatLevel != previousHeatLevel)
        {
            if (status.heatLevel == 0)
                heatGaugeAnimator.SetHeat(HeatGaugeAnimator.Heat.lowHeat);
            else if (status.heatLevel == 1)
            {
                heatGaugeAnimator.SetHeat(HeatGaugeAnimator.Heat.mediumHeat);
            }
            else
            {
                heatGaugeAnimator.SetHeat(HeatGaugeAnimator.Heat.highHeat);
            }
        }

        previousHeat = status.currentHeatAmount;
        previousHeatLevel = status.heatLevel;
    }

}
