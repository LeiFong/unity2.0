﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPlayer : Player {

	public BehaviourSelector bSelector;
	//public BehaviorOption currentBehaviour;

	public override void LoadInputs ()
	{
		bSelector = GetComponentInChildren<BehaviourSelector> ();
		bSelector.controller = this;
		bSelector.InitializeBehaviours ();

		for (int i = 0; i < moves.Length; i++) {
			enemyBuffers.Add (0);
		}
	}

	protected override void InputCheck ()
	{
		bSelector.BehaviourSelect ();
		if (bSelector.currentBehaviour != null)
		bSelector.currentBehaviour.ExecuteBehaviour ();
	}
}

