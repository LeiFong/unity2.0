﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    int c = 0;
    float t = 0;
    public float defaultTimeScale = 1;
    public float currentTimeScale = 1;
    private float previousTimeScale = 1;
    //float originalFixedDeltaTime;
    //bool slowtimeRequestToUpdate;
    float temporaryTimeScale = 1;
    private bool alterTimeRequestPending = false;

    
    private void Awake()
    {
        //originalFixedDeltaTime = Time.fixedDeltaTime;
        TimeVariables.defaultTimeScale = defaultTimeScale;
        TimeVariables.stopWatchFrames = -1;
    }
    /*
    public void AlterTimeRequest (float requestedTimeScale)
    {
        alterTimeRequestPending = true;
        temporaryTimeScale = requestedTimeScale;
    }
   */

    public void AlterTimeRequest(int requestedTimeScale)
    {
        alterTimeRequestPending = true;
        temporaryTimeScale = requestedTimeScale;
        TimeVariables.currentTimeScale = requestedTimeScale;
        //print("request: " + requestedTimeScale + " " + Time.frameCount);
        //TimeVariables.canExecuteLogic = false;
        //TimeVariables.currentTimeScale = 1 / requestedTimeScale;
    }

    private void FixedUpdate()
    {
        TimeVariables.totalFramesSinceStart++;
        if (TimeVariables.canExecuteLogic)
        {
            if (alterTimeRequestPending)
            {
                //print("request pending " + Time.frameCount);
                currentTimeScale = temporaryTimeScale;
                TimeVariables.currentTimeScale = currentTimeScale;
                alterTimeRequestPending = false;
            }
            else
            {
                //print("request NOT pending " + Time.frameCount);
                currentTimeScale = defaultTimeScale;
                TimeVariables.currentTimeScale = defaultTimeScale;
            }
        }

        if (currentTimeScale != defaultTimeScale)
        {
            //print("TIME CHANGED " + Time.frameCount);
            TimeVariables.timeHasChanged = true;
        }
        else
        {
            TimeVariables.timeHasChanged = false;
            //print("TIME HAS NOT CHANGEDDDDDDDDDDD " + Time.frameCount);
        }

        if (c >= currentTimeScale - 1)
        {
            c = 0;
            TimeVariables.canExecuteLogic = true;
            TimeVariables.logicFramesSinceStart++;

            //print("CAN EXECUTE " + Time.frameCount);
            if (TimeVariables.stopWatchFrames != -1)
            {
                TimeVariables.stopWatchFrames++;
            }
        }
        else
        {
            c++;
            
            TimeVariables.canExecuteLogic = false;
        }

        previousTimeScale = currentTimeScale;

    }

    


    /* vecchia implementazione, non cancellare per sicurezza
    void FixedUpdate()
    {
        c = 0;

        //print("TIMEEEE");
        
       
        
        t = Time.realtimeSinceStartup;

        if (TimeVariables.canExecuteLogic)
        {
            if (alterTimeRequestPending)
            {
                
                TimeVariables.canExecuteLogic = false;
                slowtimeRequestToUpdate = true;

                
            }
            else
            {

                if (Time.timeScale != defaultTimeScale)
                {
                    slowtimeRequestToUpdate = false;
                    
                }
                
            }

            alterTimeRequestPending = false;
        }
        else
        {
            //print("ciao");
        }
    }
    
    void Update()
    {
        
        
        if (slowtimeRequestToUpdate)
        {
            
            //print("T = " + (Time.realtimeSinceStartup - t));
            Time.timeScale = temporaryTimeScale;
            //Time.fixedDeltaTime = originalFixedDeltaTime * (temporaryTimeScale / defaultTimeScale);            
            //alterTimeRequestPending = false;
            //print("NEW TIMEEEEE ma che ooooh " + c);
            c++;
        }
        else
        {
            //if (Time.timeScale != defaultTimeScale)
              //  print("T = " + (Time.realtimeSinceStartup - t));
            Time.timeScale = defaultTimeScale;
            

            //Time.fixedDeltaTime = originalFixedDeltaTime;
        }
        TimeVariables.canExecuteLogic = true;
        
    }
    */
}

public static class TimeVariables
{
    public static bool canExecuteLogic = true;
    public static bool timeHasChanged;
    public static float defaultTimeScale;
    public static float currentTimeScale = 1;
    public static int logicFramesSinceStart = 0;
    public static int totalFramesSinceStart = 0;
    public static int stopWatchFrames = -1;


    public static void StopWatch()
    {
        if (stopWatchFrames == -1)
        {
            stopWatchFrames = 0;
        }
        else
        {
            Debug.Log("STOPWATCH: " + stopWatchFrames);
            stopWatchFrames = -1;
        }
    }
}
