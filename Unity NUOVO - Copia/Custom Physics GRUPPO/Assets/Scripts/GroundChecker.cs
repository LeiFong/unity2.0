﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChecker : MonoBehaviour {

	protected Rigidbody2D rb;
	protected ContactFilter2D contactFilter;
	protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
	protected List<RaycastHit2D> hitbufferList = new List<RaycastHit2D> (16);
	public float shellRadius = 0.1f;


	void OnEnable () {
		rb = GetComponent<Rigidbody2D> ();
		contactFilter.useTriggers = false;
		contactFilter.SetLayerMask (Physics2D.GetLayerCollisionMask (gameObject.layer));
		contactFilter.useLayerMask = true;
	}
	public bool GroundCheck () {


		int count = rb.Cast (Vector2.down, contactFilter, hitBuffer, shellRadius);


		return true;
	}
}
