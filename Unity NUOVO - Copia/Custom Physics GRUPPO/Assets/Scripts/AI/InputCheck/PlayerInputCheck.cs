﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerInputCheck : InputCheck {

    protected int atk1miniBuffer, atk2miniBuffer;
    public int simultaneusPressLeniency = 5; //tolleranza in frame per comandi che prevedono due tasti premuti contemporaneamente
	public override void SetBuffers ()
	{
		if (Input.GetAxisRaw (player.verticalmovementCMD) < 0)
			player.vertical = -1;
		if (Input.GetAxisRaw (player.verticalmovementCMD) > 0)
			player.vertical = 1;
		if (Input.GetAxisRaw (player.verticalmovementCMD) == 0)
			player.vertical = 0;

		if (Input.GetAxisRaw (player.horizontalmovementCMD) < 0)
			player.horizontal = -0.5f;
        if (Input.GetAxisRaw(player.horizontalmovementCMD) < -0.5)
            player.horizontal = -1f;
        if (Input.GetAxisRaw (player.horizontalmovementCMD) > 0)
			player.horizontal = 0.5f;
        if (Input.GetAxisRaw(player.horizontalmovementCMD) > 0.5)
            player.horizontal = 1f;
        if (Input.GetAxisRaw (player.horizontalmovementCMD) == 0)
			player.horizontal = 0;

        //provvisorio, da togliere
        //if (Input.GetAxisRaw(player.horizontalmovementCMD) != 0 && player.vertical == 1)
          //  player.horizontal = player.horizontal / 2;




        if (Input.GetAxisRaw (player.verticalmovementCMD) > 0 && player.verticalprevious == 0) {
			player.upPressed = true;
		} else
			player.upPressed = false;

		if (Input.GetAxisRaw (player.verticalmovementCMD) < 0 && player.verticalprevious == 0) {
			player.downpressed = true;
		} else
			player.downpressed = false;

		if (Input.GetAxisRaw (player.horizontalmovementCMD) > 0 && player.horizontalprevious == 0) {
			player.rightpressed = true;
		} else
			player.rightpressed = false;

		if (Input.GetAxisRaw (player.horizontalmovementCMD) < 0 && player.horizontalprevious == 0) {
			player.leftpressed = true;
		} else
			player.leftpressed = false;



		if (Input.GetAxisRaw (player.verticalmovementCMD) == 0 && player.verticalprevious == 1) {
			player.upreleased = true;
		} else
			player.upreleased = false;

		if (Input.GetAxisRaw (player.verticalmovementCMD) == 0 && player.verticalprevious == -1) {
			player.downreleased = true;
		} else
			player.downreleased = false;

		if (Input.GetAxisRaw (player.horizontalmovementCMD) == 0 && player.horizontalprevious == 1) {
			player.rightreleased = true;
		} else
			player.rightreleased = false;

		if (Input.GetAxisRaw (player.horizontalmovementCMD) == 0 && player.horizontalprevious == -1) {
			player.leftreleased = true;
		} else
			player.leftreleased = false;

		player.verticalprevious = player.vertical;
		player.horizontalprevious = player.horizontal;




		if (Input.GetButtonDown (player.dashCMD)) {
			//FlushBuffers ();
			player.dashpressed = true;
			//if (player.status.myPhysicsStatus == Status.physicsStatus.airborne || player.currentMove == player.animations[4])
				//player.dashBuffer = player.bufferLength;

				//if (runToggleTimer < timeToRunToggle) 
				
					
					//runToggleTimer = 0;
		}

		if (Input.GetButton (player.dashCMD)) {
			//if (player.status.mystatus == Status.status.neutral && player.status.myPhysicsStatus == Status.physicsStatus.grounded && player.horizontal != 0)
			//player.runToggleTimer++;
			//else
			//	player.runToggleTimer = 0;
			//if (!player.gachiBass)
			//	print ("frame " + Time.frameCount + "timer: " + player.runToggleTimer);


			player.holdingDash = true;
		
			//if (player.runToggleTimer > player.timeToRunToggle) {
			//	player.runToggle = true;

			//}
			//player.dashBuffer = player.bufferLength;
		} else
			player.holdingDash = false;

		if (Input.GetButtonDown (player.targetCMD)) {
			//FlushBuffers ();
			player.targetpressed = true;
			player.targetBuffer = player.bufferLength;
		}
		if (Input.GetButtonDown (player.atk1CMD)) {
			//FlushBuffers ();
			player.atk1pressed = true;
			player.atk1Buffer = player.bufferLength;
            atk1miniBuffer = simultaneusPressLeniency;
		}
		if (Input.GetButtonDown (player.special1CMD)) {
			//FlushBuffers ();
			player.special1pressed = true;
			player.special1Buffer = player.bufferLength;
		}
		if (Input.GetButtonDown(player.special2CMD))
		{
			//FlushBuffers ();
			player.special2pressed = true;
			player.special2Buffer = player.bufferLength;
		}
		if (Input.GetButtonDown(player.special3CMD))
		{
			//FlushBuffers ();
			player.special3pressed = true;
			player.special3Buffer = player.bufferLength;
		}
		if (Input.GetButtonDown(player.special4CMD))
		{
			//FlushBuffers ();
			player.special4pressed = true;
			player.special4Buffer = player.bufferLength;
		}
		if (Input.GetButtonDown (player.atk2CMD)) {
			//FlushBuffers ();
			player.atk2pressed = true;
			player.atk2Buffer = player.bufferLength;
            atk2miniBuffer = simultaneusPressLeniency;
		}
		if (Input.GetButtonDown (player.blockCMD)) {
			//FlushBuffers ();
			player.blockpressed = true;
			player.blockBuffer = player.bufferLength;
		}
		if (Input.GetButtonDown (player.jumpCMD)) {
			//FlushBuffers ();
			player.jumppressed = true;
			player.jumpBuffer = player.bufferLength;
		}



		if (Input.GetButtonUp (player.atk1CMD)) {
			player.atk1released = true;
		}


		if (Input.GetButtonUp (player.atk2CMD)) {
			player.atk2released = true;
		}

		if (Input.GetButtonUp (player.special1CMD)) {
			player.special1released = true;
		}

		if (Input.GetButtonUp(player.special2CMD))
		{
			player.special2released = true;
		}

		if (Input.GetButtonUp(player.special3CMD))
		{
			player.special3released = true;
		}

		if (Input.GetButtonUp(player.special4CMD))
		{
			player.special4released = true;
		}

		if (Input.GetButtonUp (player.targetCMD)) {
			player.targetreleased = true;
		}

		if (Input.GetButtonUp (player.dashCMD)) {
			player.dashreleased = true;
			//player.dashBuffer = player.bufferLength;
			//if (player.dashreleased)
			{

                if (player.framesHoldingDash > player.timeToRunToggle)
                {
                    player.dashBuffer = 0;
                    //print("ZEro");
                }
                else //if (player.status.myPhysicsStatus == Status.physicsStatus.grounded)
                    player.dashBuffer = player.bufferLength;
				
				player.runToggleTimer = 0;
			}
		}


		if (player.runToggleTimer <= player.timeToRunToggle || player.horizontal == 0)
			player.runToggle = false;

        if (atk1miniBuffer > 0 && atk2miniBuffer > 0)
        {
            player.special2Buffer = player.bufferLength;
            atk1miniBuffer = 0;
            atk2miniBuffer = 0;
            player.atk1Buffer = 0;
            player.atk2Buffer = 0;
        }

        if (atk2miniBuffer > 0)
            atk2miniBuffer--;
        if (atk1miniBuffer > 0)
            atk1miniBuffer--;


		if (Input.GetButtonUp (player.blockCMD)) {
			player.blockreleased = true;
		}


		if (Input.GetButtonUp (player.jumpCMD)) {
			player.jumpreleased = true;
		}

	}

}
