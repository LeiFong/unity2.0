﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

	public KeyCode horizontal, horizontal_JS;
	public KeyCode vertical, vertical_JS;
	public KeyCode attack1, attack1_JS;
	public KeyCode attack2, attack2_JS;
	public KeyCode dash, dash_JS;
	public KeyCode jump, jump_JS;
	public KeyCode special, special_JS;
	public KeyCode action, action_JS;
	public KeyCode start, start_JS;
	public KeyCode back, back_JS;
	public KeyCode target, target_JS;
	public KeyCode block, block_JS;


}
