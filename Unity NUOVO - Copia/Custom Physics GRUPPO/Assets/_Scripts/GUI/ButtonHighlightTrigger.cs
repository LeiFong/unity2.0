﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonHighlightTrigger : MonoBehaviour, ISelectHandler
{
    public EventTrigger.TriggerEvent onSelect;

    public void OnSelect(BaseEventData b) {

        onSelect.Invoke(b);
    }
}
