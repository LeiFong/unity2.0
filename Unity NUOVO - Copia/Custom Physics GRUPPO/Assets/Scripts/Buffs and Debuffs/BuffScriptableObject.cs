﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BuffScriptableObject : ScriptableObject
{
    public string buffName;
    public int index;
    public Sprite sprite;

    public int buffTotalDuration;
    public int maxStacks = 1;

}
