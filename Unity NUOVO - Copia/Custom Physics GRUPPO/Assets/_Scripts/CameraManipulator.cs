﻿using UnityEngine;
using System.Collections;

public class CameraManipulator : MonoBehaviour {

    [Tooltip("The offset of the manipulation.")]
    public Vector2 offset = new Vector2(0, 0);
    [Tooltip("The transform of the object to be followed by the camera.")]
    public Transform center;
    [Tooltip("In a center-based manipulation, it indicates how precisely the camera will follow the new object; value from 0 to 1.")]    
    public Vector2 strength;
    [Tooltip("How much you want to add to the zoom level of the camera.")]
    public float zoomOffset = 0f;
    [Tooltip("The priority of the manipulator; minimum 0; you can set the maximum in the CameraMovement component." +
        " Higher means higher priority. All the changes made by this manipulator will" +
        " be added to the other manipulators of the same priority." +
        " Also, all active manipulators of lower priority will be ignored.")]
    [Range(0, 5)]
    public int priority = 0;
    [Tooltip("The time (in frames) required by the camera to reach the new offset when entering the manipulator.")]
    public int reachTime = 60;
    [Tooltip("The time (in frames) required by the camera to release the offset when exiting the manipulator.")]
    public int releaseTime = 60;
    [Tooltip("The ratio of time over which the manipulator starts slowing down when reaching the destination; value from 0 to 1." +
        "This means that if it's =1, there is no slowing down. If it's =0.6, it will slow down in the last 40% of the Reach Time.")]
    [Range(0f, 1f)]
    public float slowDownThreshold = 0.6f;

    private CameraMovement cameraMovement;

    private void Start()
    {
        cameraMovement = CameraMovement.Instance;
    }

    public void SetFollowedObject(Transform t) {
        center = t;
    }

    public void AddMyselfToCameraMovement()
    {
        if (cameraMovement != null)
            cameraMovement.AddManipulator(this);
    }

    public void RemoveMyselfToCameraMovement()
    {
        if (cameraMovement != null)
            cameraMovement.RemoveManipulator(this);
    }

}
