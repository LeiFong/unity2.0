﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeGlowOnHit : OnHitProperties
{
    public int eyeGlowFrame = 60;
    bool activated;
    public Animator eyeAnimator;

    public override void Initialize()
    {
        base.Initialize();

        PlayerAnimatorRetriever ret = move.controller.animatorRetriever;
        if (ret != null)
        {
            eyeAnimator = ret.eyeEffects;
        }
    }
    public override void OnHitApply()
    {
        activated = true;
        
    }

    public override void OnHitStart()
    {
        base.OnHitStart();
        activated = false;
    }
    private void FixedUpdate()
    {
        if (activated)
        {
            if (move.controller.currentMove != move)
            {
                activated = false;
                return;
            }
            
            if (move.controller.seq == eyeGlowFrame)
            {
                
                if (move.status.currentStance == Status.Stances.fast)
                {
                    eyeAnimator.Play("Blue", 1);
                }
                else
                {
                    eyeAnimator.Play("Red", 1);
                }
                eyeAnimator.Play("big glow", 0, 0f);

                activated = false;
            }
        }
    }
}
