﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WeaponDamageCalculator
{
    
    private static float scalingCoefficient = 0.1f;
    private static float baseDamageCoefficient = 0.6f;
    private static float defaultRecoveryDamageBonus = 0.5f;
    public static float globalCoefficient = 0.02f;
    private static int result;

    public static int CalculateWeaponDamage(float baseDamage, Controller2D victimController, Controller2D attackerController)
    {
        float newDamage = baseDamage;

        if (attackerController != null)
        {
            float weaponScalingMultiplier = scalingCoefficient *
                (attackerController.moveset.weaponScaling[attackerController.moveset.currentWeaponUpgrade].strength
                    * attackerController.status.playerStats.currentStats.strength
                + attackerController.moveset.weaponScaling[attackerController.moveset.currentWeaponUpgrade].dexterity
                    * attackerController.status.playerStats.currentStats.dexterity
                + attackerController.moveset.weaponScaling[attackerController.moveset.currentWeaponUpgrade].synthesis
                    * attackerController.status.playerStats.currentStats.synthesis);

            float baseDamageMultiplier = (attackerController.moveset.weaponScaling[attackerController.moveset.currentWeaponUpgrade].baseWeaponDamage) * baseDamageCoefficient;

            newDamage = (float)(baseDamage *
                (baseDamageMultiplier * (1 + weaponScalingMultiplier)));

            
        }

        //if (victimController.recoveryCounter > 0)
          //  newDamage += (attackerController != null ? attackerController.status.recoveryDamageBonus : defaultRecoveryDamageBonus) * newDamage;

        //Debug.Log("newdamage: " + (weaponScalingMultiplier));

        newDamage -= (victimController.status.defensePercentage * newDamage / 100);


        newDamage *= globalCoefficient;// / 100;

        result = Mathf.RoundToInt(newDamage);
        //Debug.Log("DAMAGE: " + result);

        return result;
    }
}
