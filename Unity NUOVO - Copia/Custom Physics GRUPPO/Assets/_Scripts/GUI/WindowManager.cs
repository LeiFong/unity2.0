﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowManager : MonoBehaviour
{
    private UIManager uiManager;
    private void Start()
    {
        uiManager = UIManager.Instance ?? FindObjectOfType<UIManager>();
        uiManager.AddWindowManagerToList(this);
    }

}
