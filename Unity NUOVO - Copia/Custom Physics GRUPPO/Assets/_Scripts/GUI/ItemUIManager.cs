﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ItemUIManager : MonoBehaviour
{
    public Inventory.ItemType itemType = Inventory.ItemType.consumable;
    public Text itemName, quantity;
    public Text upperDescriptionText, lowerDescriptionText;
    public ItemUIOptions itemUiOptions;
    private ItemInfo myItem;
    private UIManager uiManager;
    private Button button;
    private int id;
    private Inventory inventory;
    private InventoryItemFiller inventoryFiller;
    private Animator animator;
    private RectTransform rect;
    ItemDatabase itemDatabase;

    public struct ItemInfo
    {
        public Item item;
        public ItemWrapper itemWrapper;
        public int quantity;
    }

    private void Start()
    {
        uiManager = UIManager.Instance ?? FindObjectOfType<UIManager>();
        button = GetComponent<Button>();
        animator = GetComponent<Animator>();
        animator.keepAnimatorControllerStateOnDisable = true;
        rect = GetComponent<RectTransform>();
        itemDatabase = FindObjectOfType<ItemDatabase>();
    }

    public void SetReferenceItem(Item item, int quantity) {

        myItem.itemWrapper = itemDatabase.GetItemFromDatabase(item);
        myItem.item = item;
        myItem.quantity = quantity;
    }

    public void SetButtonAnimationAsHighlighted() {
        if (animator != null)
        {
            animator.SetTrigger("Highlighted");
            animator.ResetTrigger("Disabled");
        }

    }

    public void SetID(int i) {
        id = i;
    }
    public void SetInventory(Inventory i) {
        inventory = i;
    }

    public void SetItemNameText(string text)
    {
        itemName.text = text;
    }
    public void SetItemNameText()
    {
        SetItemNameText(GetItemName());
    }

    public string GetItemName() {
        string ret;
        switch (uiManager.language)
        {
            case UIManager.Language.english:
                if (myItem.itemWrapper.itemName.textEnglish == "")
                    print("English text for field Item Name not set for " + myItem.itemWrapper.name);
                ret = myItem.itemWrapper.itemName.textEnglish;
                break;
            case UIManager.Language.italian:
                if (myItem.itemWrapper.itemName.textItalian == "")
                    print("Italian text for field Item Name not set for " + myItem.itemWrapper.name);
                ret = myItem.itemWrapper.itemName.textItalian;
                break;
            default:
                ret = myItem.itemWrapper.itemName.textEnglish;
                break;
        }
        return ret;
    }

    public void SetQuantityText(int qty)
    {
        quantity.text = "x" + qty.ToString();
    }
    public void SetQuantityText()
    {
        SetQuantityText(myItem.quantity);
    }

    public void SetUpperDescriptionText(string text) {
        upperDescriptionText.text = text;
    }
    public void SetUpperDescriptionText()
    {       
        switch (uiManager.language)
        {
            case UIManager.Language.english:
                print("myitem: " + myItem.itemWrapper);
                if (myItem.itemWrapper.description.textEnglish == "")
                    print("English text for field Description not set for " + myItem.itemWrapper.name);
                SetUpperDescriptionText(myItem.itemWrapper.description.textEnglish);
                break;
            case UIManager.Language.italian:
                if (myItem.itemWrapper.description.textItalian == "")
                    print("Italian text for field Description not set for " + myItem.itemWrapper.name);
                SetUpperDescriptionText(myItem.itemWrapper.description.textItalian);
                break;
            default:
                SetUpperDescriptionText(myItem.itemWrapper.description.textEnglish);
                break;
        }
    }

    public void SetLowerDescriptionText(string text)
    {
        lowerDescriptionText.text = text;
    }

    public void SetLowerDescriptionText()
    {
        switch (uiManager.language)
        {
            case UIManager.Language.english:
                if (myItem.itemWrapper.flavorText.textEnglish == "")
                    print("English text for field Flavor Text not set for " + myItem.itemWrapper.name);
                SetLowerDescriptionText(myItem.itemWrapper.flavorText.textEnglish);
                break;
            case UIManager.Language.italian:
                if (myItem.itemWrapper.flavorText.textItalian == "")
                    print("Italian text for field Flavor Text not set for " + myItem.itemWrapper.name);
                SetLowerDescriptionText(myItem.itemWrapper.flavorText.textItalian);
                break;
            default:
                SetLowerDescriptionText(myItem.itemWrapper.flavorText.textEnglish);
                break;
        }
    }

    public void UseItem() {
        inventory.UseItem(id, 1, itemType);
    }

    public void UseItem(int quantity) {
        inventory.UseItem(id, quantity, itemType);
    }

    public int GetID() {
        return id;
    }
    public ItemInfo GetReferenceItem() {
        return myItem;
    }

    public InventoryItemFiller GetInventoryFiller() {
        return inventoryFiller;
    }

    public void SetInventoryFiller(InventoryItemFiller filler) {
        inventoryFiller = filler;
    }

    public Button GetButton() {
        return button;
    }

    public float GetVerticalPosition() {

        return rect.position.y;
    }
}
