﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event : MonoBehaviour, IPreAwake
{
    public enum PromptType { action, dialogue, armorLoot, weaponLoot, consumableLoot, materialLoot, keyItemLoot } //serve per cambiare il colore del prompttext a seconda del tipo di evento
    public PromptType promptType = PromptType.action;
    public MultiLanguageText promptText;
    public bool playerOnlyEvent = true; //se l'evento interagisce solo con il giocatore o altri personaggi
    public bool deactivatesOnEventTrigger; //se l'evento si disattiva una volta triggerato
    public bool serializableEvent; //se l'evento deve essere salvato su file
    public bool resetsOnLoad; //se l'evento può essere triggerato di nuovo dopo un Load
    public bool requestPrompt; //se l'evento ha bisogno di un prompt di conferma prima di essere triggerato
    public string eventID;
    public Player controller;
    public PlayersManager playersManager;
    public Collider2D myCollider, mySecondCollider, playerCollider;
    public ContactFilter2D cf;
    public bool eventHappened = false;
    RaycastHit2D[] hitBuffer = new RaycastHit2D[150];
    public bool dudeIsInside;
    bool dudeAlreadyOutside;
    [HideInInspector]
    public BackgroundAwaker myBackgroundAwaker;

    public bool PreAwake(int preAwakeIndex, BackgroundAwaker backgroundAwaker)
    {
        myBackgroundAwaker = backgroundAwaker;
        if (serializableEvent)
        {
            //print("event init");
            GenerateEventID();
        }
        return true;
    }
    void Start ()
    {
        InitializeEvent();
        cf.useLayerMask = true;
        cf.useTriggers = true;
        cf.layerMask &= 0;

        if (playerOnlyEvent)
        {
            cf.layerMask |= 1 << LayerMask.NameToLayer("Player");
        }
        else
        {
            cf.layerMask |= 1 << LayerMask.NameToLayer("TargetableObhect"); //NON E' UN TYPO 

        }
        //print("awake: " + gameObject +" " + gameObject.activeSelf);

        myCollider = GetComponent<Collider2D>();
        if (myCollider != null)
            myCollider.isTrigger = true;
        if (mySecondCollider != null)
            mySecondCollider.isTrigger = true;


        //playersManager = FindObjectOfType<PlayersManager>();

        if (playersManager == null)
        {
            if (PlayersManager.Instance == null)
            {
                playersManager = FindObjectOfType<PlayersManager>();
                if (playersManager == null)
                {
                    print("NO PLAYERSMANAGER");
                    //return;
                }
                //print("find");
            }
            else
            {
                playersManager = PlayersManager.Instance;
                //print("instance");
            }
        }

        
        dudeIsInside = false;
        
    }

    public void GenerateEventID()
    {
        eventID = gameObject.name + " " + transform.position.x + " " + transform.position.y + " " + transform.position.z;
        playersManager.gameManager.InitializeEvent(this, myBackgroundAwaker);
    }


public virtual void ActivateEvent ()
    {
        //print("activate");
        dudeIsInside = false;
        dudeAlreadyOutside = false;
        eventHappened = false;
        gameObject.SetActive(true);
        if (myCollider != null)
        {
            myCollider.enabled = true;
        }
    }

    public virtual void DeactivateEvent()
    {
        eventHappened = true;
        dudeIsInside = false;
        dudeAlreadyOutside = true;
        if (requestPrompt)
            DisablePrompt();
        if (myCollider != null)
            myCollider.enabled = false;
    }

    protected virtual void MyResetEvent ()
    {

    }

    public virtual MultiLanguageText GetPromptText() {
        if (promptText != null)
            return promptText;

        return null;
    }

    protected virtual void MyUpdate()
    {

    }

    private void Update()
    {
        if (!playersManager.doneLoading || (myBackgroundAwaker == null || myBackgroundAwaker.mySceneLoader.activated))
        {
            if (myCollider == null)
                return;
            if (mySecondCollider == null) 
                CheckEventCollision();
            else
            {
                DoubleColliderCheckEventCollision();
            }
            MyUpdate();
        }
    }

    protected void DoubleColliderCheckEventCollision ()
    {
        if (playerOnlyEvent && controller == null)
        {

            controller = playersManager.gameManager.player;
            //playerCollider = controller.horizontalExitCollider;
            if (controller != null)
                playerCollider = controller.targetList.myTargetableCollider;
            return;
        }

        if (!eventHappened)
        {
            //dudeIsInside = false;
            
            int count = myCollider.Cast(Vector2.zero, cf, hitBuffer);

            if (count > 0)
            
            {
                dudeAlreadyOutside = false;
                
                //print("Im in");

                if (!dudeIsInside)
                {
                    OnPlayerIsInside();
                    dudeIsInside = true;
                }
            }
            
            if (dudeIsInside)
            {
                count = mySecondCollider.Cast(Vector2.zero, cf, hitBuffer);
                if (count == 0)
                    dudeIsInside = false;
                
            }

            

        }

        if (!dudeIsInside)
        {
            if (!dudeAlreadyOutside)
            {
                //print("aight im out");
                OnPlayerIsOutside();
                dudeAlreadyOutside = true;
            }
        }


    }

    protected void CheckEventCollision () {

        //if (playerOnlyEvent)
        {
            if (playerOnlyEvent && controller == null)
            {

                controller = playersManager.gameManager.player;
                //playerCollider = controller.horizontalExitCollider;
                if (controller != null)
                    playerCollider = controller.targetList.myTargetableCollider;
                return;
            }

            if (!eventHappened)
            {
                //dudeIsInside = false;
                int count = myCollider.Cast(Vector2.zero, cf, hitBuffer);

                if (count == 0)
                    dudeIsInside = false;

                for (int i = 0; i < count; i++)
                {
                    //if (playerCollider == hitBuffer[i].collider)
                    {
                        dudeAlreadyOutside = false;
                        //print("Im in");

                        if (!dudeIsInside)
                        {
                            OnPlayerIsInside();
                            dudeIsInside = true;
                            break; //???? da testare
                        }
                        //break;
                    }
                    /*
                    if (i == count - 1)
                    {
                        dudeIsInside = false;
                    }
                    */
                }

            }

            if (!dudeIsInside)
            {
                if (!dudeAlreadyOutside)
                {
                    //print("out");
                    OnPlayerIsOutside();
                    dudeAlreadyOutside = true;
                }
            }
        }
    }

    public void EnablePrompt()
    {

    }

    public void DisablePrompt()
    {

        if (playersManager != null && playersManager.promptManager != null)
        {
            if (playersManager.promptManager.eventList.Contains(this))
            {
                playersManager.promptManager.eventList.Remove(this);
            }
        }
        
    }


    protected virtual void OnPlayerIsInside ()
    {
        //print("onplayerisinside");
        if (requestPrompt)
        {
            //if (!dudeIsInside)
                DisplayPrompt();
        }
        else
        {
            TriggerEvent();
        }
    }

    protected virtual void OnPlayerIsOutside ()
    {
        //print("onplayerisoutside");
        if (requestPrompt)
            DisablePrompt();
    }

    public virtual void DisplayPrompt()
    {
        //if (!dudeIsInside)
        {
            //print("booooooooom");
            promptText = GetPromptText();
            if (!playersManager.promptManager.eventList.Contains(this))
                playersManager.promptManager.eventList.Add(this);
            
        }
        
    }

    public virtual void TriggerEvent()
    {
        if (deactivatesOnEventTrigger)
            DeactivateEvent();

    }

    protected virtual void InitializeEvent()
    {

    }
}
