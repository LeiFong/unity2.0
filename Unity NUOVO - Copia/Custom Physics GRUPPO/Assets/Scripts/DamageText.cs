﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class DamageText : MonoBehaviour {


	public int textDuration = 30;
	private int currentDuration = 0;
	public Text damageText;
	public Vector3 position;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


		currentDuration++;

		if (currentDuration >= textDuration)
			gameObject.SetActive (false);
	}
}
