﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;


public class DialogueManager : MonoBehaviour
{
    public static DialogueManager Instance;

    private Dialogue currentDialogue;
    private Queue<Dialogue.DialogueElement> dialogueElements;
    private UIManager UImanager;
    public float inputDelay = 1f;
    public Text text;
    public MenuManager dialoguePanelMenuManager, optionChooserMenuManager;
    public DialogueOptionChooser optionChooser;
    private IEnumerator typingCoroutine;
    public int textSpeed = 2;
    [Space(20)]
    public NpcInteraction currentNpc;

    void Awake()
    {
        if (Instance == null)
            Instance = this;

        dialogueElements = new Queue<Dialogue.DialogueElement>();
        UImanager = UIManager.Instance ?? FindObjectOfType<UIManager>();
    }

    public void StartDialogue(Dialogue dialogue) {

        currentDialogue = dialogue;
        dialogueElements.Clear();

        foreach (Dialogue.DialogueElement element in dialogue.dialogueElements) {
            dialogueElements.Enqueue(element);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence() {
        if (dialogueElements.Count == 0) {
            EndDialogue();
            return;
        }

        dialoguePanelMenuManager.DelayInput(inputDelay);

        Dialogue.DialogueElement element = dialogueElements.Dequeue();
        BaseEventData b = new BaseEventData(EventSystem.current);
        element.customFunction.Invoke(b);

        
        
        if (typingCoroutine != null)
            StopCoroutine(typingCoroutine);

        if (!element.functionOnly)
        {
            switch (UImanager.language)
            {
                case UIManager.Language.english:
                    typingCoroutine = TypeSentence(UImanager.ChangeStringBasedOnGender(element.sentenceEnglish));
                    StartCoroutine(typingCoroutine);
                    break;
                case UIManager.Language.italian:
                    typingCoroutine = TypeSentence(UImanager.ChangeStringBasedOnGender(element.sentenceItalian));
                    StartCoroutine(typingCoroutine);
                    break;
            }
        }
        
    }

    public void EndDialogue() {

        dialoguePanelMenuManager.CloseMenu();
        BaseEventData b = new BaseEventData(EventSystem.current);
        currentDialogue.endDialogueFunction.Invoke(b);
    }
    

    public void DisplayOptions(DialogueOptionChooserCreator creator) {

        //dialoguePanelMenuManager.OpenChildMenuAndBecomeInvisible(optionChooserMenuManager);
        optionChooserMenuManager.OpenMenu();
        creator.SetUpDialogueOptionChooser(optionChooser);
    }

    private IEnumerator TypeSentence(string sentence) {
        text.text = "";
        if (textSpeed <= 0)
            yield break;
        int k = 0;
        foreach (char letter in sentence.ToCharArray()) {
            text.text += letter;
            k++;
            if (k >= textSpeed)
            {
                k = 0;
                yield return null;
            }
        }
        yield break;
    }

    public void EndNpcInteraction() {
        if (currentNpc == null)
            return;
        currentNpc.EndInteraction();
        currentNpc = null;
    }
}
