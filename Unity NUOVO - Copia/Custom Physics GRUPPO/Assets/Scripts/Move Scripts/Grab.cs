﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grab : MoveScript
{
    [System.Serializable]
    public struct DamageFrame
    {
        public int frame;
        public int damage;
    }

    [System.Serializable]
    public struct FramePeriod
    {
        public int start;
        public int end;
    }

    [System.Serializable]
    public struct EffectFrame
    {
        public int frame;
        public int hitstop;
        public float screenShakePotency;
        public int screenShakeDuration;
        public bool hitSparks;
        
       
    }

    public Controller2D victimController;
    public bool grabsAirborneEnemies;
    public DamageFrame[] damageFrames;
    public EffectFrame[] effectFrames;
    public FramePeriod[] stickyVictimPeriod; //periodo/i in cui la vittima si posiziona nel victimEndingPosition

    public Transform victimEndingPosition;

    public int myGrabDuration, victimGrabDuration;
    public GetGrabbed.FollowingMove victimFollowupMove = new GetGrabbed.FollowingMove
    {
        index = -1, type = moveType.animation
    };
    public string grabVictimAnimation;

    public override void MyStartSequence()
    {
        status.mystatus = Status.status.stuckinanimation;
        //victimController.animator.transform.parent = controller.animator.transform;
        //controller.animator.Rebind();

        //controller.animator.Play(animatorParameterName, 0);
    }


    public override void SetStatus()
    {
        status.mystatus = Status.status.stuckinanimation;
    }

    public override void MyExecuteSequence()
    {
        status.invincible = true;
        status.mystatus = Status.status.stuckinanimation;
        controller.avoidingCharacters = true;
        if (controller.hitstop <= 0)
        {
            for (int i = 0; i < effectFrames.Length; i++)
            {
                if (controller.seq == effectFrames[i].frame)
                {
                    if (effectFrames[i].hitstop > 0)
                    {
                        controller.hitstop = effectFrames[i].hitstop;
                    }
                    if (effectFrames[i].screenShakeDuration > 0)
                    {
                        controller.cameraMovement.Shake(effectFrames[i].screenShakeDuration, effectFrames[i].screenShakePotency);
                    }
                    if (effectFrames[i].hitSparks)
                    {
                        //moveset.myHitsparkGenerator.GenerateHitsparks(new Vector2((controller.facingRight ? 80 : -80), 20), 20);
                        if (controller.hitsparkRetriever != null)
                        {
                            controller.hitsparkRetriever.GenerateSparksOnHit(3, moveset.hitsparkOrigin, 1.4f * (controller.facingRight ? 1 : -1), 0.5f);
                        }
                        else
                        {
                            print("no hitspark retriever");
                        }
                    }
                }
            }
        }

        if (controller.seq < myGrabDuration)
        {
            controller.seq++;
        }
        else
        {
            //print("stop grab");
            itsTimeToStopTheSequence = true;
        }
    }

    public bool CheckGrabConditions (Controller2D victim)
    {
        if ((!grabsAirborneEnemies && victim.status.myPhysicsStatus == Status.physicsStatus.airborne) || victim.getGrabbed == null)
        {
            return false;
        }

        return true;
    }
}
