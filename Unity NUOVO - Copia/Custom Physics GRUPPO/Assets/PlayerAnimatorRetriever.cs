﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimatorRetriever : MonoBehaviour
{
    public Animator shaker, eyeEffects, skeleton, torso, eyesFlames;
    private float currentTimeScale;
    public Animator[] additionalTimeScaleAffectedAnimators;

    public void AdjustAnimatorsSpeedBasedOnTimeScale()
    {
        currentTimeScale = TimeVariables.currentTimeScale;

        if (shaker != null)
            shaker.speed = 1 / currentTimeScale;
        if (eyeEffects != null)
            eyeEffects.speed = 1 / currentTimeScale;
        if (torso != null)
            torso.speed = 1 / currentTimeScale;
        if (eyesFlames != null)
            eyesFlames.speed = 1 / currentTimeScale;

        for (int i = 0; i < additionalTimeScaleAffectedAnimators.Length; i++)
        {
            additionalTimeScaleAffectedAnimators[i].speed = 1 / currentTimeScale;
        }
    }
}