﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GlobalDoorData : GlobalGameStateData
{
    public bool open = false;
}
