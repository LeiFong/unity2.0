﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnHitProperties : MonoBehaviour {

	public MoveScript move;
   

    public virtual void Initialize()
    {
        //print("onhitprop: " + gameObject);
        //move = GetComponent<MoveScript>();
    } 

    //viene chiamata prima di onhitApply, per inizializzare e cose varie
    public virtual void OnHitStart()
    {

    }

    public virtual void OnHitApply () {

	}
}
