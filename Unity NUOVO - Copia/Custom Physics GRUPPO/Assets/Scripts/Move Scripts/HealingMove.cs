﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingMove : MoveScript
{
    public int duration;
    public int healFrame;
    public float healPercentage;
    public override void MyExecuteSequence()
    {
        if (controller.seq == healFrame)
        {
            status.currentHealth += Mathf.RoundToInt(status.maxHealth / 100 * healPercentage);
        }

        if (controller.seq >= duration)
            itsTimeToStopTheSequence = true;
        else
            controller.seq++;
    }
}
