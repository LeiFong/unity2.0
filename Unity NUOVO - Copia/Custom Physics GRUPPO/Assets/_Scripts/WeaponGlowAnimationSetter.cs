﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponGlowAnimationSetter : MonoBehaviour
{
    [Tooltip("Usa questo componente per settare il glow dell'arma, dipendente dalla stance e dall'heat. Mettine uno su ogni arma. " +
        "Metti un Animator di tipo \"Weapons Glow\" su questo GO. Quando cambi stance, chiama il metodo SetStance; quando passi a high / low heat, chiama " +
        "SetHighHeat e SetLowHeat.")]
    public bool mouseOverForTutorial;
    public Animator weaponAnimator;
    public ParticleSystem glowParticleSystem;
    public enum Stance { quick, power }
    private int animHighHeat, animGoHighHeat;
    private bool currentlyHighHeat = false;
    private float currentGlowWeight = 0;

    public void Start()
    {
        if (weaponAnimator == null)
            weaponAnimator = GetComponent<Animator>();
        if (glowParticleSystem == null)
            glowParticleSystem = GetComponent<ParticleSystem>();
        animHighHeat = Animator.StringToHash("high heat");
        animGoHighHeat = Animator.StringToHash("start high heat");
    }

    public void SetStance(Stance stance) {
        
        if (stance == Stance.quick)
            weaponAnimator.SetLayerWeight(1, 0);
        else
            weaponAnimator.SetLayerWeight(1, 1);
    }

    public void SetLowHeat() {
        if (currentlyHighHeat)
            glowParticleSystem.Emit(10);
        weaponAnimator.SetBool(animHighHeat, false);
        currentlyHighHeat = false;
    }

    public void SetHighHeat() {
        if (!currentlyHighHeat)
        {
            glowParticleSystem.Emit(22);
            weaponAnimator.SetTrigger(animGoHighHeat);
        }
        else
            weaponAnimator.SetBool(animHighHeat, true);
        currentlyHighHeat = true;
    }

    public void Glow() {
        currentGlowWeight = 1;
        weaponAnimator.SetLayerWeight(2, 1);
    }

    private void FixedUpdate()
    {
        if (currentGlowWeight > 0) {
            currentGlowWeight -= 0.04f / (float)TimeVariables.currentTimeScale;
            if (currentGlowWeight < 0)
                currentGlowWeight = 0;
            weaponAnimator.SetLayerWeight(2, currentGlowWeight);
        }
    }
}
