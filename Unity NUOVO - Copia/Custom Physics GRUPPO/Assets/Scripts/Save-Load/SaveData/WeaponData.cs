﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WeaponData
{

    public int weaponIndex, upgrade;
   public WeaponData(int weaponIndex, int upgrade)
   {
        this.weaponIndex = weaponIndex;
        this.upgrade = upgrade;
   }
}
