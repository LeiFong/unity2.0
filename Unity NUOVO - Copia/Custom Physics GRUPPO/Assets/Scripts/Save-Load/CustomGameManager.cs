﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class CustomGameManager : MonoBehaviour
{
    public PlayersManager playersManager;
    public Player player;
    public int currentSector;
    int chunkSize = 10;

    public bool isSerializing;

    [System.Serializable]
    public struct NPC
    {
        public Controller2D controller;
        public Status status;
        public Vector3 position;
        public bool facingRight;
        public bool hasDroppedLoot;
    }

    public Dictionary<string, GlobalGameStateData> globals = new Dictionary<string, GlobalGameStateData>();

    public void AddGlobal(GlobalGameStateData newGlobal)
    {
        //print("newglobal: " + newGlobal.myName);
        if (globals.ContainsKey(newGlobal.myName))
        {
            print("global " + newGlobal.myName + " già esistente");
            globals.Remove(newGlobal.myName);
        }
        globals.Add(newGlobal.myName, newGlobal);
        //print("added " + globals[newGlobal.myName].myName);
    }

    

    public void LoadGlobals()
    {
        globals.Clear();
        GlobalGameStateData[] globalData = SaveSystem.LoadGlobals();
        if (globalData == null)
            return;

        //print("loadGloabls: " + globalData.Length);
        for (int i = 0; i < globalData.Length; i++)
        {
            //print("global " + i + ": " + globalData[i].myName);
            AddGlobal(globalData[i]);
        }
    }
    
    /*
    private void Start()
    {
        CreateTestGlobal();
        PrintA();
    }
    */
    
    //public Dictionary<string, NPC> NPCs = new Dictionary<string, NPC>();

    //public Dictionary<string, Event> events = new Dictionary<string, Event>();

    public void SaveGlobals ()
    {
        GlobalGameStateData[] globalData = new GlobalGameStateData[globals.Count];
        int i = 0;
        foreach(string g in globals.Keys)
        {
            globalData[i] = globals[g];
            i++;
        }

        SaveSystem.SaveGlobals(globalData);
       
    }

    public GlobalGameStateData GetGlobal(string globalName)
    {
        if (globals.ContainsKey(globalName))
        {
            return globals[globalName];
        }
        else
        {
            print("global " + globalName + " non trovato");
            return null;
        }
    }


    public void InitializeEvent (Event newEvent, BackgroundAwaker backgroundAwaker)
    {
        if (backgroundAwaker.events.ContainsKey(newEvent.eventID))
            backgroundAwaker.events.Remove(newEvent.eventID);
        backgroundAwaker.events.Add(newEvent.eventID, newEvent);
        //print("event init " + newEvent.eventID);

    }

    public void SceneChange (int ind)
    {
        SceneManager.LoadScene(ind);
    }

    public void SaveEventData(int sceneIndex, BackgroundAwaker backgroundAwaker)
    {
        EventData[] eventDatas = new EventData[backgroundAwaker.events.Count];
        int i = 0;
        foreach (string id in backgroundAwaker.events.Keys)
        {
            EventData eventData = new EventData(backgroundAwaker.events[id]);
            eventDatas[i] = eventData;
            //eventData.eventID = events[id].eventID;
            //eventData.hasHappened = events[id].eventHappened;
            i++;
        }

        SaveSystem.SaveEvents(eventDatas, sceneIndex);
    }

    public void LoadEventData (int sceneIndex, BackgroundAwaker backgroundAwaker)
    {
        EventData [] eventData = SaveSystem.LoadEvents(sceneIndex);
        //print("data???" + eventData[0].hasHappened);
        if (eventData == null)
            return;

        for (int i = 0; i < eventData.Length; i++)
        {
            if (!backgroundAwaker.events.ContainsKey(eventData[i].eventID) || backgroundAwaker.events[eventData[i].eventID] == null)
            {
                print("no event found " + eventData[i].eventID);

                continue;
            }
            //print("event: " + eventData[i]);
            Vector3 position = new Vector3(eventData[i].
                position[0], eventData[i].position[1], eventData[i].position[2]);
            backgroundAwaker.events[eventData[i].eventID].transform.position = position;

            if (backgroundAwaker.events[eventData[i].eventID].resetsOnLoad) {
                backgroundAwaker.events[eventData[i].eventID].eventHappened = false;
                backgroundAwaker.events[eventData[i].eventID].ActivateEvent();
            }
            else
            {
                backgroundAwaker.events[eventData[i].eventID].eventHappened = eventData[i].hasHappened;
                if (backgroundAwaker.events[eventData[i].eventID].eventHappened)
                {
                    backgroundAwaker.events[eventData[i].eventID].DeactivateEvent();
                    //print("deactivate event");
                }
                else
                {
                    //print("activated eventttt " + events[eventData[i].eventID].eventHappened);
                    backgroundAwaker.events[eventData[i].eventID].ActivateEvent();
                }
                //if (eventData[i].hasHappened)
            }
        }

    }



    public void LoadEventDataByChunks(int sceneIndex, BackgroundAwaker backgroundAwaker, EventData[] eventData, int chunkStart, int chunkEnd)
    {
        
        //print("data???" + eventData[0].hasHappened);
        if (eventData == null)
            return;

        for (int i = chunkStart; i < Mathf.Min(eventData.Length, chunkEnd); i++)
        {
            if (!backgroundAwaker.events.ContainsKey(eventData[i].eventID) || backgroundAwaker.events[eventData[i].eventID] == null)
            {
                print("no event found " + eventData[i].eventID);

                continue;
            }
            //print("event: " + eventData[i]);
            Vector3 position = new Vector3(eventData[i].
                position[0], eventData[i].position[1], eventData[i].position[2]);
            backgroundAwaker.events[eventData[i].eventID].transform.position = position;

            if (backgroundAwaker.events[eventData[i].eventID].resetsOnLoad)
            {
                backgroundAwaker.events[eventData[i].eventID].eventHappened = false;
                backgroundAwaker.events[eventData[i].eventID].ActivateEvent();
            }
            else
            {
                backgroundAwaker.events[eventData[i].eventID].eventHappened = eventData[i].hasHappened;
                if (backgroundAwaker.events[eventData[i].eventID].eventHappened)
                {
                    backgroundAwaker.events[eventData[i].eventID].DeactivateEvent();
                    //print("deactivate event");
                }
                else
                {
                    //print("activated eventttt " + events[eventData[i].eventID].eventHappened);
                    backgroundAwaker.events[eventData[i].eventID].ActivateEvent();
                }
                //if (eventData[i].hasHappened)
            }
        }

    }



    //public List <NPC> NPCs = new List<NPC>();

    public void InitializeNPC(Controller2D controller, BackgroundAwaker backgroundAwaker)
    {
        if (backgroundAwaker == null)
        {
            print("NO BGAWAKER!!!!");
            return;
        }
        if (backgroundAwaker.NPCs.ContainsKey(controller.myID))
        {
            print("npc gia aggiunto!");
            backgroundAwaker.NPCs.Remove(controller.myID);
            //return;
        }

        NPC newNPC = new NPC();

        newNPC.controller = controller;
        newNPC.status = controller.status;
        newNPC.position = controller.lastSerializablePosition;
        newNPC.facingRight = controller.lastSerializableFacing;

        if (newNPC.controller.enemySpecializer != null)
        {
            newNPC.hasDroppedLoot = newNPC.controller.enemySpecializer.hasDroppedLoot;
            
        }
        else
            print("no enemyspppp");
        //newNPC.ID = controller.myID;

        backgroundAwaker.NPCs.Add(controller.myID, newNPC);
        //print("npc added: " + controller.myID + "     " + backgroundAwaker.NPCs.Count);
        //NPCs.Add(newNPC);
    }

    

    public void SavePlayerData ()
    {

        SaveSystem.SavePlayer(player.status);
        //BuffData buffData = new BuffData(player);
        //SaveSystem.SavePlayerBuffs(buffData);
    }

    public void SaveNPCData (int sceneIndex, BackgroundAwaker backgroundAwaker)
    {
        //SaveSystem.SaveScene(NPCs);
        int i = 0;
        /*
        if (playersManager.player1 != null)
            i++;
        if (playersManager.player2 != null)
            i++;
        */
        NPCData[] data = new NPCData[backgroundAwaker.NPCs.Count - i];
        //BuffData[] buffDatas = new BuffData[data.Length];
        i = 0;
        foreach (string ID in backgroundAwaker.NPCs.Keys)
        {
            
            if (player != backgroundAwaker.NPCs[ID].controller)
            {
                data[i] = new NPCData(ID, backgroundAwaker.NPCs[ID].status);
                //buffDatas[i] = new BuffData(NPCs[ID].controller);
                i++;
                //data[i].isPlayer = true;
            }
            else
            {
                //data[i].isPlayer = false;
            }

            
            
        }
        SaveSystem.SaveNPCs(data, sceneIndex);
        //SaveSystem.SaveNPCBuffs(buffDatas);

    }

    public void ResetNPCs (BackgroundAwaker backgroundAwaker)
    {
        //da aggiornare
        foreach (string ID in backgroundAwaker.NPCs.Keys)
        {
            if (playersManager.player1 == ((Player)backgroundAwaker.NPCs[ID].controller) || playersManager.player2 == ((Player)backgroundAwaker.NPCs[ID].controller))
                continue;

            if (backgroundAwaker.NPCs[ID].controller != null && backgroundAwaker.NPCs[ID].status != null)
            {
                if (backgroundAwaker.NPCs[ID].status.currentHealth <= 0 && backgroundAwaker.NPCs[ID].controller.enemySpecializer.onlyDiesOnce)
                    continue;

                backgroundAwaker.NPCs[ID].controller.transform.position = backgroundAwaker.NPCs[ID].controller.startingPosition;

                //if (NPCs[ID].status.currentHealth > 0 || !NPCs[ID].controller.enemySpecializer.onlyDiesOnce) 
                backgroundAwaker.NPCs[ID].status.currentHealth = backgroundAwaker.NPCs[ID].status.maxHealth;
                

                if (backgroundAwaker.NPCs[ID].controller.facingRight != backgroundAwaker.NPCs[ID].controller.initialFacingRight)
                    backgroundAwaker.NPCs[ID].controller.Flip();

                backgroundAwaker.NPCs[ID].controller.buffManager.buffs.Clear();

                if (backgroundAwaker.NPCs[ID].controller.enemySpecializer.hasDroppedLoot && backgroundAwaker.NPCs[ID].controller.enemySpecializer.onlyDropsOnce)
                    backgroundAwaker.NPCs[ID].controller.enemySpecializer.hasDroppedLoot = true;
                else
                {
                    backgroundAwaker.NPCs[ID].controller.enemySpecializer.hasDroppedLoot = false;
                    backgroundAwaker.NPCs[ID].controller.enemySpecializer.ResetLoots();
                }

                backgroundAwaker.NPCs[ID].controller.MyOnEnable();
            }
        }

    }

    public void LoadNPCData (int sceneIndex, BackgroundAwaker backgroundAwaker)
    {
        //POTENZIALMENTE VECCHIA, USARE LOADNPCDATABYCHUNKS


        NPCData [] data = SaveSystem.LoadNPCs(sceneIndex);
        //print("loadnpcdata: " + sceneIndex + " " + data.Length);

        foreach (string ID in backgroundAwaker.NPCs.Keys)
        {
            if (backgroundAwaker.NPCs[ID].controller != null && backgroundAwaker.NPCs[ID].status != null && player != backgroundAwaker.NPCs[ID].controller)
            {
                //print("npc starting load: " + backgroundAwaker.NPCs[ID].controller);
                backgroundAwaker.NPCs[ID].controller.transform.position = backgroundAwaker.NPCs[ID].controller.startingPosition;
                backgroundAwaker.NPCs[ID].status.currentHealth = backgroundAwaker.NPCs[ID].status.maxHealth;
                backgroundAwaker.NPCs[ID].controller.facingRight = backgroundAwaker.NPCs[ID].controller.transform.localScale.x > 0 ? true : false;
                backgroundAwaker.NPCs[ID].controller.enemySpecializer.hasDroppedLoot = false;
                backgroundAwaker.NPCs[ID].controller.MyOnEnable();
                backgroundAwaker.NPCs[ID].controller.buffManager.buffs.Clear();

                //NPCs[ID].controller.enemySpecializer.ResetLoots();

            }
           
        }

        if (data != null)
        {
            //print("data is not null " + );
            for (int i = 0; i < data.Length; i++)
            {
                if (!backgroundAwaker.NPCs.ContainsKey(data[i].myID))
                {
                    print("NPC non trovato!");
                    continue;
                }

                if (data[i].isPlayer)
                {
                    continue;
                }

                Vector3 newPosition = new Vector3(data[i].position[0], data[i].position[1], data[i].position[2]);


                if (backgroundAwaker.NPCs[data[i].myID].controller != null && backgroundAwaker.NPCs[data[i].myID].status != null)
                {
                    backgroundAwaker.NPCs[data[i].myID].controller.transform.position = newPosition;
                    backgroundAwaker.NPCs[data[i].myID].controller.lastSerializablePosition = newPosition;
                    backgroundAwaker.NPCs[data[i].myID].controller.lastSerializableFacing = data[i].facingRight;


                    backgroundAwaker.NPCs[data[i].myID].status.currentHealth = data[i].currentHealth;
                    //print("NPC caricato: " + data[i].myID + "     " + backgroundAwaker.NPCs[data[i].myID].controller + " position: " + backgroundAwaker.NPCs[data[i].myID].controller.transform.position);


                    if (backgroundAwaker.NPCs[data[i].myID].controller.enemySpecializer != null)
                    {
                        backgroundAwaker.NPCs[data[i].myID].controller.enemySpecializer.hasDroppedLoot = data[i].hasDroppedLoot;
                        if (!backgroundAwaker.NPCs[data[i].myID].controller.enemySpecializer.onlyDropsOnce)
                            backgroundAwaker.NPCs[data[i].myID].controller.enemySpecializer.ResetLoots();
                    }

                    backgroundAwaker.NPCs[data[i].myID].controller.buffManager.LoadBuffs(data[i].buffs);

                    if (backgroundAwaker.NPCs[data[i].myID].controller.facingRight != data[i].facingRight)
                    {
                        //NPCs[data[i].myID].controller.facingRight = !NPCs[data[i].myID].controller.facingRight;
                        backgroundAwaker.NPCs[data[i].myID].controller.Flip();
                    }
                    backgroundAwaker.NPCs[data[i].myID].controller.MyOnEnable();
                }
                //NPCs[data[i].myID].controller.facingRight = data[i].facingRight;
            }
           
        }
        else
        {
            Debug.LogError("nessuna scena nel gamemanager");
        }

        //LoadBuffs();

    }

    public void LoadNPCDataByChunks(int sceneIndex, BackgroundAwaker backgroundAwaker, NPCData[] data, int chunkStart, int chunkEnd)
    {
        //NPCData[] data = SaveSystem.LoadNPCs(sceneIndex);
        //print("loadnpcdata: " + sceneIndex + " " + data.Length);

        if (data != null)
        {
            //print("data is not null " + );
            for (int i = chunkStart; i < Mathf.Min(data.Length, chunkEnd); i++)
            {
                //print("load npc chunk " + chunkStart + " to " + chunkEnd + ": i = " + i);
                if (!backgroundAwaker.NPCs.ContainsKey(data[i].myID))
                {
                    print("NPC non trovato!");
                    continue;
                }

                if (data[i].isPlayer)
                {
                    continue;
                }

                Vector3 newPosition = new Vector3(data[i].position[0], data[i].position[1], data[i].position[2]);


                if (backgroundAwaker.NPCs[data[i].myID].controller != null && backgroundAwaker.NPCs[data[i].myID].status != null)
                {
                    backgroundAwaker.NPCs[data[i].myID].controller.transform.position = newPosition;
                    backgroundAwaker.NPCs[data[i].myID].controller.lastSerializablePosition = newPosition;
                    backgroundAwaker.NPCs[data[i].myID].controller.lastSerializableFacing = data[i].facingRight;


                    backgroundAwaker.NPCs[data[i].myID].status.currentHealth = data[i].currentHealth;
                    //print("NPC caricato: " + data[i].myID + "     " + backgroundAwaker.NPCs[data[i].myID].controller + " position: " + backgroundAwaker.NPCs[data[i].myID].controller.transform.position);


                    if (backgroundAwaker.NPCs[data[i].myID].controller.enemySpecializer != null)
                    {
                        backgroundAwaker.NPCs[data[i].myID].controller.enemySpecializer.hasDroppedLoot = data[i].hasDroppedLoot;
                        if (!backgroundAwaker.NPCs[data[i].myID].controller.enemySpecializer.onlyDropsOnce)
                            backgroundAwaker.NPCs[data[i].myID].controller.enemySpecializer.ResetLoots();
                    }

                    backgroundAwaker.NPCs[data[i].myID].controller.buffManager.LoadBuffs(data[i].buffs);

                    if (backgroundAwaker.NPCs[data[i].myID].controller.facingRight != data[i].facingRight)
                    {
                        //NPCs[data[i].myID].controller.facingRight = !NPCs[data[i].myID].controller.facingRight;
                        backgroundAwaker.NPCs[data[i].myID].controller.Flip();
                    }
                    backgroundAwaker.NPCs[data[i].myID].controller.MyOnEnable();
                }
                //NPCs[data[i].myID].controller.facingRight = data[i].facingRight;
            }

        }
        else
        {
            Debug.LogError("nessuna scena nel gamemanager");
        }

        //LoadBuffs();

    }
    /*
    public void LoadBuffs ()
    {
        BuffData [] buffData = SaveSystem.LoadNPCBuffs();
        if (buffData == null)
            return;

        for (int i = 0; i < buffData.Length; i++)
        {
            if (NPCs.ContainsKey(buffData[i].controllerID) && NPCs[buffData[i].controllerID].controller != player)
            {
                NPCs[buffData[i].controllerID].controller.buffManager.LoadBuffs(buffData[i]);
            }
            else
            {
                //print("no npc");
            }
        }
    }
    */
    public void LoadPlayerData ()
    {
        PlayerData data = SaveSystem.LoadPlayer();

        if (player != null && data != null)
        {
            player.status.currentHealth = data.currentHealth;
            player.status.currentStance = data.slowStance ? Status.Stances.slow : Status.Stances.fast;
            player.status.currentStamina = data.currentStamina;
            player.status.currentMeter = data.currentMeter;

            Vector3 newPosition = new Vector3(data.position[0], data.position[1], data.position[2]);
            player.transform.position = newPosition;

            //BuffData buffData = SaveSystem.LoadPlayerBuffs();
            

            
            player.buffManager.LoadBuffs(data.buffs);
            
        
            if (player.facingRight != data.facingRight)
            {
                player.Flip();
            }

            player.MyOnDisable();
            player.controls.Enable();
        }

        else
        {
            Debug.LogError("nessun player/data");
        }

    }

    public void SaveInventory(Inventory inventory)
    {
        SaveSystem.SaveInventory(inventory);
    }

    public void LoadInventory (Inventory inventory)
    {
        SaveSystem.LoadInventory(inventory);
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            //SaveAll();
            //StartCoroutine(SaveAllGradually(5));

        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            //SaveAll();
            //StartCoroutine(SaveAllGradually());
        }


        if (Input.GetKeyDown(KeyCode.J))
        {
            //StartCoroutine(LoadAllGradually(5));
            LoadGlobals();
            GlobalDoorData door = (GlobalDoorData) GetGlobal("porta1");
            print(door.myName + " " + door.open);
            door = (GlobalDoorData)GetGlobal("porta2");
            print(door.myName + " " + door.open);
            TestGlobal test = (TestGlobal)GetGlobal("test1");
            print(test.myName + " " + test.a);
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            //SaveSystem.CheckForSceneFolder(SceneManager.GetActiveScene().name);
            //SaveEventData();
            //LoadEventData();

            //ResetNPCs();
            GlobalDoorData door = new GlobalDoorData();
            door.myName = "porta1";
            door.open = true;

            AddGlobal(door);

            GlobalDoorData door2 = new GlobalDoorData();
            door2.myName = "porta2";
            door2.open = false;

            AddGlobal(door2);

            TestGlobal test = new TestGlobal();
            test.myName = "test1";
            test.a = "aaaa";

            AddGlobal(test);

            SaveGlobals();
            


        }      
        

    }

    public void LoadAll(int sceneIndex, BackgroundAwaker backgroundAwaker) {
        SaveSystem.CheckForSceneFolder(sceneIndex);
        LoadPlayerData();
        LoadNPCData(sceneIndex, backgroundAwaker);
        LoadInventory(playersManager.inventoryP1);
        LoadEventData(sceneIndex, backgroundAwaker);
    }

    public void SaveAll(int sceneIndex, BackgroundAwaker backgroundAwaker) {
        //print("inizio salvataggio " + Time.);
        SaveSystem.CheckForSceneFolder(sceneIndex);
        SavePlayerData();
        SaveNPCData(sceneIndex, backgroundAwaker);
        SaveInventory(playersManager.inventoryP1);
        SaveEventData(sceneIndex, backgroundAwaker);
        //print("fine salvataggio" + Time.unscaledTime);

    }

    public IEnumerator SaveAllGradually(int sceneIndex, BackgroundAwaker backgroundAwaker)
    {
        isSerializing = true;
        print("inizio salvataggio " + Time.unscaledTime + " " + backgroundAwaker.NPCs.Count);
        SaveSystem.CheckForSceneFolder(sceneIndex);
        yield return null;
        //SavePlayerData();
        SaveSystem.SavePlayer(player.status);
        yield return null;
        //BuffData buffData = new BuffData(player);
        //SaveSystem.SavePlayerBuffs(buffData);
        //yield return null;
        //SaveNPCData();
        int i = 0;
        /*
        if (playersManager.player1 != null)
            i++;
        if (playersManager.player2 != null)
            i++;
        */
        NPCData[] data = new NPCData[backgroundAwaker.NPCs.Count - i];
        //BuffData[] buffDatas = new BuffData[data.Length];
        i = 0;
        foreach (string ID in backgroundAwaker.NPCs.Keys)
        {

            if (player != backgroundAwaker.NPCs[ID].controller)
            {
                //print("salvo NPC: " + i + " " + Time.unscaledTime);
                data[i] = new NPCData(ID, backgroundAwaker.NPCs[ID].status);
                //buffDatas[i] = new BuffData(NPCs[ID].controller);
                i++;
                //data[i].isPlayer = true;
                if ((i % chunkSize) == 0)
                {
                    //print("aspetto: " + i);
                    yield return null;
                    
                }
            }
            else
            {
                //data[i].isPlayer = false;
            }

        }
        SaveSystem.SaveNPCs(data, sceneIndex);
        yield return null;
        //SaveSystem.SaveNPCBuffs(buffDatas);

        yield return null;
        SaveInventory(playersManager.inventoryP1);
        yield return null;
        SaveEventData(sceneIndex, backgroundAwaker);
        print("fine salvataggio" + Time.unscaledTime);
        isSerializing = false;
        
        




    }

    public IEnumerator LoadAllGradually(int sceneIndex, BackgroundAwaker backgroundAwaker)
    {
        isSerializing = true;
        SaveSystem.CheckForSceneFolder(sceneIndex);
        yield return null;
        LoadPlayerData();
        yield return null;
        //LOAD NPC DATA
        NPCData[] data = SaveSystem.LoadNPCs(sceneIndex);
        //print("loadnpcdata: " + sceneIndex + " " + data.Length);
        int i = 0;
        foreach (string ID in backgroundAwaker.NPCs.Keys)
        {
            if (backgroundAwaker.NPCs[ID].controller != null && backgroundAwaker.NPCs[ID].status != null && player != backgroundAwaker.NPCs[ID].controller)
            {
                //print("npc starting load: " + backgroundAwaker.NPCs[ID].controller);
                backgroundAwaker.NPCs[ID].controller.transform.position = backgroundAwaker.NPCs[ID].controller.startingPosition;
                backgroundAwaker.NPCs[ID].status.currentHealth = backgroundAwaker.NPCs[ID].status.maxHealth;
                backgroundAwaker.NPCs[ID].controller.facingRight = backgroundAwaker.NPCs[ID].controller.transform.localScale.x > 0 ? true : false;
                backgroundAwaker.NPCs[ID].controller.enemySpecializer.hasDroppedLoot = false;
                backgroundAwaker.NPCs[ID].controller.MyOnEnable();
                backgroundAwaker.NPCs[ID].controller.buffManager.buffs.Clear();

                i++;
                if ((i % chunkSize) == 0)
                    yield return null;
                //NPCs[ID].controller.enemySpecializer.ResetLoots();

            }

        }
        yield return null;

        //LOAD NPCS
        if (data != null)
        {
            i = 0;

            while (i * chunkSize < data.Length)
            {
                LoadNPCDataByChunks(sceneIndex, backgroundAwaker, data, i * chunkSize, i * chunkSize + chunkSize);
                i++;
                yield return null;
            }

        }
        
        LoadInventory(playersManager.inventoryP1);
        yield return null;

        //LOAD EVENTS
        EventData[] eventData = SaveSystem.LoadEvents(sceneIndex);
        //print("data???" + eventData[0].hasHappened);
        if (eventData != null)
        {
            i = 0;

            while (i * chunkSize < eventData.Length)
            {
                LoadEventDataByChunks(sceneIndex, backgroundAwaker, eventData, i * chunkSize, i * chunkSize + chunkSize);
                i++;
                yield return null;
            }

        }

        isSerializing = false;

    }

    public IEnumerator LoadSceneGradually(int sceneIndex, BackgroundAwaker backgroundAwaker)
    {
        print("inizio caricamento scena " + sceneIndex + " " + Time.unscaledTime + " " + backgroundAwaker.NPCs.Count);

        isSerializing = true;
        SaveSystem.CheckForSceneFolder(sceneIndex);
        yield return null;
        //LoadNPCData(sceneIndex, backgroundAwaker);



        //RESET NPCS
        NPCData[] data = SaveSystem.LoadNPCs(sceneIndex);
        //print("loadnpcdata: " + sceneIndex + " " + data.Length);
        int i = 0;
        foreach (string ID in backgroundAwaker.NPCs.Keys)
        {
            if (backgroundAwaker.NPCs[ID].controller != null && backgroundAwaker.NPCs[ID].status != null && player != backgroundAwaker.NPCs[ID].controller)
            {
                //print("npc starting load: " + backgroundAwaker.NPCs[ID].controller);
                backgroundAwaker.NPCs[ID].controller.transform.position = backgroundAwaker.NPCs[ID].controller.startingPosition;
                backgroundAwaker.NPCs[ID].status.currentHealth = backgroundAwaker.NPCs[ID].status.maxHealth;
                backgroundAwaker.NPCs[ID].controller.facingRight = backgroundAwaker.NPCs[ID].controller.transform.localScale.x > 0 ? true : false;
                backgroundAwaker.NPCs[ID].controller.enemySpecializer.hasDroppedLoot = false;
                backgroundAwaker.NPCs[ID].controller.MyOnEnable();
                backgroundAwaker.NPCs[ID].controller.buffManager.buffs.Clear();

                i++;
                if ((i % chunkSize) == 0)
                    yield return null;
                //NPCs[ID].controller.enemySpecializer.ResetLoots();

            }

        }
        yield return null;

        //LOAD NPCS
        if (data != null)
        {
            i = 0;

            while (i * chunkSize < data.Length)
            {
                LoadNPCDataByChunks(sceneIndex, backgroundAwaker, data, i * chunkSize, i * chunkSize + chunkSize);
                i++;
                yield return null;
            }

        }
        
        //LOAD EVENTS
        EventData[] eventData = SaveSystem.LoadEvents(sceneIndex);
        if (eventData != null)
        {
            i = 0;

            while (i * chunkSize < eventData.Length)
            {
                LoadEventDataByChunks(sceneIndex, backgroundAwaker, eventData, i * chunkSize, i * chunkSize + chunkSize);
                i++;
                yield return null;
            }
            
        }


        isSerializing = false;
        print("fine caricamento " + Time.unscaledTime);

    }


}
